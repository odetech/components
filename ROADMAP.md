# @odecloud/components Roadmap (Last updated February 2022)

Before you start working on a item below. Remember to visit [OdeTask](https://tasks.odecloud.app/) first to create a task that is relevant to what you intend to solve. 

_Every single pull request created should include a reference guide back to the task that it is solving._

## General
- [ ] Update the merge request guidelines and examples

## Dev Related:

... Update me when you have the chance!

## Devops:
- [X] Add Unit Testing to the CI/CD pipeline
- [ ] Add Linting to the CI/CD pipeline
- [ ] Add Integration Testing to the CI/CD pipeline
- [ ] Add StoryBook Testing / Cypress to the CI/CD pipeline
- [ ] Should consider updating Node version to `14.12.0-buster` since that is what our testing uses. Unit testing pipeline doesn't run on `node:12.12-buster`
- [ ] `build-job` should run on every `git push` to a branch
- [ ] Add caching for `build-job` to share its downloadable with the other images