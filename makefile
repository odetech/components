# Require node:12.16.1
# Can get the node version using nvm: nvm use 12.16.1

define run_build
	yarn
	yarn build
endef

npm-build:
	$(call run_build)

run-storybook:
	yarn .
	yarn build-storybook
	yarn storybook

run-unit-tests:
	$(call run_build)
	yarn unit-tests
