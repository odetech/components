import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import dts from 'rollup-plugin-dts';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import typescript from 'rollup-plugin-typescript2';
import postcss from 'rollup-plugin-postcss';
import alias from '@rollup/plugin-alias';
import babel from '@rollup/plugin-babel';
import image from '@rollup/plugin-image';
import json from '@rollup/plugin-json';

import packageJson from './package.json';

const bundle = (config) => ({
  ...config,
  input: 'src/index.ts',
  external: (id) => !/^[./|~/]/.test(id),
});

export default [bundle({
  input: './src/index.ts',
  output: [
    {
      file: packageJson.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true,
    },
    {
      file: packageJson.module,
      format: 'esm',
      exports: 'named',
      sourcemap: true,
    },
  ],
  plugins: [
    alias({
      entries: [{ find: /^~\/(.*)/, replacement: `${__dirname}/src/$1` }],
    }),
    peerDepsExternal(),
    resolve(),
    commonjs(),
    typescript({
      rollupCommonJSResolveHack: false,
      clean: true,
    }),
    postcss(),
    babel({
      babelHelpers: 'bundled',
      presets: [
        ['@babel/preset-env',
          {
            targets: { node: 'current' },
          }],
        '@babel/preset-react',
        [
          '@babel/preset-typescript',
          {
            onlyRemoveTypeImports: true,
            allExtensions: true,
            isTSX: true,
          },
        ],
      ],
    }),
    image({
      extensions: /\.(png|jpg|jpeg|gif|svg)$/,
      limit: 10000
    }),
    json(),
  ],
}), bundle({
  plugins: [
    dts(),
    alias({
      entries: [{ find: /^~\/(.*)/, replacement: `${__dirname}/src/$1` }],
    }),
  ],
  output: [
    {
      file: 'dist/index.d.ts',
      format: 'es',
    },
    {
      file: 'dist/src/index.d.ts',
      format: 'es',
    },
  ],
})];
