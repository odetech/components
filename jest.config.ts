export default {
  clearMocks: true,
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  roots: ['<rootDir>/tests/'],
  moduleNameMapper: {
    '^Tests(.*)$': '<rootDir>/tests',
    'react-modal': '<rootDir>/tests/test-utils/mocks/reactModalMock',
    '\\.(css|less)$': '<rootDir>/tests/test-utils/mocks/styleMock.ts',
  },
  setupFilesAfterEnv: [
    '<rootDir>/tests/setupTests.ts',
  ],
  testMatch: [
    '<rootDir>/tests/**/?(*.)+(spec|test).[tj]s?(x)',
  ],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest',
  },
  globals: {
    'ts-jest': {
      tsConfig: './tests/tsconfig.json',
    },
  },
};
