import React from 'react';
import { render } from '@testing-library/react';
import Input from '../../../../src/components/default-components/Input';

describe('shared/default-components/Input/InputStyles', () => {
  it('Input disabled check', async () => {
    const { getByTestId } = render(
      <div data-testid="unit-test-element">
        <Input
          name="input-unit-test"
          disabled
        />
      </div>,
    );
    const element = getByTestId('unit-test-element');
    const firstInput = await element.querySelector('input');
    expect(firstInput).toHaveStyle('color: #B1B1B1;');
  });
});
