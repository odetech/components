import React from 'react';
import { render } from '@testing-library/react';
import Input from '../../../../src/components/default-components/Input';

describe('shared/default-components/Input/InputStyles', () => {
  it('Input paddings check', () => {
    const { getByTestId } = render(
      <div data-testid="unit-test-element">
        <Input
          name="input-unit-test"
        />
      </div>,
    );
    const element = getByTestId('unit-test-element');
    const firstInput = element.querySelector('input');
    expect(firstInput).toHaveStyle('padding: 10px;');
  });
});
