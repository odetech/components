import React from 'react';
import { render } from '@testing-library/react';
import DatePicker from '../../../../src/components/default-components/DatePicker';

describe('shared/default-components/Datepicker/index', () => {
  it('Datepicker color check', () => {
    const { getByTestId } = render(
      <div data-testid="unit-test-element">
        <DatePicker
          name="datepicker-unit-test"
          startDate={new Date()}
          onChange={() => ({})}
        />
      </div>,
    );
    const element = getByTestId('unit-test-element');
    const firstInput = element.querySelector('input');
    expect(firstInput).toHaveStyle('color: #3B3C3B;');
  });
});
