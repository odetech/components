import React from 'react';
import { render } from '@testing-library/react';
import Button from '../../../../src/components/default-components/Button';
import { defaultTheme } from '../../../../src/styles/themes';

describe('shared/default-components/Button/index', () => {
  it('Check choseColor -> Orange button (default values)', () => {
    const { getByText } = render(<Button>Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.brand.primary} none`);
  });

  it('Check choseColor -> Red button ', () => {
    const { getByText } = render(<Button color="red">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background',`${defaultTheme.error.primary} none`);
  });

  it('Check choseColor -> Green button ', () => {
    const { getByText } = render(<Button color="green">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.success.primary} none`);
  });

  it('Check choseColor -> Pink button ', () => {
    const { getByText } = render(<Button color="pink">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.brand.secondary} none`);
  });

  it('Check choseColor -> Yellow button ', () => {
    const { getByText } = render(<Button color="yellow">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.visualisation.yellow} none`);
  });

  it('Check choseColor -> Blue button ', () => {
    const { getByText } = render(<Button color="blue">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.info.primary} none`);
  });

  it('Check choseColor -> Purple button ', () => {
    const { getByText } = render(<Button color="purple">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.visualisation.purple} none`);
  });

  it('Check choseColor -> Black button ', () => {
    const { getByText } = render(<Button color="black">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.visualisation.black} none`);
  });

  it('Check choseColor -> Grey button ', () => {
    const { getByText } = render(<Button color="grey">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.text.onSurface} none`);
  });

  it('Check choseColor -> Default button ', () => {
    const { getByText } = render(<Button color="default">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.brand.primary} none`);
  });
});
