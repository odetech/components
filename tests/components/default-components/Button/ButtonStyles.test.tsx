import React from 'react';
import { render } from '@testing-library/react';
import Button from '../../../../src/components/default-components/Button';
import { defaultTheme } from '../../../../src/styles/themes';

describe('shared/default-components/Button/ButtonStyles', () => {
  it('Should be transparent button', () => {
    const { getByText } = render(<Button color="pink">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', '#E54367 none');
  });
  it('Should be red button', () => {
    const { getByText } = render(<Button color="red" view="default">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', `${defaultTheme.error.primary} none`);
  });
  it('Should be transparent button because view is another', () => {
    const { getByText } = render(<Button color="red" view="text">Default text</Button>);
    expect(getByText('Default text')).toHaveStyleRule('background', 'transparent none');
  });
});
