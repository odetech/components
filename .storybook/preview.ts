import "./styles.css";
import "bootstrap/dist/css/bootstrap.min.css";

const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    layout: 'centered',
  },
};
export default preview;
