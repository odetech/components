import { StorybookConfig } from '@storybook/react-webpack5';
import * as path from "path";
import 'dotenv/config';

const config: StorybookConfig = {
  framework: {
    name: '@storybook/react-webpack5',
    options: {}
  },
  stories: ["../src/**/*.stories.tsx"],
  addons: ["@storybook/addon-actions", "@storybook/addon-controls", "@storybook/addon-links", "@storybook/addon-essentials", "storybook-addon-jsx"],
  typescript: {
    reactDocgen: 'react-docgen-typescript',
    reactDocgenTypescriptOptions: {
      compilerOptions: {
        allowSyntheticDefaultImports: false,
        esModuleInterop: false,
        types: ["node", "jest", "cypress", "@testing-library/jest-dom"]
      },
      propFilter: () => true,
    },
  },
  features: {
    // @ts-ignore
    emotionAlias: false,
    babelModeV7: true
  },
  docs: {
    autodocs: true,
    defaultName: "docs",
  },
  async babel(config, { configType }) {
    config.plugins?.push("effector/babel-plugin")
    return config;
  },
  webpackFinal: async (config, { configType }) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      '~': path.resolve(__dirname, "../src"),
    };
    return config;
  },
  env: (config) => ({
    ...config,
    REACT_APP_IKINARI_URL: process.env.REACT_APP_IKINARI_URL || "",
    REACT_APP_IKINARI_TOKEN: process.env.REACT_APP_IKINARI_TOKEN || "",
    REACT_APP_IKINARI_ROOT_URL: process.env.REACT_APP_IKINARI_ROOT_URL || "",
  })
};

export default config;
