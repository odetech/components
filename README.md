# Official site of @odecloud/components

[![pipeline status](https://gitlab.com/odetech/components/badges/main/pipeline.svg)](https://gitlab.com/odetech/components/-/commits/main)[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Installation

- Use NPM:
    ```sh
    $ npm install @odecloud/components
    ```

## Running Unit Tests

You will need `node:20-buster`. You can use `nvm` to install the correct version of node.
On the root of the project, run the following line:
- `make run-unit-tests`

## Integration with Chromatic's Storybook
Please read the wiki [here](https://gitlab.com/odetech/components/-/wikis/Workflow-with-Storybook-&-Publishing-to-NPM) in regards to workflow with Chromatic's Storybook.

## Publishing to NPM

1. On the root of the directory, open up **package.json**
2. Bump the __version__ by the following guideline:
    - Our version numbering follows **Major.Minor.Patch** (e.g. 2.10.1)
        - **Major**: Stable release.
        - **Minor**: Incremental changes--including new components, remove components, or change of behavior of components.
        - **Patch**: Small efficient changes--including a fixed to a bug.
    - **Note**: in regards to Patch if the old functionality was always erroneous, then it will be considered a Patch.
3. Publish a new tag on the repository by going to https://gitlab.com/odetech/components/-/tags/new.
    - **Note**: make sure that the "Tag Name" is an exact match to the version inside `package.json` on step #2.
    - In regards to the "Release notes": we encourage detail and consistent format in order for other developers to understand the new version update.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
