import React from 'react';
import { transparentize } from 'polished';
import { css } from '@emotion/react';
import { Modal, Button } from '~/components/default-components';
import { CloseIcon } from '../../../icons';
import {
  ModalButtonsWrapper, ModalInfoBody, ModalInfoTitle,
} from '../../TimeTrackerStyles';

type TimeTrackerStopModalProps = {
  showModal: boolean;
  setShowModal: (b: boolean) => void;
  stopRequest: (b: boolean) => void;
};

export const TimeTrackerStopModal: React.FC<TimeTrackerStopModalProps> = ({
  showModal, setShowModal, stopRequest,
}) => (
  <Modal
    isOpen={showModal}
    onClose={() => setShowModal(false)}
    contentStyles={{
      maxHeight: 260,
      maxWidth: 330,
    }}
    overlayStyles={{
      background: `${transparentize(0.3, '#000000')}`,
    }}
    buttonStyles={css`
      svg {
        width: 24px;
        height: 24px;
        transform: unset;
        
        path {
          fill: #9c9c9c;
        }
      }
    `}
    closeIcon={<CloseIcon />}
  >
    <ModalInfoTitle>Stop tracking time?</ModalInfoTitle>
    <ModalInfoBody>
      You can resume anytime within 24 hours
    </ModalInfoBody>
    <ModalButtonsWrapper>
      <Button
        type="button"
        color="red"
        onClick={() => {
          setShowModal(false);
          stopRequest(false);
        }}
      >
        Pause
      </Button>
      <Button
        type="button"
        onClick={() => {
          setShowModal(false);
          stopRequest(true);
        }}
        color="green"
      >
        Stop
      </Button>
    </ModalButtonsWrapper>
  </Modal>
);
