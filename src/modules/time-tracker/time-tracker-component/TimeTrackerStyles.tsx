import styled from '@emotion/styled';
import { UncontrolledTooltip } from 'reactstrap';
import { Rnd } from 'react-rnd';

type TimeTrackerTimerButtonProps = {
  isActive: boolean;
}

export const TimeTrackerRnd = styled(Rnd)`
  display: flex !important;
  align-items: center;
  gap: 8px;
  z-index: 10;
`;

export const TimeTrackerTimerButton = styled('button')<TimeTrackerTimerButtonProps>`
  display: flex;
  align-items: center;
  padding: 8px 16px;

  width: 150px;
  height: 45px;
  border-radius: 30px;
  font-weight: 500;
  font-size: 20px;
  line-height: 30px;

  ${({ isActive }) => (isActive ? `
    background: #17A700;
    color: white;
    svg {
      fill: white;
    }
  ` : `
    background: #CECECE;
    border: 1px solid #E5E5E5;
    color: #3B3C3B;
    svg {
      fill: #3B3C3B;
    }
  `)}
  
  svg {
    width: 30px;
    height: 30px;
  }

  &:disabled {
    cursor: not-allowed;
    opacity: 0.3;
  }
`;

export const TimeTrackerButton = styled('button')`
  display: flex;
  padding: 8px;
  align-items: center;
  justify-content: center;
  gap: 8px;

  width: 45px;
  height: 45px;
  background: ${({ color }) => color};
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
  border-radius: 30px;

  svg {
    width: 20px;
    height: 20px;
  }

  &:disabled {
    cursor: not-allowed;
    opacity: 0.3;
  }
`;

export const TimeTrackerInfoTooltip = styled(UncontrolledTooltip)`

  .tooltip {
    opacity: unset !important;
    
    &:after {
      position: absolute;
      left: -4px !important;
      top: 45%;
      content: '';
      width: 10px;
      height: 10px;
      background: black;
      transform: rotate(45deg);
    }
  }

  .tooltip-inner {
    max-width: unset;
    display: flex;
    padding: 6px 10px;
    flex-direction: column;
    align-items: flex-start;
    color: white;
    text-align: center;
    background-color: black;
  }
`;

export const BoldSpan = styled('span')`
  font-weight: bold;
`;

export const TimeTrackerWrapper = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  z-index: 9;
`;

export const ModalButtonsWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  
  button {
    padding: 12px 16px;
    border-radius: 8px;

    &:first-child {
      background: #F0F0F0;
      color: #9C9C9C;
    }

    &:last-child {
      background: #E54367;
      color: white;
    }
  }
`;

export const ModalInfoTitle = styled('div')`
  text-align: center;
  margin: 42px 0 16px 0;
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  color: #3B3C3B;
  
  & ~ button {
    background: yellow;
  }
`;

export const ModalInfoBody = styled('div')`
  text-align: center;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;
  margin-bottom: 32px;
  color: #9C9C9C;
`;
