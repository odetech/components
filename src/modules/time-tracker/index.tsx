/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect, useRef } from 'react';
import { Database } from 'firebase/database';
import { useUnit } from 'effector-react';
import {
  $$timeTrackersModel,
} from '~/entities/time-trackers';
import {
  TimeTrackerTimerButton,
  TimeTrackerButton,
  TimeTrackerWrapper,
  TimeTrackerRnd,
  TimeTrackerInfoTooltip,
  BoldSpan,
} from './time-tracker-component/TimeTrackerStyles';
import { getCurrentTimeTrackingValue, getStartTime } from './helpers';
import {
  ClockIcon, PlayIcon, StopIcon, QuestionIcon,
} from './icons';
import { useSubscriptionTimeTracker } from './hooks';
import { TimeTrackerStopModal } from './time-tracker-component/components/time-tracker-stop-modal';

export type TimeTrackerPanelProps = {
  dbFirebase: Database | null;
  currentUserId: string;
};

export const TimeTrackerPanel = ({
  dbFirebase,
  currentUserId,
}: TimeTrackerPanelProps) => {
  if (!dbFirebase) {
    return null;
  }
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const rnd: any = useRef(null);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const currentTimeTracker = useUnit($$timeTrackersModel.$activeTimeTracker);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const isLoading = useUnit($$timeTrackersModel.$activeTimeTrackerLoading);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const updateTimeTracker = useUnit($$timeTrackersModel.activeTimeTrackerUpdated);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const timeTracker = useSubscriptionTimeTracker(currentUserId, dbFirebase);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [trackingTime, setTrackingTime] = useState('00:00:00');
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [showStopModal, setShowStopModal] = useState(false);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  useEffect(() => {
    if (timeTracker) {
      const startTime = getStartTime(timeTracker);
      const interval = setInterval(() => {
        setTrackingTime(getCurrentTimeTrackingValue(startTime));
      }, 1000);
      return () => {
        clearInterval(interval);
      };
    }
    return undefined;
  }, [timeTracker]);

  const handleOnClick = () => {
    if (timeTracker) {
      setShowStopModal(true);
    } else {
      updateTimeTracker(false);
    }
  };
  if (!currentTimeTracker?.id) return null;
  return (
    <TimeTrackerWrapper>
      {showStopModal && (
        <TimeTrackerStopModal
          showModal={showStopModal}
          setShowModal={setShowStopModal}
          stopRequest={updateTimeTracker}
        />
      )}
      <TimeTrackerRnd
        ref={rnd}
        // @ts-ignore
        default={{
          x: 5,
          y: window.innerHeight - 90,
          height: 75,
        }}
        enableResizing={false}
        cancel="button"
      >
        <TimeTrackerTimerButton
          onClick={handleOnClick}
          type="button"
          disabled={isLoading}
          isActive={!!timeTracker}
        >
          <ClockIcon />
          {' '}
          {` ${trackingTime}`}
        </TimeTrackerTimerButton>
        {timeTracker ? (
          <TimeTrackerButton type="button" color="#3B3C3B" onClick={handleOnClick} disabled={isLoading}>
            <StopIcon />
          </TimeTrackerButton>
        ) : (
          <TimeTrackerButton type="button" color="#10B20D" onClick={handleOnClick} disabled={isLoading}>
            <PlayIcon />
          </TimeTrackerButton>
        )}
        <TimeTrackerButton type="button" color="#CECECE" id="timeTrackerPanelTooltip" disabled={isLoading}>
          <QuestionIcon />
        </TimeTrackerButton>
        <TimeTrackerInfoTooltip
          popperClassName="timeTrackerPanelTooltip"
          placement="right"
          target="timeTrackerPanelTooltip"
        >
          <div>
            <span>Client:</span>
            {' '}
            <BoldSpan>{` ${currentTimeTracker?.orgName}`}</BoldSpan>
          </div>
          <div>
            <span>Project:</span>
            {' '}
            <BoldSpan>{` ${currentTimeTracker?.projectName}`}</BoldSpan>
          </div>
          <div>
            <span>Task:</span>
            {' '}
            <BoldSpan>{` ${currentTimeTracker?.task?.title}`}</BoldSpan>
          </div>
        </TimeTrackerInfoTooltip>
      </TimeTrackerRnd>
    </TimeTrackerWrapper>
  );
};
