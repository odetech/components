import React from 'react';
import styled from '@emotion/styled';

type SvgProps = {
  cssWidth?: string;
  cssHeight?: string;
  fill?: string;
};

const StyledIcon = styled('svg')<SvgProps>`
  width: ${({ cssWidth }) => (cssWidth || '16px')};
  height: ${({ cssHeight }) => (cssHeight || '16px')};
  fill: ${({ fill }) => fill};
  transition: fill .3s;
`;

export const ClockIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 30 30" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M13.75 5.06866V3.75H12.5V1.25H17.5V3.75H16.25V5.06866C18.4194 5.30847 20.4028 6.16512 22.0211
      7.45933L24.1142 5.35707L25.8858 7.12096L23.7891 9.22691C25.3291 11.1516 26.25 13.5933 26.25 16.25C26.25
      22.4632 21.2132 27.5 15 27.5C8.7868 27.5 3.75 22.4632 3.75 16.25C3.75 10.4593 8.12506 5.69047 13.75
      5.06866ZM15 25C19.8325 25 23.75 21.0825 23.75 16.25C23.75 11.4175 19.8325 7.5 15 7.5C10.1675 7.5 6.25
      11.4175 6.25 16.25C6.25 21.0825 10.1675 25 15 25ZM16.25 17.5V10H13.75V17.5H16.25Z"
    />
  </StyledIcon>
);

export const StopIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 30 30" {...props}>
    <rect width="30" height="30" rx="15" fill="#3B3C3B" />
    <rect x="7" y="7" width="16" height="16" rx="2" fill="white" />
  </StyledIcon>
);

export const PlayIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 20 20" {...props}>
    <path d="M5 17.5V2.5L16.25 10.2586L5 17.5Z" fill="white" />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4.16602 2.49876V17.4988C4.16602 18.1581 4.89539 18.5563 5.44998 18.1997L17.1166
          10.6997C17.6269 10.3717 17.6269 9.62581 17.1166 9.29778L5.44998 1.79778C4.89539 1.44125 4.16602
          1.83945 4.16602 2.49876ZM15.1243 9.99705L5.83203 15.9707V4.02344L15.1243 9.99705Z"
      fill="white"
    />
  </StyledIcon>
);

export const PauseIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon width="12" height="18" viewBox="0 0 12 18" {...props}>
    {/* eslint-disable-next-line max-len */}
    <path fillRule="evenodd" clipRule="evenodd" d="M3.5013 0.660156H1.83464C0.914161 0.660156 0.167969 1.40635 0.167969 2.32682V15.6602C0.167969 16.5806 0.914161 17.3268 1.83464 17.3268H3.5013C4.42178 17.3268 5.16797 16.5806 5.16797 15.6602V2.32682C5.16797 1.40635 4.42178 0.660156 3.5013 0.660156ZM10.168 0.660156H8.5013C7.58083 0.660156 6.83464 1.40635 6.83464 2.32682V15.6602C6.83464 16.5806 7.58083 17.3268 8.5013 17.3268H10.168C11.0884 17.3268 11.8346 16.5806 11.8346 15.6602V2.32682C11.8346 1.40635 11.0884 0.660156 10.168 0.660156ZM1.83464 2.32682V15.6602H3.5013V2.32682H1.83464ZM8.5013 15.6602V2.32682H10.168V15.6602H8.5013Z" fill="white" />
  </StyledIcon>
);

export const QuestionIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M1 12C1 18.0751 5.92487 23 12 23C18.0751 23 23 18.0751 23 12C23 5.92487 18.0751 1 12 1C5.92487 1 1 5.92487
          1 12ZM21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21
          7.02944 21 12ZM12.0003 16.9983C12.5528 16.9983 13.0007 16.5506 13.0007 15.9983C13.0007 15.4461 12.5528
          14.9983 12.0003 14.9983C11.4479 14.9983 11 15.4461 11 15.9983C11 16.5506 11.4479 16.9983 12.0003 16.9983ZM11
          14H13C13 13.2016 13.1254 13.0553 13.9472 12.6444C15.3754 11.9303 16 11.2016 16 9.5C16 7.32063 14.2843 6 12
          6C9.79086 6 8 7.79086 8 10H10C10 8.89543 10.8954 8 12 8C13.2772 8 14 8.55641 14 9.5C14 10.2984 13.8746
          10.4447 13.0528 10.8556C11.6246 11.5697 11 12.2984 11 14Z"
      fill="#3B3C3B"
    />
  </StyledIcon>
);

export const CloseIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path d="M18 6L6 18" stroke="#9C9C9C" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M6 6L18 18" stroke="#9C9C9C" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
  </StyledIcon>
);
