import { Action, ThunkAction } from '@reduxjs/toolkit';

export type errorProps = {
  status?: number,
  statusText?: string,
  type?: string,
  url?: string,
  detail: string,
};

export type TTimeTrackerState = {
  activeTimeTracker: {
    data: TTimeTracker | null,
    loading: boolean,
    error: errorProps | null,
  }
}

export type TTimeTracker = {
  id: string;
  taskId: string;
  accumulative: number;
  updatedAt: string;
  times: TTimes[];
  createdBy: string;
  createdAt: string;
  orgName: string;
  message?: string;
  projectName: string;
  task: {
    title: string;
    id?: string;
    description: string;
    priority: number;
    titleWithProject?: string;
  },
  trackedBy: {
    name: string;
    avatarUrl: string;
  }
};

export type TTimerTrackerDataBackend = {
  orgId: { name: string };
  projectId: { title: string };
  taskId: {
    title: string;
    description: string;
    priority: number;
  };
  trackedBy: {
    _id: string;
    profile: {
      firstName: string;
      lastName: string;
      avatar: {
        secureUrl: string;
      }
    }
  }
}

export type TTimeBackend = { startedAt: number; endedAt: number; _id: string; };

export type TTimerTrackerBackend = {
  _id?: string;
  id: string;
  taskId: string;
  accumulative: number;
  updatedAt: string;
  times: TTimeBackend[],
  createdBy: string;
  createdAt: string;
  data: TTimerTrackerDataBackend;
  message: any;
}

export type TTimes = {
  start: number;
  end: number | null;
  id?: string;
};

export type AppThunk = ThunkAction<void, TTimeTrackerState, unknown, Action<string>>;
