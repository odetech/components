/* eslint-disable react/destructuring-assignment */
import React, { useEffect, useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import { initializeApp } from '@firebase/app';
import { getDatabase } from '@firebase/database';
import OdeCloud from '@odecloud/odecloud';
import { BeatLoader } from 'react-spinners';
import { Database } from 'firebase/database';
import { componentsInit } from '~/config';
import testUser from '../../../../configs/testUserCredentials.json';
import testFirebaseConfig from '../../../../configs/testFirebaseConfig.json';
import { TimeTrackerPanel, TimeTrackerPanelProps } from '../index';

let api: any = null;

const meta: Meta<TimeTrackerPanelProps & {
  loginEmail: string,
  loginPassword: string,
  app: string,
  endpoint: string,
  shouldReloadChatStorybook: boolean,
  isStaffMode: boolean,
  firebaseApiKey: string,
  firebaseAuthDomain: string,
  firebaseDatabaseURL: string,
  firebaseProjectId: string,
  firebaseStorageBucket: string,
  firebaseMessagingSenderId: string,
  firebaseAppId: string,
}> = {
  component: TimeTrackerPanel,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.margin = 'unset';
      }
      return (
        <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
          {story()}
        </MemoryRouter>
      );
    },
  ],
  argTypes: {
    loginEmail: { name: 'loginEmail', control: 'text' },
    loginPassword: { name: 'loginPassword', control: 'text' },
    app: { name: 'app', control: 'text' },
    endpoint: { name: 'endpoint', control: 'text' },
    shouldReloadChatStorybook: { control: 'boolean' },
    isStaffMode: { control: 'boolean' },
    firebaseApiKey: { name: 'firebaseApiKey', control: 'text' },
    firebaseAuthDomain: { name: 'firebaseAuthDomain', control: 'text' },
    firebaseDatabaseURL: { name: 'firebaseDatabaseURL', control: 'text' },
    firebaseProjectId: { name: 'firebaseProjectId', control: 'text' },
    firebaseStorageBucket: { name: 'firebaseStorageBucket', control: 'text' },
    firebaseMessagingSenderId: { name: 'firebaseMessagingSenderId', control: 'text' },
    firebaseAppId: { name: 'firebaseAppId', control: 'text' },
  },
  args: {
    loginEmail: testUser.email || '',
    loginPassword: testUser.password || '',
    app: 'odetask',
    endpoint: 'https://server-beta.odecloud.app/api/v1',
    shouldReloadChatStorybook: false,
    isStaffMode: false,
    firebaseApiKey: testFirebaseConfig.apiKey || '',
    firebaseAuthDomain: testFirebaseConfig.authDomain || '',
    firebaseDatabaseURL: testFirebaseConfig.databaseURL || '',
    firebaseProjectId: testFirebaseConfig.projectId || '',
    firebaseStorageBucket: testFirebaseConfig.storageBucket || '',
    firebaseMessagingSenderId: testFirebaseConfig.messagingSenderId || '',
    firebaseAppId: testFirebaseConfig.appId || '',
  },
};
export default meta;

const startFirebase = (testConfigData: {
  apiKey: string,
  authDomain: string,
  databaseURL: string,
  projectId: string,
  storageBucket: string,
  messagingSenderId: string,
  appId: string,
}) => {
  const firebaseConfig = {
    apiKey: testConfigData.apiKey,
    authDomain: testConfigData.authDomain,
    databaseURL: testConfigData.databaseURL,
    projectId: testConfigData.projectId,
    storageBucket: testConfigData.storageBucket,
    messagingSenderId: testConfigData.messagingSenderId,
    appId: testConfigData.appId,
  };

  const app = initializeApp(firebaseConfig);
  return getDatabase(app);
};

// 👇 We create a “template” of how args map to rendering
export const Default: StoryFn<TimeTrackerPanelProps & {
  loginEmail: string,
  loginPassword: string,
  app: string,
  endpoint: string,
  shouldReloadChatStorybook: boolean,
  isStaffMode: boolean,
  firebaseApiKey: string,
  firebaseAuthDomain: string,
  firebaseDatabaseURL: string,
  firebaseProjectId: string,
  firebaseStorageBucket: string,
  firebaseMessagingSenderId: string,
  firebaseAppId: string,
}> = (args) => {
  const [dbFirebase, setDbFirebase] = useState<Database | null>(null);
  const [userId, setCurrentUserId] = useState<string | null>(null);
  const getData = async () => {
    api = new OdeCloud({
      endpoint: args.endpoint,
    });

    const loginResult = await api.auth.login({
      email: args.loginEmail,
      password: args.loginPassword,
      appUrl: 'http://localhost:3000',
    });
    const { authToken, userId: loginUserId } = loginResult;
    const url = 'https://tasks.odecloud.app';

    await api.resync({ authToken, userId: loginUserId, url });
    const userDataRequest = await api.users.getUser(
      loginUserId,
      {
        roles: 1,
        skillsets: 1,
        masterNotifications: 1,
        appStore: 1,
        profileCompletion: 1,
      },
    );
    setCurrentUserId(loginUserId);
    const firebase = startFirebase({
      apiKey: args.firebaseApiKey,
      authDomain: args.firebaseAuthDomain,
      databaseURL: args.firebaseDatabaseURL,
      projectId: args.firebaseProjectId,
      storageBucket: args.firebaseStorageBucket,
      messagingSenderId: args.firebaseMessagingSenderId,
      appId: args.firebaseAppId,
    });
    componentsInit({
      api,
      currentUser: userDataRequest,
      firebase,
    });

    setDbFirebase(firebase);
  };

  useEffect(() => {
    getData();
  }, []);

  if (userId) {
    return (
      <TimeTrackerPanel
        {...args}
        currentUserId={userId}
        dbFirebase={dbFirebase}
      />
    );
  }
  // If it's loading
  return (
    <BeatLoader color="#F7942A" size={8} />
  );
};
