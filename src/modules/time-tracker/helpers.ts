import { TTimeTracker } from '~/entities/time-trackers';
import { ensureUtcTimestamp } from '~/helpers';
import { TTimerTrackerBackend, TTimeBackend } from './types';

export const getTimeTrackingValue = (accumulative: number | null, withoutSeconds = false) : string => {
  if (!accumulative) {
    return withoutSeconds ? '00:00' : '00:00:00';
  }

  const h = Math.floor(Math.trunc(accumulative / 3600));
  const m = Math.floor(((accumulative / 3600) % 1) * 60);
  const minutesCeil = Math.ceil(((accumulative / 3600) % 1) * 60);
  const s = Math.floor(((accumulative / 60) % 1) * 60);

  if (withoutSeconds) {
    return `${h < 10 ? `0${h}` : h}:${minutesCeil < 10 ? `0${minutesCeil}` : minutesCeil}`;
  }
  return `${h < 10 ? `0${h}` : h}:${m < 10 ? `0${m}` : m}:${s < 10 ? `0${s}` : s}`;
};

export const getCurrentTimeTrackingValue = (start: number | null) : string => {
  if (!start) {
    return '00:00:00';
  }

  const today = new Date();
  const diffSeconds = Math.ceil((today.getTime() - start) / 1000);

  return getTimeTrackingValue(diffSeconds);
};

export const mapTimeTracker = ({
  _id, id, taskId, accumulative, updatedAt, times, createdBy, createdAt, data, message,
}: TTimerTrackerBackend): any => ({
  id: _id || id,
  taskId,
  accumulative,
  times: times.map((time: TTimeBackend) => ({
    start: Math.trunc(time.startedAt) * 1000,
    end: time.endedAt ? Math.trunc(time.endedAt) * 1000 : null,
    id: time._id,
  })),
  createdBy,
  createdAt: ensureUtcTimestamp(createdAt),
  updatedAt,
  orgName: data?.orgId?.name,
  projectName: data?.projectId?.title,
  message,
  task: {
    title: data?.taskId?.title,
    description: data?.taskId?.description,
    priority: data?.taskId?.priority,
    id: _id,
    _id,
    titleWithProject: `${data?.projectId?.title} - ${data?.taskId?.title}`,
  },
  trackedBy: {
    name: `${data?.trackedBy?.profile?.firstName} ${data?.trackedBy?.profile?.lastName}`,
    avatarUrl: data?.trackedBy?.profile?.avatar?.secureUrl,
    id: data?.trackedBy?._id,
  },
});

export const getStartTime = (tracker: TTimeTracker) : number | null => {
  let start = null;

  tracker.times.forEach((time) => {
    if (time.end === null) {
      start = time.start - (Math.floor(tracker.accumulative) * 1000);
    }
  });

  return start;
};
