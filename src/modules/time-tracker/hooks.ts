import { useMemo } from 'react';
import { useObjectVal } from 'react-firebase-hooks/database';
import { ref, Database } from 'firebase/database';
import { useUnit } from 'effector-react';
import { TTimeTracker, $$timeTrackersModel } from '~/entities/time-trackers';
import { mapTimeTracker } from './helpers';

export const useSubscriptionTimeTracker = (currentUserId: string, dbFirebase: Database) : TTimeTracker | null => {
  const setCurrentTimeTracker = useUnit($$timeTrackersModel.activeTimeTrackerChanged);
  const [firebaseTimeTracker] = useObjectVal<string | Record<string, TTimeTracker>>(
    ref(dbFirebase, `/time_trackers/${currentUserId}/current_active`),
  );

  return useMemo<TTimeTracker | null>(() => {
    let trackingTimeTracker: TTimeTracker | null = null;
    if (typeof firebaseTimeTracker === 'string') {
      trackingTimeTracker = firebaseTimeTracker && mapTimeTracker(JSON.parse(firebaseTimeTracker));
    }

    if (trackingTimeTracker?.id) {
      setCurrentTimeTracker(trackingTimeTracker);
    }
    return trackingTimeTracker;
  }, [firebaseTimeTracker]);
};
