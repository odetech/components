import React, { MouseEvent } from 'react';
import { Formik } from 'formik';
import { transparentize } from 'polished';
import { BeatLoader } from 'react-spinners';
import { Avatar, UserOptionComponent } from '~/index';
import { Modal, Select } from '~/components/default-components';
import { useTheme } from '@storybook/theming';
import { defaultTheme } from '../../../../styles/themes';
import { PlusIcon } from '../../icons';
import newChatModalValidation from '../../validation';
import {
  MembersLabel,
  NewChatTitle,
  MemberButton,
  SaveButton,
  StyledForm,
  ChatTitleFormikInput,
  UserInfoWrapper,
  MembersListWrapper,
  MembersList,
  ErrorTextStyle,
  WideInputWrapper,
} from '../ChannelsChatStyles';

type NewChatModalProps = {
  title: string;
  showModal: boolean;
  setShowModal: (showModal: boolean) => void;
  request: (
    requestData: {
      title: string,
      members: string[],
    },
  ) => void;
  currentUserFirstName: string,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
  isLoading: boolean,
}

const NewChatModal = ({
  showModal, setShowModal, request, title, currentUserFirstName, networkConnections, isLoading,
}: NewChatModalProps) => {
  const theme = useTheme();

  return (
    <Modal
      isOpen={showModal}
      onClose={() => setShowModal(false)}
      contentStyles={{
        maxWidth: 514,
        maxHeight: 628,
        height: 'fit-content',
      }}
      overlayStyles={{
        background: `${transparentize(0.3, theme?.background?.surface || defaultTheme.background.surface)}`,
      }}
    >
      <NewChatTitle>
        {title}
      </NewChatTitle>
      <Formik
        initialValues={{
          title: '',
          members: [],
          selectValue: null,
          availableOptions: (networkConnections?.networks?.map((item: {
          _id: string,
          profile: {
            firstName: string,
            lastName: string,
          }
        }) => ({
            value: JSON.stringify(item),
            label: `${item.profile.firstName} ${item.profile.lastName}`,
            ...item.profile,
          }))) || [],
        }}
        onSubmit={(values) => {
          const membersIdArray = values.members.map((item: {
          value: string,
          label: string,
        }) => JSON.parse(item.value)._id);

          let chatTitle = '';
          if (values.title === '' && currentUserFirstName) {
            chatTitle = `${currentUserFirstName.charAt(0)}`;
            values.members.forEach((item: {
            value: string,
            label: string,
          }) => {
              chatTitle = `${chatTitle}&${JSON.parse(item.value).profile.firstName.charAt(0)}`;
            });
          } else {
            chatTitle = values.title;
          }
          const payload: {
          title: string,
          members: string[],
        } = {
          title: chatTitle,
          members: membersIdArray,
        };

          request(payload);
          setShowModal(false);
        }}
        validate={newChatModalValidation}
      >
        {({
          values, setFieldValue, errors,
        }) => (
          <StyledForm>
            <WideInputWrapper>
              <ChatTitleFormikInput
                name="title"
                placeholder="Chat Title"
                margin="0 0 15px 0"
                errorText={errors.title}
              />
            </WideInputWrapper>
            <Select
              name="userSelect"
              options={values.availableOptions.filter((item: {
              label: string,
              value: string,
            }) => !values.members
                .find((member: any) => item.value === member.value))}
              onChange={(data) => {
                setFieldValue('members', [...values.members, data]);
              }}
              value={values.selectValue}
              placeholder="Add Chat Members"
              components={{
                Option: UserOptionComponent,
              }}
              closeMenuOnSelect={false}
            />
            <MembersLabel>
              Chat Members
            </MembersLabel>
            <MembersListWrapper>
              <MembersList
                isEmpty={values.members.length === 0}
              >
                {values.members.length !== 0 ? values.members.map((member: {
                value: string,
                label: string,
              }) => {
                  const memberValue = JSON.parse(member.value);
                  return (
                    <MemberButton
                      view="text"
                      onClick={(e: MouseEvent<HTMLButtonElement>) => {
                        e.preventDefault();
                        const memberIndex = values.members.findIndex((memberData: {
                        value: string,
                        label: string,
                      }) => memberData.value === member.value);
                        const updatedMembers = [...values.members]
                          .filter((item) => item !== values.members[memberIndex]);
                        setFieldValue('members', updatedMembers);
                      }}
                      key={`memberButton${member.value}`}
                    >
                      <UserInfoWrapper>
                        <Avatar user={memberValue} size={28} />
                        <span>{member.label}</span>
                      </UserInfoWrapper>
                      <PlusIcon />
                    </MemberButton>
                  );
                }) : (
                  'No selected members'
                )}
              </MembersList>
              {errors && errors.members && <ErrorTextStyle>{errors.members}</ErrorTextStyle>}
            </MembersListWrapper>
            <SaveButton
              type="submit"
              disabled={isLoading || values.members.length === 0}
            >
              {isLoading ? (
                <BeatLoader
                  color={theme?.brand?.primary || defaultTheme.brand.primary}
                  size={8}
                />
              ) : 'Start'}
            </SaveButton>
          </StyledForm>
        )}
      </Formik>
    </Modal>
  );
};

export default NewChatModal;
