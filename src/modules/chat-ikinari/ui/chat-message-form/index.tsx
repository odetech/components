import React, {
  Fragment, useMemo, useRef,
} from 'react';
import { Form, Formik } from 'formik';
import { useUnit } from 'effector-react';
import { extractHTMLContent } from '~/helpers';
import { dataURLtoFile } from '~/components/editor/slate-editor.image-helpers';
import { $$messagesModel } from '~/entities/chat/model/messages';
import { $$chatModel } from '~/entities/chat/model';
import { AutoSaveDraftMessageInChatRoom } from '../../../../entities/chat/helpers';
import {
  ChatRoomEditorWrapper,
} from '../ChannelsChatStyles';
import {
  ChatMessageWithMoreDataProps,
} from '../../../../entities/chat/types';
import { SlateEditor } from '../../../../components/editor';

type ValueProps = {
  message: string,
  files: { file: File, uiId: string }[],
  uiIds: string[],
  associatedId?: string | undefined,
}

type SuggestionProps = {
  _id: string,
  profile: {
    firstName: string,
    lastName: string,
  }
}

type ChatMessageFormProps = {
  onMessageSending?: () => void,
  setEdit: (a: any) => void,
  isEdit: any | null,
  setReply: (a: any) => void,
  replyData: any | null,
  editChatRoomMessageRequest: (messageNewData: ChatMessageWithMoreDataProps) => void,
  setIsOpenLightbox: (isOpenLightbox: boolean) => void,
  setPhotoIndex: (photoIndex: number) => void,
  setImages: (images: string[]) => void,
};

const ChatMessageForm = ({
  onMessageSending, setEdit, isEdit, editChatRoomMessageRequest,
  setImages, setPhotoIndex, setIsOpenLightbox, setReply, replyData,
}: ChatMessageFormProps) => {
  const filesRef = useRef<any>({});
  const messagesModel = useUnit({
    sent: $$messagesModel.sent,
    sentReply: $$messagesModel.sentReply,
  });
  const activeChat = useUnit($$chatModel.$activeChat);

  const { suggestions } = useMemo<{ suggestions: SuggestionProps[], userIds: string[] }>(() => {
    const suggestionsData = activeChat?.data.members || [];
    const userIdsData: string[] = suggestionsData.map((suggestion) => suggestion._id);
    return {
      suggestions: suggestionsData,
      userIds: userIdsData,
    };
  }, [activeChat?.data.members]);

  const onSubmit = (value: ValueProps) => {
    messagesModel.sent(value);
    // sent
  };

  let draftData: {
    message: string,
    files: {
      file: File,
      uiId: string,
    }[],
    uiIds: string[],
  } | null = null;

  const chatRoomsDraftData = JSON.parse(localStorage.getItem('chatRoomsDraftData') || 'null');
  if (chatRoomsDraftData?.chatRoomsDrafts && activeChat) {
    const draftMessageChatRoomIndex = chatRoomsDraftData.chatRoomsDrafts
      .findIndex((
        chatRoom: {
          id: string,
          message: string,
          files: any[],
          uiIds: string[],
        },
      ) => (activeChat && chatRoom.id === activeChat._id));

    if (draftMessageChatRoomIndex !== undefined && draftMessageChatRoomIndex !== -1) {
      draftData = {
        ...chatRoomsDraftData.chatRoomsDrafts[draftMessageChatRoomIndex],
        files: chatRoomsDraftData.chatRoomsDrafts[draftMessageChatRoomIndex].files
          .map((fileData: {
            file: string,
            uiId: string,
            fileName: string,
          }) => ({
            file: dataURLtoFile(fileData.file, fileData.fileName),
            uiId: fileData.uiId,
            url: fileData.file,
          })),
      };
    }
  }

  if (activeChat?._id) {
    return (
      <Fragment
        key={(isEdit?._id) || 'chatEditor'}
      >
        <Formik
          initialValues={{
            message: isEdit ? isEdit.text || isEdit.value : (draftData?.message || ''),
            files: draftData?.files || [],
            uiIds: draftData?.uiIds || [],
          }}
          onSubmit={async (values: ValueProps, formikActions) => {
            if (isEdit) {
              const messageData = {
                ...isEdit,
                text: values.message,
                rawText: extractHTMLContent(values.message),
              };
              messageData.files = [...values.files];
              messageData.uiIds = [...values.uiIds];
              if (activeChat) {
                editChatRoomMessageRequest(messageData);
              }

              setEdit(null);
            } else {
              await onSubmit({
                ...values,
                associatedId: replyData ? replyData._id : undefined,
              });
              setReply(null);

              if (onMessageSending) {
                onMessageSending();
              }
            }
            formikActions.setFieldValue('message', '');
            formikActions.setFieldValue('files', []);
            formikActions.setFieldValue('uiIds', []);
          }}
        >
          {({ setFieldValue, values, handleSubmit }) => (
            <Form>
              {!isEdit && (
                <AutoSaveDraftMessageInChatRoom
                  chatRoomId={activeChat?._id || ''}
                />
              )}
              <ChatRoomEditorWrapper>
                <SlateEditor
                  setFieldValue={setFieldValue}
                  value={values.message}
                  valueFiles={values.files}
                  editValue={isEdit ? isEdit.text || isEdit.value : ''}
                  closeEdit={() => {
                    setEdit(null);
                    if (replyData) {
                      setReply(null);
                    }
                  }}
                  field="message"
                  handleSubmit={handleSubmit}
                  userSuggestions={suggestions}
                  isMinimal
                  isChannelMention
                  uploadImageCallBack={(file: File, uiId: string) => {
                    if (filesRef.current) filesRef.current[uiId] = ({ file, uiId });
                    setFieldValue('files', Object.values(filesRef.current));
                  }}
                  setImages={setImages}
                  setIsOpenLightbox={setIsOpenLightbox}
                  setPhotoIndex={setPhotoIndex}
                  files={isEdit?.files ? isEdit.files : (draftData?.files || [])}
                  submitOnEnter
                  withAutoFocus
                  maxInputHeight={300}
                  replyData={replyData}
                />
              </ChatRoomEditorWrapper>
            </Form>
          )}
        </Formik>
      </Fragment>
    );
  }
  return null;
};

export default ChatMessageForm;
