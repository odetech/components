import React from 'react';
import { useUnit } from 'effector-react';
import { $$messagesModel } from '~/entities/chat/model';
import { OpenChatButton, NewMessagesCircle } from './ChatButtonStyles';
import { MessageOutlineIcon } from '../../icons';

type ChatButtonProps = {
  setIsChatOpen: (value: boolean) => void,
  isChatOpen: boolean,
};

export const ChatButton = ({
  isChatOpen,
  setIsChatOpen,
}: ChatButtonProps) => {
  const unreadMessageCount = useUnit($$messagesModel.$unreadMessageCount);

  if (!isChatOpen) {
    return (
      <OpenChatButton
        onClick={() => {
          setIsChatOpen(true);
        }}
      >
        <MessageOutlineIcon />
        {!!unreadMessageCount?.total && (
          <NewMessagesCircle>
            {unreadMessageCount?.total}
          </NewMessagesCircle>
        )}
      </OpenChatButton>
    );
  }

  return null;
};
