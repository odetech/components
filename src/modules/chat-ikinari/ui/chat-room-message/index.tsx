import React, { Fragment, useState } from 'react';
import 'react-image-lightbox/style.css';
import { useUnit } from 'effector-react';
import {
  OriginalReplyMessageItemsWrap,
} from '~/components/editor/slate-editor.styled';
import { ensureUtcTimestamp, getFormattedDate, getUserName } from '~/helpers';
import { Reactions } from '~/entities/reactions';
import { $$chatModel } from '~/entities/chat/model';
import { useTheme } from '@storybook/theming';
import { defaultTheme } from '../../../../styles/themes';
import {
  PencilIcon, AlertCircleOutlineIcon, DeleteIcon, ArrowLeftTopIcon, ChevronDownIcon, CheckIcon, CheckAllIcon,
} from '../../icons';
import ChatAvatar from '../chat-avatar';
import { ChatMessageWithMoreDataProps, UserData } from '../../../../entities/chat/types';
import {
  ChatMessageWrapper,
  MessageContentWrapper,
  MessageUnreadWrapper,
  DateWrapper,
  ChatMessageContainer,
  MessageErrorWrap,
  ChatUserName,
  OriginalReplyMessageSmall,
  MessageBottom,
  MessageControlButton,
  MessageActionsDropdownToggle,
  MessageActionsDropdownMenuItem,
  MessageActionsDropdownMenu,
  MessageActionsDropdown,
  MessageActionsDropdownWrapper,
  MessageMainContainer,
  ChatOriginalReplyMessageWrap,
} from '../ChannelsChatStyles';
import { SlateReader } from '../../../../components/editor';

type ChatRoomMessageProps = {
  currentUserData: UserData,
  onMessageAvatarClick: (userData: UserData['info'] | null) => void,
  setEdit: (isEdit: any) => void,
  setReply: (isEdit: any) => void,
  setShowDeleteModal: (showDeleteModal: boolean) => void,
  setDeleteMessage: (deleteMessage: null | ChatMessageWithMoreDataProps) => void,
  setIsOpenLightbox: (isOpenLightbox: boolean) => void,
  setPhotoIndex: (photoIndex: number) => void,
  setImages: (images: string[]) => void,
  chatRoomHighlightMessageId?: string | null,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  message: ChatMessageWithMoreDataProps,
  setShowModal: (isShow: boolean) => void,
  setShouldScrollToTheLatestUpdatedData: (value: boolean) => void,
  setPdfFile: (pdfFile: string) => void,
  isStaffMode: boolean,
  showAvatar: boolean,
  isFirstUnreadMessage?: boolean,
  showDate: boolean,
  clearQuery?: () => void,
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
};

const ChatRoomMessage = ({
  currentUserData, onMessageAvatarClick, chatRoomHighlightMessageId, message, setEdit, setDeleteMessage,
  setShowDeleteModal, setIsOpenLightbox, setPhotoIndex, setImages, setShowModal, setPdfFile, isStaffMode,
  showAvatar, showDate, setReply, setChatRoomHighlightMessageId, clearQuery, onMentionClick, onHashtagClick,
  isFirstUnreadMessage, setShouldScrollToTheLatestUpdatedData,
}: ChatRoomMessageProps) => {
  const theme = useTheme();
  const urlParams = new URLSearchParams(window.location.search);
  const activeChat = useUnit($$chatModel.$activeChat);
  const isStaffFromQuery = urlParams.get('isStaff');
  const isCurrentUser = !!message && !!message.createdBy && message.createdBy === currentUserData.id;

  const chatMemberData = isCurrentUser ? currentUserData.info : activeChat?.data.members
    ?.find((member) => member._id === message.createdBy) || {
    _id: null,
    profile: {
      firstName: 'Unknown',
      lastName: 'User',
      avatar: null,
    },
  };

  const date = message?.createdAt && new Date(ensureUtcTimestamp(message.createdAt).replace('+00:00', ''));
  const [isActionsMenuOpen, setIsActionsMenuOpen] = useState(false);
  const isRead = isCurrentUser ? !!message?.views?.find((userId) => (userId !== currentUserData.id))
    : false;

  return (
    <Fragment>
      {showDate && date && (
        <DateWrapper>
          <span>
            {getFormattedDate(ensureUtcTimestamp(message?.createdAt), 'month')}
          </span>
        </DateWrapper>
      )}
      {isFirstUnreadMessage ? <MessageUnreadWrapper>Unread messages</MessageUnreadWrapper> : null}
      <ChatMessageContainer
        isHighlight={!!message && !!chatRoomHighlightMessageId && chatRoomHighlightMessageId === message._id}
      >
        <ChatMessageWrapper
          isCurrentUserMessage={isCurrentUser}
          showAvatar={showAvatar}
        >
          {showAvatar && (
            <ChatAvatar
              onMessageAvatarClick={onMessageAvatarClick}
              chatMemberData={chatMemberData}
              isCurrentUserAvatar={isCurrentUser}
              currentUserData={currentUserData}
            />
          )}
          <MessageContentWrapper
            isCurrentUser={isCurrentUser}
            showAvatar={showAvatar}
          >
            {showAvatar && !isCurrentUser && (
            <ChatUserName>
              {getUserName(chatMemberData)}
            </ChatUserName>
            )}
            {message.associatedId && (
            <ChatOriginalReplyMessageWrap
              onClick={async () => {
                if (clearQuery) {
                  clearQuery();
                }
                await setChatRoomHighlightMessageId(null);
                if (message?.associatedMessageData?._id) {
                  setShouldScrollToTheLatestUpdatedData(true);
                  setChatRoomHighlightMessageId(message.associatedMessageData._id);
                }
              }}
            >
              <OriginalReplyMessageItemsWrap>
                <ChatUserName isSmall>
                  {getUserName(message.associatedMessageData?.data?.createdBy)}
                </ChatUserName>
                <OriginalReplyMessageSmall>
                  {message.associatedMessageData?.previewText}
                </OriginalReplyMessageSmall>
              </OriginalReplyMessageItemsWrap>
            </ChatOriginalReplyMessageWrap>
            )}
            <MessageMainContainer>
              <SlateReader
                  // @ts-ignore
                value={message?.text || message?.value || ''}
                setImages={setImages}
                setIsOpenLightbox={setIsOpenLightbox}
                setPhotoIndex={setPhotoIndex}
                setShowModal={setShowModal}
                setPdfFile={setPdfFile}
                files={message?.files || []}
                onMentionClick={onMentionClick}
                onHashtagClick={onHashtagClick}
              />
              {(isCurrentUser || (
                currentUserData.isStaff && Number(isStaffFromQuery) === 1
              ) || isStaffMode) && message._id && (
              <MessageActionsDropdownWrapper>
                <MessageActionsDropdown
                  isOpen={isActionsMenuOpen}
                  toggle={() => {
                    setIsActionsMenuOpen(!isActionsMenuOpen);
                  }}
                  backgroundColor={isCurrentUser
                    ? (theme?.brand?.secondaryContainer || defaultTheme.brand.secondaryContainer) : null}
                >
                  <MessageActionsDropdownToggle
                    isOpen={isActionsMenuOpen}
                  >
                    <ChevronDownIcon />
                  </MessageActionsDropdownToggle>
                  <MessageActionsDropdownMenu>
                    <Fragment>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          setEdit({
                            ...message,
                            data: {
                              files: message.files,
                              createdBy: activeChat?.data?.members?.find((t) => t._id === message.createdBy) || {},
                            },
                            files: message.files,
                          });
                        }}
                      >
                        <PencilIcon />
                        Edit
                      </MessageActionsDropdownMenuItem>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          setShowDeleteModal(true);
                          if (activeChat) {
                            setDeleteMessage(message || null);
                          }
                        }}
                      >
                        <DeleteIcon />
                        Delete
                      </MessageActionsDropdownMenuItem>
                    </Fragment>
                    {currentUserData.isStaff && Number(isStaffFromQuery) === 1 && (
                    <Fragment>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          navigator.clipboard
                            .writeText(message?._id || '');
                        }}
                      >
                        Copy message ID
                      </MessageActionsDropdownMenuItem>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          navigator.clipboard
                            .writeText(activeChat?._id || '');
                        }}
                      >
                        Copy Chat Room ID
                      </MessageActionsDropdownMenuItem>
                    </Fragment>
                    )}
                  </MessageActionsDropdownMenu>
                </MessageActionsDropdown>
              </MessageActionsDropdownWrapper>
              )}
            </MessageMainContainer>
            <Reactions
              itemType="chat"
              itemData={message}
              messageId={message._id}
              position="left"
              isSmall
              justifyReverse={!isCurrentUser}
            />
            <MessageBottom
              isCurrentUser={isCurrentUser}
            >
              {!message._id ? (
                <div style={{
                  display: 'flex',
                }}
                >
                  <div>
                    {!message._id && !isRead && <CheckIcon />}
                  </div>
                </div>
              ) : (
                <MessageControlButton
                  type="button"
                  onClick={() => {
                    setReply({
                      ...message,
                      data: {
                        files: message.files,
                        createdBy: activeChat?.data?.members?.find((t) => t._id === message.createdBy) || {},
                      },
                    });
                  }}
                >
                  <ArrowLeftTopIcon />
                  <span>Reply</span>
                </MessageControlButton>
              )}
              {date && (
              <div style={{ display: 'flex' }}>
                {isCurrentUser && (
                <span
                  style={{
                    fontSize: '10px',
                    fontWeight: 300,
                    whiteSpace: 'nowrap',
                    margin: '4px 2px 0 auto',
                  }}
                  id={`message-item-${message._id}`}
                >
                  {isRead && message._id && (
                    <CheckAllIcon
                      fill={theme?.brand?.secondary || defaultTheme.brand.secondary}
                    />
                  )}
                  {message._id && !isRead && <CheckAllIcon />}
                </span>
                )}
                <div
                  style={{
                    fontSize: '10px',
                    fontWeight: 300,
                    whiteSpace: 'nowrap',
                    margin: '5px 0 0 auto',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    color: theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar,
                  }}
                >
                  <span>{getFormattedDate(ensureUtcTimestamp(message.createdAt).replace('+00:00', ''), 'time')}</span>
                </div>
              </div>
              )}
            </MessageBottom>
          </MessageContentWrapper>
          {message.error && (
            <MessageErrorWrap>
              <AlertCircleOutlineIcon />
            </MessageErrorWrap>
          )}
        </ChatMessageWrapper>
      </ChatMessageContainer>
    </Fragment>
  );
};

export default ChatRoomMessage;
