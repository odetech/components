import React, { Fragment } from 'react';
import { useStoreMap } from 'effector-react';
import {
  getInitials, getTimeDiffFromNow,
} from '~/helpers';
import { $$messagesModel } from '~/entities/chat/model';
import {
  getAssociatedData,
} from '../../../../entities/chat/helpers';
import {
  ChatButtonAvatarWrapper,
  ChatButtonAvatar,
  ChatButtonAvatarPlaceholder,
  ChatButtonInfoWrapper,
  ChatButtonInfoTitle,
  ChatButtonInfoMessage,
  ChatButton, ChatButtonInfoHeader,
  ChatButtonInfoTimeDot, ChatButtonInfoTime, ChatButtonAvatarsWrapper,
} from '../ChannelsChatStyles';
import { ChatDataProps } from '../../../../entities/chat/types';

type ChatRoomButtonProps = {
  chatRoomData: ChatDataProps & {
    messageSearchInfo?: {
      _id: string,
      rawText: string,
      filesLength: number,
      createdAt: string,
      tag: string,
      isAlreadyLoaded: boolean,
    }
  },
  updateActiveChatData: (chatId: string) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string) => void,
}

const ChatRoomButton = ({
  chatRoomData,
  updateActiveChatData,
  setChatRoomHighlightMessageId,
}: ChatRoomButtonProps) => {
  const unreadMessages: any = useStoreMap({
    store: $$messagesModel.$unreadMessages,
    keys: [chatRoomData?._id],
    fn: (chats, [id]) => chats[id!],
  });
  const unreadMessagesAmount: number = useStoreMap({
    store: $$messagesModel.$unreadMessageCount,
    keys: [chatRoomData?._id],
    // @ts-ignore
    fn: (data) => (chatRoomData?._id ? data?.tags?.[chatRoomData?._id] : 0),
  });
  const getLastChatMessage = () => {
    if (!chatRoomData.data?.messages?.data.length) return '* New chat *';

    const message = chatRoomData.data?.messages?.data[chatRoomData.data.messages.data.length - 1] || null;

    if (!message) return '';

    return getAssociatedData(message).previewText;
  };

  const getChatRoomSearchMessage = () => {
    const filesLength = chatRoomData.messageSearchInfo?.filesLength || 0;
    let previewText = '';

    if (filesLength) {
      previewText = `🖼️ ${chatRoomData.messageSearchInfo?.rawText || ''}`;
    } else {
      previewText = chatRoomData.messageSearchInfo?.rawText || '';
    }
    return previewText;
  };

  const message = chatRoomData.data?.messages?.data?.[(chatRoomData.data?.messages.data.length || 0) - 1] || null;

  const dateString = chatRoomData.messageSearchInfo
    ? chatRoomData.messageSearchInfo.createdAt.replace('+00:00', '')
    : message?.createdAt.replace('+00:00', '');

  return (
    <ChatButton
      onClick={async () => {
        await updateActiveChatData(chatRoomData._id);
        // Update of chatRoomHighlightMessageId if it is a message search chat room click
        if (chatRoomData.messageSearchInfo && chatRoomData.messageSearchInfo._id) {
          setChatRoomHighlightMessageId(chatRoomData.messageSearchInfo._id || '');
        } else if (unreadMessages?.length) {
          setChatRoomHighlightMessageId(unreadMessages[0]._id);
        }
      }}
      hasNewMessage={`${!!unreadMessagesAmount}`}
    >
      <ChatButtonAvatarsWrapper>
        {chatRoomData.data?.members?.slice(0, 4).map((member, index) => (
          member.profile.avatar && member.profile.avatar.secureUrl ? (
            <ChatButtonAvatarWrapper
              key={member._id}
              membersAmount={chatRoomData.data.members?.length}
              index={index}
            >
              <ChatButtonAvatar
                src={member.profile.avatar.secureUrl}
              />
            </ChatButtonAvatarWrapper>
          ) : (
            <ChatButtonAvatarPlaceholder
              key={member._id}
              index={index}
              membersAmount={chatRoomData.data?.members?.length}
            >
              {getInitials(member.profile.firstName, member.profile.lastName)}
            </ChatButtonAvatarPlaceholder>
          )
        ))}
      </ChatButtonAvatarsWrapper>
      <ChatButtonInfoWrapper>
        <ChatButtonInfoHeader>
          <ChatButtonInfoTitle>
            {chatRoomData.title}
          </ChatButtonInfoTitle>
          {chatRoomData.data?.messages?.data && chatRoomData.data.messages.data.length !== 0 && (
            <Fragment>
              <ChatButtonInfoTimeDot />
              <ChatButtonInfoTime>
                {getTimeDiffFromNow(dateString!)}
              </ChatButtonInfoTime>
            </Fragment>
          )}
        </ChatButtonInfoHeader>
        <ChatButtonInfoMessage
          hasNewMessage={`${!!unreadMessagesAmount}`}
        >
          {chatRoomData.messageSearchInfo ? getChatRoomSearchMessage() : getLastChatMessage()}
        </ChatButtonInfoMessage>
      </ChatButtonInfoWrapper>
    </ChatButton>
  );
};

export default ChatRoomButton;
