import React from 'react';
import { getInitials } from '../../../../helpers';
import {
  ChatButtonAvatar,
  ChatMessageAvatarPlaceholder,
  ChatMessageAvatarWrapper,
  InvisibleProfilePreviewButton,
} from '../ChannelsChatStyles';
import { UserData } from '../../../../entities/chat/types';

type ChatAvatarProps = {
  onMessageAvatarClick: (userData: UserData['info'] | null) => void,
  chatMemberData: UserData['info'],
  currentUserData: UserData,
  isCurrentUserAvatar: boolean,
};

const renderAvatar = (user: UserData['info'], margin: string) => {
  const { avatar, lastName, firstName } = user.profile;

  return (avatar?.secureUrl ? (
    <ChatMessageAvatarWrapper
      margin={margin}
    >
      <ChatButtonAvatar
        src={avatar.secureUrl}
      />
    </ChatMessageAvatarWrapper>
  ) : (
    <ChatMessageAvatarPlaceholder
      margin={margin}
    >
      {getInitials(firstName, lastName)}
    </ChatMessageAvatarPlaceholder>
  ));
};

const ChatAvatar = ({
  onMessageAvatarClick, chatMemberData, isCurrentUserAvatar, currentUserData,
}:ChatAvatarProps) => (
  <InvisibleProfilePreviewButton
    onClick={() => {
      onMessageAvatarClick(chatMemberData || currentUserData.info);
    }}
  >
    {renderAvatar(
      chatMemberData || currentUserData.info,
      !isCurrentUserAvatar ? '0 10px 0 0' : '0 0 0 10px',
    )}
  </InvisibleProfilePreviewButton>
);

export default ChatAvatar;
