import React, { useState, useEffect, Fragment } from 'react';
import { useUnit } from 'effector-react';
import { $api } from '~/config';
import { $$chatModel, $$chatsModel } from '~/entities/chat/model';
import { ChatMessageWithMoreDataProps, UserData } from '../../../entities/chat/types';
import NewChatModal from './new-chat-modal';
import ChatRoom from './chat-room';
import ChatRoomsList, { searchSubmit } from './chat-rooms-list';
import ChatContainer from './chat-container';
import { ChatPageWrapper } from './ChannelsChatStyles';

type ChatProps = {
  fetchChatDataRequest: () => void,
  isChatLoading: boolean,
  onMessageAvatarClick: (userData: UserData['info'] | null) => void,
  activeChatRoomId?: string | null,
  chatRoomHighlightMessageId?: string | null,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  createChatRequest: (
    chatPayload: {
      title: string,
      members: string[],
    },
    setShowModal?: (showModal: boolean) => void,
  ) => void,
  updateActiveChatData: (chatId: string) => void,
  getMoreChatRoomMessagesRequest: (chatRoomId: string) => void,
  editChatRoomMessageRequest: (messageNewData: ChatMessageWithMoreDataProps) => void,
  editChatRoomDataRequest: (
    updatedChatData: {
      title?: string,
      members: string[],
      _id: string,
    },
  ) => void,
  leaveOrArchiveChatRoomRequest: (
    updatedChatId: string,
    isArchive?: boolean,
  ) => void,
  convertChatRoomRequest: (
    updatedChatRoomId: string,
  ) => void,
  searchChatRoomsRequest: (
    {
      chatSearchType,
      chatSearchInputValue,
    }: {
      chatSearchType: string,
      chatSearchInputValue: string,
    }
  ) => void,
  searchGetMoreChatMessagesRequest: (
    {
      tagId,
      messageCreatedAtDate,
    }: {
      tagId: string,
      messageCreatedAtDate: string,
    }
  ) => void,
  currentUserData: UserData,
  isStaffMode: boolean,
  width?: number,
  isChatOpen: boolean,
  tagsLoading: boolean,
  setIsChatOpen: (isChatOpen: boolean) => void,
  isChatPage?: boolean,
  chatContainerZIndex?: number,
  appSearchChatRoomData?: {
    userFirstname: string,
    userId: string,
  } | null,
  setAppSearchChatRoomData?: (
    appSearchChatRoomData: {
      userFirstname: string,
      userId: string,
    } | null,
  ) => void,
  clearQuery?: () => void,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
  onMentionClick?: (userId: string) => void,
  onContextMenu?: any;
  onHashtagClick?: (hashtagValue: string) => void,
  incomingNetworks?: any[],
  outgoingNetworks?: any[],
};

export const Chat = ({
  onMessageAvatarClick, activeChatRoomId, chatRoomHighlightMessageId, isChatLoading, createChatRequest,
  updateActiveChatData, fetchChatDataRequest, getMoreChatRoomMessagesRequest,
  currentUserData, editChatRoomMessageRequest, onContextMenu,
  isStaffMode, editChatRoomDataRequest, leaveOrArchiveChatRoomRequest, searchChatRoomsRequest,
  convertChatRoomRequest, width, isChatOpen, setIsChatOpen, isChatPage, searchGetMoreChatMessagesRequest,
  setChatRoomHighlightMessageId, chatContainerZIndex,
  appSearchChatRoomData, setAppSearchChatRoomData, clearQuery, networkConnections, onMentionClick, onHashtagClick,
  incomingNetworks, outgoingNetworks, tagsLoading,
}: ChatProps) => {
  const [showNewChatModal, setShowNewChatModal] = useState(false);
  const [isMaximize, setIsMaximize] = useState<boolean>(false);
  const [scrollPosition, setScrollPosition] = useState<number|null>(null);
  // Chat search
  const [chatSearchInputValue, setChatSearchInputValue] = useState('');
  const [isAppSearchChatOver, setIsAppSearchChatOver] = useState(false);
  const [chatSearchType, setChatSearchType] = useState<string | 'chatroom' | 'message'>('chatroom');

  const chatsModel = useUnit({
    chats: $$chatsModel.$usersChats,
    currentChats: $$chatsModel.$chats,
    searchedChats: $$chatsModel.$searchedChats,
  });

  const api = useUnit($api);

  const activeChat = useUnit($$chatModel.$activeChat);

  useEffect(() => {
    if (!tagsLoading && (isChatOpen || isChatPage) && !isChatLoading && Object.keys(chatsModel.chats).length === 0) {
      fetchChatDataRequest();
    }
  }, [isChatOpen, isChatPage, api, tagsLoading]);

  useEffect(() => {
    if (appSearchChatRoomData?.userFirstname) {
      (async () => {
        await updateActiveChatData('');
        await setChatRoomHighlightMessageId(null);
        await setChatSearchInputValue(appSearchChatRoomData.userFirstname);
        await searchSubmit({
          searchChatRoomsRequest,
          chatSearchType,
          chatSearchInputValue: appSearchChatRoomData.userFirstname,
        });
        setIsAppSearchChatOver(true);
      })();
    }
  }, [appSearchChatRoomData]);

  // query params logic
  useEffect(() => {
    if (!!activeChatRoomId && !!chatRoomHighlightMessageId) {
      setIsChatOpen(true);
    }

    if (chatsModel.chats && !(activeChat?._id === activeChatRoomId) && activeChatRoomId
      && chatRoomHighlightMessageId) {
      const updatedData = chatsModel.chats[activeChatRoomId];

      if (updatedData) {
        updateActiveChatData(updatedData._id);
      }
    }

    // message search
    if (chatRoomHighlightMessageId && chatsModel.searchedChats && chatSearchType === 'message' && activeChat
      && !activeChat.data.messages?.data
        .find((chatroomMessage) => (chatroomMessage._id === chatRoomHighlightMessageId))) {
      // eslint-disable-next-line no-restricted-syntax
      searchGetMoreChatMessagesRequest({
        tagId: activeChat._id,
        messageCreatedAtDate: Object.values(chatsModel.searchedChats)?.find((chatRoom) => (
          chatRoom.messageSearchInfo
          && chatRoom.messageSearchInfo._id === chatRoomHighlightMessageId))?.messageSearchInfo?.createdAt || '',
      });
    }

    // reply click
    if (chatRoomHighlightMessageId && activeChat && !activeChat.data.messages?.data
      .find((chatroomMessage) => (chatroomMessage._id === chatRoomHighlightMessageId)) && !isChatLoading) {
      const messageDataWithAssociatedMessageData = activeChat.data.messages?.data
        .find((chatroomMessage) => (chatroomMessage.associatedMessageData
          && chatroomMessage.associatedMessageData._id === chatRoomHighlightMessageId));
      if (messageDataWithAssociatedMessageData?.associatedMessageData?.createdAt) {
        searchGetMoreChatMessagesRequest({
          tagId: activeChat._id,
          messageCreatedAtDate: messageDataWithAssociatedMessageData.associatedMessageData.createdAt,
        });
      }
    }
  }, [activeChatRoomId, chatRoomHighlightMessageId, chatsModel.chats]);

  return (
    <Fragment>
      {isChatOpen && !isChatPage && ( // Chat floating component
        <ChatContainer
          width={width}
          onContextMenu={onContextMenu}
          isMaximize={isMaximize}
          setIsMaximize={setIsMaximize}
          chatContainerZIndex={chatContainerZIndex}
        >
          {!activeChat && ( // All Chat Rooms list
            <ChatRoomsList
              setShowNewChatModal={setShowNewChatModal}
              setIsChatOpen={setIsChatOpen}
              isChatLoading={isChatLoading}
              updateActiveChatData={updateActiveChatData}
              isMaximize={isMaximize}
              setIsMaximize={setIsMaximize}
              chatSearchInputValue={chatSearchInputValue}
              setChatSearchInputValue={setChatSearchInputValue}
              chatSearchType={chatSearchType}
              setChatSearchType={setChatSearchType}
              chatRooms={chatSearchInputValue ? chatsModel.searchedChats : chatsModel.chats}
              searchChatRoomsRequest={searchChatRoomsRequest}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              currentUserData={currentUserData}
              appSearchChatRoomData={appSearchChatRoomData}
              setAppSearchChatRoomData={setAppSearchChatRoomData}
              createChatRequest={createChatRequest}
              isAppSearchChatOver={isAppSearchChatOver}
              setIsAppSearchChatOver={setIsAppSearchChatOver}
              scrollPosition={scrollPosition}
              setScrollPosition={setScrollPosition}
            />
          )}
          {activeChat && ( // Active Chat Room info
            <ChatRoom
              isChatLoading={isChatLoading}
              onMessageAvatarClick={onMessageAvatarClick}
              setIsChatOpen={setIsChatOpen}
              chatRoomHighlightMessageId={chatRoomHighlightMessageId}
              getMoreChatRoomMessagesRequest={getMoreChatRoomMessagesRequest}
              currentUserData={currentUserData}
              editChatRoomMessageRequest={editChatRoomMessageRequest}
              updateActiveChatData={updateActiveChatData}
              isStaffMode={isStaffMode}
              editChatRoomDataRequest={editChatRoomDataRequest}
              leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
              convertChatRoomRequest={convertChatRoomRequest}
              isMaximize={isMaximize}
              setIsMaximize={setIsMaximize}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              clearQuery={clearQuery}
              networkConnections={networkConnections}
              onMentionClick={onMentionClick}
              onHashtagClick={onHashtagClick}
              incomingNetworks={incomingNetworks}
              outgoingNetworks={outgoingNetworks}
            />
          )}
          {showNewChatModal && (
            <NewChatModal
              title="Start a chat"
              showModal={showNewChatModal}
              setShowModal={setShowNewChatModal}
              request={createChatRequest}
              currentUserFirstName={currentUserData.info.profile.firstName!}
              networkConnections={networkConnections}
              isLoading={isChatLoading}
            />
          )}
        </ChatContainer>
      )}
      {isChatPage && ( // Chat page
        <ChatPageWrapper>
          {!activeChat && ( // All Chat Rooms list
            <ChatRoomsList
              setShowNewChatModal={setShowNewChatModal}
              setIsChatOpen={setIsChatOpen}
              isChatLoading={isChatLoading}
              updateActiveChatData={updateActiveChatData}
              isChatPage={isChatPage}
              chatSearchInputValue={chatSearchInputValue}
              setChatSearchInputValue={setChatSearchInputValue}
              chatSearchType={chatSearchType}
              setChatSearchType={setChatSearchType}
              chatRooms={chatSearchInputValue ? chatsModel.searchedChats : chatsModel.chats}
              searchChatRoomsRequest={searchChatRoomsRequest}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              currentUserData={currentUserData}
              appSearchChatRoomData={appSearchChatRoomData}
              setAppSearchChatRoomData={setAppSearchChatRoomData}
              createChatRequest={createChatRequest}
              isAppSearchChatOver={isAppSearchChatOver}
              setIsAppSearchChatOver={setIsAppSearchChatOver}
              scrollPosition={scrollPosition}
              setScrollPosition={setScrollPosition}
            />
          )}
          {activeChat && ( // Active Chat Room info
            <ChatRoom
              isChatLoading={isChatLoading}
              onMessageAvatarClick={onMessageAvatarClick}
              setIsChatOpen={setIsChatOpen}
              chatRoomHighlightMessageId={chatRoomHighlightMessageId}
              getMoreChatRoomMessagesRequest={getMoreChatRoomMessagesRequest}
              currentUserData={currentUserData}
              editChatRoomMessageRequest={editChatRoomMessageRequest}
              updateActiveChatData={updateActiveChatData}
              isStaffMode={isStaffMode}
              editChatRoomDataRequest={editChatRoomDataRequest}
              leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
              convertChatRoomRequest={convertChatRoomRequest}
              isChatPage={isChatPage}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              clearQuery={clearQuery}
              networkConnections={networkConnections}
              onMentionClick={onMentionClick}
              onHashtagClick={onHashtagClick}
              incomingNetworks={incomingNetworks}
              outgoingNetworks={outgoingNetworks}
            />
          )}
          {showNewChatModal && (
            <NewChatModal
              title="Start a chat"
              showModal={showNewChatModal}
              setShowModal={setShowNewChatModal}
              request={createChatRequest}
              currentUserFirstName={currentUserData.info.profile.firstName!}
              networkConnections={networkConnections}
              isLoading={isChatLoading}
            />
          )}
        </ChatPageWrapper>
      )}
    </Fragment>
  );
};
