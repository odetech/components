import styled from '@emotion/styled';
import { Rnd } from 'react-rnd';

type ChatWrapperProps = {
  chatContainerZIndex?: number;
};

export const ChatWrapper = styled.div<ChatWrapperProps>`
  position: fixed;
  top: 0;
  bottom: 0;
  z-index: ${({ chatContainerZIndex }) => chatContainerZIndex || 3};
`;

export const ChatContainerGhost = styled('div')`
  pointer-events: none;
  position: fixed;
  top: 80px;
  left: 80px;
  bottom: 15px;
  right: 15px;
`;

export const ChannelsChatWrapper = styled(Rnd)`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  background: transparent;
`;
