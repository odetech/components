import React, { ReactNode, useEffect, useRef } from 'react';
import { ChatWrapper, ChannelsChatWrapper, ChatContainerGhost } from './ChatContainerStyles';

type ChatProps = {
  children: ReactNode,
  width?: number,
  isMaximize: boolean,
  onContextMenu: any;
  setIsMaximize: (isMaximize: boolean) => void,
  chatContainerZIndex?: number,
};

const ChatContainer: React.FC<ChatProps> = ({
  children, width,
  isMaximize, setIsMaximize, chatContainerZIndex, onContextMenu,
}) => {
  const rnd: any = useRef(null);

  const defaultWidth = document.body.clientWidth - 400 - 15;
  const defaultHeight = window.innerHeight - 550 - 15;

  useEffect(() => {
    if (rnd?.current && isMaximize) {
      rnd.current.updateSize({
        width: document.body.clientWidth - 80 - 15,
        height: window.innerHeight - 80 - 15,
      });
      rnd.current.updatePosition({
        x: 80,
        y: 80,
      });
    } else if (rnd && rnd.current && !isMaximize) {
      rnd.current.updateSize({
        width: 400,
        height: 550,
      });
      rnd.current.updatePosition({
        x: document.body.clientWidth - 400 - 15,
        y: window.innerHeight - 550 - 15,
      });
    }
  }, [isMaximize]);

  useEffect(() => {
    if (rnd && rnd.current && !isMaximize) {
      rnd.current.updatePosition({
        x: document.body.clientWidth - 400 - 15,
        y: window.innerHeight - 550 - 15,
      });
    }
  }, [width]);

  return (
    <ChatWrapper chatContainerZIndex={chatContainerZIndex} onContextMenu={onContextMenu}>
      <ChatContainerGhost className="chat-container" />
      <ChannelsChatWrapper
        ref={rnd}
        default={{
          x: defaultWidth,
          y: defaultHeight,
          width: (width && width > 768) ? 400 : 320,
          height: 550,
        }}
        minWidth={(width && width > 768) ? 400 : 320}
        minHeight={500}
        bounds=".chat-container"
        dragHandleClassName="chat-header"
        resizeHandleStyles={{
          right: {
            width: 5,
          },
        }}
        onResize={() => {
          if (isMaximize) {
            setIsMaximize(false);
          }
        }}
      >
        {children}
      </ChannelsChatWrapper>
    </ChatWrapper>
  );
};

export default ChatContainer;
