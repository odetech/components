import styled from '@emotion/styled';
import { transparentize } from 'polished';
import { Form } from 'formik';
import isPropValid from '@emotion/is-prop-valid';
import { getVerticalScrollbarStyles } from '../../../../styles';
import { defaultTheme } from '../../../../styles/themes';
import { Button } from '../../../../components/default-components';

type ChatRoomSettingsWrapperProps = {
  isOpen: boolean,
};

export const ChatRoomSettingsWrapper = styled('div', {
  shouldForwardProp: isPropValid,
})<ChatRoomSettingsWrapperProps>`
  width: 100%;
  height: 100%;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};  
  z-index: ${({ isOpen }) => (isOpen ? '8' : '-1')};
  position: absolute;
  top: 0;
  border-radius: 0 0 10px 10px;
  transition: 0.6s;
`;

export const ChatRoomSettingsCardWrapper = styled('div')`
  width: 100%;
  height: 100%;
  position: relative;
  overflow-x: hidden;

  ${({ theme }) => getVerticalScrollbarStyles(theme)} 
`;

type ChatRoomSettingsCardProps = {
  isOpen: boolean,
};

export const ChatRoomSettingsCard = styled('div', {
  shouldForwardProp: isPropValid,
})<ChatRoomSettingsCardProps>`
  width: calc(100% - 35px);
  min-height: 100%;
  padding: 25px 30px;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  position: absolute;
  right: ${({ isOpen }) => (isOpen ? '0' : '-365px')};
  transition: 0.5s;  
  ${({ theme }) => getVerticalScrollbarStyles(theme)} 
`;

export const ChatRoomSettingsForm = styled(Form)`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const ChatRoomSettingsSaveButton = styled(Button)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};  
  background: ${({ theme }) => transparentize(0.8, theme?.brand?.primary || defaultTheme.brand.primary)};  
  color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};        
  margin: 0 0 30px 0;

  svg {
    margin: 0 10px 0 0;
    fill: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  }

  &:disabled {
    border-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
    opacity: 0.5;    
  }

  &:focus {
    background: ${({ theme }) => transparentize(0.8, theme?.brand?.primary || defaultTheme.brand.primary)}; 
  }

  &:hover {
    background: ${({ theme }) => transparentize(0.8, theme?.brand?.surfaceContainerVar || defaultTheme.brand.primary)}; 
  }

  &:active {
    background: ${({ theme }) => transparentize(0.8, theme?.brand?.surfaceContainerVar || defaultTheme.brand.primary)}; 
  }
`;

type ChatRoomSettingsButtonsWrapperProps = {
  isCreatedByThisUser?: boolean,
};

export const ChatRoomSettingsButtonsWrapper = styled('div')<ChatRoomSettingsButtonsWrapperProps>`
  margin: ${({ isCreatedByThisUser }) => (isCreatedByThisUser ? 'auto 0 8px 0' : 'auto 0 0 0')};
  display: flex;
  align-items: center;
  justify-content: space-between;

  button {
    font-weight: 500;
    font-size: 16px;
    line-height: 24px;
    padding: 10px 15px;
    width: 100%;
    white-space: nowrap;

    svg {
      width: 20px;
      height: 20px;
      margin: 0 5px 0 0;
    }

    &:not(:last-child) {
      margin: 0 20px 0 0;
    }
  }
`;

export const ChatRoomSettingsLeaveButton = styled(Button)`
  background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};   
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};

  svg {
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  }

  &:focus {
    background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};   
  }
  
  &:hover {
    background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};   
  }

  &:active {
    background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};   
  }
`;

export const ChatRoomSettingsDeleteButton = styled(Button)`
  background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};   
  color: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};

  svg {
    fill: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};
  }

  &:focus {
    background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};   
  }

  &:hover {
    background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};
  }

  &:active {
    background: ${({ theme }) => theme?.error?.primaryContainer || defaultTheme.error.primaryContainer};
  }
`;

export const ChatRoomSettingsMembersListWrapper = styled('div')`
  margin: 0 0 30px 0;
  display: flex;
  flex-direction: column;
`;

export const ChatRoomSettingsMembersLabel = styled('p')`
  font-weight: 500;
  font-size: 12px;
  line-height: 18px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};    
  margin: 0 0 7px 0;
`;

type ChatRoomSettingsMembersListProps = {
  isEmpty: boolean,
};

export const ChatRoomSettingsMembersList = styled('div')<ChatRoomSettingsMembersListProps>`
  width: 100%;
  min-height: 42px;
  max-height: 198px;
  overflow: auto;
  border: 1px dashed ${({ theme }) => theme?.outline?.[300] || defaultTheme.outline[300]};
  box-sizing: border-box;
  border-radius: 6px;

  ${({ theme }) => getVerticalScrollbarStyles(theme)}

  ${({ isEmpty }) => isEmpty && `
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `}
`;

export const ChatRoomSettingsCloseButton = styled(Button)`
  margin: 0 0 0 auto;
  padding: 4px;

  svg {
    width: 20px;
    height: 20px;
    transform: rotate(45deg);
  }
`;

export const ChatRoomSettingsAlertCardWrapper = styled('div')`
  width: 100%;
  height: 100%;
  background: ${({ theme }) => transparentize(0.8, theme?.background?.surface || defaultTheme.background.surface)};
  position: relative;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 0 0 10px 10px;
`;

export const ChatRoomSettingsAlertCard = styled('div')`
  width: 328px;
  height: 258px;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  padding: 30px 20px 25px 20px;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
  border-radius: 6px;
`;

export const ChatRoomSettingsAlertCardTitle = styled('p')`
  text-align: center;
  font-weight: 500;
  font-size: 20px;
  line-height: 30px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  margin: 0 0 5px 0;
`;

export const ChatRoomSettingsAlertCardDescription = styled('p')`
  text-align: center;
  font-size: 15px;
  line-height: 22px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  margin: 0 0 30px 0;
`;

export const ChatRoomSettingsAlertCardButtonsWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  
  button {
    &:not(:last-child) {
      margin: 0 20px 0 0;
    }
  }
`;

export const ChatRoomSettingsFlexWrapper = styled('div')`
  display: flex;
`;
