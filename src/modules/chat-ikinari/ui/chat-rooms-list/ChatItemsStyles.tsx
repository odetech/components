import styled from '@emotion/styled';
import { defaultTheme } from '../../../../styles/themes';

export const HeaderLeftSideWrapper = styled('div')`
  display: flex;
  align-items: center;
  
  span {
    margin: 0 5px 0 0;
  }

  button {
    padding: 5px;
    color: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
    z-index: 1;

    svg {
      fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
      width: 24px;
      height: 24px;
    }

    &:focus {
      color: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
      background: transparent;

      svg {
        fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
      }
    }

    &:hover, &:active {
      color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
      background: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};

      svg {
        fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
      }
    }

    &:not(:last-of-type) {
      margin: 0 5px 0 0;
    }
  }
`;

export const HeaderButtonsWrapper = styled('div')`
  display: flex;
  align-items: center;

  button {
    padding: 5px;
    color: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};

    svg {
      fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
      width: 24px;
      height: 24px;
    }

    &:hover, &:active, &:focus {
      color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
      background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};

      svg {
        fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
      }
    }

    &:not(:last-of-type) {
      margin: 0 5px 0 0;
    }
  }
`;
