import React, { Fragment, useEffect, useRef } from 'react';
import { Virtuoso } from 'react-virtuoso';
import { BeatLoader } from 'react-spinners';
import { useUnit } from 'effector-react';
import { isToday } from 'date-fns';
import { $$chatsModel } from '~/entities/chat/model';
import { $currentUser } from '~/config';
import { useTheme } from '@storybook/theming';
import { ensureUtcTimestamp } from '~/helpers';
import { defaultTheme } from '../../../../styles/themes';
import {
  HeaderButtonsWrapper,
  HeaderLeftSideWrapper,
} from './ChatItemsStyles';
import {
  ChatHeader,
  ChatBody,
  MinimizeButton,
  ChatItemsWrapper,
  CenterItemWrapper,
  ChatHeaderDraggable,
  HeaderActionButton,
  ChatSearchInputWrapper,
  ChatSearchItemsWrapper,
  ChatSearchForButton, ChatSearchInputClearButton, ChatSearchInputSubmitButton,
} from '../ChannelsChatStyles';
import {
  CloseIcon,
  MagnifyIcon,
  PlusBoxOutlineIcon, WindowMaximizeIcon, WindowMinimizeIcon, WindowRestoreIcon,
} from '../../icons';
import ChatRoomButton from '../chat-room-button';
import { Input, Button } from '../../../../components/default-components';
import { ChatDataProps, UserData } from '../../../../entities/chat/types';

const virtuosoItemContent = (
  index: number,
  chatRooms: ChatDataProps[],
  openChatRoom: (chatRoomId: string) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
) => (
  <ChatRoomButton
    key={`${chatRooms[index].messageSearchInfo?._id || chatRooms[index]?._id}-${index}`}
    chatRoomData={chatRooms[index]}
    updateActiveChatData={openChatRoom}
    setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
  />
);

const getInfoChannel = (channel: ChatDataProps, userId: string) => {
  const lengthMessages = channel.data?.messages?.data?.length || 0;
  const lastMessage = channel.data?.messages?.data?.[lengthMessages - 1];
  let createdAt: string | Date = '';
  if (channel?.createdAt) {
    createdAt = ensureUtcTimestamp(channel.createdAt);
    createdAt = createdAt.replace('+00:00', '');
  } else {
    createdAt = new Date();
  }
  return {
    createdAt,
    lengthMessages,
    isUnread: Boolean(userId && lastMessage && !lastMessage?.views?.includes(userId)),
    lastMessageCreatedAt: (lastMessage?.createdAt && ensureUtcTimestamp(lastMessage.createdAt).replace('+00:00', '')) || createdAt,
  };
};

export const searchSubmit = ({
  searchChatRoomsRequest,
  chatSearchType,
  chatSearchInputValue,
}: {
  // eslint-disable-next-line no-shadow
  searchChatRoomsRequest: ({ chatSearchType, chatSearchInputValue }: {
    chatSearchType: string,
    chatSearchInputValue: string,
  }
  ) => void,
  chatSearchType: string,
  chatSearchInputValue: string,
}) => {
  searchChatRoomsRequest({
    chatSearchType,
    chatSearchInputValue,
  });
};

type ChannelsChatProps = {
  isChatLoading: boolean,
  setIsChatOpen: (b: boolean) => void,
  setShowNewChatModal: (b: boolean) => void,
  updateActiveChatData: (chatId: string) => void,
  isChatPage?: boolean,
  isMaximize?: boolean,
  setIsMaximize?: (isMaximize: boolean) => void,
  chatSearchInputValue: string,
  setChatSearchInputValue: (chatSearchInputValue: string) => void,
  chatSearchType: string,
  setChatSearchType: (chatSearchType: string) => void,
  chatRooms: Record<string, ChatDataProps> | ChatDataProps[],
  searchChatRoomsRequest: (
    {
      chatSearchType,
      chatSearchInputValue,
    }: {
      chatSearchType: string,
      chatSearchInputValue: string,
    }
  ) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  currentUserData: UserData,
  appSearchChatRoomData?: {
    userFirstname: string,
    userId: string,
  } | null,
  setAppSearchChatRoomData?: (
    appSearchChatRoomData: {
      userFirstname: string,
      userId: string,
    } | null,
  ) => void,
  createChatRequest: (
    chatPayload: {
      title: string,
      members: string[],
    },
    setShowModal?: (showModal: boolean) => void,
  ) => void,
  isAppSearchChatOver: boolean,
  setIsAppSearchChatOver: (isAppSearchChatOver: boolean) => void,
  scrollPosition: number | null;
  setScrollPosition: (value: number | null) => void;
};

const ChatRoomsList = ({
  setIsChatOpen, setShowNewChatModal, isChatLoading, updateActiveChatData,
  isChatPage, isMaximize, setIsMaximize, chatSearchInputValue, setChatSearchInputValue, chatSearchType,
  setChatSearchType, chatRooms, searchChatRoomsRequest, setChatRoomHighlightMessageId,
  currentUserData, appSearchChatRoomData, setAppSearchChatRoomData, createChatRequest, isAppSearchChatOver,
  setIsAppSearchChatOver, scrollPosition, setScrollPosition,
}: ChannelsChatProps) => {
  const theme = useTheme();

  const chatsModel = useUnit({
    requested: $$chatsModel.requested,
    searchedChats: $$chatsModel.$searchedChats,
    hasNextPage: $$chatsModel.$hasNextPage,
    page: $$chatsModel.$page,
  });
  const currentUser = useUnit($currentUser);
  const roomsArray = Object.values(chatRooms).sort((a, b) => {
    const aInfo = getInfoChannel(a, currentUser?._id);
    const bInfo = getInfoChannel(b, currentUser?._id);

    // @ts-ignore
    if (isToday(new Date(aInfo.createdAt)) && !aInfo.lengthMessages) return -1;
    // @ts-ignore
    if (isToday(new Date(bInfo.createdAt)) && !bInfo.lengthMessages) return -1;

    if (aInfo.isUnread && !bInfo.isUnread) return -1;
    if (bInfo.isUnread && !aInfo.isUnread) return 1;

    return new Date(bInfo.lastMessageCreatedAt).getTime() - new Date(aInfo.lastMessageCreatedAt).getTime();
  });
  const virtuoso = useRef(null);

  const scrollToFirstChatRoom = () => {
    if (virtuoso && virtuoso.current) {
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: 0,
        align: 'end',
        behavior: 'auto',
      });
    }
  };

  useEffect(() => {
    scrollToFirstChatRoom();
  }, [chatsModel.searchedChats]);

  useEffect(() => {
    if (virtuoso && virtuoso.current && scrollPosition !== null) {
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: scrollPosition,
        align: 'end',
        behavior: 'auto',
      });
      setScrollPosition(null);
    }
  }, [virtuoso.current]);

  const openChatRoom = (chatRoomId: string) => {
    updateActiveChatData(chatRoomId);
    const chatIndex = roomsArray.findIndex((item: ChatDataProps) => item._id === chatRoomId);
    setScrollPosition(chatIndex);
  };

  useEffect(() => {
    // Create chat room if appSearchChatRoomData exists and searchChatRooms is empty
    if (chatsModel.searchedChats && chatsModel.searchedChats.length === 0
      && !isChatLoading && appSearchChatRoomData?.userFirstname && chatSearchInputValue
      && chatSearchInputValue === appSearchChatRoomData.userFirstname
      && currentUserData?.info?.profile?.firstName && setAppSearchChatRoomData && isAppSearchChatOver) {
      (async () => {
        // Add template message?
        await createChatRequest({
          title: `${currentUserData.info.profile.firstName}, ${appSearchChatRoomData.userFirstname}`,
          members: [appSearchChatRoomData.userId],
        });

        setIsAppSearchChatOver(false);

        setTimeout(() => {
          searchSubmit({
            searchChatRoomsRequest,
            chatSearchType,
            chatSearchInputValue,
          });
        }, 1000);
      })();
    }
  }, [chatsModel.searchedChats, isChatLoading, appSearchChatRoomData,
    chatSearchInputValue, appSearchChatRoomData, currentUserData, setAppSearchChatRoomData, isAppSearchChatOver]);

  useEffect(() => {
    const keys = Object.keys(chatRooms);
    // open chat room if only one chatroom found via appSearchChatRoomData
    if (!isChatLoading && appSearchChatRoomData?.userFirstname && chatSearchInputValue
      && chatSearchInputValue === appSearchChatRoomData.userFirstname
      && setAppSearchChatRoomData) {
      if (keys.length === 1) {
        updateActiveChatData(keys[0]);
      }
      // Removing appSearchChatRoomData if search via appSearchChatRoomData is successful
      if (keys.length !== 0) {
        setAppSearchChatRoomData(null);
      }
    }
  }, [chatsModel.searchedChats]);

  const onEnd = () => {
    const fetchNext = () => {
      chatsModel.requested(
        {
          userId: currentUserData?.id,
          limit: 15,
          pageNumber: chatsModel.page + 1,
          messagesPageNumber: 1,
          messagesLimit: 15,
        },
      );
    };
    // eslint-disable-next-line max-len
    return (!chatsModel.hasNextPage || isChatLoading || Object.values(chatsModel.searchedChats).length ? () => ({}) : fetchNext);
  };

  return (
    <ChatItemsWrapper>
      <ChatHeader>
        <ChatHeaderDraggable className="chat-header" />
        <HeaderLeftSideWrapper>
          {!isChatPage && (
            <span>
              Direct Chats
            </span>
          )}
          <Button
            view="text"
            onClick={() => {
              setShowNewChatModal(true);
            }}
          >
            <PlusBoxOutlineIcon />
          </Button>
        </HeaderLeftSideWrapper>
        <HeaderButtonsWrapper>
          {!isChatPage && (
            <Fragment>
              <MinimizeButton
                view="text"
                onClick={() => {
                  setIsChatOpen(false);
                }}
              >
                <WindowMinimizeIcon />
              </MinimizeButton>
              {setIsMaximize && (
                <HeaderActionButton
                  view="text"
                  onClick={() => {
                    setIsMaximize(!isMaximize);
                  }}
                >
                  {isMaximize ? <WindowRestoreIcon /> : <WindowMaximizeIcon />}
                </HeaderActionButton>
              )}
            </Fragment>
          )}
        </HeaderButtonsWrapper>
      </ChatHeader>
      <ChatSearchItemsWrapper>
        <ChatSearchInputWrapper
          isInputEmpty={chatSearchInputValue === ''}
          onSubmit={(e) => {
            e.preventDefault();

            searchSubmit({
              searchChatRoomsRequest,
              chatSearchType,
              chatSearchInputValue,
            });
          }}
        >
          <Input
            name="chatSearchInput"
            placeholder="Search"
            value={chatSearchInputValue}
            onChange={(e) => {
              setChatSearchInputValue(e.target.value);
            }}
          />
          <ChatSearchInputClearButton
            onClick={() => {
              setChatSearchInputValue('');
              searchChatRoomsRequest({
                chatSearchType,
                chatSearchInputValue: '',
              });
              scrollToFirstChatRoom();
            }}
          >
            <CloseIcon />
          </ChatSearchInputClearButton>
          <ChatSearchInputSubmitButton
            view="text"
            type="submit"
            disabled={isChatLoading}
          >
            {isChatLoading
              ? (
                <BeatLoader
                  size={5}
                  color={theme?.brand?.secondary || defaultTheme.brand.secondary}
                />
              )
              : <MagnifyIcon />}
          </ChatSearchInputSubmitButton>
        </ChatSearchInputWrapper>
        {chatSearchInputValue !== '' && (
          <ChatSearchForButton
            view="text"
            onClick={() => {
              setChatSearchType(chatSearchType === 'chatroom' ? 'message' : 'chatroom');
            }}
          >
            {chatSearchType === 'chatroom' ? 'Search for messages' : 'Search for chat room'}
          </ChatSearchForButton>
        )}
      </ChatSearchItemsWrapper>
      <ChatBody>
        {Object.keys(chatRooms).length !== 0 && (
          <Virtuoso
            ref={virtuoso}
            style={{
              overscrollBehavior: 'contain',
            }}
            computeItemKey={(_, data) => `chat-room-${data.messageSearchInfo?._id || data._id}`}
            id="virtuoso-scroller"
            data={roomsArray}
            endReached={onEnd()}
            itemContent={(index) => (
              virtuosoItemContent(index, roomsArray, openChatRoom, setChatRoomHighlightMessageId))}
            components={{
              // eslint-disable-next-line react/no-unstable-nested-components
              Footer: chatsModel.hasNextPage && isChatLoading ? () => (
                <div
                  style={{
                    padding: '10px 4px 4px 4px',
                    display: 'flex',
                    justifyContent: 'center',
                  }}
                >
                  <BeatLoader color={theme?.brand?.primary || defaultTheme.brand.primary} />
                </div>
              ) : undefined,
            }}
          />
        )}
        {Object.keys(chatRooms).length === 0 && (
          <CenterItemWrapper>
            {isChatLoading
              ? <BeatLoader color={theme?.brand?.primary || defaultTheme.brand.primary} />
              : (<span>No chats found yet</span>)}
          </CenterItemWrapper>
        )}
      </ChatBody>
    </ChatItemsWrapper>
  );
};

export default ChatRoomsList;
