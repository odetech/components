import React, {
  useEffect, useRef, useState,
} from 'react';
import { BeatLoader } from 'react-spinners';
import { Virtuoso } from 'react-virtuoso';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import { useUnit } from 'effector-react';
import { DeleteModal } from '~/components/default-components';
import { $$messagesModel } from '~/entities/chat/model/messages';
import { $$networksModel } from '~/entities/networks';
import { useTheme } from '@storybook/theming';
import { $$chatModel } from '~/entities/chat/model';
import { ensureUtcTimestamp } from '~/helpers';
import { defaultTheme } from '../../../../styles/themes';
import ChatRoomHeader from '../chat-room-header';
import ChatMessageForm from '../chat-message-form';
import ChatRoomSettings from '../chat-room-settings';
import {
  ChatBody,
  CenterItemWrapper,
  MessagesWrapper,
  ChatItemsWrapper,
  ScrollToBottomButtonWrapper,
  ScrollToBottomButton,
  DeclineNetworkChatRoomButton,
  AcceptChatRoomButton,
  NetworkChatRoomItemsWrapper,
  NetworkChatRoomTextInfo,
  NetworkChatRoomTextUserInfo,
  NetworkChatRoomButtonsWrapper,
  ChatRoomHR,
  ArchiveChatRoomButton,
} from '../ChannelsChatStyles';
import {
  ChatMessageWithMoreDataProps,
  UserData,
} from '../../../../entities/chat/types';
import ChatRoomMessage from '../chat-room-message';
import { ArrowDownIcon } from '../../icons';
import {
  ChatRoomSettingsAlertCardDescription,
  ChatRoomSettingsAlertCardTitle,
} from '../chat-room-settings/ChatRoomSettingsStyles';

export const getStartDateTime = (date:string): number => new Date(new Date(date).setHours(0, 0, 0, 0)).getTime();

type ChatRoomProps = {
  isChatLoading: boolean,
  onMessageAvatarClick: (userData: any | null) => void,
  setIsChatOpen: (isChatOpen: boolean) => void,
  chatRoomHighlightMessageId?: string | null,
  getMoreChatRoomMessagesRequest: (chatRoomId: string) => void,
  editChatRoomMessageRequest: (messageNewData: ChatMessageWithMoreDataProps) => void,
  updateActiveChatData: (chatId: string) => void,
  editChatRoomDataRequest: (
    updatedChatData: {
      title?: string,
      members: string[],
      _id: string,
    },
  ) => void,
  currentUserData: UserData,
  isStaffMode: boolean,
  leaveOrArchiveChatRoomRequest: (
    updatedChatId: string,
    isArchive?: boolean,
  ) => void,
  convertChatRoomRequest: (
    updatedChatRoomId: string,
  ) => void,
  isChatPage?: boolean,
  isMaximize?: boolean,
  setIsMaximize?: (isMaximize: boolean) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  clearQuery?: () => void,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
  incomingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
  outgoingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
};

const ChatRoom = ({
  onMessageAvatarClick, chatRoomHighlightMessageId, setIsChatOpen, isChatLoading,
  getMoreChatRoomMessagesRequest, currentUserData, editChatRoomMessageRequest,
  updateActiveChatData, isStaffMode, editChatRoomDataRequest, leaveOrArchiveChatRoomRequest,
  convertChatRoomRequest, isChatPage, isMaximize, setIsMaximize,
  setChatRoomHighlightMessageId, clearQuery, networkConnections, onMentionClick, onHashtagClick,
  incomingNetworks, outgoingNetworks,
}: ChatRoomProps) => {
  const theme = useTheme();
  const virtuoso = useRef(null);
  const messagesRead = useUnit($$messagesModel.read);
  const activeChat = useUnit($$chatModel.$activeChat);
  const unreadMessages = useUnit($$messagesModel.$unreadMessages);
  const accepted = useUnit($$networksModel.networkAccepted);
  const fetchNetwork = useUnit($$networksModel.fetchNetworkByUserId);
  const pendingNetwork = useUnit($$networksModel.$pendingNetworkByUserId);
  const [isRequestRun, setIsRequestRun] = useState(false);
  const messagesModel = useUnit({
    deleted: $$messagesModel.deleted,
  });
  const messages = activeChat?.data.messages?.data || [];

  const [isEdit, setEdit] = useState<any>(null);
  const [replyData, setReply] = useState<any>(null);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [deleteMessage, setDeleteMessage] = useState<null | ChatMessageWithMoreDataProps>(null);
  const [images, setImages] = useState<string[]>([]);
  const [isOpenLightbox, setIsOpenLightbox] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [showModal, setShowModal] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pdfFile, setPdfFile] = useState<string | null>(null);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [isChatRoomSettingsOpen, setIsChatRoomSettingsOpen] = useState(false);
  const [isAtStart, setIsAtStart] = useState(true);
  const [isIncomingNetwork, setIsIncomingNetwork] = useState(false);
  const [isOutgoingNetwork, setIsOutgoingNetwork] = useState(false);
  const [isArchiveChatAlertOpen, setIsArchiveChatAlertOpen] = useState(false);
  // TODO: come up with another solution for scrolling?
  const [shouldScrollToTheLatestUpdatedData, setShouldScrollToTheLatestUpdatedData] = useState(false);
  // const [startIndex, setStartIndex] = useState(0);

  const rangeChanged = async (range: any) => {
    if (range.startIndex === 0 && !isChatLoading && chatRoomHighlightMessageId === null) {
      if (activeChat && activeChat.data.messages?.hasNextPage && !isChatLoading) {
        await getMoreChatRoomMessagesRequest(activeChat._id);
        setShouldScrollToTheLatestUpdatedData(true);
      }
    }
  };

  useEffect(() => {
    setTimeout(() => {
      if (chatRoomHighlightMessageId && virtuoso && virtuoso.current) {
        let chatRoomHighlightMessageIdIndex;
        if (chatRoomHighlightMessageId) {
          chatRoomHighlightMessageIdIndex = activeChat?.data.messages?.data
            .findIndex((article) => article._id === chatRoomHighlightMessageId);
        }
        if (chatRoomHighlightMessageIdIndex !== -1) {
          setTimeout(() => {
            setChatRoomHighlightMessageId(null);
          }, 4000);
        }
        setShouldScrollToTheLatestUpdatedData(true);
        // @ts-ignore
        virtuoso.current.scrollToIndex({
          index: chatRoomHighlightMessageIdIndex || 15,
          align: 'end',
          behavior: 'auto',
        });
      }
    }, 100);
  }, [chatRoomHighlightMessageId]);

  const scrollToFirstMessage = () => {
    if (virtuoso && virtuoso.current && activeChat) {
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: messages.length ? (messages.length) : 0,
        align: 'end',
        behavior: 'auto',
      });
    }
  };

  useEffect(() => {
    // @ts-ignore
    // eslint-disable-next-line max-len
    if (currentUserData.id && unreadMessages[activeChat?._id || '']?.length) {
      messagesRead({
        currentUserId: currentUserData.id,
        messagesIds: unreadMessages[activeChat?._id || ''].map((t: any) => t._id),
      });
    }
    // TODO: come up with another solution for scrolling?
    if (virtuoso && virtuoso.current && messages.length && shouldScrollToTheLatestUpdatedData) {
      let chatRoomHighlightMessageIdIndex;
      if (chatRoomHighlightMessageId) {
        chatRoomHighlightMessageIdIndex = activeChat?.data.messages?.data
          .findIndex((article) => article._id === chatRoomHighlightMessageId);
      }
      if (chatRoomHighlightMessageIdIndex !== -1) {
        setTimeout(() => {
          setChatRoomHighlightMessageId(null);
        }, 4000);
      } else if (chatRoomHighlightMessageId && activeChat) {
        getMoreChatRoomMessagesRequest(activeChat?._id);
        setShouldScrollToTheLatestUpdatedData(true);
      }
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: chatRoomHighlightMessageIdIndex || 15,
        align: 'start',
        behavior: 'auto',
      });
      if (!chatRoomHighlightMessageId && activeChat) setShouldScrollToTheLatestUpdatedData(false);
    }
  }, [activeChat?.data.messages?.data, shouldScrollToTheLatestUpdatedData]);

  useEffect(() => {
    if (!pendingNetwork) {
      if (!isIncomingNetwork) {
        const isOneMemberChatRoomUserConnected = !!(activeChat?.data.members
          && activeChat.data.members.length === 1 && networkConnections
          && networkConnections.networks.find((networkUserData) => (
            activeChat?.data.members && networkUserData._id === activeChat.data.members[0]._id
          )));

        const isIncomingNetworkCheck = !isOneMemberChatRoomUserConnected && !!(incomingNetworks
          && incomingNetworks.find((incomingNetwork) => (
            activeChat?.data.members && incomingNetwork.userId1 === activeChat.data.members[0]._id
          )));
        setIsIncomingNetwork(isIncomingNetworkCheck);

        const isOutgoingNetworkCheck = !isOneMemberChatRoomUserConnected && !!(outgoingNetworks
          && outgoingNetworks.find((outgoingNetwork) => (
            activeChat?.data.members && outgoingNetwork.userId2 === activeChat.data.members[0]._id
          )));
        setIsOutgoingNetwork(isOutgoingNetworkCheck);

        // Archive chat if there are no incoming request and there are only 2 members in the chat room
        if (activeChat?.data.members && activeChat.data.members.length === 1
          && !isOneMemberChatRoomUserConnected && !isIncomingNetworkCheck && !isOutgoingNetworkCheck) {
          if (isRequestRun) {
            setIsArchiveChatAlertOpen(true);
          } else {
            setIsRequestRun(true);
            fetchNetwork(activeChat.data.members[0]._id);
          }
        }
      }
    }
  }, [
    pendingNetwork,
    activeChat?.data.members, incomingNetworks, isIncomingNetwork, networkConnections, outgoingNetworks]);

  useEffect(() => {
    if ((activeChat?.data.members
      && activeChat.data.members.length === 1 && networkConnections
      && networkConnections.networks.find((networkUserData) => (
        activeChat?.data.members && networkUserData._id === activeChat.data.members[0]._id
      ))) || (activeChat?.data.members && activeChat.data.members.length > 1)) {
      setIsIncomingNetwork(false);
      setIsOutgoingNetwork(false);
    }
  }, [activeChat]);

  const renderMessage = (index: number) => {
    if (!messages[index]) return null;

    const messageDate = messages[index].createdAt
      ? getStartDateTime(ensureUtcTimestamp(messages[index].createdAt).replace('+00:00', ''))
      : new Date(new Date().setHours(0, 0, 0, 0)).getTime();

    const prevMessage = messages[index - 1] || {};
    const prevDate = ensureUtcTimestamp(prevMessage.createdAt)?.replace('+00:00', '');
    const isFirstForDay = !index || messageDate !== getStartDateTime(prevDate);
    const isFirstForUser = isFirstForDay || !index || messages[index].createdBy !== prevMessage.createdBy;
    return (
      <ChatRoomMessage
        showAvatar={isFirstForUser}
        showDate={isFirstForDay}
        currentUserData={currentUserData}
        message={messages[index]}
        setEdit={setEdit}
        setReply={setReply}
        setDeleteMessage={setDeleteMessage}
        setIsOpenLightbox={setIsOpenLightbox}
        setPhotoIndex={setPhotoIndex}
        setShowDeleteModal={setShowDeleteModal}
        onMessageAvatarClick={onMessageAvatarClick}
        chatRoomHighlightMessageId={chatRoomHighlightMessageId}
        setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
        setImages={setImages}
        setPdfFile={setPdfFile}
        setShowModal={setShowModal}
        isStaffMode={isStaffMode}
        clearQuery={clearQuery}
        onMentionClick={onMentionClick}
        setShouldScrollToTheLatestUpdatedData={setShouldScrollToTheLatestUpdatedData}
        onHashtagClick={onHashtagClick}
        isFirstUnreadMessage={
          unreadMessages[activeChat?._id || '']?.length
            ? unreadMessages[activeChat?._id || ''][0]._id === messages[index]._id
            : false
        }
      />
    );
  };

  return (
    <ChatItemsWrapper>
      <ChatRoomHeader
        setIsChatOpen={setIsChatOpen}
        setIsChatRoomSettingsOpen={setIsChatRoomSettingsOpen}
        isChatRoomSettingsOpen={isChatRoomSettingsOpen}
        updateActiveChatData={updateActiveChatData}
        isChatPage={isChatPage}
        isMaximize={isMaximize}
        setIsMaximize={setIsMaximize}
        setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
        clearQuery={clearQuery}
      />
      <ChatBody>
        {messages.length === 0 && (
          <CenterItemWrapper>
            {isChatLoading ? <BeatLoader color={theme?.brand?.primary || defaultTheme.brand.primary} /> : (
              <span>
                Type your message
              </span>
            )}
          </CenterItemWrapper>
        )}
        {messages.length !== 0 && (
          <MessagesWrapper>
            <Virtuoso
              style={{
                overscrollBehavior: 'contain',
              }}
              ref={virtuoso}
              // @ts-ignore
              computeItemKey={(_, data) => `message-${data.ts || data._id}`}
              initialTopMostItemIndex={messages.length ? (messages.length - 1) : 0}
              alignToBottom
              totalCount={messages.length}
              data={activeChat?.data.messages?.data}
              rangeChanged={rangeChanged}
              itemContent={renderMessage}
              components={{
                // eslint-disable-next-line react/no-unstable-nested-components
                Header: activeChat?.data.messages?.hasNextPage ? () => (
                  <div
                    style={{
                      padding: '4px',
                      display: 'flex',
                      justifyContent: 'center',
                    }}
                  >
                    <BeatLoader color={theme?.brand?.primary || defaultTheme.brand.primary} />
                  </div>
                ) : undefined,
              }}
              followOutput="auto"
              atBottomStateChange={(atBottom: boolean) => {
                setIsAtStart(atBottom);
              }}
            />
          </MessagesWrapper>
        )}
        <ScrollToBottomButtonWrapper>
          {!isAtStart && (
            <ScrollToBottomButton
              onClick={scrollToFirstMessage}
            >
              <ArrowDownIcon />
            </ScrollToBottomButton>
          )}
        </ScrollToBottomButtonWrapper>
        <ChatRoomHR />
        {isArchiveChatAlertOpen && (
          <NetworkChatRoomItemsWrapper>
            <ChatRoomSettingsAlertCardTitle>
              Archive Chat?
            </ChatRoomSettingsAlertCardTitle>
            <ChatRoomSettingsAlertCardDescription>
              <div>You are not connected and there is an incoming network connection request from this user.</div>
              <div>Do you want to archive this chat room?</div>
              <div>You will be able to restore your chat rooms in a future version release.</div>
            </ChatRoomSettingsAlertCardDescription>
            <NetworkChatRoomButtonsWrapper>
              <DeclineNetworkChatRoomButton
                disabled={isChatLoading}
                onClick={() => {
                  setIsArchiveChatAlertOpen(false);
                }}
              >
                Cancel
              </DeclineNetworkChatRoomButton>
              <ArchiveChatRoomButton
                color="pink"
                disabled={isChatLoading}
                onClick={async () => {
                  await leaveOrArchiveChatRoomRequest(activeChat?._id || '', true);
                  setIsArchiveChatAlertOpen(false);
                }}
              >
                Archive Chat
              </ArchiveChatRoomButton>
            </NetworkChatRoomButtonsWrapper>
          </NetworkChatRoomItemsWrapper>
        )}
        {(((!isIncomingNetwork && !isArchiveChatAlertOpen) || isOutgoingNetwork) || (
          activeChat?.data.members?.length && activeChat.data.members.length > 1
        )) && (
          <ChatMessageForm
            onMessageSending={scrollToFirstMessage}
            setEdit={setEdit}
            setReply={setReply}
            isEdit={isEdit}
            replyData={replyData}
            editChatRoomMessageRequest={editChatRoomMessageRequest}
            setImages={setImages}
            setIsOpenLightbox={setIsOpenLightbox}
            setPhotoIndex={setPhotoIndex}
          />
        )}
        {(isIncomingNetwork && !isOutgoingNetwork && !isArchiveChatAlertOpen && activeChat
          && activeChat.data.members?.length
          && activeChat.data.members.length === 1
        ) && (
          <NetworkChatRoomItemsWrapper>
            <NetworkChatRoomTextInfo>
              <NetworkChatRoomTextUserInfo>
                {`${activeChat.data.members[0].profile.firstName} ${activeChat.data.members[0].profile
                  .lastName}`}
              </NetworkChatRoomTextUserInfo>
              would like to connect and chat
            </NetworkChatRoomTextInfo>
            <NetworkChatRoomButtonsWrapper>
              <DeclineNetworkChatRoomButton
                disabled={isChatLoading}
                onClick={async () => {
                  if (incomingNetworks && activeChat?.data.members) {
                    const incomingNetworkData = incomingNetworks.find((incomingNetwork) => (
                      activeChat?.data.members
                      && incomingNetwork.userId1 === activeChat.data.members[0]._id
                    ));
                    if (incomingNetworkData) {
                      await leaveOrArchiveChatRoomRequest(activeChat?._id || '', true);
                    }
                  }
                }}
              >
                Decline
              </DeclineNetworkChatRoomButton>
              <AcceptChatRoomButton
                color="pink"
                disabled={isChatLoading}
                onClick={async () => {
                  if (incomingNetworks && activeChat?.data.members) {
                    const incomingNetworkData = incomingNetworks.find((incomingNetwork) => (
                      activeChat?.data.members
                      && incomingNetwork.userId1 === activeChat.data.members[0]._id
                    ));
                    if (incomingNetworkData) {
                      accepted({
                        payloadData: {
                          userId1: incomingNetworkData.userId1, // recipient UserId
                          userId2: incomingNetworkData.userId2, // sender UserId
                          _id: incomingNetworkData._id,
                        },
                      });
                      setIsIncomingNetwork(false);
                      setIsOutgoingNetwork(false);
                    }
                  }
                }}
              >
                Accept
              </AcceptChatRoomButton>
            </NetworkChatRoomButtonsWrapper>
          </NetworkChatRoomItemsWrapper>
        )}
        <ChatRoomSettings
          currentUserData={currentUserData}
          isOpen={isChatRoomSettingsOpen}
          setIsOpen={setIsChatRoomSettingsOpen}
          editChatRoomDataRequest={editChatRoomDataRequest}
          leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
          convertChatRoomRequest={convertChatRoomRequest}
          isStaffMode={isStaffMode}
          networkConnections={networkConnections}
        />
      </ChatBody>
      {showDeleteModal && (
        <DeleteModal
          showModal={showDeleteModal}
          setShowModal={(setShowModalData) => {
            setShowDeleteModal(setShowModalData);
            setDeleteMessage(null);
          }}
          onSubmit={() => {
            if (deleteMessage) {
              messagesModel.deleted({
                _id: deleteMessage._id,
                withoutRequest: false,
              });
              setEdit(null);
            }
          }}
          title="Warning"
          infoText="Are you sure you want to delete this message?"
        />
      )}
      {isOpenLightbox && (
        // @ts-ignore
        <Lightbox
          mainSrc={images[photoIndex]}
          nextSrc={images[(photoIndex + 1) % images.length]}
          prevSrc={images[(photoIndex + images.length - 1) % images.length]}
          onCloseRequest={() => setIsOpenLightbox(false)}
          onMovePrevRequest={() => setPhotoIndex((photoIndex + images.length - 1) % images.length)}
          onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % images.length)}
        />
      )}
    </ChatItemsWrapper>
  );
};

export default ChatRoom;
