import styled from '@emotion/styled';
import { transparentize } from 'polished';
import { Form } from 'formik';
import { UncontrolledTooltip } from 'reactstrap';
import { defaultTheme } from '../../../styles/themes';
import { Button } from '../../../components/default-components';
import { FormikInput } from '../../../components/formik';
import {
  getVerticalScrollbarStyles,
  StyledDropdown, StyledDropdownItem, StyledDropdownMenu, StyledDropdownToggle,
} from '../../../styles/common';
import {
  OriginalReplyMessage,
  OriginalReplyMessageWrap,
} from '../../../components/editor/slate-editor.styled';

export const OpenChatButton = styled(Button)`
  position: fixed;
  bottom: 15px;
  right: 15px;
  z-index: 4;
  padding: 22px 20px;
  width: 66px;
  height: 66px;
  max-height: unset;
  border-radius: 50%;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};

  svg {
    position: absolute;
    width: 34px;
    height: 34px;
    fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
  }

  &:hover {  
    background: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  }
`;

export const NewMessagesCircle = styled('div')`
  position: absolute;
  top: 12px;
  right: 6px;
  width: 20px;
  height: 20px;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  border-radius: 50%;
  border: 0.5px solid ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  box-sizing: border-box;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 10px;
  font-weight: 600;
  line-height: 15px;
  color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
`;

type ChatButtonProps = {
  hasNewMessage: 'true' | 'false' | string,
};

export const ChatButton = styled('button') <ChatButtonProps>`
  display: flex;
  align-items: center;
  background: transparent;
  border: unset;
  width: 100%;
  padding: 8px 30px;
  position: relative;
  transition: background 0.3s;

  &:hover {
    background: ${({ theme }) => transparentize(0.75, theme?.brand?.secondary
  || defaultTheme.brand.secondary)};    
  }

  ${({ hasNewMessage, theme }) => hasNewMessage === 'true' && `
    background: ${theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar};    

    &:before {
      content: '';
      height: 5px;
      width: 5px;
      border-radius: 50%;
      position: absolute;
      background: ${theme?.brand?.secondary || defaultTheme.brand.secondary};
      transition: 0.3s;
      left: 14px;
      top: 30px;
    }
  `}
`;

export const ChatButtonInfoWrapper = styled('div')`
  width: calc(100% - 72px);
`;

export const ChatButtonInfoHeader = styled('div')`
  display: flex;
  align-items: center;
`;

export const ChatButtonInfoTitle = styled('p')`  
  margin: 0 10px 3px 0;
  font-weight: 500;
  font-size: 17px;
  line-height: 25px;
  text-align: left;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};  
`;

export const ChatButtonInfoTimeDot = styled('div')`
  width: 100%;
  min-width: 6px;
  max-width: 6px;
  height: 6px;
  border-radius: 50%;
  background: ${({ theme }) => theme?.outline?.[200] || defaultTheme.outline[200]};  
  margin: 0 5px 0 0;
`;

export const ChatButtonInfoTime = styled('div')`
  color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};  
  font-weight: 500;
  font-size: 10px;
  line-height: 15px;
  min-width: 70px;
`;

type ChatButtonInfoMessageProps = {
  hasNewMessage: 'true' | 'false' | string,
};

export const ChatButtonInfoMessage = styled('p') <ChatButtonInfoMessageProps>`
  margin: 0;
  font-size: 15px;
  line-height: 22px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  text-align: left;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  ${({ hasNewMessage, theme }) => hasNewMessage === 'true' && `
    color: ${theme?.brand?.secondary || defaultTheme.brand.secondary};
    font-weight: 500;
  `}
`;

export const ChatButtonAvatarsWrapper = styled('div')`
  display: flex;
  margin: 0 20px 0 0;
  position: relative;
  min-width: 50px;
  min-height: 50px;
  z-index: 1;
`;

const setChatAvatarsPositions = (membersAmount: number, index: number, theme: any) => {
  let position = '';
  switch (true) {
    case (membersAmount === 1):
      return '';
    case (membersAmount === 2):
      if (index === 0) {
        position = `
            top: -2px;
            left: -2px;
            z-index: 1;
          `;
      }
      if (index === 1) {
        position = `
            bottom: -2px;
            left: 12px;
            z-index: 2;
          `;
      }

      return `
        border: 2px solid ${theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
        width: 40px;
        max-width: 40px;
        min-width: 40px;
        height: 40px;
        position: absolute;
        ${position}
        `;
    case (membersAmount === 3):
      if (index === 0) {
        position = `
            top: -2px;
            left: -2px;
            z-index: 1;
          `;
      }
      if (index === 1) {
        position = `
            top: -2px;
            left: 17px;
            z-index: 2;
          `;
      }
      if (index === 2) {
        position = `
            bottom: -2px;
            left: 8px;
            z-index: 3;
          `;
      }

      return `
        border: 2px solid ${theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
        width: 35px;
        max-width: 35px;
        min-width: 35px;
        height: 35px;
        position: absolute;

        ${position}
        `;
    case (membersAmount >= 4):
      if (index === 0 || (index % 4 === 0)) {
        position = `
            top: -2px;
            left: -2px;
          `;
      }
      if (index === 1 || (index % 4 === 1)) {
        position = `
            top: -2px;
            left: 17px;
          `;
      }
      if (index === 2 || (index % 4 === 2)) {
        position = `
            bottom: -2px;
            left: 17px;
          `;
      }
      if (index === 3 || (index % 4 === 3)) {
        position = `
            bottom: -2px;
            left: -2px;
          `;
      }

      return `
        border: 2px solid ${theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
        width: 35px;
        max-width: 35px;
        min-width: 35px;
        height: 35px;
        position: absolute;
        ${position}
        z-index: ${index + 1};
        `;
    default:
      return '';
  }
};

type ChatButtonAvatarProps = {
  membersAmount?: number,
  index?: number,
};

export const ChatButtonAvatarWrapper = styled('div') <ChatButtonAvatarProps>`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;

  ${({ membersAmount, index, theme }) => setChatAvatarsPositions(membersAmount || 1, index || 0, theme)}
`;

export const ChatButtonAvatar = styled('img') <ChatButtonAvatarProps>`
  height: 100%;
  width: 100%;
  object-fit: cover;
  background: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
`;

type ChatButtonAvatarPlaceholderProps = {
  membersAmount?: number,
  index?: number,
};

export const ChatButtonAvatarPlaceholder = styled('div') <ChatButtonAvatarPlaceholderProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 15px;
  border-radius: 50%;
  min-width: 50px;
  max-width: 50px;
  height: 50px;
  margin: 0;
  background: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  color: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};

  ${({ membersAmount, index, theme }) => setChatAvatarsPositions(membersAmount || 1, index || 0, theme)}
`;

type ChatMessageAvatarProps = {
  margin?: string,
}

export const ChatMessageAvatarWrapper = styled(ChatButtonAvatarWrapper) <ChatMessageAvatarProps>`
  width: 36px;
  height: 36px;

  margin: ${({ margin }) => margin || '0'};
`;

type ChatMessageAvatarPlaceholderProps = {
  margin?: string,
};

export const ChatMessageAvatarPlaceholder = styled(ChatButtonAvatarPlaceholder) <ChatMessageAvatarPlaceholderProps>`
  min-width: 36px;
  max-width: 36px;
  height: 36px;
  font-size: 13px;
  margin: ${({ margin }) => margin || '0'};
`;

type ChatMessageContainerProps = {
  isHighlight?: boolean,
};

export const ChatMessageContainer = styled('div') <ChatMessageContainerProps>`
  display: flex;
  width: 100%;
  padding: 4px 0;
  position: relative;

  ${({ isHighlight, theme }) => (isHighlight && `
    animation-duration: 3s;
    animation-name: highlightFadeout;

    @keyframes highlightFadeout {
      from {
        background: ${transparentize(0.95, theme?.brand?.secondary
  || defaultTheme.brand.secondary)};
      }
  
      50% {
       background: ${transparentize(0.98, theme?.brand?.secondary
    || defaultTheme.brand.secondary)};        
      }
  
      to {
        background: ${theme?.background?.surface || defaultTheme.background.surface};
      }
    }

    &:before {
      content: "";
      height: calc(100% - 8px);
      width: 4px;
      position: absolute;
      background: transparent;
      transition: 0.3s;
      left: 0;
      animation: borderHighlightFadeout 2s;
    }
  
    @keyframes borderHighlightFadeout {
      from {
        background: ${theme?.brand?.secondary || defaultTheme.brand.secondary};
      }
  
      50% {
        background: ${transparentize(0.5, theme?.brand?.secondary || defaultTheme.brand.secondary)};
      }
  
      to {
        background: transparent;
      }
    }
  `)}
`;

type ChatMessageWrapperProps = {
  isCurrentUserMessage: boolean,
  showAvatar: boolean,
};

export const ChatMessageWrapper = styled('div') <ChatMessageWrapperProps>`
  display: flex;
  padding: 0 30px;
  max-width: 100%;

  ${(props) => !props.showAvatar && !props.isCurrentUserMessage && 'padding-left: 76px'};
  ${(props) => !props.showAvatar && props.isCurrentUserMessage && 'padding-right: 76px'};

  ${({ isCurrentUserMessage }) => (isCurrentUserMessage && `
    flex-direction: row-reverse;
    margin-left: auto;
  `)}
`;

export const ArticlePointerWrapper = styled('div')`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 10px 0 0 0;
`;

export const ChatMessagePointerWrapper = styled(ArticlePointerWrapper)`
  margin: 0 0 5px 0;
  padding: 0 30px;
`;

type MessageContentWrapperProps = {
  isCurrentUser: boolean,
  showAvatar: boolean,
  isLoading?: boolean,
};

export const MessageContentWrapper = styled('div') <MessageContentWrapperProps>`
  word-break: break-word;
  background: ${({ theme }) => theme?.brand?.secondaryContainer || defaultTheme.brand.secondaryContainer};            
  border-radius: 10px;
  padding: 10px;
  max-width: ${(props) => (props.showAvatar ? 'calc(100% - 46px)' : '100%')};
  position: relative;

  ${({ isCurrentUser, theme }) => (isCurrentUser && `
    background: ${theme?.background?.surface || defaultTheme.background.surface};
    border: 1px solid ${theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar};
  `)}

  & > div {
    button {
      background: transparent;
      color: transparent;

      svg {
        fill: transparent;
      }
    }
  }

  &:hover {
    & > div {
      button {
        color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};          

        svg {
          fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};            
        }
      }
    }

    .dropdown {
      display: flex;
      
      .btn {
        color: inherit;

        svg {
          fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
        }
      }
    }
  }
`;

export const ScrollToBottomButtonWrapper = styled('div')`
  position: relative;
`;

export const ScrollToBottomButton = styled('button')`
  position: absolute;
  right: 15px;
  bottom: 15px;
  width: 50px;
  height: 50px;
  background: ${({ theme }) => transparentize(0.55, theme?.brand?.secondary || defaultTheme.brand.secondary)};  
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0.8;
  outline: unset;
  border: unset;

  svg {
    width: 24px;
    height: 24px;
    fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};        
  }
`;

export const CenterItemWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;

  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};          
`;

export const MessagesWrapper = styled('div')`
  height: 100%;
  position: relative;

  div {
   ${({ theme }) => getVerticalScrollbarStyles(theme, false)}   
  }
`;

export const ChatRoomEditorWrapper = styled('div')`
  padding: 0 30px;
`;

export const ChatRoomHeaderButton = styled(Button)`
  padding: 5px;
  z-index: 1;

  svg {
    width: 24px;
    height: 24px;
    fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
  }

  &:focus {
    background: transparent;

    svg {
      fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
    }
  }

  &:hover, &:active {
    background: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};

    svg {
      fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
    }
  }

  &:not(:last-of-type) {
    margin: 0 12px 0 0;
  }
`;

export const ChatRoomHeaderBackButton = styled(ChatRoomHeaderButton)`
  margin: 0 12px 0 0;
  z-index: 1;
`;

export const ChatRoomHeaderTitle = styled('span')`
  color: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
  font-weight: 500;
  font-size: 17px;
  line-height: 25px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const FlexWrapper = styled('div')`
  display: flex;
  align-items: center;
`;

export const ChatItemsWrapper = styled('div')`
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
`;

export const ChatHeader = styled('div')`
  display: flex;
  justify-content: space-between;  
  background: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  color: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
  width: 100%;
  height: 54px;
  padding: 12px 30px;
  font-weight: 500;
  font-size: 17px;
  line-height: 25px;
  border-radius: 10px 10px 0 0;
  cursor: move;
  position: relative;
`;

export const ChatHeaderDraggable = styled('div')`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 0;
`;

export const HeaderFlexWrapper = styled('div')`
  display: flex;
  align-items: center;
  overflow: hidden;

  & form {
    flex-grow: 1;
  }
`;

export const HeaderLeftSideWrapper = styled(HeaderFlexWrapper)`
  width: calc(100% - 115px);
`;

export const ChatSearchItemsWrapper = styled('div')`
  margin: 20px 24px 14px 24px;
`;

type ChatSearchInputWrapperProps = {
  isInputEmpty: boolean,
}

export const ChatSearchInputWrapper = styled('form') <ChatSearchInputWrapperProps>`
  position: relative;
  display: flex;
  margin: ${({ isInputEmpty }) => (isInputEmpty ? 'unset' : '0 0 16px 0')};
  
  .odecloud-input-wrapper {
    width: 100%;
  }

  input {
    padding: 8px 34px 8px 10px;
    position: relative;
    font-weight: 500;
    font-size: 15px;
    line-height: 22px;
    color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
    background: ${({ theme }) => theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar};
    max-width: unset;
    border: unset;
  }

  & > svg {
    position: absolute;
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
    z-index: 1;
    left: 15px;
    top: 10px;
    width: 20px;
    height: 20px;
  }
`;

export const ChatSearchInputSubmitButton = styled(Button)`
  padding: 2px;
  width: 40px;
  height: 40px;
  margin: 0 0 0 5px;

  svg {
    width: 24px;
    height: 24px;
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};    
  }   
`;

export const ChatSearchInputClearButton = styled(Button)`
  position: absolute;
  padding: 2px;
  right: 48px;
  top: 8px;
  width: 25px;
  height: 25px;
  background: transparent !important;

  svg {
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};    
  }
`;

export const ChatSearchForButton = styled(Button)`
  margin: 0 auto;
  padding: 8px;
  font-weight: 500;
  font-size: 14px;
  line-height: 21px;
  border-radius: 4px;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
`;

export const ChatBody = styled('div')`
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  width: 100%;
  height: calc(100% - 54px);
  padding: 0 0 14px 0;
  border-radius: 0 0 10px 10px;
  display: flex;
  flex-direction: column;
  position: relative;
  overflow: hidden;

  #virtuoso-scroller {
    ${({ theme }) => getVerticalScrollbarStyles(theme)}
  }
`;

export const HeaderActionButton = styled(Button)`
  padding: 5px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  z-index: 1;

  svg {
    fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary}; 
    width: 24px;
    height: 24px;
  }

  &:hover, &:active, &:focus {
    color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
    background: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};    

    svg {
      fill: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};      
    }
  }

  &:not(:last-of-type) {
    margin: 0 5px 0 0;
  }
`;

export const MinimizeButton = styled(HeaderActionButton)`

  svg {
    margin: 6px 0 -6px 0;
  }
`;

export const NewChatTitle = styled('p')`
  font-weight: 500;
  font-size: 22px;
  line-height: 33px;
  margin: 0 0 40px 0;
  width: 100%;
  text-align: center;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
`;

export const MembersLabel = styled('p')`
  font-weight: 500;
  font-size: 12px;
  line-height: 18px;
  margin: 0 0 5px 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
`;

type MembersListProps = {
  isEmpty: boolean,
};

export const MembersList = styled('div') <MembersListProps>`
  width: 100%;
  min-height: 42px;
  border: 1px dashed ${({ theme }) => theme?.outline?.[200] || defaultTheme.outline[200]};
  box-sizing: border-box;
  border-radius: 6px;
  max-height: 232px;
  overflow: auto;

  span {
    color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};    
  }

  ${({ theme }) => getVerticalScrollbarStyles(theme)}
  

  ${({ isEmpty, theme }) => isEmpty && `
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    color: ${theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  `}
`;

export const MemberButton = styled(Button)`
  width: 100%;
  border-radius: unset;
  justify-content: space-between;
  padding: 6px 15px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  height: 40px;

  p {
    margin: 0 10px 0 0;
  }

  svg {
    display: none;
    fill: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
    transform: rotate(45deg);
  }

  &:hover {
    color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};

    svg {
      display: initial;
    }
  }

  &:disabled {
    svg {
      display: none;
    }
  }
`;

export const UserInfoWrapper = styled('div')`
  display: flex;
  align-items: center;
  width: 100%;

  span {
    margin-left: 16px;
  }
`;

export const SaveButton = styled(Button)`
  margin: auto 0 0 auto;
  padding: 25px 40px;
  font-weight: 500;
  font-size: 20px;
  line-height: 30px;
  max-width: 130px;
`;

export const StyledForm = styled(Form)`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const WideInputWrapper = styled('div')`
  .odecloud-input-wrapper {
    width: 100%;

    input {
      max-width: unset;
    }
  }
`;

export const ChatTitleFormikInputWrapper = styled(WideInputWrapper)`
  p {
    font-weight: 500;
    font-size: 12px;
    line-height: 18px !important;
    color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};              
    margin: 0 0 7px 0 !important;
  }
`;

export const ChatTitleFormikInput = styled(FormikInput)`
  font-size: 14px;
  padding: 11px 16px;
  border-radius: 7px;
`;

export const MembersListWrapper = styled('div')`
  margin: 0 0 40px 0;
`;

export const DateWrapper = styled('div')`
  padding: 12px 30px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  font-size: 12px;
`;

export const MessagesDate = styled(DateWrapper)`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  background: ${({ theme }) => transparentize(0.8, theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar)};
  padding: 4px 12px;
  border-radius: 10px;
  z-index: 1;
`;

export const ChatPageWrapper = styled('div')`
  height: 100%;

  @media screen and (min-width: 769px) {
    height: calc(100vh - 140px);
  }
`;

type ChatUserNameProps = {
  isSmall?: boolean;
}

export const ChatUserName = styled('div') <ChatUserNameProps>`
  font-size: ${({ isSmall }) => (isSmall ? 10 : 12)}px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  font-weight: 400;
  line-height: 1;
  margin-bottom: 4px;
`;

export const ChatOriginalReplyMessageWrap = styled(OriginalReplyMessageWrap)`
  cursor: pointer;
`;

export const MessageErrorWrap = styled('div')`
  margin-right: 8px;

  svg {
    fill: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};
    height: 24px;
    width: 24px;
  }
`;

export const InvisibleProfilePreviewButton = styled(Button)`
  max-height: unset;
  height: fit-content;
  padding: 0;
  background: transparent;

  &:hover, &:active, &:focus {
    background: transparent;
  }
`;

export const PointerRedLine = styled('div')`
  width: 100%;
  min-width: 24px;
  height: 2px;
  background: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};
`;

export const CommentOptionsTooltip = styled(UncontrolledTooltip)`

  .tooltip {
    opacity: unset !important;
  }

  .tooltip-inner {
    max-width: unset;
    background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};          
    display: flex;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};         
    box-shadow: ${({ theme }) => theme?.shadow?.inner?.[1] || defaultTheme.shadow.inner[1]};
    padding: 6px 10px;
  }
`;

export const StyledButton = styled(Button)`
  padding: 5px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};

  svg {
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  }

  &:not(:last-of-type) {
    margin: 0 10px 0 0;
  }

  &:focus {
    color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};

    svg {
      fill: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    }
  }

  &:not(:disabled):hover, &:not(:disabled):active {
    color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};

    svg {
      fill: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    }
  }
`;

export const PointerText = styled('p')`
  max-width: calc(100% - 48px);
  padding: 0 10px;
  flex-shrink: 0;
  color: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};
  display: flex;
  justify-content: center;
  margin: 0;
  text-align: center;
`;

export const ErrorTextStyle = styled('div')`
  font-size: 11px;
  color: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};  
  white-space: pre-wrap;
  max-width: fit-content;
`;

export const OriginalReplyMessageSmall = styled(OriginalReplyMessage)`
  line-height: 1;
`;

export const MessageControlButton = styled('button')`
  background: transparent;
  border: none;
  padding: 0;
  color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  display: flex;
  align-items: center;
  transition: 0.3s;
  font-size: 12px;

  svg {
    margin-right: 4px;
    height: 12px;
    width: 12px;
    fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  }

  &:hover {
    color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};

    svg {
      fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
    }
  }
`;

type MessageBottomProps = {
  isCurrentUser: boolean,
};

export const MessageBottom = styled('div') <MessageBottomProps>`
  display: flex;
  justify-content: flex-end;
  flex-direction: ${({ isCurrentUser }) => (isCurrentUser ? 'row' : 'row-reverse')};

  button {
    ${({ isCurrentUser }) => (isCurrentUser ? 'margin-right' : 'margin-left')}: 12px;
  }
`;

export const MessageMainContainer = styled('div')`
  position: relative;
`;

export const MessageActionsDropdownWrapper = styled('div')`
  position: absolute;
  top: 0;
  right: 0;
`;

type MessageActionsDropdownProps = {
  disabled?: boolean,
  backgroundColor: string | null;

};

export const MessageActionsDropdown = styled(StyledDropdown) <MessageActionsDropdownProps>`
  display: none;
  transition: 0.3s;

  .btn {
    transition: 0.3s;
    background: linear-gradient(225deg,
     ${({ backgroundColor, theme }) => (backgroundColor
    || theme?.background?.surfaceContainerVar
    || defaultTheme.background.surfaceContainerVar)} 50%, transparent 100%) !important;
    border-radius: 0 0 0 50%;
    padding: 0;  
    display: flex;
    border: unset;
    width: 100%;
    align-items: center;
    justify-content: center;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};    

    svg {
      fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};     
    }

    &:hover, &:active, &:focus {
      background: linear-gradient(225deg, 
        ${({ backgroundColor, theme }) => (backgroundColor
    || theme?.background?.surfaceContainerVar
    || defaultTheme.background.surfaceContainerVar)} 50%, transparent 100%) !important;
      outline: none;
      box-shadow: unset !important;
    }
  }

  svg {
    width: 24px;
    height: 24px;
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};        
  }

  &:disabled {    
    background: ${({ theme }) => theme?.background?.surfaceContainerVar
    || defaultTheme.background.surfaceContainerVar} !important;
  }

  &:hover, &:active {
    background: transparent !important;
    outline: none;
    box-shadow: unset !important;
  }
`;

type MessageActionsDropdownToggleProps = {
  isOpen?: boolean,
};

export const MessageActionsDropdownToggle = styled(StyledDropdownToggle) <MessageActionsDropdownToggleProps>`
  svg {
    width: 24px;
    height: 24px;
    fill: ${({ isOpen, theme }) => (isOpen ? (theme?.text?.onSurface || defaultTheme.text.onSurface) : 'transparent')};

    ${({ isOpen }) => (isOpen && `
      transform: rotate(180deg);
    `)}
  }
    
  padding: 0;  

  &:disabled {
    background: ${({ theme }) => theme?.background?.surfaceContainerVar
    || defaultTheme.background.surfaceContainerVar} !important;        
    cursor: not-allowed;
  }
`;

export const MessageActionsDropdownMenuItem = styled(StyledDropdownItem)`
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface} !important;

  svg {
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface} !important;       
  }

  &:hover, &:active, &:focus {
    color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary} !important;    

    background: ${({ theme }) => transparentize(0.75, theme?.brand?.secondary
  || defaultTheme.brand.secondary)} !important;

    svg {
      fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary} !important;
    }
  }
`;

export const MessageActionsDropdownMenu = styled(StyledDropdownMenu)`
  background: ${({ theme }) => theme?.background?.surfaceContainerHigh
    || defaultTheme.background.surfaceContainerHigh};  
`;

export const ChatRoomHR = styled('hr')`
  width: 100%;
  margin: 12px 0;
  border-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
`;

export const NetworkChatRoomItemsWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  padding: 10px 24px 32px 24px;
`;

export const NetworkChatRoomTextInfo = styled('p')`
  font-weight: 500;
  font-size: 15px;
  line-height: 22px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};  
  text-align: center;
`;

export const NetworkChatRoomTextUserInfo = styled('span')`
  font-weight: 600;
  font-size: 15px;
  line-height: 22px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  margin: 0 5px 0 0;
`;

export const NetworkChatRoomButtonsWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const DeclineNetworkChatRoomButton = styled(Button)`
  padding: 16px 50px;
  font-weight: 500;
  font-size: 18px;
  line-height: 27px;
  color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  background: ${({ theme }) => theme?.brand?.secondaryContainer || defaultTheme.brand.secondaryContainer};
  border: 1px solid ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  max-height: unset;
  border: unset;

  &:hover,
  &:active,
  &:focus {
    color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
    background: ${({ theme }) => theme?.brand?.secondaryContainer || defaultTheme.brand.secondaryContainer};
  }
`;

export const AcceptChatRoomButton = styled(Button)`
  padding: 16px 50px;
  font-weight: 500;
  font-size: 18px;
  line-height: 27px;
  max-height: unset;
`;

export const ArchiveChatRoomButton = styled(AcceptChatRoomButton)`
  padding: 16px 32px;
`;

export const MessageUnreadWrapper = styled('div')`
  background: ${({ theme }) => transparentize(0.8, theme?.brand?.secondary || defaultTheme.brand.secondary)};
  color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  font-size: 12px;
  text-align: center;
  padding: 2px 0;
  margin: 4px 0;
`;
