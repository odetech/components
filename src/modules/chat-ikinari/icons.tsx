import React from 'react';
import styled from '@emotion/styled';

type SvgProps = {
  cssWidth?: string;
  cssHeight?: string;
  fill?: string;
};

const StyledIcon = styled('svg')<SvgProps>`
  width: ${({ cssWidth }) => (cssWidth || '16px')};
  height: ${({ cssHeight }) => (cssHeight || '16px')};
  fill: ${({ fill }) => fill};
  transition: fill .3s;
`;

export const ArrowLeftIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M20,11V13H8L13.5,18.5L12.08,19.92L4.16,12L12.08,4.08L13.5,5.5L8,11H20Z"
    />
  </StyledIcon>
);

export const ArrowDownIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M11,4H13V16L18.5,10.5L19.92,11.92L12,19.84L4.08,11.92L5.5,10.5L11,16V4Z"
    />
  </StyledIcon>
);

export const WindowMinimizeIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M20,14H4V10H20"
    />
  </StyledIcon>
);

export const TuneIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M3,17V19H9V17H3M3,5V7H13V5H3M13,21V19H21V17H13V15H11V21H13M7,9V11H3V13H7V15H9V9H7M21,13V11H11V13H21M15,
      9H17V7H21V5H17V3H15V9Z"
    />
  </StyledIcon>
);

export const MessageOutlineIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M20 2H4C2.9 2 2 2.9 2 4V22L6 18H20C21.1 18 22 17.1 22 16V4C22 2.9 21.1 2 20 2M20 16H5.2L4 17.2V4H20V16Z"
    />
  </StyledIcon>
);

export const PlusIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"
    />
  </StyledIcon>
);

export const PlusBoxOutlineIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M19,19V5H5V19H19M19,3A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5C3,3.89 3.9,3
      5,3H19M11,7H13V11H17V13H13V17H11V13H7V11H11V7Z"
    />
  </StyledIcon>
);

export const CheckCircleOutlineIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M12 2C6.5 2 2 6.5 2 12S6.5 22 12 22 22 17.5 22 12 17.5 2 12 2M12 20C7.59 20 4 16.41 4 12S7.59 4 12 4 20 7.59
      20 12 16.41 20 12 20M16.59 7.58L10 14.17L7.41 11.59L6 13L10 17L18 9L16.59 7.58Z"
    />
  </StyledIcon>
);

export const ArchiveIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M22 19C22 20.1046 21.1046 21 20 21H4C2.89543 21 2 20.1046 2 19V11.7947L2.08085 11.6061L5.10236
      4.55926C5.55303 3.64142 6.12144 3.07301 7 3H17L17.1308 3.00859C17.8837 3.10794 18.4351 3.65935 18.9191
      4.60608L22 11.7947V19ZM4 13V19H20V13H16C16 14.1046 15.1046 15 14 15H10C8.89543 15 8 14.1046
      8 13H4ZM4.51654 11H10V13H14V11H19.484L17.1107 5.45729C16.9987 5.23944 16.8953 5.08654 16.8118 4.99993L7.17592
      4.99623C7.0987 5.07946 7.00333 5.22529 6.91915 5.39392L4.51654 11Z"
    />
  </StyledIcon>
);

export const PencilIcon: React.FC<SvgProps> = (props) => (
  <StyledIcon cssWidth="24px" cssHeight="24px" viewBox="0 0 20 20" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      /* eslint-disable-next-line max-len */
      d="M2.5 14.375V17.5H5.625L14.8417 8.28333L11.7167 5.15833L2.5 14.375ZM4.93333 15.8333H4.16667V15.0667L11.7167 7.51668L12.4833 8.28335L4.93333 15.8333ZM17.2584 4.69167L15.3084 2.74167C15.1417 2.575 14.9334 2.5 14.7167 2.5C14.5 2.5 14.2917 2.58333 14.1334 2.74167L12.6084 4.26667L15.7334 7.39167L17.2584 5.86667C17.5834 5.54167 17.5834 5.01667 17.2584 4.69167Z"
    />
  </StyledIcon>
);

export const TrashIcon: React.FC<SvgProps> = (props) => (
  <StyledIcon
    cssWidth="20px"
    cssHeight="20px"
    viewBox="0 0 24 24"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    {...props}
  >
    <polyline points="3 6 5 6 21 6" />
    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2" fill="none" />
  </StyledIcon>
);

export const AlertCircleOutlineIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0
       12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z"
    />
  </StyledIcon>
);

export const DeleteIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon cssWidth="24px" cssHeight="24px" viewBox="0 0 24 24" {...props}>
    <path
      d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8,9H16V19H8V9M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z"
    />
  </StyledIcon>
);

export const CloseIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon cssWidth="24px" cssHeight="24px" viewBox="0 0 24 24" {...props}>
    <path
      d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z"
    />
  </StyledIcon>
);

export const ArrowLeftTopIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M20 13.5V20H18V13.5C18 11 16 9 13.5 9H7.83L10.91 12.09L9.5 13.5L4 8L9.5
        2.5L10.92 3.91L7.83 7H13.5C17.09 7 20 9.91 20 13.5Z"
    />
  </StyledIcon>
);

export const CopyIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M16 1H4C2.9 1 2 1.9 2 3V17H4V3H16V1ZM15.0001 5.00013H8.00013C6.90013 5.00013 6.01013 5.90013 6.01013
      7.00013L6.00013 21.0001C6.00013 22.1001 6.89013 23.0001 7.99013 23.0001H19.0001C20.1001 23.0001
      21.0001 22.1001 21.0001 21.0001V11.0001L15.0001 5.00013ZM8 21V7H14V12H19V21H8Z"
    />
  </StyledIcon>
);

export const WindowMaximizeIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M4,4H20V20H4V4M6,8V18H18V8H6Z"
    />
  </StyledIcon>
);

export const WindowRestoreIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M4,8H8V4H20V16H16V20H4V8M16,8V14H18V6H10V8H16M6,12V18H14V12H6Z"
    />
  </StyledIcon>
);

export const MagnifyIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59
      14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16
      9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14
      9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z"
    />
  </StyledIcon>
);

export const ChevronDownIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />
  </StyledIcon>
);

export const CheckAllIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M14.6201 6L6.54685 14.1999L3.3554 10.95L1.97461 12.35L6.54685 17L16.0009 7.40006L14.6201 6ZM10.6674
      15.0943L12.5469 17L22.0009 7.40006L20.6201 6L12.5469 14.1999L12.0472 13.6932L10.6674 15.0943Z"
    />
  </StyledIcon>
);

export const CheckIcon: React.FC<SvgProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M9.70711 14.2929L19 5L20.4142 6.41421L9.70711 17.1213L4 11.4142L5.41421 10L9.70711 14.2929Z"
    />
  </StyledIcon>
);
