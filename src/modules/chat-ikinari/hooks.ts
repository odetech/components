import { useEffect, createContext, useContext } from 'react';

export const useQuery = (search: string) => new URLSearchParams(search);

export const getImagesArray = (content: string) => {
  let imgArray: string[] = [];
  const matchArray = content.match(/<img [^>]*src="[^"]*"[^>]*>/gm);
  matchArray?.forEach((item) => {
    if (item) {
      imgArray = [item.replace(/.*src="([^"]*)".*/, '$1'), ...imgArray];
    }
  });

  return imgArray.filter((src) => !src.match(/[^"]*.+\.pdf/g)).reverse();
};

export const useGallery = (
  content: string,
  name: string,
  setImages: (images: string[]) => void,
  setIsOpenLightbox: (isOpen: boolean) => void,
  setPhotoIndex: (index: number) => void,
  setShowModal: (isShow: boolean) => void,
  setPdfFile: (pdfFile: string) => void,
) => {
  const onImageClick = () => {
    setImages(getImagesArray(content));
    setIsOpenLightbox(true);
    setPhotoIndex(0);
  };

  const onPdfClick = (link: string) => () => {
    setShowModal(true);
    setPdfFile(link);
  };

  useEffect(() => {
    const imgButtonWrappers: HTMLCollectionOf<Element> = document
      .getElementsByClassName(`${name}`);
    for (let i = 0; i < imgButtonWrappers.length; i += 1) {
      imgButtonWrappers[i].addEventListener('click', onImageClick, false);
    }

    const pdfButtonWrappers: HTMLCollectionOf<Element> = document
      .getElementsByClassName(`${name}Pdf`);
    for (let i = 0; i < pdfButtonWrappers.length; i += 1) {
      const dataPdfAttr = pdfButtonWrappers[i].getAttribute('data-pdf');

      if (dataPdfAttr) {
        pdfButtonWrappers[i].addEventListener(
          'click',
          onPdfClick(dataPdfAttr),
          false,
        );
      }
    }
  }, [content]);
};

export const ChatContext = createContext<{
  appErrorHandler:((
    error: any,
    options?: {
      userRoles?: {
        _id?: string | undefined;
        isStaff?: boolean | undefined;
        isDev?: boolean | undefined;
      } | null | undefined;
      noToast?: boolean | undefined;
      toastMessage?: string | undefined;
    } | undefined) => void) | undefined; } | null>(null);

export const useChat = () => {
  const context = useContext(ChatContext);
  if (!context) {
    throw new Error('ChatContext should be passed');
  }
  return context;
};
