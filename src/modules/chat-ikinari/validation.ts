import * as yup from 'yup';
import { yupToFormErrors } from 'formik';

const newChatModalValidation = () => (
  yup.object().shape({
    members: yup.array()
      .min(1, 'Cannot be empty'),
    title: yup.string()
      .required('Cannot be empty')
      .test(
        'start-end-with-spaces',
        'This field cannot start from spaces',
        (value = '') => value.trim().length === value.length,
      )
  })
);

const validate = (values: unknown) => {
  const context = {};
  try {
    newChatModalValidation().validateSync(values, { abortEarly: false, context });
  } catch (e) {
    const errors = yupToFormErrors(e);
    if (!errors) {
      return {};
    }
    return errors;
  }
  return {};
};

export default validate;
