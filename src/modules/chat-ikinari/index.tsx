import React, {
  useCallback, useEffect, useState, Fragment,
} from 'react';
import { useUnit } from 'effector-react';

import OdeCloud from '@odecloud/odecloud';
import { $$networksModel } from '~/entities/networks';
import {
  $$chatModel, $$chatsModel, $loading, $$messagesModel,
} from '~/entities/chat/model';
import { ThemeProvider } from '@storybook/theming';
import { ThemeKey, ThemesKey } from '~/styles/types';
import { themes } from '../../styles/themes';
import { Chat, ChatButton } from './ui';
import { ChatMessageWithMoreDataProps, UserData } from '../../entities/chat/types';

export type ChannelsChatProps = {
  setProfilePreviewUserData: (isProfileCardOpen: UserData['info'] | null) => void,
  activeChatRoomId?: string | null,
  highlightMessageId?: string | null,
  width?: number,
  isChatOpen: boolean,
  setIsChatOpen: (isChatOpen: boolean) => void,
  isChatPage?: boolean,
  fetchTags?: () => void,
  // eslint-disable-next-line react/no-unused-prop-types
  api?: OdeCloud,
  userData: UserData,
  tagsLoading: boolean,
  // eslint-disable-next-line react/no-unused-prop-types
  app?: string,
  isStaffMode: boolean,
  appErrorHandler?: (
    error: Error,
    options?: {
      userRoles?: {
        _id?: string,
        isStaff?: boolean,
        isDev?: boolean,
      } | null,
      noToast?: boolean,
      toastMessage?: string,
    },
  ) => void,
  chatContainerZIndex?: number,
  appSearchChatRoomData?: {
    userFirstname: string,
    userId: string,
  } | null,
  setAppSearchChatRoomData?: (
    appSearchChatRoomData: {
      userFirstname: string,
      userId: string,
    } | null,
  ) => void,
  clearQuery?: () => void,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
  onMentionClick?: (userId: string) => void,
  onContextMenu?: any;
  onHashtagClick?: (hashtagValue: string) => void,
  withPreviewChatButton?: boolean,
  themeMode ?: ThemeKey,
};

export const ChannelsChat = ({
  setProfilePreviewUserData, activeChatRoomId, highlightMessageId, width, isChatOpen, setIsChatOpen, isChatPage,
  userData, fetchTags, tagsLoading, isStaffMode, appErrorHandler, chatContainerZIndex,
  appSearchChatRoomData, setAppSearchChatRoomData, clearQuery, networkConnections,
  onMentionClick, onHashtagClick, onContextMenu,
  withPreviewChatButton = true, themeMode = ThemesKey.Light,
}: ChannelsChatProps) => {
  const [chatRoomHighlightMessageId, setChatRoomHighlightMessageId] = useState(highlightMessageId || null);

  const chatModel = useUnit({
    created: $$chatModel.created,
    edited: $$chatModel.edited,
    leavedOrArchived: $$chatModel.leavedOrArchived,
    activeChat: $$chatModel.$activeChat,
    activated: $$chatModel.activated,
    privateBecame: $$chatModel.privateBecame,
  });

  const chatsModel = useUnit({
    requested: $$chatsModel.requested,
    searched: $$chatsModel.searched,
    chats: $$chatsModel.$chats,
  });

  const chatsLoading = useUnit($loading);
  const incomingNetworks = useUnit($$networksModel.$incomingNetworks);
  const outgoingNetworks = useUnit($$networksModel.$outgoingNetworks);

  const messagesModel = useUnit({
    requested: $$messagesModel.requested,
    edited: $$messagesModel.edited,
    deleted: $$messagesModel.deleted,
    searched: $$messagesModel.searched,
  });

  useEffect(() => {
    setChatRoomHighlightMessageId(highlightMessageId || null);
  }, [highlightMessageId]);

  const onMessageAvatarClick = useCallback((messageUserData: UserData['info'] | null) => {
    setProfilePreviewUserData(messageUserData);
  }, [setProfilePreviewUserData]);

  /* actions with chat */
  const createChatRequest = useCallback((
    requestData: {
      title: string,
      members: string[],
    },
  ) => {
    chatModel.created({
      title: requestData.title,
      members: requestData.members,
      appErrorHandler,
    });
  }, []);

  const editChatRoomRequest = useCallback((
    updatedChatData: {
      title?: string,
      members: string[],
      _id: string,
    },
  ) => {
    chatModel.edited(updatedChatData);
  }, []);

  const leaveOrArchiveChatRoomRequest = useCallback((
    updatedChatId: string,
    isArchive?: boolean,
  ) => {
    chatModel.leavedOrArchived({
      _id: updatedChatId,
      isArchive,
    });
  }, []);

  const updateActiveChatData = useCallback((chatId: string | null) => {
    chatModel.activated(chatId);
  }, [chatsModel.chats]);

  const convertChatRoomRequest = useCallback((
    updatedChatRoomId: string,
  ) => {
    chatModel.privateBecame({
      _id: updatedChatRoomId,
      callback: fetchTags,
    });
  }, []);

  /* actions with chats */
  const fetchChatDataRequest = useCallback(() => {
    if (!chatsLoading && !tagsLoading) {
      chatsModel.requested({
        userId: userData.id,
        limit: 15,
        pageNumber: 1,
      });
      chatsModel.requested({
        userId: userData.id,
        limit: 15,
        pageNumber: 2,
      });
      chatsModel.requested({
        userId: userData.id,
        limit: 15,
        pageNumber: 3,
      });
    }
  }, [tagsLoading, chatsLoading]);

  useEffect(() => {
    fetchChatDataRequest();
  }, []);

  const searchChatRoomsRequest = (
    {
      chatSearchType,
      chatSearchInputValue,
    }: {
      chatSearchType: string,
      chatSearchInputValue: string,
    },
  ) => {
    chatsModel.searched({
      chatSearchType,
      chatSearchInputValue,
      secondMemberId: appSearchChatRoomData?.userId || undefined,
    });
  };

  /* actions with messages */
  const getMoreChatRoomMessagesRequest = useCallback((chatRoomId: string) => {
    messagesModel.requested({ chatId: chatRoomId });
  }, []);

  const editChatRoomMessageRequest = (messageNewData: ChatMessageWithMoreDataProps) => {
    if (chatModel.activeChat) {
      messagesModel.edited(messageNewData);
    }
  };

  const searchGetMoreChatMessagesRequest = (
    {
      tagId,
      messageCreatedAtDate,
    }: {
      tagId: string,
      messageCreatedAtDate: string,
    },
  ) => {
    messagesModel.searched({
      tagId,
      messageCreatedAtDate,
    });
  };

  return (
    <ThemeProvider theme={themes[themeMode] || themes.light}>
      <Fragment>
        {withPreviewChatButton && (
        <ChatButton
          isChatOpen={isChatOpen}
          setIsChatOpen={setIsChatOpen}
        />
        )}
        <Chat
          onContextMenu={onContextMenu}
          isChatOpen={isChatOpen}
          setIsChatOpen={setIsChatOpen}
          tagsLoading={tagsLoading}
          fetchChatDataRequest={fetchChatDataRequest}
          isChatLoading={chatsLoading}
          onMessageAvatarClick={onMessageAvatarClick}
          activeChatRoomId={activeChatRoomId}
          chatRoomHighlightMessageId={chatRoomHighlightMessageId}
          createChatRequest={createChatRequest}
          updateActiveChatData={updateActiveChatData}
          getMoreChatRoomMessagesRequest={getMoreChatRoomMessagesRequest}
          editChatRoomMessageRequest={editChatRoomMessageRequest}
          editChatRoomDataRequest={editChatRoomRequest}
          leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
          convertChatRoomRequest={convertChatRoomRequest}
          searchChatRoomsRequest={searchChatRoomsRequest}
          searchGetMoreChatMessagesRequest={searchGetMoreChatMessagesRequest}
          setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
          currentUserData={userData}
          isStaffMode={isStaffMode}
          width={width}
          isChatPage={isChatPage}
          chatContainerZIndex={chatContainerZIndex}
          appSearchChatRoomData={appSearchChatRoomData}
          setAppSearchChatRoomData={setAppSearchChatRoomData}
          clearQuery={clearQuery}
          networkConnections={networkConnections}
          onMentionClick={onMentionClick}
          onHashtagClick={onHashtagClick}
          incomingNetworks={incomingNetworks}
          outgoingNetworks={outgoingNetworks}
        />
      </Fragment>
    </ThemeProvider>
  );
};
