import { Action, ThunkAction } from '@reduxjs/toolkit';

export type ChatDataProps = {
  data: {
    members: MemberDataProps[] | null,
    messages: {
      hasNextPage: boolean,
      pageNumber: string,
      pageSize: number,
      total: number,
      data: ChatMessageWithMoreDataProps[],
    } | null,
    unreadMessages: MessageDataProps[],
  },
  title: string,
  members: string[],
  createdBy: string,
  createdAt: string,
  _id: string,
  messageSearchInfo?: {
    _id: string,
    rawText: string,
    filesLength: number,
    createdAt: string,
    tag: string,
    isAlreadyLoaded: boolean,
  },
  isArchive?: boolean,
};

export type MemberDataProps = {
  _id: string,
  profile: {
    firstName: string,
    lastName: string,
    avatar: {
      secureUrl?: string,
      _id?: string,
    } | null,
  }
};

export type ChatMessageWithMoreDataProps = MessageWithMoreDataProps & {
  associatedMessageData?: MessageDataProps & {
    previewText?: string,
  },
  uiIds?: string[],
};

export type MessageWithMoreDataProps = MessageDataProps & {
  status?: 'editing' | 'deleting' | 'creating',
  error?: 'true' | string,
};

export type ArticleFileProps = {
  _id: string,
  associatedId: string,
  createdBy: string,
  createdAt:string,
  cloudinary: {
    public_id: string,
    format:string,
    bytes: string,
    url: string,
    secure_url: string
  },
  uiId: string
};

export type MessageDataProps = {
  associatedId: string | null,
  createdAt: string,
  createdBy: string,
  data?: {
    bookmark?: null | {
      url: string,
      userId: string,
    },
    commentsCount?: number | null,
    createdBy?: MemberDataProps | null,
    files?: ArticleFileProps[] | null,
    notificationSettings?: {
      currentFrequencies: string[],
      userId: string,
      _id: string,
    },
  },
  extra: null,
  files: null | any[],
  isByStaff: boolean,
  isDraft: boolean,
  likes: string[],
  mentions: string[],
  rawText: string,
  tags: string[] | [],
  text: string,
  title: string,
  ts?: number,
  updatedAt: string,
  updatedBy: string,
  views: string[],
  _id: string,
};

export type errorProps = {
  status?: number,
  statusText?: string,
  type?: string,
  url?: string,
  detail: string,
};

export type ChatsState = {
  chats: {
    data: {
      elements: ChatDataProps[],
      activeChat: ChatDataProps | null,
      searchChatRooms: ChatDataProps[] | null,
      hasNextPage: boolean,
      pageNumber: number,
      pageSize: number,
      total: number,
      allChatRoomsIds: string[],
      unreadMessages: ChatMessageWithMoreDataProps[] | null,
    },
    loading: boolean,
    error: null | errorProps,
  }
};

export type AppThunk = ThunkAction<void, ChatsState, unknown, Action<string>>;

export type UserData = {
  info: {
    profile: {
      firstName?: string,
      lastName?: string,
      avatar?: {
        secureUrl?: string | null,
      } | null,
    },
    _id?: string | null,
  },
  isStaff: boolean,
  isDev: boolean,
  id: string,
}
