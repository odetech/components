/* eslint-disable no-underscore-dangle */
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ref, onValue } from 'firebase/database';
import { useObjectVal } from 'react-firebase-hooks/database';
import Chat from './chat-component';
import {
  createChat,
  updateActiveChat,
  fetchChats,
  getMoreChatMessages,
  deleteChatMessage,
  editChatMessage,
  editChat,
  leaveOrArchiveChatRoom,
  convertChatRoomToPrivateChannel,
  firebaseMessagesDataUpdate,
} from './redux/chats';
import { ChatMessageWithMoreDataProps, ChatsState, UserData } from './types';
import { searchChatRooms, searchGetMoreChatMessages } from './redux/chats/chatsThunks';
import { ChatContext } from './hooks';
import ChatButton from './chat-component/components/chat-button';

export type ChannelsChatProps = {
  setProfilePreviewUserData: (isProfileCardOpen: UserData['info'] | null) => void,
  activeChatRoomId?: string | null,
  highlightMessageId?: string | null,
  width?: number,
  isChatOpen: boolean,
  setIsChatOpen: (isChatOpen: boolean) => void,
  isChatPage?: boolean,
  fetchTags?: any,
  api: any,
  userData: UserData,
  tagsLoading: boolean,
  app: string,
  isStaffMode: boolean,
  appErrorHandler?: (
    error: any,
    options?: {
      userRoles?: {
        _id?: string,
        isStaff?: boolean,
        isDev?: boolean,
      } | null,
      noToast?: boolean,
      toastMessage?: string,
    },
  ) => void,
  chatContainerZIndex?: number,
  appSearchChatRoomData?: {
    userFirstname: string,
    userId: string,
  } | null,
  setAppSearchChatRoomData?: (
    appSearchChatRoomData: {
      userFirstname: string,
      userId: string,
    } | null,
  ) => void,
  clearQuery?: () => void,
  dbFirebase: any,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
  incomingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
  outgoingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
  withPreviewChatButton?: boolean,
};

export const ChannelsChat = ({
  setProfilePreviewUserData, activeChatRoomId, highlightMessageId, width, isChatOpen, setIsChatOpen, isChatPage,
  userData, fetchTags, tagsLoading, app, api, isStaffMode, appErrorHandler, chatContainerZIndex,
  appSearchChatRoomData, setAppSearchChatRoomData, clearQuery, dbFirebase, networkConnections,
  onMentionClick, onHashtagClick, incomingNetworks, outgoingNetworks,
  withPreviewChatButton = true,
}: ChannelsChatProps) => {
  const dispatch = useDispatch();

  const [chatRoomHighlightMessageId, setChatRoomHighlightMessageId] = useState(highlightMessageId || null);
  const [isFirebaseCanBeUsed, setIsFirebaseCanBeUsed] = useState(false);

  const {
    data: chatsData,
    loading: chatsLoading,
  } = useSelector((state: ChatsState) => state.chats);

  useEffect(() => {
    setChatRoomHighlightMessageId(highlightMessageId || null);
  }, [highlightMessageId]);

  useEffect(() => {
    if (!chatsLoading && !isFirebaseCanBeUsed) {
      setIsFirebaseCanBeUsed(true);
    }
  }, [chatsLoading]); // check for a new chat rooms

  // Check for new chat rooms (private chat tags) !!!
  // const [firebaseUserTags]: any = useObjectVal(
  //   ref(dbFirebase, "/tags"),
  // );
  //
  // useEffect(() => {
  //   const firebaseUserTagsData = firebaseUserTags && Object.keys(firebaseUserTags).map((key) => (key));
  //   if (chatsData.allChatRoomsIds.length) {
  //     dispatch(fetchAllChatRoomsIds(
  //       {
  //         userId: userData.id,
  //       },
  //       api,
  //     ));
  //   }
  // }, [firebaseUserTags]);
  useEffect(() => {
    if (isFirebaseCanBeUsed && dbFirebase) {
      chatsData.allChatRoomsIds.forEach((chatRoomId) => {
        // Add message
        const firebaseTagLastMessagesRef = ref(dbFirebase, `/tags/${chatRoomId}/last_message`).ref;
        // @ts-ignore
        const repoListens = firebaseTagLastMessagesRef?._repo?.persistentConnection_?.listens;
        const repoListensCopy = [...repoListens];
        if (repoListensCopy && !repoListensCopy
          .find((listen: any[]) => (listen[0] === `/tags/${chatRoomId}/last_message`))) {
          onValue(firebaseTagLastMessagesRef, (snapshot) => {
            const data = JSON.parse(snapshot.val());
            // Add 'unread' parameter check!!!
            dispatch(firebaseMessagesDataUpdate(
              {
                newMessageData: data,
                chatRoomId,
                type: 'addMessage',
              },
              userData.id,
              api,
              app,
            ));
          });
        }

        // Update message
        const firebaseTagLastUpdatedMessagesRef = ref(dbFirebase, `/tags/${chatRoomId}/last_updated_message`).ref;
        if (repoListensCopy && !repoListensCopy
          .find((listen: any[]) => (listen[0] === `/tags/${chatRoomId}/last_updated_message`))) {
          onValue(firebaseTagLastUpdatedMessagesRef, (snapshot) => {
            const data = JSON.parse(snapshot.val());
            dispatch(firebaseMessagesDataUpdate(
              {
                newMessageData: data,
                chatRoomId,
                type: 'updateMessage',
              },
              userData.id,
              api,
              app,
            ));
          });
        }

        // Delete message
        const firebaseTagLastDeletedMessagesRef = ref(dbFirebase, `/tags/${chatRoomId}/last_deleted_message`).ref;
        if (repoListensCopy && !repoListensCopy
          .find((listen: any[]) => (listen[0] === `/tags/${chatRoomId}/last_deleted_message`))) {
          onValue(firebaseTagLastDeletedMessagesRef, (snapshot) => {
            try {
              const data = JSON.parse(snapshot.val());
              dispatch(firebaseMessagesDataUpdate(
                {
                  newMessageData: data,
                  chatRoomId,
                  type: 'deleteMessage',
                },
                userData.id,
                api,
                app,
              ));
            } catch (e) {
              console.log('Deleted message:', snapshot.val());
              console.error(e);
            }
          });
        }
      });
    }
  }, [chatsData.allChatRoomsIds.length, isFirebaseCanBeUsed, dbFirebase]);

  // favicon update logic
  useEffect(() => {
    const favicon: any = document.getElementById('favicon');

    if (favicon) {
      if (chatsData.unreadMessages?.length) {
        favicon.href = `${process.env.PUBLIC_URL}/img/main/OdeSocialIcon.svg`;
      } else {
        favicon.href = `${process.env.PUBLIC_URL}/img/main/OdeSocialIcon.svg`;
      }
    }
  }, [chatsData.unreadMessages]);

  const onMessageAvatarClick = useCallback((messageUserData: UserData['info'] | null) => {
    setProfilePreviewUserData(messageUserData);
  }, []);

  const createChatRequest = useCallback((
    requestData: {
      title: string,
      members: string[],
    },
    setShowModal?: (showModal: boolean) => void,
  ) => {
    dispatch(createChat(requestData, userData, api, setShowModal, appErrorHandler));
  }, []);

  const getMoreChatRoomMessagesRequest = useCallback((chatRoomId: string) => {
    dispatch(getMoreChatMessages(chatRoomId, api));
  }, []);

  const deleteChatRoomMessageRequest = (deleteMessageData: ChatMessageWithMoreDataProps) => {
    if (chatsData.activeChat) {
      dispatch(deleteChatMessage(deleteMessageData._id, chatsData.activeChat._id, api, app, false));
    }
  };

  const editChatRoomMessageRequest = (messageNewData: ChatMessageWithMoreDataProps) => {
    if (chatsData.activeChat) {
      dispatch(editChatMessage(
        {
          messageNewData,
          currentUserId: userData.id,
        },
        {
          api,
          app,
        },
      ));
    }
  };

  const searchChatRoomsRequest = (
    {
      chatSearchType,
      chatSearchInputValue,
    }: {
    chatSearchType: string,
    chatSearchInputValue: string,
  },
  ) => {
    dispatch(searchChatRooms(
      {
        chatSearchType,
        chatSearchInputValue,
        currentUserId: userData.id,
        secondMemberId: appSearchChatRoomData?.userId || undefined,
      },
      api,
    ));
  };

  const searchGetMoreChatMessagesRequest = (
    {
      tagId,
      messageCreatedAtDate,
    }: {
      tagId: string,
      messageCreatedAtDate: string,
  },
  ) => {
    dispatch(searchGetMoreChatMessages(
      {
        tagId,
        messageCreatedAtDate,
      },
      api,
    ));
  };

  const updateActiveChatData = useCallback((chatId: string | undefined) => {
    dispatch(updateActiveChat(chatId));
  }, []);

  const editChatRoomDataRequest = useCallback((
    updatedChatData: {
      title?: string,
      members: string[],
      _id: string,
    },
  ) => {
    dispatch(editChat(updatedChatData, userData.id, api));
  }, []);

  const leaveOrArchiveChatRoomRequest = useCallback((
    updatedChatId: string,
    isArchive?: boolean,
  ) => {
    dispatch(leaveOrArchiveChatRoom(updatedChatId, userData.id, api, isArchive));
  }, []);

  const convertChatRoomRequest = useCallback((
    updatedChatRoomId: string,
  ) => {
    dispatch(convertChatRoomToPrivateChannel(updatedChatRoomId, api, fetchTags));
  }, []);

  const fetchChatDataRequest = useCallback(() => {
    if (!chatsLoading && !tagsLoading) {
      dispatch(fetchChats(
        {
          userId: userData.id,
          limit: 15,
          pageNumber: 1,
        },
        api,
      ));
    }
  }, []);

  let firebaseIncomingNetworksData: undefined | {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[];

  if (!incomingNetworks) {
    const [firebaseIncomingNetworks]: any = useObjectVal(dbFirebase && userData?.id
      ? ref(dbFirebase, `/networks/${userData.id}/incoming`) : null);

    firebaseIncomingNetworksData = firebaseIncomingNetworks
      ? Object.keys(firebaseIncomingNetworks).map((key) => ({
        _id: key,
        ...JSON.parse(firebaseIncomingNetworks[key]),
      })) : [];
  }

  let firebaseOutgoingNetworksData: undefined | {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[];

  if (!outgoingNetworks) {
    const [firebaseOutgoingNetworks]: any = useObjectVal(dbFirebase && userData?.id
      ? ref(dbFirebase, `/networks/${userData.id}/outgoing`) : null);

    firebaseOutgoingNetworksData = firebaseOutgoingNetworks
      ? Object.keys(firebaseOutgoingNetworks).map((key) => ({
        _id: key,
        ...JSON.parse(firebaseOutgoingNetworks[key]),
      })) : [];
  }

  return (
    <ChatContext.Provider value={{ api, app, appErrorHandler }}>
      {withPreviewChatButton && (
        <ChatButton isChatOpen={isChatOpen} setIsChatOpen={setIsChatOpen} />
      )}
      <Chat
        isChatOpen={isChatOpen}
        setIsChatOpen={setIsChatOpen}
        fetchChatDataRequest={fetchChatDataRequest}
        isChatLoading={chatsLoading}
        chatData={chatsData}
        onMessageAvatarClick={onMessageAvatarClick}
        activeChatRoomId={activeChatRoomId}
        chatRoomHighlightMessageId={chatRoomHighlightMessageId}
        createChatRequest={createChatRequest}
        updateActiveChatData={updateActiveChatData}
        getMoreChatRoomMessagesRequest={getMoreChatRoomMessagesRequest}
        deleteChatRoomMessageRequest={deleteChatRoomMessageRequest}
        editChatRoomMessageRequest={editChatRoomMessageRequest}
        editChatRoomDataRequest={editChatRoomDataRequest}
        leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
        convertChatRoomRequest={convertChatRoomRequest}
        searchChatRoomsRequest={searchChatRoomsRequest}
        searchGetMoreChatMessagesRequest={searchGetMoreChatMessagesRequest}
        setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
        currentUserData={userData}
        isStaffMode={isStaffMode}
        width={width}
        isChatPage={isChatPage}
        app={app}
        chatContainerZIndex={chatContainerZIndex}
        appSearchChatRoomData={appSearchChatRoomData}
        setAppSearchChatRoomData={setAppSearchChatRoomData}
        clearQuery={clearQuery}
        networkConnections={networkConnections}
        onMentionClick={onMentionClick}
        onHashtagClick={onHashtagClick}
        incomingNetworks={incomingNetworks || firebaseIncomingNetworksData}
        outgoingNetworks={outgoingNetworks || firebaseOutgoingNetworksData}
      />
    </ChatContext.Provider>
  );
};
