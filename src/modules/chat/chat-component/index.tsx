import React, { useState, useEffect, Fragment } from 'react';
import NewChatModal from './components/new-chat-modal';
import ChatRoom from './components/chat-room';
import ChatRoomsList, { searchSubmit } from './components/chat-rooms-list';
import ChatContainer from './components/chat-container';
import { ChatMessageWithMoreDataProps, ChatDataProps, UserData } from '../types';
import { ChatPageWrapper } from './ChannelsChatStyles';
// import { useChat } from '../hooks';

type ChatProps = {
  fetchChatDataRequest: () => void,
  isChatLoading: boolean,
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
    searchChatRooms: ChatDataProps[] | null,
    hasNextPage: boolean,
    pageNumber: number,
    pageSize: number,
    total: number,
  },
  onMessageAvatarClick: (userData: UserData['info'] | null) => void,
  activeChatRoomId?: string | null,
  chatRoomHighlightMessageId?: string | null,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  createChatRequest: (
    chatPayload: {
      title: string,
      members: string[],
    },
    setShowModal?: (showModal: boolean) => void,
  ) => void,
  updateActiveChatData: (chatId: string) => void,
  getMoreChatRoomMessagesRequest: (chatRoomId: string) => void,
  deleteChatRoomMessageRequest: (deleteMessageData: ChatMessageWithMoreDataProps) => void,
  editChatRoomMessageRequest: (messageNewData: ChatMessageWithMoreDataProps) => void,
  editChatRoomDataRequest: (
    updatedChatData: {
      title?: string,
      members: string[],
      _id: string,
    },
  ) => void,
  leaveOrArchiveChatRoomRequest: (
    updatedChatId: string,
    isArchive?: boolean,
  ) => void,
  convertChatRoomRequest: (
    updatedChatRoomId: string,
  ) => void,
  searchChatRoomsRequest: (
    {
      chatSearchType,
      chatSearchInputValue,
    }: {
      chatSearchType: string,
      chatSearchInputValue: string,
    }
  ) => void,
  searchGetMoreChatMessagesRequest: (
    {
      tagId,
      messageCreatedAtDate,
    }: {
      tagId: string,
      messageCreatedAtDate: string,
    }
  ) => void,
  currentUserData: UserData,
  isStaffMode: boolean,
  width?: number,
  isChatOpen: boolean,
  setIsChatOpen: (isChatOpen: boolean) => void,
  isChatPage?: boolean,
  app: string,
  chatContainerZIndex?: number,
  appSearchChatRoomData?: {
    userFirstname: string,
    userId: string,
  } | null,
  setAppSearchChatRoomData?: (
    appSearchChatRoomData: {
      userFirstname: string,
      userId: string,
    } | null,
  ) => void,
  clearQuery?: () => void,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
  incomingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
  outgoingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
};

const Chat = ({
  onMessageAvatarClick, activeChatRoomId, chatRoomHighlightMessageId, chatData, isChatLoading, createChatRequest,
  updateActiveChatData, fetchChatDataRequest, getMoreChatRoomMessagesRequest,
  deleteChatRoomMessageRequest, currentUserData, editChatRoomMessageRequest,
  isStaffMode, editChatRoomDataRequest, leaveOrArchiveChatRoomRequest, searchChatRoomsRequest,
  convertChatRoomRequest, width, isChatOpen, setIsChatOpen, isChatPage, searchGetMoreChatMessagesRequest,
  app, setChatRoomHighlightMessageId, chatContainerZIndex,
  appSearchChatRoomData, setAppSearchChatRoomData, clearQuery, networkConnections, onMentionClick, onHashtagClick,
  incomingNetworks, outgoingNetworks,
}: ChatProps) => {
  // const { api } = useChat();
  const [showNewChatModal, setShowNewChatModal] = useState(false);
  const [isMaximize, setIsMaximize] = useState<boolean>(false);
  const [scrollPosition, setScrollPosition] = useState<number|null>(null);
  // Chat search
  const [chatSearchInputValue, setChatSearchInputValue] = useState('');
  const [isAppSearchChatOver, setIsAppSearchChatOver] = useState(false);
  const [chatSearchType, setChatSearchType] = useState<string | 'chatroom' | 'message'>('chatroom');
  const chatRooms = chatData?.searchChatRooms || chatData?.elements || [];
  useEffect(() => {
    if ((isChatOpen || isChatPage) && !isChatLoading && chatData.elements.length === 0) {
      fetchChatDataRequest();
    }
  }, [isChatOpen, isChatPage]);

  useEffect(() => {
    if (appSearchChatRoomData?.userFirstname) {
      (async () => {
        await updateActiveChatData('');
        await setChatRoomHighlightMessageId(null);
        await setChatSearchInputValue(appSearchChatRoomData.userFirstname);
        await searchSubmit({
          searchChatRoomsRequest,
          chatSearchType,
          chatSearchInputValue: appSearchChatRoomData.userFirstname,
        });
        setIsAppSearchChatOver(true);
      })();
    }
  }, [appSearchChatRoomData]);

  // query params logic
  useEffect(() => {
    if (!!activeChatRoomId && !!chatRoomHighlightMessageId) {
      setIsChatOpen(true);
    }

    if (chatData.elements.length && !(chatData.activeChat && chatData.activeChat._id === activeChatRoomId)
      && chatRoomHighlightMessageId) {
      const updatedData = chatData.elements.find((item) => item._id === activeChatRoomId);

      if (updatedData) {
        updateActiveChatData(updatedData._id);
      }
    }

    // message search
    if (chatRoomHighlightMessageId && chatData.searchChatRooms && chatSearchType === 'message' && chatData.activeChat
      && !chatData.activeChat.data.messages?.data
        .find((chatroomMessage) => (chatroomMessage._id === chatRoomHighlightMessageId))) {
      searchGetMoreChatMessagesRequest({
        tagId: chatData.activeChat._id,
        messageCreatedAtDate: chatData.searchChatRooms?.find((chatRoom) => (
          chatRoom.messageSearchInfo
            && chatRoom.messageSearchInfo._id === chatRoomHighlightMessageId))?.messageSearchInfo?.createdAt || '',
      });
    }

    // reply click
    if (chatRoomHighlightMessageId && chatData.activeChat && !chatData.activeChat.data.messages?.data
      .find((chatroomMessage) => (chatroomMessage._id === chatRoomHighlightMessageId)) && !isChatLoading) {
      const messageDataWithAssociatedMessageData = chatData.activeChat.data.messages?.data
        .find((chatroomMessage) => (chatroomMessage.associatedMessageData
          && chatroomMessage.associatedMessageData._id === chatRoomHighlightMessageId));
      if (messageDataWithAssociatedMessageData?.associatedMessageData?.createdAt) {
        searchGetMoreChatMessagesRequest({
          tagId: chatData.activeChat._id,
          messageCreatedAtDate: messageDataWithAssociatedMessageData.associatedMessageData.createdAt,
        });
      }
    }
  }, [activeChatRoomId, chatRoomHighlightMessageId, chatData.elements.length]);

  return (
    <Fragment>
      {isChatOpen && !isChatPage && ( // Chat floating component
        <ChatContainer
          width={width}
          isMaximize={isMaximize}
          setIsMaximize={setIsMaximize}
          chatContainerZIndex={chatContainerZIndex}
        >
          {!chatData.activeChat && ( // All Chat Rooms list
            <ChatRoomsList
              chatData={chatData}
              setShowNewChatModal={setShowNewChatModal}
              setIsChatOpen={setIsChatOpen}
              isChatLoading={isChatLoading}
              updateActiveChatData={updateActiveChatData}
              isMaximize={isMaximize}
              setIsMaximize={setIsMaximize}
              chatSearchInputValue={chatSearchInputValue}
              setChatSearchInputValue={setChatSearchInputValue}
              chatSearchType={chatSearchType}
              setChatSearchType={setChatSearchType}
              chatRooms={chatRooms}
              searchChatRoomsRequest={searchChatRoomsRequest}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              currentUserData={currentUserData}
              appSearchChatRoomData={appSearchChatRoomData}
              setAppSearchChatRoomData={setAppSearchChatRoomData}
              createChatRequest={createChatRequest}
              isAppSearchChatOver={isAppSearchChatOver}
              setIsAppSearchChatOver={setIsAppSearchChatOver}
              scrollPosition={scrollPosition}
              setScrollPosition={setScrollPosition}
            />
          )}
          {chatData.activeChat && ( // Active Chat Room info
            <ChatRoom
              chatData={chatData}
              isChatLoading={isChatLoading}
              onMessageAvatarClick={onMessageAvatarClick}
              setIsChatOpen={setIsChatOpen}
              chatRoomHighlightMessageId={chatRoomHighlightMessageId}
              getMoreChatRoomMessagesRequest={getMoreChatRoomMessagesRequest}
              deleteChatRoomMessageRequest={deleteChatRoomMessageRequest}
              currentUserData={currentUserData}
              editChatRoomMessageRequest={editChatRoomMessageRequest}
              updateActiveChatData={updateActiveChatData}
              isStaffMode={isStaffMode}
              editChatRoomDataRequest={editChatRoomDataRequest}
              leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
              convertChatRoomRequest={convertChatRoomRequest}
              app={app}
              isMaximize={isMaximize}
              setIsMaximize={setIsMaximize}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              clearQuery={clearQuery}
              networkConnections={networkConnections}
              onMentionClick={onMentionClick}
              onHashtagClick={onHashtagClick}
              incomingNetworks={incomingNetworks}
              outgoingNetworks={outgoingNetworks}
            />
          )}
          {showNewChatModal && (
            <NewChatModal
              title="Start a chat"
              showModal={showNewChatModal}
              setShowModal={setShowNewChatModal}
              request={createChatRequest}
              currentUserFirstName={currentUserData.info.profile.firstName!}
              networkConnections={networkConnections}
              isLoading={isChatLoading}
            />
          )}
        </ChatContainer>
      )}
      {isChatPage && ( // Chat page
        <ChatPageWrapper>
          {!chatData.activeChat && ( // All Chat Rooms list
            <ChatRoomsList
              chatData={chatData}
              setShowNewChatModal={setShowNewChatModal}
              setIsChatOpen={setIsChatOpen}
              isChatLoading={isChatLoading}
              updateActiveChatData={updateActiveChatData}
              isChatPage={isChatPage}
              chatSearchInputValue={chatSearchInputValue}
              setChatSearchInputValue={setChatSearchInputValue}
              chatSearchType={chatSearchType}
              setChatSearchType={setChatSearchType}
              chatRooms={chatRooms}
              searchChatRoomsRequest={searchChatRoomsRequest}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              currentUserData={currentUserData}
              appSearchChatRoomData={appSearchChatRoomData}
              setAppSearchChatRoomData={setAppSearchChatRoomData}
              createChatRequest={createChatRequest}
              isAppSearchChatOver={isAppSearchChatOver}
              setIsAppSearchChatOver={setIsAppSearchChatOver}
              scrollPosition={scrollPosition}
              setScrollPosition={setScrollPosition}
            />
          )}
          {chatData.activeChat && ( // Active Chat Room info
            <ChatRoom
              chatData={chatData}
              isChatLoading={isChatLoading}
              onMessageAvatarClick={onMessageAvatarClick}
              setIsChatOpen={setIsChatOpen}
              chatRoomHighlightMessageId={chatRoomHighlightMessageId}
              getMoreChatRoomMessagesRequest={getMoreChatRoomMessagesRequest}
              deleteChatRoomMessageRequest={deleteChatRoomMessageRequest}
              currentUserData={currentUserData}
              editChatRoomMessageRequest={editChatRoomMessageRequest}
              updateActiveChatData={updateActiveChatData}
              isStaffMode={isStaffMode}
              editChatRoomDataRequest={editChatRoomDataRequest}
              leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
              convertChatRoomRequest={convertChatRoomRequest}
              isChatPage={isChatPage}
              app={app}
              setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
              clearQuery={clearQuery}
              networkConnections={networkConnections}
              onMentionClick={onMentionClick}
              onHashtagClick={onHashtagClick}
              incomingNetworks={incomingNetworks}
              outgoingNetworks={outgoingNetworks}
            />
          )}
          {showNewChatModal && (
            <NewChatModal
              title="Start a chat"
              showModal={showNewChatModal}
              setShowModal={setShowNewChatModal}
              request={createChatRequest}
              currentUserFirstName={currentUserData.info.profile.firstName!}
              networkConnections={networkConnections}
              isLoading={isChatLoading}
            />
          )}
        </ChatPageWrapper>
      )}
    </Fragment>
  );
};

export default Chat;
