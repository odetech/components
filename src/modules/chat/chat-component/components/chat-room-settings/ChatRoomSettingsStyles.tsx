import styled from '@emotion/styled';
import { transparentize } from 'polished';
import { Form } from 'formik';
import isPropValid from '@emotion/is-prop-valid';
import { Button } from '../../../../../components/default-components';
import { VerticalScrollbarStyles } from '../../../../../styles';

type ChatRoomSettingsWrapperProps = {
  isOpen: boolean,
};

export const ChatRoomSettingsWrapper = styled('div', {
  shouldForwardProp: isPropValid,
})<ChatRoomSettingsWrapperProps>`
  width: 100%;
  height: 100%;
  background: ${transparentize(0.8, '#000000')};
  z-index: ${({ isOpen }) => (isOpen ? '8' : '-1')};
  position: absolute;
  top: 0;
  border-radius: 0 0 10px 10px;
  transition: 0.6s;
`;

export const ChatRoomSettingsCardWrapper = styled('div')`
  width: 100%;
  height: 100%;
  position: relative;
  overflow-x: hidden;

  ${VerticalScrollbarStyles};
`;

type ChatRoomSettingsCardProps = {
  isOpen: boolean,
};

export const ChatRoomSettingsCard = styled('div', {
  shouldForwardProp: isPropValid,
})<ChatRoomSettingsCardProps>`
  width: calc(100% - 35px);
  min-height: 100%;
  padding: 25px 30px;
  background: #FFFFFF;
  position: absolute;
  right: ${({ isOpen }) => (isOpen ? '0' : '-365px')};
  transition: 0.5s;
  ${VerticalScrollbarStyles};
`;

export const ChatRoomSettingsForm = styled(Form)`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const ChatRoomSettingsSaveButton = styled(Button)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  border: 1px solid #F7942A;
  background: ${transparentize(0.8, '#F7942A')};
  color: #F7942A;
  margin: 0 0 30px 0;

  svg {
    margin: 0 10px 0 0;
    fill: #F7942A;
  }

  &:disabled {
    border-color: #BBBBBB;
    opacity: 0.5;
    background: #F0F0F0 !important;
    
    svg {
      fill: #9C9C9C;
    }
  }

  &:focus {
    background: ${transparentize(0.8, '#F7942A')};
  }

  &:hover {
    background: ${transparentize(0.9, '#F7942A')};
  }

  &:active {
    background: ${transparentize(0.7, '#F7942A')};
  }
`;

type ChatRoomSettingsButtonsWrapperProps = {
  isCreatedByThisUser?: boolean,
};

export const ChatRoomSettingsButtonsWrapper = styled('div')<ChatRoomSettingsButtonsWrapperProps>`
  margin: ${({ isCreatedByThisUser }) => (isCreatedByThisUser ? 'auto 0 8px 0' : 'auto 0 0 0')};
  display: flex;
  align-items: center;
  justify-content: space-between;

  button {
    font-weight: 500;
    font-size: 16px;
    line-height: 24px;
    padding: 10px 15px;
    width: 100%;
    white-space: nowrap;

    svg {
      width: 20px;
      height: 20px;
      margin: 0 5px 0 0;
    }

    &:not(:last-child) {
      margin: 0 20px 0 0;
    }
  }
`;

export const ChatRoomSettingsLeaveButton = styled(Button)`
  background: ${transparentize(0.8, '#9C9C9C')};
  color: #9C9C9C;

  svg {
    fill: #9C9C9C;
  }

  &:focus {
    background: ${transparentize(0.8, '#9C9C9C')};
  }
  
  &:hover {
    background: ${transparentize(0.9, '#9C9C9C')};
  }

  &:active {
    background: ${transparentize(0.7, '#9C9C9C')};
  }
`;

export const ChatRoomSettingsDeleteButton = styled(Button)`
  background: ${transparentize(0.8, '#F8000C')};
  color: #F8000C;

  svg {
    fill: #F8000C;
  }

  &:focus {
    background: ${transparentize(0.8, '#F8000C')};
  }

  &:hover {
    background: ${transparentize(0.9, '#F8000C')};
  }

  &:active {
    background: ${transparentize(0.7, '#F8000C')};
  }
`;

export const ChatRoomSettingsMembersListWrapper = styled('div')`
  margin: 0 0 30px 0;
  display: flex;
  flex-direction: column;
`;

export const ChatRoomSettingsMembersLabel = styled('p')`
  font-weight: 500;
  font-size: 12px;
  line-height: 18px;
  color: #9C9C9C;
  margin: 0 0 7px 0;
`;

type ChatRoomSettingsMembersListProps = {
  isEmpty: boolean,
};

export const ChatRoomSettingsMembersList = styled('div')<ChatRoomSettingsMembersListProps>`
  width: 100%;
  min-height: 42px;
  max-height: 198px;
  overflow: auto;
  border: 1px dashed #9C9C9C;
  box-sizing: border-box;
  border-radius: 6px;

  ${VerticalScrollbarStyles};

  ${({ isEmpty }) => isEmpty && `
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `}
`;

export const ChatRoomSettingsCloseButton = styled(Button)`
  margin: 0 0 0 auto;
  padding: 4px;

  svg {
    width: 20px;
    height: 20px;
    transform: rotate(45deg);
  }
`;

export const ChatRoomSettingsAlertCardWrapper = styled('div')`
  width: 100%;
  height: 100%;
  background: ${transparentize(0.8, '#000000')};
  position: relative;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 0 0 10px 10px;
`;

export const ChatRoomSettingsAlertCard = styled('div')`
  width: 328px;
  height: 258px;
  background: #FFFFFF;
  padding: 30px 20px 25px 20px;
  box-shadow: 0 2px 16px ${transparentize(0.75, '#000000')};
  border-radius: 6px;
`;

export const ChatRoomSettingsAlertCardTitle = styled('p')`
  text-align: center;
  font-weight: 500;
  font-size: 20px;
  line-height: 30px;
  color: #3B3C3B;
  margin: 0 0 5px 0;
`;

export const ChatRoomSettingsAlertCardDescription = styled('p')`
  text-align: center;
  font-size: 15px;
  line-height: 22px;
  color: #9C9C9C;
  margin: 0 0 30px 0;
`;

export const ChatRoomSettingsAlertCardButtonsWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  
  button {
    &:not(:last-child) {
      margin: 0 20px 0 0;
    }
  }
`;

export const ChatRoomSettingsFlexWrapper = styled('div')`
  display: flex;
`;
