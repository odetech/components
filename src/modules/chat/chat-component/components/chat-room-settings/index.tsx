/* eslint-disable no-underscore-dangle */
import React, { Fragment, MouseEvent, useState } from 'react';
import { Formik } from 'formik';
import { Avatar } from '~/index';
import { Select, Button } from '../../../../../components/default-components';
import newChatModalValidation from '../../../validation';
import {
  MemberButton,
  ChatTitleFormikInput, UserInfoWrapper,
  ChatTitleFormikInputWrapper, ErrorTextStyle,
} from '../../ChannelsChatStyles';
import {
  ChatRoomSettingsAlertCard,
  ChatRoomSettingsAlertCardButtonsWrapper,
  ChatRoomSettingsAlertCardDescription,
  ChatRoomSettingsAlertCardTitle,
  ChatRoomSettingsAlertCardWrapper,
  ChatRoomSettingsButtonsWrapper,
  ChatRoomSettingsCard,
  ChatRoomSettingsCardWrapper,
  ChatRoomSettingsCloseButton,
  ChatRoomSettingsDeleteButton,
  ChatRoomSettingsFlexWrapper,
  ChatRoomSettingsForm,
  ChatRoomSettingsLeaveButton,
  ChatRoomSettingsMembersLabel,
  ChatRoomSettingsMembersList,
  ChatRoomSettingsMembersListWrapper,
  ChatRoomSettingsSaveButton,
  ChatRoomSettingsWrapper,
} from './ChatRoomSettingsStyles';
import {
  ArchiveIcon, CheckCircleOutlineIcon, CopyIcon, PlusIcon,
} from '../../../icons';
import { ChatDataProps, UserData } from '../../../types';

export const ArchiveChatRoomInfo = ({
  setIsArchiveChatAlertOpen, leaveOrArchiveChatRoomRequest, chatData, setIsChatRoomSettingsOpen,
}: {
  setIsArchiveChatAlertOpen: (isArchiveChatAlertOpen: boolean) => void,
  leaveOrArchiveChatRoomRequest: (
    updatedChatId: string,
    isArchive?: boolean,
  ) => void,
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
  },
  setIsChatRoomSettingsOpen: (isOpen: boolean) => void,
}) => (
  <Fragment>
    <ChatRoomSettingsCloseButton
      view="text"
      onClick={() => {
        setIsArchiveChatAlertOpen(false);
      }}
    >
      <PlusIcon />
    </ChatRoomSettingsCloseButton>
    <ChatRoomSettingsAlertCardTitle>
      Archive Chat?
    </ChatRoomSettingsAlertCardTitle>
    <ChatRoomSettingsAlertCardDescription>
      You will be able to restore your chat rooms in a future version release.
    </ChatRoomSettingsAlertCardDescription>
    <ChatRoomSettingsAlertCardButtonsWrapper>
      <ChatRoomSettingsLeaveButton
        onClick={() => {
          setIsArchiveChatAlertOpen(false);
        }}
      >
        Cancel
      </ChatRoomSettingsLeaveButton>
      <Button
        color="pink"
        onClick={() => {
          leaveOrArchiveChatRoomRequest(chatData.activeChat?._id || '', true);
          setIsArchiveChatAlertOpen(false);
          setIsChatRoomSettingsOpen(false);
        }}
      >
        Archive Chat
      </Button>
    </ChatRoomSettingsAlertCardButtonsWrapper>
  </Fragment>
);

type ChatRoomSettingsProps = {
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
  },
  currentUserData: UserData,
  isOpen: boolean,
  setIsOpen: (isOpen: boolean) => void,
  editChatRoomDataRequest: (
    updatedChatData: {
      title?: string,
      members: string[],
      _id: string,
    },
  ) => void,
  leaveOrArchiveChatRoomRequest: (
    updatedChatId: string,
    isArchive?: boolean,
  ) => void,
  convertChatRoomRequest: (
    updatedChatRoomId: string,
  ) => void,
  isStaffMode: boolean,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
};

const ChatRoomSettings = ({
  chatData, currentUserData, isOpen, setIsOpen, editChatRoomDataRequest, leaveOrArchiveChatRoomRequest,
  convertChatRoomRequest, isStaffMode, networkConnections,
}: ChatRoomSettingsProps) => {
  const [isLeaveChatAlertOpen, setIsLeaveChatAlertOpen] = useState(false);
  const [isConvertChatAlertOpen, setIsConvertChatAlertOpen] = useState(false);
  const [isArchiveChatAlertOpen, setIsArchiveChatAlertOpen] = useState(false);

  return (
    <ChatRoomSettingsWrapper
      isOpen={isOpen}
    >
      <ChatRoomSettingsCardWrapper>
        <ChatRoomSettingsCard
          isOpen={isOpen}
        >
          {isOpen && (
            <Formik
              initialValues={{
                title: chatData.activeChat?.title || '',
                members: (chatData.activeChat?.data.members?.length !== 0 && chatData.activeChat?.data.members
                  ?.filter((item) => item._id !== currentUserData.id).map((memberData) => ({
                    value: JSON.stringify(memberData),
                    label: memberData ? `${memberData.profile.firstName} ${memberData.profile.lastName}` : 'Unknown',
                  }))) || [],
                selectValue: null,
                availableOptions: networkConnections?.networks?.map((item: {
                    _id: string,
                    profile: {
                      firstName: string,
                      lastName: string,
                    }
                  }) => ({
                  value: JSON.stringify(item),
                  label: `${item.profile.firstName} ${item.profile.lastName}`,
                })) || [],
              }}
              onSubmit={(values) => {
                let chatTitle = '';
                if (values.title === '' && currentUserData && currentUserData.info.profile
                  && currentUserData.info.profile.firstName) {
                  chatTitle = `${currentUserData.info.profile.firstName.charAt(0)}`;
                  values.members.forEach((item: {
                    value: string,
                    label: string,
                  }) => {
                    chatTitle = `${chatTitle}&${JSON.parse(item.value).profile.firstName.charAt(0)}`;
                  });
                } else {
                  chatTitle = values.title;
                }

                const updatedChatData: {
                  title?: string,
                  members: string[],
                  _id: string,
                } = {
                  _id: chatData.activeChat?._id || '',
                  members: values.members.map((item: {
                    value: string,
                    label: string,
                  }) => JSON.parse(item.value)._id),
                };
                if (chatData.activeChat?.title !== chatTitle) {
                  updatedChatData.title = chatTitle;
                }

                editChatRoomDataRequest(updatedChatData);
                setIsOpen(false);
              }}
              validate={newChatModalValidation}
            >
              {({
                values, setFieldValue, errors, initialValues,
              }) => (
                <ChatRoomSettingsForm>
                  {isStaffMode && (
                    <ChatRoomSettingsFlexWrapper>
                      <ChatRoomSettingsMembersLabel>
                        {`Chat room (Tag) ID: ${chatData.activeChat?._id}`}
                      </ChatRoomSettingsMembersLabel>
                      <Button
                        size="sm"
                        view="text"
                        color="black"
                        type="button"
                        onClick={() => {
                          navigator.clipboard.writeText(
                            chatData.activeChat?._id ? chatData.activeChat._id : '',
                          );
                        }}
                      >
                        <CopyIcon />
                      </Button>
                    </ChatRoomSettingsFlexWrapper>
                  )}
                  <ChatRoomSettingsCloseButton
                    view="text"
                    type="button"
                    onClick={() => {
                      setIsOpen(false);
                    }}
                  >
                    <PlusIcon />
                  </ChatRoomSettingsCloseButton>
                  <ChatTitleFormikInputWrapper>
                    <ChatTitleFormikInput
                      name="title"
                      label="Group Chat Title"
                      placeholder="Chat Title"
                      margin="0 0 15px 0"
                      errorText={errors.title}
                      disabled={!(!!chatData.activeChat && currentUserData.id === chatData.activeChat.createdBy)}
                    />
                  </ChatTitleFormikInputWrapper>
                  <Select
                    name="userSelect"
                    options={values.availableOptions.filter((item: {
                      label: string,
                      value: string,
                    }) => !values.members
                      .find((member: any) => (JSON.parse(item.value)._id === JSON.parse(member.value)._id)))}
                    onChange={(data) => {
                      setFieldValue('members', [...values.members, data]);
                    }}
                    value={values.selectValue}
                    placeholder="Add Chat Members"
                    closeMenuOnSelect={false}
                  />
                  <ChatRoomSettingsMembersLabel>
                    Group Chat Members
                  </ChatRoomSettingsMembersLabel>
                  <ChatRoomSettingsMembersListWrapper>
                    <ChatRoomSettingsMembersList
                      isEmpty={values.members.length === 0}
                    >
                      {values.members.length !== 0 ? values.members.map((member: {
                        value: string,
                        label: string,
                      }) => {
                        const memberValue = JSON.parse(member.value);
                        return (
                          <MemberButton
                            view="text"
                            type="button"
                            key={member.value}
                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                              e.preventDefault();
                              const memberIndex = values.members.findIndex((memberData: {
                                value: string,
                                label: string,
                              }) => memberData.value === member.value);
                              const updatedMembers = [...values.members]
                                .filter((item) => item !== values.members[memberIndex]);
                              setFieldValue('members', updatedMembers);
                            }}
                            disabled={!(!!chatData.activeChat
                              && currentUserData.id === chatData.activeChat.createdBy)
                            && !!(chatData.activeChat?.data.members?.find((item) => item._id === memberValue._id))}
                          >
                            <UserInfoWrapper>
                              <Avatar user={memberValue} size={28} />
                              <span>{member.label}</span>
                            </UserInfoWrapper>
                            <PlusIcon />
                          </MemberButton>
                        );
                      }) : (
                        'No selected members'
                      )}
                    </ChatRoomSettingsMembersList>
                    {/* @ts-ignore */}
                    {errors && errors.members && <ErrorTextStyle>{errors.members}</ErrorTextStyle>}
                  </ChatRoomSettingsMembersListWrapper>
                  <ChatRoomSettingsSaveButton
                    type="submit"
                    disabled={JSON.stringify(values) === JSON.stringify(initialValues)}
                  >
                    <CheckCircleOutlineIcon />
                    Save Changes
                  </ChatRoomSettingsSaveButton>
                  <ChatRoomSettingsButtonsWrapper
                    isCreatedByThisUser={!!chatData.activeChat
                    && currentUserData.id === chatData.activeChat.createdBy}
                  >
                    {/* TODO: Show two buttons in the future if there is a transfer of the admin role */}
                    {!(!!chatData.activeChat && currentUserData.id === chatData.activeChat.createdBy) && (
                      <ChatRoomSettingsLeaveButton
                        type="button"
                        onClick={() => {
                          setIsLeaveChatAlertOpen(true);
                        }}
                      >
                        Leave Chat
                      </ChatRoomSettingsLeaveButton>
                    )}
                    {!!chatData.activeChat && currentUserData.id === chatData.activeChat.createdBy && (
                      <ChatRoomSettingsDeleteButton
                        type="button"
                        onClick={() => {
                          setIsArchiveChatAlertOpen(true);
                        }}
                      >
                        <ArchiveIcon />
                        Archive Chat
                      </ChatRoomSettingsDeleteButton>
                    )}
                  </ChatRoomSettingsButtonsWrapper>
                  {!!chatData.activeChat && currentUserData.id === chatData.activeChat.createdBy && (
                    <ChatRoomSettingsDeleteButton
                      type="button"
                      onClick={() => {
                        setIsConvertChatAlertOpen(true);
                      }}
                    >
                      Convert to a channel
                    </ChatRoomSettingsDeleteButton>
                  )}
                </ChatRoomSettingsForm>
              )}
            </Formik>
          )}
        </ChatRoomSettingsCard>
        {isLeaveChatAlertOpen && (
          <ChatRoomSettingsAlertCardWrapper>
            <ChatRoomSettingsAlertCard>
              <ChatRoomSettingsCloseButton
                view="text"
                onClick={() => {
                  setIsLeaveChatAlertOpen(false);
                }}
              >
                <PlusIcon />
              </ChatRoomSettingsCloseButton>
              <ChatRoomSettingsAlertCardTitle>
                Leave Chat?
              </ChatRoomSettingsAlertCardTitle>
              <ChatRoomSettingsAlertCardDescription>
                You will no longer receive messages from this group chat
              </ChatRoomSettingsAlertCardDescription>
              <ChatRoomSettingsAlertCardButtonsWrapper>
                <ChatRoomSettingsLeaveButton
                  onClick={() => {
                    setIsLeaveChatAlertOpen(false);
                  }}
                >
                  Cancel
                </ChatRoomSettingsLeaveButton>
                <Button
                  color="pink"
                  onClick={() => {
                    leaveOrArchiveChatRoomRequest(chatData.activeChat?._id || '');
                    setIsLeaveChatAlertOpen(false);
                    setIsOpen(false);
                  }}
                >
                  Leave
                </Button>
              </ChatRoomSettingsAlertCardButtonsWrapper>
            </ChatRoomSettingsAlertCard>
          </ChatRoomSettingsAlertCardWrapper>
        )}
        {isArchiveChatAlertOpen && (
          <ChatRoomSettingsAlertCardWrapper>
            <ChatRoomSettingsAlertCard>
              <ArchiveChatRoomInfo
                setIsArchiveChatAlertOpen={setIsArchiveChatAlertOpen}
                leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
                chatData={chatData}
                setIsChatRoomSettingsOpen={setIsOpen}
              />
            </ChatRoomSettingsAlertCard>
          </ChatRoomSettingsAlertCardWrapper>
        )}
        {isConvertChatAlertOpen && (
          <ChatRoomSettingsAlertCardWrapper>
            <ChatRoomSettingsAlertCard>
              <ChatRoomSettingsCloseButton
                view="text"
                onClick={() => {
                  setIsConvertChatAlertOpen(false);
                }}
              >
                <PlusIcon />
              </ChatRoomSettingsCloseButton>
              <ChatRoomSettingsAlertCardTitle>
                Convert Chat?
              </ChatRoomSettingsAlertCardTitle>
              <ChatRoomSettingsAlertCardDescription>
                You will convert your chat room to private channel.
              </ChatRoomSettingsAlertCardDescription>
              <ChatRoomSettingsAlertCardButtonsWrapper>
                <ChatRoomSettingsLeaveButton
                  onClick={() => {
                    setIsConvertChatAlertOpen(false);
                  }}
                >
                  Cancel
                </ChatRoomSettingsLeaveButton>
                <Button
                  color="pink"
                  onClick={() => {
                    convertChatRoomRequest(chatData.activeChat?._id || '');
                    setIsConvertChatAlertOpen(false);
                    setIsOpen(false);
                  }}
                >
                  Convert Chat
                </Button>
              </ChatRoomSettingsAlertCardButtonsWrapper>
            </ChatRoomSettingsAlertCard>
          </ChatRoomSettingsAlertCardWrapper>
        )}
      </ChatRoomSettingsCardWrapper>
    </ChatRoomSettingsWrapper>
  );
};

export default ChatRoomSettings;
