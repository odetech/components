import React, { Fragment, useState } from 'react';
import 'react-image-lightbox/style.css';
import {
  OriginalReplyMessageItemsWrap,
} from '~/components/editor/slate-editor.styled';
import { ensureUtcTimestamp, getFormattedDate, getUserName } from '~/helpers';
import {
  PencilIcon, AlertCircleOutlineIcon, DeleteIcon, ArrowLeftTopIcon, ChevronDownIcon, CheckIcon, CheckAllIcon,
} from '../../../icons';
import ChatAvatar from '../chat-avatar';
import { ChatMessageWithMoreDataProps, ChatDataProps, UserData } from '../../../types';
import {
  ChatMessageWrapper,
  MessageContentWrapper,
  MessageUnreadWrapper,
  DateWrapper,
  ChatMessageContainer,
  MessageErrorWrap,
  ChatUserName,
  OriginalReplyMessageSmall,
  MessageBottom,
  MessageControlButton,
  MessageActionsDropdownToggle,
  MessageActionsDropdownMenuItem,
  MessageActionsDropdownMenu,
  MessageActionsDropdown,
  MessageActionsDropdownWrapper,
  MessageMainContainer,
  ChatOriginalReplyMessageWrap,
} from '../../ChannelsChatStyles';
// import { BeatLoader } from "react-spinners";
import { SlateReader } from '../../../../../components/editor';

type ChatRoomMessageProps = {
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
  },
  currentUserData: UserData,
  onMessageAvatarClick: (userData: UserData['info'] | null) => void,
  setEdit: (isEdit: any) => void,
  setReply: (isEdit: any) => void,
  setShowDeleteModal: (showDeleteModal: boolean) => void,
  setDeleteMessage: (deleteMessage: null | ChatMessageWithMoreDataProps) => void,
  setIsOpenLightbox: (isOpenLightbox: boolean) => void,
  setPhotoIndex: (photoIndex: number) => void,
  setImages: (images: string[]) => void,
  chatRoomHighlightMessageId?: string | null,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  message: ChatMessageWithMoreDataProps,
  setShowModal: (isShow: boolean) => void,
  setShouldScrollToTheLatestUpdatedData: (value: boolean) => void,
  setPdfFile: (pdfFile: string) => void,
  isStaffMode: boolean,
  showAvatar: boolean,
  isFirstUnreadMessage?: boolean,
  showDate: boolean,
  clearQuery?: () => void,
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
};

const ChatRoomMessage = ({
  chatData, currentUserData, onMessageAvatarClick, chatRoomHighlightMessageId, message, setEdit, setDeleteMessage,
  setShowDeleteModal, setIsOpenLightbox, setPhotoIndex, setImages, setShowModal, setPdfFile, isStaffMode,
  showAvatar, showDate, setReply, setChatRoomHighlightMessageId, clearQuery, onMentionClick, onHashtagClick,
  isFirstUnreadMessage, setShouldScrollToTheLatestUpdatedData,
}: ChatRoomMessageProps) => {
  const urlParams = new URLSearchParams(window.location.search);
  const isStaffFromQuery = urlParams.get('isStaff');
  const isCurrentUser = !!message && !!message.createdBy && message.createdBy === currentUserData.id;

  const chatMemberData = isCurrentUser ? currentUserData.info : chatData.activeChat?.data.members
    ?.find((member) => member._id === message.createdBy) || {
    _id: null,
    profile: {
      firstName: 'Unknown',
      lastName: 'User',
      avatar: null,
    },
  };

  const date = message?.createdAt && new Date(ensureUtcTimestamp(message.createdAt).replace('+00:00', ''));
  const status = message?.status;
  const [isActionsMenuOpen, setIsActionsMenuOpen] = useState(false);
  const isRead = isCurrentUser ? !!message?.views?.find((userId) => (userId !== currentUserData.id))
    : false;

  return (
    <Fragment>
      {showDate && date && (
        <DateWrapper>
          <span>
            {getFormattedDate(ensureUtcTimestamp(message?.createdAt), 'month')}
          </span>
        </DateWrapper>
      )}
      {isFirstUnreadMessage ? <MessageUnreadWrapper>Unread messages</MessageUnreadWrapper> : null}
      <ChatMessageContainer
        isHighlight={!!message && !!chatRoomHighlightMessageId && chatRoomHighlightMessageId === message._id}
      >
        <ChatMessageWrapper
          isCurrentUserMessage={isCurrentUser}
          showAvatar={showAvatar}
        >
          {showAvatar && (
            <ChatAvatar
              onMessageAvatarClick={onMessageAvatarClick}
              chatMemberData={chatMemberData}
              isCurrentUserAvatar={isCurrentUser}
              currentUserData={currentUserData}
            />
          )}
          <MessageContentWrapper
            isCurrentUser={isCurrentUser}
            showAvatar={showAvatar}
          >
            {showAvatar && !isCurrentUser && (
            <ChatUserName>
              {getUserName(chatMemberData)}
            </ChatUserName>
            )}
            {message.associatedId && (
            <ChatOriginalReplyMessageWrap
              onClick={async () => {
                if (clearQuery) {
                  clearQuery();
                }
                await setChatRoomHighlightMessageId(null);
                if (message?.associatedMessageData?._id) {
                  setShouldScrollToTheLatestUpdatedData(true);
                  setChatRoomHighlightMessageId(message.associatedMessageData._id);
                }
              }}
            >
              <OriginalReplyMessageItemsWrap>
                <ChatUserName isSmall>
                  {getUserName(message.associatedMessageData?.data?.createdBy)}
                </ChatUserName>
                <OriginalReplyMessageSmall>
                  {message.associatedMessageData?.previewText}
                </OriginalReplyMessageSmall>
              </OriginalReplyMessageItemsWrap>
            </ChatOriginalReplyMessageWrap>
            )}
            <MessageMainContainer>
              <SlateReader
                  // @ts-ignore
                value={message?.text || message?.value || ''}
                setImages={setImages}
                setIsOpenLightbox={setIsOpenLightbox}
                setPhotoIndex={setPhotoIndex}
                setShowModal={setShowModal}
                setPdfFile={setPdfFile}
                files={message?.data?.files || []}
                onMentionClick={onMentionClick}
                onHashtagClick={onHashtagClick}
              />
              {(isCurrentUser || (
                currentUserData.isStaff && Number(isStaffFromQuery) === 1
              ) || isStaffMode) && !(!!status || !message?.createdAt) && (
              <MessageActionsDropdownWrapper>
                <MessageActionsDropdown
                  isOpen={isActionsMenuOpen}
                  toggle={() => {
                    setIsActionsMenuOpen(!isActionsMenuOpen);
                  }}
                  backgroundColor={isCurrentUser ? '#FBE3E8' : '#F0F0F0'}
                >
                  <MessageActionsDropdownToggle
                    isOpen={isActionsMenuOpen}
                  >
                    <ChevronDownIcon />
                  </MessageActionsDropdownToggle>
                  <MessageActionsDropdownMenu>
                    <Fragment>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          setEdit(message);
                        }}
                      >
                        <PencilIcon />
                        Edit
                      </MessageActionsDropdownMenuItem>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          setShowDeleteModal(true);
                          if (chatData.activeChat) {
                            setDeleteMessage(message || null);
                          }
                        }}
                      >
                        <DeleteIcon />
                        Delete
                      </MessageActionsDropdownMenuItem>
                    </Fragment>
                    {currentUserData.isStaff && Number(isStaffFromQuery) === 1 && (
                    <Fragment>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          navigator.clipboard
                            .writeText(message?._id || '');
                        }}
                      >
                        Copy message ID
                      </MessageActionsDropdownMenuItem>
                      <MessageActionsDropdownMenuItem
                        onClick={() => {
                          navigator.clipboard
                            .writeText(chatData.activeChat?._id || '');
                        }}
                      >
                        Copy Chat Room ID
                      </MessageActionsDropdownMenuItem>
                    </Fragment>
                    )}
                  </MessageActionsDropdownMenu>
                </MessageActionsDropdown>
              </MessageActionsDropdownWrapper>
              )}
            </MessageMainContainer>
            <MessageBottom
              isCurrentUser={isCurrentUser}
            >
              {(status || !message?.createdAt) ? (
                <div style={{
                  display: 'flex',
                }}
                >
                  {/* <div style={{ margin: "0 4px" }}> */}
                  {/*  <BeatLoader color="#E54367" size={3} /> */}
                  {/* </div> */}
                  <div>
                    {(status || !message?.createdAt) && !isRead && <CheckIcon />}
                  </div>
                </div>
              ) : (
                <MessageControlButton
                  type="button"
                  onClick={() => setReply(message)}
                >
                  <ArrowLeftTopIcon />
                  <span>Reply</span>
                </MessageControlButton>
              )}
              {date && (
              <div style={{ display: 'flex' }}>
                {isCurrentUser && (
                <span
                  style={{
                    fontSize: '10px',
                    fontWeight: 300,
                    whiteSpace: 'nowrap',
                    margin: '4px 2px 0 auto',
                  }}
                >
                  {isRead && !(status || !message?.createdAt) && <CheckAllIcon fill="#E54367" />}
                  {!(status || !message?.createdAt) && !isRead && <CheckAllIcon />}
                </span>
                )}
                <span
                  style={{
                    fontSize: '10px',
                    fontWeight: 300,
                    whiteSpace: 'nowrap',
                    margin: '5px 0 0 auto',
                  }}
                >
                  {getFormattedDate(ensureUtcTimestamp(message.createdAt).replace('+00:00', ''), 'time')}
                </span>
              </div>
              )}
            </MessageBottom>
          </MessageContentWrapper>
          {message.error && (
            <MessageErrorWrap>
              <AlertCircleOutlineIcon />
            </MessageErrorWrap>
          )}
        </ChatMessageWrapper>
      </ChatMessageContainer>
    </Fragment>
  );
};

export default ChatRoomMessage;
