import React, {
  Fragment, useEffect, useMemo, useRef,
} from 'react';
import { Form, Formik } from 'formik';
import {
  useDispatch,
} from 'react-redux';
import { extractHTMLContent } from '~/helpers';
import { dataURLtoFile } from '~/components/editor/slate-editor.image-helpers';
import { AutoSaveDraftMessageInChatRoom } from '../../../helpers';
import {
  ChatRoomEditorWrapper,
} from '../../ChannelsChatStyles';
import {
  ChatDataProps, UserData,
  ChatMessageWithMoreDataProps, MessageWithMoreDataProps,
} from '../../../types';
import { SlateEditor, getMentionsFromValue } from '../../../../../components/editor';
import {
  addMessageToQueue,
  // requestMessageFailure,
  sendChatMessage,
  sendChatReply,
} from '../../../redux/chats';
import { useChat } from '../../../hooks';

type ValueProps = {
  message: string,
  files: { file: File, uiId: string }[],
  uiIds: string[],
  associatedId?: string | undefined,
}

type SuggestionProps = {
  _id: string,
  profile: {
    firstName: string,
    lastName: string,
  }
}

type ChatMessageFormProps = {
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
  },
  onMessageSending?: () => void,
  setEdit: (a: any) => void,
  isEdit: any | null,
  setReply: (a: any) => void,
  replyData: any | null,
  editChatRoomMessageRequest: (messageNewData: ChatMessageWithMoreDataProps) => void,
  setIsOpenLightbox: (isOpenLightbox: boolean) => void,
  setPhotoIndex: (photoIndex: number) => void,
  setImages: (images: string[]) => void,
  queue: MessageWithMoreDataProps[],
  userData: UserData,
  app: string,
};

const ChatMessageForm = ({
  onMessageSending, setEdit, isEdit, chatData, editChatRoomMessageRequest, queue,
  setImages, setPhotoIndex, setIsOpenLightbox, setReply, replyData, userData, app,
}: ChatMessageFormProps) => {
  const filesRef = useRef<any>({});

  const dispatch = useDispatch();
  const { api } = useChat();
  // const connected = useSelector((state: any) => state.websocket.connected);

  // useEffect(() => {
  //   if (!connected) {
  //     queue.forEach((message) => {
  //       dispatch(requestMessageFailure(message._id));
  //     });
  //   }
  // }, [connected]);

  const { suggestions, userIds } = useMemo<{ suggestions: SuggestionProps[], userIds: string[] }>(() => {
    const suggestionsData = chatData.activeChat?.data.members || [];
    const userIdsData: string[] = suggestionsData.map((suggestion) => suggestion._id);
    return {
      suggestions: suggestionsData,
      userIds: userIdsData,
    };
  }, [chatData.activeChat?.data.members]);

  const onSubmit = (value: ValueProps) => {
    dispatch(addMessageToQueue(value, chatData.activeChat?._id, userData.id));
  };

  const sendMessage = async (item: MessageWithMoreDataProps) => {
    // @ts-ignore
    const { values } = item;

    const messageData = {
      text: values.message,
      rawText: extractHTMLContent(values.message),
      mentions: getMentionsFromValue(values.message, userIds),
      uuid: item._id,
      associatedId: values.associatedId,
    };

    if (chatData.activeChat) {
      if (messageData.associatedId) {
        dispatch(sendChatReply(
          chatData.activeChat._id,
          messageData,
          userData.id,
          api,
          // @ts-ignore
          values.files.filter((file) => values.uiIds.includes(file.uiId)),
          app,
        ));
      } else {
        dispatch(sendChatMessage(
          chatData.activeChat._id,
          messageData,
          userData.id,
          api,
          values.files.filter((file: any) => values.uiIds.includes(file.uiId)),
          app,
        ));
      }
    }
  };

  const filteredQueue = queue.filter((message) => !message.error);

  useEffect(() => {
    if (filteredQueue[0]?._id) {
      sendMessage(filteredQueue[0]);
    }
  }, [filteredQueue[0]?._id]);

  let draftData: {
    message: string,
    files: {
      file: File,
      uiId: string,
    }[],
    uiIds: string[],
  } | null = null;

  const chatRoomsDraftData = JSON.parse(localStorage.getItem('chatRoomsDraftData') || 'null');
  if (chatRoomsDraftData?.chatRoomsDrafts && chatData.activeChat) {
    const draftMessageChatRoomIndex = chatRoomsDraftData.chatRoomsDrafts
      .findIndex((
        chatRoom: {
          id: string,
          message: string,
          files: any[],
          uiIds: string[],
        },
      ) => (chatData.activeChat && chatRoom.id === chatData.activeChat._id));

    if (draftMessageChatRoomIndex !== undefined && draftMessageChatRoomIndex !== -1) {
      draftData = {
        ...chatRoomsDraftData.chatRoomsDrafts[draftMessageChatRoomIndex],
        files: chatRoomsDraftData.chatRoomsDrafts[draftMessageChatRoomIndex].files
          .map((fileData: {
            file: string,
            uiId: string,
            fileName: string,
          }) => ({
            file: dataURLtoFile(fileData.file, fileData.fileName),
            uiId: fileData.uiId,
            url: fileData.file,
          })),
      };
    }
  }

  if (chatData.activeChat?._id) {
    return (
      <Fragment
        key={(isEdit?._id) || 'chatEditor'}
      >
        <Formik
          initialValues={{
            message: isEdit ? isEdit.text || isEdit.value : (draftData?.message || ''),
            files: draftData?.files || [],
            uiIds: draftData?.uiIds || [],
          }}
          onSubmit={async (values: ValueProps, formikActions) => {
            if (isEdit) {
              const messageData = {
                ...isEdit,
                text: values.message,
                rawText: extractHTMLContent(values.message),
              };
              messageData.files = [...values.files];
              messageData.uiIds = [...values.uiIds];
              if (chatData.activeChat) {
                editChatRoomMessageRequest(messageData);
              }

              setEdit(null);
            } else {
              await onSubmit({
                ...values,
                associatedId: replyData ? replyData._id : undefined,
              });
              setReply(null);

              if (onMessageSending) {
                onMessageSending();
              }
            }
            formikActions.setFieldValue('message', '');
            formikActions.setFieldValue('files', []);
            formikActions.setFieldValue('uiIds', []);
          }}
        >
          {({ setFieldValue, values, handleSubmit }) => (
            <Form>
              {!isEdit && (
                <AutoSaveDraftMessageInChatRoom
                  chatRoomId={chatData.activeChat?._id || ''}
                />
              )}
              <ChatRoomEditorWrapper>
                <SlateEditor
                  setFieldValue={setFieldValue}
                  value={values.message}
                  valueFiles={values.files}
                  editValue={isEdit ? isEdit.text || isEdit.value : ''}
                  closeEdit={() => {
                    setEdit(null);
                    if (replyData) {
                      setReply(null);
                    }
                  }}
                  field="message"
                  handleSubmit={handleSubmit}
                  userSuggestions={suggestions}
                  isMinimal
                  isChannelMention
                  uploadImageCallBack={(file: File, uiId: string) => {
                    if (filesRef.current) filesRef.current[uiId] = ({ file, uiId });
                    setFieldValue('files', Object.values(filesRef.current));
                  }}
                  setImages={setImages}
                  setIsOpenLightbox={setIsOpenLightbox}
                  setPhotoIndex={setPhotoIndex}
                  files={isEdit?.data?.files ? isEdit.data.files : (draftData?.files || [])}
                  submitOnEnter
                  withAutoFocus
                  maxInputHeight={300}
                  replyData={replyData}
                />
              </ChatRoomEditorWrapper>
            </Form>
          )}
        </Formik>
      </Fragment>
    );
  }
  return null;
};

export default ChatMessageForm;
