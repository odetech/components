import React, {
  useEffect, useRef, useState,
} from 'react';
import { useDispatch } from 'react-redux';
import { BeatLoader } from 'react-spinners';
import { Virtuoso } from 'react-virtuoso';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import { DeleteModal } from '~/components/default-components';
import { ensureUtcTimestamp } from '~/helpers';
import ChatRoomHeader from '../chat-room-header';
import ChatMessageForm from '../chat-message-form';
import ChatRoomSettings from '../chat-room-settings';
import {
  ChatBody,
  CenterItemWrapper,
  MessagesWrapper,
  ChatItemsWrapper,
  ScrollToBottomButtonWrapper,
  ScrollToBottomButton,
  DeclineNetworkChatRoomButton,
  AcceptChatRoomButton,
  NetworkChatRoomItemsWrapper,
  NetworkChatRoomTextInfo,
  NetworkChatRoomTextUserInfo,
  NetworkChatRoomButtonsWrapper,
  ChatRoomHR,
  ArchiveChatRoomButton,
} from '../../ChannelsChatStyles';
import {
  ChatMessageWithMoreDataProps,
  ChatDataProps,
  UserData,
} from '../../../types';
import ChatRoomMessage from '../chat-room-message';
import { ArrowDownIcon } from '../../../icons';
import {
  ChatRoomSettingsAlertCardDescription,
  ChatRoomSettingsAlertCardTitle,
} from '../chat-room-settings/ChatRoomSettingsStyles';
import { networkAcceptInvitation, readMessages } from '../../../redux/chats/chatsThunks';
import { useChat } from '../../../hooks';

export const getStartDateTime = (date:string): number => new Date(new Date(date).setHours(0, 0, 0, 0)).getTime();

type ChatRoomProps = {
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
  },
  isChatLoading: boolean,
  onMessageAvatarClick: (userData: any | null) => void,
  setIsChatOpen: (isChatOpen: boolean) => void,
  chatRoomHighlightMessageId?: string | null,
  getMoreChatRoomMessagesRequest: (chatRoomId: string) => void,
  deleteChatRoomMessageRequest: (deleteMessageData: ChatMessageWithMoreDataProps) => void,
  editChatRoomMessageRequest: (messageNewData: ChatMessageWithMoreDataProps) => void,
  updateActiveChatData: (chatId: string) => void,
  editChatRoomDataRequest: (
    updatedChatData: {
      title?: string,
      members: string[],
      _id: string,
    },
  ) => void,
  currentUserData: UserData,
  isStaffMode: boolean,
  leaveOrArchiveChatRoomRequest: (
    updatedChatId: string,
    isArchive?: boolean,
  ) => void,
  convertChatRoomRequest: (
    updatedChatRoomId: string,
  ) => void,
  isChatPage?: boolean,
  app: string,
  isMaximize?: boolean,
  setIsMaximize?: (isMaximize: boolean) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  clearQuery?: () => void,
  networkConnections: {
    count: number,
    networks: {
      _id: string,
      profile: {
        firstName: string,
        lastName: string,
      }
    }[],
  },
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
  incomingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
  outgoingNetworks?: {
    createdAt: string,
    isPending: boolean,
    updatedAt: string,
    userId1: string,
    userId2: string,
    _id: string,
    user1?: UserData,
    user2?: UserData,
  }[],
};

const ChatRoom = ({
  onMessageAvatarClick, chatRoomHighlightMessageId, setIsChatOpen, chatData, isChatLoading,
  getMoreChatRoomMessagesRequest, deleteChatRoomMessageRequest, currentUserData, editChatRoomMessageRequest,
  updateActiveChatData, isStaffMode, editChatRoomDataRequest, leaveOrArchiveChatRoomRequest,
  convertChatRoomRequest, isChatPage, app, isMaximize, setIsMaximize,
  setChatRoomHighlightMessageId, clearQuery, networkConnections, onMentionClick, onHashtagClick,
  incomingNetworks, outgoingNetworks,
}: ChatRoomProps) => {
  const virtuoso = useRef(null);
  const dispatch = useDispatch();
  const { api } = useChat();

  const [isEdit, setEdit] = useState<any>(null);
  const [replyData, setReply] = useState<any>(null);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [deleteMessage, setDeleteMessage] = useState<null | ChatMessageWithMoreDataProps>(null);
  const [images, setImages] = useState<string[]>([]);
  const [isOpenLightbox, setIsOpenLightbox] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [showModal, setShowModal] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pdfFile, setPdfFile] = useState<string | null>(null);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [isChatRoomSettingsOpen, setIsChatRoomSettingsOpen] = useState(false);
  const [messages, setMessages] = useState(chatData?.activeChat?.data.messages?.data || []);
  const [isAtStart, setIsAtStart] = useState(true);
  const [isIncomingNetwork, setIsIncomingNetwork] = useState(false);
  const [isOutgoingNetwork, setIsOutgoingNetwork] = useState(false);
  const [isArchiveChatAlertOpen, setIsArchiveChatAlertOpen] = useState(false);
  // TODO: come up with another solution for scrolling?
  const [shouldScrollToTheLatestUpdatedData, setShouldScrollToTheLatestUpdatedData] = useState(false);
  // const [startIndex, setStartIndex] = useState(0);

  const rangeChanged = async (range: any) => {
    if (range.startIndex === 0 && !isChatLoading && !shouldScrollToTheLatestUpdatedData) {
      if (chatData.activeChat && chatData.activeChat.data.messages?.hasNextPage && !isChatLoading) {
        await getMoreChatRoomMessagesRequest(chatData.activeChat._id);
        setShouldScrollToTheLatestUpdatedData(true);
      }
    }
  };

  useEffect(() => {
    setTimeout(() => {
      if (chatRoomHighlightMessageId && virtuoso && virtuoso.current) {
        let chatRoomHighlightMessageIdIndex;
        if (chatRoomHighlightMessageId) {
          chatRoomHighlightMessageIdIndex = chatData.activeChat?.data.messages?.data
            .findIndex((article) => article._id === chatRoomHighlightMessageId);
        }
        if (chatRoomHighlightMessageIdIndex !== -1) {
          setTimeout(() => {
            setChatRoomHighlightMessageId(null);
          }, 4000);
        }
        // @ts-ignore
        virtuoso.current.scrollToIndex({
          index: chatRoomHighlightMessageIdIndex || 15,
          align: 'end',
          behavior: 'auto',
        });
      }
    }, 100);
  }, [chatRoomHighlightMessageId]);

  const scrollToFirstMessage = () => {
    if (virtuoso && virtuoso.current && chatData.activeChat) {
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: messages.length ? (messages.length) : 0,
        align: 'end',
        behavior: 'auto',
      });
    }
  };

  const getMessages = async () => {
    const newMessages = [...(chatData.activeChat?.data.messages?.data || [])];

    setMessages(newMessages);
  };

  useEffect(() => {
    getMessages();
    if (currentUserData.id && chatData.activeChat?.data?.unreadMessages?.length) {
      dispatch(readMessages(
        {
          currentUserId: currentUserData.id,
          messagesIds: chatData.activeChat.data.unreadMessages?.map((m) => m._id),
        },
        api,
        app,
      ));
    }
    // TODO: come up with another solution for scrolling?
    if (virtuoso && virtuoso.current && messages.length && shouldScrollToTheLatestUpdatedData) {
      let chatRoomHighlightMessageIdIndex;
      if (chatRoomHighlightMessageId) {
        chatRoomHighlightMessageIdIndex = chatData.activeChat?.data.messages?.data
          .findIndex((article) => article._id === chatRoomHighlightMessageId);
      }
      if (chatRoomHighlightMessageIdIndex !== -1) {
        setTimeout(() => {
          setChatRoomHighlightMessageId(null);
        }, 4000);
      }
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: chatRoomHighlightMessageIdIndex || 15,
        align: 'start',
        behavior: 'auto',
      });
      setShouldScrollToTheLatestUpdatedData(false);
    }
  }, [chatData?.activeChat?.data.messages?.data]);

  const queue = messages.filter((message) => (message?.status
    && message.status === 'creating'));

  useEffect(() => {
    if (!isIncomingNetwork) {
      const isOneMemberChatRoomUserConnected = !!(chatData.activeChat?.data.members
        && chatData.activeChat.data.members.length === 1 && networkConnections
        && networkConnections.networks.find((networkUserData) => (
          chatData.activeChat?.data.members && networkUserData._id === chatData.activeChat.data.members[0]._id
        )));

      const isIncomingNetworkCheck = !isOneMemberChatRoomUserConnected && !!(incomingNetworks
        && incomingNetworks.find((incomingNetwork) => (
          chatData.activeChat?.data.members && incomingNetwork.userId1 === chatData.activeChat.data.members[0]._id
        )));
      setIsIncomingNetwork(isIncomingNetworkCheck);

      const isOutgoingNetworkCheck = !isOneMemberChatRoomUserConnected && !!(outgoingNetworks
        && outgoingNetworks.find((outgoingNetwork) => (
          chatData.activeChat?.data.members && outgoingNetwork.userId2 === chatData.activeChat.data.members[0]._id
        )));
      setIsOutgoingNetwork(isOutgoingNetworkCheck);

      // Archive chat if there are no incoming request and there are only 2 members in the chat room
      if (chatData.activeChat?.data.members && chatData.activeChat.data.members.length === 1
        && !isOneMemberChatRoomUserConnected && !isIncomingNetworkCheck && !isOutgoingNetworkCheck) {
        setIsArchiveChatAlertOpen(true);
      }
    }
  }, [chatData.activeChat?.data.members, incomingNetworks, isIncomingNetwork, networkConnections, outgoingNetworks]);

  useEffect(() => {
    if ((chatData.activeChat?.data.members
      && chatData.activeChat.data.members.length === 1 && networkConnections
      && networkConnections.networks.find((networkUserData) => (
        chatData.activeChat?.data.members && networkUserData._id === chatData.activeChat.data.members[0]._id
      ))) || (chatData.activeChat?.data.members && chatData.activeChat.data.members.length > 1)) {
      setIsIncomingNetwork(false);
      setIsOutgoingNetwork(false);
    }
  }, [chatData.activeChat]);

  const renderMessage = (index: number) => {
    if (!messages[index]) return null;

    const messageDate = messages[index].createdAt
      ? getStartDateTime(ensureUtcTimestamp(messages[index].createdAt).replace('+00:00', ''))
      : new Date(new Date().setHours(0, 0, 0, 0)).getTime();

    const prevMessage = messages[index - 1] || {};
    const prevDate = ensureUtcTimestamp(prevMessage.createdAt)?.replace('+00:00', '');
    const isFirstForDay = !index || messageDate !== getStartDateTime(prevDate);
    const isFirstForUser = isFirstForDay || !index || messages[index].createdBy !== prevMessage.createdBy;
    return (
      <ChatRoomMessage
        showAvatar={isFirstForUser}
        showDate={isFirstForDay}
        chatData={chatData}
        currentUserData={currentUserData}
        message={messages[index]}
        setEdit={setEdit}
        setReply={setReply}
        setDeleteMessage={setDeleteMessage}
        setIsOpenLightbox={setIsOpenLightbox}
        setPhotoIndex={setPhotoIndex}
        setShowDeleteModal={setShowDeleteModal}
        onMessageAvatarClick={onMessageAvatarClick}
        chatRoomHighlightMessageId={chatRoomHighlightMessageId}
        setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
        setImages={setImages}
        setPdfFile={setPdfFile}
        setShowModal={setShowModal}
        isStaffMode={isStaffMode}
        clearQuery={clearQuery}
        onMentionClick={onMentionClick}
        setShouldScrollToTheLatestUpdatedData={setShouldScrollToTheLatestUpdatedData}
        onHashtagClick={onHashtagClick}
        isFirstUnreadMessage={
          chatData.activeChat?.data?.unreadMessages?.length
            ? chatData.activeChat.data.unreadMessages[0]._id === messages[index]._id
            : false
        }
      />
    );
  };

  return (
    <ChatItemsWrapper>
      <ChatRoomHeader
        chatData={chatData}
        setIsChatOpen={setIsChatOpen}
        setIsChatRoomSettingsOpen={setIsChatRoomSettingsOpen}
        isChatRoomSettingsOpen={isChatRoomSettingsOpen}
        updateActiveChatData={updateActiveChatData}
        isChatPage={isChatPage}
        isMaximize={isMaximize}
        setIsMaximize={setIsMaximize}
        setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
        clearQuery={clearQuery}
      />
      <ChatBody>
        {messages.length === 0 && (
          <CenterItemWrapper>
            {isChatLoading ? <BeatLoader color="#F7942A" /> : (
              <span>
                Type your message
              </span>
            )}
          </CenterItemWrapper>
        )}
        {messages.length !== 0 && (
          <MessagesWrapper>
            {/* {date && ( */}
            {/*  <MessagesDate> */}
            {/*    {format(date, date.getFullYear() === new Date().getFullYear() ? 'MMM do' : 'MMM do yyyy')} */}
            {/*  </MessagesDate> */}
            {/* )} */}
            <Virtuoso
              ref={virtuoso}
              computeItemKey={(_, data) => `chat-message-${data._id}`}
              initialTopMostItemIndex={messages.length ? (messages.length - 1) : 0}
              alignToBottom
              style={{
                overscrollBehavior: 'contain',
              }}
              totalCount={messages.length}
              data={chatData.activeChat?.data.messages?.data}
              rangeChanged={rangeChanged}
              itemContent={renderMessage}
              components={{
                // eslint-disable-next-line react/no-unstable-nested-components
                Header: chatData?.activeChat?.data.messages?.hasNextPage ? () => (
                  <div
                    style={{
                      padding: '4px',
                      display: 'flex',
                      justifyContent: 'center',
                    }}
                  >
                    <BeatLoader color="#F7942A" />
                  </div>
                ) : undefined,
              }}
              followOutput="auto"
              atBottomStateChange={(atBottom: boolean) => {
                setIsAtStart(atBottom);
              }}
            />
          </MessagesWrapper>
        )}
        <ScrollToBottomButtonWrapper>
          {!isAtStart && (
            <ScrollToBottomButton
              onClick={scrollToFirstMessage}
            >
              <ArrowDownIcon />
            </ScrollToBottomButton>
          )}
        </ScrollToBottomButtonWrapper>
        <ChatRoomHR />
        {isArchiveChatAlertOpen && (
          <NetworkChatRoomItemsWrapper>
            <ChatRoomSettingsAlertCardTitle>
              Archive Chat?
            </ChatRoomSettingsAlertCardTitle>
            <ChatRoomSettingsAlertCardDescription>
              <div>You are not connected and there is an incoming network connection request from this user.</div>
              <div>Do you want to archive this chat room?</div>
              <div>You will be able to restore your chat rooms in a future version release.</div>
            </ChatRoomSettingsAlertCardDescription>
            <NetworkChatRoomButtonsWrapper>
              <DeclineNetworkChatRoomButton
                disabled={isChatLoading}
                onClick={() => {
                  setIsArchiveChatAlertOpen(false);
                }}
              >
                Cancel
              </DeclineNetworkChatRoomButton>
              <ArchiveChatRoomButton
                color="pink"
                disabled={isChatLoading}
                onClick={async () => {
                  await leaveOrArchiveChatRoomRequest(chatData.activeChat?._id || '', true);
                  setIsArchiveChatAlertOpen(false);
                }}
              >
                Archive Chat
              </ArchiveChatRoomButton>
            </NetworkChatRoomButtonsWrapper>
          </NetworkChatRoomItemsWrapper>
        )}
        {(((!isIncomingNetwork && !isArchiveChatAlertOpen) || isOutgoingNetwork) || (
          chatData.activeChat?.data.members?.length && chatData.activeChat.data.members.length > 1
        )) && (
          <ChatMessageForm
            chatData={chatData}
            onMessageSending={scrollToFirstMessage}
            setEdit={setEdit}
            setReply={setReply}
            isEdit={isEdit}
            replyData={replyData}
            editChatRoomMessageRequest={editChatRoomMessageRequest}
            setImages={setImages}
            setIsOpenLightbox={setIsOpenLightbox}
            setPhotoIndex={setPhotoIndex}
            queue={queue}
            userData={currentUserData}
            app={app}
          />
        )}
        {(isIncomingNetwork && !isOutgoingNetwork && !isArchiveChatAlertOpen && chatData.activeChat
          && chatData.activeChat.data.members?.length
          && chatData.activeChat.data.members.length === 1
        ) && (
          <NetworkChatRoomItemsWrapper>
            <NetworkChatRoomTextInfo>
              <NetworkChatRoomTextUserInfo>
                {`${chatData.activeChat.data.members[0].profile.firstName} ${chatData.activeChat.data.members[0].profile
                  .lastName}`}
              </NetworkChatRoomTextUserInfo>
              would like to connect and chat
            </NetworkChatRoomTextInfo>
            <NetworkChatRoomButtonsWrapper>
              <DeclineNetworkChatRoomButton
                disabled={isChatLoading}
                onClick={async () => {
                  if (incomingNetworks && chatData.activeChat?.data.members) {
                    const incomingNetworkData = incomingNetworks.find((incomingNetwork) => (
                      chatData.activeChat?.data.members
                      && incomingNetwork.userId1 === chatData.activeChat.data.members[0]._id
                    ));
                    if (incomingNetworkData) {
                      // TODO: delete incoming network request in future?
                      // await dispatch(deleteNetwork(
                      //   {
                      //     networkId: incomingNetworkData._id,
                      //   },
                      //   api,
                      // ));
                      await leaveOrArchiveChatRoomRequest(chatData.activeChat?._id || '', true);
                    }
                  }
                }}
              >
                Decline
              </DeclineNetworkChatRoomButton>
              <AcceptChatRoomButton
                color="pink"
                disabled={isChatLoading}
                onClick={async () => {
                  if (incomingNetworks && chatData.activeChat?.data.members) {
                    const incomingNetworkData = incomingNetworks.find((incomingNetwork) => (
                      chatData.activeChat?.data.members
                      && incomingNetwork.userId1 === chatData.activeChat.data.members[0]._id
                    ));
                    if (incomingNetworkData) {
                      await dispatch(networkAcceptInvitation(
                        {
                          userId1: incomingNetworkData.userId1, // recipient UserId
                          userId2: incomingNetworkData.userId2, // sender UserId
                          _id: incomingNetworkData._id,
                        },
                        api,
                      ));
                      setIsIncomingNetwork(false);
                      setIsOutgoingNetwork(false);
                    }
                  }
                }}
              >
                Accept
              </AcceptChatRoomButton>
            </NetworkChatRoomButtonsWrapper>
          </NetworkChatRoomItemsWrapper>
        )}
        <ChatRoomSettings
          chatData={chatData}
          currentUserData={currentUserData}
          isOpen={isChatRoomSettingsOpen}
          setIsOpen={setIsChatRoomSettingsOpen}
          editChatRoomDataRequest={editChatRoomDataRequest}
          leaveOrArchiveChatRoomRequest={leaveOrArchiveChatRoomRequest}
          convertChatRoomRequest={convertChatRoomRequest}
          isStaffMode={isStaffMode}
          networkConnections={networkConnections}
        />
      </ChatBody>
      {showDeleteModal && (
        <DeleteModal
          showModal={showDeleteModal}
          setShowModal={(setShowModalData) => {
            setShowDeleteModal(setShowModalData);
            setDeleteMessage(null);
          }}
          onSubmit={() => {
            if (deleteMessage && chatData.activeChat) {
              deleteChatRoomMessageRequest(deleteMessage);
            }
          }}
          title="Warning"
          infoText="Are you sure you want to delete this message?"
        />
      )}
      {isOpenLightbox && (
        <Lightbox
          mainSrc={images[photoIndex]}
          nextSrc={images[(photoIndex + 1) % images.length]}
          prevSrc={images[(photoIndex + images.length - 1) % images.length]}
          onCloseRequest={() => setIsOpenLightbox(false)}
          onMovePrevRequest={() => setPhotoIndex((photoIndex + images.length - 1) % images.length)}
          onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % images.length)}
        />
      )}
      {/* <ArticlePdfModal */}
      {/*  showModal={showModal} */}
      {/*  setShowModal={setShowModal} */}
      {/*  pdfFile={pdfFile} */}
      {/* /> */}
    </ChatItemsWrapper>
  );
};

export default ChatRoom;
