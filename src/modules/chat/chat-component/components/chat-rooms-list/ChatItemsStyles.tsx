import styled from '@emotion/styled';

export const HeaderLeftSideWrapper = styled('div')`
  display: flex;
  align-items: center;
  
  span {
    margin: 0 5px 0 0;
  }

  button {
    padding: 5px;
    color: #FFFFFF;
    z-index: 1;

    svg {
      fill: #FFFFFF;
      width: 24px;
      height: 24px;
    }

    &:focus {
      color: #FFFFFF;
      background: transparent;

      svg {
        fill: #FFFFFF;
      }
    }

    &:hover, &:active {
      color: #E54367;
      background: #FFFFFF;

      svg {
        fill: #E54367;
      }
    }

    &:not(:last-of-type) {
      margin: 0 5px 0 0;
    }
  }
`;

export const HeaderButtonsWrapper = styled('div')`
  display: flex;
  align-items: center;

  button {
    padding: 5px;
    color: #FFFFFF;

    svg {
      fill: #FFFFFF;
      width: 24px;
      height: 24px;
    }

    &:hover, &:active, &:focus {
      color: #E54367;
      background: #FFFFFF;

      svg {
        fill: #E54367;
      }
    }

    &:not(:last-of-type) {
      margin: 0 5px 0 0;
    }
  }
`;
