import React, { Fragment, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { Virtuoso } from 'react-virtuoso';
import { BeatLoader } from 'react-spinners';
import {
  HeaderButtonsWrapper,
  HeaderLeftSideWrapper,
} from './ChatItemsStyles';
import {
  ChatHeader,
  ChatBody,
  MinimizeButton,
  ChatItemsWrapper,
  CenterItemWrapper,
  ChatHeaderDraggable,
  HeaderActionButton,
  ChatSearchInputWrapper,
  ChatSearchItemsWrapper,
  ChatSearchForButton, ChatSearchInputClearButton, ChatSearchInputSubmitButton,
} from '../../ChannelsChatStyles';
import {
  CloseIcon,
  MagnifyIcon,
  PlusBoxOutlineIcon, WindowMaximizeIcon, WindowMinimizeIcon, WindowRestoreIcon,
} from '../../../icons';
import ChatRoomButton from '../chat-room-button';
import { Input, Button } from '../../../../../components/default-components';
import { ChatDataProps, UserData } from '../../../types';
import { fetchChats } from '../../../redux/chats';
import { useChat } from '../../../hooks';

const virtuosoItemContent = (
  index: number,
  chatRooms: ChatDataProps[],
  openChatRoom: (chatRoomId: string) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
) => (
  <ChatRoomButton
    key={`${chatRooms[index]._id}-${index}`}
    chatRoomData={chatRooms[index]}
    updateActiveChatData={openChatRoom}
    setChatRoomHighlightMessageId={setChatRoomHighlightMessageId}
  />
);

export const searchSubmit = ({
  searchChatRoomsRequest,
  chatSearchType,
  chatSearchInputValue,
}: {
  // eslint-disable-next-line no-shadow
  searchChatRoomsRequest: ({ chatSearchType, chatSearchInputValue }: {
      chatSearchType: string,
      chatSearchInputValue: string,
    }
  ) => void,
  chatSearchType: string,
  chatSearchInputValue: string,
}) => {
  searchChatRoomsRequest({
    chatSearchType,
    chatSearchInputValue,
  });
};

const getMoreChatRooms = (
  requestParams: {
    userData: UserData,
    currentChatRoomsPage: number,
  },
  dispatch: any,
  api: any,
) => {
  const request = dispatch(fetchChats(
    {
      userId: requestParams.userData.id,
      limit: 15,
      pageNumber: requestParams.currentChatRoomsPage + 1,
      messagesPageNumber: 1,
      messagesLimit: 15,
    },
    api,
  ));

  return request;
};

type ChannelsChatProps = {
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
    searchChatRooms: ChatDataProps[] | null,
    hasNextPage: boolean,
    pageNumber: number,
    pageSize: number,
    total: number,
  },
  isChatLoading: boolean,
  setIsChatOpen: (b: boolean) => void,
  setShowNewChatModal: (b: boolean) => void,
  updateActiveChatData: (chatId: string) => void,
  isChatPage?: boolean,
  isMaximize?: boolean,
  setIsMaximize?: (isMaximize: boolean) => void,
  chatSearchInputValue: string,
  setChatSearchInputValue: (chatSearchInputValue: string) => void,
  chatSearchType: string,
  setChatSearchType: (chatSearchType: string) => void,
  chatRooms: ChatDataProps[],
  searchChatRoomsRequest: (
    {
      chatSearchType,
      chatSearchInputValue,
    }: {
      chatSearchType: string,
      chatSearchInputValue: string,
    }
  ) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  currentUserData: UserData,
  appSearchChatRoomData?: {
    userFirstname: string,
    userId: string,
  } | null,
  setAppSearchChatRoomData?: (
    appSearchChatRoomData: {
      userFirstname: string,
      userId: string,
    } | null,
  ) => void,
  createChatRequest: (
    chatPayload: {
      title: string,
      members: string[],
    },
    setShowModal?: (showModal: boolean) => void,
  ) => void,
  isAppSearchChatOver: boolean,
  setIsAppSearchChatOver: (isAppSearchChatOver: boolean) => void,
  scrollPosition: number | null;
  setScrollPosition: (value: number | null) => void;
};

const ChatRoomsList = ({
  setIsChatOpen, setShowNewChatModal, chatData, isChatLoading, updateActiveChatData,
  isChatPage, isMaximize, setIsMaximize, chatSearchInputValue, setChatSearchInputValue, chatSearchType,
  setChatSearchType, chatRooms, searchChatRoomsRequest, setChatRoomHighlightMessageId,
  currentUserData, appSearchChatRoomData, setAppSearchChatRoomData, createChatRequest, isAppSearchChatOver,
  setIsAppSearchChatOver, scrollPosition, setScrollPosition,
}: ChannelsChatProps) => {
  const { api } = useChat();
  const dispatch = useDispatch();
  const virtuoso = useRef(null);

  const scrollToFirstChatRoom = () => {
    if (virtuoso && virtuoso.current) {
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: 0,
        align: 'end',
        behavior: 'auto',
      });
    }
  };

  useEffect(() => {
    scrollToFirstChatRoom();
  }, [chatData?.searchChatRooms?.length]);

  useEffect(() => {
    if (virtuoso && virtuoso.current && scrollPosition !== null) {
      // @ts-ignore
      virtuoso.current.scrollToIndex({
        index: scrollPosition,
        align: 'end',
        behavior: 'auto',
      });
      setScrollPosition(null);
    }
  }, [virtuoso.current]);

  const openChatRoom = (chatRoomId: string) => {
    updateActiveChatData(chatRoomId);
    const chatIndex = chatRooms.findIndex((item: ChatDataProps) => item._id === chatRoomId);
    setScrollPosition(chatIndex);
  };

  useEffect(() => {
    // Create chat room if appSearchChatRoomData exists and searchChatRooms is empty
    if (chatData.searchChatRooms && chatData.searchChatRooms.length === 0
      && !isChatLoading && appSearchChatRoomData?.userFirstname && chatSearchInputValue
      && chatSearchInputValue === appSearchChatRoomData.userFirstname
      && currentUserData?.info?.profile?.firstName && setAppSearchChatRoomData && isAppSearchChatOver) {
      (async () => {
        // Add template message?
        await createChatRequest({
          title: `${currentUserData.info.profile.firstName}, ${appSearchChatRoomData.userFirstname}`,
          members: [appSearchChatRoomData.userId],
        });

        setIsAppSearchChatOver(false);

        setTimeout(() => {
          searchSubmit({
            searchChatRoomsRequest,
            chatSearchType,
            chatSearchInputValue,
          });
        }, 1000);
      })();
    }
  }, [chatData.searchChatRooms]);

  useEffect(() => {
    // open chat room if only one chatroom found via appSearchChatRoomData
    if (!isChatLoading && appSearchChatRoomData?.userFirstname && chatSearchInputValue
      && chatSearchInputValue === appSearchChatRoomData.userFirstname
      && setAppSearchChatRoomData) {
      if (chatRooms.length === 1) {
        updateActiveChatData(chatRooms[0]._id);
      }
      // Removing appSearchChatRoomData if search via appSearchChatRoomData is successful
      if (chatRooms.length !== 0) {
        setAppSearchChatRoomData(null);
      }
    }
  }, [chatData?.searchChatRooms]);

  return (
    <ChatItemsWrapper>
      <ChatHeader>
        <ChatHeaderDraggable className="chat-header" />
        <HeaderLeftSideWrapper>
          {!isChatPage && (
            <span>
              Direct Chats
            </span>
          )}
          <Button
            view="text"
            onClick={() => {
              setShowNewChatModal(true);
            }}
          >
            <PlusBoxOutlineIcon />
          </Button>
        </HeaderLeftSideWrapper>
        <HeaderButtonsWrapper>
          {!isChatPage && (
            <Fragment>
              <MinimizeButton
                view="text"
                onClick={() => {
                  setIsChatOpen(false);
                }}
              >
                <WindowMinimizeIcon />
              </MinimizeButton>
              {setIsMaximize && (
                <HeaderActionButton
                  view="text"
                  onClick={() => {
                    setIsMaximize(!isMaximize);
                  }}
                >
                  {isMaximize ? <WindowRestoreIcon /> : <WindowMaximizeIcon />}
                </HeaderActionButton>
              )}
            </Fragment>
          )}
        </HeaderButtonsWrapper>
      </ChatHeader>
      <ChatSearchItemsWrapper>
        <ChatSearchInputWrapper
          isInputEmpty={chatSearchInputValue === ''}
          onSubmit={(e) => {
            e.preventDefault();

            searchSubmit({
              searchChatRoomsRequest,
              chatSearchType,
              chatSearchInputValue,
            });
          }}
        >
          <Input
            name="chatSearchInput"
            placeholder="Search"
            value={chatSearchInputValue}
            onChange={(e) => {
              setChatSearchInputValue(e.target.value);
            }}
            color="red"
          />
          <ChatSearchInputClearButton
            onClick={() => {
              setChatSearchInputValue('');
              searchChatRoomsRequest({
                chatSearchType,
                chatSearchInputValue: '',
              });
              scrollToFirstChatRoom();
            }}
          >
            <CloseIcon />
          </ChatSearchInputClearButton>
          <ChatSearchInputSubmitButton
            color="pink"
            view="text"
            type="submit"
            disabled={isChatLoading}
          >
            {isChatLoading ? <BeatLoader color="#E54367" size={5} /> : <MagnifyIcon />}
          </ChatSearchInputSubmitButton>
        </ChatSearchInputWrapper>
        {chatSearchInputValue !== '' && (
          <ChatSearchForButton
            color="pink"
            view="text"
            onClick={() => {
              setChatSearchType(chatSearchType === 'chatroom' ? 'message' : 'chatroom');
            }}
          >
            {chatSearchType === 'chatroom' ? 'Search for messages' : 'Search for chat room'}
          </ChatSearchForButton>
        )}
      </ChatSearchItemsWrapper>
      <ChatBody>
        {chatRooms.length !== 0 && (
          <Virtuoso
            ref={virtuoso}
            style={{
              overscrollBehavior: 'contain',
            }}
            id="virtuoso-scroller"
            computeItemKey={(_, data) => `chat-room-${data._id}`}
            data={chatRooms}
            endReached={() => (!chatData.hasNextPage || isChatLoading
            || chatData.searchChatRooms ? () => ({}) : getMoreChatRooms(
                {
                  userData: currentUserData,
                  currentChatRoomsPage: chatData.pageNumber,
                },
                dispatch,
                api,
              ))}
            itemContent={(index) => (
              virtuosoItemContent(index, chatRooms, openChatRoom, setChatRoomHighlightMessageId))}
            components={{
              // eslint-disable-next-line react/no-unstable-nested-components
              Footer: chatData.hasNextPage && isChatLoading ? () => (
                <div
                  style={{
                    padding: '10px 4px 4px 4px',
                    display: 'flex',
                    justifyContent: 'center',
                  }}
                >
                  <BeatLoader color="#E54367" />
                </div>
              ) : undefined,
            }}
          />
        )}
        {chatRooms.length === 0 && (
          <CenterItemWrapper>
            {isChatLoading ? <BeatLoader color="#E54367" /> : (
              <span>No chats found yet</span>
            )}
          </CenterItemWrapper>
        )}
      </ChatBody>
    </ChatItemsWrapper>
  );
};

export default ChatRoomsList;
