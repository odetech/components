import React from 'react';
import { useSelector } from 'react-redux';
import { ChatsState } from '../../../types';
import { OpenChatButton, NewMessagesCircle } from './ChatButtonStyles';
import { MessageOutlineIcon } from '../../../icons';

type ChatButtonProps = {
  setIsChatOpen: (value: boolean) => void,
  isChatOpen: boolean,
};

const ChatButton = ({
  isChatOpen,
  setIsChatOpen,
}: ChatButtonProps) => {
  const unreadMessagesLength = useSelector((state: ChatsState) => state.chats.data.unreadMessages?.length || 0);
  if (!isChatOpen) {
    return (
      <OpenChatButton
        color="pink"
        onClick={() => {
          setIsChatOpen(true);
        }}
      >
        <MessageOutlineIcon />
        {unreadMessagesLength !== 0 && (
          <NewMessagesCircle>
            {unreadMessagesLength}
          </NewMessagesCircle>
        )}
      </OpenChatButton>
    );
  }
  return null;
};

export default ChatButton;
