import styled from '@emotion/styled';
import { defaultTheme } from '~/styles/themes';

export const OpenChatButton = styled('button')`
  position: fixed;
  bottom: 15px;
  right: 15px;
  z-index: 4;
  padding: 22px 20px;
  width: 66px;
  height: 66px;
  max-height: unset;
  border-radius: 50%;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
  background-color: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};
  display: flex;
  align-items: center;
  justify-content: center;
  border: unset;
  outline: unset;

  svg {
    position: absolute;
    width: 34px;
    height: 34px;
    fill: white;
  }
`;

export const NewMessagesCircle = styled('div')`
  position: absolute;
  top: 12px;
  right: 6px;
  width: 20px;
  height: 20px;
  background: ${({ theme }) => theme?.background?.surfaceContainer
   || defaultTheme.background.surfaceContainer};
  border-radius: 50%;
  border: 0.5px solid ${({ theme }) => theme?.brand?.secondaryContainer || defaultTheme.brand.secondaryContainer};
  box-sizing: border-box;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 10px;
  font-weight: 600;
  line-height: 15px;
  color: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};
`;
