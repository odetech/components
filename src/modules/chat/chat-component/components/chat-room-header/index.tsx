import React, { Fragment } from 'react';
import {
  ChatHeader, ChatHeaderDraggable,
  ChatRoomHeaderBackButton,
  ChatRoomHeaderTitle,
  HeaderFlexWrapper,
  HeaderLeftSideWrapper,
  HeaderActionButton,
  MinimizeButton,
} from '../../ChannelsChatStyles';
import {
  ArrowLeftIcon, TuneIcon, WindowMaximizeIcon, WindowMinimizeIcon, WindowRestoreIcon,
} from '../../../icons';
import { ChatDataProps } from '../../../types';

type ChatRoomHeaderProps = {
  chatData: {
    elements: ChatDataProps[],
    activeChat: ChatDataProps | null,
  },
  setIsChatRoomSettingsOpen: (isChatRoomSettingsOpen: boolean) => void,
  setIsChatOpen: (isChatOpen: boolean) => void,
  isChatRoomSettingsOpen: boolean,
  updateActiveChatData: (chatId: string) => void,
  isChatPage?: boolean,
  isMaximize?: boolean,
  setIsMaximize?: (isMaximize: boolean) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string | null) => void,
  clearQuery?: () => void,
};

const ChatRoomHeader = ({
  chatData, setIsChatOpen, setIsChatRoomSettingsOpen, isChatRoomSettingsOpen, setChatRoomHighlightMessageId,
  updateActiveChatData, isChatPage, isMaximize, setIsMaximize, clearQuery,
}: ChatRoomHeaderProps) => (
  <ChatHeader>
    <ChatHeaderDraggable className="chat-header" />
    <HeaderLeftSideWrapper>
      <ChatRoomHeaderBackButton
        view="text"
        onClick={() => {
          updateActiveChatData('');
          setChatRoomHighlightMessageId(null);
          if (clearQuery) {
            clearQuery();
          }
        }}
      >
        <ArrowLeftIcon />
      </ChatRoomHeaderBackButton>
      <ChatRoomHeaderTitle>
        {chatData.activeChat?.title}
      </ChatRoomHeaderTitle>
    </HeaderLeftSideWrapper>
    <HeaderFlexWrapper>
      <HeaderActionButton
        view="text"
        onClick={() => {
          setIsChatRoomSettingsOpen(!isChatRoomSettingsOpen);
        }}
      >
        <TuneIcon />
      </HeaderActionButton>

      {!isChatPage && (
        <Fragment>
          <MinimizeButton
            view="text"
            onClick={() => {
              setIsChatOpen(false);
              if (clearQuery) {
                clearQuery();
              }
            }}
          >
            <WindowMinimizeIcon />
          </MinimizeButton>
          {setIsMaximize && (
            <HeaderActionButton
              view="text"
              onClick={() => {
                setIsMaximize(!isMaximize);
              }}
            >
              {isMaximize ? <WindowRestoreIcon /> : <WindowMaximizeIcon />}
            </HeaderActionButton>
          )}
        </Fragment>
      )}
    </HeaderFlexWrapper>
  </ChatHeader>
);

export default ChatRoomHeader;
