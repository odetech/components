import React, { Fragment } from 'react';
import {
  getInitials, getTimeDiffFromNow,
} from '~/helpers';
import {
  getAssociatedData,
} from '../../../helpers';
import {
  ChatButtonAvatarWrapper,
  ChatButtonAvatar,
  ChatButtonAvatarPlaceholder,
  ChatButtonInfoWrapper,
  ChatButtonInfoTitle,
  ChatButtonInfoMessage,
  ChatButton, ChatButtonInfoHeader,
  ChatButtonInfoTimeDot, ChatButtonInfoTime, ChatButtonAvatarsWrapper,
} from '../../ChannelsChatStyles';
import { ChatDataProps } from '../../../types';

type ChatRoomButtonProps = {
  chatRoomData: ChatDataProps & {
    messageSearchInfo?: {
      _id: string,
      rawText: string,
      filesLength: number,
      createdAt: string,
      tag: string,
      isAlreadyLoaded: boolean,
    }
  },
  updateActiveChatData: (chatId: string) => void,
  setChatRoomHighlightMessageId: (chatRoomHighlightMessageId: string) => void,
}

const ChatRoomButton = ({
  chatRoomData,
  updateActiveChatData,
  setChatRoomHighlightMessageId,
}: ChatRoomButtonProps) => {
  const getLastChatMessage = () => {
    if (!chatRoomData.data.messages?.data.length) return '* New chat *';

    const message = chatRoomData.data.messages?.data[chatRoomData.data.messages.data.length - 1] || null;

    if (!message) return '';

    return getAssociatedData(message).previewText;
  };

  const getChatRoomSearchMessage = () => {
    const filesLength = chatRoomData.messageSearchInfo?.filesLength || 0;
    let previewText = '';

    if (filesLength) {
      previewText = `🖼️ ${chatRoomData.messageSearchInfo?.rawText || ''}`;
    } else {
      previewText = chatRoomData.messageSearchInfo?.rawText || '';
    }
    return previewText;
  };

  const message = chatRoomData.data.messages?.data[chatRoomData.data.messages.data.length - 1] || null;

  const dateString = chatRoomData.messageSearchInfo
    ? chatRoomData.messageSearchInfo.createdAt.replace('+00:00', '')
    : message?.createdAt.replace('+00:00', '');

  return (
    <ChatButton
      onClick={async () => {
        await updateActiveChatData(chatRoomData._id);
        // Update of chatRoomHighlightMessageId if it is a message search chat room click
        if (chatRoomData.messageSearchInfo && chatRoomData.messageSearchInfo._id) {
          setChatRoomHighlightMessageId(chatRoomData.messageSearchInfo._id || '');
        } else if (chatRoomData.data.unreadMessages?.length) {
          setChatRoomHighlightMessageId(chatRoomData.data.unreadMessages[0]._id);
        }
      }}
      hasNewMessage={`${!!chatRoomData.data.unreadMessages?.length}`}
    >
      <ChatButtonAvatarsWrapper>
        {chatRoomData.data.members?.slice(0, 4).map((member, index) => (
          member.profile.avatar && member.profile.avatar.secureUrl ? (
            <ChatButtonAvatarWrapper
              key={member._id}
              membersAmount={chatRoomData.data.members?.length}
              index={index}
            >
              <ChatButtonAvatar
                src={member.profile.avatar.secureUrl}
              />
            </ChatButtonAvatarWrapper>
          ) : (
            <ChatButtonAvatarPlaceholder
              key={member._id}
              index={index}
              membersAmount={chatRoomData.data.members?.length}
            >
              {getInitials(member.profile.firstName, member.profile.lastName)}
            </ChatButtonAvatarPlaceholder>
          )
        ))}
      </ChatButtonAvatarsWrapper>
      <ChatButtonInfoWrapper>
        <ChatButtonInfoHeader>
          <ChatButtonInfoTitle>
            {chatRoomData.title}
          </ChatButtonInfoTitle>
          {chatRoomData.data.messages?.data && chatRoomData.data.messages.data.length !== 0 && (
            <Fragment>
              <ChatButtonInfoTimeDot />
              <ChatButtonInfoTime>
                {getTimeDiffFromNow(dateString!)}
              </ChatButtonInfoTime>
            </Fragment>
          )}
        </ChatButtonInfoHeader>
        <ChatButtonInfoMessage
          hasNewMessage={`${!!chatRoomData.data.unreadMessages?.length}`}
        >
          {chatRoomData.messageSearchInfo ? getChatRoomSearchMessage() : getLastChatMessage()}
        </ChatButtonInfoMessage>
      </ChatButtonInfoWrapper>
    </ChatButton>
  );
};

export default ChatRoomButton;
