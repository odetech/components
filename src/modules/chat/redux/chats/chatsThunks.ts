/* eslint-disable no-underscore-dangle,max-len,@typescript-eslint/no-unused-vars,@typescript-eslint/no-loss-of-precision */
import { compareDesc, isBefore } from 'date-fns';
import {
  ensureUtcTimestamp,
  filesUpload, sortDates,
} from '~/helpers';
import { generateUUID, getBase64 } from '~/components/editor/slate-editor.image-helpers';
import {
  defaultChatsRequest,
  getChatsSuccess,
  defaultChatsFailure,
  createNewChatSuccess,
  editChatSuccess,
  fetchChatsSuccess,
  removeChatSuccess,
  openActiveChatsSuccess,
  getMoreMessagesSuccess,
  sendMessageSuccess,
  deleteMessageSuccess,
  editMessageSuccess,
  editMessageRequest,
  deleteMessageRequest,
  addMessageToQueueSuccess,
  requestMessageFailure,
  searchChatRoomsSuccess, fetchAllChatRoomsIdsSuccess,
  fetchUnreadMessagesSuccess,
  editAllUnreadMessageSuccess,
} from './chatsSlice';
import {
  chatsSorting, getAssociatedData,
} from '../../helpers';
import {
  AppThunk,
  ChatDataProps,
  errorProps,
  MemberDataProps,
  UserData,
  MessageWithMoreDataProps,
  ChatMessageWithMoreDataProps,
  MessageDataProps,
} from '../../types';

const errorHandler = (e: any) => {
  console.log(e);
  const error = {} as errorProps;
  return error;
};

const getRepliesData = async ({
  messages,
}: {
  messages: MessageWithMoreDataProps[],
}, api: any) => {
  const updatedMessages: ChatMessageWithMoreDataProps[] = [];

  const receivedRepliesCache: MessageWithMoreDataProps[] = [];

  await Promise.all(messages.map(async (message: MessageWithMoreDataProps) => {
    let replyData;

    try {
      if (message.associatedId) {
        // Find Reply data in chat room data
        replyData = messages.find((messageData) => (messageData._id === message.associatedId));

        // Find Reply data in previous requests for Reply data
        if (!replyData) {
          replyData = receivedRepliesCache.find((messageData) => (messageData._id === message.associatedId));
        }

        // Request and save Reply data if it hasn't been found up to this point
        if (!replyData) {
          const replyDataRequest = await api.messages.getMessage(
            message.associatedId,
            {
              data: ['createdBy', 'files'],
              origin: 'chat',
            },
          );

          // Cache Reply request data
          receivedRepliesCache.push(replyDataRequest);

          replyData = replyDataRequest;
        }
      } else {
        replyData = null;
      }
    } catch (e) {
      // Handling the error of the request to receive Reply data
      replyData = {
        _id: message.associatedId,
        text: '<span>Deleted message</span>',
        rawText: 'Deleted message',
      };
    }

    if (replyData) {
      replyData = {
        ...replyData,
        ...getAssociatedData(replyData),
        data: {
          ...replyData.data,
          files: replyData.files,
        },
      };
    }

    updatedMessages.push({
      ...message,
      associatedMessageData: replyData,
    });
  }));

  return updatedMessages;
};

export const getReplyData = async ({
  message,
  allMessages,
}: {
  message: MessageWithMoreDataProps,
  allMessages: MessageWithMoreDataProps[],
}, api: any) => {
  let replyData;

  try {
    if (message.associatedId) {
      // Find Reply data in chat room data
      replyData = allMessages.find((messageData) => (messageData._id === message.associatedId));

      // Request and save Reply data if it hasn't been found up to this point
      if (!replyData) {
        const replyDataRequest = await api.messages.getMessage(
          message.associatedId,
          {
            data: ['createdBy', 'files'],
            origin: 'chat',
          },
        );

        replyData = replyDataRequest;
      }
    } else {
      replyData = null;
    }
  } catch (e) {
    // Handling the error of the request to receive Reply data
    replyData = {
      _id: message.associatedId,
      text: '<span>Deleted message</span>',
      rawText: 'Deleted message',
    };
  }

  if (replyData) {
    replyData = {
      ...replyData,
      ...getAssociatedData(replyData),
      data: {
        ...replyData.data,
        files: replyData.files,
      },
    };
  }

  const updatedMessage: ChatMessageWithMoreDataProps = {
    ...message,
    associatedMessageData: replyData,
  };

  return updatedMessage;
};

export const fetchUnreadMessages = (api: any) : AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());
    const unreadMessages = await api.messages.getUnreadMessages({ origin: 'chat' });
    dispatch(fetchUnreadMessagesSuccess({ unreadMessages }));
  } catch (e) {
    console.log(e);
    await errorHandler(e);
  }
};

export const fetchChats = (
  payloadData : {
    userId: string,
    limit?: number,
    pageNumber?: number,
    messagesPageNumber?: number,
    messagesLimit?: number,
  },
  api: any,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());
    const chatsData = getState().chats.data;
    let { unreadMessages } = chatsData;
    let allChatRoomsIds: undefined | string[];
    if (chatsData.allChatRoomsIds.length === 0) {
      const allChatRoomsData = await api.tags.getTags({
        isDirectChat: true,
        members: [payloadData.userId],
        isPublic: false,
      });

      allChatRoomsIds = allChatRoomsData.map((chatRoom: {
        _id: string,
        title: string,
      }) => (chatRoom._id));
    }

    const chatRoomsRequest: {
      data: ChatDataProps[],
      hasNextPage: boolean,
      pageNumber: number,
      pageSize: number,
      total: number,
    } = await api.tags.getChatRooms({
      isDirectChat: true,
      members: [payloadData.userId],
      isPublic: false,
      data: ['members', 'files'],
      limit: payloadData.limit || 15,
      pageNumber: payloadData.pageNumber || 1,
      messagesPageNumber: payloadData.messagesPageNumber || 1,
      messagesLimit: payloadData.messagesLimit || 15,
    });

    // Unread messages data
    const unreadMessagesByChatRoom: Record<string, MessageDataProps[]> = {};
    if (unreadMessages == null) {
      await dispatch(fetchUnreadMessages(api));
      unreadMessages = getState().chats.data.unreadMessages;
    }
    [...(unreadMessages || [])]?.sort(sortDates).forEach((message: MessageWithMoreDataProps) => {
      if (message.tags.length && !(message.tags[0] in unreadMessagesByChatRoom)) {
        unreadMessagesByChatRoom[message.tags[0]] = [];
      }
      if (message?._id) unreadMessagesByChatRoom[message.tags[0]].push(message);
    });
    // end

    const updatedChatsData = await Promise.all(chatRoomsRequest.data.map(async (chatRoom) => {
      let messagesData: MessageWithMoreDataProps[] = [];
      const sortedUnreadMessages = (unreadMessagesByChatRoom[chatRoom._id] || [])?.sort(sortDates);

      // get additional data if there are more than 1 page of unread messages
      if (unreadMessagesByChatRoom[chatRoom._id]?.length
        && chatRoom.data.messages?.data[chatRoom.data.messages.data.length - 1].createdAt
        && (sortedUnreadMessages[0]._id === chatRoom.data.messages.data[chatRoom.data.messages.data.length - 1]._id
          || isBefore(
            new Date(sortedUnreadMessages[0].createdAt),
            new Date(chatRoom.data.messages.data[chatRoom.data.messages.data.length - 1].createdAt),
          ))) {
        const messagesFromFirstUnread = await api.messages.getMessages({
          origin: 'chat',
          tags: [chatRoom._id],
          limit: 50,
          createdAtStart: sortedUnreadMessages[0].createdAt,
          createdAtEnd: chatRoom.data.messages.data[chatRoom.data.messages.data.length - 1].createdAt,
          data: ['files'],
          loadBy: ['chat'],
        });
        if (messagesFromFirstUnread.data.length) {
          if (messagesFromFirstUnread.data.length % 15 === 0) {
            messagesData = [
              ...chatRoom.data.messages.data,
              ...messagesFromFirstUnread.data,
            ];
            const additionalPageMessages = await api.messages.getMessages({
              origin: 'chat',
              tags: [chatRoom._id],
              limit: 15,
              pageNumber: Math.ceil((messagesData.length || 0) / 15),
              data: ['files'],
              loadBy: ['chat'],
            });

            messagesData = [
              ...messagesData,
              ...additionalPageMessages.data,
            ];
          } else if (messagesFromFirstUnread.data.length % 15 !== 0) {
            messagesData = [
              ...chatRoom.data.messages.data,
              ...messagesFromFirstUnread.data,
            ];

            // removing unnecessary messages
            const pages = Math.ceil((messagesFromFirstUnread.data.length || 0) / 15);
            const chatMessagesAmountToDelete = (messagesData.length) - (pages * 15);
            messagesData.splice(-chatMessagesAmountToDelete, chatMessagesAmountToDelete);

            const fullPageMessages = await api.messages.getMessages({
              origin: 'chat',
              tags: [chatRoom._id],
              limit: 15,
              pageNumber: Math.ceil((messagesData.length || 0) / 15) + 1,
              data: ['files'],
              loadBy: ['chat'],
            });

            messagesData = [
              ...messagesData,
              ...fullPageMessages.data,
            ];
          }
        }
      } else {
        messagesData = chatRoom.data.messages?.data || [];
      }
      const updatedMessagesData = await getRepliesData({
        messages: messagesData,
      }, api);

      return ({
        ...chatRoom,
        data: {
          // members without your account data
          members: chatRoom.data.members?.filter((member: MemberDataProps) => member._id !== payloadData.userId),
          messages: {
            ...chatRoom.data.messages,
            total: messagesData.length,
            data: updatedMessagesData
              ?.sort(sortDates)
              .map((message: MessageWithMoreDataProps) => ({
                ...message,
                data: message.data ? {
                  ...message.data,
                  createdBy: chatRoom.data
                    .members?.find((member: MemberDataProps) => (member._id === message.createdBy)),
                } : {},
              }) || []),
          },
          unreadMessages: unreadMessagesByChatRoom[chatRoom._id] ? sortedUnreadMessages : [],
        },
      });
    }));

    const updatedChatData: {
      elements: ChatDataProps[],
      hasNextPage: boolean,
      pageNumber: number,
      pageSize: number,
      total: number,
      allChatRoomsIds?: string[],
    } = {
      elements: chatsSorting(updatedChatsData),
      hasNextPage: chatRoomsRequest.hasNextPage,
      pageNumber: chatRoomsRequest.pageNumber,
      pageSize: chatRoomsRequest.pageSize,
      total: chatRoomsRequest.total,
    };

    if (allChatRoomsIds) {
      updatedChatData.allChatRoomsIds = allChatRoomsIds;
    }

    dispatch(fetchChatsSuccess(updatedChatData));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const fetchAllChatRoomsIds = (
  payloadData : {
    userId: string,
  },
  api: any,
): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());

    let allChatRoomsIds: string[] = [];

    const allChatRoomsData = await api.tags.getTags({
      isDirectChat: true,
      members: [payloadData.userId],
      isPublic: false,
    });

    allChatRoomsIds = allChatRoomsData.map((chatRoom: {
      _id: string,
      title: string,
    }) => (chatRoom._id));

    const updatedChatData: {
      allChatRoomsIds: string[],
    } = {
      allChatRoomsIds,
    };

    dispatch(fetchAllChatRoomsIdsSuccess(updatedChatData));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

// NETWORKS
export const checkIncomingNetworkConnectionRequest = (
  payloadData : {
    currentUserId: string,
    userIdToCheck: string,
  },
  api: any,
): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());

    const incomingNetworksDataRequest = await api.networks.getNetworks({
      userId1: payloadData.userIdToCheck,
      userId2: payloadData.currentUserId,
      isPending: true,
    });

    dispatch(getChatsSuccess());
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const networkAcceptInvitation = (
  payloadData : {
    userId1: string,
    userId2: string,
    _id: string,
  },
  api: any,
): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());

    await api.networks.acceptInvitation(payloadData);

    dispatch(getChatsSuccess());
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const deleteNetwork = (
  payloadData : {
    networkId: string,
  },
  api: any,
): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());

    await api.networks.delete(payloadData.networkId);

    dispatch(getChatsSuccess());
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

// CHAT
export const createChat = (
  chatPayload: {
    title: string,
    members: string[],
  },
  userData: UserData,
  api: any,
  setShowModal?: (showModal: boolean) => void,
  appErrorHandler?: (
    error: any,
    options?: {
      userRoles?: {
        _id?: string,
        isStaff?: boolean,
        isDev?: boolean,
      } | null,
      noToast?: boolean,
      toastMessage?: string,
    },
  ) => void,
): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());

    const postPayload = {
      owner: userData.id,
      title: chatPayload.title,
      members: chatPayload.members,
      isDirectChat: true,
      isPublic: false,
      data: ['members'],
    };

    if (userData.id) {
      postPayload.members.push(userData.id);
    }

    const postRequest = await api.tags.post(postPayload);

    const newChatData = {
      ...postRequest,
      data: {
        ...postRequest.data,
        members: postRequest.data.members?.filter((member: MemberDataProps) => member._id !== userData.id),
        messages: postRequest.data.articles || {
          data: [],
          hasNextPage: false,
          pageNumber: 1,
          pageSize: 26,
          total: 0,
        },
      },
    };

    if (setShowModal) {
      setShowModal(false);
    }

    dispatch(createNewChatSuccess(newChatData));
  } catch (e) {
    let error: any;

    if (appErrorHandler) {
      error = await appErrorHandler(
        e,
        {
          userRoles: {
            isStaff: userData.isStaff,
            isDev: userData.isDev,
          },
        },
      );
    } else {
      error = await errorHandler(e);
    }

    dispatch(defaultChatsFailure(error));
  }
};

export const editChat = (
  updatedChatData: {
    title?: string,
    members: string[],
    _id: string,
  },
  userId: string,
  api: any,
): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());

    // Payload data
    const patchPayload: {
      title?: string,
      members: string[],
      data: string[]
    } = {
      members: updatedChatData.members.concat(userId || ''),
      data: ['members'],
    };
    if (updatedChatData.title) {
      patchPayload.title = updatedChatData.title;
    }

    const patchRequest = await api.tags.patch(updatedChatData._id, patchPayload);

    dispatch(editChatSuccess(patchRequest));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const leaveOrArchiveChatRoom = (
  updatedChatId: string,
  userId: string,
  api: any,
  isArchive?: boolean,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());

    const chatRoomDataCopy = getState().chats.data.elements.find((chat) => chat._id === updatedChatId);
    const membersWithoutYou: string[] = chatRoomDataCopy?.members
      .filter((item: string) => item !== userId) || [];

    const patchPayload: {
      isArchive?: boolean,
      members?: string[],
    } = {};

    // Leave chat patch payload
    if (!isArchive) {
      patchPayload.members = membersWithoutYou;
    }
    // Archive chat patch payload
    if (isArchive) {
      patchPayload.isArchive = isArchive;
    }

    await api.tags.patch(updatedChatId, patchPayload);

    dispatch(removeChatSuccess(updatedChatId));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const convertChatRoomToPrivateChannel = (
  updatedChatId: string,
  api: any,
  fetchTags?: any,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());

    const patchPayload: {
      isDirectChat?: boolean,
    } = {};

    // Convert chat room to private channel patch payload
    patchPayload.isDirectChat = false;

    await api.tags.patch(updatedChatId, patchPayload);

    if (fetchTags) {
      dispatch(fetchTags());
    }

    dispatch(removeChatSuccess(updatedChatId));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const updateActiveChat = (
  activeChatId: string | undefined,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());
    dispatch(openActiveChatsSuccess(activeChatId));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

// MESSAGES
export const addMessageToQueue = (
  values: {
    associatedId?: string,
    message: string,
    files: { file: File, uiId: string }[],
    uiIds: string[],
  },
  chatId: string | undefined,
  userId: string,
): AppThunk => async (dispatch, getState) => {
  try {
    if (chatId) {
      const files: any = [];

      await Promise.all(values.files.map(async (file: { file: File }) => {
        const url = await getBase64(file.file);
        files.push({
          ...file,
          url,
        });
      }));

      let previewText = {};

      if (values.associatedId) {
        const associatedData = getState().chats.data.elements
          .find((chat) => chat._id === chatId)?.data.messages?.data
          .find((msg) => msg._id === values.associatedId);

        if (associatedData) {
          previewText = getAssociatedData(associatedData);
        }
      }

      const formattedMessage = {
        _id: generateUUID(),
        ...previewText,
        text: values.message,
        createdBy: userId,
        // @ts-ignore
        data: { files },
        status: 'creating',
        tags: [chatId],
        values,
      };
      // @ts-ignore
      dispatch(addMessageToQueueSuccess(formattedMessage));
    }
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const sendChatMessage = (
  chatRoomId: string,
  messageData: {
    text: string,
    rawText: string,
    mentions?: string[],
    uuid?: string,
  },
  userId: string,
  api: any,
  files?: { file: File, uiId: string }[],
  app?: string,
): AppThunk => async (dispatch) => {
  try {
    const payload: {
      text: string,
      rawText: string,
      tags: string[],
      isByStaff: boolean,
      isDraft: boolean,
      createdBy: string,
      updatedBy: string,
      mentions?: string[],
    } = {
      text: messageData.text,
      rawText: messageData.rawText,
      tags: [chatRoomId],
      isByStaff: false,
      isDraft: false,
      createdBy: userId,
      updatedBy: userId,
    };

    if (messageData.mentions && messageData.mentions.length !== 0) {
      payload.mentions = messageData.mentions;
    }

    let uploadedFiles: { cloudinary: any, uiId: string }[] = [];
    if (files?.length) {
      uploadedFiles = await filesUpload(files);
    }
    if (uploadedFiles.length) {
      await api.messages.postFiles(
        {
          message: payload,
          files: uploadedFiles,
        },
        {
          app,
          isChat: true,
        },
      );
    } else {
      await api.messages.post(payload, {
        app,
        isChat: true,
      });
    }
    dispatch(getChatsSuccess()); // firebaseMessagesDataUpdate will do everything
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
    if (messageData.uuid) {
      dispatch(requestMessageFailure(messageData.uuid));
    }
  }
};

export const sendChatMessages = (
  newMessagesData: {
    chatRoomId: string,
    messageData: {
      text: string,
      rawText: string,
      mentions?: string[],
      files?: { cloudinary: any, uiId: string }[],
    },
  }[],
  userId: string,
  api: any,
  app: string,
): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultChatsRequest());

    await Promise.all(newMessagesData.map(async (item) => {
      const payload: {
        text: string,
        rawText: string,
        tags: string[],
        isByStaff: boolean,
        isDraft: boolean,
        createdBy: string,
        updatedBy: string,
        mentions?: string[],
      } = {
        text: item.messageData.text,
        rawText: item.messageData.rawText,
        tags: [item.chatRoomId],
        isByStaff: false,
        isDraft: false,
        createdBy: userId,
        updatedBy: userId,
      };

      if (item.messageData.files?.length) {
        await api.messages.postFiles(
          {
            message: payload,
            files: item.messageData.files,
          },
          {
            app,
            isChat: true,
          },
        );
      } else {
        await api.messages.post(payload, {
          app,
          isChat: true,
        });
      }
    }));

    dispatch(getChatsSuccess()); // firebaseMessagesDataUpdate will do everything
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const editChatMessage = (
  editChatMessageData: {
    messageNewData: ChatMessageWithMoreDataProps,
    currentUserId: string,
  },
  requestAdditionalData: {
    api: any,
    app: string,
  },
  withoutRequest?: boolean,
): AppThunk => async (dispatch, getState) => {
  try {
    const formattedMessage = {
      ...editChatMessageData.messageNewData,
      data: {
        ...editChatMessageData.messageNewData.data,
        files: [...(editChatMessageData.messageNewData.data?.files || [])],
      },
    };
    if (formattedMessage.data) {
      formattedMessage.data.files = formattedMessage.data.files
        .filter((file) => !formattedMessage.uiIds?.includes(file.uiId));
      if (formattedMessage.files) {
        formattedMessage.data.files.push(...formattedMessage.files);
      }
      const files: any[] = [];

      if (formattedMessage.data?.files?.length) {
        await Promise.all(formattedMessage.data.files.map(async (file) => {
          // @ts-ignore
          if (file.file) {
            // @ts-ignore
            const url = await getBase64(file.file);
            files.push({
              ...file,
              url,
            });
          } else {
            files.push(file);
          }
        }));
      }
      formattedMessage.data.files = files;
    }
    dispatch(editMessageRequest(formattedMessage));

    let updatedMessage = editChatMessageData.messageNewData;
    const payload = {
      text: editChatMessageData.messageNewData.text,
      rawText: editChatMessageData.messageNewData.rawText,
    };

    if (!withoutRequest) {
      let deletions: any[] = [];
      let insertions: any[] = [];

      if (editChatMessageData.messageNewData.data?.files && editChatMessageData.messageNewData.files
        && (editChatMessageData.messageNewData.files?.length
          || (editChatMessageData.messageNewData.uiIds && editChatMessageData.messageNewData.uiIds.length))) {
        const uploadedFiles: { cloudinary: any, uiId: string }[] = await filesUpload(
          editChatMessageData.messageNewData.files.filter(
            (file) => editChatMessageData.messageNewData.uiIds?.includes(file.uiId),
          ),
        );
        deletions = editChatMessageData.messageNewData.data.files
          .filter((file) => !editChatMessageData.messageNewData.uiIds?.includes(file.uiId));
        insertions = uploadedFiles;
      }

      if (deletions.length || insertions.length) {
        updatedMessage = await requestAdditionalData.api.messages.patchFiles(
          editChatMessageData.messageNewData._id,
          {
            message: payload,
            deletions,
            insertions,
          },
          {
            app: requestAdditionalData.app,
            isChat: true,
          },
        );
      } else {
        updatedMessage = await requestAdditionalData.api.messages.patch(
          editChatMessageData.messageNewData._id,
          payload,
          {
            app: requestAdditionalData.app,
            isChat: true,
          },
        );
      }
    }

    if (updatedMessage.associatedId) {
      const chatDataCopy = JSON.parse(JSON.stringify(getState().chats.data));
      const chatRoomMessages = chatDataCopy.elements
        .find((chatRoom: ChatDataProps) => chatRoom._id === updatedMessage.tags[0])?.data?.messages?.data;

      updatedMessage = await getReplyData(
        {
          message: updatedMessage,
          allMessages: chatRoomMessages || [],
        },
        requestAdditionalData.api,
      );
    }

    dispatch(editMessageSuccess({
      updatedMessage,
      currentUserId: editChatMessageData.currentUserId,
    }));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
    dispatch(requestMessageFailure(editChatMessageData.messageNewData._id));
  }
};

export const deleteChatMessage = (
  messageId: string,
  chatId: string,
  api: any,
  app: string,
  withoutRequest?: boolean,
): AppThunk => async (dispatch) => {
  try {
    dispatch(deleteMessageRequest({
      messageId,
      chatId,
    }));

    if (!withoutRequest) {
      await api.messages.delete(messageId, {
        app,
        isChat: true,
      });
    }

    dispatch(deleteMessageSuccess({
      messageId,
      chatId,
    }));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
    dispatch(requestMessageFailure(messageId));
  }
};

export const getMoreChatMessages = (
  tagId: string,
  api: any,
  createdAt?: string,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());

    const chatsData = getState().chats.data;
    const chatData: ChatDataProps | undefined = chatsData.elements.find((item) => item._id === tagId);

    if (chatData) {
      const requestBody: {
        tags: string[],
        data: string[],
        loadBy: string[],
        limit?: number,
        createdAtEnd?: string,
        page?: number
      } = {
        tags: [tagId],
        data: ['files'],
        loadBy: ['chat'],
      };
      if (createdAt) {
        if (chatData.data.messages?.data[0].createdAt
          && isBefore(new Date(createdAt), new Date(chatData.data.messages.data[0].createdAt))) {
          dispatch(getMoreMessagesSuccess(null));
        } else {
          requestBody.createdAtEnd = createdAt;
          requestBody.limit = 10000;
        }
      } else {
        requestBody.page = Math.ceil((chatData.data.messages?.data.length || 0) / 15) + 1;
        requestBody.limit = 15;
      }
      const chatMessagesData = await api.messages.getMessages({
        origin: 'chat',
        tags: [tagId],
        limit: 15,
        pageNumber: Math.ceil((chatData.data.messages?.data.length || 0) / 15) + 1,
        data: ['files'],
        loadBy: ['chat'],
      });

      const chatMessagesUpdatedData = await getRepliesData({
        messages: chatMessagesData.data,
      }, api);

      dispatch(getMoreMessagesSuccess({
        newData: {
          data: chatMessagesUpdatedData,
          hasNextPage: chatMessagesData.hasNextPage,
        },
        tagId,
      }));
    } else {
      dispatch(getMoreMessagesSuccess(null));
    }
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

// SEARCH
export const searchChatRooms = (
  {
    chatSearchType, chatSearchInputValue, currentUserId, secondMemberId,
  }: {
  chatSearchType: string,
  chatSearchInputValue: string,
  currentUserId?: string,
  secondMemberId?: string,
},
  api: any,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());
    const chatDataCopy = JSON.parse(JSON.stringify(getState().chats.data));

    let filteredChatRooms: ChatDataProps[] = chatDataCopy.elements;

    // Search chat room by title
    if (chatSearchType === 'chatroom' && chatSearchInputValue !== '' && currentUserId) {
      const getChatRoomsPayload: {
        isDirectChat: boolean,
        isPublic: boolean,
        data: string[],
        limit: number,
        pageNumber: number,
        messagesPageNumber: number,
        messagesLimit: number,
        members: string[],
        title?: string,
      } = {
        isDirectChat: true,
        isPublic: false,
        data: ['members', 'files'],
        limit: 50,
        pageNumber: 1,
        messagesPageNumber: 1,
        messagesLimit: 15,
        members: [currentUserId],
      };

      if (secondMemberId) {
        getChatRoomsPayload.members.push(secondMemberId);
      } else {
        getChatRoomsPayload.title = chatSearchInputValue;
      }

      const searchRequest = await api.searches.getSearchQueries({
        limit: 50,
        searchStr: chatSearchInputValue,
        query: ['tags'],
        isDirectChat: true,
        isPublic: false,
      });
      const chatRoomsFoundBySearch = searchRequest.tags.values.map((searchValue: {
        data: ChatDataProps[],
      }) => ({
        ...searchValue.data,
      })).filter((chatRoomData: ChatDataProps) => (chatRoomData?.members
        ?.find((memberId) => memberId === currentUserId)
          && (chatRoomData.isArchive !== undefined && !chatRoomData.isArchive)));
      const searchChatRoomsFullDataRequest = await Promise.all(chatRoomsFoundBySearch
        .map(async (chatRoomData: ChatDataProps) => {
          const ChatRoomFullDataRequest = await api.tags.getChatRoom(
            chatRoomData._id,
            {
              isDirectChat: true,
              members: [currentUserId],
              isPublic: false,
              data: ['members', 'files'],
              limit: 15,
              pageNumber: 1,
              messagesLimit: 15,
              messagesPageNumber: 1,
            },
          );

          return ChatRoomFullDataRequest;
        }));

      const updatedFoundChatRooms = await Promise.all(searchChatRoomsFullDataRequest.map(async (chatRoom) => {
        const updatedMessagesData = await getRepliesData({
          messages: chatRoom.data.messages?.data || [],
        }, api);
        return ({
          ...chatRoom,
          data: {
            // members without your account data
            members: chatRoom.data.members?.filter((member: MemberDataProps) => member._id !== currentUserId),
            messages: {
              ...chatRoom.data.messages,
              data: updatedMessagesData
                ?.sort(sortDates)
                .map((message: MessageWithMoreDataProps) => ({
                  ...message,
                  data: message.data ? {
                    ...message.data,
                    createdBy: chatRoom.data
                      .members?.find((member: MemberDataProps) => (member._id === message.createdBy)),
                  } : {},
                }) || []),
            },
          },
        });
      }));
      // Find chat rooms by members first name filter
      // const chatRoomsFoundMembers: ChatDataProps[] = [];
      // filteredChatRooms
      //   .forEach((chatRoom) => (chatRoom.data.members?.forEach((member) => {
      //     const foundMemberInfo = member.profile.firstName?.toLowerCase().includes(chatSearchInputValue.toLowerCase())
      //       || member.profile.lastName?.toLowerCase().includes(chatSearchInputValue.toLowerCase()) || "";
      //     if (foundMemberInfo !== "") {
      //       chatRoomsFoundMembers.push(chatRoom);
      //     }
      //   })));
      // filteredChatRooms = chatRoomsFoundTitle.concat(chatRoomsFoundMembers)
      //   .filter((chatRoom, index, array) => (array.indexOf(chatRoom) === index));
      filteredChatRooms = chatsSorting(updatedFoundChatRooms);
    } else if (chatSearchType === 'message' && chatSearchInputValue !== '') { // Search chat room by message
      const foundMessagesRequest = await api.messages.search({
        origin: 'chat',
        search: chatSearchInputValue,
        tags: chatDataCopy.elements.map((chatroom: ChatDataProps) => (chatroom._id)),
        isDirectChat: true,
        isPublic: false,
      });

      const foundMessagesChatRooms: ChatDataProps[] = foundMessagesRequest.data.map((message: MessageDataProps) => ({
        messageSearchInfo: {
          _id: message._id,
          rawText: message.rawText,
          filesLength: message.data?.files?.length || 0,
          createdAt: message.createdAt,
          tag: message.tags[0],
          isAlreadyLoaded: !!filteredChatRooms.find((chatroom) => (chatroom.data.messages?.data
            .find((chatroomMessage) => (chatroomMessage._id === message._id)))),
        },
        ...filteredChatRooms.find((chatRoom) => (chatRoom._id === message.tags[0])),
      }));

      // Sort by message createdAt
      filteredChatRooms = foundMessagesChatRooms.sort((a, b) => {
        let dateA = '';
        let dateB = '';
        if (a.messageSearchInfo && b.messageSearchInfo) {
          dateA = ensureUtcTimestamp(a.messageSearchInfo.createdAt).replace('+00:00', '');
          dateB = ensureUtcTimestamp(b.messageSearchInfo.createdAt).replace('+00:00', '');
        }

        return new Date(dateB).getTime() - new Date(dateA).getTime();
      });
    }

    dispatch(searchChatRoomsSuccess(filteredChatRooms));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

export const searchGetMoreChatMessages = (
  {
    tagId,
    messageCreatedAtDate,
  }: {
    tagId: string,
    messageCreatedAtDate: string,
  },
  api: any,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());

    const chatsData = getState().chats.data;
    const chatData: ChatDataProps | undefined = chatsData.elements.find((item) => item._id === tagId);

    const chatRoomMessages = chatData?.data.messages?.data;

    const pageSize = 15;

    if (chatData && chatRoomMessages) {
      const chatRoomLastMessageData = chatRoomMessages[0];

      const chatNewMessagesData = await api.messages.getMessages({
        origin: 'chat',
        tags: [tagId],
        data: ['files'],
        createdAtStart: messageCreatedAtDate,
        createdAtEnd: chatRoomLastMessageData.createdAt,
        loadBy: ['chat'],
        limit: 50,
      });

      const hasExtraMessages = ((chatRoomMessages.length + chatNewMessagesData.data.length) % pageSize) > 0;

      let pages = 0;
      let chatMessagesAdditionalData = null;

      if (hasExtraMessages) {
        pages = Math.floor((chatRoomMessages.length + chatNewMessagesData.data.length) / pageSize);

        chatMessagesAdditionalData = await api.messages.getMessages({
          origin: 'chat',
          tags: [tagId],
          limit: pageSize,
          pageNumber: pages + 1,
          data: ['files'],
          loadBy: ['chat'],
        });
      }

      // New messages copy
      let chatMessagesList = JSON.parse(JSON.stringify(chatNewMessagesData.data));

      // Removing the same element associated with createdAtEnd
      chatMessagesList.shift();

      if (hasExtraMessages && pages && chatMessagesAdditionalData) {
        // Deleting duplicate messages
        const messagesAmountToDelete = (chatRoomMessages.length + chatNewMessagesData.data.length) - (pages * pageSize);
        chatMessagesList.splice(-messagesAmountToDelete, messagesAmountToDelete);

        // Adding a full page of new messages
        chatMessagesList = chatMessagesList.concat(chatMessagesAdditionalData.data);
      }

      const chatMessagesUpdatedData = await getRepliesData({
        messages: chatMessagesList,
      }, api);

      dispatch(getMoreMessagesSuccess({
        newData: {
          data: chatMessagesUpdatedData,
          hasNextPage: chatData.data.messages?.hasNextPage || false,
        },
        tagId,
      }));
    }
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};

// REPLIES
export const sendChatReply = (
  chatRoomId: string,
  messageData: {
    associatedId: string,
    text: string,
    rawText: string,
    mentions?: string[],
    uuid?: string,
  },
  userId: string,
  api: any,
  files?: { file: File, uiId: string }[],
  app?: string,
): AppThunk => async (dispatch, getState) => {
  try {
    const {
      associatedId,
      text,
      rawText,
      mentions,
    } = messageData;

    const postPayload = new api.messages.model({
      createdBy: userId,
      updatedBy: userId,
      associatedId,
      text,
      rawText,
    }).payload().getPostPayload();

    if (chatRoomId) {
      postPayload.tags = [chatRoomId];
    }

    if (associatedId) {
      postPayload.associatedId = associatedId;
    }

    if (mentions && mentions.length !== 0) {
      postPayload.mentions = mentions;
    }

    let uploadedFiles: { cloudinary: any, uiId: string }[] = [];
    if (files?.length) {
      uploadedFiles = await filesUpload(files);
    }
    if (files?.length) {
      await api.messages.postFiles(
        {
          message: postPayload,
          files: uploadedFiles,
          target: 'articles/comments',
        },
        {
          app,
          isChat: true,
        },
      );
    } else {
      await api.messages.post(postPayload, {
        app,
        isChat: true,
      });
    }

    dispatch(getChatsSuccess()); // firebaseMessagesDataUpdate will do everything
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
    if (messageData.uuid) {
      dispatch(requestMessageFailure(messageData.uuid));
    }
  }
};

// READ / UNREAD MESSAGES
export const readMessages = (
  readMessagesData: {
    currentUserId: string,
    messagesIds: string[],
  },
  api: any,
  app: string,
): AppThunk => async (dispatch, getState) => {
  try {
    await api.messages.patchReadMessages(
      {
        messages: readMessagesData.messagesIds,
      },
      {
        app,
        isChat: true,
      },
    );
    const { activeChat } = getState().chats.data;
    if (activeChat) {
      const copyChat = { ...activeChat };
      dispatch(editChatSuccess({
        ...activeChat,
        data: {
          ...activeChat.data,
          unreadMessages: copyChat.data.unreadMessages.filter((message) => !readMessagesData.messagesIds.includes(message._id))?.sort(sortDates),
          messages: copyChat.data.messages ? {
            ...copyChat.data.messages,
            data: copyChat.data.messages?.data?.length ? copyChat.data.messages?.data.map((message) => ({
              ...message,
              views: readMessagesData.messagesIds.includes(message._id)
                ? Array.from(new Set([...message.views, readMessagesData.currentUserId]))
                : message.views,
            })) : [],
          } : null,
        },
      }));
      dispatch(editAllUnreadMessageSuccess({ ids: readMessagesData.messagesIds }));
    }
  } catch (e) {
    console.log(e);
    await errorHandler(e);
  }
};

// FIREBASE
export const firebaseMessagesDataUpdate = (
  chatRoomUpdatedData: {
    newMessageData: MessageWithMoreDataProps,
    chatRoomId: string,
    type: string | 'addMessage' | 'updateMessage' | 'deleteMessage',
  },
  userId: string,
  api: any,
  app: string,
): AppThunk => async (dispatch, getState) => {
  try {
    dispatch(defaultChatsRequest());
    const NewMessageSoundPath = 'sound/new-message.mp3';

    // Adding a new message to the chat room data
    if (chatRoomUpdatedData?.chatRoomId && chatRoomUpdatedData?.newMessageData
      && chatRoomUpdatedData.type === 'addMessage') {
      const addedMessageData = chatRoomUpdatedData.newMessageData;

      // Copy of new message chat room
      let newMessageChatRoom = getState().chats.data.elements
        .find((chatRoomData: ChatDataProps) => addedMessageData && chatRoomData._id === addedMessageData.tags[0]);
      let isNewChatRoom = false;

      const currentTimeWithoutSeconds = new Date(new Date().setSeconds(0, 0));
      const addedMessageCreatedAtWithoutSeconds = new Date(
        new Date(ensureUtcTimestamp(addedMessageData.createdAt).replace('+00:00', ''))
          .setSeconds(0, 0),
      );
      const isCurrentDateAndMinute = compareDesc(currentTimeWithoutSeconds, addedMessageCreatedAtWithoutSeconds) === 0;

      // Getting chat data if there was no data and isCurrentDateAndMinute is same to current date and minute
      // TODO: update the second check in future to "view" check
      if (newMessageChatRoom === undefined && isCurrentDateAndMinute) {
        isNewChatRoom = true;

        const newChatRoomDataRequest = await api.tags.getChatRoom(
          addedMessageData.tags[0],
          {
            isDirectChat: true,
            members: [userId],
            isPublic: false,
            data: ['members', 'files'],
            limit: 15,
            pageNumber: 1,
            messagesLimit: 15,
            messagesPageNumber: 1,
          },
        );

        const updatedMessagesData = await getRepliesData({
          messages: newChatRoomDataRequest.data.messages?.data || [],
        }, api);

        const newChatRoomData = {
          ...newChatRoomDataRequest,
          data: {
            // members without your account data
            members: newChatRoomDataRequest.data.members?.filter((member: MemberDataProps) => member._id !== userId),
            messages: {
              ...newChatRoomDataRequest.data.messages,
              data: updatedMessagesData
                ?.sort(sortDates)
                .map((message: MessageWithMoreDataProps) => ({
                  ...message,
                  data: message.data ? {
                    ...message.data,
                    createdBy: newChatRoomDataRequest.data
                      .members?.find((member: MemberDataProps) => (member._id === message.createdBy)),
                  } : {},
                }) || []),
            },
            unreadMessages: newChatRoomDataRequest.data.unreadMessages
              .filter((message?: MessageWithMoreDataProps) => message?.createdBy !== userId),
          },
        };

        newMessageChatRoom = newChatRoomData;
      }

      if (newMessageChatRoom) {
        const sameMessage = newMessageChatRoom.data.messages?.data
          .find((message: MessageWithMoreDataProps) => message._id === addedMessageData._id);
        const isNewMessageCreatedAtAfterLastMessage = newMessageChatRoom.data.messages?.data && (
          newMessageChatRoom.data.messages.data.length === 0 || compareDesc(
            new Date(ensureUtcTimestamp(newMessageChatRoom.data.messages.data[newMessageChatRoom.data.messages.data.length - 1]
              .createdAt).replace('+00:00', '')),
            new Date(ensureUtcTimestamp(addedMessageData.createdAt).replace('+00:00', '')),
          )
        );

        // Is 'new message' check
        if ((!sameMessage || isNewChatRoom) && isNewMessageCreatedAtAfterLastMessage !== -1) {
          const newMessageData = {
            ...chatRoomUpdatedData.newMessageData,
            data: {
              files: chatRoomUpdatedData.newMessageData.files || [],
            },
          };
          const updatedNewMessageData: ChatMessageWithMoreDataProps = await getReplyData({
            message: newMessageData,
            allMessages: newMessageChatRoom.data.messages?.data || [],
          }, api);
          const { activeChat } = getState().chats.data;
          // New message sound
          if (NewMessageSoundPath && updatedNewMessageData.createdBy !== userId) {
            new Audio(NewMessageSoundPath).play().catch((e) => {
              console.log(`Audio is disabled: ${e}`);
            });
            const isChatOpen = activeChat && activeChat._id === newMessageChatRoom._id;
            if (isChatOpen && updatedNewMessageData.createdBy !== userId) {
              await api.messages.patchReadMessages(
                {
                  messages: [updatedNewMessageData._id],
                },
                {
                  app,
                  isChat: true,
                },
              );
              updatedNewMessageData.views.push(userId);
            }
          }
          dispatch(sendMessageSuccess({
            message: updatedNewMessageData,
            chatId: newMessageChatRoom._id,
            newChatRoomData: isNewChatRoom ? newMessageChatRoom : null,
            userId,
          }));
        } else if (sameMessage && sameMessage.views.length !== chatRoomUpdatedData.newMessageData.views.length) {
          const editMessageData = {
            ...chatRoomUpdatedData.newMessageData,
            data: {
              files: chatRoomUpdatedData.newMessageData.files || [],
            },
          };

          dispatch(editChatMessage(
            {
              messageNewData: editMessageData,
              currentUserId: userId,
            },
            {
              api,
              app,
            },
            true,
          ));
        }
      }
    }

    // Removing a message from the chat room
    if (chatRoomUpdatedData?.chatRoomId && chatRoomUpdatedData?.newMessageData
      && chatRoomUpdatedData.type === 'deleteMessage') {
      const removedMessageData = chatRoomUpdatedData.newMessageData;
      const messageChatRoom = getState().chats.data.elements
        .find((chatRoomData: ChatDataProps) => removedMessageData && chatRoomData._id === removedMessageData.tags[0]);

      const oldMessageData = messageChatRoom?.data.messages?.data
        .find((message: MessageWithMoreDataProps) => message._id === removedMessageData._id);
      if (oldMessageData) {
        dispatch(deleteChatMessage(removedMessageData._id, removedMessageData.tags[0], api, app, true));
      } else {
        dispatch(getChatsSuccess());
      }
    }

    // Editing a message in the chat room
    if (chatRoomUpdatedData?.chatRoomId && chatRoomUpdatedData?.newMessageData
      && chatRoomUpdatedData.type === 'updateMessage') {
      const editMessageData = {
        ...chatRoomUpdatedData.newMessageData,
        data: {
          files: chatRoomUpdatedData.newMessageData.files || [],
        },
      };
      const messageChatRoom = getState().chats.data.elements
        .find((chatRoomData: ChatDataProps) => editMessageData && chatRoomData._id === editMessageData.tags[0]);

      const oldMessageData = messageChatRoom?.data.messages?.data
        .find((message: MessageWithMoreDataProps) => message._id === editMessageData._id);
      if (oldMessageData) {
        dispatch(editChatMessage(
          {
            messageNewData: editMessageData,
            currentUserId: userId,
          },
          {
            api,
            app,
          },
          true,
        ));
      } else {
        dispatch(getChatsSuccess());
      }
    }
    dispatch(getChatsSuccess());
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultChatsFailure(error));
  }
};
