/* eslint-disable no-param-reassign */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { sortDates } from '~/helpers';
import {
  MessageWithMoreDataProps, ChatDataProps, ChatMessageWithMoreDataProps, ChatsState,
} from '../../types';
import { chatsSorting } from '../../helpers';

const initialState: ChatsState['chats'] = {
  data: {
    elements: [],
    activeChat: null,
    searchChatRooms: null,
    hasNextPage: true,
    pageNumber: 1,
    pageSize: 15,
    total: 0,
    allChatRoomsIds: [],
    unreadMessages: null,
  },
  loading: false,
  error: null,
};

const chatsSlice = createSlice({
  name: 'chats',
  initialState,
  reducers: {
    defaultChatsRequest(state) {
      state.loading = true;
      state.error = null;
    },
    getChatsSuccess(state, action: PayloadAction<ChatsState['chats']['data'] | undefined>) {
      if (action.payload) {
        state.data = action.payload;
      }
      state.loading = false;
      state.error = null;
    },
    fetchChatsSuccess(state, action: PayloadAction<{
      elements: ChatDataProps[],
      hasNextPage: boolean,
      pageNumber: number,
      pageSize: number,
      total: number,
      allChatRoomsIds?: string[],
    }>) {
      const hasExtraChatRooms = (state.data.elements.length % action.payload.pageSize) > 0;

      let updatedElements: ChatDataProps[] = [];

      if (hasExtraChatRooms) {
        const oldChatRoomsDataCopy = JSON.parse(JSON.stringify(state.data.elements));

        const pages = action.payload.pageNumber - 1;
        const chatRoomsAmountToDelete = (state.data.elements.length) - (pages * action.payload.pageSize);

        oldChatRoomsDataCopy.splice(-chatRoomsAmountToDelete, chatRoomsAmountToDelete);

        updatedElements = [...oldChatRoomsDataCopy, ...action.payload.elements];
      } else {
        updatedElements = [...state.data.elements, ...action.payload.elements];
      }

      state.data = {
        ...state.data,
        ...action.payload,
        elements: updatedElements,
      };

      state.loading = false;
      state.error = null;
    },
    fetchUnreadMessagesSuccess(state, action: PayloadAction<{ unreadMessages: MessageWithMoreDataProps[] }>) {
      state.data.unreadMessages = action.payload.unreadMessages;
      state.loading = true;
      state.error = null;
    },
    fetchAllChatRoomsIdsSuccess(state, action: PayloadAction<{
      allChatRoomsIds: string[],
    }>) {
      state.data = {
        ...state.data,
        ...action.payload,
      };

      state.loading = false;
      state.error = null;
    },
    openActiveChatsSuccess(state, action: PayloadAction<string | undefined>) {
      if (action.payload) {
        let chatIndex: number | undefined;
        let activeChat: ChatDataProps | null = null;
        if (state.data.searchChatRooms) {
          chatIndex = state.data.searchChatRooms.findIndex((item: ChatDataProps) => item._id === action.payload);
          activeChat = JSON.parse(JSON.stringify(state.data.searchChatRooms[chatIndex]));
        } else {
          chatIndex = state.data.elements.findIndex((item: ChatDataProps) => item._id === action.payload);
          activeChat = JSON.parse(JSON.stringify(state.data.elements[chatIndex]));
        }

        state.data.activeChat = activeChat;
      } else {
        state.data.activeChat = null;
      }

      state.loading = false;
      state.error = null;
    },
    // update
    searchChatRoomsSuccess(state, action: PayloadAction<ChatDataProps[] | undefined>) {
      if (action.payload) {
        state.data.searchChatRooms = action.payload;
      } else {
        state.data.searchChatRooms = null;
      }

      state.loading = false;
      state.error = null;
    },
    createNewChatSuccess(state, action: PayloadAction<ChatDataProps>) {
      if (action.payload) {
        state.data.elements.unshift(action.payload);
        state.data.allChatRoomsIds.unshift(action.payload._id);
      }
      state.loading = false;
      state.error = null;
    },
    editAllUnreadMessageSuccess(state, action: PayloadAction<{ ids: string[]}>) {
      state.data.unreadMessages = state.data.unreadMessages
        ?.filter((message) => !action.payload.ids.includes(message._id))
        ?.sort(sortDates) || [];
      state.loading = false;
      state.error = null;
    },
    editChatSuccess(state, action: PayloadAction<ChatDataProps>) {
      const chatIndex = state.data.elements.findIndex((item) => item._id === action.payload._id);

      const chatData = {
        ...state.data.elements[chatIndex],
        ...action.payload,
        data: {
          ...state.data.elements[chatIndex].data,
          messages: action.payload.data.messages ?? state.data.elements[chatIndex].data.messages,
          unreadMessages: action.payload.data.unreadMessages ?? state.data.elements[chatIndex].data.unreadMessages,
          members: action.payload.data.members,
        },
      };

      state.data.elements[chatIndex] = chatData;

      if (state.data.activeChat && state.data.activeChat._id === chatData._id) {
        state.data.activeChat = chatData;
      }
      state.loading = false;
      state.error = null;
    },
    removeChatSuccess(state, action: PayloadAction<string>) {
      const elementsDataCopy = JSON.parse(JSON.stringify(state.data.elements));
      if (elementsDataCopy?.length) {
        const chatRoomIndex = elementsDataCopy.findIndex((item: ChatDataProps) => item._id === action.payload);

        elementsDataCopy.splice(chatRoomIndex, 1);
      }
      state.data.elements = elementsDataCopy;

      const searchChatRoomsDataCopy = JSON.parse(JSON.stringify(state.data.searchChatRooms));
      if (searchChatRoomsDataCopy?.length) {
        const chatRoomIndex = searchChatRoomsDataCopy.findIndex((item: ChatDataProps) => item._id === action.payload);

        searchChatRoomsDataCopy.splice(chatRoomIndex, 1);
      }
      state.data.searchChatRooms = searchChatRoomsDataCopy;

      state.data.elements = elementsDataCopy;
      state.data.searchChatRooms = searchChatRoomsDataCopy;

      if (state.data.activeChat && state.data.activeChat._id === action.payload) {
        state.data.activeChat = null;
      }

      state.loading = false;
      state.error = null;
    },

    getMoreMessagesSuccess(state, action: PayloadAction<{
      newData: {
        data: ChatMessageWithMoreDataProps[],
        hasNextPage: boolean,
      }
      tagId: string,
    } | null>) {
      if (action.payload !== null) {
        const { newData, tagId } = action.payload;
        const chat = state.data.elements.find((item: ChatDataProps) => item._id === tagId);

        if (chat?.data.messages) {
          chat.data.messages.data.unshift(...newData.data);
          chat.data.messages.hasNextPage = newData.hasNextPage;

          // TODO temporary
          let newMessages: string[] = [];
          chat.data.messages.data = chat.data.messages.data?.sort(sortDates);
          chat.data.messages.data = chat.data.messages.data.filter((message) => {
            if (newMessages.includes(message._id)) return false;
            newMessages.push(message._id);
            return true;
          });

          if (state.data.activeChat?.data.messages && state.data.activeChat._id === tagId) {
            state.data.activeChat.data.messages.data.unshift(...newData.data);
            state.data.activeChat.data.messages.hasNextPage = newData.hasNextPage;

            newMessages = [];
            // TODO temporary
            state.data.activeChat.data.messages.data = state.data.activeChat.data.messages.data?.sort(sortDates);
            state.data.activeChat.data.messages.data = state.data.activeChat.data.messages.data.filter((message) => {
              if (newMessages.includes(message._id)) return false;
              newMessages.push(message._id);
              return true;
            });
          }
        }
        state.data.elements = chatsSorting(state.data.elements);
      }
      state.loading = false;
      state.error = null;
    },

    addMessageToQueueSuccess(state, action: PayloadAction<ChatMessageWithMoreDataProps>) {
      const chatId = action.payload.tags[0];
      if (state.data.activeChat && state.data.activeChat._id === chatId) {
        state.data.activeChat.data.messages?.data.push(action.payload);
      }
    },
    sendMessageSuccess(
      state,
      action: PayloadAction<{
        message: ChatMessageWithMoreDataProps & {
          uuid?: string,
        },
        userId?: string,
        chatId: string,
        newChatRoomData?: ChatDataProps | null,
        uuid?: string,
      }>,
    ) {
      // If a new message chat is open
      if (state.data.activeChat?.data.messages?.data && state.data.activeChat._id === action.payload.chatId) {
        const oldMessages = [...state.data.activeChat.data.messages.data.map((o) => ({ ...o }))];
        const queue = oldMessages
          .filter((item) => item.status === 'creating' && item._id !== action.payload.uuid) || [];

        if (!action.payload.uuid && queue.length && action.payload.message.createdBy === queue[0].createdBy) {
          const index = queue.findIndex((item) => item.text === action.payload.message.text);

          if (index !== -1) {
            queue.splice(index, 1);
          }
        }
        // TODO: delete 2ed filter after logic is fixed
        const messages = oldMessages
          .filter((item) => (item.status !== 'creating' && item._id !== action.payload.message._id)) || [];
        messages.push(action.payload.message);
        if (messages.length > 1) {
          messages?.sort(sortDates);
        }
        messages.push(...queue);
        state.data.activeChat.data.messages.data = messages;
      }
      // If new chat room data exists
      if (action.payload.newChatRoomData) {
        const chatRoomDataCopy = JSON.parse(JSON.stringify(action.payload.newChatRoomData));
        state.data.elements = chatsSorting([chatRoomDataCopy, ...state.data.elements]);
        state.data.unreadMessages = action.payload.newChatRoomData.data.messages?.data || [];
      } else {
        const chatIndex = state.data.elements.findIndex((chatRoomData) => chatRoomData._id === action.payload.chatId);
        const chat = state.data.elements.find((chatRoomData) => chatRoomData._id === action.payload.chatId);
        if (chat?.data.messages) {
          const messages = [...chat.data.messages.data, action.payload.message];
          messages?.sort(sortDates);
          chat.data.messages.data = messages;
          state.data.elements.splice(0, 0, state.data.elements.splice(chatIndex, 1)[0]);
          state.data.elements = chatsSorting(state.data.elements);

          if (action.payload.userId && action.payload.message.createdBy !== action.payload.userId
            && (!state.data.activeChat || (state.data.activeChat
              && state.data.activeChat._id !== action.payload.chatId))) {
            const unreadMessages = [...chat.data.unreadMessages, action.payload.message];
            chat.data.unreadMessages = unreadMessages?.sort(sortDates);
            state.data.unreadMessages?.push(action.payload.message);
          }
        }
      }
      // search chat room update
      const searchChatRoomIndex = state.data.searchChatRooms && state.data.searchChatRooms
        .findIndex((searchChatRoomData) => searchChatRoomData._id === action.payload.chatId);
      if (state.data.searchChatRooms && searchChatRoomIndex !== -1 && searchChatRoomIndex !== null) {
        const searchChatRoom = state.data.searchChatRooms[searchChatRoomIndex];
        if (searchChatRoom && searchChatRoom.data.messages) {
          const messages = [...searchChatRoom.data.messages.data, action.payload.message];
          messages?.sort(sortDates);
          searchChatRoom.data.messages.data = messages;
          state.data.searchChatRooms.splice(0, 0, state.data.searchChatRooms.splice(searchChatRoomIndex, 1)[0]);
        }
      }

      state.loading = false;
      state.error = null;
    },

    editMessageRequest(state, action: PayloadAction<ChatMessageWithMoreDataProps>) {
      const updatedMessage = { ...action.payload };
      updatedMessage.status = 'editing';
      const chatId = updatedMessage.tags[0];

      if (state.data.activeChat && state.data.activeChat._id === chatId && state.data.activeChat.data.messages) {
        const oldMessages = [...state.data.activeChat.data.messages.data.map((activeChatMessages) => {
          if (updatedMessage._id === activeChatMessages._id) {
            return updatedMessage;
          }
          return { ...activeChatMessages };
        })];
        state.data.activeChat.data.messages.data = oldMessages;
      }
    },

    editMessageSuccess(state, action: PayloadAction<{
      updatedMessage: ChatMessageWithMoreDataProps,
      currentUserId: string,
    }>) {
      const updatedMessage = { ...action.payload.updatedMessage };

      // Deleting 'editing' status
      if (updatedMessage.status) {
        delete updatedMessage.status;
      }

      const chatId = updatedMessage.tags[0];
      const chatRoom = state.data.elements.find((chatRoomData) => chatRoomData._id === chatId);
      const messageIndex = chatRoom?.data.messages?.data
        .findIndex((message) => message._id === updatedMessage._id) || -1;

      if (messageIndex !== -1 && chatRoom?.data.messages) {
        if (!updatedMessage?.data?.files) {
          updatedMessage.data = chatRoom.data.messages?.data[messageIndex].data;
        }
        chatRoom.data.messages.data[messageIndex] = updatedMessage;

        if (state.data.activeChat && state.data.activeChat._id === chatId && state.data.activeChat.data.messages) {
          state.data.activeChat.data.messages.data[messageIndex] = updatedMessage;
        }

        // Unread messages update if viewed
        const isViewed = !!updatedMessage.views.find((userId) => userId === action.payload.currentUserId);
        if (action.payload.currentUserId && isViewed) {
          chatRoom.data.unreadMessages = chatRoom.data.unreadMessages.filter((unreadMessage) => (
            unreadMessage._id !== updatedMessage._id
          ));
          state.data.unreadMessages = state.data.unreadMessages
            ?.filter((unreadMessage) => unreadMessage._id !== updatedMessage._id) || [];
        }
      }
    },

    deleteMessageRequest(state, action: PayloadAction<{ messageId: string, chatId: string }>) {
      if (state.data.activeChat?.data.messages?.data && state.data.activeChat._id === action.payload.chatId) {
        const message = state.data.activeChat.data.messages.data.find((msg) => msg._id === action.payload.messageId);
        if (message) {
          message.status = 'deleting';
        }
      }
    },
    deleteMessageSuccess(state, action: PayloadAction<{ messageId: string, chatId: string }>) {
      const chatIndex = state.data.elements.findIndex((chatRoomData) => chatRoomData._id === action.payload.chatId);
      const messageIndex = state.data.elements[chatIndex]?.data.messages?.data
        .findIndex((message) => message._id === action.payload.messageId) || -1;

      if (state.data.activeChat && state.data.activeChat._id === action.payload.chatId
        && state.data.activeChat.data.messages?.data) {
        const oldMessages = [...state.data.activeChat.data.messages.data.map((o) => ({ ...o }))];
        state.data.activeChat.data.messages.data = oldMessages
          .filter((message) => message._id !== action.payload.messageId);
      }

      if (messageIndex !== -1) {
        state.data.elements[chatIndex].data.messages?.data.splice(messageIndex, 1);
        state.data.elements[chatIndex].data.unreadMessages = state.data.elements[chatIndex].data.unreadMessages
          .filter((message) => message._id !== action.payload.messageId);
        state.data.unreadMessages = state.data.unreadMessages?.filter(
          (message) => message._id !== action.payload.messageId,
        ) || [];
      }

      state.data.elements = chatsSorting(state.data.elements);
    },

    requestMessageFailure(state, action: PayloadAction<string>) {
      if (state.data.activeChat?.data.messages?.data) {
        const message = state.data.activeChat.data.messages.data.find((msg) => msg._id === action.payload);
        if (message) {
          message.error = 'true';
        }
      }
    },

    sendChatMessagesSuccess(state, action: PayloadAction<{
      chatRoomId: string,
      messageData: ChatMessageWithMoreDataProps,
    }[]>) {
      action.payload.forEach((item) => {
        const chat = state.data.elements.find((chatRoomData) => chatRoomData._id === item.chatRoomId);

        if (chat?.data.messages) {
          const messages = [...chat.data.messages.data, item.messageData];
          messages?.sort(sortDates);
          chat.data.messages.data = messages;

          if (state.data.activeChat?.data.messages && state.data.activeChat._id === item.chatRoomId) {
            const oldMessages = [...state.data.activeChat.data.messages.data.map((o) => ({ ...o }))];
            const queue = oldMessages.filter((message) => message.status === 'creating') || [];
            messages.push(...queue);
            state.data.activeChat.data.messages.data = messages;
          }
        }
      });
      state.data.elements = chatsSorting(state.data.elements);
      state.loading = false;
      state.error = null;
    },

    defaultChatsFailure(state, action: PayloadAction<ChatsState['chats']['error']>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  defaultChatsRequest,
  getChatsSuccess,
  defaultChatsFailure,

  createNewChatSuccess,
  editChatSuccess,
  fetchChatsSuccess,
  fetchAllChatRoomsIdsSuccess,
  removeChatSuccess,
  openActiveChatsSuccess,

  searchChatRoomsSuccess,

  getMoreMessagesSuccess,
  addMessageToQueueSuccess,
  sendMessageSuccess,
  deleteMessageRequest,
  deleteMessageSuccess,
  editMessageSuccess,
  editMessageRequest,
  requestMessageFailure,

  sendChatMessagesSuccess,
  fetchUnreadMessagesSuccess,
  editAllUnreadMessageSuccess,
} = chatsSlice.actions;

export const chatsReducer = chatsSlice.reducer;
