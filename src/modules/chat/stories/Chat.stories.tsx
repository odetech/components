/* eslint-disable react/destructuring-assignment */
import React, { useEffect, useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { BeatLoader } from 'react-spinners';
import { initializeApp } from '@firebase/app';
import { getDatabase } from '@firebase/database';
import { useObjectVal } from 'react-firebase-hooks/database';
import { ref } from 'firebase/database';
import OdeCloud from '@odecloud/odecloud';
import Modal from 'react-modal';
import testUser from '../../../../configs/testUserCredentials.json';
import testFirebaseConfig from '../../../../configs/testFirebaseConfig.json';
import { UserData } from '../types';
import { chatsReducer } from '../redux/chats';
import { ChannelsChat, ChannelsChatProps } from '../index';

Modal.setAppElement('body');

let api: any = null;

const store = configureStore({
  reducer: {
    chats: chatsReducer,
  },
});

const meta: Meta<ChannelsChatProps & {
  loginEmail: string,
  loginPassword: string,
  endpoint: string,
  shouldReloadChatStorybook: boolean,
  firebaseApiKey: string,
  firebaseAuthDomain: string,
  firebaseDatabaseURL: string,
  firebaseProjectId: string,
  firebaseStorageBucket: string,
  firebaseMessagingSenderId: string,
  firebaseAppId: string,
}> = {
  component: ChannelsChat,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.margin = 'unset';
        // @ts-ignore
        document.getElementById('storybook-root').style.flexDirection = 'unset';
        // @ts-ignore
        document.getElementById('storybook-root').style.justifyContent = 'unset';
      }
      return (
        <Provider store={store}>
          <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
            {story()}
          </MemoryRouter>
        </Provider>
      );
    },
  ],
  argTypes: {
    loginEmail: { name: 'loginEmail', control: 'text' },
    loginPassword: { name: 'loginPassword', control: 'text' },
    app: { name: 'app', control: 'text' },
    endpoint: { name: 'endpoint', control: 'text' },
    shouldReloadChatStorybook: { control: 'boolean' },
    isStaffMode: { control: 'boolean' },
    firebaseApiKey: { name: 'firebaseApiKey', control: 'text' },
    firebaseAuthDomain: { name: 'firebaseAuthDomain', control: 'text' },
    firebaseDatabaseURL: { name: 'firebaseDatabaseURL', control: 'text' },
    firebaseProjectId: { name: 'firebaseProjectId', control: 'text' },
    firebaseStorageBucket: { name: 'firebaseStorageBucket', control: 'text' },
    firebaseMessagingSenderId: { name: 'firebaseMessagingSenderId', control: 'text' },
    firebaseAppId: { name: 'firebaseAppId', control: 'text' },
  },
  args: {
    loginEmail: testUser.email || '',
    loginPassword: testUser.password || '',
    app: 'odetask',
    endpoint: 'https://server-beta.odecloud.app/api/v1',
    shouldReloadChatStorybook: false,
    isStaffMode: false,
    firebaseApiKey: testFirebaseConfig.apiKey || '',
    firebaseAuthDomain: testFirebaseConfig.authDomain || '',
    firebaseDatabaseURL: testFirebaseConfig.databaseURL || '',
    firebaseProjectId: testFirebaseConfig.projectId || '',
    firebaseStorageBucket: testFirebaseConfig.storageBucket || '',
    firebaseMessagingSenderId: testFirebaseConfig.messagingSenderId || '',
    firebaseAppId: testFirebaseConfig.appId || '',
  },
};
export default meta;

const useWindowSize = () => {
  const [size, setSize] = useState([0, 0]);

  useEffect(() => {
    const updateSize = () => {
      setSize([window.innerWidth, window.innerHeight]);
    };
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  return size;
};

const startFirebase = (testConfigData: {
  apiKey: string,
  authDomain: string,
  databaseURL: string,
  projectId: string,
  storageBucket: string,
  messagingSenderId: string,
  appId: string,
}) => {
  const firebaseConfig = {
    apiKey: testConfigData.apiKey,
    authDomain: testConfigData.authDomain,
    databaseURL: testConfigData.databaseURL,
    projectId: testConfigData.projectId,
    storageBucket: testConfigData.storageBucket,
    messagingSenderId: testConfigData.messagingSenderId,
    appId: testConfigData.appId,
  };

  const app = initializeApp(firebaseConfig);
  return getDatabase(app);
};

// 👇 We create a “template” of how args map to rendering
export const Default: StoryFn<ChannelsChatProps & {
  loginEmail: string,
  loginPassword: string,
  endpoint: string,
  shouldReloadChatStorybook: boolean,
  firebaseApiKey: string,
  firebaseAuthDomain: string,
  firebaseDatabaseURL: string,
  firebaseProjectId: string,
  firebaseStorageBucket: string,
  firebaseMessagingSenderId: string,
  firebaseAppId: string,
}> = (args) => {
  const [userData, setUserData] = useState<UserData | null>(null);
  const [dbFirebase, setDbFirebase] = useState<any>(null);
  const [isChatOpen, setIsChatOpen] = useState<boolean>(true);
  const getData = async () => {
    api = new OdeCloud({
      endpoint: args.endpoint,
    });

    const loginResult = await api.auth.login({
      email: args.loginEmail,
      password: args.loginPassword,
      appUrl: 'http://localhost:3000',
    });
    const { authToken, userId } = loginResult;
    const url = 'https://tasks.odecloud.app';

    await api.resync({ authToken, userId, url });

    const userDataRequest = await api.users.getUser(
      userId,
      {
        roles: 1,
        skillsets: 1,
        masterNotifications: 1,
        appStore: 1,
        profileCompletion: 1,
      },
    );

    setUserData({
      id: userDataRequest._id,
      isStaff: true,
      isDev: false,
      info: {
        profile: {
          ...userDataRequest.profile,
        },
      },
    });

    setDbFirebase(startFirebase({
      apiKey: args.firebaseApiKey,
      authDomain: args.firebaseAuthDomain,
      databaseURL: args.firebaseDatabaseURL,
      projectId: args.firebaseProjectId,
      storageBucket: args.firebaseStorageBucket,
      messagingSenderId: args.firebaseMessagingSenderId,
      appId: args.firebaseAppId,
    }));
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (args.shouldReloadChatStorybook) {
      getData();
    }
  }, [args.shouldReloadChatStorybook]);

  const [width] = useWindowSize(); // 'height' is also available

  const [firebaseNetworkConnections]: any = useObjectVal(dbFirebase && userData?.id
    ? ref(dbFirebase, `/networks/${userData.id}`) : null);

  // const [firebaseIncomingNetworks]: any = useObjectVal(dbFirebase && userData?.id
  //   ? ref(dbFirebase, `/networks/${userData.id}/incoming`) : null,
  // );
  // const firebaseIncomingNetworksData = firebaseIncomingNetworks
  //   ? Object.keys(firebaseIncomingNetworks).map((key) => ({
  //     _id: key,
  //     ...JSON.parse(firebaseIncomingNetworks[key]),
  //   })) : [];
  // console.log("firebaseIncomingNetworksData", firebaseIncomingNetworksData);

  // const [firebaseOutgoingNetworks]: any = useObjectVal(dbFirebase && userData?.id
  //   ? ref(dbFirebase, `/networks/${userData.id}/outgoing`) : null,
  // );
  // const firebaseOutgoingNetworksData = firebaseOutgoingNetworks
  //   ? Object.keys(firebaseOutgoingNetworks).map((key) => ({
  //     _id: key,
  //     ...JSON.parse(firebaseOutgoingNetworks[key]),
  //   })) : [];
  // console.log("firebaseOutgoingNetworksData", firebaseOutgoingNetworksData);

  const [appSearchChatRoomData, setAppSearchChatRoomData] = useState<{
    userFirstname: string,
    userId: string,
  } | null>(
    null,
    // {
    //   userFirstname: "RavilTEST2",
    //   userId: "9da20daf-1a47-45a8-b7a4-955f32c7a280",
    // },
  );

  // @ts-ignore
  const params: {
    chat?: string,
    message?: string,
  } = new Proxy(new URLSearchParams(window.location.search), {
    // @ts-ignore
    get: (searchParams, prop) => searchParams.get(prop),
  });

  if (!args.loginEmail || !args.loginPassword || !args.endpoint || !args.app || !args.firebaseApiKey
    || !args.firebaseAuthDomain || !args.firebaseDatabaseURL || !args.firebaseProjectId || !args.firebaseStorageBucket
    || !args.firebaseMessagingSenderId || !args.firebaseAppId) {
    // If not enough data
    return (
      <span>Please fill in all Controls fields</span>
    );
  } if (userData?.id && dbFirebase && !firebaseNetworkConnections?.connections) {
    // If no connections
    return (
      <span>No network connections</span>
    );
  } if (userData?.id && dbFirebase && firebaseNetworkConnections?.connections) {
    // If there is all the data
    return (
      <ChannelsChat
        {...args}
        setProfilePreviewUserData={(profilePreviewUserData: UserData['info'] | null) => {
          console.log('Profile Preview User Data', profilePreviewUserData);
        }}
        onMentionClick={(userId: string) => {
          console.log('userId', userId);
        }}
        onHashtagClick={(hashtagValue: string) => {
          console.log('hashtagValue', hashtagValue);
        }}
        activeChatRoomId={params.chat}
        highlightMessageId={params.message}
        userData={userData}
        api={api}
        isChatOpen={isChatOpen}
        setIsChatOpen={setIsChatOpen}
        width={width}
        tagsLoading={false}
        dbFirebase={dbFirebase}
        networkConnections={JSON.parse(firebaseNetworkConnections.connections)
          || {
            count: 0,
            networks: [],
          }}
        appSearchChatRoomData={appSearchChatRoomData}
        setAppSearchChatRoomData={setAppSearchChatRoomData}
        // incomingNetworks={firebaseIncomingNetworksData}
        // outgoingNetworks={firebaseOutgoingNetworks}
      />
    );
  }
  // If it's loading
  return (
    <BeatLoader color="#F7942A" size={8} />
  );
};
