import React, {
  ReactNode,
} from 'react';
import {
  DropdownWrapper,
  GamificationDropdownButton,
} from '../../GamificationStyles';
import { ChevronDownIcon } from '../../../../components/icons';

export type GamificationDropdownProps = {
  mainChildren: ReactNode,
  menuChildren: ReactNode,
  isMenuOpen: boolean,
  setIsMenuOpen: (isMenuOpen?: string | null) => void,
  margin?: string,
  gamificationLevelData?: {
    userId: string,
    receipts: any,
    wallet: 0,
    rank: string,
    history: any[],
    messagesUnit: any,
    totalMpsUnit?: number | null,
    createdAt: null,
    updatedAt: null,
    completion: any,
  },
};

export const GamificationDropdown = ({
  margin,
  gamificationLevelData,
  mainChildren,
  menuChildren,
  isMenuOpen, setIsMenuOpen,
}: GamificationDropdownProps) => (
  <DropdownWrapper
    margin={margin}
  >
    <GamificationDropdownButton
      isOpen={isMenuOpen}
      onClick={() => {
        if (!isMenuOpen) {
          setIsMenuOpen(gamificationLevelData?.rank.toLowerCase() || null);
        } else {
          setIsMenuOpen(null);
        }
      }}
    >
      {mainChildren}
      <ChevronDownIcon />
    </GamificationDropdownButton>
    {isMenuOpen && (
      menuChildren
    )}
  </DropdownWrapper>
);

export default GamificationDropdown;
