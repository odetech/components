import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CheckmarkIcon } from '~/components/icons';
import { StyledTooltip } from '~/styles';
import { defaultTheme } from '~/styles/themes';
import { fetchGamificationData } from './redux/gamification';
import { GamificationState } from './types';
import {
  GamificationPositionWrapper,
  OpenButton,
  OpenButtonNotification,
  OpenButtonNotificationWrapper,
  GamificationCardWrapper,
  GamificationCard,
  GamificationCardItemsWrapper,
  CloseButton,
  WalletValue,
  OdeCoinsText,
  InfoItemsWrapper,
  MainInfoItemsWrapper,
  YourRankItemsWrapper,
  RanksWrapper,
  GamificationProgressItemsWrapper,
  ProgressBarItemsWrapper,
  ProgressBarCompletion,
  GamificationHRWrapper,
  GamificationHRLine,
  GamificationHRText,
  NextRankButton,
  NextRankItemsWrapper,
  NextRankPrizeText,
  NextRankPrizeTextValue,
  ProgressBarValue,
  ProgressBarValueWrapper,
  RankTitle,
  RankInfo,
  DropdownMenuItem,
  ButtonsWrapper,
  StaffDataButton,
  RankLeftSideWrapper,
  RankRightSideWrapper,
  RankItemsWrapper,
  RankInfoItemsWrapper,
  RankMainInfoWrapper,
  RankMainInfoTitle,
  RankMainInfoProgressBarItemsWrapper,
  RankMainInfoProgressBarCompletion, RankMainInfoValue, MoreDetailsLink,
} from './GamificationStyles';
import {
  CloseIcon, GrandMasterRankIcon, IncognitoIcon, LegendaryRankIcon, OdeCloudIcon, ProfessorRankIcon, TrophyIcon,
} from './icons';
import GamificationDropdown from './components/dropdown';

const getRankIcon = (rank: string) => {
  switch (rank) {
    case 'grand master':
      return (<GrandMasterRankIcon />);
    case 'professor':
      return (<ProfessorRankIcon />);
    case 'legendary':
      return (<LegendaryRankIcon />);
    default:
      return null;
  }
};

export type GamificationWidgetProps = {
  api: any,
  currentUserData: {
    _id: string,
    profile?: {
      firstName: string | null,
      lastName: string | null,
      roles: {
        _id: string,
        isStaff: boolean,
        isDev?: boolean,
      } | null,
      wallet: {
        userId: string,
        receipts: any,
        wallet: 0,
        rank: string,
        history: any[],
        messagesUnit: any,
        totalMpsUnit?: number | null,
        createdAt: null,
        updatedAt: null,
        completion: any,
      },
    },
  },
  wrapperTop: number,
  wrapperRight: number,
  isStaff: boolean,
  appData?: {
    isOpen?: boolean,
    openDropdownRank?: string | null,
  } | null,
  setAppData?: (appData: {
    isOpen?: boolean,
    openDropdownRank?: string | null,
  } | null) => void,
};

export const GamificationWidget = ({
  api, currentUserData, wrapperTop, wrapperRight, isStaff, appData, setAppData,
}: GamificationWidgetProps) => {
  const dispatch = useDispatch();

  const {
    data: gamificationData,
    loading: gamificationDataLoading,
  } = useSelector((state: GamificationState) => state.gamification);

  const [isOpen, setIsOpen] = useState(false);
  const [gamificationLevels, setGamificationLevels] = useState<any[]>([]);
  const [currentRankData, setCurrentRankData] = useState<any>(null);
  const [openDropdownRank, setOpenDropdownRank] = useState<string | undefined | null>(null);
  const [challenges, setChallenges] = useState<any | undefined | null>(null);

  useEffect(() => {
    dispatch(fetchGamificationData({
      payloadData: currentUserData,
      api,
    }));
  }, [currentUserData]);

  useEffect(() => {
    if (gamificationData?.currentUserData?.profile?.wallet?.completion
      && gamificationData?.currentUserData?.profile?.wallet?.rank
      && gamificationData?.currentUserData?.profile?.wallet?.challenges) {
      const levels: any[] = Object.keys(gamificationData.currentUserData.profile.wallet.completion).map((key) => ({
        ...gamificationData.currentUserData?.profile.wallet.completion[key],
      }));
      setGamificationLevels(levels);
      setCurrentRankData(levels.find((level: any) => (level.rank
        .toLowerCase() === gamificationData.currentUserData?.profile.wallet.rank.toLowerCase())));

      // Challenges
      const challengesData = Object.keys(gamificationData.currentUserData.profile.wallet.challenges).map((key) => (
        gamificationData.currentUserData?.profile.wallet.challenges[key]
      ));
      setChallenges(challengesData);
    }
  }, [gamificationData]);

  useEffect(() => {
    if (appData) {
      if (appData.isOpen !== undefined) {
        setIsOpen(appData.isOpen);
      }
      if (appData.openDropdownRank !== undefined) {
        setOpenDropdownRank(appData.openDropdownRank);
      }

      if (setAppData) {
        setAppData(null);
      }
    }
  }, [appData]);

  const nextRankData = (currentRankData && gamificationLevels.find((gamificationLevel) => (
    gamificationLevel.level === (currentRankData.level + 1)))) || null;
  return (
    <GamificationPositionWrapper
      top={wrapperTop}
      right={wrapperRight}
    >
      {!gamificationDataLoading && !isOpen && currentRankData && (
        <OpenButton
          onClick={() => {
            if (!isOpen) {
              setIsOpen(true);
            }
          }}
          isOpen={isOpen}
        >
          <OpenButtonNotificationWrapper>
            <OpenButtonNotification />
          </OpenButtonNotificationWrapper>
          <TrophyIcon />
        </OpenButton>
      )}
      {!gamificationDataLoading && gamificationData?.currentUserData?.profile?.wallet && currentRankData && (
        <GamificationCardWrapper>
          <GamificationCard
            isOpen={isOpen}
          >
            <GamificationCardItemsWrapper>
              <CloseButton
                onClick={() => {
                  setIsOpen(false);
                }}
              >
                <CloseIcon />
              </CloseButton>
              <MainInfoItemsWrapper>
                <WalletValue>
                  {gamificationData.currentUserData.profile.wallet.wallet?.toLocaleString('en-US')}
                </WalletValue>
                <OdeCoinsText>
                  OdeCoins
                </OdeCoinsText>
              </MainInfoItemsWrapper>
              <InfoItemsWrapper>
                <YourRankItemsWrapper>
                  <GamificationProgressItemsWrapper>
                    <ProgressBarItemsWrapper>
                      <ProgressBarCompletion
                        width={Number(gamificationLevels
                          .find((gamificationLevel) => (
                            gamificationLevel.level === currentRankData?.level))?.percentage || 0
                          .toFixed(2))}
                      >
                        <ProgressBarValueWrapper>
                          <ProgressBarValue
                            id="ProgressBarPercentageValue"
                          >
                            {`${Number(gamificationLevels
                              .find((gamificationLevel) => (
                                gamificationLevel.level === currentRankData?.level))?.percentage || 0
                              .toFixed(2))}%`}
                          </ProgressBarValue>
                        </ProgressBarValueWrapper>
                      </ProgressBarCompletion>
                      {currentRankData?.level !== 0 && (
                        <StyledTooltip
                          target="ProgressBarPercentageValue"
                          placement="top"
                          isOpen={isOpen}
                          key={`progressBarValueTooltip-${isOpen}`}
                        >
                          You got this!
                        </StyledTooltip>
                      )}
                    </ProgressBarItemsWrapper>
                  </GamificationProgressItemsWrapper>
                  <NextRankItemsWrapper>
                    <RankLeftSideWrapper>
                      <NextRankPrizeText>
                        Prize:
                      </NextRankPrizeText>
                      <NextRankPrizeTextValue>
                        {gamificationLevels.find((gamificationLevel) => (
                          gamificationLevel.level === (currentRankData?.level)))?.denominator?.toLocaleString('en-US')}
                        <OdeCloudIcon
                          fill={defaultTheme.text.onPrimary}
                        />
                      </NextRankPrizeTextValue>
                    </RankLeftSideWrapper>
                    <RankRightSideWrapper>
                      <RankInfo>
                        next rank
                      </RankInfo>
                      {nextRankData && (
                        <NextRankButton
                          onClick={() => {
                            if (openDropdownRank !== nextRankData.rank.toLowerCase()) {
                              setOpenDropdownRank(nextRankData.rank.toLowerCase());
                            } else {
                              setOpenDropdownRank(null);
                            }
                          }}
                        >
                          {nextRankData?.rank.toUpperCase() || 'EXPLORER'}
                        </NextRankButton>
                      )}
                    </RankRightSideWrapper>
                  </NextRankItemsWrapper>
                </YourRankItemsWrapper>
                <GamificationHRWrapper>
                  <GamificationHRLine />
                  <GamificationHRText>ranks</GamificationHRText>
                  <GamificationHRLine />
                </GamificationHRWrapper>
                <RanksWrapper>
                  {gamificationLevels.map((gamificationLevel, index) => {
                    let rankInfo: any;
                    if (gamificationData.currentUserData?.profile.wallet.rank
                      .toLowerCase() === gamificationLevel.rank.toLowerCase()) {
                      rankInfo = 'current rank';
                    } else if (currentRankData && currentRankData.level > gamificationLevel.level) {
                      rankInfo = (<CheckmarkIcon />);
                    } else if (currentRankData && (currentRankData.level + 1) === gamificationLevel.level) {
                      rankInfo = 'next rank';
                    }

                    return (
                      <GamificationDropdown
                        gamificationLevelData={gamificationLevel}
                        margin={gamificationLevels.length === (index + 1) ? '0' : '0 0 24px 0'}
                        mainChildren={(
                          <RankItemsWrapper>
                            <RankTitle>
                              {getRankIcon(gamificationLevel.rank.toLowerCase() || '')}
                              {gamificationLevel.rank.toUpperCase() || ''}
                            </RankTitle>
                            <RankInfo>
                              {rankInfo}
                            </RankInfo>
                          </RankItemsWrapper>
                        )}
                        menuChildren={(
                          <div>
                            {challenges && challenges[`${gamificationLevel.level}`].map((challenge: any) => (
                              <DropdownMenuItem>
                                <RankInfoItemsWrapper>
                                  <OdeCloudIcon />
                                  <RankMainInfoWrapper>
                                    <RankMainInfoTitle>
                                      {challenge.fullName}
                                    </RankMainInfoTitle>
                                    <RankMainInfoProgressBarItemsWrapper>
                                      <RankMainInfoProgressBarCompletion
                                        width={challenge.percentage * 100}
                                      />
                                    </RankMainInfoProgressBarItemsWrapper>
                                    <RankMainInfoValue>
                                      {challenge.numerator / challenge.denominator === 1 ? 'Completed'
                                        : `${challenge.numerator}/${challenge.denominator}`}
                                    </RankMainInfoValue>
                                  </RankMainInfoWrapper>
                                </RankInfoItemsWrapper>
                              </DropdownMenuItem>
                            ))}
                          </div>
                        )}
                        isMenuOpen={gamificationLevel.rank.toLowerCase() === openDropdownRank}
                        setIsMenuOpen={setOpenDropdownRank}
                      />
                    );
                  })}
                </RanksWrapper>
                <ButtonsWrapper>
                  <MoreDetailsLink
                    href="https://odecloud.com/gamification/"
                    target="_blank"
                  >
                    More details
                  </MoreDetailsLink>
                  {isStaff && (
                    <StaffDataButton>
                      <IncognitoIcon stroke={defaultTheme.text.onPrimary} />
                    </StaffDataButton>
                  )}
                </ButtonsWrapper>
              </InfoItemsWrapper>
            </GamificationCardItemsWrapper>
          </GamificationCard>
        </GamificationCardWrapper>
      )}
    </GamificationPositionWrapper>
  );
};
