import {
  defaultGamificationRequest,
  getGamificationSuccess,
  defaultGamificationFailure,
} from './gamificationSlice';
import {
  AppThunk,
  errorProps,
} from '../../types';

const errorHandler = (e: any) => {
  console.log(e);
  const error = {} as errorProps;
  return error;
};

export const fetchGamificationData = ({
  payloadData,
  api,
}: {
  payloadData: {
    _id: string,
    profile?: {
      firstName: string | null,
      lastName: string | null,
      roles: {
        _id: string,
        isStaff: boolean,
        isDev?: boolean,
      } | null,
      wallet: {
        userId: string,
        receipts: any,
        wallet: 0,
        rank: string,
        history: any[],
        messagesUnit: any,
        totalMpsUnit?: number | null,
        createdAt: null,
        updatedAt: null,
        completion: any,
      },
    },
  },
  api: any,
}): AppThunk => async (dispatch) => {
  try {
    dispatch(defaultGamificationRequest());

    let currentUserData = JSON.parse(JSON.stringify(payloadData));

    if (!currentUserData?.profile?.wallet) {
      currentUserData = await api.users.getUser(
        currentUserData._id,
        {
          wallet: 1,
        },
      );
    }

    dispatch(getGamificationSuccess({
      currentUserData,
    }));
  } catch (e) {
    const error = await errorHandler(e);
    dispatch(defaultGamificationFailure(error));
  }
};
