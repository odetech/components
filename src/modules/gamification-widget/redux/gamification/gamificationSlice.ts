/* eslint-disable no-param-reassign */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { GamificationState } from '../../types';

const initialState: GamificationState['gamification'] = {
  data: {
    currentUserData: null,
  },
  loading: false,
  error: null,
};

const gamificationSlice = createSlice({
  name: 'gamification',
  initialState,
  reducers: {
    defaultGamificationRequest(state) {
      state.loading = true;
      state.error = null;
    },
    getGamificationSuccess(state, action: PayloadAction<GamificationState['gamification']['data'] | undefined>) {
      if (action.payload) {
        state.data = action.payload;
      }
      state.loading = false;
      state.error = null;
    },
    defaultGamificationFailure(state, action: PayloadAction<GamificationState['gamification']['error']>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  defaultGamificationRequest,
  getGamificationSuccess,
  defaultGamificationFailure,
} = gamificationSlice.actions;

export const gamificationReducer = gamificationSlice.reducer;
