/* eslint-disable react/destructuring-assignment */
import React, { Fragment, useEffect, useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { BeatLoader } from 'react-spinners';
import OdeCloud from '@odecloud/odecloud';
import { Button } from '~/components/default-components';
import testUser from '../../../../configs/testUserCredentials.json';
import { gamificationReducer } from '../redux/gamification/gamificationSlice';
import { GamificationWidget, GamificationWidgetProps } from '../index';

let api: any = null;

const store = configureStore({
  reducer: {
    gamification: gamificationReducer,
  },
});

const meta: Meta<GamificationWidgetProps & {
  loginEmail: string,
  loginPassword: string,
  app: string,
  endpoint: string,
  isStaffMode: boolean,
}> = {
  component: GamificationWidget,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.margin = 'unset';
        // @ts-ignore
        document.getElementById('storybook-root').style.flexDirection = 'unset';
        // @ts-ignore
        document.getElementById('storybook-root').style.justifyContent = 'unset';
      }
      return (
        <Provider store={store}>
          <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
            {story()}
          </MemoryRouter>
        </Provider>
      );
    },
  ],
  argTypes: {
    loginEmail: { name: 'loginEmail', control: 'text' },
    loginPassword: { name: 'loginPassword', control: 'text' },
    app: { name: 'app', control: 'text' },
    endpoint: { name: 'endpoint', control: 'text' },
    isStaffMode: { control: 'boolean' },
  },
  args: {
    loginEmail: testUser.email || '',
    loginPassword: testUser.password || '',
    app: 'odetask',
    endpoint: 'https://server-alpha.odecloud.app/api/v1',
    isStaffMode: false,
  },
};
export default meta;

// 👇 We create a “template” of how args map to rendering
export const Default: StoryFn<GamificationWidgetProps & {
  loginEmail: string,
  loginPassword: string,
  app: string,
  endpoint: string,
  isStaffMode: boolean,
}> = (args) => {
  const [userData, setUserData] = useState<any | null>(null);
  const [appData, setAppData] = useState<any | null>(null);

  const getData = async () => {
    // @ts-ignore
    api = new OdeCloud({
      endpoint: args.endpoint,
    });

    const loginResult = await api.auth.login({
      email: args.loginEmail,
      password: args.loginPassword,
      appUrl: 'http://localhost:3000',
    });
    const { authToken, userId } = loginResult;
    const url = 'https://tasks.odecloud.app';

    await api.resync({ authToken, userId, url });

    const userDataRequest = await api.users.getUser(
      userId,
      {
        roles: 1,
        skillsets: 1,
        masterNotifications: 1,
        appStore: 1,
        profileCompletion: 1,
        wallet: 1,
      },
    );

    setUserData({
      _id: userDataRequest._id,
      profile: {
        ...userDataRequest.profile,
      },
    });
  };

  useEffect(() => {
    getData();
  }, []);

  if (!args.loginEmail || !args.loginPassword || !args.endpoint || !args.app) {
    // If not enough data
    return (
      <span>Please fill in all Controls fields</span>
    );
  } if (userData?._id) {
    // If there is all the data
    return (
      <Fragment>
        <Button
          onClick={() => {
            setAppData({
              isOpen: true,
              openDropdownRank: 'rising star',
            });
          }}
        >
          Learn more
        </Button>
        <GamificationWidget
          {...args}
          api={api}
          currentUserData={userData}
          appData={appData}
          setAppData={setAppData}
        />
      </Fragment>
    );
  }
  // If it's loading
  return (
    <BeatLoader color="#F7942A" size={8} />
  );
};
