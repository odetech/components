import { Action, ThunkAction } from '@reduxjs/toolkit';

export type errorProps = {
  status?: number,
  statusText?: string,
  type?: string,
  url?: string,
  detail: string,
};

export type GamificationState = {
  gamification: {
    data: {
      currentUserData?: {
        _id: string,
        profile: {
          firstName: string | null,
          lastName: string | null,
          roles: {
            _id: string,
            isStaff: boolean,
            isDev?: boolean,
          } | null,
          wallet: {
            userId: string,
            receipts: any,
            wallet: 0,
            rank: string,
            history: any[],
            messagesUnit: any,
            totalMpsUnit?: number | null,
            createdAt: null,
            updatedAt: null,
            completion: any,
            challenges: any,
          },
        }
      } | null,
    },
    loading: boolean,
    error: null | errorProps,
  }
};

export type AppThunk = ThunkAction<void, GamificationState, unknown, Action<string>>;
