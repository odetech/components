/* eslint-disable max-len */
import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';
import { rgba, transparentize } from 'polished';
import { Link } from '~/components/default-components';
import { VerticalScrollbarStyles } from '~/styles';
import { defaultTheme } from '../../styles/themes';

type GamificationPositionWrapperProps = {
  right?: number;
  top?: number;
};

export const GamificationPositionWrapper = styled('div')<GamificationPositionWrapperProps>`
  position: absolute;
  right: ${({ right }) => (right ? `${right}px` : '0')};
  top: ${({ top }) => (top ? `${top}px` : '64px')};
  z-index: 8;
  transition: 0.6s;
`;

export const GamificationCardWrapper = styled('div')`
  width: 100%;
  height: 100%;
  position: relative;
`;

type GamificationCardProps = {
  isOpen: boolean,
};

export const GamificationCard = styled('div')<GamificationCardProps>`
  width: 300px;  
  right: ${({ isOpen }) => (isOpen ? '0' : '-400px')};
  background: linear-gradient(138.34deg, ${defaultTheme.gradient.purple.start} -37.18%, ${defaultTheme.gradient.purple.end} 134.86%);
  border-radius: 72px 0 0 72px;
  position: absolute;
  transition: 0.5s;
`;

export const GamificationCardItemsWrapper = styled('div')`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

type OpenButtonProps = {
  right?: number;
  top?: number;
  isOpen: boolean;
};

export const OpenButton = styled('button')<OpenButtonProps>`
  outline: none;
  border: none;
  position: absolute;
  right: ${({ right }) => ((right || right === 0) ? `${right}px` : '0')};
  top: ${({ top }) => ((top || top === 0) ? `${top}px` : '408px')};
  width: 64px;
  height: 72px;
  background: linear-gradient(126.87deg, ${defaultTheme.gradient.purple.start} -26.07%, ${defaultTheme.gradient.purple.end} 124.76%);
  border-radius: 16px 0 0 16px;
  padding: 20px 16px;
  z-index: ${({ isOpen }) => (!isOpen ? '8' : '0')};

  svg {
    width: 32px;
    height: 32px;
    fill: ${defaultTheme.background.surface};
  }
`;

export const CloseButton = styled('button')`
  outline: none;
  border: none;
  background: transparent;
  position: absolute;
  top: 16px;
  right: 16px;

  svg {
    width: 24px;
    height: 24px;
    fill: ${defaultTheme.background.surface};
  }
`;

export const OpenButtonNotificationWrapper = styled('div')`
  position: relative;
`;

export const OpenButtonNotification = styled('div')`
  position: absolute;
  right: -8px;
  top: -6px;
  width: 14px;
  height: 14px;
  background: ${transparentize(0.5, defaultTheme.background.surface)};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;

  &:after {
    content: '';
    width: 8px;
    height: 8px;
    background: ${defaultTheme.background.surface};
    border-radius: 50%;
  }
`;

export const WalletValue = styled('p')`
  text-align: center;
  font-weight: 700;
  font-size: 56px;
  line-height: 64px;
  margin: 0;
  color: ${defaultTheme.background.surface};
`;

export const OdeCoinsText = styled('p')`
  text-align: center;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  margin: 0 0 8px 0;
  color: ${defaultTheme.text.onPrimary};
`;

export const TodayValue = styled('p')`
  text-align: center;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  margin: 0;
  color: ${defaultTheme.brand.primary};
`;

export const MainInfoItemsWrapper = styled('div')`
  padding: 48px 0 30px 0;
`;

export const InfoItemsWrapper = styled('div')`
  width: 100%;  
  background-image: url('data:image/svg+xml,<svg width="300" height="748" viewBox="0 0 300 748" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 52.4643C0 42.7263 0 37.8574 3.57932 32.0607C7.15865 26.264 10.1173 24.7817 16.0347 21.8171C73.1554 -6.80051 218.843 -7.61827 281.637 21.4573C288.844 24.7945 292.448 26.463 296.224 32.3726C300 38.2821 300 43.7224 300 54.6028V748H0V52.4643Z" fill="black" fill-opacity="0.08"/></svg>');
  background-repeat: no-repeat;
  border-radius: 10% 10% 0 72px;
  padding: 32px 24px 44px 24px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const YourRankItemsWrapper = styled('div')`
  display: flex;
  flex-direction: column;
`;

export const GamificationProgressItemsWrapper = styled('div')`
  width: 252px;
  margin: 90px 0 16px 0;
`;

export const ProgressBarItemsWrapper = styled('div')`
  position: relative;
  height: 16px;
  width: 100%;
  background: ${defaultTheme.background.surface};
  border: unset;
  border-radius: 50px;
`;

type ProfileCompletionProgressBar = {
  width: number,
};

export const ProgressBarCompletion = styled('div', {
  shouldForwardProp: isPropValid,
})<ProfileCompletionProgressBar>`
  width: ${({ width }) => `${width}%`};
  height: 100%;
  transition: width .6s linear;
  background: linear-gradient(90deg, ${defaultTheme.gradient.orange.start} 0%, ${defaultTheme.gradient.orange.end} 100%);
  border-radius: 50px;
  position: relative;
`;

export const ProgressBarValueWrapper = styled('div')`
  position: absolute;
  right: -6px;
  top: -22px;
`;

export const ProgressBarValue = styled('p')`
  position: relative;
  font-weight: 500;
  font-size: 12px;
  line-height: 16px;
  color: ${defaultTheme.text.onPrimary};
  margin: 0;
`;

export const NextRankItemsWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const RankItemsWrapper = styled('div')`
  display: flex;
  align-items: center;
  
  svg {
    width: 16px;
    height: 16px;
    margin: 0 4px 0 0;
    transform: unset;
  }
`;

export const RankLeftSideWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: start;
  gap: 2px;
`;

export const RankRightSideWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: end;
  gap: 2px;
`;

export const RankFlexWrapper = styled('div')`
  display: flex;
  align-items: center;
`;

export const NextRankButton = styled('button')`
  outline: unset;
  border: unset;
  background: ${rgba(defaultTheme.visualisation.white, 0.28)};
  border-radius: 2.27401px;
  padding: 4px 6px;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: ${defaultTheme.text.onPrimary};
  margin: 0;
`;

export const RankInfo = styled('div')`
  font-weight: 500;
  font-size: 12px;
  line-height: 16px;
  color: ${defaultTheme.text.onPrimary};
  margin: 0;
  text-align: center;
  
  svg {
    fill: ${defaultTheme.text.onPrimary};
  }
`;

export const NextRankPrizeText = styled('p')`
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  color: ${defaultTheme.text.onPrimary};
  margin: 0 8px 0 0;
  text-align: center;
`;

export const NextRankPrizeTextValue = styled('div')`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  color: ${defaultTheme.text.onPrimary};
  margin: 0;
  text-align: center;

  svg {
    margin: 0 0 0 4px;
  }
`;

export const GamificationHRWrapper = styled('div')`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 32px 0;
`;

export const GamificationHRLine = styled('div')`
  width: 100%;
  min-width: 24px;
  height: 0.5px;
  background: ${defaultTheme.text.onPrimary};
`;

export const GamificationHRText = styled('p')`
  max-width: calc(100% - 48px);
  padding: 0 8px;
  flex-shrink: 0;
  color: ${defaultTheme.text.onPrimary};
  display: flex;
  justify-content: center;
  margin: 0;
  text-align: center;
  font-weight: 500;
  font-size: 12px;
  line-height: 16px;
`;

export const RanksWrapper = styled('div')`
  width: 100%;
  height: 350px;
  padding: 0 8px 0 0;
  margin: 0 0 24px 0;

  svg {
    width: 12px;
    height: 12px;
  }

  .dropdown {
    width: 100%;

    .btn {
      svg {
        fill: ${defaultTheme.text.onPrimary} !important;
      }
      
      &:hover, &:active, &:focus {
        background: transparent !important;
        color: ${defaultTheme.text.onPrimary} !important;
      }
    }
    
    .dropdown-menu {
      background: transparent !important;
      color: ${defaultTheme.text.onPrimary} !important;
      box-shadow: unset;

      &:hover, &:active, &:focus {
        background: transparent !important;
        color: ${defaultTheme.text.onPrimary} !important;
      }
      
      svg {
        fill: ${defaultTheme.text.onPrimary} !important;
      }
    }
    
    .dropdown-item {
      padding: 0 12px !important;

      &:hover, &:active, &:focus {
        background: transparent !important;
        color: ${defaultTheme.text.onPrimary} !important;
      }
    }
  }

  ${VerticalScrollbarStyles}
`;

type GamificationDropdownButtonProps = {
  isOpen: boolean,
};

export const GamificationDropdownButton = styled('button')<GamificationDropdownButtonProps>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: transparent;
  outline: unset;
  border: none;
  width: 100%;
  padding: 0;

  svg {
    fill: ${defaultTheme.text.onPrimary};
  }

  ${({ isOpen }) => isOpen && `
    margin: 0 0 12px 0;
    svg {
      transform: rotate(-180deg);
    }
  `}
`;

type DropdownWrapperProps = {
  margin?: string,
};

export const DropdownWrapper = styled('div')<DropdownWrapperProps>`
  margin: ${({ margin }) => margin || 0};
`;

export const DropdownMenuItem = styled('div')`
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: ${defaultTheme.background.surface};
  margin: 0 0 4px 0;
  
  span {
    margin: 0 4px 0 0;
  }

  svg {
    width: 18px;
    height: 18px;
    fill: ${defaultTheme.text.onPrimary};
  }
`;

export const RankTitle = styled('div')`
  background: ${rgba(defaultTheme.visualisation.white, 0.28)};
  border-radius: 2.27401px;
  padding: 4px 6px;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: ${defaultTheme.text.onPrimary};
  margin: 0 12px 0 0;
`;

export const ButtonsWrapper = styled('div')`
  display: flex;
  align-items: center;
  gap: 16px;
`;

export const MoreDetailsLink = styled(Link)`
  outline: unset;
  border: unset;
  background: linear-gradient(76.66deg, ${defaultTheme.gradient.orange.start} -14.35%, ${defaultTheme.gradient.orange.end} 103.44%);
  border-radius: 8px;
  padding: 12px 16px;
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  color: ${defaultTheme.text.onPrimary};

  &:hover {
    color: ${defaultTheme.text.onPrimary};
  }
  
  &:disabled {
    cursor: not-allowed;
  }
`;

export const StaffDataButton = styled('button')`
  outline: unset;
  border: unset;
  padding: 10px;
  background: ${transparentize(0.58, defaultTheme.text.onPrimary)};
  border: 1px solid ${defaultTheme.text.onPrimary};
  border-radius: 8px;
  color: ${defaultTheme.text.onPrimary};

  svg {
    width: 28px;
    height: 28px;
  }
`;

export const RankInfoItemsWrapper = styled('div')`
  display: flex;
  gap: 8px;
  
  svg {
    width: 16px;
    height: 16px;
  }
`;

export const RankMainInfoWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const RankMainInfoTitle = styled('p')`
  font-size: 12px;
  font-weight: 500;
  line-height: 16px;
  margin: 0 0 8px 0;
  color: ${defaultTheme.text.onPrimary};
`;

export const RankMainInfoValue = styled('p')`
  font-size: 10px;
  font-weight: 500;
  line-height: 16px;
  color: ${defaultTheme.background.surface};
  margin: 0;
`;

export const RankMainInfoProgressBarItemsWrapper = styled('div')`
  position: relative;
  height: 8px;
  width: 100%;
  background: ${transparentize(0.4, defaultTheme.background.surface)};
  border: unset;
  border-radius: 50px;
  margin: 0 0 4px 0;
`;

type RankMainInfoProfileCompletionProgressBar = {
  width: number,
};

export const RankMainInfoProgressBarCompletion = styled('div', {
  shouldForwardProp: isPropValid,
})<RankMainInfoProfileCompletionProgressBar>`
  width: ${({ width }) => `${width}%`};
  height: 100%;
  transition: width .6s linear;
  background: ${defaultTheme.background.surface};
  border-radius: 50px;
  position: relative;
`;
