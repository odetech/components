import React, { useState } from 'react';
import { reflect } from '@effector/reflect';
import { Collapse } from 'reactstrap';
import { Toggle } from '~/components/default-components';
import {
  AtIcon, ConversationsIcon, ChevronUpIcon,
} from '~/components/icons';
import {
  NewsletterIcon, TasksIcon, ChatIcon, AccountPlusOutlineIcon,
} from '~/modules/notification-widget/icons';
import { MuteSettings } from '~/modules/notification-widget/ui/MuteSettings';
import { useTheme } from '@storybook/theming';
import { defaultTheme } from '../../../styles/themes';
import {
  $masterNotifications, $widgetHeight, MasterNotification, settingsUpdated, mute,
} from '../model';
import {
  SettingsItem, SettingsTitle, SettingsWrapper, Elements, ColumnsName,
} from './styles';

const SettingsComponent = ({
  height,
  masterNotifications,
  updated,
  muteUpdated,
}: {
  height: number,
  masterNotifications: MasterNotification | null,
  updated: (payload: { app?: string; frequency: string; is_active: boolean;}) => void,
  muteUpdated: (payload: { app?: string; status: string; }) => void,
}) => {
  const theme = useTheme();
  const [isOpenSocial, setIsOpenSocial] = useState(false);
  const [isOpenTasks, setIsOpenTasks] = useState(false);
  const [isChat, setIsChat] = useState(false);
  const [isOther, setIsOther] = useState(false);

  const odesocial = masterNotifications?.odesocial?.frequencies || [];
  const odetask = masterNotifications?.odetask?.frequencies || [];

  return (
    <SettingsWrapper height={height}>
      <MuteSettings masterNotifications={masterNotifications} muteUpdated={muteUpdated} />
      <SettingsTitle
        type="button"
        isOpen={isOpenSocial}
        onClick={() => setIsOpenSocial((prevState) => !prevState)}
      >
        <span>OdeSocial</span>
        <ChevronUpIcon />
      </SettingsTitle>
      <Collapse isOpen={isOpenSocial}>
        <Elements>
          <SettingsItem>
            <div>
              <ConversationsIcon stroke={theme?.text?.onSurface || defaultTheme.text.onSurface} />
              <span>All post comments & replies</span>
            </div>
            <div>
              <ColumnsName isOpen={isOpenSocial}>
                <span>In-app</span>
                <span>Email</span>
              </ColumnsName>
              <Toggle
                checked={odesocial.includes('ac')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'ac',
                    is_active: !odesocial.includes('ac'),
                  });
                }}
              />
              <Toggle
                checked={odesocial.includes('ec')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'ec',
                    is_active: !odesocial.includes('ec'),
                  });
                }}
              />
            </div>
          </SettingsItem>
          <SettingsItem>
            <div>
              <AtIcon />
              <span>Only mentions</span>
            </div>
            <div>
              <Toggle
                checked={odesocial.includes('ab')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'ab',
                    is_active: !odesocial.includes('ab'),
                  });
                }}
              />
              <Toggle
                checked={odesocial.includes('eb')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'eb',
                    is_active: !odesocial.includes('eb'),
                  });
                }}
              />
            </div>
          </SettingsItem>
          <SettingsItem>
            <div>
              <AccountPlusOutlineIcon />
              <span>Network request</span>
            </div>
            <div>
              <Toggle
                checked={odesocial.includes('ad')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'ad',
                    is_active: !odesocial.includes('ad'),
                  });
                }}
              />
              <Toggle
                checked={odesocial.includes('ai')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'ai',
                    is_active: !odesocial.includes('ai'),
                  });
                }}
              />
            </div>
          </SettingsItem>
        </Elements>
      </Collapse>
      <SettingsTitle
        type="button"
        isOpen={isOpenTasks}
        onClick={() => setIsOpenTasks((prevState) => !prevState)}
      >
        <span>OdeTask</span>
        <ChevronUpIcon />
      </SettingsTitle>
      <Collapse isOpen={isOpenTasks}>
        <Elements>
          <SettingsItem>
            <div>
              <TasksIcon stroke="transparent" />
              <span>All tasks comments & replies</span>
            </div>
            <div>
              <ColumnsName isOpen={isOpenTasks}>
                <span>In-app</span>
                <span>Email</span>
              </ColumnsName>
              <Toggle
                checked={odetask.includes('ac')}
                onChange={() => {
                  updated({
                    app: 'odetask',
                    frequency: 'ac',
                    is_active: !odetask.includes('ac'),
                  });
                }}
              />
              <Toggle
                checked={odetask.includes('ec')}
                onChange={() => {
                  updated({
                    app: 'odetask',
                    frequency: 'ec',
                    is_active: !odetask.includes('ec'),
                  });
                }}
              />
            </div>
          </SettingsItem>
          <SettingsItem>
            <div>
              <AtIcon />
              <span>Only mentions</span>
            </div>
            <div>
              <Toggle
                checked={odetask.includes('ab')}
                onChange={() => {
                  updated({
                    app: 'odetask',
                    frequency: 'ab',
                    is_active: !odetask.includes('ab'),
                  });
                }}
              />
              <Toggle
                checked={odetask.includes('eb')}
                onChange={() => {
                  updated({
                    app: 'odetask',
                    frequency: 'eb',
                    is_active: !odetask.includes('eb'),
                  });
                }}
              />
            </div>
          </SettingsItem>
          <SettingsItem>
            <div>
              <TasksIcon stroke="transparent" />
              <span>Task assigned to me</span>
            </div>
            <div>
              <Toggle
                checked={odetask.includes('ba')}
                onChange={() => {
                  updated({
                    app: 'odetask',
                    frequency: 'ba',
                    is_active: !odetask.includes('ba'),
                  });
                }}
              />
              <Toggle
                hide
                checked={odetask.includes('eb')}
                onChange={() => {
                  updated({
                    app: 'odetask',
                    frequency: 'eb',
                    is_active: !odetask.includes('eb'),
                  });
                }}
              />
            </div>
          </SettingsItem>
        </Elements>
      </Collapse>
      <SettingsTitle
        type="button"
        isOpen={isChat}
        onClick={() => setIsChat((prevState) => !prevState)}
      >
        <span>Direct chat</span>
        <ChevronUpIcon />
      </SettingsTitle>
      <Collapse isOpen={isChat}>
        <Elements>
          <SettingsItem>
            <div>
              <ChatIcon />
              <span>Chat mentions</span>
            </div>
            <div>
              <ColumnsName isOpen={isChat}>
                <span>In-app</span>
                <span>Email</span>
              </ColumnsName>
              <Toggle
                checked={odesocial.includes('aa')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'aa',
                    is_active: !odesocial.includes('aa'),
                  });
                }}
              />
              <Toggle
                checked={odesocial.includes('ag')}
                onChange={() => {
                  updated({
                    app: 'odesocial',
                    frequency: 'ag',
                    is_active: !odesocial.includes('ag'),
                  });
                }}
              />
            </div>
          </SettingsItem>
        </Elements>
      </Collapse>
      <SettingsTitle
        type="button"
        isOpen={isOther}
        onClick={() => setIsOther((prevState) => !prevState)}
      >
        <span>Marketing emails</span>
        <ChevronUpIcon />
      </SettingsTitle>
      <Collapse isOpen={isOther}>
        <Elements>
          <SettingsItem>
            <div>
              <NewsletterIcon />
              <span>Newsletter</span>
            </div>
            <Toggle
              checked={!!masterNotifications?.frequencies?.includes('af')}
              onChange={() => {
                updated({
                  app: 'frequencies',
                  frequency: 'af',
                  is_active: !masterNotifications?.frequencies?.includes('af'),
                });
              }}
            />
          </SettingsItem>
        </Elements>
      </Collapse>
    </SettingsWrapper>
  );
};

export const Settings = reflect({
  bind: {
    height: $widgetHeight,
    masterNotifications: $masterNotifications,
    updated: settingsUpdated,
    muteUpdated: mute,
  },
  view: SettingsComponent,
});
