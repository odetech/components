import React, { Fragment } from 'react';
import { useUnit } from 'effector-react';
import { reflect } from '@effector/reflect';
import { EyeIcon, EyeOffIcon } from '~/components/icons';
import { ThemeProps } from '~/styles/types';
import {
  ShowReadButton, Tab, TabsRow, TabsWrapper,
} from './styles';
import * as $$notificationsModel from '../model';
import { Elements } from './elements';

const ListComponent = reflect({
  view: Elements,
  bind: {
    elements: $$notificationsModel.$elements,
    loading: $$notificationsModel.$loading,
    height: $$notificationsModel.$widgetHeight,
    hasNextPage: $$notificationsModel.$hasNextPage,
    isEmpty: $$notificationsModel.$isEmpty,
    readOne: $$notificationsModel.readOne,
    callbacks: $$notificationsModel.$callbacks,
    currentApp: $$notificationsModel.$currentTab,
    pageChanged: $$notificationsModel.pageChanged,
  },
});

export const List = ({ theme }: { theme: ThemeProps }) => <ListComponent theme={theme} />;

export const Notifications = ({ theme }: { theme: ThemeProps}) => {
  const {
    isShowRead, toggleRead, isSocialTab, isTasksTab, setSocialTab, setTasksTab, setChatTab, isChatTab,
  } = useUnit({
    isShowRead: $$notificationsModel.$isShowRead,
    toggleRead: $$notificationsModel.isShowReadToggled,
    isSocialTab: $$notificationsModel.$isSocialTab,
    isTasksTab: $$notificationsModel.$isTasksTab,
    setSocialTab: $$notificationsModel.socialTabSet,
    setTasksTab: $$notificationsModel.tasksTabSet,
    setChatTab: $$notificationsModel.chatTabSet,
    isChatTab: $$notificationsModel.$isChatTab,
  });
  return (
    <Fragment>
      <TabsRow>
        <TabsWrapper>
          <Tab isActive={isSocialTab} onClick={setSocialTab}>OdeSocial</Tab>
          <Tab isActive={isTasksTab} onClick={setTasksTab}>OdeTasks</Tab>
          <Tab isActive={isChatTab} onClick={setChatTab}>Chat</Tab>
        </TabsWrapper>
        <ShowReadButton onClick={toggleRead}>
          <span>
            {`${isShowRead ? 'Hide ' : 'Show '} read`}
          </span>
          {isShowRead ? <EyeOffIcon /> : <EyeIcon />}
        </ShowReadButton>
      </TabsRow>
      <hr />
      <List theme={theme} />
    </Fragment>
  );
};
