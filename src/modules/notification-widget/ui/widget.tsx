import React, { useEffect } from 'react';
import { useUnit } from 'effector-react';
import { variant } from '@effector/reflect';
import { DragIcon } from '~/components/icons';
import { defaultTheme } from '~/styles/themes';
import { ThemeProps } from '~/styles/types';
import { Wrapper, DragButton } from './styles';
import * as $$notificationModel from '../model';
import { Notifications } from './notifications';
import { Settings } from './settings';
import { NotificationHeader, SettingsHeader } from './headers';
import { Callbacks, Tabs } from '../model';
import '../model/channels';

export type WidgetProps = {
  defaultTab?: Tabs;
  callbacks?: Callbacks;
  isOpen: boolean;
  theme: ThemeProps;
};

const Window = variant({
  if: $$notificationModel.$isNotificationWindow,
  then: (props: { theme: ThemeProps }) => <Notifications {...props} />,
  else: () => <Settings />,
});

const Header = variant({
  if: $$notificationModel.$isNotificationWindow,
  then: NotificationHeader,
  else: SettingsHeader,
});

export const Widget = ({
  defaultTab,
  isOpen,
  callbacks,
  theme = defaultTheme,
}: WidgetProps) => {
  const {
    widgetInit, setSocialTab, setTasksTab, setChatTab,
  } = useUnit({
    widgetInit: $$notificationModel.widgetStarted,
    setSocialTab: $$notificationModel.socialTabSet,
    setTasksTab: $$notificationModel.tasksTabSet,
    setChatTab: $$notificationModel.chatTabSet,
  });

  useEffect(() => {
    widgetInit({ callbacks });
  }, [callbacks, widgetInit]);

  useEffect(() => {
    if (isOpen) {
      switch (defaultTab) {
        case 'odetask':
          setTasksTab();
          break;
        case 'odesocial':
          setSocialTab();
          break;
        case 'chat':
          setChatTab();
          break;
        default:
          setSocialTab();
          break;
      }
    }
  }, [defaultTab, isOpen, setSocialTab, setTasksTab]);

  const mouseDowned = useUnit($$notificationModel.mouseDowned);
  return (
    <Wrapper>
      <Header />
      <Window theme={theme} />
      <DragButton type="button" onMouseDown={mouseDowned}>
        <DragIcon />
      </DragButton>
    </Wrapper>
  );
};
