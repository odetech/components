import React from 'react';
import { useUnit } from 'effector-react';
import { DotsIcon, ArrowLeftIcon } from '~/components/icons';
import { StyledDropdownMenu } from '~/styles/common';
import { CheckIcon, SettingsIcon } from '../icons';
import {
  settingsOpened, notificationsOpened, readAllMade, $isEmpty,
} from '../model';
import {
  Dropdown,
  DropdownButton,
  DropdownMenuItem,
  Title,
  TitleButton,
} from './styles';

export const NotificationHeader = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  const openSettings = useUnit(settingsOpened);
  const readAll = useUnit(readAllMade);
  const isEmpty = useUnit($isEmpty);
  return (
    <Title>
      <span>Notifications</span>
      <Dropdown
        isOpen={isOpen}
        toggle={() => {
          setIsOpen(!isOpen);
        }}
      >
        <DropdownButton
          isOpen={isOpen}
        >
          <DotsIcon />
        </DropdownButton>
        <StyledDropdownMenu>
          <DropdownMenuItem
            disabled={isEmpty}
            onClick={readAll}
          >
            <CheckIcon />
            <span>Mark all as read</span>
          </DropdownMenuItem>
          <DropdownMenuItem
            onClick={openSettings}
          >
            <SettingsIcon />
            <span>Settings</span>
          </DropdownMenuItem>
        </StyledDropdownMenu>
      </Dropdown>
    </Title>
  );
};

export const SettingsHeader = () => {
  const openNotifications = useUnit(notificationsOpened);
  return (
    <Title>
      <TitleButton onClick={openNotifications}>
        <ArrowLeftIcon />
        <span>Notifications settings</span>
      </TitleButton>
    </Title>
  );
};
