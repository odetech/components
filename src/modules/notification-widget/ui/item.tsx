/* eslint-disable no-underscore-dangle */
import React, { useEffect } from 'react';
import { formatDistanceToNowStrict } from 'date-fns';
import he from 'he';
import { Avatar } from '~/components/avatar';
import {
  NotificationDropdownItem,
  NotificationItemWrap,
  NotificationAvatarWrap,
  NotificationItemInfo,
  NotificationItemTitle,
  NotificationLabel,
  NotificationItemDate,
} from '~/modules/notification-widget/ui/styles';
import { ReactionData, Reaction } from '~/entities/reactions';
import { ThemeProps } from '~/styles/types';
import { useTheme } from '@storybook/theming';
import { defaultTheme } from '~/styles/themes';
import { ensureUtcTimestamp } from '~/helpers';
import {
  AccountPlusOutlineIcon,
  AtIcon,
  ConversationsIcon,
  LetterOutlineIcon,
  MessageOutlineIcon,
  ListWithTickIcon,
} from '../icons';
import { Callbacks, Tabs } from '../model';

type NotificationItemProps = {
  notification: any;
  callbacks: Callbacks;
  reaction?: ReactionData;
  wasSeen: boolean;
  setWasSeen: any;
  app: string;
  background: string;
  readOne: (_id: string) => void;
};

export const notificationTypes = {
  chat: 'aa',
  mention: 'ab',
  comment: 'ac',
  request: 'ad',
  like: 'ae',
  email: 'ag',
  taskAssignment: 'ba',
};

const getNotificationColorByReaction = (theme: ThemeProps, reaction?: string | null) => {
  switch (reaction) {
    case 'likes':
      return theme?.info?.primaryContainer || defaultTheme.info.primaryContainer;
    case 'fun':
      return theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar;
    case 'insightful':
      return theme?.brand?.primaryContainer || defaultTheme.brand.primaryContainer;
    case 'important':
      return theme?.brand?.secondaryContainer || defaultTheme.brand.secondaryContainer;
    case 'helpful':
      return theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar;

    default:
      return theme?.visualisation?.warmRed || defaultTheme.visualisation.warmRed;
  }
};

const getNotificationColor = (app: string, theme: ThemeProps, reaction?: string | null) => {
  switch (app) {
    case 'odetask':
      return `linear-gradient(122deg, ${theme?.gradient?.pink?.start || defaultTheme.gradient.pink.start} -8.69%, 
        ${theme?.gradient?.pink?.end || defaultTheme.gradient.pink.end} 145.88%)`;
    case notificationTypes.like:
      return getNotificationColorByReaction(theme, reaction);
    case 'odesocial':
      return `linear-gradient(133deg, ${theme?.gradient?.blue?.start || defaultTheme.gradient.blue.start}
        8.06%, ${theme?.gradient?.blue?.end || defaultTheme.gradient.blue.end} 68.71%)`;
    default:
      return `linear-gradient(133deg, ${theme?.gradient?.blue?.start || defaultTheme.gradient.blue.start}
        8.06%, ${theme?.gradient?.blue?.end || defaultTheme.gradient.blue.end} 68.71%)`;
  }
};

const notificationIcons = {
  [notificationTypes.comment]: ConversationsIcon,
  [notificationTypes.mention]: AtIcon,
  [notificationTypes.request]: AccountPlusOutlineIcon,
  [notificationTypes.chat]: MessageOutlineIcon,
  [notificationTypes.email]: LetterOutlineIcon,
  [notificationTypes.taskAssignment]: ListWithTickIcon,
};

const getNotificationIcon = (type: string, reactionData?: ReactionData) => {
  const IconComponent = notificationIcons[type] || ConversationsIcon;
  // For the 'like' type, we need to pass the reaction to chooseReactionIcon
  if (type === notificationTypes.like && reactionData) {
    return <Reaction reactionData={reactionData} />;
  }
  return <IconComponent />;
};

export const Item = ({
  notification, app, readOne, callbacks, wasSeen, setWasSeen, background, reaction,
}: NotificationItemProps) => {
  const theme = useTheme();
  const {
    type, content, isRead, data, createdAt, _id, title,
  } = notification;

  // Pass the content through he.decode first.
  // he (HTML Entity) will decode &amp; &gt; &lt; etc and convert it to safe symbols.
  const decodedContent = he.decode(content);

  useEffect(() => {
    if (!wasSeen) {
      setTimeout(() => {
        setWasSeen((prevState: any) => ({
          ...prevState,
          [_id]: true,
        }));
      }, 1500);
    }
  }, [wasSeen]);

  const isRequestType = type === notificationTypes.request;

  const onNotificationClick = (event: any) => {
    event.preventDefault();
    if (app === Tabs.OdeSocial && callbacks?.odesocial.onClick) {
      callbacks.odesocial.onClick(notification);
    }
    if (app === Tabs.OdeTasks && callbacks?.odetask.onClick) {
      callbacks.odetask.onClick(notification);
    }
    if (app === Tabs.Chat && callbacks?.chat.onClick) {
      callbacks.chat.onClick(notification);
    }
    if (!isRead) {
      readOne(_id);
    }
    if (callbacks?.handleClose) callbacks.handleClose();
  };

  return (
    <NotificationItemWrap>
      <NotificationDropdownItem
        onClick={(event) => onNotificationClick(event)}
        background={!isRead ? background : 'transparent'}
      >
        <NotificationAvatarWrap isRead={isRead}>
          <Avatar
            size={44}
            user={notification?.data?.senderUserId || {
              profile: {
                firstName: 'Undefined',
                lastName: 'Undefined',
              },
            }}
          />
          <NotificationLabel
            type={type}
            app={app}
            background={getNotificationColor(app, theme, data?.reaction)}
          >
            {getNotificationIcon(type, reaction)}
          </NotificationLabel>
        </NotificationAvatarWrap>
        <NotificationItemInfo type={type} isRead={isRead}>
          <NotificationItemTitle>
            <span>
              {title}
              {!isRequestType && ':'}
            </span>
            <div>
              {decodedContent}
            </div>
          </NotificationItemTitle>
          <NotificationItemDate>
            {formatDistanceToNowStrict(new Date(ensureUtcTimestamp(createdAt)), {
              addSuffix: false,
            })}
            {' '}
            ago
          </NotificationItemDate>
        </NotificationItemInfo>
      </NotificationDropdownItem>
    </NotificationItemWrap>
  );
};
