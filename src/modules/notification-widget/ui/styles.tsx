import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';
import { Button } from '~/components/default-components';
import { getVerticalScrollbarStyles, StyledDropdown, StyledDropdownItem } from '~/styles/common';
import { CustomDropdownToggle } from '~/components/default-components/Dropdown/DropdownStyles';
import { defaultTheme } from '../../../styles/themes';

export const Wrapper = styled.div`
  border-radius: 12px;
  border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  width: 480px;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.1);

  hr {
    margin: 3px 0;
  }
`;

export const Title = styled.div`
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 24px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 16px 24px 0;
  margin-bottom: 12px;
`;

export const TitleButton = styled(Button)`
  background: transparent;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 24px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0;    
  gap: 12px;

  &:hover, &:focus, &:active {
    background: transparent;
  }
    
  svg {
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }
`;

export const DropdownButton = styled(CustomDropdownToggle)`
  height: 24px;
  padding: 0;
  border-radius: 0;
  background: transparent;

  &:hover,
  &:active,
  &:focus {
    background: transparent !important;
  }

  svg {
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    transform: rotate(0deg);
  }
`;

export const Tab = styled(Button)<{ isActive: boolean }>`
  padding: 0;
  white-space: nowrap;
  border-radius: 0;
  position: relative;
  font-size: 14px;
  font-style: normal;
  font-weight: 500;
  line-height: 20px;
  background: transparent;
  color: ${({ isActive, theme }) => (isActive
    ? (theme?.brand?.primary || defaultTheme.brand.primary)
    : (theme?.text?.onSurface || defaultTheme.text.onSurface))};

  &:hover,
  &:active,
  &:focus {
    background: transparent;
    color: ${({ isActive, theme }) => (isActive
    ? (theme?.brand?.primary || defaultTheme.brand.primary)
    : (theme?.text?.onSurface || defaultTheme.text.onSurface))};
  }

  ${({ isActive, theme }) => isActive
    && `
    &:after {
      content: '';
      position: absolute;
      height: 2px;
      width: 100%;
      background: ${theme?.brand?.primary || defaultTheme.brand.primary};
      bottom: -4px;
    }
  `}
`;

export const TabsWrapper = styled('div')`
  display: flex;
  gap: 16px;
  align-items: center;
`;

export const TabsRow = styled('div')`
  display: flex;
  justify-content: space-between;
  padding: 0px 24px 0;
`;

export const ShowReadButton = styled(Button)`
  background: transparent;
  white-space: nowrap;
  padding: 0;
  font-size: 14px;
  font-style: normal;
  font-weight: 500;
  line-height: 20px;
  display: flex;
  color: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
  align-items: center;
  gap: 6px;

  svg {
    fill: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
  }

  &:hover,
  &:active,
  &:focus {
    background: transparent;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
      
    svg {
      fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    }  
  }
`;

export const NotificationItemWrap = styled('div')`
  position: relative;
  width: 100%;
`;

export const NotificationSettingsItem = styled('div')`
  padding: 8px 24px 8px 20px !important;
  display: flex;
  border-bottom: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  align-items: flex-start;

  label > span {
    margin: 0;
  }

  &:last-of-type:not(:first-of-type) {
    border: none;
  }
`;

export const NotificationDropdownItem = styled(
  NotificationSettingsItem,
)<{ background: string;}>`
  transition: 0.3s;
  cursor: pointer;
  position: relative;
  width: 100%;
  background-color: ${({ background }) => background};

  &:hover {
    background: ${({ theme }) => theme?.brand?.primaryContainer || defaultTheme.brand.primaryContainer};
  }

  &:active {
    background: ${({ theme }) => theme?.brand?.primaryContainer || defaultTheme.brand.primaryContainer} !important;
  }
`;

export const NotificationAvatarWrap = styled('div')<{ isRead: boolean }>`
  flex-shrink: 1;
  position: relative;
  height: 44px;
  ${({ isRead }) => (isRead ? 'opacity: 0.6;' : '')};  

  & > span:last-of-type {
    position: absolute;
    right: 0;
    bottom: 0;
  }


  &:before {
    content: "";
    display: ${({ isRead }) => (!isRead ? 'block' : 'none')};
    height: 4px;
    width: 4px;
    border-radius: 50%;
    background: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: -10px;
  }
`;

type NotificationLabelProps = {
  type: string;
  app: string;
  background: string;
  reaction?: string | null;
};

export const NotificationLabel = styled('span', {
  shouldForwardProp: isPropValid,
})<NotificationLabelProps>`
  border-radius: 50%;
  height: 24px;
  width: 24px;
  background: ${(props) => props.background};
  border: 1px solid ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  display: flex;
  align-items: center;
  justify-content: center;

  svg,
  img {
    fill: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
    stroke: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
    stroke-width: 0;
    height: 15px;
    width: 15px;
  }
`;

export const NotificationItemInfo = styled('div', {
  shouldForwardProp: isPropValid,
})<{ type: string; isRead: boolean; }>`
  margin-left: 16px;
  max-width: calc(100% - 60px);
  ${({ isRead }) => (isRead ? 'opacity: 0.8;' : '')};
`;

export const NotificationItemTitle = styled('div')`
  font-size: 14px;
  line-height: 21px;
  width: 100%;
  margin-bottom: 4px;
  white-space: break-spaces;
  max-width: 100%;
  overflow: hidden;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  font-weight: 400;

  & > span {
    font-weight: 500;
    font-size: 14px;
    line-height: normal;
    display: block; 
    margin-bottom: 4px;
    overflow: hidden;  
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
      
    & > span {
      font-weight: 500;
    }
  }
    
  & > div {
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }
`;

export const ItemsWrapper = styled('div')<{ isEmpty: boolean }>`
  width: 100%;

  & > div {
    overflow: auto;
    ${({ theme }) => getVerticalScrollbarStyles(theme)}
  }
    
  ${({ isEmpty }) => isEmpty && `
    display: flex;
    align-items: center;
    justify-content: center;
  `}  
`;

export const CenterPositionWrapper = styled('div')`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
    
  svg {
    width: 120px;
    height: 120px;
    margin: 0 0 40px 0;
  }  
`;

export const NoNotificationsTitle = styled('p')`
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  text-align: center;
  font-size: 14px;
  font-weight: 500;
  margin: 0 0 8px 0;
`;

export const NoNotificationsDescription = styled('p')`
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  text-align: center;
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  margin: 0;
`;

export const DragButton = styled(Button)`
  height: 24px;
  width: 100%;
  border-radius: 0px 0px 12px 12px;
  border-top: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};
    
  &:hover, &:focus {
    background: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  }

  &:active {
    background: ${({ theme }) => theme?.background?.surfaceContainerVar
     || defaultTheme.background.surfaceContainerVar}; 
  }  
`;

export const NotificationItemDate = styled('p')`
  font-size: 10px;
  margin-bottom: 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
`;

export const Dropdown = styled(StyledDropdown)``;

export const DropdownMenuItem = styled(StyledDropdownItem)`
  padding: 5px 20px !important;
  margin: 0;
  display: flex;
  align-items: center;

  &:hover, &:active, &:focus {
    background: transparent;
  }  
`;

export const SettingsTitle = styled('button')<{ isOpen: boolean}>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: transparent;
  width: 100%;
  box-shadow: none;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  font-size: 16px;
  font-weight: 400;
  line-height: 24px;
  border: 0;
  padding: 16px 24px;

  border-top: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};

  &:hover, &:active, &:focus {
    background: transparent;
    box-shadow: none;
  }
    
  svg {
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    transition: 0.3s;    
    ${({ isOpen }) => !isOpen && `
      transform: rotate(180deg);
    `}
  }  
`;

export const SettingsItem = styled('div')<{ bottom?: boolean, top?: boolean}>`
  display: flex;
  justify-content: space-between;
  gap: 16px;
  padding: 12px 0;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  ${({ bottom, theme }) => (bottom
    ? `border-bottom: 1px solid ${theme?.outline?.[100] || defaultTheme.outline[100]};` : '')}
  ${({ top, theme }) => (top
    ? `border-top: 1px solid ${theme?.outline?.[100] || defaultTheme.outline[100]};` : '')}
    
  div {
    position: relative;  
    display: flex;
    align-items: center;
    justify-content: flex-start;
    gap: 8px;

    svg {
      fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    }
  }
    
  label {
    justify-content: center;
  } 
    
  & > div:last-child {
    gap: 16px;
  }  
    
`;

export const SettingsWrapper = styled('div')<{height: number }>`
  height: ${({ height }) => height}px;
  overflow: scroll;
`;

export const GroupTitle = styled('p')`
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  padding: 12px 24px 6px;
  margin: 0;
  font-size: 12px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;
`;

export const Elements = styled('div')`
  padding: 0 24px;
  margin-top: 8px;  
`;

export const MuteItem = styled(SettingsItem)`
  padding: 16px 24px;
  margin: 0;  
`;

export const ColumnsName = styled('div')<{ isOpen: boolean }>`
  position: absolute !important;
  display: flex;
  align-items: center;
  white-space: nowrap;
  top: -24px;
  right: 0px;
  opacity: ${({ isOpen }) => (isOpen ? 1 : 0)};  
  transition: opacity 500ms;
  gap: 16px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  text-align: right;
  font-size: 12px;
  font-weight: 500;
`;
