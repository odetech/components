import React from 'react';
import { MuteItem } from '~/modules/notification-widget/ui/styles';
import { BellMuteIcon } from '~/components/icons';
import { Toggle } from '~/components/default-components';
import { MasterNotification } from '~/modules/notification-widget/model';

type MuteSettingsProps = {
  masterNotifications: MasterNotification | null,
  muteUpdated: (payload: { app?: string; status: string; }) => void,
}

export const MuteSettings = ({ masterNotifications, muteUpdated }: MuteSettingsProps) => {
  const allMute = !masterNotifications || (masterNotifications?.odetask?.frequencies.length === 0
    && masterNotifications?.odesocial?.frequencies.length === 0);
  return (
    <MuteItem top>
      <div>
        <BellMuteIcon />
        <span>Turn off all notifications</span>
      </div>
      <Toggle
        checked={allMute}
        onChange={() => {
          muteUpdated({
            status: allMute ? 'unmute' : 'mute',
          });
        }}
      />
    </MuteItem>
  );
};
