import React, { Fragment } from 'react';
import { BeatLoader } from 'react-spinners';
import { isToday } from 'date-fns';
import { useUnit } from 'effector-react/effector-react.umd';
import { $$reactionModel } from '~/entities/reactions';
import { useVirtualizer } from '@tanstack/react-virtual';
import { defaultTheme } from '~/styles/themes';
import { ensureUtcTimestamp } from '~/helpers';
import { ThemeProps } from '~/styles/types';
import { Item } from './item';
import {
  CenterPositionWrapper,
  ItemsWrapper,
  NoNotificationsTitle,
  NoNotificationsDescription,
  GroupTitle,
} from './styles';
import { NoNewNotificationsIcon } from './NoNewNotificationsIcon';
import { Notification, Callbacks } from '../model';

type NotificationListProps = {
  elements: Notification[];
  hasNextPage: boolean;
  loading: boolean;
  isEmpty: boolean;
  currentApp: string;
  height: number;
  pageChanged: () => void;
  readOne: (_id: string) => void;
  callbacks: Callbacks;
  theme: ThemeProps;
}

export const Elements = ({
  elements, loading, isEmpty, height, hasNextPage, currentApp, pageChanged, readOne, callbacks, theme,
}: NotificationListProps) => {
  const [wasSeen, setWasSeen] = React.useState<Record<string, boolean>>({});
  const parentRef = React.useRef<HTMLDivElement>(null);
  const rowVirtualizer = useVirtualizer({
    count: hasNextPage ? elements.length + 1 : elements.length,
    getScrollElement: () => parentRef.current,
    estimateSize: () => 145,
    enabled: true,
  });

  React.useEffect(() => {
    const [lastItem] = [...rowVirtualizer.getVirtualItems()].reverse();

    if (!lastItem) {
      return;
    }

    if (
      lastItem.index >= elements.length - 1
      && hasNextPage
      && !loading
    ) {
      pageChanged();
    }
  }, [
    hasNextPage,
    pageChanged,
    elements.length,
    loading,
    rowVirtualizer.getVirtualItems(),
  ]);

  const reactionsData = useUnit($$reactionModel.$reactions);
  const items = rowVirtualizer.getVirtualItems();
  return (
    <ItemsWrapper isEmpty={isEmpty}>
      {!loading && isEmpty && (
        <CenterPositionWrapper>
          <NoNewNotificationsIcon />
          <NoNotificationsTitle>
            No unread notifications
          </NoNotificationsTitle>
          <NoNotificationsDescription>
            You’ll be notified when something happens
          </NoNotificationsDescription>
        </CenterPositionWrapper>
      )}
      {!isEmpty && (
        <div
          ref={parentRef}
          className="List"
          style={{
            height: `${height}px`,
            width: '100%',
            overflow: 'auto',
            contain: 'strict',
          }}
        >
          <div
            style={{
              height: `${rowVirtualizer.getTotalSize()}px`,
              width: '100%',
              position: 'relative',
            }}
          >

            <div
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                transform: `translateY(${items[0]?.start ?? 0}px)`,
              }}
            >
              {items.map((virtualRow) => {
                const isLoaderRow = virtualRow.index > elements.length - 1;
                const i = virtualRow.index;
                const item = elements[virtualRow.index];
                const currentDate = item?.createdAt || '';
                const previousDate = elements[virtualRow.index - 1]?.createdAt ?? '';
                const today = isToday(
                  new Date(ensureUtcTimestamp(currentDate)),
                );
                const previous = isToday(
                  new Date(ensureUtcTimestamp(previousDate)),
                );
                return (
                  <div
                    key={virtualRow.index}
                    className={
                      virtualRow.index % 2 ? 'ListItemOdd' : 'ListItemEven'
                    }
                    ref={rowVirtualizer.measureElement}
                  >
                    {isLoaderRow && hasNextPage ? (
                      <CenterPositionWrapper style={{ height: '145px' }}>
                        <BeatLoader color={theme?.brand?.secondary || defaultTheme.brand.secondary} />
                      </CenterPositionWrapper>

                    ) : null}
                    {!isLoaderRow && item._id ? (
                      <Fragment key={item._id}>
                        {i === 0 && today && <GroupTitle>Today</GroupTitle>}
                        {i !== 0 && !today && previous && <GroupTitle>Older</GroupTitle>}
                        <Item
                          notification={item}
                          key={item._id}
                          app={currentApp}
                          reaction={reactionsData.find((r) => r.value === item.data?.reaction)}
                          readOne={readOne}
                          background={wasSeen[item._id]
                            ? 'transparent' : theme?.brand?.primaryContainer || defaultTheme.brand.primaryContainer}
                          callbacks={callbacks}
                          wasSeen={wasSeen[item._id]}
                          setWasSeen={setWasSeen}
                        />
                      </Fragment>
                    ) : null}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )}
    </ItemsWrapper>
  );
};
