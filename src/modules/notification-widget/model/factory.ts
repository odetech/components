import {
  attach, combine, createEvent, createStore, sample,
} from 'effector';
import {
  either, not, spread,
} from 'patronum';
import { $$usersModel } from '~/entities/users';
import { $api, $currentUser } from '~/config';
import { fromArrayToHash } from '~/helpers/globalHelpers';
import {
  fetchFx, readAllFx, readOneFx, fetchNotificationFx, getCreatedByFx,
} from './api';
import { $isShowRead } from './read';
import {
  $currentTab, $isSocialTab, $isTasksTab, $isChatTab,
} from './tab';
import { Notification } from './types';

const createNotificationsModel = (app: string) => {
  // what we can
  const notificationFetched = createEvent();
  const pageIncremented = createEvent();
  const elementAdded = createEvent<string>();
  const elementRead = createEvent<Notification>();
  const elementRequestRead = createEvent<string>();
  const reset = createEvent();

  // saved
  const $hasNextPage = createStore<boolean>(false);
  const $pageNumber = createStore<number>(1)
    .on(pageIncremented, (page) => page + 1);
  const $total = createStore<number>(1); // save for future
  const $elements = createStore<Notification[]>([]);
  const $resetMark = createStore<boolean>(false); // hack for reload

  // read element
  sample({
    clock: elementRequestRead,
    source: $api,
    fn: (api, _id) => ({ _id, app, api }),
    target: readOneFx,
  });

  // add element
  const fetchElementFx = attach({ effect: fetchNotificationFx });
  sample({
    clock: elementRead,
    source: $elements,
    fn: (elements, element) => {
      const copyElements: Notification[] = structuredClone(elements);
      const elementIndex = copyElements.findIndex((t) => t._id === element._id);
      if (elementIndex !== -1) copyElements[elementIndex].isRead = element.isRead;
      return copyElements;
    },
    target: $elements,
  });

  sample({
    clock: [elementAdded, fetchElementFx.doneData, readOneFx.doneData],
    source: $$usersModel.$users,
    fn: (users, notification) => ({ users, notification }),
    target: getCreatedByFx,
  });

  sample({
    clock: getCreatedByFx.doneData,
    source: $elements,
    fn: (elements, element) => {
      const array: Notification[] = Object
        .values([...elements, element].reduce((acc, i) => ({
          ...acc,
          [i._id]: i,
        }), {}));
      array.sort((a: Notification, b: Notification) => (new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1));
      return array;
    },
    target: $elements,
  });

  // get list
  const fetchElementsFx = attach({ effect: fetchFx });

  sample({
    clock: [
      $api, $pageNumber, $resetMark, $currentTab,
    ],
    target: notificationFetched,
  });

  sample({
    clock: notificationFetched,
    source: {
      api: $api,
      user: $currentUser,
      page: $pageNumber,
      isRead: $isShowRead,
      currentTab: $currentTab,
    },
    filter: ({ currentTab }) => currentTab === app,
    fn: (sources) => ({ ...sources, userId: sources.user._id, app }),
    target: fetchElementsFx,
  });

  sample({
    clock: fetchElementsFx.done,
    source: $elements,
    fn: (elements, { result, params }) => {
      const oldReadNotificationsDataCopy = structuredClone(elements);
      if (params.page === 1) {
        let copyResult: any = structuredClone(result.elements);
        copyResult = fromArrayToHash(copyResult);
        copyResult = Object.values(copyResult);
        copyResult
          .sort((a: Notification, b: Notification) => (new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1));
        return {
          total: copyResult.length,
          elements: copyResult,
          hasNextPage: result.hasNextPage,
        };
      }

      const pages = params.page;
      const pageSize = 10;
      const hasExtraNotifications = oldReadNotificationsDataCopy.length % 10 !== 0;
      let newReadNotifications: any = [];
      if (hasExtraNotifications && pages) {
        // Deleting duplicate notifications
        const messagesAmountToDelete = (oldReadNotificationsDataCopy.length
          + result.elements) - (pages * pageSize);
        oldReadNotificationsDataCopy.splice(-messagesAmountToDelete, messagesAmountToDelete);

        // Adding a full page of new notifications
        newReadNotifications = oldReadNotificationsDataCopy.concat(result.elements);
      } else {
        newReadNotifications = [...oldReadNotificationsDataCopy, ...result.elements];
      }
      newReadNotifications = fromArrayToHash(newReadNotifications);
      newReadNotifications = Object.values(newReadNotifications);
      newReadNotifications
        .sort((a: Notification, b: Notification) => (new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1));
      return {
        total: newReadNotifications.length,
        elements: newReadNotifications,
        hasNextPage: result.hasNextPage,
      };
    },
    target: spread({
      targets: {
        elements: $elements,
        total: $total,
        hasNextPage: $hasNextPage,
      },
    }),
  });

  // if toggle isShowRead we reset data
  sample({
    clock: $isShowRead,
    source: $currentTab,
    filter: (currentTab) => currentTab === app,
    target: reset,
  });

  sample({
    clock: reset,
    source: $resetMark,
    fn: (resetMark) => ({
      elements: [],
      total: 0,
      pageNumber: 1,
      hasNextPage: false,
      resetMark: !resetMark,
    }),
    target: spread({
      targets: {
        elements: $elements,
        total: $total,
        pageNumber: $pageNumber,
        resetMark: $resetMark,
        hasNextPage: $hasNextPage,
      },
    }),
  });

  return {
    $elements,
    $hasNextPage,
    pageIncremented,
    reset,
    elementRead,
    elementRequestRead,
    elementAdded,
    $loading: fetchElementsFx.pending,
  };
};

const $$socialNotifications = createNotificationsModel('odesocial');
const $$tasksNotifications = createNotificationsModel('odetask');
const $$chatNotifications = createNotificationsModel('chat');

const $haveUnreadElements = combine(
  $$socialNotifications.$elements,
  $$tasksNotifications.$elements,
  $$chatNotifications.$elements,
  (social, tasks, chat) => [...social, ...tasks, ...chat].some((element) => !element.isRead),
);

// just for simplify
const $elements = combine(
  either(
    $isSocialTab,
    $$socialNotifications.$elements,
    either(
      $isTasksTab,
      $$tasksNotifications.$elements,
      $$chatNotifications.$elements,
    ),
  ),
  $isShowRead,
  (elements, isRead) => elements.filter((el) => (!isRead ? !el.isRead : true)),
);
const $isEmpty = combine($elements, (els) => els.length === 0);

const $hasNextPage = either(
  $isSocialTab,
  $$socialNotifications.$hasNextPage,
  either(
    $isTasksTab,
    $$tasksNotifications.$hasNextPage,
    $$chatNotifications.$hasNextPage,
  ),
);
const $loading = either(
  $isSocialTab,
  $$socialNotifications.$loading,
  either(
    $isTasksTab,
    $$tasksNotifications.$loading,
    $$chatNotifications.$loading,
  ),
);
const pageChanged = createEvent();
const readAllMade = createEvent();
const readOne = createEvent<string>();

sample({
  clock: readAllMade,
  filter: not($isEmpty),
  source: {
    api: $api,
    app: $currentTab,
  },
  target: readAllFx,
});

// pageChanged
sample({
  clock: pageChanged,
  source: $isSocialTab,
  filter: Boolean,
  target: $$socialNotifications.pageIncremented,
});
sample({
  clock: pageChanged,
  source: $isTasksTab,
  filter: Boolean,
  target: $$tasksNotifications.pageIncremented,
});
sample({
  clock: pageChanged,
  source: $isChatTab,
  filter: Boolean,
  target: $$chatNotifications.pageIncremented,
});

// readOne
sample({
  clock: readOne,
  source: $isSocialTab,
  filter: Boolean,
  fn: (_, params) => params,
  target: $$socialNotifications.elementRequestRead,
});
sample({
  clock: readOne,
  source: $isTasksTab,
  filter: Boolean,
  fn: (_, params) => params,
  target: $$tasksNotifications.elementRequestRead,
});
sample({
  clock: readOne,
  source: $isChatTab,
  filter: Boolean,
  fn: (_, params) => params,
  target: $$chatNotifications.elementRequestRead,
});

// readAll
sample({
  clock: readAllFx.done,
  source: $isSocialTab,
  filter: Boolean,
  target: $$socialNotifications.reset,
});
sample({
  clock: readAllFx.done,
  source: $isTasksTab,
  filter: Boolean,
  target: $$tasksNotifications.reset,
});
sample({
  clock: readAllFx.done,
  source: $isChatTab,
  filter: Boolean,
  target: $$chatNotifications.reset,
});

export {
  $loading,
  $$tasksNotifications,
  $$socialNotifications,
  $$chatNotifications,
  $haveUnreadElements,
  $elements,
  $isEmpty,
  $hasNextPage,
  pageChanged,
  readOne,
  readAllMade,
};
