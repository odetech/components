import {
  combine, createEvent, createStore, sample,
} from 'effector';

enum Tabs {
  OdeSocial = 'odesocial',
  OdeTasks = 'odetask',
  Chat = 'chat',
}

/* Tab logic */
const $currentTab = createStore<Tabs>(Tabs.OdeSocial);

const $isSocialTab = combine($currentTab, (tab) => tab === Tabs.OdeSocial);

const $isTasksTab = combine($currentTab, (tab) => tab === Tabs.OdeTasks);
const $isChatTab = combine($currentTab, (tab) => tab === Tabs.Chat);
const socialTabSet = createEvent();

const tasksTabSet = createEvent();
const chatTabSet = createEvent();

sample({ clock: socialTabSet, fn: () => Tabs.OdeSocial, target: $currentTab });
sample({ clock: tasksTabSet, fn: () => Tabs.OdeTasks, target: $currentTab });
sample({ clock: chatTabSet, fn: () => Tabs.Chat, target: $currentTab });

export {
  $isTasksTab,
  $isSocialTab,
  $isChatTab,
  chatTabSet,
  socialTabSet,
  tasksTabSet,
  $currentTab,
  Tabs,
};
