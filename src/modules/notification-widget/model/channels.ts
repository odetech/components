import {
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { Channel, Socket } from 'phoenix';
import { $$socketModel } from '~/entities/ikinari';
import {
  $$socialNotifications,
  $$tasksNotifications,
  $$chatNotifications,
} from '~/modules/notification-widget/model/factory';

const failed = createEvent<unknown>();

const $channel = createStore<Channel | null>(null);

export const $$channelsModel = {
  $channel,
};

const connectChannelFx = createEffect((
  {
    socket, userId,
  }:
    { socket: Socket | null, userId: string | null },
) => {
  if (!socket) return null;
  const connectedChannel = socket.channel(`notifications:${userId}`);
  connectedChannel.onError(console.log);
  connectedChannel.join()
    .receive('ok', () => console.log('connected'))
    .receive('error', (e: unknown) => failed(e));

  connectedChannel.on('new_notification', (response) => {
    if (response.data?.app === 'odesocial') {
      $$socialNotifications.elementAdded(response.data);
    }
    if (response.data?.app === 'odetask') {
      $$tasksNotifications.elementAdded(response.data);
    }
    if (response.data?.app === 'chat') {
      $$chatNotifications.elementAdded(response.data);
    }
  });

  connectedChannel.on('updated_notification', (response) => {
    if (response.data?.app === 'odesocial') {
      $$socialNotifications.elementRead(response.data);
    }
    if (response.data?.app === 'odetask') {
      $$tasksNotifications.elementRead(response.data);
    }
    if (response.data?.app === 'chat') {
      $$chatNotifications.elementRead(response.data);
    }
  });

  return connectedChannel;
});

sample({
  clock: $$socketModel.$isConnected,
  source: {
    socket: $$socketModel.$socket,
    userId: $$socketModel.$userId,
    isConnected: $$socketModel.$isConnected,
  },
  filter: ({ isConnected }) => isConnected,
  fn: ({
    socket, userId,
  }) => ({
    socket,
    userId,
  }),
  target: connectChannelFx,
});

sample({
  clock: connectChannelFx.doneData,
  target: $channel,
});

sample({
  clock: connectChannelFx.fail,
  fn: ({ params: { socket }, error }) => ({ socket, e: error }),
  target: createEffect(({ socket }: { socket: Socket | null, e: unknown }) => {
    if (socket) socket.disconnect();
  }),
});

sample({
  clock: [
    connectChannelFx.failData,
    failed,
  ],
  target: createEffect((error: unknown) => console.error(JSON.stringify(error))),
});
