import {
  createEffect,
} from 'effector';
import { $$usersModel } from '~/entities/users';
import { MasterNotification, Notification } from './types';

const fetchNotificationFx = createEffect((
  { _id, api, currentUserId }: { _id: string; api: any, currentUserId: string },
) => {
  const payload: {
    recipientUserId: string | undefined,
    origin: string,
    data: string[],
  } = {
    recipientUserId: currentUserId,
    origin: 'odesocial',
    data: ['senderUserId', 'liked', 'reaction'],
  };

  return api.notifications
    .getNotification(_id, payload);
});

const fetchFx = createEffect(
  async ({
    userId, page, api, isRead, app,
  }:
  { userId: string, page: number, api: any, isRead?: boolean, app: string }) => {
    const params = {
      recipientUserId: userId,
      app,
      isRead,
      data: ['senderUserId', 'liked', 'reaction'],
      limit: 10,
      pageNumber: page,
    };
    if (isRead) {
      delete params.isRead;
    }
    const notificationsRequestData = await api.notifications.getNotifications(params);
    return {
      hasNextPage: notificationsRequestData.hasNextPage,
      pageNumber: notificationsRequestData.pageNumber,
      pageSize: notificationsRequestData.pageSize,
      total: notificationsRequestData.total,
      elements: notificationsRequestData.data,
    };
  },
);

const readAllFx = createEffect(
  async ({ api, app }: { api: any, app: string}) => {
    await api.notifications.readAll({
      app,
    });
    await api.notifications.readAll({
      app: 'chat',
    });
  },
);

const readOneFx = createEffect(async ({
  api, app, _id,
}: {
  api: any, app: string, _id: string;
}) => api.notifications.patch(_id, { isRead: true }, { origin: app }));

const patchFrequenciesFx = createEffect(async ({
  // eslint-disable-next-line camelcase
  frequency, api, app, is_active,
}: {
  masterNotifications: MasterNotification | null;
  api: any;
  app?: string
  frequency: string;
  is_active?: boolean;
}) => api.users.patchFrequencies(app, frequency, {
  isActive: Boolean(is_active),
}));

const muteFrequenciesFx = createEffect(async ({
  // eslint-disable-next-line camelcase
  status, api, app,
}: {
  masterNotifications: MasterNotification | null;
  api: any;
  app?: string
  status: string;
}) => api.users.muteNotifications(status, {
  app,
}));

const getCreatedByFx = createEffect(async ({ users, notification }: {
  users: Record<string, any>;
  notification: Notification;
}) => {
  if (users[notification.createdBy]) {
    const { _id, ...profile } = users[notification.createdBy];
    return { ...notification, data: { senderUserId: { _id, profile } } };
  }
  const { _id, ...profile }: any = await $$usersModel.api.fetchUserFx({ userId: notification.createdBy });
  return { ...notification, data: { senderUserId: { _id, profile } } };
});

export {
  patchFrequenciesFx,
  muteFrequenciesFx,
  fetchFx,
  getCreatedByFx,
  fetchNotificationFx,
  readAllFx,
  readOneFx,
};
