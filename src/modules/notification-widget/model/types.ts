export type Notification = {
  content: string,
  createdAt: string,
  type: string,
  extraData: any,
  app: string,
  fullUrl: string,
  isRead: true,
  notify: {
    sendEmail: true
    sendText: false
  },
  recipientUserId: string,
  senderUserId: string,
  createdBy: string,
  source: string,
  title: string,
  updatedAt: string,
  updatedBy: string,
  reaction: string | null,
  url: string,
  _id: string,
  data?: {
    senderUserId?: { profile: any },
    liked?: boolean,
    reaction?: string,
  },
};

export type Callbacks = {
  odetask: {
    onClick?:(e: Notification) => void,
    onReplyClick?:(e: Notification) => void,
  },
  odesocial: {
    onClick?:(e: Notification) => void,
    onReplyClick?:(e: Notification) => void,
  },
  chat: {
    onClick?:(e: Notification) => void,
    onReplyClick?:(e: Notification) => void,
  },
  handleClose: () => void,
} | null;

export type MasterNotification = {
  update: (newValues: MasterNotification) => void,
  frequencies: string[],
  odetask: {
    frequencies: string[],
    muteOptions: string[],
  }
  odesocial: {
    frequencies: string[],
    muteOptions: string[],
  }
}
