import {
  createEvent, createStore, sample,
} from 'effector';

/* Read logic */
const $isShowRead = createStore(false);
const isShowReadToggled = createEvent();

sample({
  clock: isShowReadToggled,
  source: $isShowRead,
  fn: (cur) => !cur,
  target: $isShowRead,
});

export {
  isShowReadToggled,
  $isShowRead,
};
