import {
  combine,
  createEvent, createStore, sample,
} from 'effector';
import { createEffect } from 'effector/compat';
import { persist } from 'effector-storage/local';
import { Callbacks } from './types';

type WidgetStartedPayload = {
  callbacks?: Callbacks;
}

const $callbacks = createStore<Callbacks>(null);

const widgetStarted = createEvent<WidgetStartedPayload>();

sample({
  clock: widgetStarted,
  fn: ({ callbacks }) => callbacks || null,
  target: $callbacks,
});

const $widgetHeight = createStore<number>(300);

persist({ store: $widgetHeight, key: 'notification-widget-height' });

const heightChanged = createEvent<number>();
const mouseDowned = createEvent<MouseEvent>();

const dragEffectFx = createEffect(({ height, event }: { height: number; event: MouseEvent }) => {
  const onMouseMove = (mouseMoveEvent: MouseEvent) => {
    const calculated = height - event.pageY + mouseMoveEvent.pageY;
    if (calculated > 696) {
      heightChanged(696);
    } else {
      heightChanged(height - event.pageY + mouseMoveEvent.pageY);
    }
  };
  const onMouseUp = () => document.body.removeEventListener('mousemove', onMouseMove);

  document.body.addEventListener('mousemove', onMouseMove);
  document.body.addEventListener('mouseup', onMouseUp, { once: true });
});

sample({ clock: heightChanged, target: $widgetHeight });

sample({
  clock: mouseDowned,
  source: $widgetHeight,
  fn: (height, event) => ({ height, event }),
  target: dragEffectFx,
});

/* current window */
enum Windows {
  Notifications,
  Settings
}
const $window = createStore<Windows>(Windows.Notifications);
const notificationsOpened = createEvent();
const settingsOpened = createEvent<void>();

sample({ clock: settingsOpened, fn: () => Windows.Settings, target: $window });
sample({ clock: notificationsOpened, fn: () => Windows.Notifications, target: $window });

const $isNotificationWindow = combine($window, (window) => window === Windows.Notifications);

export {
  $widgetHeight,
  mouseDowned,
  settingsOpened,
  notificationsOpened,
  $window,
  $isNotificationWindow,
  Windows,
  $callbacks,
  widgetStarted,
};
