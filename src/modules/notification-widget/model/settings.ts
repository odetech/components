import {
  combine, createEvent, sample,
} from 'effector';
import { $api, $currentUser, currentUserUpdated } from '~/config';
import { muteFrequenciesFx, patchFrequenciesFx } from './api';

const $masterNotifications = combine($currentUser, (user) => user?.profile?.masterNotifications || null);
const settingsUpdated = createEvent<{
  app?: string;
  frequency: string;
  is_active: boolean;
}>();

const mute = createEvent<{
  app?: string;
  status: string;
}>();

sample({
  clock: settingsUpdated,
  source: {
    api: $api,
    masterNotifications: $masterNotifications,
  },
  fn: (sources, payload) => ({
    ...sources,
    ...payload,
  }),
  target: patchFrequenciesFx,
});

sample({
  clock: mute,
  source: {
    api: $api,
    masterNotifications: $masterNotifications,
  },
  fn: (sources, payload) => ({
    ...sources,
    ...payload,
  }),
  target: muteFrequenciesFx,
});

sample({
  clock: [muteFrequenciesFx.doneData, patchFrequenciesFx.doneData],
  fn: (data) => ({
    'profile.masterNotifications': data,
  }),
  target: currentUserUpdated,
});

export {
  $masterNotifications,
  settingsUpdated,
  mute,
};
