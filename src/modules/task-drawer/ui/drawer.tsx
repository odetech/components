import React, { useEffect, useState } from 'react';
import 'react-modern-drawer/dist/index.css';

import { $$form, $$task, drawerUnmounted } from '~/modules/task-drawer/model';
import { useUnit } from 'effector-react';
import { InitProps, Form } from '~/modules/task-drawer/model/form';
import { $currentUser, $firebase, $isComponentsInitted } from '~/config';
import { $$usersModel } from '~/entities/users';
import { $$themeModel } from '~/entities/theme';
import { useSubscriptionTimeTracker } from '~/modules/time-tracker/hooks';
import { $$tasksModel } from '~/entities/tasks';
import { BeatLoader } from 'react-spinners';
import { css } from '@emotion/react';
import { Colors } from '~/styles/palette';
import {
  DrawerWrapper, HorizontalLine,
} from './styles';

import { PanelBlock } from './panel-block';
import { InfoBlock, TabProps } from './info-block';

export type DrawerTaskProps = {
  isOpen: boolean;
  isDuplicate?: boolean;
  taskId: string | null;
  orgId?: { label: string; value: string };
  projectId?: { label: string; value: string };
  closeDrawer: () => void;
  initialTab: TabProps;
  app: 'odetask' | 'odesocial';
  defaultValues?: Partial<Form>;
};

const LoaderCss = css`
  position: absolute;
  top: calc(50%);
  left: 50%;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  transform: translate(-50%, -50%);
  z-index: 2;
  background: rgba(200, 200, 200, 0.4);
  border-radius: 20px;
`;

export const DrawerTask = ({
  isOpen,
  closeDrawer,
  taskId,
  projectId,
  orgId,
  isDuplicate,
  initialTab,
  defaultValues,
  app,
}: DrawerTaskProps) => {
  const viewInitted = useUnit($$form.viewInitialized);
  const formInitialized = useUnit($$form.$formWasInitialized);
  const taskFetched = useUnit($$task.taskFetched);
  const activeTask = useUnit($$task.$activeTask);
  const closeComponent = useUnit(drawerUnmounted);
  const taskLoading = useUnit($$tasksModel.$tasksLoading);
  const users = useUnit($$usersModel.$users);
  const [fixedHeight, setHeight] = useState(0);
  const activities = useUnit($$themeModel.$activities);
  const currentUser = useUnit($currentUser);
  const dbFirebase = useUnit($firebase);
  useSubscriptionTimeTracker(currentUser._id, dbFirebase!);
  const isComponentsInitted = useUnit($isComponentsInitted);
  useEffect(() => {
    if (taskId) {
      taskFetched(taskId);
      $$tasksModel.events.taskConnected(taskId);
    } else {
      $$tasksModel.events.taskDisconnected();
    }
  }, [isOpen, taskId, taskFetched]);

  useEffect(() => {
    if(!isOpen) closeComponent();
  }, [isOpen])

  useEffect(() => {
    if (!taskLoading) {
      if (taskId && activeTask) {
        const props: InitProps = {
          activeOrg: null,
          activeProject: null,
          currentUser,
          defaultValues: {
            _id: activeTask.id,
            activity: { label: '', value: activeTask.activity },
            title: activeTask.title,
            followers: activeTask.followers,
            priority: { label: '', value: activeTask.priority },
            description: '',
            dueDate: new Date(activeTask.dueDate),
            orgId: { label: '', value: activeTask.orgId },
            projectId: { label: '', value: activeTask.projectId },
            assignedToUserId: { label: '', value: activeTask.assignedToUserId },
            createdBy: { label: '', value: activeTask.createdBy },
            files: [],
            hashtags: activeTask.hashtags,
            uiIds: [],
            isDuplicate,
            requestedBy: { label: '', value: activeTask.requestedBy },
          },
        };
        viewInitted(props);
      } else {
        const props: InitProps = {
          activeOrg: orgId || null,
          activeProject: projectId || null,
          currentUser,
          defaultValues: {
            ...defaultValues,
            isDuplicate,
          },
        };
        viewInitted(props);
      }
    }
  }, [
    taskId,
    isOpen,
    taskLoading,
    viewInitted,
    activeTask,
    isComponentsInitted,
    currentUser,
    users,
    isDuplicate,
    activities,
  ]);

  if (!formInitialized) return null;

  return (
    <DrawerWrapper
      size="1070px"
      fixedHeight={fixedHeight}
      open={isOpen}
      onClose={closeDrawer}
      direction="right"
      className="task-components-drawer"
    >
      {taskLoading ? (
        <BeatLoader
          color={Colors.primary[500]}
          size={25}
          css={LoaderCss}
        />
      ) : null}

      <PanelBlock />
      <HorizontalLine />
      <InfoBlock
        key={`${activeTask?.id}-info-block`}
        activeTask={taskId ? activeTask : null}
        initialTab={initialTab}
        isActiveDrawer={isOpen}
        setHeight={setHeight}
        closeDrawer={closeDrawer}
        app={app}
      />
    </DrawerWrapper>
  );
};
