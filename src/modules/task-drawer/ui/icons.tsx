/* eslint-disable max-len */
import React from 'react';
import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';

type IconsProps = {
  width?: string;
  height?: string;
  fill?: string;
  stroke?: string;
  viewBox?: string;
};

const StyledIcon = styled('svg', {
  shouldForwardProp: isPropValid,
})<IconsProps>`
    width: ${({ width }) => width || '20px'};
    height: ${({ height }) => height || '20px'};
    stroke: ${({ stroke }) => stroke};
    fill: ${({ fill }) => fill};
    stroke-width: ${({ strokeWidth }) => strokeWidth};
    transition: 0.3s;
`;

export const ArrowLeftDoubleIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon viewBox="0 0 25 20" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M24.667 17.65L17.0337 10L24.667 2.35L22.317 0L12.317 10L22.317 20L24.667 17.65Z"
      fill="#F7942A"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M13 17.65L5.36667 10L13 2.35L10.65 0L0.650002 10L10.65 20L13 17.65Z"
      fill="#F7942A"
    />
  </StyledIcon>
);

export const ClientIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon width="24" height="24" viewBox="0 0 24 24" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12 2C6.48 2 2 6.48 2 12C2 17.52 6.48 22 12 22C17.52 22 22 17.52 22 12C22 6.48 17.52 2 12 2ZM7.06982 18.28C7.49982 17.38 10.1198 16.5 11.9998 16.5C13.8798 16.5 16.5098 17.38 16.9298 18.28C15.5698 19.36 13.8598 20 11.9998 20C10.1398 20 8.42982 19.36 7.06982 18.28ZM18.36 16.83C16.93 15.09 13.46 14.5 12 14.5C10.54 14.5 7.07 15.09 5.64 16.83C4.62 15.49 4 13.82 4 12C4 7.59 7.59 4 12 4C16.41 4 20 7.59 20 12C20 13.82 19.38 15.49 18.36 16.83ZM12 6C10.06 6 8.5 7.56 8.5 9.5C8.5 11.44 10.06 13 12 13C13.94 13 15.5 11.44 15.5 9.5C15.5 7.56 13.94 6 12 6ZM12 11C11.17 11 10.5 10.33 10.5 9.5C10.5 8.67 11.17 8 12 8C12.83 8 13.5 8.67 13.5 9.5C13.5 10.33 12.83 11 12 11Z"
      fill="#9C9C9C"
    />
  </StyledIcon>
);

export const ProjectIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon width="24" height="24" viewBox="0 0 24 24" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M7 8V11.5L3 14.5V20H7C7 21.1046 7.89543 22 9 22H11V23H13V22H15C16.1046 22 17 21.1046 17 20H21V14.5L17 11.5V8C17 4.84392 15.0864 1 12 1C8.91356 1 7 4.84392 7 8ZM13 20H15V10V8C15 5.80724 13.6025 3 12 3C10.3975 3 9 5.80724 9 8V10V20H11V14H13V20ZM5 15.5L7 14V18H5V15.5ZM17 18V14L19 15.5V18H17ZM12 11C10.8954 11 10 10.1046 10 9C10 7.89543 10.8954 7 12 7C13.1046 7 14 7.89543 14 9C14 10.1046 13.1046 11 12 11Z"
      fill="#9C9C9C"
    />
  </StyledIcon>
);

export const StatusIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon width="24" height="24" viewBox="0 0 24 24" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M14 11H2V13H14V11ZM14 6H2V8H14V6ZM2 18H10V16H2V18ZM21.5 12.5L23 14L16.01 21L11.5 16.5L13 15L16.01 18L21.5 12.5Z"
      fill="#9C9C9C"
    />
  </StyledIcon>

);

export const PriorityIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon width="24" height="24" viewBox="0 0 24 24" fill="none" {...props}>
    <path d="M19 3V15H22L18 20L14 15H17V3H19ZM11 13V15H0V13H11ZM13 8V10H0V8H13ZM13 3V5H0V3H13Z" fill="#9C9C9C" />
  </StyledIcon>

);

export const UserIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon width="24" height="24" viewBox="0 0 24 24" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12 6C13.1 6 14 6.9 14 8C14 9.1 13.1 10 12 10C10.9 10 10 9.1 10 8C10 6.9 10.9 6 12 6ZM12 15C14.7 15 17.8 16.29 18 17V18H6V17.01C6.2 16.29 9.3 15 12 15ZM12 4C9.79 4 8 5.79 8 8C8 10.21 9.79 12 12 12C14.21 12 16 10.21 16 8C16 5.79 14.21 4 12 4ZM12 13C9.33 13 4 14.34 4 17V20H20V17C20 14.34 14.67 13 12 13Z"
      fill="#9C9C9C"
    />
  </StyledIcon>
);

export const TagIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon width="24" height="24" viewBox="0 0 24 24" fill="none" {...props}>
    <g clipPath="url(#clip0_24712_172091)">
      <path
        d="M3.96938 12.9694C3.82899 12.8288 3.75009 12.6383 3.75 12.4397V3.75H12.4397C12.6383 3.75009 12.8288 3.82899 12.9694 3.96938L22.2806 13.2806C22.4212 13.4213 22.5001 13.612 22.5001 13.8108C22.5001 14.0096 22.4212 14.2003 22.2806 14.3409L14.3438 22.2806C14.2031 22.4212 14.0124 22.5001 13.8136 22.5001C13.6148 22.5001 13.4241 22.4212 13.2834 22.2806L3.96938 12.9694Z"
        stroke="#9C9C9C"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.875 9C8.49632 9 9 8.49632 9 7.875C9 7.25368 8.49632 6.75 7.875 6.75C7.25368 6.75 6.75 7.25368 6.75 7.875C6.75 8.49632 7.25368 9 7.875 9Z"
        fill="#9C9C9C"
      />
    </g>
    <defs>
      <clipPath id="clip0_24712_172091">
        <rect width="24" height="24" fill="white" />
      </clipPath>
    </defs>
  </StyledIcon>
);

export const FollowerIcon: React.FC<IconsProps> = ({ ...props }) => (
  <StyledIcon width="20" height="20" viewBox="0 0 20 20" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9.99974 18.333C10.9164 18.333 11.6664 17.583 11.6664 16.6663H8.33307C8.33307 17.583 9.08307 18.333 9.99974 18.333ZM14.9997 13.333V9.16634C14.9997 6.60801 13.6414 4.46634 11.2497 3.89967V3.33301C11.2497 2.64134 10.6914 2.08301 9.99974 2.08301C9.30807 2.08301 8.74974 2.64134 8.74974 3.33301V3.89967C6.36641 4.46634 4.99974 6.59967 4.99974 9.16634V13.333L3.33307 14.9997V15.833H16.6664V14.9997L14.9997 13.333ZM13.3331 14.1663H6.66641V9.16634C6.66641 7.09967 7.92474 5.41634 9.99974 5.41634C12.0747 5.41634 13.3331 7.09967 13.3331 9.16634V14.1663ZM6.31641 3.39967L5.12474 2.20801C3.12474 3.73301 1.80807 6.08301 1.69141 8.74967H3.35807C3.48307 6.54134 4.61641 4.60801 6.31641 3.39967ZM16.6414 8.74967H18.3081C18.1831 6.08301 16.8664 3.73301 14.8747 2.20801L13.6914 3.39967C15.3747 4.60801 16.5164 6.54134 16.6414 8.74967Z"
      fill="#3B3C3B"
    />
  </StyledIcon>

);
