import * as React from 'react';
import { Button, Toggle } from '~/components/default-components';
import { $$form } from '~/modules/task-drawer/model';
import { useUnit } from 'effector-react';
import { $$tasksModel } from '~/entities/tasks';
import { $currentUser } from '~/config/init';
import styled from '@emotion/styled';
import { FollowerIcon } from '~/modules/task-drawer/ui/icons';
import { createSubmit } from '~/modules/task-drawer/model/create';

const FooterWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  margin-top: auto;
`;

const ButtonsWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  gap: 16px;
    
  button:first-of-type {
    background: transparent;
    color: #3B3C3B;
  }  
`;

const FollowerLabelWrapper = styled('span')`
  display: inline-flex;
  align-items: center;
  color: #3B3C3B;
  gap: 6px;  
  padding: 0;  
  font-size: 14px;
  font-weight: 500;
  margin-right: 12px;  
  line-height: 20px;
`;

export const Footer = ({ closeDrawer }: { closeDrawer: () => void }) => {
  const currentUser = useUnit($currentUser);
  const tasksLoading = useUnit($$tasksModel.$tasksLoading);
  const { form, onChange, isValid } = useUnit({
    form: $$form.$form,
    isValid: $$form.$formIsValid,
    onChange: $$form.valuesChanged,
  });

  const toggleFollowers = () => {
    let copyFollowers = [...form.followers];
    if (form.followers.findIndex((f) => f === currentUser._id) !== -1) {
      copyFollowers = copyFollowers.filter((follower) => follower !== currentUser._id);
    } else {
      copyFollowers.push(currentUser._id);
    }
    onChange({ key: 'followers', value: copyFollowers });
  };

  return (
    <FooterWrapper>
      <Toggle
        checked={form.followers.findIndex((f) => f === currentUser._id) !== -1}
        label={(
          <FollowerLabelWrapper>
            <FollowerIcon />
            Follow task
          </FollowerLabelWrapper>
        )}
        onChange={toggleFollowers}
      />
      {form._id === 'new' && (
        <ButtonsWrapper>
          <Button onClick={closeDrawer}>Cancel</Button>
          <Button onClick={() => createSubmit()} disabled={!isValid || tasksLoading}>Create task</Button>
        </ButtonsWrapper>
      )}
    </FooterWrapper>
  );
};
