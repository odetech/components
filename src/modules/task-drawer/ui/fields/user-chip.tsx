import React, { Fragment } from 'react';
// @ts-ignore
import { Avatar } from '~/components/avatar';
import styled from '@emotion/styled';

const Wrapper = styled('div')<{ label?: string, withLabel?: boolean, hoverOff?: boolean }>`
  display: flex;
  white-space: nowrap;
  align-items: center;
  font-size: 15px;
  font-weight: 400;
  cursor: pointer;
  justify-content: flex-start;
  color: ${({ label }) => (label ? '#3B3C3B' : '#767776')};
  
  & > div:first-child > div {
    font-size: 12px;
  }
  
  & .assignedUserImageLabel {
    margin-left: 10px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 1.3;
  }
  
  & > div {
    flex-shrink: 0;
  }
  
  border-radius: 50px;
`;

export const UserChip: React.FC<Omit<any, 'color'> & { withLabel?: boolean }> = ({
  avatarEntity, label, focusHandler, emptyLabel, withLabel = true, hoverOff,
}) => (
  <Wrapper
    onClick={focusHandler ? () => focusHandler() : undefined}
    data-clickoutside="false"
    label={label}
    withLabel={withLabel}
    hoverOff={hoverOff}
  >
    {label ? (
      <Fragment>
        <Avatar
          size={24}
          user={avatarEntity}
        />
        {withLabel && <span className="assignedUserImageLabel" data-clickoutside="false">{label}</span>}
      </Fragment>
    ) : emptyLabel}
  </Wrapper>
);
