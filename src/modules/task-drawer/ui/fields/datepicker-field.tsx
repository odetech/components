import React, {
  CSSProperties, forwardRef, useEffect, useRef,
} from 'react';
import { DatePicker } from '~/components/default-components';
import styled from '@emotion/styled';

type EditableChipProps = {
  component: React.ComponentType<any>,
  width?: string,
  id?: string,
  onChange: any;
  value: Date,
  focused: boolean,
  isEdit?: boolean,
  onClickOutside: any,
};

const Input = styled('input')`
  padding: 0 !important;
  height: 24px !important;
  width: 110px !important;
  max-width: 110px !important;
  min-width: 110px !important;
  border: 0 !important;
  background: transparent !important; 
`;

const DatePickerInput = forwardRef<HTMLInputElement, any>(({
  value, onChange, ...rest
}, ref) => (
  <Input
    onChange={onChange}
    value={value}
    ref={ref}
    {...rest}
    placeholder="Choose date"
  />
));

export const DatePickerField: React.FC<EditableChipProps> = ({
  component: Component,
  focused,
  onClickOutside,
  width,
  isEdit = true,
  onChange,
  value,
  id,
}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (wrapperRef.current && !wrapperRef.current?.contains(event?.target)
        && !event?.target?.id.includes('-option-')
        && (
          !event?.target.getAttribute('data-clickoutside')
          && !event?.target.parentElement?.parentElement?.getAttribute('data-clickoutside'))
      ) {
        onClickOutside();
      }
    };
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, false);
    };
  }, []);

  const wrapperStyle: CSSProperties = {
    width: width || '100%',
  };

  if (focused && isEdit) {
    return (
      <div ref={wrapperRef} style={wrapperStyle} id={id}>
        <DatePicker
          startDate={value}
          name="any"
          dateFormat="MMM dd, yyyy"
          // @ts-ignore
          open
          customInput={<DatePickerInput autoFocus />}
          onChange={onChange}
        />
      </div>
    );
  }

  return (
    <Component />
  );
};
