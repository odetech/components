import React, { useCallback, useMemo } from 'react';
import { MenuPlacement } from 'react-select';
import { DatePickerField } from '~/modules/task-drawer/ui/fields/datepicker-field';
import { EditableChipSelect } from './basic-select';
import { Chip } from './chip';

type EditableChipAnySelectProps = {
  value: any;
  focused: boolean;
  loading: boolean;
  clearAfterChanged?: boolean;
  width?: string;
  id?: string;
  position?: MenuPlacement;
  chipChanged: () => void;
  chipCleared: () => void;
  options: any[];
  onChange: (val: any) => void;
  Component?: React.FunctionComponent<any>;
  chipProps: any;
  isDate?: boolean,
  components?: any;
};

export const EditableChipAnySelect = ({
  focused, loading, chipChanged, options, value, clearAfterChanged, components,
  onChange, Component, chipProps, chipCleared, width, position, id, isDate,
}: EditableChipAnySelectProps) => {
  const onClickOutside = useCallback(() => {
    chipCleared();
  }, [chipCleared]);

  const valueChanged = (val: any) => {
    onChange(val);
    if (clearAfterChanged) {
      chipCleared();
    }
  };

  const component = useMemo(() => (
    Component ? () => <Component id={id} focusHandler={chipChanged} {...chipProps} /> : () => (
      <Chip
        label={chipProps.label}
        id={id}
        focusHandler={chipChanged}
        emptyLabel={chipProps.emptyLabel}
      />
    )), [Component, id, chipChanged, chipProps]);

  if (isDate) {
    return (
      <DatePickerField
        component={component}
        value={value}
        width={width || '167px'}
        focused={focused}
        onChange={valueChanged}
        id={id}
        onClickOutside={onClickOutside}
      />
    );
  }

  return (
    <EditableChipSelect
      component={component}
      options={options}
      value={value}
      onChange={valueChanged}
      width={width || '167px'}
      focused={focused}
      id={id}
      loading={loading}
      onClickOutside={onClickOutside}
      position={position}
      components={components}
      withPortal
    />
  );
};
