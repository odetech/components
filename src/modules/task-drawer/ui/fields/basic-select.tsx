import React, {
  CSSProperties, useEffect, useLayoutEffect, useRef, useState,
} from 'react';
import { MenuPlacement } from 'react-select';
import { ClipLoader } from 'react-spinners';
import { Select } from './select';

const LoadingMessage = (props: any) => {
  const { innerProps, getStyles } = props;
  return (
    <div
      {...innerProps}
      style={{
        ...getStyles('loadingMessage', props) as CSSProperties,
        display: 'flex',
        direction: 'row',
        gap: '8px',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '14px',
      }}
    >
      <ClipLoader size={16} color="#9C9C9C" />
      Loading
    </div>
  );
};

type EditableChipProps = {
  component: React.ComponentType<any>,
  options: any[],
  value: any,
  onChange: any,
  width?: string,
  id?: string,
  focused: boolean,
  isEdit?: boolean,
  loading: boolean,
  onClickOutside: any,
  withPortal?: boolean,
  position?: MenuPlacement,
  components?: any;
};

export const EditableChipSelect: React.FC<EditableChipProps> = ({
  component: Component,
  options,
  value,
  onChange,
  focused,
  loading,
  onClickOutside,
  width,
  isEdit = true,
  position,
  components,
  id,
  withPortal = false,
}) => {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [portal, setPortal] = useState<HTMLElement | null>(null);
  useLayoutEffect(() => {
    setPortal(document.body);
  }, []);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      const el = event.target.closest("[class$='-option']");
      if (wrapperRef.current && !wrapperRef.current?.contains(event?.target)
        && !el?.id.includes('-option-')
        && (
          !el?.getAttribute('data-clickoutside')
          && !el?.parentElement?.parentElement?.getAttribute('data-clickoutside'))
      ) {
        onClickOutside();
      }
    };
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, false);
    };
  }, []);

  const wrapperStyle: CSSProperties = {
    width: width || '100%',
  };

  if (focused && isEdit) {
    return (
      <div ref={wrapperRef} style={wrapperStyle} id={id}>
        <Select
          options={options}
          value={value}
          onChange={async (val: any) => {
            await onChange(val);
          }}
          type="bordered"
          menuIsOpen={!loading}
          menuPortalTarget={withPortal ? portal : null}
          hideSelectedOptions
          menuPlacement={position}
          loading={loading}
          autoFocus
          withPortal={withPortal}
          components={{
            ...components,
            LoadingMessage,
            LoadingIndicator: () => null,
          }}
        />
      </div>
    );
  }

  return (
    <Component />
  );
};
