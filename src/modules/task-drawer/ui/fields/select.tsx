import React from 'react';
import ReactSelect, {
  Props, components,
} from 'react-select';
import { CSSObject } from '@emotion/react';
import styled from '@emotion/styled';
import { Avatar } from '~/components/avatar';

// region styles
const Wrapper = styled('div')<{ margin?: string, width?: string, disabled?: boolean }>`
  display: inline-flex;
  flex-direction: column;
  min-width: 100px;
  width: ${({ width }) => width ?? '100%'};
  ${({ margin }) => margin && `margin: ${margin}`};
  opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'default')};
`;

const Label = styled('span')`
  display: block;
  margin-bottom: 5px;
  
  font-size: 14px;
  color: ${'#9C9C9C'};
`;

const ValueComponent = styled('div')`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 8px;
`;

const newComponents = {
  MultiValue: ({ children, ...props }: any) => (
    <components.MultiValue {...props}>
      <ValueComponent>
        {props.data.src && (
          <Avatar
            size={24}
            user={{
              profile: {
                firstName: props.data.firstName,
                lastName: props.data.lastName,
                avatar: {
                  secureUrl: props.data.src,
                },
              },
            }}
          />
        )}
        <span>{children}</span>
      </ValueComponent>
    </components.MultiValue>
  ),
};

type SelectProps = {
  label?: string;
  margin?: string;
  padding?: string;
  width?: string;
  value: any;
  withPortal?: boolean;
  disabled?: boolean;
  loading?: boolean;
  withError?: boolean;
  defaultValue?: any;
  type?: string;
} & Props<any>;

const getBorderedSelectStyles: any = ({ withError, padding }: any) => ({
  control(base: CSSObject, { isFocused, isDisabled, isMulti }: any): CSSObject {
    let bg = 'transparent';

    if (isDisabled) {
      bg = '#F0F0F0';
    }

    return ({
      ...base,
      alignItems: 'center',
      border: 'none',
      backgroundColor: bg,
      borderRadius: '8px',
      cursor: 'pointer',
      boxShadow: 'none',
      minHeight: '24px',
      maxHeight: isMulti ? 'unset' : '24px',
      padding: padding ?? '0',
      ':hover': {
        borderColor: 'rgba(247, 148, 42, 0.5)',
      },
    });
  },
  indicatorSeparator(): CSSObject {
    return {};
  },
  indicatorsContainer(base: CSSObject): CSSObject {
    return ({
      ...base,
    });
  },
  dropdownIndicator(base: CSSObject, { isFocused }: any): CSSObject {
    return ({
      ...base,
      padding: '0px',
      color: isFocused ? 'rgba(247, 148, 42, 0.5)' : '#3B3C3B',
      '& > svg': {
        transform: isFocused ? 'rotate(180deg)' : 'none',
        transition: 'transform 0.4s',
      },
    });
  },
  clearIndicator(base: CSSObject): CSSObject {
    return ({
      ...base,
      padding: '3px 0px',
    });
  },
  singleValue(base: CSSObject): CSSObject {
    return ({
      ...base,
      fontWeight: 500,
      color: '#3B3C3B',
      fontSize: '14px',
      padding: '0',
      margin: '0',
    });
  },
  menu(base: CSSObject): CSSObject {
    return ({
      ...base,
      margin: '12px 0 0',
      borderRadius: '6px',
      borderColor: 'rgba(247, 148, 42, 0.5)',
      zIndex: 1200,
      width: '216px',
      left: '-12px',
      boxShadow: 'none',
      border: '1px solid #F7942A',
    });
  },
  menuList(base: CSSObject): CSSObject {
    return ({
      ...base,
      borderRadius: '8px',
      scrollbarColor: '#eee #FFF',
      scrollbarWidth: 'thin',
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: '#b9b9b9',
        borderRadius: 10,
      },
      '&::-webkit-scrollbar': {
        width: '4px',
        backgroundColor: '#e6e6e6',
      },
    });
  },
  valueContainer(base: CSSObject): CSSObject {
    return ({
      ...base,
      padding: 0,
      gap: '8px',
      height: '24px',
    });
  },
  placeholder(base: CSSObject): CSSObject {
    return ({
      ...base,
      fontSize: '14px',
    });
  },
  option(base: CSSObject, { isFocused, isSelected }: any): CSSObject {
    let bgc = 'transparent';

    if (isFocused) {
      bgc = 'rgba(247, 148, 42, 0.1)';
    }

    if (isSelected) {
      bgc = 'rgba(247, 148, 42, 0.2)';
    }

    return ({
      ...base,
      backgroundColor: bgc,
      color: isSelected ? '#F7942A' : '#3B3C3B',
      fontWeight: 500,
      cursor: 'pointer',
      fontSize: '14px',
      ':active': {
        backgroundColor: 'rgba(247, 148, 42, 0.5)',
      },
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    });
  },
  menuPortal(base: CSSObject): CSSObject {
    return ({
      ...base,
      zIndex: 1200,
    });
  },
  multiValue(base: CSSObject): CSSObject {
    return ({
      ...base,
      backgroundColor: '#f0f0f0',
      padding: '4px 8px',
      height: '32px',
      margin: 0,
    });
  },
  multiValueLabel(base: CSSObject): CSSObject {
    return ({
      ...base,
      color: '#3B3C3B',
      fontWeight: 600,
      padding: '0 !important',
      display: 'flex',
    });
  },
  multiValueRemove(base: CSSObject): CSSObject {
    return ({
      ...base,
      color: '#3B3C3B',
      '&:hover': {
        color: '#3B3C3B',
        backgroundColor: 'transparent',
      },
    });
  },
});

export const Select: React.FC<SelectProps> = ({
  label,
  margin,
  width,
  value,
  withPortal,
  disabled,
  loading,
  withError,
  defaultValue,
  padding,
  ...rest
}) => {
  const currentUserId = '1';

  return (
    <Wrapper margin={margin} width={width} disabled={disabled}>
      {label && <Label>{label}</Label>}
      <ReactSelect
        value={value || defaultValue || null}
        styles={getBorderedSelectStyles({ withError, currentUserId, padding })}
        menuPortalTarget={withPortal ? document.body : null}
        isDisabled={disabled}
        isLoading={loading}
        components={newComponents}
        {...rest}
      />
    </Wrapper>
  );
};
