import React from 'react';
import styled from '@emotion/styled';

const Wrapper = styled('div')<{ bgColor?: string, customStyle?: string}>`
    padding: 8px 0;
    max-width: 120px;
    min-width: 120px;
    border-radius: 28px;
    text-align: center;
    font-weight: 500;
    font-size: 12px;
    line-height: 16px;

    background-color: ${({ bgColor }) => `${bgColor}`};
    color: white;

    &:hover {
        cursor: pointer;
        opacity: 0.9;
        transition: opacity 0.3s;
    }
    ${({ customStyle }) => customStyle || ''}
`;

export const ColorChip: React.FC<Omit<any, 'avatarEntity'>> = ({
  color, label, focusHandler, customStyle,
}) => (
  <Wrapper bgColor={color} onClick={focusHandler} data-clickoutside="false" customStyle={customStyle}>
    {label}
  </Wrapper>
);
