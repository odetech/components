import React from 'react';
import styled from '@emotion/styled';

const ChipWrapper = styled('div')<{ label?: string }>`
  display: inline;
  white-space: nowrap;
  align-items: center;
  cursor: pointer;
  background: #FFFFFF;
  color: ${({ label }) => (label ? '#3B3C3B' : '#767776')};
  border-radius: 38px;
  width: 128px;
  text-align: center;
  justify-content: center;
  text-overflow: ellipsis;
  overflow: hidden;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
`;

export const Chip: React.FC<any> = ({
  label, focusHandler, emptyLabel, id,
}) => (
  <ChipWrapper onClick={focusHandler} data-clickoutside="false" label={label} id={id}>
    {label || emptyLabel}
  </ChipWrapper>
);
