import React from 'react';
import styled from '@emotion/styled';

const Wrapper = styled('div')<{ bgColor?: string }>`
  display: flex;
  white-space: nowrap;
  align-items: center;
  cursor: pointer;
  background: #FFFFFF;
  border: none;
  border-radius: 4px;
  font-weight: 400;
  font-size: 14px;
  width: 95px;
  line-height: 20px;
  /* identical to box height, or 143% */


  color: #3B3C3B;

  &::before {
    content: "";
    display: block;
    width: 20px;
    height: 20px;
    margin-right: 5px;
    border-radius: 50%;
    background-color: ${({ bgColor }) => bgColor};
  }
  
  &:hover {
    background-color: #F0F0F0;
  }
`;

export const ColorDotChip: React.FC<Omit<any, 'avatarEntity'>> = ({
  color, label, focusHandler,
}) => (
  <Wrapper bgColor={color} onClick={focusHandler} data-clickoutside="false">
    {label}
  </Wrapper>
);
