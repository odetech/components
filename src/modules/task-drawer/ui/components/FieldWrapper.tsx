import React, { CSSProperties } from 'react';
import { Field, FieldProps } from 'formik';
import styled from '@emotion/styled';

// region styles
const Label = styled('label')<{
  paddingBottom?: number;
  marginBottom?: number;
  width?: string;
  margin?: string;
  disabledWrapper?: boolean,
}>`
  position: relative;
  width: ${({ width }) => width ?? '100%'};
  display: inline-flex;
  flex-direction: column;
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: 20px;
  align-items: flex-start;
  padding-bottom: ${({ paddingBottom }) => (typeof paddingBottom === 'number' ? paddingBottom : 19)}px;
  ${({ margin }) => margin && `margin: ${margin}`};
  ${({ marginBottom }) => marginBottom && `margin-bottom: ${marginBottom}px;`};
  ${({ disabledWrapper }) => disabledWrapper && `
    opacity: 0.5;
  `};
`;

const ErrorStyled = styled('span')<{ errorBottom?: number }>`
  position: absolute;
  left: 0;
  bottom: 3px;

  font-size: 11px;
  color: #F33822;
  white-space: pre-wrap;
  max-width: fit-content;
  ${({ errorBottom }) => errorBottom && `bottom: ${errorBottom}px;`};
`;

const StyledLabelText = styled('span')`
  margin-bottom: 10px;
  font-size: 14px;
`;

export const FormikFieldWrapper: React.FC<{
  inputProps: any;
  marginBottom?: number;
  errorBottom?: number;
  component: React.FC<any>;
  label?: React.ReactNode;
  labelStyle?: CSSProperties;
  customStyles?: CSSProperties;
  width?: string;
  margin?: string;
  withPasswordIcon?: boolean;
  isStatic?: boolean;
  paddingBottom?: number;
}> = ({
  inputProps,
  marginBottom,
  errorBottom,
  component: Component,
  label,
  labelStyle,
  customStyles,
  width,
  margin,
  isStatic,
  paddingBottom,
}) => (
  <Field name={inputProps.name}>
    {({ field, meta }: FieldProps) => {
      const { error, touched } = meta;
      const { disabled } = inputProps;

      return (
        <Label
          marginBottom={marginBottom}
          width={width}
          margin={margin}
          style={customStyles}
          disabledWrapper={disabled}
          paddingBottom={paddingBottom}
        >
          {label && <StyledLabelText style={labelStyle}>{label}</StyledLabelText>}
          {isStatic ? (
            <Component
              {...inputProps}
              type={inputProps.type || 'text'}
              {...field}
              error={touched && error}
            >
              {field?.value}
            </Component>
          ) : (
            <Component
              {...inputProps}
              type={inputProps.type || 'text'}
              {...field}
              error={touched && error}
            />
          )}
          <ErrorStyled errorBottom={errorBottom} role="alert">{touched && error}</ErrorStyled>
        </Label>
      );
    }}
  </Field>
);
