import React, { CSSProperties, InputHTMLAttributes } from 'react';
import styled from '@emotion/styled';
import { FormikFieldWrapper } from './FieldWrapper';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  theme?: string;
  padding?: string;
  styles?: string;
}

export const StyledInput = styled('input')<{ error?: string; padding?: boolean, styles?: string }>`
  padding: ${({ padding }) => padding || '5px 20px'};
  background-color: transparent;
  
  line-height: 1.7;
  color: #3B3C3B;
  
  transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;

  width: 100%;
  cursor: pointer;

  font-weight: 500;
  font-size: 14px;

  border: 1px solid #CECECE;
  border-radius: 10px;
  
  ${({ error }) => error && 'border-color: red;'};
  
  &:hover:not(:disabled):not(:read-only) {
    border-color: rgba(247, 148, 42, 0.5);
  }
  
  &:focus:not(:read-only) {
    background-color: rgba(247, 148, 42, 0.1);
    border-color: rgba(247, 148, 42, 0.5);
  }
  
  &:disabled {
    opacity: 0.5;
    
    background-color: #f7f7f7;
    
  }
  
  &:disabled,
  &:read-only {
    cursor: not-allowed;
  }

  ${({ styles }) => styles}
`;

const getLabelStyle = (disabled?: boolean): CSSProperties => ({
  display: 'block',
  marginBottom: '5px',
  fontSize: '14px',
  color: '#9C9C9C',
  opacity: disabled ? '0.5' : '1',
});

export const RoundInput: React.FC<{
  inputProps: InputProps;
  marginBottom?: number;
  paddingBottom?: number;
  errorBottom?: number;
  label?: string;
  width?: string;
  margin?: string;
  withPasswordIcon?: boolean;
}> = ({
  inputProps,
  marginBottom,
  paddingBottom,
  errorBottom,
  label,
  width,
  margin,
  withPasswordIcon,
}) => (
  <FormikFieldWrapper
    inputProps={inputProps}
    component={StyledInput}
    marginBottom={marginBottom}
    errorBottom={errorBottom}
    paddingBottom={paddingBottom}
    label={label}
    labelStyle={getLabelStyle(inputProps.disabled)}
    width={width}
    margin={margin}
    withPasswordIcon={withPasswordIcon}
  />
);
