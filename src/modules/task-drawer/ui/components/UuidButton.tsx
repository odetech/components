import React from 'react';
import styled from '@emotion/styled';
import { Button } from '~/components/default-components';
import { copyLinkOnTask } from '~/entities/tasks';
import { useUnit } from 'effector-react';
import { $currentUser } from '~/config';
import { LinkDrawerIcon } from '~/components/icons';

type UuidCopyButtonProps = {
  uuid: string;
  id: string;
  taskId: string;
  props?: Record<string, string>;
};

const StyledCopyButton = styled(Button)`
  padding: 2px;
  margin: 0;
  white-space: nowrap;
  position: relative;
  background: transparent;
  color: #3B3C3B;
  font-size: 16px;
  font-weight: normal;
  line-height: 24px;

  .cloud-element-toast {
    padding: 8px 12px 8px 12px;
    border-radius: 22px;
    gap: 10px;
    position: absolute;
    background: white;
    box-shadow: 0 0 15px 0 #D8D8D8;
    font-size: 12px;
    font-weight: 400;
    z-index: 3;
    line-height: 16px;
  }

  svg {
    display: none;
    margin-left: 4px;
    fill: #3B3C3B;
  }

  &:focus, &:active {
    background-color: transparent;
  }

  &:hover {
    background-color: #F0F0F0;
    svg {
      display: block;
    }
  }
`;

export const UuidCopyButton = ({
  uuid, id, taskId, props,
}: UuidCopyButtonProps) => {
  const currentUser = useUnit($currentUser);
  const onClick = async () => {
    await copyLinkOnTask(
      taskId,
      false,
      currentUser?._id,
      document.getElementById(id)!,
      [1000, { top: '-40px', ...props }],
    );
  };

  return (
    <StyledCopyButton
      type="button"
      id={id}
      onClick={onClick}
    >
      {uuid}
      {' '}
      <LinkDrawerIcon />
    </StyledCopyButton>
  );
};
