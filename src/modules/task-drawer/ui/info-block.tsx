import React, { useEffect, useState } from 'react';
import { Creating } from '~/modules/task-drawer/ui/widgets/creating';
import styled from '@emotion/styled';
import { SubTaskForm } from '~/modules/task-drawer/ui/widgets/subtasks/subtasks';
import { EditableText } from '~/components/editable-text';
import { useUnit } from 'effector-react';
import { $$form } from '~/modules/task-drawer/model';
import { Footer } from '~/modules/task-drawer/ui/footer';
import { variant } from '@effector/reflect';
import { combine } from 'effector';
import { SubTasks } from '~/modules/task-drawer/ui/widgets/subtasks/subtasks-detail';
import { TimeTrackerManageForm } from '~/modules/task-drawer/ui/widgets/time-tracker/time-tracker-manage-form';
import { $$usersModel } from '~/entities/users';
import { $$timeTrackersModel } from '~/entities/time-trackers';
import { $$trackerEditTaskModel } from '~/modules/task-drawer/ui/widgets/time-tracker/edit-model';
import { $estimatedTime } from '~/modules/task-drawer/ui/widgets/time-tracker/common';
import { BeatLoader } from 'react-spinners';
import { $$subtasksModel as $$subtaskEntities } from '~/entities/subtasks';
import { PauseIcon, PlayIcon, StopIcon } from '~/modules/time-tracker/icons';
import { TimeTrackerStopModal } from '~/modules/time-tracker/time-tracker-component/components/time-tracker-stop-modal';
import { UuidCopyButton } from '~/modules/task-drawer/ui/components/UuidButton';
import { CloseIcon, LinkTaskIcon } from '~/components/icons';
import ReactTooltip from 'react-tooltip';
import { secondToHumanFormat } from '~/modules/task-drawer/lib';
import { $$trackerCreateTaskModel } from './widgets/time-tracker/model';
import {
  HeaderButtons, Info, Tab, Tabs, CloseButton, TaskLink, TitleWrapper,
} from './styles';
import * as $$subtasksModel from '../model/subtasks';
import { Comments } from './widgets/comments';

export type TabProps = 'comments' | 'subtasks' | 'time';

const CommentsBlock = variant({
  if: combine($$form.$form, (form) => Boolean(form._id && form._id !== 'new')),
  then: Comments,
  else: Creating,
});

const SubtasksBlock = variant({
  if: combine($$form.$form, (form) => Boolean(form._id && form._id !== 'new')),
  then: SubTasks,
  else: SubTaskForm,
});

type InfoBlockProps = {
  activeTask: any | null;
  app: 'odetask' | 'odesocial';
  initialTab: TabProps;
  isActiveDrawer: boolean;
  closeDrawer: () => void;
  setHeight: (value: number) => void;
}

export const TaskTitle = styled('input')`
  border: 1px solid transparent;
  border-radius: 5px;
  font-size: 20px;
  font-weight: 600;
  color: #3B3C3B;
  line-height: 1.2;
  padding: 2px 6px;
  cursor: pointer;
  width: calc(100% - 24px);
  resize: none;
  overflow: hidden;

  &:hover,
  &:focus {
      border-color: #E5E5E5;
      outline: none;
  }
`;

export const TaskTitleStatic = styled(TaskTitle)`
  white-space: pre-line;
  padding: 0;
`.withComponent('h2');

export const InfoBlock = ({
  activeTask, initialTab, closeDrawer, isActiveDrawer, setHeight, app,
}: InfoBlockProps) => {
  const [tab, setTab] = useState<TabProps>(initialTab || 'comments');
  const { form, onChange } = useUnit({ form: $$form.$form, onChange: $$form.valuesChanged });
  const subtasksModel = useUnit($$subtasksModel);
  const [subTasks, subtaskFetched] = useUnit([$$subtaskEntities.$subtasks, $$subtaskEntities.fetched]);
  const users = useUnit($$usersModel.$users);
  const updateTimeTracker = useUnit($$timeTrackersModel.activeTimeTrackerUpdated);
  const timeTrackers = useUnit($$timeTrackersModel.$timeTrackers);
  const countSubtasks = activeTask?.id ? Object.keys(subTasks).length : 0;

  useEffect(() => {
    if (initialTab) setTab(initialTab);
  }, [initialTab, activeTask?.id]);

  useEffect(() => {
    if (activeTask?.id) {
      subtaskFetched({ taskId: activeTask.id, taskOrder: activeTask.subTasksOrder });
    }
  }, [activeTask, subtaskFetched]);

  const horizontalFormCreateModel = useUnit($$trackerCreateTaskModel);
  const estimatedTime = useUnit($estimatedTime);
  const horizontalFormEditModel = useUnit($$trackerEditTaskModel);
  const isPause = useUnit($$timeTrackersModel.$isPause);
  const timeTrackersSilentLoading = useUnit($$timeTrackersModel.$activeTimeTrackerLoading);
  const activeTimeTracker = useUnit($$timeTrackersModel.$activeTimeTracker);
  const renderButtonIcon = () => {
    if (timeTrackersSilentLoading) {
      return (
        <BeatLoader color="white" loading size={5} />
      );
    }
    if (isPause) {
      return <PauseIcon />;
    }
    if (activeTimeTracker?.taskId === form?._id) {
      return <StopIcon />;
    }

    return <PlayIcon />;
  };

  return (
    <Info key={activeTask?.id}>
      <HeaderButtons>
        {activeTask && (
          <UuidCopyButton
            uuid={activeTask.uuid}
            id={`task-uuid-drawer-${activeTask.id}`}
            taskId={activeTask.id}
          />
        )}
        <CloseButton type="button" onClick={closeDrawer}>
          <CloseIcon />
        </CloseButton>
      </HeaderButtons>
      <TitleWrapper>
        <EditableText
          onChange={(newTitle: string) => onChange({ key: 'title', value: newTitle })}
          StaticComponent={TaskTitleStatic}
          EditComponent={TaskTitle}
          isEditOnEmptyInput
          onChangeSize={setHeight}
          initialText={form.title || ''}
          autoHeight={false}
          isFocused={isActiveDrawer && !activeTask?.id}
          renderKey={activeTask?.id ?? 'new'}
          placeholder="Task name"
        />
        {activeTask?.id && (
          <TaskLink
            className="task-list-link"
            to={app === 'odetask'
              ? `/conversations?activeTask=${activeTask.id}`
              : `/redirect/odetask?page=conversations&params=${
                encodeURI(JSON.stringify({ activeTask: activeTask.id }))}
              `}
            target="_blank"
            data-tip
            data-for={`${activeTask?.id}-drawer-hashtags-tooltip`}
          >
            <LinkTaskIcon fill="#9C9C9C" />
          </TaskLink>
        )}
        <ReactTooltip id={`${activeTask?.id}-drawer-hashtags-tooltip`}>
          Open in Conversations
        </ReactTooltip>
      </TitleWrapper>
      <Tabs>
        <Tab type="button" isActive={tab === 'comments'} onClick={() => setTab('comments')}>
          Comments
        </Tab>
        <Tab type="button" isActive={tab === 'subtasks'} onClick={() => setTab('subtasks')}>
          {`Subtasks${!countSubtasks ? '' : ` (${countSubtasks})`}`}
        </Tab>
        <Tab type="button" isActive={tab === 'time'} onClick={() => setTab('time')}>
          {`Time${!activeTask?.currentTime ? '' : ` (${secondToHumanFormat(activeTask?.currentTime || 0, true)})`}`}
        </Tab>
      </Tabs>
      {tab === 'comments' && (
        <CommentsBlock activeTask={activeTask?.id} currentActiveTask={activeTask} />
      )}
      {tab === 'subtasks' && (
        <SubtasksBlock
          taskId={activeTask?.id}
          // @ts-ignore
          activeTask={activeTask?.id}
          initialValues={{
            subTasks: subtasksModel.$subTasks,
            subTaskTitle: '',
          }}
          handleSubmit={subtasksModel.onSubmit}
          // @ts-ignore
          handleDelete={subtasksModel.onDelete}
          // @ts-ignore
          handleCheck={subtasksModel.onCheck}
          handleDragCallback={subtasksModel.onDragCallback}
          isLoading={false}
        />
      )}
      {tab === 'time' && (
        <TimeTrackerManageForm
          task={form._id === 'new' ? {
            id: form._id,
            title: form.title,
            orgId: form.orgId?.value || '',
            estimatedTime,
          } : activeTask}
          activeTimeTracker={activeTimeTracker}
          showSpentTime={form._id !== 'new'}
          isPause={isPause}
          onTrackTime={form._id === 'new' ? undefined : horizontalFormEditModel.trackedToggled}
          horizontalFormSubmitted={form._id === 'new'
            ? horizontalFormCreateModel.trackerCreated
            : horizontalFormEditModel.trackerCreated}
          onChangeMessage={form._id === 'new'
            ? horizontalFormCreateModel.messageChanged
            : horizontalFormEditModel.messageChanged}
          onChangeTime={form._id === 'new'
            ? horizontalFormCreateModel.timeChanged
            : horizontalFormEditModel.timeChanged}
          onChangeDate={form._id === 'new'
            ? horizontalFormCreateModel.dateChanged
            : horizontalFormEditModel.dateChanged}
          onEstimatedTimeChange={form._id === 'new'
            ? horizontalFormCreateModel.onEstimatedTimeChange
            : horizontalFormEditModel.estimatedTimeEdited}
          removeTimeTracker={form._id === 'new'
            ? horizontalFormCreateModel.trackerRemoved
            : $$timeTrackersModel.removed}
          users={users}
          renderButtonIcon={form._id === 'new' ? undefined : renderButtonIcon}
          timeTrackers={form._id === 'new'
            ? horizontalFormCreateModel.$timeTrackers
            : timeTrackers}
        />
      )}
      {horizontalFormEditModel.$showModal && (
        <TimeTrackerStopModal
          showModal={horizontalFormEditModel.$showModal}
          setShowModal={horizontalFormEditModel.modalChanged}
          stopRequest={updateTimeTracker}
        />
      )}
      <Footer closeDrawer={closeDrawer} />
    </Info>
  );
};
