import * as React from 'react';
import { SlateEditor } from '~/components/editor';
import styled from '@emotion/styled';
import { $$form } from '~/modules/task-drawer/model';
import { useUnit } from 'effector-react';
import { useRef } from 'react';
import { getSuggestionsByProject } from '~/modules/task-drawer/lib';
import { $$usersModel } from '~/entities/users';
import { $$projectsModel } from '~/entities/projects';
import { Form } from '~/modules/task-drawer/model/form';

const SlateWrapper = styled('div')`
  button[type="submit"] {
    display: none;
  }
  div[data-slate-editor="true"] {
    min-height: 264px !important;
  }
`;

export const Creating = () => {
  const form = useUnit($$form.$form);
  const onChange = useUnit($$form.valuesChanged);
  const users = useUnit($$usersModel.$users);
  const projects = useUnit($$projectsModel.$projectsActive);
  const filesRef = useRef<{ file: File; uiId: string; }[]>([]);
  return (
    <SlateWrapper>
      <SlateEditor
        setFieldValue={(key: string, val: string) => {
          onChange({ key: key as keyof Form, value: val });
        }}
        value={form.description}
        valueFiles={form.files}
        // @ts-ignore
        files={form.files}
        field="description"
        userSuggestions={
          getSuggestionsByProject(users, projects[form.projectId?.value || ''])
        }
        uploadImageCallBack={(file: File, uiId: string) => {
          if (filesRef.current) filesRef.current.push({ file, uiId });
          onChange({ key: 'files', value: [...filesRef.current, { file, uiId }] });
        }}
        placeholder="Description"
        // @ts-ignore
        maxInputHeight="75vh"
        withAutoFocus
      />
    </SlateWrapper>
  );
};
