import React from 'react';
import { Comment } from './Comment';

type ThreadsProps = {
  threads?: Record<string, any>,
  userId: string,
  associatedId: string,
  setImages: (images: string[]) => void,
  setIsOpenLightbox: (isOpen: boolean) => void,
  setPhotoIndex: (index: number) => void,
  setEdit: any;
  setReplyComment?: any;
  setDeleteMessage?: any;
  highlightMessage?: {
    comment: string | null,
    reply: string | null,
  },
};

export const Threads: React.FC<ThreadsProps> = ({
  threads, userId, associatedId,
  setPhotoIndex, setImages, setIsOpenLightbox,
  setEdit, setReplyComment, highlightMessage, setDeleteMessage,
}) => (
  // eslint-disable-next-line react/jsx-no-useless-fragment
  <>
    {threads && Object.values(threads).map((thread) => {
      const threadWithCommentId = {
        ...thread,
        commentAssociatedId: associatedId,
      };
      return (
        <Comment
          comment={threadWithCommentId}
          userId={userId}
          withThread
          disabledButtons={false}
          deleteModal={() => {
            setDeleteMessage(threadWithCommentId.id);
          }}
          key={threadWithCommentId.id}
          setImages={setImages}
          setIsOpenLightbox={setIsOpenLightbox}
          setPhotoIndex={setPhotoIndex}
          setEdit={setEdit}
          setReplyComment={setReplyComment}
          highlightMessage={highlightMessage}
        />
      );
    })}
  </>
);
