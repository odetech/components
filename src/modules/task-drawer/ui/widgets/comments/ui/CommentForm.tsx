import { SlateEditor, getMentionsFromValue } from '~/components/editor';
import { extractHTMLContent, filesUpload } from '~/helpers';
import { Formik, useFormikContext } from 'formik';
import React, {
  useEffect,
  useMemo, useRef,
  useState,
} from 'react';
import { $$projectsModel } from '~/entities/projects';
import { $$usersModel } from '~/entities/users';
import { useUnit } from 'effector-react';
import { $$commentsModel } from '~/entities/comments';
import { getReplyValue } from '../helpers';
import { StyledForm } from '../styles/comments';

type CommentValues = {
  comment: string;
  files: { file: File, uiId: string }[];
  uiIds: string[];
};

type CommentFormProps = {
  projectId: string,
  replyComment?: any,
  isEdit: any,
  activeTask: string,
  position?: string,
  userId?: string,
  customPadding?: string,
  setReplyComment: any,
  setEdit: any,
  choseTask: any,
  scrollTo?: (id: string) => void;
  withSeparateView?: boolean,
  noPadding?: boolean,
  setActiveThread?: (activeThread: string) => void;
  setImages: (images: string[]) => void,
  setIsOpenLightbox: (isOpen: boolean) => void,
  setPhotoIndex: (index: number) => void,
};

export const AutoSaveComment = ({ id, commentWasRead }: { id: string, commentWasRead: boolean }) => {
  const { values } = useFormikContext();
  const updateComment = useUnit($$commentsModel.events.updateCommentDB);
  useEffect(() => {
    const doSaving = async () => {
      if (id && commentWasRead) {
        const data = {
          // @ts-ignore
          ...values,
          _id: id,
          date: new Date().getTime(),
        };

        // TODO can't save and restore files
        delete data.files;
        delete data.uiIds;
        updateComment(data);
      }
    };
    doSaving();
  }, [values, commentWasRead]);
  return null;
};

export const CommentForm = ({
  projectId,
  replyComment,
  isEdit,
  activeTask,
  userId,
  setReplyComment,
  setEdit,
  choseTask,
  noPadding,
  setActiveThread,
  position,
  setIsOpenLightbox,
  withSeparateView,
  setImages,
  setPhotoIndex,
  customPadding,
  scrollTo,
}: CommentFormProps) => {
  const {
    created,
    edited,
    lastId,
  } = useUnit({
    created: $$commentsModel.events.sentTaskMessage,
    edited: $$commentsModel.events.editTaskMessage,
    lastId: $$commentsModel.$lastChangedCommentId,
  });
  const db = useUnit($$commentsModel.$savedComments);
  const [comment, setComment] = useState<any>(null);
  const [commentCheck, setCommentCheck] = useState<boolean>(false);
  const { users, projects } = useUnit({
    users: $$usersModel.$users,
    projects: $$projectsModel.$projectsActive,
  });
  const [isProcess, setIsProcess] = useState<boolean>(false);
  const filesRef = useRef<any>([]);

  const userSuggestions = useMemo(() => {
    const result: {
      _id: string;
      profile: {
        firstName: string;
        lastName: string;
      }
    }[] = [];
    const project = projects[projectId];

    if (project) {
      project.assignedToUserId.forEach((assignedToUserId) => {
        const user = users[assignedToUserId];
        if (user) {
          result.push({
            _id: user.id,
            profile: {
              firstName: user.firstName!,
              lastName: user.lastName!,
            },
          });
        }
      });
    }

    return result;
  }, [users, projects, projectId]);

  const value = useMemo(() => {
    if (replyComment) {
      return getReplyValue(replyComment, users);
    }
    return (isEdit && (isEdit.replyValue || isEdit.value)) || '';
  }, [
    replyComment,
    isEdit,
    users,
  ]);

  const currentId = useMemo(() => {
    if (replyComment?.id) {
      return `reply-form-${replyComment?.id}`;
    }
    return 'comment-form';
  }, [replyComment]);

  const associatedId = useMemo(() => {
    if (replyComment?.id) {
      return replyComment.commentAssociatedId || replyComment.id;
    }
    return activeTask;
  }, [replyComment]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await db.get(associatedId);
        setComment(result);
        // @ts-ignore
      } catch (err) { }
      setCommentCheck(true);
    };

    fetchData();
  }, [db, associatedId]);

  const initialValues: CommentValues = {
    comment: comment?.comment || '',
    files: [],
    uiIds: [],
  };

  useEffect(() => {
    if (lastId && scrollTo) {
      scrollTo(lastId);
    }
  }, [lastId]);

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize
      onSubmit={async (values, { setFieldValue }) => {
        if (isProcess) return;
        setIsProcess(true);
        // upload images
        let uploadedFiles: { cloudinary: any, uiId: string }[] = [];
        if (values.files?.length) {
          const uniqUnids: string[] = [];
          const uniqFiles: { file: File, uiId: string }[] = [];
          values.files.filter((file) => values.uiIds.includes(file.uiId)).forEach((file) => {
            if (!uniqUnids.includes(file.uiId)) {
              uniqFiles.push(file);
              uniqUnids.push(file.uiId);
            }
          });
          uploadedFiles = await filesUpload(uniqFiles);
        }

        const payload: any = {
          text: values.comment.replace(/src="(data[^"]+?)"/g, 'src=""'),
          rawText: extractHTMLContent(values.comment),
          createdBy: userId,
          isDraft: false,
          projectId,
          app: 'odetask',
          updatedBy: userId,
          ts: new Date().getTime(),
          mentions: getMentionsFromValue(values.comment),
          tags: [activeTask],
        };

        if (uploadedFiles.length) {
          payload.files = uploadedFiles;
        }

        if (replyComment) {
          payload.associatedId = replyComment.commentAssociatedId || replyComment.id;
          payload.isReply = true;
        }

        if (isEdit) {
          payload._id = isEdit?.id;
          const editPayload: any = payload;
          if (isEdit.isReply) {
            editPayload.associatedId = isEdit.associatedId;
            editPayload.isReply = true;
            editPayload._id = isEdit.id;
          }
          edited(editPayload);
        } else {
          created(payload);
          if (!payload.isReply) {
            const currentTask = {
              ...choseTask,
              commentsCount: choseTask.commentsCount + 1,
            };
            // await dispatch(updateTask(currentTask));
            console.warn('update task', currentTask);
          } else if (setActiveThread) {
            setActiveThread(payload?.associatedId);
          }
        }
        setReplyComment(null);
        setEdit(null);
        setFieldValue('comment', '');
        setFieldValue('files', []);
        setFieldValue('uiIds', []);
        setIsProcess(false);
      }}
    >
      {({
        setFieldValue, values, handleSubmit,
      }) => (
        <StyledForm id={currentId} className="slateEditor" noPadding={noPadding} customPadding={customPadding}>
          <AutoSaveComment id={associatedId} commentWasRead={commentCheck} />
          <SlateEditor
            setFieldValue={(field: string, val: string) => {
              setFieldValue(field, val);
            }}
            value={values.comment}
            valueFiles={values.files}
            editValue={value}
            wrapReply
            closeEdit={() => {
              setEdit(null);
              setReplyComment(null);
              setFieldValue('comment', '');
            }}
            field="comment"
            handleSubmit={handleSubmit}
            userSuggestions={userSuggestions}
            uploadImageCallBack={(file: File, uiId: string) => {
              if (filesRef.current) filesRef.current.push({ file, uiId });
              setFieldValue('files', [...filesRef.current, { file, uiId }]);
            }}
            // @ts-ignore
            position={position ?? 'bottom'}
            setImages={setImages}
            setIsOpenLightbox={setIsOpenLightbox}
            setPhotoIndex={setPhotoIndex}
            files={isEdit?.files ? isEdit.files : []}
            placeholder="What do you think about this?"
            // @ts-ignore
            isReply={!!replyComment}
            replyData={replyComment && {
              ...replyComment,
              data: {
                ...replyComment?.data,
                createdBy: {
                  profile: users[replyComment.createdBy],
                },
              },
              rawText: replyComment.rawValue,
            }}
            submitOnEnter
            withAutoFocus
            maxInputHeight={withSeparateView ? 200 : 500}
          />
        </StyledForm>
      )}
    </Formik>
  );
};
