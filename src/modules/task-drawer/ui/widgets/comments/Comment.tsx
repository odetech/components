import React, {
  Fragment, useEffect, useMemo,
} from 'react';
// @ts-ignore
import { SlateReader } from '~/components/editor';
import { Avatar } from '~/components/avatar';
import { format } from 'date-fns';
import { ArrowLeftIcon, PencilIcon, TrashIcon2 } from '~/components/icons';
import ContentLoader from 'react-content-loader';
import styled from '@emotion/styled';
import { useIsOnline } from '~/hooks/useIsOnline';
import { $$usersModel } from '~/entities/users';
import { useUnit } from 'effector-react';
import { Reactions } from '~/entities/reactions';
import {
  Footer,
  FooterControls,
  Header,
  Info,
  Time,
  UserName,
  Wrapper,
  UserInfoWrapper,
  HighLightMessage, CollapseComment,
} from './styles/comments';
import { defaultTheme } from '~/styles/themes';
import { useTheme } from '@storybook/theming';

type CommentProps = {
  comment: any,
  userId: string,
  withThread?: boolean;
  withThreads?: boolean;
  isCollapsed?: boolean;
  canBeCollapsed?: boolean;
  toggleCollapse?: () => void;
  setActiveThread?: (activeThread: string) => void;
  disabledButtons: boolean;
  deleteModal: any;
  threadsCount?: number;
  isFirst?: boolean;
  setImages: (images: string[]) => void;
  setIsOpenLightbox: (isOpen: boolean) => void;
  setPhotoIndex: (index: number) => void;
  setEdit: any;
  setReplyComment?: any;
  highlightMessage?: {
    comment: string | null,
    reply: string | null,
  },
};

export const Comment: React.FC<CommentProps> = ({
  comment, userId, withThread, disabledButtons, deleteModal,
  withThreads, setActiveThread, threadsCount, setIsOpenLightbox, setImages,
  setPhotoIndex, setEdit, setReplyComment, highlightMessage, isCollapsed, canBeCollapsed, toggleCollapse, isFirst,
}) => {
  const theme = useTheme();
  const setProfilePreviewUserData = (props: any) => console.warn('setProfilePreviewUserData should be defined', props);

  const users = useUnit($$usersModel.$users);
  const isLoading = useUnit($$usersModel.$usersPending);
  const isOnline = useIsOnline(comment.createdBy);

  const avatarEntity = useMemo(() => {
    const { createdBy: commentCreatedBy, user: commentUser } = comment;
    const self = users[commentCreatedBy] || {};

    return {
      profile: {
        firstName: self.firstName || commentUser.firstName,
        lastName: self.lastName || commentUser.lastName,
        avatar: {
          secureUrl: self.avatarUrl || commentUser.avatarUrl,
        },
      },
    };
  }, [comment, users]);

  const isHighlighted = highlightMessage?.reply === comment.id
    || (!highlightMessage?.reply && highlightMessage?.comment === comment.id);

  useEffect(() => {
    if (highlightMessage?.reply && !comment?.commentAssociatedId
      && highlightMessage?.comment === comment.id) {
      setActiveThread?.(comment.id);
    }
  }, [highlightMessage]);

  const getLoomVideoLinks = (localComment: any) => {
    const { links } = localComment;

    if (links.length) {
      return Array.from(new Set(links
        .filter((link: any) => link.includes('loom.com/share'))
        .map((link: any) => (link.indexOf('"') !== -1 ? link.slice(0, link.indexOf('"')) : link))));
    }

    return null;
  };

  const openUserProfile = () => {
    const user = users[comment.createdBy] || {};
    setProfilePreviewUserData?.({ _id: user.id });
  };

  return (
    <Wrapper key={comment.id} id={comment.id} withTreadStyles={withThread} isFirst={isFirst}>
      <Header>
        <Info isOpened={canBeCollapsed ? (!isCollapsed ?? true) : true}>
          <UserInfoWrapper isOnline={!!isOnline}>
            {/* eslint-disable-next-line max-len */}
            {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
            <div onClick={openUserProfile}>
              {isLoading ? (
                <ContentLoader
                  key="avatar-2"
                  viewBox="0 0 26 26"
                  style={{
                    height: '26px', minWidth: '26px', display: 'flex', flexDirection: 'column',
                  }}
                >
                  <rect x="0" y="0" rx="50" ry="50" width="26" height="26" />
                </ContentLoader>
              ) : (
                <Avatar
                  size={26}
                  user={avatarEntity}
                />
              )}
              {isLoading ? (
                <ContentLoader
                  key="name-2"
                  viewBox="0 0 100 24"
                  style={{
                    height: '24px', minWidth: '100px', display: 'flex', flexDirection: 'column', marginLeft: '10px',
                  }}
                >
                  <rect x="0" y="0" rx="5" ry="5" width="100" height="24" />
                </ContentLoader>
              ) : (
                <UserName>{`${users[comment.createdBy]?.name || 'Unnamed'}`}</UserName>
              )}
            </div>
            <Time>{format(new Date(comment.createdAt), 'PP \'at\' p')}</Time>
            {canBeCollapsed && (
              // eslint-disable-next-line @typescript-eslint/no-use-before-define
              <CollapsedButton type="button" onClick={toggleCollapse} isCollapsed={Boolean(isCollapsed)}>
                <span>Task details</span>
                <ArrowLeftIcon />
              </CollapsedButton>
            )}
          </UserInfoWrapper>
          <CollapseComment isOpened={canBeCollapsed ? (!isCollapsed ?? true) : true}>
            <SlateReader
              value={comment?.value || ''}
              setShowModal={() => ({})}
              setPdfFile={() => ({})}
              setImages={setImages}
              setIsOpenLightbox={setIsOpenLightbox}
              setPhotoIndex={setPhotoIndex}
              files={comment?.files || []}
              // @ts-ignore
              links={getLoomVideoLinks(comment)}
              onMentionClick={(mentionUser: string) => {
                setProfilePreviewUserData?.({
                  _id: mentionUser,
                });
              }}
            />
          </CollapseComment>
        </Info>
      </Header>
      <CollapseComment isOpened={canBeCollapsed ? (!isCollapsed ?? true) : true}>
        <Footer>
          <button
            type="button"
            id={`reply-${comment.id}`}
            onClick={() => {
              setReplyComment(comment);
              setEdit(null);
            }}
            disabled={disabledButtons}
          >
            Reply
          </button>
          {(!!threadsCount) && (
            <button
              type="button"
              onClick={() => {
                setActiveThread?.(withThreads ? '' : comment.id);
              }}
            >
              {withThreads ? 'Hide replies' : 'Show replies'}
              {`(${threadsCount})`}
            </button>
          )}
          <FooterControls>
            <Reactions
              itemType="tasks"
              itemData={{
                ...comment,
                ...comment.reactions,
              }}
              messageId={comment.id}
              position="left"
              isSmall
            />
            {comment.createdBy === userId
              && (
                <Fragment>
                  {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
                  <button
                    type="button"
                    onClick={() => {
                      setEdit({
                        ...comment,
                        isReply: withThread,
                      });
                      setReplyComment(null);
                    }}
                    disabled={disabledButtons}
                  >
                    <PencilIcon height="22px" width="22px" />
                  </button>
                  {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
                  <button
                    type="button"
                    onClick={() => {
                      deleteModal();
                    }}
                    disabled={disabledButtons}
                    className="commentPencilButton"
                  >
                    <TrashIcon2 fill={theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar} />
                  </button>
                </Fragment>
              )}
          </FooterControls>
        </Footer>
      </CollapseComment>
      {isHighlighted && <HighLightMessage id="selected-line">Message you were looking for  ↑</HighLightMessage>}
    </Wrapper>
  );
};

const CollapsedButton = styled('button') <{ isCollapsed: boolean }>`
  box-shadow: none;
  border: 0;
  margin-left: auto;
  background: transparent;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;

  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  
  &:active, &:focus, &:hover {
    box-shadow: none;
    border: 0;
    background: transparent;
  }
  
  span {
    margin-right: 10px;
  }
  
  svg {
    transform: rotate(${({ isCollapsed }) => (isCollapsed ? '270' : 90)}deg);
    height: 12px;
  }
`;
