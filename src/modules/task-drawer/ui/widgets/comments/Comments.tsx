import React, {
  Fragment,
  useEffect, useRef, useState,
} from 'react';
import { $$commentsModel, TComment } from '~/entities/comments';
import Lightbox from 'react-image-lightbox';
import { useHistory, useLocation } from 'react-router';
import * as qs from 'query-string';
import { differenceInSeconds } from 'date-fns';
import { $currentUser } from '~/config';
import { useStoreMap, useUnit } from 'effector-react';
import { DeleteModal } from '~/components/default-components';
import {
  createEvent, createStore,
} from 'effector';
import { CommentForm } from './ui/CommentForm';
import {
  renderCommentsLoaders,
} from './helpers';
import {
  CommentsLoaderWrapper, Outer, Inner, CommentThreadWrapper, NoCommentWrapper, CommentsWithFormWrapper,
} from './styles/comments';
import { Threads } from './Threads';
import { Comment } from './Comment';
// @ts-ignore
import EmptyMessage from './ui/empty-message.png';

type CommentsProps = {
  activeTask: string,
  styles?: string,
  noPadding?: boolean;
  position?: string;
  isConversation?: boolean;
  scrollToLast?: boolean;
  currentActiveTask?: any;
  withSeparateView?: boolean;
  highlightMessage?: {
    comment: string | null,
    reply: string | null,
  },
  unreadTasks?: Record<string, any>;
};

const $activeThread = createStore('');
const activeThreadSet = createEvent<string>();
$activeThread.on(activeThreadSet, (_, thread) => thread);

export const Comments: React.FC<CommentsProps> = ({
  activeTask,
  withSeparateView,
  highlightMessage,
  unreadTasks,
  currentActiveTask,
  styles,
  noPadding,
  isConversation,
  position,
  scrollToLast,
}) => {
  const history = useHistory();
  const { search } = useLocation();
  const [deleteMessage, setDeleteMessage] = useState<null | { id: string, withThread?: boolean }>(null);
  const comments = useStoreMap({
    store: $$commentsModel.$comments,
    keys: [activeTask],
    fn: (commentsMap, [id]) => ({ ref: commentsMap.ref.get(id) || new Map() }),
  });
  const deleteComment = useUnit($$commentsModel.events.deleteTaskMessage);
  const commentsLoading = useUnit($$commentsModel.$pending);
  const fetchComments = useUnit($$commentsModel.fetched);
  const readMessages = useUnit($$commentsModel.events.readTaskMessages);
  const user = useUnit($currentUser);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const [images, setImages] = useState<string[]>([]);
  const [isOpenLightbox, setIsOpenLightbox] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [isEdit, setEdit] = useState<any>(null);
  const [replyComment, setReplyComment] = useState<any>(null);
  const [activeThread, setActiveThread] = useState('');
  const [isDescriptionCollapsed, setCollapsed] = useState(false);

  const toggleCollapse = () => setCollapsed((prevState) => !prevState);
  const containerRef = useRef<HTMLDivElement>(null);

  const choseTask = currentActiveTask || unreadTasks?.[activeTask] || {};
  const { projectId } = choseTask;

  useEffect(() => {
    if (activeTask) {
      fetchComments(activeTask);
    }
  }, [activeTask, fetchComments]);

  useEffect(() => {
    Array.from(comments.ref.values())
      .forEach((t) => {
        if (t.views && !t.views.includes(user._id)) readMessages([t.id]);
        if (Object.keys(t.threads || {}).length) {
          Object.values(t.threads || {}).forEach((m: any) => {
            if (m.views && !m.views.includes(user._id)) readMessages([m.id]);
          });
        }
      });
  }, [comments]);

  useEffect(() => {
    const commentsArray = comments ? Array.from(comments.ref.values()) : [];
    if (scrollToLast && commentsArray.length) {
      setTimeout(() => {
        document.getElementById(`reply-${commentsArray[commentsArray.length - 1].id}`)
          ?.scrollIntoView({ behavior: 'smooth', block: 'center' });
      }, 500);
    }
  }, [scrollToLast, comments]);

  useEffect(() => {
    const { comment, reply } = highlightMessage || {};
    setTimeout(() => {
      if (comment) {
        if (reply) {
          setActiveThread(comment);
          if (comments.ref.get(comment)?.threads?.[reply]) {
            document.getElementById(reply)?.scrollIntoView({ behavior: 'smooth' });
          }
        } else if (comments.ref.get(comment)) {
          document.getElementById(comment)?.scrollIntoView({ behavior: 'smooth' });
        }
        const query = qs.parse(search);
        if (query.comment || query.reply) {
          delete query.comment;
          delete query.reply;
          history.push({
            search: qs.stringify(query),
          });
        }
      }
    }, 500);
  }, [containerRef, highlightMessage, comments, search]);

  const moveToId = (id: string) => {
    setTimeout(() => {
      containerRef?.current?.querySelector?.(`[id='${id}']`)
        ?.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }, 300);
  };

  useEffect(() => {
    if (replyComment?.id) {
      moveToId(`reply-form-${replyComment.id}`);
    }
  }, [replyComment]);

  const renderContent = () => {
    if (commentsLoading) {
      if (withSeparateView) {
        return (
          <Inner withSeparateView={withSeparateView} isLoading={commentsLoading}>
            {renderCommentsLoaders(5)}
          </Inner>
        );
      }
      return (
        <CommentsLoaderWrapper>
          {renderCommentsLoaders(3)}
        </CommentsLoaderWrapper>
      );
    }

    return (
      <Fragment>
        <Inner withSeparateView={withSeparateView} ref={containerRef} styles={styles}>
          {comments?.ref.size === 0
            ? (
              <NoCommentWrapper
                withSeparateView={withSeparateView}
              >
                <img src={EmptyMessage} alt="empty-dialog" />
                <span>Send your first message!</span>
              </NoCommentWrapper>
            )
            : Array.from(comments.ref.values()).map((comment, index) => (
              <CommentsWithFormWrapper>
                <CommentThreadWrapper
                  key={comment.id}
                  withTreads={activeThread === comment.id && !!Object.keys(comment?.threads || {}).length}
                >
                  <Comment
                    comment={comment}
                    userId={user?._id}
                    disabledButtons={isEdit}
                    deleteModal={() => {
                      setDeleteMessage({
                        id: comment.id,
                      });
                      setShowDeleteModal(true);
                    }}
                    setActiveThread={setActiveThread}
                    isFirst={index === 0}
                    withThreads={activeThread === comment.id}
                    threadsCount={comment?.threads ? Object.keys(comment?.threads).length : 0}
                    setImages={setImages}
                    setIsOpenLightbox={setIsOpenLightbox}
                    setPhotoIndex={setPhotoIndex}
                    setEdit={setEdit}
                    setReplyComment={setReplyComment}
                    highlightMessage={highlightMessage}
                    isCollapsed={isDescriptionCollapsed}
                    canBeCollapsed={
                      Math.abs(differenceInSeconds(new Date(choseTask.createdAt!), new Date(comment.createdAt))) < 5
                    }
                    toggleCollapse={toggleCollapse}
                  />
                  {(!isDescriptionCollapsed || index !== 0) && activeThread === comment.id && (
                    <Threads
                      threads={comment?.threads}
                      userId={user?._id}
                      associatedId={comment.id}
                      setDeleteMessage={(id: string) => {
                        setDeleteMessage({
                          id,
                          withThread: true,
                        });
                        setShowDeleteModal(true);
                      }}
                      setImages={setImages}
                      setIsOpenLightbox={setIsOpenLightbox}
                      setPhotoIndex={setPhotoIndex}
                      setEdit={setEdit}
                      setReplyComment={setReplyComment}
                      highlightMessage={highlightMessage}
                    />
                  )}
                </CommentThreadWrapper>
                {(comment.id === replyComment?.id || replyComment?.associatedId === comment.id) && (
                  <CommentForm
                    projectId={projectId}
                    customPadding="20px"
                    position={position}
                    noPadding={noPadding}
                    activeTask={activeTask}
                    setActiveThread={setActiveThread}
                    choseTask={choseTask}
                    setImages={setImages}
                    withSeparateView={withSeparateView}
                    isEdit={isEdit}
                    setEdit={setEdit}
                    replyComment={replyComment}
                    setReplyComment={setReplyComment}
                    setIsOpenLightbox={setIsOpenLightbox}
                    setPhotoIndex={setPhotoIndex}
                    userId={user?._id}
                    scrollTo={moveToId}
                  />
                )}
              </CommentsWithFormWrapper>
            ))}
        </Inner>
        <CommentForm
          projectId={projectId}
          noPadding={noPadding}
          position={position}
          activeTask={activeTask}
          setActiveThread={setActiveThread}
          choseTask={choseTask}
          setImages={setImages}
          withSeparateView={withSeparateView}
          isEdit={isEdit}
          setEdit={setEdit}
          setReplyComment={setReplyComment}
          setIsOpenLightbox={setIsOpenLightbox}
          setPhotoIndex={setPhotoIndex}
          userId={user?._id}
          scrollTo={moveToId}
        />
      </Fragment>
    );
  };

  return (
    <Outer
      withSeparateView={withSeparateView}
      isConversation={isConversation}
      className="comments-widget"
    >
      {renderContent()}
      {isOpenLightbox && (
        <Lightbox
          mainSrc={images[photoIndex]}
          nextSrc={images[(photoIndex + 1) % images.length]}
          prevSrc={images[(photoIndex + images.length - 1) % images.length]}
          onCloseRequest={() => setIsOpenLightbox(false)}
          onMovePrevRequest={() => setPhotoIndex((photoIndex + images.length - 1) % images.length)}
          onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % images.length)}
        />
      )}
      {showDeleteModal && (
        <DeleteModal
          showModal={showDeleteModal}
          setShowModal={(setShowModalData) => {
            setShowDeleteModal(setShowModalData);
            setDeleteMessage(null);
          }}
          onSubmit={() => {
            if (deleteMessage) {
              // deleteComments({ id: deleteMessage.id, withThread: deleteMessage.withThread });
              deleteComment({ _id: deleteMessage.id, withThread: deleteMessage.withThread });
            }
          }}
          title="Delete comment"
          infoText="Are you sure you want to delete this comment?"
        />
      )}
    </Outer>
  );
};
