import styled from '@emotion/styled';
import { Form } from 'formik';
import { rgba } from 'polished';
import { UnmountClosed } from 'react-collapse';

export const CommentsLoaderWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const Header = styled('div')`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  font-size: 15px;
  line-height: 1.6;
  font-weight: 500;
`;

export const Info = styled('div')<{ isOpened?: boolean }>`
  margin-right: 5px;
  width: 100%;
  
  .task-value {
    display: block;
    max-width: 100%;
    word-break: normal;
    color: #3b3c3b;
    padding-left: 35px;
    
    & a {
      color: #9C9C9C;
      
      &:hover {
        color: #E54367;
      }
    }
  }
  
  & > div:last-of-type {
    padding-left: ${({ isOpened }) => (isOpened ? '35' : 0)}px;
  }
`;

export const UserInfoWrapper = styled('div')<{ isOnline?: boolean }>`
  display:flex;
  align-items:center;
  margin-bottom: 10px;
  width: 100%;
  
  & > div:first-of-type {
    display:flex;
    align-items:center;
    cursor: pointer;
    
    & p:hover {
      color: ${rgba('#0095E9', 0.8)};
    }
  }

  .ReactCollapse--collapse {
    transition: height 500ms;
  }

  > div:first-of-type > p:first-of-type {
    position: relative;
  }
  
  > div:first-of-type > p:first-of-type:after {
    position: absolute;
    content: "";
    background: ${({ isOnline }) => (isOnline ? '#4BC133' : '#9C9C9C')};
    border-radius: 50%;
    width: 8px;
    height: 8px;
    border: 1px solid white;
    bottom: 0;
    left: -17px;
  }
  }
`;

export const UserName = styled('p')`
  font-size: 15px;
  color: #0095E9;
  margin-bottom: 0;  
  margin-right: 10px;
  margin-left: 10px;
  
  flex-shrink: 0;
`;

export const Wrapper = styled('div')<{ withTreadStyles?: boolean, isFirst?: boolean }>`
  padding: 15px 0 15px;
  ${({ isFirst }) => (isFirst ? 'padding-top: 0px;' : '')}
  position: relative;
  z-index: 1;
  
  ${({ withTreadStyles }) => withTreadStyles && `
    width: calc(100% - 36px);
    margin: 0 0 0 auto;
    padding: 10px 0;
    position: relative;
    
    &::before {
      content: "";
      background-color: #c4c4c4;
      display: block;
      position: absolute;
      width: 13px;
      height: 1px;
      top: 23px;
      right: calc(100% + 10px);
    }
    
    &:last-of-type {
      &::after {
        content: "";
        background-color: white;
        display: block;
        position: absolute;
        width: 1px;
        top: 24px;
        bottom: 0px;
        right: calc(100% + 22px);
      }
    }
  `};
`;

export const CommentsWithFormWrapper = styled('div')`
  &:not(:last-of-type) {
    border-bottom: 2px solid #E5E5E5;
  }
`;

export const CommentThreadWrapper = styled('div')<{ withTreads: boolean }>`
  position: relative;
  
  ${({ withTreads }) => withTreads && `
    &::after {
      content: "";
      background-color: #c4c4c4;
      display: block;
      position: absolute;
      width: 1px;
      top: 20px;
      right: calc(100% - 14px);
      bottom: 0;
    }
  `};
`;

export const Inner = styled('div')<{ withSeparateView?: boolean; isLoading?: boolean, styles?: string }>`
  margin-bottom: 25px;
  overflow-y: auto;
  padding-right: 25px;
  flex-basis: fit-content;
  scrollbar-color: #eee #FFF;
  scrollbar-width: thin;

  &::-webkit-scrollbar-thumb {
      background-color: #B1B1B1;
      border-radius: 10px;
  }
  &::-webkit-scrollbar {
      width: 6px;
      height: 6px;
      background-color: #F0F0F0;
  }
  ${({ withSeparateView, isLoading, styles }) => withSeparateView && `
    max-height: none;
    background-color: white;
    flex-basis: 100vh;
    ${styles ?? `
      padding-left: 25px;
      padding-bottom: 25px;
    `}
    border-radius: 6px;
    ${isLoading && 'margin-bottom: 0;'}
    ${isLoading && 'padding-top: 30px;'}
  `}
`;

export const Outer = styled('div')<{ withSeparateView?: boolean, isConversation?: boolean }>`
  display: flex;
  flex-direction: column;
  max-height: 100%;
  overflow: auto;
`;

export const Time = styled('span')`
  font-size: 12px;
  font-weight: 400;
  color: #9C9C9C;
  flex-shrink: 0;
`;

export const StyledForm = styled(Form)<{ noPadding?: boolean, customPadding?: string }>`
  margin-top: auto;
  z-index: 1;
  scroll-padding: 30px;
  ${({ noPadding }) => !noPadding && `
    padding-right: 35px;
    padding-bottom: 25px;
  `}

  ${({ customPadding }) => customPadding && `padding: ${customPadding};`}
`;

export const Footer = styled('div')`
  padding-left: 36px;
  margin-top: 10px;
  display: flex;
  align-items: center;
  
  & button {
    background-color: transparent;
    border: none;
    padding: 0;
    font-size: 13px;
    font-weight: 500;
    color:#9C9C9C;
    display:flex;
    align-items:center;
    transition: all .3s;
    
    & svg {
        fill: #9C9C9C;
    }
    
    &:hover:not(:disabled) {
      color: #F7942A;
      
      & svg {
        fill: #F7942A;
        
        & path[stroke] {
          stroke: #F7942A;
        }
      }
    }
    
    &:disabled {
      opacity: 0.3;
    }
  }
  
  & > button {
    &:not(:last-of-type) {
      margin-right: 20px;
    }
    
    &:not(:first-of-type) {
      position: relative;
      
      &::before {
        content: "";
        width: 3px;
        height: 3px;
        border-radius: 50%;
        position: absolute;
        left: -10px;
        background-color: #C4C4C4;
        top: 50%;
        transform: translateY(-50%);
      }
    }
  }
`;

export const FooterControls = styled('div')`
  margin-left: auto;
  display: flex;
  align-items: center;
  
  & .commentPencilButton {
    margin: 0 5px;
  }
`;

export const NoCommentWrapper = styled('p')<{ withSeparateView?: boolean }>`
  margin-top: ${({ withSeparateView }) => (withSeparateView ? '0px' : '20px')};
  height: max-content;
  color: #9C9C9C;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  gap: 24px;
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
  
  img {
    width: 120px;
    height: 120px;
    object-fit: contain;
  }
`;

export const TaskInfoWrapper = styled('div')<{ withBorder?: boolean }>`
  margin-right: 20px;
  display: flex;
  position: relative;
  gap: 20px;
  align-items: center;
  padding-bottom: 10px;
  margin-bottom: 20px;
  ${({ withBorder }) => (withBorder ? 'border-bottom: 1px solid #E5E5E5;' : '')}  

  & > p:nth-of-type(1) {
    font-weight: 500;
    margin-bottom: 10px;
  }

  h2 {
    width: fit-content;
  }
`;

export const HighLightMessage = styled('div')`
  padding: 0px 10px;
  margin-top: 20px;
  flex-shrink: 0;
  color: rgb(243, 56, 34);
  display: flex;
  justify-content: space-between;
  align-items: center;
  white-space: nowrap;
  
  &::before,
  &::after {
    content: "";
    display:block;
    width: calc(50% - 135px);
    height: 1px;
    background-color: rgb(243, 56, 34);
  }
`;

export const ChipsWrapper = styled('div')`
  display: grid;
  grid-template-columns: minmax(67px, 100px) 100px 140px 140px;
  grid-column-gap: 5px;
  
  & > div {
    z-index: 2;
  }
`;

export const CommentsEditingWrapper = styled('div')`
  margin-top: 18px;
  padding-left: 6px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const MarkAsClosedButton = styled('button')<{ closed: boolean }>`
  width: 130px;
  height: 32px;
  padding: 0 8px;
  border: 1px solid ${({ closed }) => (closed ? '#10B20D' : '#9C9C9C')};
  background-color: ${({ closed }) => (closed ? 'rgba(16, 178, 13, 0.1)' : 'transparent')};
  border-radius: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
  
  font-size: 13px;
  font-weight: 500;
  color: ${({ closed }) => (closed ? '#10B20D' : '#9C9C9C')};
  
  &:hover {
    opacity: 0.7;
    transition: opacity 0.3s;
  }
`;

export const TaskTitle = styled('input')`
  border: 1px solid transparent;
  border-radius: 5px;
  font-size: 20px;
  font-weight: 600;
  color: #3B3C3B;
  line-height: 1.2;
  padding: 2px 6px;
  cursor: pointer;
  width: 100%;
  resize: none;
  overflow: hidden;
  
  &:hover,
  &:focus {
    border-color: #E5E5E5;
    outline: none;
  }
`;

export const TaskTitleStatic = styled(TaskTitle)`
  white-space: pre-line;
`.withComponent('h2');

export const CollapseComment = styled(UnmountClosed)``;
