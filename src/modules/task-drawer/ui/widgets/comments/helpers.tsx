import React from 'react';
import ContentLoader from 'react-content-loader';

export const renderCommentsLoaders = (num: number) => {
  const mockedTasks = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < num; i++) {
    mockedTasks.push(
      <ContentLoader
        key={`loader${i}`}
        viewBox="0 0 380 70"
        style={{
          height: '70px', marginBottom: '10px', display: 'flex', flexDirection: 'column',
        }}
      >
        <rect x="0" y="0" rx="5" ry="5" width="62" height="66" />
        <rect x="80" y="0" rx="4" ry="4" width="150" height="15" />
        <rect x="80" y="25" rx="3" ry="3" width="50" height="10" />
        <rect x="80" y="55" rx="3" ry="3" width="100" height="10" />
      </ContentLoader>,
    );
  }
  return mockedTasks;
};

export const getDeleteCommentModal = (
  entityId: string,
  title: string,
  label: string,
  deleteActionArgs?: any[],
  callback?: () => void,
): any => ({
  modalComponent: 'DELETE_MODAL',
  modalProps: {
    title,
    label,
    entityId,
    deleteAction: 'DELETE_COMMENT',
    deleteActionArgs,
    callback,
  },
});

export const getReplyValue = (
  replyComment: any,
  usersDict: any,
  // eslint-disable-next-line max-len
) => `<p><span data-mention data-id="${usersDict[replyComment.createdBy]?.id}" data-firstname="${usersDict[replyComment.createdBy]?.firstName}" data-lastname="${usersDict[replyComment.createdBy]?.lastName}" href="undefined">@${usersDict[replyComment.createdBy]?.firstName} ${usersDict[replyComment.createdBy]?.lastName}</span>,</p>`;
