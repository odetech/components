import React, {
  CSSProperties, useCallback, useEffect, useState,
} from 'react';
import styled from '@emotion/styled';
import { EyeIcon, EyeOffIcon } from '~/components/icons';
import { Textarea } from '~/components/default-components';

// region styles
// eslint-disable-next-line max-len
const Label = styled('label')<{ paddingBottom?: number; marginBottom?: number; width?: string; margin?: string; disabledWrapper?: boolean }>`
  position: relative;

  color: #9C9C9C;
  width: ${({ width }) => width ?? '100%'};
  display: inline-flex;
  flex-direction: column;
  align-items: flex-start;
  padding-bottom: ${({ paddingBottom }) => (typeof paddingBottom === 'number' ? paddingBottom : 19)}px;
  ${({ margin }) => margin && `margin: ${margin}`};
  ${({ marginBottom }) => marginBottom && `margin-bottom: ${marginBottom}px;`};
  ${({ disabledWrapper }) => disabledWrapper && `
    opacity: 0.5;
  `};
    
  input {
    max-width: 100%;
    width: 100%;
  }  
    
  label {
    width: 100%;
  }  
`;

const ErrorStyled = styled('span')<{ errorBottom?: number }>`
  position: absolute;
  left: 0;
  bottom: 3px;

  font-size: 11px;
  color: #F33822;
  white-space: pre-wrap;
  max-width: fit-content;
  ${({ errorBottom }) => errorBottom && `bottom: ${errorBottom}px;`};
`;

const StyledLabelText = styled('span')`
  margin-bottom: 5px;
  
  font-size: 14px;
`;

const IconButton = styled('button')`
  background-color: transparent;
  border: none;
  position: absolute;
  right: 0;
  bottom: 20px;
  transition: opacity .3s;
  
  &:hover {
    opacity: 0.8;
  }
`;

export const InputWithLabel: React.FC<{
  inputProps: any;
  marginBottom?: number;
  errorBottom?: number;
  label?: string;
  labelStyle?: CSSProperties;
  width?: string;
  name?: string;
  onChange: (value: string) => void;
  value?: string;
  error?: string;
  margin?: string;
  withPasswordIcon?: boolean;
  isStatic?: boolean;
  touched?: boolean;
  paddingBottom?: number;
}> = ({
  inputProps,
  name,
  onChange,
  value,
  marginBottom,
  errorBottom,
  label,
  labelStyle,
  width,
  margin,
  withPasswordIcon,
  isStatic,
  paddingBottom,
  touched,
  error,
}) => {
  const [isVisible, setVisible] = useState(false);
  const [elementRect, setElementRect] = useState<HTMLInputElement | null>(null);

  const handleRect = useCallback((node) => {
    setElementRect(node);
  }, []);

  const customOnChange = (innerValue: string) => {
    onChange(innerValue);
    if (elementRect) {
      if (innerValue) {
        // eslint-disable-next-line no-param-reassign
        elementRect.style.height = `${elementRect.scrollHeight}px`;
      } else {
        // eslint-disable-next-line no-param-reassign
        elementRect.style.height = '135px';
      }
    }
  };

  useEffect(() => {
    if (elementRect) {
      if (!value) {
        elementRect.style.height = '135px';
      }
    }
  }, [elementRect, value]);

  const { type, disabled } = inputProps;
  return (
    <Label
      marginBottom={marginBottom}
      width={width}
      margin={margin}
      disabledWrapper={disabled}
      paddingBottom={paddingBottom}
    >
      {label && <StyledLabelText style={labelStyle}>{label}</StyledLabelText>}
      {isStatic ? (
        <Textarea
          {...inputProps}
          type={(isVisible && inputProps.type === 'password') ? 'text' : (inputProps.type || 'text')}
          name={name}
          minHeight="135px"
          maxHeight="332px"
          ref={handleRect}
          error={touched && error}
          onChange={customOnChange}
        >
          {value}
        </Textarea>
      ) : (
        <Textarea
          {...inputProps}
          minHeight="135px"
          maxHeight="332px"
          type={(isVisible && inputProps.type === 'password') ? 'text' : (inputProps.type || 'text')}
          value={value}
          ref={handleRect}
          onChange={customOnChange}
          error={touched && error}
        />
      )}
      <ErrorStyled errorBottom={errorBottom}>{touched && error}</ErrorStyled>
      {type === 'password' && withPasswordIcon && (
        <IconButton type="button" onClick={() => setVisible(!isVisible)} tabIndex={-1}>
          {isVisible ? <EyeIcon /> : <EyeOffIcon />}
        </IconButton>
      )}
    </Label>
  );
};
