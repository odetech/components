import React, {
  forwardRef, Fragment, useEffect, useRef, useState,
} from 'react';
import styled from '@emotion/styled';
import {
  addDays, endOfDay, format, startOfDay, sub,
} from 'date-fns';
import { EditableText } from '~/components/editable-text';
import {
  ClockIcon, PencilIcon, TrashIcon2,
} from '~/components/icons';
import { TTask } from '~/entities/tasks/types';
import {
  TTimeTracker, TTimeTrackers, $$timeTrackersModel,
} from '~/entities/time-trackers';
import { FormikDatePicker } from '~/components/formik';
import { useUnit } from 'effector-react';
import { $currentUser } from '~/config';
import { BeatLoader } from 'react-spinners';
import { ellipsis } from 'polished';
import ReactDOM from 'react-dom';
import { TimeInput } from './time-input';
import { UserChip } from '../../fields/user-chip';
import { secondToHumanFormat, getTimeTrackingValue } from '../../../lib';
import {
  TimeTrackerHorizontalForm,
} from './time-tracker-horizontal-form';
import { TInitialValues } from './model';

type TimeTrackerManageFormProps = {
  onEstimatedTimeChange: (value: string) => void;
  onTrackTime?: () => void;
  horizontalFormSubmitted: (values: TInitialValues) => void;
  onChangeMessage: (values: {trackerId: string, message: string}) => void;
  onChangeTime: (values: {trackerId: string, time: string}) => void;
  onChangeDate: (values: {trackerId: string, date: Date}) => void;
  removeTimeTracker: (trackerId: string) => void;
  activeTimeTracker: TTimeTracker | null;
  renderButtonIcon?: () => JSX.Element;
  timeTrackers: TTimeTrackers,
  users: any;
  isPause?: boolean;
  isLoading?: boolean;
  task: Partial<TTask>;
  ExtraHeader?: React.ReactNode;
  showSpentTime?: boolean;
};

const TextButton = styled('button')<{ showMore?: boolean; moreButton?: boolean }>`
  text-align: left;
  border: 0;
  box-shadow: none;
  background: transparent;
  color: #3B3C3B;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

  font-size: 14px;
  line-height: 20px;
  width: 100%;
  max-width: 100%;

  &:hover, &:active, &:focus {
    box-shadow: none;
    border: 0;
    background: transparent;
  }
`;

const CollapseButton = styled('button')`
  color: #9C9C9C;
  font-weight: 500;
  padding: 2px 8px !important;
  font-size: 14px;
  line-height: 20px;
  background: none;
  margin-bottom: 20px;  
  border: 0;
  box-shadow: none;
    
  &:hover, &:active, &:focus {
    background: none;
    border: 0;
    box-shadow: none;
  }  
`;

const TextButtonAsP = styled(TextButton)`
  margin: 20px 0;
  padding: 0;  
  ${({ showMore }) => showMore && ellipsis(undefined, 3)}
  white-space: pre-line;
  ${({ moreButton }) => (moreButton ? 'margin-bottom: 2px;' : '')}
`.withComponent('p');

const AddNotePlaceholder = styled('span')`
  color: #9C9C9C;
`;

const Text = ({ children, ...props }: { children: React.ReactNode }) => {
  const [short, setShort] = useState<boolean>(false);
  const previousRef = useRef<string>('');
  const [moreButton, setMoreButton] = useState<boolean>(false);
  const descTextEl = useRef(null);

  useEffect(() => {
    const currentEl = descTextEl.current;
    let checkHeight = false;
    if (previousRef.current === '' && children !== '') {
      previousRef.current = children as string;
      checkHeight = true;
    }
    if (checkHeight) {
      // @ts-ignore
      const elementStyle = document.defaultView.getComputedStyle(
        // @ts-ignore
        // eslint-disable-next-line react/no-find-dom-node
        ReactDOM.findDOMNode(currentEl),
        null,
      ); // getBoundingClientRect
      let divHeight: number | string = elementStyle.getPropertyValue('height');
      divHeight = parseInt(divHeight.replace('px', ''), 10);
      const lines = divHeight / 20;
      setTimeout(() => {
        setMoreButton(lines > 3);
        setShort(lines > 3);
      }, 200);
    }
  }, [children]);

  return (
    <Fragment>
      <TextButtonAsP {...props} ref={descTextEl} showMore={short} moreButton={moreButton}>
        {children || <AddNotePlaceholder>Add note</AddNotePlaceholder>}
      </TextButtonAsP>
      {moreButton ? (
        <CollapseButton
          type="button"
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setShort((prevState) => !prevState);
          }}
        >
          {`${short ? '+ Show more' : '- Show less'}`}
        </CollapseButton>
      ) : null }
    </Fragment>
  );
};

const Time = styled('div')`
  white-space: nowrap;
  font-weight: 400;
  font-size: 14px;
  min-width: 115px;
  width: 115px;
  display: flex;
  flex-direction: column;
  line-height: 20px;
  color: #3B3C3B;
  
  span {
    color: #9C9C9C;
  }
`;

const TimeComponent = ({ value, ...props }: { value: string }) => {
  const time = parseInt(value, 10);
  return (
    <Time {...props}>
      {`${time ? format(new Date(time), 'MMM dd, yyyy') : null}`}
      <span>{time ? format(new Date(time), 'iiii') : null}</span>
    </Time>
  );
};

const TimeButton = styled(TextButton)`
  text-align: left;
  border: 0;
  box-shadow: none;
  background: transparent;
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: 14px;
  gap: 6px;
  line-height: 20px;
  color: #3B3C3B;

  &:hover, &:active, &:focus {
    box-shadow: none;
    border: 0;
    background: transparent;
  }

  input {
    height: 40px;
  }
`;

const EstimatedTime = ({ children, ...props }: { children: React.ReactNode }) => (
  <TimeButton {...props}>
    {children}
    {' '}
    <PencilIcon />
  </TimeButton>
);

const TextInput = styled('input')`
  border: 1px solid #E5E5E5;
  border-radius: 5px;
  font-size: 14px;
  font-weight: 400;
  color: #3B3C3B;
  line-height: 20px;
  cursor: pointer;
  width: 100%;
  resize: none;
  overflow: hidden;
  padding: 10px;
  height: 40px;
  outline: none;
`;

const TextArea = TextInput.withComponent('textarea');

const CustomInput = forwardRef<HTMLTextAreaElement, { children: React.ReactNode }>(
  (props: { children: React.ReactNode }, ref) => (
    <Fragment>
      {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
      <label htmlFor="note-id">Note</label>
      <TextArea id="note-id" autoFocus ref={ref} {...props} />
    </Fragment>
  ),
);

const CurrentTime = styled('div')`
  align-items: center;
  width: 115px;  
  min-width: 115px;  
`;

const TimeTrackerHistoryHorizontal = styled('div')`
  display: flex;
  flex-direction: column;
  overflow: auto;
  gap: 20px;
  max-height: 63vh;
  min-height: 200px;
`;

const TimeTrackerItem = styled('div')`
  border-bottom: 1px solid #f0f0f0;
  
  & > button {
    padding-top: 20px;
    padding-bottom: 20px;
  }
  
  & > label {
    padding-top: 20px;
    margin-bottom: 5px;

    font-size: 14px;
    color: #9C9C9C;
  }
  
  & > input {
    margin-bottom: 20px;
    margin-top: 8px;
  }
`;

// region styles
const Wrapper = styled('div')`
  border-radius: 10px;
  overflow-y: auto;
  overflow-x: hidden;
  display: flex;
  flex-direction: column;
  height: calc(100% - 136px);  
  
  form {
    padding: 20px 0;
    border-top: 1px solid #DEDEDE;
  }
`;

const TimeSummary = styled('div')`
  display: flex;
  align-items: center;
  gap: 32px;
  padding: 18px 0 24px;
`;

const TimeSummaryDivider = styled('div')`
  height: 100%;
  min-width: 1px;
  background: #D9D9D9;
`;

const TimeSummaryItem = styled('div')`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  gap: 6px;
  line-height: 20px;
  color: #9C9C9C;
  
  span:last-of-type, button {
    font-weight: 500;
    font-size: 20px;
    line-height: 30px;
    color: #3B3C3B;
    display: flex;
    align-items: center;
    gap: 10px;
  }
  
  input {
    margin-right: 0;
  }
`;

const Info = styled('div')`
  display: flex;
  gap: 20px;
  padding-top: 0;
  align-items: center;
  justify-content: space-between;

  & > div {
      display: flex;
      justify-content: flex-start;
  }
`;

const Users = styled('div')`
  width: 128px;
  & > div {
    justify-content: flex-start;
  }
`;

const DeleteButton = styled('button')`
  border: 0;
  padding: 0;
  box-shadow: none;
  background: transparent;
  width: 20px;
  height: 20px;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover, &:active, &:focus {
    box-shadow: none;
    border: 0;
    background: transparent;
  }
`;

const TrackerButton = styled('button')<{ isTracking: boolean; isPause?: boolean }>`
  border: 0;
  box-shadow: none;
  padding: 8px 16px;
  border-radius: 8px;
  background-color: ${({ isTracking }) => (isTracking ? '#F33822' : '#4BC133')};
  font-weight: 500;
  font-size: 14px;
  display: flex;
  align-items: center;
  gap: 5px;
  line-height: 24px;
  color: white;
  
  &:active, &:hover, &:focus {
    border: 0;
    box-shadow: none;
    background-color: ${({ isTracking }) => (isTracking ? '#F33822' : '#4BC133')};
  }
  
  svg {
    fill: white;
    ${({ isPause }) => (isPause ? '' : `path:first-child {
      display: none;
    }`)}
  }
`;

const ButtonWrapper = styled('div')`
  position: relative;
  display: flex;
  align-items: center;
  margin-left: auto;
`;

export const TimeTrackerManageForm = ({
  ExtraHeader,
  task,
  users,
  isPause,
  isLoading,
  removeTimeTracker,
  onTrackTime,
  renderButtonIcon,
  horizontalFormSubmitted,
  onEstimatedTimeChange,
  onChangeTime,
  onChangeDate,
  onChangeMessage,
  activeTimeTracker,
  timeTrackers,
  showSpentTime = true,
}: TimeTrackerManageFormProps) => {
  const currentUser = useUnit($currentUser);
  const fetchTimeTrackers = useUnit($$timeTrackersModel.fetched);
  useEffect(() => {
    if (task?.createdAt && task?.id !== 'new') {
      const createdDate = task?.createdAt ? new Date(task?.createdAt) : new Date();
      const monthLate = sub(createdDate, { months: 3 });
      fetchTimeTrackers({
        taskId: task?.id,
        dateFilter: {
          startDate: startOfDay(monthLate).toISOString(),
          endDate: endOfDay(addDays(new Date(), 2)).toISOString(),
        },
        limit: 1000,
      });
    }
  }, [task?.id]);

  return (
    <Wrapper id={`TimeTrackerManageForm-${task?.id}`}>
      {ExtraHeader ?? null}
      <TimeSummary>
        {showSpentTime && (
          <Fragment>
            <TimeSummaryItem>
              <span>Total time spent</span>
              <span>
                <ClockIcon />
                {secondToHumanFormat(task?.currentTime || 0, true)}
              </span>
            </TimeSummaryItem>
            <TimeSummaryDivider />
          </Fragment>
        )}
        <TimeSummaryItem>
          <span>Estimated time</span>
          <span>
            <ClockIcon />
            <EditableText
              onChange={onEstimatedTimeChange}
              StaticComponent={EstimatedTime}
              EditComponent={TimeInput}
              initialText={secondToHumanFormat(task?.estimatedTime || 0, true)}
              autoHeight={false}
              mask="99\h 59\m"
              className="estimated-time-editor"
            />
          </span>
        </TimeSummaryItem>
        {renderButtonIcon && (
          <ButtonWrapper>
            <TrackerButton
              type="button"
              onClick={onTrackTime}
              isTracking={!isPause && activeTimeTracker?.taskId === task?.id}
              isPause={isPause}
            >
              {renderButtonIcon()}
              <span>
                {/* eslint-disable-next-line no-nested-ternary */}
                {isPause ? 'Paused' : (
                  activeTimeTracker?.taskId === task?.id ? 'Stop tracking' : 'Start tracking'
                )}
              </span>
            </TrackerButton>
          </ButtonWrapper>
        )}
      </TimeSummary>
      <TimeTrackerHorizontalForm
        handleSubmit={horizontalFormSubmitted}
        user={users[currentUser?._id]}
        isDisabled={isLoading}
      />
      <TimeTrackerHistoryHorizontal>
        {isLoading && <BeatLoader color="#F7942A" />}
        {Object.values({ ...timeTrackers }).sort((a, b) => {
          const timeA = Number(a.times[0]?.start || a.createdAt);
          const timeB = Number(b.times[0]?.start || b.createdAt);
          if (timeA > timeB) return -1;
          return 1;
        }).map((tracker) => {
          const time = Number(tracker.times[0]?.start || tracker.createdAt);
          const acc = getTimeTrackingValue(tracker.accumulative);
          return (
            <TimeTrackerItem key={tracker.id}>
              <Info>
                <Users>
                  <UserChip
                    hoverOff
                    label={users[tracker.trackedBy.id]?.shortName || 'Unknown'}
                    avatarEntity={{
                      profile: {
                        firstName: users[tracker.trackedBy.id]?.firstName || '',
                        lastName: users[tracker.trackedBy.id]?.lastName || 'Unknown',
                        avatar: {
                          secureUrl: users[tracker.trackedBy.id]?.avatarUrl,
                        },
                      },
                    }}
                  />
                </Users>
                <EditableText
                  onChange={(value) => onChangeTime({ trackerId: tracker.id, time: value })}
                  StaticComponent={CurrentTime}
                  EditComponent={TimeInput}
                  initialText={acc}
                  autoHeight={false}
                />
                <EditableText
                  onChange={(value) => onChangeMessage({ trackerId: tracker.id, message: value })}
                  initialText={time.toString()}
                  StaticComponent={TimeComponent}
                  selected={new Date(time)}
                  setFieldValue={(_: string, date: Date) => onChangeDate({ trackerId: tracker.id, date })}
                  width="115px"
                  portalId={`TimeTrackerManageForm-${task?.id}`}
                  type="bordered"
                  EditComponent={FormikDatePicker}
                  dateFormat="MMM dd, yyyy"
                  maxDate={new Date()}
                  minDate={sub(new Date(), { months: 1 })}
                  marginWrapper="0 0 5px"
                />
                <DeleteButton onClick={() => removeTimeTracker(tracker.id)} type="button">
                  <TrashIcon2 />
                </DeleteButton>
              </Info>
              <EditableText
                key={`${tracker.id}-text`}
                onChange={(value) => onChangeMessage({ trackerId: tracker.id, message: value })}
                initialText={tracker?.message?.text}
                StaticComponent={Text}
                EditComponent={CustomInput}
                autoHeight
              />
            </TimeTrackerItem>
          );
        })}
      </TimeTrackerHistoryHorizontal>
    </Wrapper>
  );
};
