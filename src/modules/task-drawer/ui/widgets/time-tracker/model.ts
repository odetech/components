import { format } from 'date-fns';
import { $currentUser } from '~/config';
import {
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { $$commentsModel } from '~/entities/comments';
import { createTimeTrackerFx } from '~/entities/time-trackers';
import { getCurrentTrackedTime } from '../../../lib';

export type TInitialValues = {
  accumulative: string,
  message: string,
  createdAt: Date,
};

const createModel = () => {
  const $timeTrackers = createStore<any>({});
  const $estimatedTime = createStore<number>(0);

  const onEstimatedTimeChange = createEvent<string>();
  const messageChanged = createEvent<{ trackerId: string, message: string }>();
  const timeChanged = createEvent<{ trackerId: string, time: string }>();
  const dateChanged = createEvent<{ trackerId: string, date: Date }>();
  const trackerRemoved = createEvent<string>();
  const trackerCreated = createEvent<any>();

  $estimatedTime.on(onEstimatedTimeChange, (_, value) => getCurrentTrackedTime(value));

  sample({
    clock: messageChanged,
    source: $timeTrackers,
    fn: (timeTrackers, { trackerId, message }) => {
      const newTimeTrackers: any = { ...timeTrackers };
      return {
        ...newTimeTrackers,
        [trackerId]: {
          ...newTimeTrackers[trackerId],
          message: {
            text: message,
          },
        },
      };
    },
    target: $timeTrackers,
  });

  sample({
    clock: timeChanged,
    source: $timeTrackers,
    fn: (timeTrackers, { trackerId, time }) => {
      const newTimeTrackers: any = { ...timeTrackers };
      return {
        ...newTimeTrackers,
        [trackerId]: {
          ...newTimeTrackers[trackerId],
          accumulative: getCurrentTrackedTime(time),
        },
      };
    },
    target: $timeTrackers,
  });

  sample({
    clock: dateChanged,
    source: $timeTrackers,
    fn: (timeTrackers, { trackerId, date }) => {
      const newTimeTrackers: any = { ...timeTrackers };
      const tracker = newTimeTrackers[trackerId];
      const time = format(new Date(), 'HH:mm:ss.SSS');
      // eslint-disable-next-line max-len
      const beginningThatDay = new Date(`${date.getFullYear()}-${(`0${date.getMonth() + 1}`).slice(-2)}-${(`0${date.getDate()}`).slice(-2)}T${time}`);
      const timeThatDay = beginningThatDay.getTime() + beginningThatDay.getTimezoneOffset() * 60000;
      const times: any = [{
        start: timeThatDay,
        end: timeThatDay + (tracker.accumulative * 1000),
      }];
      return {
        ...newTimeTrackers,
        [trackerId]: {
          ...newTimeTrackers[trackerId],
          times,
        },
      };
    },
    target: $timeTrackers,
  });

  sample({
    clock: trackerCreated,
    source: { timeTrackers: $timeTrackers, currentUser: $currentUser },
    fn: ({ timeTrackers, currentUser }, values) => {
      const secOfCutomValue = getCurrentTrackedTime(values.accumulative);
      const thatDay = new Date(values.createdAt);
      const time = format(new Date(), 'HH:mm:ss.SSS');
      // eslint-disable-next-line max-len
      const beginningThatDay = new Date(`${thatDay.getFullYear()}-${(`0${thatDay.getMonth() + 1}`).slice(-2)}-${(`0${thatDay.getDate()}`).slice(-2)}T${time}`);
      const timeThatDay = beginningThatDay.getTime() + beginningThatDay.getTimezoneOffset() * 60000;
      const times = [{
        start: timeThatDay,
        end: timeThatDay + (secOfCutomValue * 1000),
      }];
      const id = Math.random().toString();

      const newTimeTrackers:any = { ...timeTrackers };
      newTimeTrackers[id] = {
        times,
        id,
        createdBy: currentUser._id,
        trackedBy: {
          id: currentUser._id,
        },
        accumulative: (times[0].end - times[0].start) / 1000,
        message: {
          text: values.message,
        },
      };
      return newTimeTrackers;
    },
    target: $timeTrackers,
  });

  sample({
    clock: trackerRemoved,
    source: $timeTrackers,
    fn: (timeTrackers, trackerId) => {
      const newTimeTrackers: any = { ...timeTrackers };
      delete newTimeTrackers[trackerId];
      return newTimeTrackers;
    },
    target: $timeTrackers,
  });

  const taskCreated = createEvent<string>();

  const createMessageFx = createEffect(
    async ({ tracker, message, userId }: {tracker: any, message: string, userId: string }) => {
      const payload = {
        text: message,
        rawText: message,
        createdBy: userId,
        updatedBy: userId,
        associatedId: tracker?.times[0].id,
        tags: [tracker.id],
        projectId: tracker.projectId,
      };
      $$commentsModel.created({ payload });
    },
  );

  const createTimeTrackersFx = createEffect(
    async ({ taskId, timeTrackers, userId }: { taskId: string, timeTrackers: any, userId: string }) => {
      await Promise.all(Object.values(timeTrackers).map(async (timeTracker: any) => {
        const times = timeTracker.times.map((time: any) => ({ ...time, endedAt: time.end, startedAt: time.start }));
        const newTimeTracker = await createTimeTrackerFx({ taskId, times });
        if (timeTracker.message.text) {
          await createMessageFx({ tracker: newTimeTracker, message: timeTracker.message.text, userId });
        }
      }));
    },
  );

  sample({
    clock: taskCreated,
    source: { timeTrackers: $timeTrackers, currentUser: $currentUser },
    fn: ({ timeTrackers, currentUser }, taskId) => ({ taskId, timeTrackers, userId: currentUser._id }),
    target: createTimeTrackersFx,
  });

  return {
    $timeTrackers,
    messageChanged,
    timeChanged,
    $estimatedTime,
    dateChanged,
    onEstimatedTimeChange,
    trackerRemoved,
    trackerCreated,
    taskCreated,
  };
};

export const $$trackerCreateTaskModel = createModel();
