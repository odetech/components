import React from 'react';
import styled from '@emotion/styled';
import { TimeInput } from './time-input';

type FormikDatePickerProps = {
  label?: string;
  value: string;
  error?: string;
  name: string;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void;
  onChange?: (value: string) => void;
  margin?: string;
  width?: string;
  minWidth?: string;
  disabled?: boolean;
  type?: string;
  padding?: string;
};

const StyledInput = styled('input')<{ error?: string; padding?: string; theme?: string }>`
  width: 100%;
  background-color: transparent;
  
  line-height: 1.5;
  font-size: 14px;
  font-weight: 500;
  color: #3B3C3B;
  
  transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;

  color: #3B3C3B;

  ${({ theme, padding }) => {
    switch (theme) {
      case 'bordered':
        return `
          padding: ${padding || '0 0 0px 30px'};
          width: 100%;
          cursor: pointer;
          
          font-weight: 500;
          font-size: 14px;
          
          border-radius: 6px;
          border: 1px solid #E5E5E5;
          height: 40px;
        `;
      default:
        return `
          padding: ${padding || '0 0 4px 40px'};
          width: 100%;
          cursor: pointer;
          
          font-weight: 500;
          font-size: 14px;
          
          border: none;
          border-bottom: 1px solid #E5E5E5;
        `;
    }
  }}
  
  ${({ error }) => error && 'border-color: red;'};
  
  &:hover:not(:disabled):not(:read-only) {
    border-color: rgba(247, 148, 42, 0.5);
  }
  
  &:focus:not(:read-only) {
    background-color: rgba(247, 148, 42, 0.1);
    border-color: rgba(247, 148, 42, 0.5);
  }
  
  &:disabled {
    opacity: 0.5;
    
    background-color: #f7f7f7;
    
  }
  
  &:disabled,
  &:read-only {
    cursor: not-allowed;
  }
`;

const Input = (inputProps: Record<string, unknown>) => (
  <StyledInput
    {...inputProps}
    type="text"
  />
);

// region styles
const Wrapper = styled('div')<{ margin?: string, width?: string; minWidth?: string; disabled?: boolean }>`
  display: inline-flex;
  flex-direction: column;
  width: ${({ width }) => width ?? '100%'};
  ${({ margin }) => margin && `margin: ${margin};`}
  ${({ minWidth }) => minWidth && `min-width: ${minWidth};`}
  opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
  
  &:hover input:not(:disabled) {
    transition: all 100ms;
    border-color: #F7942A;
  }
`;

const Label = styled('span')`
  display: block;
  margin-bottom: 5px;
  
  font-size: 14px;
  color: #9C9C9C;
`;

export const FormikTimePicker: React.FC<FormikDatePickerProps> = ({
  label,
  value,
  name,
  minWidth,
  setFieldValue,
  onChange,
  margin,
  width,
  disabled,
  type,
  padding,
  error,
}) => {
  const onCustomChange = (val: string) => {
    onChange?.(val);
    setFieldValue(name, val);
  };
  return (
    <Wrapper margin={margin} width={width} disabled={disabled} minWidth={minWidth}>
      {label && <Label>{label}</Label>}
      <TimeInput
        value={value}
        onChange={onCustomChange}
        disabled={disabled}
        theme={type}
        renderInput={Input}
        padding={padding}
        name={name}
        error={error}
      />
    </Wrapper>
  );
};
