import React, { memo } from 'react';
import { Formik, Form, FormikHelpers } from 'formik';
import { sub } from 'date-fns';
import { FormikDatePicker } from '~/components/formik';
import styled from '@emotion/styled';
import { TAssignedToUser } from '~/entities/tasks/types';
import { PlusIcon } from '~/components/icons';
import { UserChip } from '../../fields/user-chip';
import { horizontalSchema } from './validation';
import { TInitialValues } from './model';
import { InputWithLabel } from './input-with-label';
import { FormikTimePicker } from './time-picker';

export type TimeTrackerHorizontalFormProps = {
  user: TAssignedToUser;
  isDisabled?: boolean;
  handleSubmit: (values: TInitialValues) => void,
};

const AddButton = styled('button')`
  background: transparent;
  box-shadow: none;
  border: 0;
  
  &:hover, &:active, &:focus {
    background: transparent;
    box-shadow: none;
    border: 0;
  }
`;

const Label = styled('span')`
  display: block;
  margin-bottom: 5px;
  
  font-size: 14px;
  color: #9C9C9C;
`;

const StyledBottomForm = styled('div')`
  display: flex;
  gap: 20px;
  align-items: center;
  
  button {
    padding-top: 10px;
    height: 18px;
    width: 18px;
  }
  
  & > label {
    width: 200px;
  }  
`;

const Wrapper = styled('div')<{ margin?: string, width?: string; disabled?: boolean }>`
  display: inline-flex;
  flex-direction: column;
  height: 60px;
  width: ${({ width }) => width ?? '100%'};
  ${({ margin }) => margin && `margin: ${margin}`};
  opacity: ${({ disabled }) => (disabled ? '0.5' : '1')};
  
  &:hover input:not(:disabled) {
    transition: all 100ms;
    border-color: #F7942A;
  }

  & > div:first-of-type {
    justify-content: flex-start;
    height: 40px;  
  }
`;

const TimeForm = ({
  user,
  handleSubmit,
  isDisabled,
} : TimeTrackerHorizontalFormProps) => (
  <Formik
    initialValues={{
      accumulative: '0h 0m',
      message: '',
      createdAt: new Date(),
    }}
    enableReinitialize
    validationSchema={horizontalSchema}
    onSubmit={(values: TInitialValues, { resetForm } : FormikHelpers<TInitialValues>) => {
      handleSubmit(values);
      resetForm();
    }}
  >
    {({
      values,
      setFieldValue,
      errors,
      handleChange,
    }) => (
      <Form>
        <StyledBottomForm>
          <Wrapper width="128px">
            <Label>User</Label>
            <UserChip
              hoverOff
              label={user?.shortName || 'Unknown'}
              avatarEntity={{
                profile: {
                  firstName: user?.firstName || '',
                  lastName: user?.lastName || 'Unknown',
                  avatar: {
                    secureUrl: user?.avatarUrl,
                  },
                },
              }}
            />
          </Wrapper>
          <FormikTimePicker
            value={values.accumulative}
            name="accumulative"
            label="Time spent"
            margin="0 0 0 0"
            setFieldValue={setFieldValue}
            type="bordered"
            padding="12px"
            width="200px"
            error={errors?.accumulative}
          />
          <FormikDatePicker
            name="createdAt"
            dateFormat="MMM dd, yyyy"
            label="Date"
            maxDate={new Date()}
            minDate={sub(new Date(), { months: 1 })}
            margin="0"
          />
          <AddButton
            type="submit"
            role="button"
            disabled={isDisabled}
          >
            <PlusIcon fill="#F7942A" />
          </AddButton>
        </StyledBottomForm>
        <InputWithLabel
          label="Note"
          value={values.message}
          inputProps={{
            name: 'message',
            placeholder: 'Enter a note',
            theme: 'bordered',
            padding: '12px',
            id: 'creating-message-id',
          }}
          paddingBottom={0}
          margin="0"
          touched
          onChange={handleChange}
          errorBottom={-19}
        />
      </Form>
    )}
  </Formik>
);

export const TimeTrackerHorizontalForm = memo(
  TimeForm,
  (prevProps, newProps) => (
    prevProps.isDisabled === newProps.isDisabled
    && prevProps.user?.id === newProps.user?.id
    && prevProps.handleSubmit === newProps.handleSubmit
  ),
);
