import {
  $$timeTrackersModel, createTimeTrackerFx, editTimeTrackerFx, TTimeTracker,
} from '~/entities/time-trackers';
import {
  combine, createEvent, createEffect, sample, createStore,
} from 'effector';
import { $$form } from '~/modules/task-drawer/model';
import { Form } from '~/modules/task-drawer/model/form';
import { getCurrentTrackedTime } from '~/modules/task-drawer/lib';
import { $currentUser } from '~/config';
import { createCommentFx } from '~/entities/comments/api';
import { format } from 'date-fns';
import { taskEdited } from '~/modules/task-drawer/model/task';

const createModel = () => {
  const $isPause = combine(
    $$timeTrackersModel.$activeTimeTracker,
    $$timeTrackersModel.$isPause,
    $$form.$form,
    (timeTracker, isPause, form) => isPause && form._id === timeTracker?.taskId,
  );
  const $showModal = createStore(false);
  const modalChanged = createEvent<boolean>();
  $showModal.on(modalChanged, (_, status) => status);
  const trackedToggled = createEvent();
  const estimatedTimeEdited = createEvent();
  const onEstimatedTimeChangeFx = createEffect(async ({ form, value }: {form: Form, value: string }) => {
    const estimatedTime = getCurrentTrackedTime(value);
    taskEdited({ taskId: form._id, payload: { projectId: form.projectId?.value, estimatedTime } });
  });
  sample({ clock: estimatedTimeEdited, onEstimatedTimeChangeFx });
  const onTrackTimeFx = createEffect(async ({
    isPause,
    activeTimeTracker,
    task,
  }: {
    isPause: boolean;
    activeTimeTracker: TTimeTracker | null;
    task: $$form.Form;
  }) => {
    if (isPause) {
      $$timeTrackersModel.finished({
        timeTrackerId: activeTimeTracker?.id,
        currentTrackedId: activeTimeTracker?.id,
      });
    } else if (activeTimeTracker?.taskId !== task?._id) {
      $$timeTrackersModel.createdAndRan(task?._id || '');
    } else {
      modalChanged(true);
    }
  });
  sample({
    clock: trackedToggled,
    source: {
      task: $$form.$form,
      activeTimeTracker: $$timeTrackersModel.$activeTimeTracker,
      isPause: $isPause,
    },
    target: onTrackTimeFx,
  });
  const messageChanged = createEvent<{ trackerId: string, message: string }>();
  const changeMessageFx = createEffect(async ({
    tracker, message, currentUserId,
  }: {
    tracker: TTimeTracker, message: string, currentUserId: string | null,
  }) => {
    let createdMessageId: any = tracker.message?.messageId || null;
    if (!createdMessageId && message) {
      const payload = {
        text: message,
        rawText: message,
        createdBy: currentUserId,
        updatedBy: currentUserId,
        associatedId: tracker?.times[0]?.id,
        tags: [tracker.id],
        projectId: tracker.projectId,
      };
      const newComment = await createCommentFx({ payload });
      createdMessageId = newComment?.id;
    }
    const payloadMessage = {
      text: message,
      rawText: message,
      createdBy: currentUserId,
      updatedBy: currentUserId,
      associatedId: tracker?.times[0]?.id,
      tags: [tracker.id],
      _id: createdMessageId,
    };
    await editTimeTrackerFx({
      trackerId: tracker.id,
      payload: {
        timeTracker: {
          taskId: tracker.taskId,
          createdAt: tracker.createdAt,
          data: ['orgId', 'projectId', 'taskId'],
          updatedBy: currentUserId,
          add_time: tracker.accumulative,
        },
        message: createdMessageId && payloadMessage,
      },
    });
  });
  sample({
    clock: messageChanged,
    source: { trackers: $$timeTrackersModel.$timeTrackers, user: $currentUser },
    fn: ({ user, trackers }, { trackerId, message }) => ({
      currentUserId: user._id,
      tracker: trackers[trackerId],
      message,
    }),
    target: changeMessageFx,
  });
  const timeChanged = createEvent<{ trackerId: string, time: string }>();
  sample({
    clock: timeChanged,
    source: { trackers: $$timeTrackersModel.$timeTrackers, user: $currentUser },
    fn: ({ trackers, user }, { trackerId, time }) => ({
      trackerId: trackers[trackerId].id,
      payload: {
        timeTracker: {
          taskId: trackers[trackerId].taskId,
          createdAt: trackers[trackerId].createdAt,
          data: ['orgId', 'projectId', 'taskId'],
          updatedBy: user._id,
          add_time: getCurrentTrackedTime(time),
        },
      },
    }),
    target: editTimeTrackerFx,
  });
  const dateChanged = createEvent<{ trackerId: string, date: Date }>();
  sample({
    clock: dateChanged,
    source: { trackers: $$timeTrackersModel.$timeTrackers, user: $currentUser },
    fn: ({ user, trackers }, { trackerId, date }) => ({
      trackerId: trackers[trackerId].id,
      payload: {
        timeTracker: {
          taskId: trackers[trackerId].taskId,
          createdAt: date,
          data: ['orgId', 'projectId', 'taskId'],
          updatedBy: user._id,
          add_time: trackers[trackerId].accumulative,
        },
      },
    }),
    target: editTimeTrackerFx,
  });

  // eslint-disable-next-line max-len
  const createTrackerFx = createEffect(async ({ taskId, values, currentUserId }:{ taskId: string, values: any, currentUserId: string }) => {
    const secOfCutomValue = getCurrentTrackedTime(values.accumulative);
    const thatDay = new Date(values.createdAt);
    const time = format(new Date(), 'HH:mm:ss.SSS');
    // eslint-disable-next-line max-len
    const beginningThatDay = new Date(`${thatDay.getFullYear()}-${(`0${thatDay.getMonth() + 1}`).slice(-2)}-${(`0${thatDay.getDate()}`).slice(-2)}T${time}`);
    const times = [{
      startedAt: beginningThatDay.getTime(),
      endedAt: beginningThatDay.getTime() + (secOfCutomValue * 1000),
    }];
    const tracker = await createTimeTrackerFx({ taskId, times });
    if (values.message) {
      const payload = {
        text: values.message,
        rawText: values.message,
        createdBy: currentUserId,
        updatedBy: currentUserId,
        associatedId: tracker?.times[0].id,
        tags: [tracker?.id],
        projectId: tracker?.projectId,
      };
      const newMessage = await createCommentFx({ payload });
      $$timeTrackersModel.updateMessage({
        id: tracker.id,
        message: {
          messageId: newMessage.id,
          text: newMessage.value,
        },
      });
    }
  });

  const trackerCreated = createEvent<any>();

  sample({
    clock: trackerCreated,
    source: { form: $$form.$form, currentUser: $currentUser },
    fn: ({ form, currentUser }, values) => ({
      values,
      taskId: form._id,
      currentUserId: currentUser._id,
    }),
    target: createTrackerFx,
  });
  return ({
    $isPause,
    $showModal,
    trackedToggled,
    estimatedTimeEdited,
    dateChanged,
    timeChanged,
    messageChanged,
    trackerCreated,
    modalChanged,
    createTrackerFx,
  });
};

export const $$trackerEditTaskModel = createModel();
