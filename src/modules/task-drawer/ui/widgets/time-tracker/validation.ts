import * as Yup from 'yup';
import { differenceInDays } from 'date-fns';
import { getCurrentTrackedTime } from '../../../lib';

export const horizontalSchema = Yup.object().shape({
  accumulative: Yup.string()
    .test(
      'Please enter time greater that 0',
      'Please enter time greater that 0',
      (value) => getCurrentTrackedTime(value!) !== 0,
    ),
  message: Yup.string(),
  createdAt: Yup.date().default(new Date())
    .test(
      'The difference between the current date and the selected date should not be more than 1 month',
      'The difference between the current date and the selected date should not be more than 1 month',
      (value) => {
        const diffDays = differenceInDays(new Date(), value);
        return diffDays >= 0 && diffDays <= 30;
      },
    ),
});
