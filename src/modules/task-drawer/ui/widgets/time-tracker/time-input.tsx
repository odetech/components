import React from 'react';
import InputMask from 'react-input-mask';
import styled from '@emotion/styled';

const StyledInput = styled('input')`
  width: 120px;
  padding: 5px 10px;
  color: #3B3C3B;
  border: 1px solid #E5E5E5;
  border-radius: 4px;
  font-weight: 600;
  margin-right: 20px;
  
  &:disabled {
    opacity: 0.3;
    cursor: not-allowed;
  }
`;

type TimeInputProps = {
  value: string;
  name: string;
  theme?: string;
  error?: string;
  mask?: string;
  padding?: string;
  onChange: (value: string) => void;
  disabled?: boolean;
  renderInput?: (inputProps: Record<string, unknown>) => React.ReactNode,
};

const defaultInput = (inputProps: Record<string, unknown>) => (
  <StyledInput
    {...inputProps}
    type="text"
  />
);

export const TimeInput = ({
  value, onChange, disabled, renderInput, theme, padding, name, mask, ...props
}: TimeInputProps) => (
  <InputMask
    {...props}
    /* eslint-disable no-useless-escape */
    mask={mask || 'ab\h 59\m'}
    // @ts-ignore
    formatChars={{
      9: '[0-9]',
      5: '[0-5]',
      a: '[0-2]',
      b: '[0-3]',
    }}
    placeholder="0h 0m"
    maskChar="0"
    value={value}
    name={name}
    onChange={(e) => onChange(e.currentTarget.value.replace(/_/gi, '0'))}
    disabled={disabled}
    theme={theme}
    padding={padding}
  >
    {renderInput ?? defaultInput}
  </InputMask>
);
