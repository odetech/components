import { combine } from 'effector';
import { either } from 'patronum';
import { $$form, $$task } from '~/modules/task-drawer/model';
import { $$trackerCreateTaskModel } from './model';
import { $$trackerEditTaskModel } from './edit-model';

const $estimatedTime = either(
  combine($$form.$form, (form) => form._id === '_id'),
  $$trackerCreateTaskModel.$estimatedTime,
  $$task.$estimatedTime,
);

export {
  $estimatedTime,
};
