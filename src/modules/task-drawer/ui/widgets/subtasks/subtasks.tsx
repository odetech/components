import React, {
  ChangeEvent,
  forwardRef, useEffect, useRef, useState,
} from 'react';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import styled from '@emotion/styled';
import { ellipsis, rgba } from 'polished';
import {
  CheckmarkIcon, CrossIcon, DotsIcon, TrashIcon2,
} from '~/components/icons';
import { EditableText } from '~/components/editable-text';
import ReactDOM from 'react-dom';
import { Dropdown, DropdownMenuItem } from '~/modules/notification-widget/ui/styles';
import { StyledDropdownMenu } from '~/styles';
import { CustomDropdownToggle } from '~/components/default-components/Dropdown/DropdownStyles';

type InitialValuesProps = {
  subTasks: Record<string, any>;
  subTaskTitle: string;
};

// region styles
const SubTasksWrapper = styled('div')`
  border-radius: 10px;
  height: calc(100% - 136px);
  overflow: auto;

  div[data-dragging="true"] {
    right: 32px;

    &:before,
    &:after {
      background-color: #9c9c9c;
    }
  }
`;

const StyledForm = styled(Form)``;

const SmallButton = styled('button')`
  box-shadow: none;
  background: transparent;
  border: 0;
  text-align: left;
  width: 100%;

  &:hover, &:active, &:focus {
      box-shadow: none;
      background: transparent;
      border: 0;
  }
`;

const ButtonsWrapper = styled('div')`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-top: 8px;  
`;

const TextButtonAsP = styled('p')<{ showMore?: boolean; moreButton?: boolean }>`
  text-align: left;
  border: 0;
  box-shadow: none;
  background: transparent;
  color: #3b3c3b;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

  font-size: 14px;
  line-height: 20px;
  width: 100%;
  max-width: 100%;

  &:hover,
  &:active,
  &:focus {
    box-shadow: none;
    border: 0;
    background: transparent;
  }

  padding-top: 8px;
  ${({ showMore }) => showMore && ellipsis(undefined, 3)}
  white-space: pre-line;
  ${({ moreButton }) => (moreButton ? 'margin-bottom: 2px;' : '')}
`;

const CollapseButton = styled('button')`
  color: #9c9c9c;
  font-weight: 500;
  padding: 2px 8px !important;
  font-size: 14px;
  line-height: 20px;
  background: none;
  margin-bottom: 20px;
  border: 0;
  box-shadow: none;

  &:hover,
  &:active,
  &:focus {
    background: none;
    border: 0;
    box-shadow: none;
  }
`;

const TextWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: flex-start;  
`;

const Text = ({ children, ...props }: { children: React.ReactNode }) => {
  const [short, setShort] = useState<boolean>(false);
  const previousRef = useRef<string>('');
  const [moreButton, setMoreButton] = useState<boolean>(false);
  const descTextEl = useRef(null);

  useEffect(() => {
    const currentEl = descTextEl.current;
    let checkHeight = false;
    if (previousRef.current === '' && children !== '') {
      previousRef.current = children as string;
      checkHeight = true;
    }
    if (checkHeight) {
      // @ts-ignore
      const elementStyle = document.defaultView.getComputedStyle(
        // @ts-ignore
        // eslint-disable-next-line react/no-find-dom-node
        ReactDOM.findDOMNode(currentEl),
        null,
      ); // getBoundingClientRect
      let divHeight: number | string = elementStyle.getPropertyValue('height');
      divHeight = parseInt(divHeight.replace('px', ''), 10);
      const lines = divHeight / 20;
      setTimeout(() => {
        setMoreButton(lines > 3);
        setShort(lines > 3);
      }, 200);
    }
  }, [children]);

  return (
    <TextWrapper>
      <TextButtonAsP
        {...props}
        ref={descTextEl}
        showMore={short}
        moreButton={moreButton}
      >
        {children}
      </TextButtonAsP>
      {moreButton ? (
        <CollapseButton
          type="button"
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            setShort((prevState) => !prevState);
          }}
        >
          {`${short ? '+ Show more' : '- Show less'}`}
        </CollapseButton>
      ) : null}
    </TextWrapper>
  );
};

const Label = styled('label')<{ isActive: boolean; disabled: boolean }>`
  display: flex;
  align-items: center;
  font-weight: 500;
  cursor: pointer;
  transition: opacity 0.3s, background-color 0.5s;
  padding: 8px 0;
  border-radius: 8px;
  margin: 0;

  &:hover {
    background-color: ${rgba('#F0F0F0', 0.4)};
  }

  ${({ isActive }) => isActive
    && `
    text-decoration: line-through;
    color: #9C9C9C;
  `};

  ${({ disabled }) => disabled
    && `
    opacity: 0.3;
    cursor: not-allowed;
  `};

  & > input:first-of-type {
    display: none;
  }
`;

const FakeCheckbox = styled('div')<{ isActive: boolean }>`
  width: 22px;
  height: 22px;
  border: 1px solid #a8a8a8;
  border-radius: 5px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
  transition: all 0.3s;
  flex-shrink: 0;

  & svg {
    display: none;
  }

  ${({ isActive }) => isActive
    && `
    background-color: rgba(247,148,42,.1);
    border: 1px solid #f7942a;
    
    & svg {
      display: block;
    }
  `};
`;

const Controls = styled('div')`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 16px 0;

  label {
    margin: 0 8px 0 0;
  }

  input {
    height: 40px;
  }
`;

const SubTasksHeader = styled('div')`
  background-color: #fff;
  border-bottom: 1px solid #dedede;
  border-radius: 10px;
  padding: 10px 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 15px;
  color: #9c9c9c;
`;

const Progress = styled('div')`
  position: relative;
  padding-bottom: 15px;
  display: flex;
  justify-content: space-between;
  width: 80%;

  &::before {
    content: "";
    width: 100%;
    height: 10px;
    background-color: #f0f0f0;
    border-radius: 13px;
    position: absolute;
    bottom: 0;
    left: 0;
  }
`;

export const DropdownButton = styled(CustomDropdownToggle)`
  height: 24px;
  padding: 0;
  border-radius: 0;
  background: transparent;

  &:hover,
  &:active,
  &:focus {
    background: transparent !important;
  }

  svg {
    fill: #3b3c3b;
    transform: rotate(0deg);
  }
`;

const TextArea = styled('textarea')`
  font-size: 14px;
  font-weight: 400;
  color: #3b3c3b;
  line-height: 20px;
  margin-bottom: 20px;
  cursor: pointer;
  width: 100%;
  resize: none;
  overflow: hidden;
  height: 40px;
  outline: none;
  display: flex;
  padding: 10px 12px;
  align-items: flex-start;
  gap: 8px;
  align-self: stretch;
  border-radius: 10px;
  border: 1px solid #f7942a;
  background: #fff;
`;

export const AddButton = styled('button')`
  height: 40px;
  font-weight: 500;
  font-size: 15px;
  padding: 0 15px;
  line-height: 22px;
  border: 1px solid ${'#F7942A'};
  background-color: #fef4e9;
  color: ${'#F7942A'};
  transition: opacity 0.3s;
  border-radius: 8px;
  display: flex;
  align-items: center;

  &:hover {
    opacity: 0.9;
  }
`;

const CustomInput = forwardRef<
  HTMLTextAreaElement,
  { children: React.ReactNode }
>((props: { children: React.ReactNode }, ref) => (
  <TextArea id="note-id" autoFocus ref={ref} {...props} />
));

const ProgressCurrent = styled('div')<{ width: number }>`
  width: ${({ width }) => `${width}%`};
  height: 10px;
  background-color: #03b44a;
  border-radius: 13px;
  position: absolute;
  bottom: 0;
  left: 0;
  transition: width 0.4s;
`;

export const DeleteButton = styled('button')`
  box-shadow: none;
  border: 0;
  margin-left: auto;
  background: transparent;

  &:hover,
  &:active,
  &:focus {
    box-shadow: none;
    border: 0;
    background: transparent;
  }

  svg {
    stroke: #9c9c9c;
  }
`;

const TaskWrapper = styled('div')`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
`;

// validation
const SubTasksSchema = Yup.object().shape({
  subTaskTitle: Yup.string().required('Title is required'),
});

type SubTaskFormProps = {
  initialValues: InitialValuesProps;
  handleSubmit: (values: InitialValuesProps) => void;
  handleDelete: (id: string, activity: number) => void;
  handleCheck: (subTask: any) => void;
  handleEdit: (id: string, title: string) => void;
  isLoading: boolean;
  ExtraHeader?: React.ReactNode;
  // eslint-disable-next-line react/no-unused-prop-types
  taskId: string;
  countProgress: () => number;
};

export const SubTaskForm = ({
  initialValues,
  handleSubmit,
  handleDelete,
  handleCheck,
  handleEdit,
  isLoading,
  ExtraHeader,
  countProgress,
}: SubTaskFormProps) => {
  const [isEdit, setIsEdit] = useState<Record<string, boolean>>({});
  const [isOpen, setIsOpen] = useState<string>('');
  const [currentEditText, setCurrentEditText] = useState<Record<string, string>>({});

  const toggleEdit = (id: string) => (val: boolean) => {
    setIsEdit((prevState) => ({ ...prevState, [id]: val }));
  };

  const changeText = (id: string) => (val: string) => {
    setCurrentEditText((prevState) => ({ ...prevState, [id]: val }));
  };

  const changeHeight = (event: ChangeEvent<HTMLTextAreaElement> | string) => {
    if (typeof event !== 'string') {
      if (event.currentTarget.value) {
        // eslint-disable-next-line no-param-reassign
        event.currentTarget.style.height = `${event.currentTarget.scrollHeight}px`;
      } else {
        // eslint-disable-next-line no-param-reassign
        event.currentTarget.style.height = '38px';
      }
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={SubTasksSchema}
      enableReinitialize
    >
      {({ values, isSubmitting, setFieldValue }) => {
        const { subTasks: st } = values;
        const valuesSubTasks = Object.values(st || {});
        return (
          <SubTasksWrapper>
            {ExtraHeader ?? null}
            {countProgress ? (
              <SubTasksHeader className="subtask-header">
                <Progress>
                  <span>Progress</span>
                  <span>
                    {countProgress()}
                    %
                  </span>
                  <ProgressCurrent width={countProgress()} />
                </Progress>
                <span>
                  {/* eslint-disable-next-line max-len */}
                  {`${
                    valuesSubTasks?.filter(({ activity }) => activity === 1)
                      .length
                  } ${
                    valuesSubTasks?.filter(({ activity }) => activity === 1)
                      .length > 1
                      ? 'subtasks'
                      : 'subtask'
                  } remaining`}
                </span>
              </SubTasksHeader>
            ) : null}
            <StyledForm>
              <Controls>
                <TextArea
                  disabled={isSubmitting}
                  value={values.subTaskTitle}
                  placeholder="Add subtask"
                  onChange={(e) => {
                    setFieldValue('subTaskTitle', e.target.value);
                    changeHeight(e);
                  }}
                  onKeyDown={(e) => {
                    if (e.key === 'Enter' && !e.shiftKey) {
                      handleSubmit(values);
                      e.preventDefault();
                    }
                  }}
                />
              </Controls>
              <div>
                {valuesSubTasks?.map((subTask) => (
                  <TaskWrapper>
                    <Label
                      isActive={subTask.activity === 4}
                      key={subTask.id}
                      disabled={isLoading}
                    >
                      <input
                        checked={subTask.activity === 4}
                        type="checkbox"
                        onChange={() => handleCheck(subTask)}
                      />
                      <FakeCheckbox isActive={subTask.activity === 4}>
                        <CheckmarkIcon fill="#F7942A" />
                      </FakeCheckbox>
                    </Label>
                    <EditableText
                      key={`${subTask.id}-text`}
                      onChange={(value) => handleEdit(subTask.id, value)}
                      initialText={subTask.title}
                      StaticComponent={Text}
                      EditComponent={CustomInput}
                      autoHeight
                      isEdit={isEdit[subTask.id]}
                      onEdit={changeText(subTask.id)}
                      setIsEdit={toggleEdit(subTask.id)}
                      isControllable
                    />
                    {isEdit[subTask.id] ? (
                      <ButtonsWrapper>
                        <SmallButton
                          type="button"
                          onClick={() => {
                            handleEdit(subTask.id, currentEditText[subTask.id] || subTask.title);
                            toggleEdit(subTask.id)(false);
                          }}
                        >
                          <CheckmarkIcon />
                        </SmallButton>
                        <SmallButton
                          type="button"
                          onClick={() => {
                            changeText(subTask.id)(subTask.title);
                            toggleEdit(subTask.id)(false);
                          }}
                        >
                          <CrossIcon />
                        </SmallButton>
                      </ButtonsWrapper>
                    ) : (
                      <Dropdown
                        isOpen={isOpen === subTask.id}
                        toggle={() => {
                          setIsOpen(isOpen === subTask.id ? '' : subTask.id);
                        }}
                      >
                        <DropdownButton
                          isOpen={isOpen === subTask.id}
                        >
                          <DotsIcon />
                        </DropdownButton>
                        <StyledDropdownMenu>
                          <DropdownMenuItem
                            onClick={() => handleDelete(subTask.id, subTask.activity)}
                          >
                            <TrashIcon2 />
                            {' '}
                            <span>Delete</span>
                          </DropdownMenuItem>
                        </StyledDropdownMenu>
                      </Dropdown>
                    )}
                  </TaskWrapper>
                ))}
              </div>
            </StyledForm>
          </SubTasksWrapper>
        );
      }}
    </Formik>
  );
};
