import React from 'react';
import { $$subtasksModel } from '~/entities/subtasks';
import { useUnit } from 'effector-react';
import { TSubTask } from '~/entities/subtasks/types';
import { BeatLoader } from 'react-spinners';
import { css } from '@emotion/react';
import { $currentUser } from '~/config';
import { $activeTask } from '../../../model/task';
import { SubTaskForm } from './subtasks';

type LoadingSmallDotsProps = {
  size?: number;
};

const override = css`
  display: block;
  text-align: center;
`;

export const LoadingSmallDots: React.FC<LoadingSmallDotsProps> = ({ size = 10 }) => (
  // @ts-ignore
  // eslint-disable-next-line jsx-a11y/aria-role
  <BeatLoader color="#F7942A" loading size={size} css={override} role="loader" />
);

type SubTasksProps = {
  taskId: string;
  ExtraHeader?: React.ReactNode;
  initialValues: any;
  handleSubmit: (values: any) => void;
  handleDelete: (id: string, activity: number) => void
  handleEdit: (id: string, value: string) => void
  handleCheck: (subTask: any) => void;
  handleDragCallback?: (order: string[]) => void;
  isLoading: boolean;
  countProgress: () => number;
};

export const SubTasks: React.FC<SubTasksProps> = ({ taskId, ExtraHeader }) => {
  const {
    subTasks,
    isLoading,
    deleted,
    created,
    edited,
  } = useUnit({
    subTasks: $$subtasksModel.$subtasks,
    isLoading: $$subtasksModel.$pending,
    deleted: $$subtasksModel.deleted,
    edited: $$subtasksModel.edited,
    created: $$subtasksModel.created,
  });
  const activeTask = useUnit($activeTask);
  const { _id: userId } = useUnit($currentUser) || {};
  const initialValues: any = {
    subTasks,
    subTaskTitle: '',
  };

  const handleDeleteSubtask = async (subTaskId: string) => {
    await deleted({ taskId: subTaskId });
  };

  const handleSubmit = async (values: typeof initialValues) => {
    const payload = {
      title: values.subTaskTitle,
      activity: 1,
      subTaskId: taskId,
      createdBy: userId,
      updatedBy: userId,
      projectId: activeTask?.projectId
    };
    created({ payload });
  };

  const handleCheck = async (subTask: TSubTask) => {
    const payload = {
      activity: subTask.activity === 4 ? 1 : 4,
      subTaskId: taskId,
      createdBy: userId,
      updatedBy: userId,
    };
    edited({ taskId: subTask.id, payload });
  };

  const handleEdit = (subTaskId: string, title: string) => {
    const payload = {
      title,
      projectId: activeTask?.projectId,
      updatedBy: userId,
    };
    edited({ taskId: subTaskId, payload });
  };

  if (isLoading) {
    return <LoadingSmallDots />;
  }

  const countProgress = () => {
    if (!subTasks || Object.keys(subTasks).length === 0) {
      return 0;
    }

    const allSubTasks = Object.values(subTasks).length;
    const doneSubTasks = Object.values(subTasks).filter((s) => s.activity === 4).length;
    return Math.floor((doneSubTasks / allSubTasks) * 100);
  };

  return (
    <SubTaskForm
      // @ts-ignore
      taskId={activeTask?.id}
      initialValues={initialValues}
      handleSubmit={handleSubmit}
      isLoading={isLoading}
      handleDelete={handleDeleteSubtask}
      handleEdit={handleEdit}
      handleCheck={handleCheck}
      countProgress={countProgress}
      ExtraHeader={ExtraHeader}
    />
  );
};
