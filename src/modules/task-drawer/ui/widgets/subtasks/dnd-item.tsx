import React, { useRef } from 'react';
import {
  useDrag, useDrop, DropTargetMonitor, DragSourceMonitor,
} from 'react-dnd';
import { XYCoord } from 'dnd-core';
import styled from '@emotion/styled';

type DndItemProps = {
  id: any;
  children: React.ReactNode;
  index: number;
  border?: boolean;
  moveCard: (dragIndex: number, hoverIndex: number) => void;
};

type DragItem = {
  index: number;
  id: string;
  type: string;
};

// region styles
const DragContainer = styled('div')<{ isDragging: boolean, border?: boolean }>`
  border: ${({ border }) => (border ? '1px solid #E5E5E5' : '0')};
  border-radius: 6px;
  margin-bottom: .5rem;
  background-color: white;
  position: relative;
  opacity: ${({ isDragging }) => (isDragging ? '0' : '1')};
  
  & > label {
    padding-left: 10px !important;
  }
  
  ${({ isDragging }) => isDragging && `
    position: relative;
    
    &::before {
      position: absolute;
      top: 0;
      bottom: 0;
      content: "";
      display: block;
      border: 1px solid red;
      width: 100%;
    }
  `}
  
  label {
    margin-bottom: 0 !important;
  }
`;

const Wrapper = styled('div')<{ isDragging: boolean }>`
  position: absolute;
  top: 0;
  bottom: 0;
  content: "";
  display: block;
  border: 1px solid red;
  width: 100%;
`;

const DragTarget = styled('div')`
  position: absolute;
  right: 10px;
  top: 0;
  bottom: 0;
  width: 18px;
  display: flex;
  align-items:center;
  flex-direction: column;
  justify-content: center;
  cursor: move;
  
  &::before,
  &::after {
    content: "";
    display: block;
    width: 100%;
    height: 2px;
    background-color: #9C9C9C;
    border-radius: 1px;
  }
  
  &::before {
    margin-bottom: 4px;
  }
`;

export const DndItem: React.FC<DndItemProps> = ({
  id, children, index, moveCard, border = true,
}) => {
  const ref = useRef<HTMLDivElement>(null);
  const [{ handlerId }, drop] = useDrop({
    accept: 'card',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    // @ts-ignore
    hover(item: DragItem, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) {
        return;
      }

      const hoverBoundingRect = ref.current?.getBoundingClientRect();

      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      const clientOffset = monitor.getClientOffset();

      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      moveCard(dragIndex, hoverIndex);

      // @ts-ignore
      // eslint-disable-next-line no-param-reassign
      item.index = hoverIndex;
    },
  }) as any;

  const [{ isDragging }, drag, preview] = useDrag({
    type: 'card',
    item: () => ({ id, index }),
    collect: (monitor: DragSourceMonitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  preview(drop(ref));
  return (
    <DragContainer ref={ref} isDragging={isDragging} data-handler-id={handlerId} border={border}>
      {isDragging && <Wrapper isDragging={isDragging} />}
      <DragTarget data-dragging="true" ref={drag} />
      {children}
    </DragContainer>
  );
};
