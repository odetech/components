import React, { Fragment, useEffect, useState } from 'react';

import { Collapse } from 'reactstrap';
import { EditableChipAnySelect } from '~/modules/task-drawer/ui/fields/chipSelect';
import { CUSTOM_CHIPS_IDS } from '~/modules/task-drawer/chips';
import { useUnit } from 'effector-react';
import { $$clientsModel } from '~/entities/clients';
import { $$themeModel } from '~/entities/theme';
import { ColorChip } from '~/modules/task-drawer/ui/fields/color-chip';
import { ColorDotChip } from '~/modules/task-drawer/ui/fields/color-dot-chip';
import { format } from 'date-fns';
import { $$projectsModel } from '~/entities/projects';
import { $$usersModel } from '~/entities/users';
import { UserChip } from '~/modules/task-drawer/ui/fields/user-chip';
import { Avatar } from '~/components/avatar';
import { TagsElements } from '~/components/tags';
import ReactTooltip from 'react-tooltip';
import styled from '@emotion/styled';
import { DueDateIcon } from '~/components/icons';
import { UserOptionComponent } from '~/components/default-components';
import {
  ArrowLeftDoubleIcon, ClientIcon, PriorityIcon, ProjectIcon, StatusIcon, TagIcon, UserIcon,
} from './icons';
import {
  Panel, Icons, Fields, FieldTitle, Field, ToggleButton, FieldValue,
} from './styles';
import { getAssignedTo, getHashtagsByProject, getOptionsUsers } from '../lib';
import type { Form } from '../model/form';
import { $$form } from '../model';

const TagButton = styled('button')`
  background: transparent;
  box-shadow: none;
  border: 0;
  display: flex;
  align-items: center;
  justify-content: center;  
  
  &:active, &:focus, &:hover {
    background: transparent;
    box-shadow: none;
    border: 0;
  }
    
  svg {
    width: 20px;
    height: 20px;
    stroke: #3B3C3B;
  }

  &:disabled {
    opacity: 0.6;
    cursor: not-allowed;  
  }
`;

const HashtagsWrapper = styled('div')`
  min-height: 24px;
  position: absolute;  
  display: flex;
  left: -5px;  
  align-items: center;
`;

const STATIC_URL = process.env.API_URL?.replace('/api/v1', '') || 'https://server-beta.odecloud.app';

export const PanelBlock = () => {
  const orgOptions = useUnit($$clientsModel.$clientsOptions);
  const clientsLoading = useUnit($$clientsModel.$clientsLoading);
  const projectsLoading = useUnit($$projectsModel.$projectsLoading);
  const projects = useUnit($$projectsModel.$projectsActive);
  const projectTagsUpdated = useUnit($$projectsModel.events.tagsUpdated);
  const users = useUnit($$usersModel.$users);
  const [canClose, setCanClose] = useState(false);
  const [showHashtags, setShowHashtags] = useState<boolean>(false);
  const {
    projectOptions, form, onChanged, clientsContacts,
  } = useUnit({
    projectOptions: $$form.$projectOptions,
    form: $$form.$form,
    onChanged: $$form.valuesChanged,
    clientsContacts: $$form.$clientsContacts,
  });

  const {
    priorities,
    priorityOptions,
    activityOptions,
    activities,
  } = useUnit({
    priorityOptions: $$themeModel.$priorityOptions,
    priorities: $$themeModel.$priorities,
    activities: $$themeModel.$activities,
    activityOptions: $$themeModel.$activityOptions,
  });

  const [isOpen, setIsOpen] = useState(true);
  const [chipFocused, setChipFocus] = useState<string | null>(null);
  const clearChip = () => setChipFocus(null);

  const toggle = () => setIsOpen(!isOpen);

  useEffect(() => {
    const element: HTMLDivElement | null = document.querySelector('.task-components-drawer');
    if (isOpen) {
      if (element) {
        element.style.width = '1070px';
      }
    } else if (element) {
      element.style.width = '733px';
    }
  }, [isOpen]);

  const onChange = (value: string | string[], key: keyof Form) => onChanged({ key, value });
  const currentProject = projects[form.projectId?.value || ''];
  const avatarUrl = currentProject?.avatar?.includes('cloudinary')
    ? currentProject?.avatar
    : `${STATIC_URL}${currentProject?.avatar}`;

  const allTags = getHashtagsByProject(
    projects,
    form.orgId?.value,
    form.projectId?.value,
  );

  return (
    <Panel isOpen={isOpen} onClick={!isOpen ? toggle : undefined}>
      <ToggleButton
        isOpen={isOpen}
        onClick={toggle}
        aria-controls="collapse-block"
        aria-expanded={isOpen}
        data-tip
        data-for={`${form._id}-drawer-collapse-button-icon`}
      >
        <ArrowLeftDoubleIcon />
      </ToggleButton>
      <div style={{ display: 'flex', whiteSpace: 'nowrap' }}>
        <Icons isOpen={isOpen}>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-client-icon`}
          >
            <ClientIcon />
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-project-icon`}
          >
            {!isOpen && currentProject && currentProject?.avatar ? (
              <img alt="project_image" src={avatarUrl} />
            ) : <ProjectIcon />}
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-priority-icon`}
          >
            {!isOpen ? (
              <ColorDotChip
                label=""
                color={priorities[form.priority?.value || -1]?.color}
              />
            ) : <PriorityIcon />}
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-status-icon`}
          >
            <StatusIcon />
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-due-date-icon`}
          >
            <DueDateIcon />
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-created-by-icon`}
          >
            {!isOpen && users[form.createdBy?.value || '']?.avatarUrl ? (
              <Avatar
                size={24}
                user={{
                  profile: {
                    firstName: users[form.createdBy?.value || '']?.firstName || 'Not',
                    lastName: users[form.createdBy?.value || '']?.lastName || 'Assigned',
                    avatar: {
                      // eslint-disable-next-line max-len
                      secureUrl: users[form.createdBy?.value || '']?.avatarUrl || undefined,
                    },
                  },
                }}
              />
            ) : (
              <UserIcon />
            )}
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-assigned-to-icon`}
          >
            {!isOpen && users[form.assignedToUserId?.value || '']?.avatarUrl ? (
              <Avatar
                size={24}
                user={{
                  profile: {
                    firstName: users[form.assignedToUserId?.value || '']?.firstName || 'Not',
                    lastName: users[form.assignedToUserId?.value || '']?.lastName || 'Assigned',
                    avatar: {
                      // eslint-disable-next-line max-len
                      secureUrl: users[form.assignedToUserId?.value || '']?.avatarUrl || undefined,
                    },
                  },
                }}
              />
            ) : (
              <UserIcon />
            )}
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-requested-by-icon`}
          >
            {!isOpen && users[form.requestedBy?.value || '']?.avatarUrl ? (
              <Avatar
                size={24}
                user={getAssignedTo(
                  users,
                  clientsContacts,
                  form,
                  'requestedByUserEntity',
                  'requestedBy',
                ).avatarEntity}
              />
            ) : (
              <UserIcon />
            )}
          </div>
          <div
            data-tip
            data-for={`${form._id}-drawer-task-tags-icon`}
          >
            <TagIcon />
          </div>
        </Icons>
        <Collapse isOpen={isOpen} horizontal>
          <Fields id="collapse-block">
            <Field>
              <FieldTitle>Client</FieldTitle>
              <FieldValue isOpen={chipFocused === CUSTOM_CHIPS_IDS.CLIENT}>
                <EditableChipAnySelect
                  focused={chipFocused === CUSTOM_CHIPS_IDS.CLIENT}
                  loading={clientsLoading}
                  options={clientsLoading ? [] : orgOptions}
                  value={clientsLoading ? null : form.orgId}
                  onChange={(val: any) => onChange(val, 'orgId')}
                  chipProps={{ label: form.orgId?.label, emptyLabel: 'Choose client' }}
                  chipChanged={() => setChipFocus(CUSTOM_CHIPS_IDS.CLIENT)}
                  chipCleared={clearChip}
                  clearAfterChanged
                  id="client-select"
                />
              </FieldValue>
            </Field>
            <Field>
              <FieldTitle>Project</FieldTitle>
              <FieldValue isOpen={chipFocused === CUSTOM_CHIPS_IDS.PROJECT}>
                <EditableChipAnySelect
                  focused={chipFocused === CUSTOM_CHIPS_IDS.PROJECT}
                  loading={projectsLoading}
                  options={projectsLoading ? [] : projectOptions}
                  value={projectsLoading ? null : form.projectId}
                  onChange={(val) => onChange(val, 'projectId')}
                  chipProps={{ label: form.projectId?.label, emptyLabel: 'Choose project' }}
                  chipChanged={() => setChipFocus(CUSTOM_CHIPS_IDS.PROJECT)}
                  chipCleared={clearChip}
                  clearAfterChanged
                  id="project-select"
                />
              </FieldValue>
            </Field>
            <Field>
              <FieldTitle>Priority</FieldTitle>
              <FieldValue isOpen={chipFocused === CUSTOM_CHIPS_IDS.PRIORITY}>
                <EditableChipAnySelect
                  Component={ColorDotChip}
                  chipProps={{
                    label: priorities[form.priority?.value || -1]?.name,
                    color: priorities[form.priority?.value || -1]?.color,
                  }}
                  options={priorityOptions}
                  value={form.priority}
                  onChange={(val: any) => {
                    onChange(val, 'priority');
                    clearChip();
                  }}
                  focused={chipFocused === CUSTOM_CHIPS_IDS.PRIORITY}
                  loading={false}
                  chipCleared={clearChip}
                  chipChanged={() => setChipFocus(CUSTOM_CHIPS_IDS.PRIORITY)}
                  width="167px"
                />
              </FieldValue>
            </Field>
            <Field>
              <FieldTitle>Status</FieldTitle>
              <FieldValue isOpen={chipFocused === CUSTOM_CHIPS_IDS.ACTIVITY}>
                <EditableChipAnySelect
                  Component={ColorChip}
                  chipProps={{
                    label: activities[form.activity?.value || -1]?.name,
                    color: activities[form.activity?.value || -1]?.color,
                    customStyle: 'position: absolute; top: -5px;',
                  }}
                  options={activityOptions}
                  value={form.activity}
                  onChange={(val: any) => {
                    onChange(val, 'activity');
                    clearChip();
                  }}
                  focused={chipFocused === CUSTOM_CHIPS_IDS.ACTIVITY}
                  loading={false}
                  chipCleared={clearChip}
                  chipChanged={() => setChipFocus(CUSTOM_CHIPS_IDS.ACTIVITY)}
                  width="167px"
                />
              </FieldValue>
            </Field>
            <Field>
              <FieldTitle>Due date</FieldTitle>
              <FieldValue isOpen={chipFocused === CUSTOM_CHIPS_IDS.DUE_DATE}>
                <EditableChipAnySelect
                  focused={chipFocused === CUSTOM_CHIPS_IDS.DUE_DATE}
                  loading={false}
                  options={[]}
                  value={form.dueDate || new Date()}
                  onChange={(val: any) => {
                    onChange(val, 'dueDate');
                    clearChip();
                  }}
                  chipProps={{
                    label: format(form.dueDate || new Date(), 'MMM dd, yyyy').toString(),
                    emptyLabel: 'Choose date',
                  }}
                  chipChanged={() => setChipFocus(CUSTOM_CHIPS_IDS.DUE_DATE)}
                  chipCleared={clearChip}
                  clearAfterChanged
                  id="dueDate-field"
                  isDate
                />
              </FieldValue>
            </Field>

            <Field>
              <FieldTitle>Assigned by</FieldTitle>
              <FieldValue>
                <UserChip
                  // @ts-ignore
                  /* eslint-disable-next-line max-len */
                  label={form.createdBy?.label}
                  hoverOff
                  emptyLabel="-"
                  avatarEntity={{
                    profile: {
                      firstName: users[form.createdBy?.value || '']?.firstName || 'Not',
                      lastName: users[form.createdBy?.value || '']?.lastName || 'Assigned',
                      avatar: {
                        // eslint-disable-next-line max-len
                        secureUrl: users[form.createdBy?.value || '']?.avatarUrl || undefined,
                      },
                    },
                  }}
                />
              </FieldValue>
            </Field>

            <Field>
              <FieldTitle>Assigned to</FieldTitle>
              <FieldValue isOpen={chipFocused === CUSTOM_CHIPS_IDS.ASSIGNED_TO}>
                <EditableChipAnySelect
                  Component={UserChip}
                  chipProps={{
                    label: form.assignedToUserId?.label || 'Unassigned',
                    avatarEntity: {
                      profile: {
                        firstName: users[form.assignedToUserId?.value || '']?.firstName || 'Not',
                        lastName: users[form.assignedToUserId?.value || '']?.lastName || 'Assigned',
                        avatar: {
                          // eslint-disable-next-line max-len
                          secureUrl: users[form.assignedToUserId?.value || '']?.avatarUrl || undefined,
                        },
                      },
                    },
                    emptyLabel: '-',
                    hoverOff: true,
                  }}
                  options={getOptionsUsers(
                    users,
                    clientsContacts,
                    false,
                    projects[form.projectId?.value || ''],
                  )}
                  components={{
                    Option: UserOptionComponent,
                  }}
                  value={form.assignedToUserId || null}
                  onChange={(val: any) => {
                    onChange(val, 'assignedToUserId');
                  }}
                  focused={chipFocused === CUSTOM_CHIPS_IDS.ASSIGNED_TO}
                  loading={false}
                  chipCleared={clearChip}
                  chipChanged={() => setChipFocus(CUSTOM_CHIPS_IDS.ASSIGNED_TO)}
                  position="bottom"
                  width="167px"
                  clearAfterChanged
                />
              </FieldValue>
            </Field>

            <Field>
              <FieldTitle>Requested by</FieldTitle>
              <FieldValue isOpen={chipFocused === CUSTOM_CHIPS_IDS.REQUESTED_BY}>
                <EditableChipAnySelect
                  chipProps={{
                    label: form.requestedBy?.label || '',
                    avatarEntity: getAssignedTo(
                      users,
                      clientsContacts,
                      form,
                      'requestedByUserEntity',
                      'requestedBy',
                    ).avatarEntity,
                    emptyLabel: '-',
                  }}
                  width="167px"
                  Component={UserChip}
                  components={{
                    Option: UserOptionComponent,
                  }}
                  options={getOptionsUsers(
                    users,
                    clientsContacts,
                    true,
                    projects[form.projectId?.value || ''],
                  )}
                  value={form.requestedBy || null}
                  onChange={(val: any) => {
                    onChange(val, 'requestedBy');
                  }}
                  focused={chipFocused === CUSTOM_CHIPS_IDS.REQUESTED_BY}
                  loading={false}
                  chipChanged={() => setChipFocus(CUSTOM_CHIPS_IDS.REQUESTED_BY)}
                  chipCleared={clearChip}
                  position="top"
                  clearAfterChanged
                />
              </FieldValue>
            </Field>

            <Field>
              <FieldTitle>Tags</FieldTitle>
              <FieldValue>
                {!showHashtags && form.hashtags.length === 0 ? (
                  <>
                    <TagButton
                      disabled={!form.projectId?.value}
                      type="button"
                      data-for="new-tag-tooltip"
                      data-tip
                      onClick={() => {
                        setShowHashtags(true);
                      }}
                    >
                      -
                    </TagButton>
                    {!form.projectId?.value && (
                      <ReactTooltip
                        id="new-tag-tooltip"
                        type="dark"
                        effect="solid"
                        place="right"
                        multiline
                      >
                        Select client and project first
                      </ReactTooltip>
                    )}
                  </>
                ) : (
                  <HashtagsWrapper>
                    <TagsElements
                      allTags={allTags}
                      value={form.hashtags.map((h) => ({ label: h, value: h }))}
                      isActive={showHashtags}
                      onChange={(values: { label: string, value: string }[]) => {
                        onChange(values.map((h) => h.value), 'hashtags');
                      }}
                      canClose={canClose}
                      setCanClose={setCanClose}
                      close={() => {
                        setShowHashtags(false);
                        setCanClose(false);
                      }}
                      onChangeTags={(newTags: { label: string, value: string }[]) => {
                        projectTagsUpdated({
                          project: projects[form.projectId?.value || '']!,
                          values: { hashtags: newTags.map((t) => t.value) },
                        });
                      }}
                      taskId={`${form._id}`}
                      customStyles={{
                        padding: 0,
                        width: 200,
                      }}
                    />
                  </HashtagsWrapper>
                )}
              </FieldValue>
            </Field>
          </Fields>
        </Collapse>
      </div>
      <ReactTooltip
        id={`${form._id}-drawer-collapse-button-icon`}
        effect="solid"
      >
        {isOpen ? 'Collapse' : 'Expand'}
      </ReactTooltip>
      {!isOpen && (
        <Fragment>
          <ReactTooltip
            id={`${form._id}-drawer-task-client-icon`}
            effect="solid"
          >
            {form.orgId?.label || 'Client doesn\'t choose'}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-project-icon`}
            effect="solid"
          >
            {form.projectId?.label || 'Project doesn\'t choose'}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-priority-icon`}
            effect="solid"
          >
            {form.priority?.label || 'Priority doesn\'t choose'}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-status-icon`}
            effect="solid"
          >
            {form.activity?.label || 'Status doesn\'t choose'}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-due-date-icon`}
            effect="solid"
          >
            Due
            {' '}
            {format(form.dueDate, 'MMM dd, yyyy')}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-created-by-icon`}
            effect="solid"
          >
            {`Created by ${form.createdBy?.label || 'Nobody'}`}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-assigned-to-icon`}
            effect="solid"
          >
            {`Assigned to ${form.assignedToUserId?.label || 'Nobody'}`}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-requested-by-icon`}
            effect="solid"
          >
            {`Requested by ${form.requestedBy?.label || 'Nobody'}`}
          </ReactTooltip>
          <ReactTooltip
            id={`${form._id}-drawer-task-tags-icon`}
            effect="solid"
          >
            <div style={{ display: 'flex', flexDirection: 'column', gap: '4px' }}>
              {form.hashtags.length ? form.hashtags.map((t) => <span>{t}</span>) : 'Hashtags is empty'}
            </div>
          </ReactTooltip>
        </Fragment>
      )}
    </Panel>
  );
};
