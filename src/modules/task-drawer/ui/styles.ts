import styled from '@emotion/styled';
import Button from '~/components/default-components/Button';
import Drawer from 'react-modern-drawer';
import { Link } from 'react-router-dom';
import { Colors } from '../../../styles/palette';

export const DrawerWrapper = styled(Drawer)<{ fixedHeight?: number }>`
  margin-top: 64px;
  z-index: 1101 !important;
  height: calc(100vh - 64px) !important;
  display: flex;
  transition: width 0.3s;
  flex-direction: row;
  border-radius: 20px 0px 0px 20px;
  background: #FFF;
  box-shadow: 0px 0px 15px 0px #D8D8D8;
  padding: 24px;
  gap: 24px;  

  & > div {
      padding-bottom: 0;
      margin-bottom: 0;
  }

  .comments-widget {
      height: calc(100% - 200px - ${({ fixedHeight }) => (fixedHeight ? (fixedHeight - 30) : 0)}px);

      & > div {
          padding-right: 0;
      }
  }
  #comment-form {
      padding-bottom: 0;
      padding-right: 0;
  }

  .subtask-header {
      flex-direction: column;
      align-items: flex-end;
      gap: 5px;
      border: 0;

      & > div {
          width: 100%;
      }
  }
`;

export const Panel = styled('div')<{ isOpen: boolean}>`
  padding-top: 16px;
    
  min-width: ${({ isOpen }) => (isOpen ? '360px' : '24px')};
  max-width: ${({ isOpen }) => (isOpen ? '360px' : '24px')};
    
  transition: width 500ms;
    
  .collapse-horizontal {
    width: 100%;
  }  
`;

export const Info = styled('div')`
  width: 100%;
  display: flex;
  flex-direction: column;  
`;

export const Tabs = styled('div')`
  display: flex;
  gap: 14px;
  margin-bottom: 24px;  
  margin-top: 16px;
`;

export const Tab = styled('button')<{ isActive: boolean }>`
  font-size: 14px;
  font-style: normal;
  font-weight: 500;
  line-height: 20px;
  background: transparent;
  box-shadow: none;
  border: 0;  
  color: ${({ isActive }) => (isActive ? Colors.primary[900] : Colors.grey[900])};
  border-bottom: 2px solid ${({ isActive }) => (isActive ? Colors.primary[900] : 'transparent')};

  &:active, &:focus, &:hover {
    background: transparent;
    box-shadow: none;
  }
`;

export const CloseButton = styled(Button)`
  background: transparent;
  position: absolute;
  right: 0;
  top: 0;
  padding: 2px 0;
  
  svg {
    width: 24px;
    height: 24px;
    
    fill: ${Colors.grey[900]};
  }
  
  &:hover, &:active, &:focus {
    background: transparent;
  }
`;

export const Icons = styled('div')<{isOpen: boolean}>`
  display: flex;
  flex-direction: column;  
  gap: 28px;
  padding: 8px 0;
  
  & > div:not(.__react_component_tooltip) {
    width: 24px;
    height: 24px;
    cursor: ${({ isOpen }) => (isOpen ? 'inherit' : 'pointer')};  
  }  
    
  svg {
    width: 24px;
    height: 24px;
  }
`;

export const ToggleButton = styled(Button)<{ isOpen: boolean }>`
  background: transparent;
  border: 0;
  width: 24px;
  height: 24px;
  justify-content: flex-start;
  margin-bottom: 40px;  
  padding: 0;  
  box-shadow: none;
    
  & > svg {
    transform: rotate(${({ isOpen }) => (isOpen ? 180 : 0)}deg);
  }  
  
  &:hover, &:active, &:focus {
    background: transparent;
    border: 0;
    box-shadow: none;
  }
`;

export const Fields = styled('div')`
  display: flex;
  flex-direction: column;
  gap: 24px;
  max-width: 300px;  
  width: 300px;  
  padding: 9px 8px 8px 8px;
`;

export const Field = styled('div')`
  display: flex;
  gap: 24px;
  padding: 2px 0;
    
  &:first-of-type {
    padding-top: 0;
  }  
`;

export const FieldTitle = styled('div')`
  font-size: 14px;
  font-style: normal;
  display: flex;
  align-items: center;
  font-weight: 500;
  line-height: 20px;
  min-width: 104px;
  max-width: 104px;
  color: #767776;
`;

export const FieldValue = styled('div')<{isOpen?: boolean }>`
  position: relative;
  height: 24px;
  width: 100%;  

  ${({ isOpen }) => isOpen && `
    &:before {
      content: "";
      border: 1px solid #F7942A;
      border-radius: 8px;
      top: -8px;
      left: -12px;
      height: calc(100% + 16px);
      width: 191px;
      position: absolute;
    }
  `}
`;

// Icons, Fields, FieldTitle, FieldValue
export const HorizontalLine = styled('div')`
  width: 1px;
  height: 100%; 
  background: #CECECE;  
`;

export const HeaderButtons = styled('div')`
  display: flex;
  position: relative;  
  justify-content: space-between;
`;

export const TitleWrapper = styled('div')`
  display: flex;
`;

export const TaskLink = styled(Link)`
  height: 22px;
  display: flex;
  align-items: center;

  svg {
    height: 22px;
    width: 22px;
  }
`;
