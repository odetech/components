import { TProject } from '~/entities/projects';
import { TAssignedToUser } from '~/entities/tasks/types';
import { differenceInMinutes, formatDuration, intervalToDuration } from 'date-fns';

export const getCurrentTrackedTime = (timeTrackerCustomValue: string) => {
  if (!timeTrackerCustomValue) return 0;
  const splittedCustomValue = timeTrackerCustomValue.split(' ');
  return splittedCustomValue.reduce((prev, current, index): any => {
    const value = current.replace(/\D/gi, '');
    switch (index) {
      case 0:
        return prev + (Number(value) * 3600);
      case 1:
        return prev + (Number(value) * 60);
      default:
        return 0;
    }
  }, 0);
};

export const getTimeTrackingValue = (accumulative: number | null) => {
  if (!accumulative) {
    return '0h 0m';
  }

  const h = Math.floor(Math.trunc(accumulative / 3600));
  const m = Math.floor(((accumulative / 3600) % 1) * 60);
  return `${h < 10 ? `0${h}` : h}h ${m < 10 ? `0${m}` : m}m`;
};

type FormatShort = {
  xSeconds: string;
  xMinutes: string;
  xHours: string;
  xDays: string;
  xWeeks: string;
  xMonths: string;
  xYears: string;
};

const formatDistanceLocale: FormatShort = {
  xSeconds: '{{count}}s',
  xMinutes: '{{count}}m',
  xHours: '{{count}}h',
  xDays: '{{count}}d',
  xWeeks: '{{count}}w',
  xMonths: '{{count}}mon',
  xYears: '{{count}}y',
};

type KeysShort = keyof FormatShort;

const shortEnLocale = {
  formatDistance: (token: KeysShort, count: number) => formatDistanceLocale[token].replace('{{count}}', count.toString()),
};

function toHoursAndMinutes(totalMinutes: number) {
  const hours = Math.floor(totalMinutes / 60);
  const minutes = totalMinutes % 60;

  return { hours, minutes };
}

export const secondToHumanFormat = (seconds: number, isShort = false, onlyHours = true, leadZero = false) => {
  if (onlyHours && isShort && seconds) {
    const diffInMinutes = differenceInMinutes(new Date(0), new Date(seconds * 1000));
    const formatedMinutes = toHoursAndMinutes(Math.abs(diffInMinutes));
    if (leadZero) {
      const { hours, minutes } = formatedMinutes;
      return `${hours < 10 ? `0${hours}` : hours}h ${minutes < 10 ? `0${minutes}` : minutes}m`;
    }
    return `${formatedMinutes.hours}h ${formatedMinutes.minutes}m`;
  }
  if (isShort && seconds) {
    const durations = intervalToDuration({ start: 0, end: seconds * 1000 });
    let firstTypeDuration = ['minutes'];
    if (durations.seconds && durations.seconds >= 30) durations.minutes = (durations?.minutes || 0) + 1;
    if (durations.minutes) firstTypeDuration = ['minutes'];
    if (durations.hours) firstTypeDuration = ['hours', 'minutes'];
    if (durations.days) firstTypeDuration = ['days', 'hours'];
    if (durations.months) firstTypeDuration = ['months', 'days'];
    if (durations.years) firstTypeDuration = ['years', 'months'];
    return formatDuration(durations, { format: firstTypeDuration, locale: shortEnLocale }) || '0m';
  }
  return (seconds ? formatDuration(intervalToDuration({ start: 0, end: seconds * 1000 })) : '0h 0m');
};

const DEFAULT_VALUE_ASSIGNED_USERS = {
  value: null,
  label: 'Unassigned',
  firstName: 'Unassigned',
  lastName: 'Unassigned',
  avatar: {
    secureUrl: undefined,
  },
};

export const getHashtagsByProject = (projects: Record<string, TProject>, orgId?: string, projectId?: string) => {
  let currentOptions: any[] = [];
  if (projectId && orgId && projects[projectId]) {
    currentOptions = projects[projectId].hashtags?.map((hash: string) => ({ label: hash, value: hash })) || [];
    return currentOptions;
  }
  if (orgId) {
    Object.values(projects).forEach((project) => {
      if (project.orgId === orgId) {
        project.hashtags.forEach((hash: string) => {
          currentOptions.push(({ label: hash, value: hash }));
        });
      }
    });
    return currentOptions;
  }
  return currentOptions;
};

export const getOptionFromEntity = (entity: any, labelKey = 'title') => {
  if (!entity?.id) {
    return null;
  }

  return ({
    value: entity.id,
    label: entity?.[labelKey],
  });
};

export const getOptionsUsers = (
  users: Record<string, any>,
  contacts: any[],
  withContact: boolean,
  project?: any,
) : any[] => {
  if (project) {
    const opts = [
      DEFAULT_VALUE_ASSIGNED_USERS,
      ...project.assignedToUserId
        .filter((assignedToUserIdT: any) => users[assignedToUserIdT])
        .map((assignedToUserIdT: any) => ({
          ...getOptionFromEntity(users[assignedToUserIdT], 'shortName'),
          firstName: users[assignedToUserIdT]?.firstName,
          lastName: users[assignedToUserIdT]?.lastName,
          avatar: {
            secureUrl: users[assignedToUserIdT]?.avatarUrl,
          },
        })),
      ...(withContact
        ? contacts.map((contact) => ({
          value: contact.id,
          label: `${contact.shortName}-contact`,
          firstName: contact.firstName,
          lastName: contact.lastName,
          avatar: {
            secureUrl: undefined,
          },
        }))
        : []),
    ];
    return opts
      .filter((value, index, self) => index === self.findIndex((opt) => (
        opt?.value === value?.value
      ))).map((item) => ({
        label: item?.label || '',
        value: item?.value || '',
        firstName: item.firstName,
        lastName: item.lastName,
        avatar: item.avatar,
      }));
  }

  return [];
};

type TKeyValue = 'assignedToUserEntity' | 'assignedByUserEntity' | 'requestedByUserEntity';
type TIdValue = 'assignedToUserId' | 'requestedBy' | 'assignedBy';

export const getAssignedTo = (
  users: Record<string, any>,
  organizationsContacts: any[],
  values: any,
  keyValue: TKeyValue,
  idValue: TIdValue,
) => {
  const currentidValue = values[idValue]?.value;
  const currentItem = organizationsContacts.find((item) => item.id === values.requestedBy?.value);
  if (!currentItem?.id || idValue !== 'requestedBy') {
    return {
      value: getOptionFromEntity(values[keyValue], 'shortName'),
      label: values[keyValue]?.name || users[currentidValue || '']?.name || 'Unassigned',
      avatarEntity: {
        profile: {
          firstName: values[keyValue]?.firstName || users[currentidValue || '']?.firstName || 'Not',
          lastName: values[keyValue]?.lastName || users[currentidValue || '']?.lastName || 'Assigned',
          avatar: {
            secureUrl: values[keyValue]?.avatarUrl
              || users[currentidValue || '']?.avatarUrl || undefined,
          },
        },
      },
    };
  }
  return {
    value: { value: currentItem?.id, label: currentItem?.name } || null,
    label: currentItem?.name || 'Unknown',
    avatarEntity: {
      profile: {
        firstName: currentItem?.firstName || 'Not',
        lastName: currentItem?.lastName || 'Assigned',
        avatar: {
          secureUrl: undefined,
        },
      },
    },
  };
};

export const getSuggestionsByProject = (
  users: Record<string, TAssignedToUser>,
  project?: TProject | null,
) => {
  const result: {
    _id: string;
    profile: {
      firstName: string;
      lastName: string;
    }
  }[] = [];

  if (project) {
    project.assignedToUserId.forEach((assignedToUserId) => {
      const user = users[assignedToUserId];
      if (user) {
        result.push({
          _id: user.id,
          profile: {
            firstName: user.firstName!,
            lastName: user.lastName!,
          },
        });
      }
    });
  }

  return result;
};
