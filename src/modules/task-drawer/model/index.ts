import { reset } from 'patronum';
import * as $$form from './form';
import * as $$task from './task';
import './edit';
import '../ui/widgets/time-tracker/edit-model';
import { $$trackerCreateTaskModel } from '../ui/widgets/time-tracker/model';
import * as $$subtasksModel from './subtasks';
import { createEvent } from 'effector';

const drawerUnmounted = createEvent();

reset({
  clock: drawerUnmounted,
  target: [
    $$form.$form,
    $$form.$formWasInitialized,
    $$trackerCreateTaskModel.$timeTrackers,
    $$trackerCreateTaskModel.$estimatedTime,
    $$subtasksModel.$subTasks,
    $$subtasksModel.$subTaskOrder,
  ],
});

export {
  $$form,
  $$task,
  drawerUnmounted,
};
