import {
  combine, createEvent, createStore, sample,
} from 'effector';
import { $$tasksModel } from '~/entities/tasks';
import { createTimeTrackerFx, editTimeTrackerFx, removeTimeTrackerFx } from '~/entities/time-trackers';

const $activeTaskId = createStore<string | null>(null);
const $activeTask = combine($$tasksModel.$tasks, $activeTaskId, (tasks, activeTaskId) => {
  if (activeTaskId) {
    return tasks[activeTaskId] || null;
  }
  return null;
});
const $estimatedTime = combine($activeTask, (activeTask) => activeTask?.estimatedTime || 0);
const taskFetched = createEvent<string>();
const taskEdited = createEvent<{ taskId: string, payload: any }>();

sample({
  clock: taskFetched,
  target: $activeTaskId,
});

sample({
  clock: $activeTaskId,
  filter: (taskId): taskId is string => taskId !== null,
  target: $$tasksModel.events.taskRequested,
});

sample({
  clock: taskEdited,
  fn: ({ taskId, payload }) => {
    const changesTask = { ...payload };
    if (changesTask?.hashtags?.length) {
      changesTask.hashtags = Array.from(
        new Set(changesTask.hashtags.map((str: string) => (str[0] === '#' ? str : `#${str}`))),
      );
    }

    return {
      type: 'update_task',
      payload: {
        taskId,
        ...changesTask,
      },
    };
  },
  target: $$tasksModel.events.socketSent,
});

sample({
  clock: [createTimeTrackerFx.done, editTimeTrackerFx.done, removeTimeTrackerFx.done],
  source: $activeTask,
  fn: (task) => task?.id || '',
  target: taskFetched,
});

export {
  taskFetched,
  taskEdited,
  $activeTask,
  $estimatedTime,
};
