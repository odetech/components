import {
  combine, createEffect, createEvent, createStore, sample,
} from 'effector';
import { $$projectsModel, TProject } from '~/entities/projects';
import { $$themeModel } from '~/entities/theme';
import { $$usersModel } from '~/entities/users';
import { showToast } from '~/components/default-components';
import { $currentUser } from '~/config';
import { newTaskSchema } from '~/entities/tasks';
import { $$clientsModel } from '~/entities/clients';

export type Form = {
  activity: { label: string; value: string | number } | null
  title: string,
  followers: string[],
  _id: string,
  priority: { label: string; value: string | number } | null,
  description: string,
  dueDate: Date,
  orgId: { label: string; value: string } | null,
  projectId: { label: string; value: string } | null,
  assignedToUserId: { label: string; value: string } | null,
  createdBy: { label: string; value: string } | null,
  files: { file: File; uiId: string; }[],
  hashtags: string[],
  uiIds: string[],
  isDuplicate?: boolean,
  requestedBy: { label: string; value: string } | null,
}

export type InitProps = {
  defaultValues?: Partial<Form>;
  isNewProject?: boolean;
  activeOrg: { value: string, label: string } | null;
  activeProject: { value: string, label: string } | null;
  currentUser: { _id: string } | null;
}

const $form = createStore<Form>({
  activity: null,
  title: '',
  _id: 'new',
  priority: null,
  description: '',
  dueDate: new Date(Date.now() + 12096e5),
  followers: [],
  orgId: null,
  projectId: null,
  assignedToUserId: null,
  createdBy: null,
  files: [],
  hashtags: [],
  uiIds: [],
  requestedBy: null,
});

const defaultActivity = 2;
const defaultPriority = 5;

/* Init form */
const viewInitialized = createEvent<InitProps>();
const formInitialized = createEvent<InitProps>();
const $formWasInitialized = createStore(false);
const $formIsValid = createStore(false);
const valuesChanged = createEvent<{ key: keyof Form, value: string | string[] | { file: File; uiId: string; }[] }>();
const $errors = createStore<Record<string, string>>({}).reset([formInitialized, valuesChanged]);

// const $comments = createStore<Record<string, { comment?: string }> | null>(null);

sample({
  clock: viewInitialized,
  source: {
    clients: $$clientsModel.$clientsActive,
    projects: $$projectsModel.$projectsActive,
    form: $form,
    users: $$usersModel.$users,
    currentUser: $currentUser,
    activities: $$themeModel.$activities,
    priorities: $$themeModel.$priorities,
  },
  fn: (
    {
      clients,
      projects,
      users,
      form,
      priorities,
      activities,
    },
    {
      defaultValues,
      isNewProject,
      activeOrg,
      activeProject,
      currentUser,
    },
  ) =>
    // eslint-disable-next-line no-nested-ternary,implicit-arrow-linebreak
    (defaultValues ? {
      _id: defaultValues?._id || 'new',
      activity: (defaultValues.activity ? {
        label: activities[defaultValues.activity.value]?.name,
        value: defaultValues.activity.value,
      } : {
        label: activities[2]?.name,
        value: activities[2]?.value,
      }),
      title: defaultValues.title,
      priority: (defaultValues.priority ? {
        label: priorities[defaultValues.priority.value]?.name,
        value: defaultValues.priority.value,
      } : {
        label: priorities[5]?.name,
        value: priorities[5]?.value,
      }),
      description: defaultValues.description,
      dueDate: defaultValues.dueDate ? new Date(defaultValues.dueDate) : new Date(Date.now() + 12096e5),
      orgId: defaultValues.orgId ? {
        value: clients[defaultValues.orgId.value]?.id,
        label: clients[defaultValues.orgId.value]?.name,
      } : (activeOrg || null),
      projectId: defaultValues.projectId ? {
        value: projects[defaultValues.projectId.value]?.id,
        label: projects[defaultValues.projectId.value]?.title,
      } : activeProject,
      assignedToUserId: defaultValues.assignedToUserId ? {
        value: users[defaultValues.assignedToUserId.value]?.id,
        label: users[defaultValues.assignedToUserId.value]?.shortName,
      } : null,
      createdBy: defaultValues.createdBy ? {
        value: users[defaultValues.createdBy.value]?.id,
        label: users[defaultValues.createdBy.value]?.shortName,
      } : {
        value: users[currentUser?._id || '']?.id,
        label: users[currentUser?._id || '']?.shortName,
      },
      files: [],
      followers: defaultValues.followers || [currentUser?._id],
      hashtags: defaultValues.hashtags || [],
      uiIds: [],
      requestedBy: defaultValues.requestedBy ? {
        value: users[defaultValues.requestedBy.value]?.id,
        label: users[defaultValues.requestedBy.value]?.shortName,
      } : {
        value: users[currentUser?._id || '']?.id,
        label: users[currentUser?._id || '']?.shortName,
      },
    } : (isNewProject
      ? {
        _id: 'new',
        activity: { label: activities[defaultActivity], value: defaultActivity },
        title: '',
        priority: { label: priorities[defaultPriority], value: defaultPriority },
        description: '',
        dueDate: new Date(Date.now() + 12096e5),
        orgId: activeOrg || null,
        projectId: activeProject,
        assignedToUserId: null,
        files: [],
        hashtags: [],
        followers: [currentUser?._id],
        uiIds: [],
        createdBy: {
          value: users[currentUser?._id || '']?.id,
          label: users[currentUser?._id || '']?.shortName,
        },
        requestedBy: {
          value: users[currentUser?._id || '']?.id,
          label: users[currentUser?._id || '']?.shortName,
        },
      }
      : {
        activity: form?.activity || { label: activities[defaultActivity], value: defaultActivity },
        title: '',
        _id: 'new',
        priority: form?.priority || { label: priorities[defaultPriority], value: defaultPriority },
        description: '',
        dueDate: form?.dueDate ? new Date(form.dueDate) : new Date(),
        orgId: form?.orgId || activeOrg || null,
        projectId: form?.projectId || activeProject,
        assignedToUserId: form?.assignedToUserId || null,
        files: [],
        hashtags: [],
        followers: [currentUser?._id],
        uiIds: [],
        createdBy: {
          value: users[currentUser?._id || '']?.id,
          label: users[currentUser?._id || '']?.shortName,
        },
        requestedBy: {
          value: users[currentUser?._id || '']?.id,
          label: users[currentUser?._id || '']?.shortName,
        },
      })) as any,
  target: [$form, formInitialized],
});

sample({
  clock: formInitialized,
  fn: () => true,
  target: $formWasInitialized,
});

const getProjectOptionsByOrgId = (
  projects: Record<string, TProject>,
  orgId: string | undefined,
): { label: string, value: string }[] => Object.values(projects)
  .filter((project) => orgId && project.orgId === orgId)
  .map((project) => ({
    value: project.id,
    label: project.title,
  }));

const $clientsContacts = combine(
  $$clientsModel.$clientsActive,
  $form,
  (clients, values) => clients[values?.orgId?.value || '']?.contacts || [],
);

const $projectOptions = combine(
  $$projectsModel.$projectsActive,
  $form,
  (
    projects,
    values,
  ) => getProjectOptionsByOrgId(projects, values?.orgId?.value),
);

sample({
  clock: valuesChanged,
  source: $form,
  fn: (values, { key, value }) => {
    if (key === 'orgId' && typeof value !== 'string' && !Array.isArray(value)) {
      return {
        ...values,
        [key]: value,
        projectId: null,
      };
    }
    return {
      ...values,
      [key]: value,
    };
  },
  target: $form,
});

const showError = createEffect((errors: Record<string, string>) => {
  Object.values(errors).forEach((error) => {
    showToast({
      message: error,
      type: 'error',
    });
  });
});

sample({
  clock: $errors,
  filter: (errs) => Object.keys(errs).length > 0,
  target: showError,
});

const validationFx = createEffect(async (form: Form) => {
  try {
    await newTaskSchema.validate(form, { abortEarly: false });
    return true;
  } catch {
    return false;
  }
});

sample({
  clock: $form,
  target: validationFx,
});

sample({
  clock: validationFx.doneData,
  target: $formIsValid,
});

export {
  $form,
  $formIsValid,
  $errors,
  valuesChanged,
  getProjectOptionsByOrgId,
  $clientsContacts,
  $projectOptions,
  viewInitialized,
  $formWasInitialized,
};
