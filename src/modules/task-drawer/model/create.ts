import { createEffect, createEvent, sample } from 'effector';
import { extractHTMLContent, filesUpload } from '~/helpers';
import { getMentionsFromValue } from '~/components/editor';
import { $currentUser } from '~/config';
import { newTaskSchema } from '~/entities/tasks/lib';
import { $$trackerCreateTaskModel } from '../ui/widgets/time-tracker/model';
import { $form, Form, $errors } from './form';
import { createTask } from '../../../entities/tasks/model';

const createSubmit = createEvent();

const createFx = createEffect(async ({
  form, estimatedTime, userId,
}: {
  form: Form,
  estimatedTime: number,
  userId: string,
}) => {
  const d = document.createElement('div');
  d.innerHTML = form.description;
  const textContent = d.textContent || d.innerText;
  let payload2: any;
  if (textContent?.length > 0 || form.description.includes('src')) {
    // upload images
    let uploadedFiles: { cloudinary: any, uiId: string }[] = [];
    if (form.files?.length) {
      const filteredFiles: { file: File, uiId: string }[] = [];
      const uids = Array.from(new Set(form.uiIds));
      uids.forEach((uid) => {
        const file = form.files.find((ff) => uid === ff.uiId);
        if (file) filteredFiles.push(file);
      });
      uploadedFiles = await filesUpload(filteredFiles);
    }
    payload2 = {
      associatedId: '',
      text: form.description.replace(/src="(data[^"]+?)"/g, 'src=""'),
      rawText: extractHTMLContent(form.description),
      createdBy: userId,
      projectId: form.projectId?.value,
      updatedBy: userId,
      mentions: getMentionsFromValue(form?.description),
      tags: [''],
    };

    if (uploadedFiles.length) {
      payload2.files = uploadedFiles;
    }
  }
  const taskPayload: any = {
    title: form.title,
    description: '',
    orgId: form.orgId?.value,
    projectId: form.projectId?.value,
    activity: form.activity?.value,
    estimatedTime,
    priority: form.priority?.value,
    hashtags: form.hashtags.map((value) => (value[0] === '#' ? value : `#${value}`)),
    assignedToUserId: form.assignedToUserId?.value,
    requestedBy: form.requestedBy?.value || userId,
    dueDate: new Date(form.dueDate.getTime() - (form.dueDate.getTimezoneOffset() * 60000)).toISOString(),
  };
  if (payload2?.createdBy) {
    taskPayload.lastInsertedMessage = payload2;
  }

  createTask({ payload: taskPayload, isDuplicate: form.isDuplicate })
});

// eslint-disable-next-line consistent-return
const createSubmitFx = createEffect(async ({ form, userId }: { form: Form, userId: string }) => {
  if (!form?.projectId?.value) return null;
  try {
    await createFx(
      {
        form: {
          ...form,
          dueDate: typeof form.dueDate === 'string' ? new Date(form.dueDate) : form.dueDate,
        },
        estimatedTime: $$trackerCreateTaskModel.$estimatedTime.getState() || 0,
        userId,
      },
    );
  } catch (e: any) {
    const errorData = await e.response?.json();
    console.log(errorData?.detail);
  }
});

const validationFx = createEffect(async ({ form, userId }: { form: Form, userId: string }) => {
  await newTaskSchema.validate(form, { abortEarly: false });
  $errors.reset();
  return { form, userId };
});

sample({
  clock: createSubmit,
  source: { form: $form, currentUser: $currentUser },
  fn: ({ form, currentUser }) => ({
    form,
    userId: currentUser?._id,
  }),
  target: validationFx,
});

sample({
  clock: validationFx.failData,
  fn: (err) => {
    const errors: any = {};
    // @ts-ignore
    err.inner.forEach((validationError) => {
      errors[validationError.path] = validationError.message;
    });
    return errors;
  },
  target: $errors,
});

sample({
  clock: validationFx.doneData,
  target: createSubmitFx,
});

export {
  createSubmit,
  createSubmitFx,
};
