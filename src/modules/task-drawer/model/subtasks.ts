import {
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { $currentUser } from '~/config';
import { createSubTaskFx } from '~/entities/subtasks';

const $subTasks = createStore<Record<string, any>>({});
const $subTaskOrder = createStore<string[]>([]);

const onSubmit = createEvent<{ subTasks: Record<string, any>, subTaskTitle: string }>();
const onDelete = createEvent<string>();
const onCheck = createEvent<any>();
const onDragCallback = createEvent<string[]>();
const subtasksCreated = createEvent<{ taskId: string, projectId: string }>();

const subtasksCreateFx = createEffect(
  async ({
    subTasks, subTaskOrder, taskId, userId, projectId,
  }: { subTasks: Record<string, any>, subTaskOrder: string[], taskId: string, userId: string, projectId: string }) => {
    await Promise.all(subTaskOrder.map(async (order) => {
      const payload = {
        title: subTasks[order].title,
        activity: subTasks[order].activity,
        subTaskId: taskId,
        createdBy: userId,
        projectId,
        updatedBy: userId,
      };

      await createSubTaskFx({ payload });
    }));
  },
);

sample({
  clock: subtasksCreated,
  source: {
    subTasks: $subTasks,
    subTaskOrder: $subTaskOrder,
    currentUser: $currentUser,
  },
  fn: (sources, { taskId, projectId }) => ({
    subTasks: sources.subTasks,
    subTaskOrder: sources.subTaskOrder,
    userId: sources.currentUser?._id,
    taskId,
    projectId,
  }),
  target: subtasksCreateFx,
});

$subTaskOrder.on(onDragCallback, (_, orders) => orders);

sample({
  clock: onSubmit,
  source: {
    subTasks: $subTasks,
    subTaskOrder: $subTaskOrder,
  },
  fn: ({ subTasks, subTaskOrder }, values) => {
    const id = (crypto as any).randomUUID();
    if (subTaskOrder.length) {
      const copyState = { ...subTasks };
      const newState:Record<string, any> = {};
      subTaskOrder.forEach((key: string) => {
        newState[key] = copyState[key];
      });
      return {
        ...newState,
        [id]: {
          id,
          title: values.subTaskTitle,
          activity: 1,
        },
      };
    }
    return {
      ...subTasks,
      [id]: {
        id,
        title: values.subTaskTitle,
        activity: 1,
      },
    };
  },
  target: $subTasks,
});

sample({
  clock: onDelete,
  source: {
    subTasks: $subTasks,
    subTaskOrder: $subTaskOrder,
  },
  fn: ({ subTasks, subTaskOrder }, id) => {
    let copyState = { ...subTasks };
    if (subTaskOrder.length) {
      const newState:Record<string, any> = {};
      subTaskOrder.forEach((key: string) => {
        newState[key] = copyState[key];
      });
      copyState = newState;
    }
    delete copyState[id];
    return copyState;
  },
  target: $subTasks,
});

sample({
  clock: onCheck,
  source: {
    subTasks: $subTasks,
    subTaskOrder: $subTaskOrder,
  },
  fn: ({ subTasks, subTaskOrder }, subTask) => {
    let copyState: any = { ...subTasks };
    if (subTaskOrder.length) {
      const newState:Record<string, any> = {};
      subTaskOrder.forEach((key: string) => {
        newState[key] = copyState[key];
      });
      copyState = newState;
    }
    copyState[subTask.id] = {
      ...copyState[subTask.id],
      activity: copyState[subTask.id].activity === 1 ? 4 : 1,
    };
    return copyState;
  },
  target: $subTasks,
});

sample({
  clock: $subTasks,
  fn: (subTasks) => Object.keys(subTasks),
  target: $subTaskOrder,
});

export {
  $subTasks,
  $subTaskOrder,
  onDelete,
  onCheck,
  onDragCallback,
  onSubmit,
  subtasksCreated,
};
