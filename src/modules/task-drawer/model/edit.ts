import { createEffect, sample } from 'effector';
import { format, startOfDay } from 'date-fns';
import { $api } from '~/config';
import { taskEdited } from '~/modules/task-drawer/model/task';
import { $form, valuesChanged } from './form';

export const getBeginningOfDay = (date: Date, withoutMs?: boolean, utc?: boolean) => {
  let newDate = startOfDay(date);
  if (utc) {
    newDate = new Date(newDate.getTime() + date.getTimezoneOffset() * 60000);
  }
  return (
    format(newDate, `yyyy-MM-dd'T'HH:mm:ss${withoutMs ? "" : ".SSS"}`)
  );
};


const taskChangeFx = createEffect(async ({
  key, value, values, taskId, api,
}: any) => {
  if (key === 'orgId' && typeof value !== 'string' && !Array.isArray(value)) {
    valuesChanged({
      ...values,
      orgId: value as { label: string, value: string },
      projectId: null,
      hashtags: [],
    });
    return;
  }
  if (key === 'projectId') {
    // eslint-disable-next-line no-param-reassign
    values.hashtags = [];
  }
  const payload = api.tasks.payload.getPostPayload({
    title: values.title,
    orgId: values.orgId?.value,
    projectId: values.projectId?.value,
    activity: values.activity?.value,
    priority: values.priority?.value,
    assignedToUserId: values.assignedToUserId?.value || null,
    // eslint-disable-next-line max-len,no-nested-ternary
    [key]: (typeof value === 'string' || typeof value === 'number') ? value : (!Array.isArray(value) ? value?.value : value),
    // eslint-disable-next-line max-len
    dueDate: key === 'dueDate' ? getBeginningOfDay(new Date(value as string)) : getBeginningOfDay(values.dueDate as Date),
    // eslint-disable-next-line max-len,no-nested-ternary
    hashtags: key === 'hashtags' ? (Array.isArray(value) ? value.map((hash) => hash?.value ?? hash) : []) : values.hashtags,
  });

  if (!payload.assignedToUserId) {
    // @ts-ignore
    payload.assignedToUserId = null;
  }
  taskEdited({ taskId, payload });
});

sample({
  clock: valuesChanged,
  source: { form: $form, api: $api },
  filter: ({ form: { title, projectId, _id } }) => Boolean(projectId?.value) && Boolean(title) && _id !== 'new',
  fn: ({ form, api }, { key, value }) => ({
    key, value, values: form, taskId: form._id, api,
  }),
  target: taskChangeFx,
});
