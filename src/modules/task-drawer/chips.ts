export const CUSTOM_CHIPS_IDS = {
  ACTIVITY: 'activity',
  PRIORITY: 'priority',
  ASSIGNED_USERS: 'assignedUsers',
  ASSIGNED_BY: 'ASSIGNED_BY',
  ASSIGNED_TO: 'ASSIGNED_TO',
  REQUESTED_BY: 'REQUESTED_BY',
  CREATED_BY: 'CREATED_BY',
  DUE_DATE: 'dueDate',
  CLIENT: 'client',
  PROJECT: 'project',
};
