/* eslint-disable react/destructuring-assignment */
import React, { useEffect, useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { BeatLoader } from 'react-spinners';
import OdeCloud from '@odecloud/odecloud';
import Modal from 'react-modal';
import { componentsInit } from '~/config/init';
import { $$themeModel } from '~/entities/theme';
import { $$popoverMain, $$popoverSecondary } from '~/components/popover/model';
import { initializeApp } from '@firebase/app';
import { getDatabase } from '@firebase/database';
import { $$networksModel } from '~/entities/networks';
import { $$commentsModel } from '~/entities/comments';
import { $$socketModel } from '~/entities/ikinari';
import { useUnit } from 'effector-react';
import { Toast } from '~/components/default-components';
import { $$projectsModel } from '~/entities/projects';
import testUser from '../../../../configs/testUserCredentials.json';
import { DrawerTask, DrawerTaskProps } from '../ui/drawer';
import testFirebaseConfig from '../../../../configs/testFirebaseConfig.json';

Modal.setAppElement('body');

type UserData = {
  info: {
    profile: {
      firstName?: string,
      lastName?: string,
      avatar?: {
        secureUrl?: string | null,
      } | null,
    },
    _id?: string | null,
  },
  isStaff: boolean,
  isDev: boolean,
  id: string,
}

let api: any = null;

const store = configureStore({
  reducer: {},
});

const meta: Meta<DrawerTaskProps & {
  loginEmail: string,
  loginPassword: string,
  endpoint: string,
  shouldReloadChatStorybook: boolean,
  firebaseApiKey: string,
  firebaseAuthDomain: string,
  firebaseDatabaseURL: string,
  firebaseProjectId: string,
  firebaseStorageBucket: string,
  firebaseMessagingSenderId: string,
  firebaseAppId: string,
}> = {
  component: DrawerTask,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.margin = 'unset';
        // @ts-ignore
        document.getElementById('storybook-root').style.flexDirection = 'unset';
        // @ts-ignore
        document.getElementById('storybook-root').style.justifyContent = 'unset';
      }
      return (
        <Provider store={store}>
          <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
            {story()}
          </MemoryRouter>
        </Provider>
      );
    },
  ],
  argTypes: {
    loginEmail: { name: 'loginEmail', control: 'text' },
    loginPassword: { name: 'loginPassword', control: 'text' },
    endpoint: { name: 'endpoint', control: 'text' },
    shouldReloadChatStorybook: { control: 'boolean' },
    firebaseApiKey: { name: 'firebaseApiKey', control: 'text' },
    firebaseAuthDomain: { name: 'firebaseAuthDomain', control: 'text' },
    firebaseDatabaseURL: { name: 'firebaseDatabaseURL', control: 'text' },
    firebaseProjectId: { name: 'firebaseProjectId', control: 'text' },
    firebaseStorageBucket: { name: 'firebaseStorageBucket', control: 'text' },
    firebaseMessagingSenderId: { name: 'firebaseMessagingSenderId', control: 'text' },
    firebaseAppId: { name: 'firebaseAppId', control: 'text' },
  },
  args: {
    loginEmail: testUser.email || '',
    loginPassword: testUser.password || '',
    endpoint: 'https://server-alpha.odecloud.app/api/v1',
    shouldReloadChatStorybook: false,
    firebaseApiKey: testFirebaseConfig.apiKey || '',
    firebaseAuthDomain: testFirebaseConfig.authDomain || '',
    firebaseDatabaseURL: testFirebaseConfig.databaseURL || '',
    firebaseProjectId: testFirebaseConfig.projectId || '',
    firebaseStorageBucket: testFirebaseConfig.storageBucket || '',
    firebaseMessagingSenderId: testFirebaseConfig.messagingSenderId || '',
    firebaseAppId: testFirebaseConfig.appId || '',
  },
};
export default meta;

const startFirebase = (testConfigData: {
  apiKey: string,
  authDomain: string,
  databaseURL: string,
  projectId: string,
  storageBucket: string,
  messagingSenderId: string,
  appId: string,
}) => {
  const firebaseConfig = {
    apiKey: testConfigData.apiKey,
    authDomain: testConfigData.authDomain,
    databaseURL: testConfigData.databaseURL,
    projectId: testConfigData.projectId,
    storageBucket: testConfigData.storageBucket,
    messagingSenderId: testConfigData.messagingSenderId,
    appId: testConfigData.appId,
  };

  const app = initializeApp(firebaseConfig);
  return getDatabase(app);
};

const MainPopover = $$popoverMain.PopoverPortal;
const SecondaryPopover = $$popoverSecondary.PopoverPortal;

// 👇 We create a “template” of how args map to rendering
export const Default: StoryFn<DrawerTaskProps & {
  loginEmail: string,
  loginPassword: string,
  endpoint: string,
  shouldReloadChatStorybook: boolean,
  firebaseApiKey: string,
  firebaseAuthDomain: string,
  firebaseDatabaseURL: string,
  firebaseProjectId: string,
  firebaseStorageBucket: string,
  firebaseMessagingSenderId: string,
  firebaseAppId: string,
}> = (args) => {
  const [userData, setUserData] = useState<UserData | null>(null);
  const [isOpen, setIsOpen] = useState<string | null>(null);
  const isConnected = useUnit($$socketModel.$isConnected);
  const inputRef = React.useRef<any>();
  useEffect(() => {
    if (isConnected) {
      $$commentsModel.events.teamConnected();
    }
  }, [isConnected]);
  const getData = async () => {
    api = new OdeCloud({
      endpoint: args.endpoint,
    });

    const loginResult = await api.auth.login({
      email: args.loginEmail,
      password: args.loginPassword,
      appUrl: 'http://localhost:3000',
    });
    const { authToken, userId } = loginResult;
    const url = 'https://tasks.odecloud.app';

    const data = await api.auth.resync({
      // @ts-ignore
      authToken, userId, url, interval: 300,
    });
    api.setAuthToken(data.authToken);
    api.setUserId(data.userId);
    $$themeModel.themesStarted(data.themes);

    const userDataRequest = await api.users.getUser(
      userId,
      {
        roles: 1,
        skillsets: 1,
        masterNotifications: 1,
        appStore: 1,
        profileCompletion: 1,
      },
    );

    componentsInit({
      api,
      currentUser: userDataRequest,
      firebase: startFirebase({
        apiKey: args.firebaseApiKey,
        authDomain: args.firebaseAuthDomain,
        databaseURL: args.firebaseDatabaseURL,
        projectId: args.firebaseProjectId,
        storageBucket: args.firebaseStorageBucket,
        messagingSenderId: args.firebaseMessagingSenderId,
        appId: args.firebaseAppId,
      }),
    });

    setUserData({
      id: userDataRequest._id,
      isStaff: true,
      isDev: false,
      info: {
        profile: {
          ...userDataRequest.profile,
        },
      },
    });
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (args.shouldReloadChatStorybook) {
      getData();
    }
  }, [args.shouldReloadChatStorybook]);

  if (!args.loginEmail || !args.loginPassword || !args.endpoint) {
    // If not enough data
    return (
      <span>Please fill in all Controls fields</span>
    );
  }
  if (userData?.id) {
    // If there is all the data
    // @ts-ignore
    return (
      <div>
        <Toast position="top-left" />
        <MainPopover />
        <SecondaryPopover />
        <DrawerTask
          {...args}
          isOpen={isOpen !== null}
          taskId={isOpen}
          app="odetask"
          defaultValues={{
            description: isOpen !== null ? isOpen : '',
          }}
          closeDrawer={() => setIsOpen(null)}
        />
        <input ref={inputRef} />
        <button type="button" onClick={() => $$networksModel.socketSent(JSON.parse(inputRef.current.value))}>
          send to networks
        </button>
        <button type="button" onClick={() => console.log($$networksModel.$outgoingNetworks.getState())}>
          show outgoing networks
        </button>
        <button type="button" onClick={() => console.log($$networksModel.$incomingNetworks.getState())}>
          show incoming networks
        </button>
        <button
          type="button"
          onClick={() => $$projectsModel.events.projectArchived({
            projectId: '5e3bd403-6876-49bd-ada6-3b34b2d51650',
          })}
        >
          archive project
        </button>

        <button
          type="button"
          onClick={() => console.log($$networksModel.deleted('20e74127-7128-45d8-a2b7-32655f6f5737'))}
        >
          Delete network
        </button>

        <button
          type="button"
          onClick={() => $$networksModel.networkAccepted({
            payloadData: {
              userId1: '042203fd-9873-4ed1-90fa-fdfb92b017c6',
              userId2: 'f5327c54-a18d-461f-ac59-7d90e517cefd',
            },
          })}
        >
          accept
        </button>
        <button type="button" onClick={() => setIsOpen('50499364-f29b-46fe-ad76-73f8a7403ae3')}>
          open with description
        </button>
        <button type="button" onClick={() => setIsOpen('MiHqRLgseit4bpn8Z')}>
          open with description another
        </button>

        <button type="button" onClick={() => setIsOpen('')}>
          open without description
        </button>
      </div>
    );
  }
  // If it's loading
  return (
    <BeatLoader color="#F7942A" size={8} />
  );
};
