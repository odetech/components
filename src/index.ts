// Default components export
export {
  Button, ButtonProps,
  Link,
  NavLink,
  Modal, DeleteModal,
  Input,
  InputWithHiddenInfo,
  Toast, showToast,
  Toggle,
  Textarea,
  Select,
  DatePicker,
  UserOptionComponent,
  RadioButton,
} from './components/default-components';
export {
  ModalItemsWrapper, ModalButtonsWrapper, ModalTitle, ModalInfoText, ModalCancelButton, ModalSubmitButton,
} from './components/default-components/Modal/ModalStyles';
// Formik components export
export {
  FormikChipsInput,
  FormikDatePicker,
  FormikField,
  FormikInput,
  FormikInputWithHiddenInfo,
  FormikRadioButtonsGroup,
  FormikSelect,
  FormikTextarea,
  FormikToggleButton,
  FormikErrorTextStyle,
} from './components/formik';
export {
  ChannelsChat,
} from './modules/chat';
export {
  ChannelsChat as IkinariChat,
} from './modules/chat-ikinari';

export * as ikinariModels from './entities/chat/model';
export * as ikinariApi from './entities/chat/api';
export {
  GamificationWidget,
} from './modules/gamification-widget';
export {
  gamificationReducer,
  fetchGamificationData,
} from './modules/gamification-widget/redux/gamification';
export {
  chatsReducer,
  firebaseMessagesDataUpdate,
  fetchChats,
  sendChatMessages,
} from './modules/chat/redux/chats';
export {
  $$timeTrackersModel,
} from '~/entities/time-trackers';
export {
  Sidebar,
} from './components/layout';
export {
  SlateEditor,
  SlateReader,
  getMentionsFromValue,
} from './components/editor';
export { Avatar } from '~/components/avatar/index';
export {
  TimeTrackerPanel,
} from './modules/time-tracker';
export {
  DefaultAppStyles,
  VerticalScrollbarStyles,
  getVerticalScrollbarStyles,
  StyledDropdown, StyledDropdownToggle, StyledDropdownMenu, StyledDropdownItem,
  StyledTooltip,
  Card, CardProps,
  DefaultTheme,
} from './styles';

export * from './styles/types';
export { ThemesKey } from './styles/types';

export {
  filesUpload, sortDates, getInitials, getTimeDiffFromNow, capitalizeWordFirstLetter, getUserName, extractHTMLContent,
  getFormattedDate, getDeepValue, dataURLtoFile, applyJson, ensureUtcTimestamp,
} from './helpers';
export {
  NotificationWidget, $$notificationModel,
} from './modules/notification-widget';

export {
  DrawerTask, createTaskFx,
} from './modules/task-drawer';
export {
  $$themeModel,
} from './entities/theme';
export {
  $$networksModel, NetworkTypes,
} from './entities/networks';
export {
  $$usersModel, UsersTypes,
} from './entities/users';
export {
  $$reactionModel, Reactions,
} from './entities/reactions';
export {
  $$tasksModel,
} from './entities/tasks';
export {
  $$commentsModel, commentsHelpers,
} from './entities/comments';
export {
  $$projectsModel, TProject, mapProject, mapProjects,
} from './entities/projects';
export {
  $$clientsModel, ClientsTypes,
} from './entities/clients';
export {
  $$popoverMain,
  $$popoverSecondary,
  createPopoverFactory,
} from './components/popover/model';

export {
  componentsInit,
  $currentUser,
  currentUserUpdated,
  componentsStarted,
  $api,
} from './config';
