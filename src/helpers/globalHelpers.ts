import imageCompression from 'browser-image-compression';
import { ensureUtcTimestamp } from './dateParser';

export const getTimeDiffFromNow = (date: string) => {
  const thatDate = new Date(ensureUtcTimestamp(date));
  const now = new Date();
  const diffSeconds = (now.getTime() - thatDate.getTime()) / 1000;

  if (diffSeconds >= 31536000) {
    return `${Math.round(diffSeconds / 31536000)} yr ago`;
  } if (diffSeconds >= 2678400) {
    return `${Math.round(diffSeconds / 2678400)} mo ago`;
  } if (diffSeconds >= 86400) {
    return `${Math.round(diffSeconds / 86400)} d ago`;
  } if (diffSeconds >= 3600) {
    return `${Math.round(diffSeconds / 3600)} hr ago`;
  } if (diffSeconds >= 60) {
    return `${Math.round(diffSeconds / 60)} min ago`;
  }

  return diffSeconds < 0 ? 'Just now' : `${Math.ceil(diffSeconds)} sec ago`;
};

const formatAMPM = (date: Date): string => date.toLocaleString(
  'en-US',
  { hour: 'numeric', minute: 'numeric', hour12: true },
);

const getNth = (d: number) => {
  if (d > 3 && d < 21) return 'th';
  switch (d % 10) {
    case 1: return 'st';
    case 2: return 'nd';
    case 3: return 'rd';
    default: return 'th';
  }
};

export const getFormattedDate = (date: string, format?: string): string => {
  let dateObject = null;
  if (date === '') {
    dateObject = new Date();
  } else {
    dateObject = new Date(date);
  }

  const yyyy = dateObject.getFullYear();
  const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const mmm = month[dateObject.getMonth()];
  const dd = dateObject.getDate().toString();
  const time = formatAMPM(dateObject);

  switch (format) {
    case 'time': {
      return time;
    }
    case 'month': {
      return `${mmm} ${dd}${getNth(Number(dd))}`;
    }
    default: {
      return `${mmm} ${dd}${getNth(Number(dd))} ${yyyy}, ${time}`;
    }
  }
};

/**
 * Extracts the raw text from an HTML string.
 *
 * This ensures we're getting the text content only.
 * Any HTML like "<div>hello</div>" or <script> tags
 * will be converted to "&lt;div&gt;hello",
 * which is safe to render.
 */
export const extractHTMLContent = (s: string) => {
  const span = document.createElement('span');
  span.innerHTML = s;

  const rawText = span.textContent || span.innerText;

  const element = document.createElement('div');
  if (rawText) {
    element.textContent = rawText;
  }

  return element.innerHTML;
};

export const getUserName = (user?: any) => {
  if (!user?.profile) return '';

  return `${user.profile.firstName || ''} ${user.profile.lastName || ''}`;
};

export const compressImage = (file: File) => imageCompression(file, {
  maxSizeMB: 0.5,
  maxWidthOrHeight: 1024,
  useWebWorker: true,
});

export const filesUpload = async (files: { file: File, uiId: string }[], url?: string) => {
  const uploadedFiles: { cloudinary: any, uiId: string }[] = [];
  if (files.length) {
    await Promise.all(files.map(async (fileParams) => {
      const formData = new FormData();
      const file = { ...fileParams };
      if (file.file.type?.includes('image') && !file.file.type?.includes('webp') && !file.file.type?.includes('apng')
        && !file.file.type?.includes('avif') && !file.file.type?.includes('mng') && !file.file.type?.includes('flif')
        && !file.file.type?.includes('gif')) {
        // eslint-disable-next-line no-param-reassign
        file.file = await compressImage(file.file);
      }

      formData.append('file', file.file);
      formData.append('upload_preset', 'i5m3S0Aquesf');
      formData.append('folder', 'production/articles');

      let currentUrl = url ?? 'https://api.cloudinary.com/v1_1/https-platform-odecloud-com/image/upload';
      if (file.file?.type?.includes('video')) {
        currentUrl = currentUrl.replace('image', 'video');
      }
      if (!(file.file?.type?.includes('video') || file.file?.type?.includes('image'))) {
        currentUrl = currentUrl.replace('image', 'raw');
      }

      await fetch(currentUrl, {
        method: 'POST',
        body: formData,
      })
        .then((response) => response.text())
        .then((data) => {
          uploadedFiles.push({ cloudinary: JSON.parse(data), uiId: file.uiId });
        });
    }));
  }
  return uploadedFiles;
};

export const sortDates = (a: any, b: any) => {
  const dateA = (a?.createdAt && ensureUtcTimestamp(a.createdAt).replace('+00:00', '')) || new Date();
  const dateB = (b?.createdAt && ensureUtcTimestamp(b.createdAt).replace('+00:00', '')) || new Date();

  return new Date(dateA).getTime() - new Date(dateB).getTime();
};

export const getInitials = (firstName?: string | null, lastName?: string | null) => {
  let initials = '';
  if (firstName) {
    initials = `${firstName.charAt(0)}`;
  }
  if (lastName) {
    initials = `${initials}${lastName.charAt(0)}`;
  }
  if (!firstName && !lastName) {
    initials = 'UU';
  }

  return initials;
};

export const capitalizeWordFirstLetter = (word: string) => word.charAt(0).toUpperCase() + word.slice(1);

export const getDeepValue = (obj: any, path: string) => {
  let value = structuredClone(obj);
  // eslint-disable-next-line no-plusplus
  for (let i = 0, pathPartsArray = path.split('.'), len = pathPartsArray.length; i < len; i++) {
    value = value[pathPartsArray[i]];
  }
  return value;
};

export const dataURLtoFile = (dataUrl: string, filename: string) => {
  const arr = dataUrl?.split(',');
  const mime = arr[0].match(/:(.*?);/)?.[1];
  const bstr = atob(arr[1]);
  let n = bstr.length;
  const u8arr = new Uint8Array(n);

  // eslint-disable-next-line no-plusplus
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
};

export const fromArrayToHash = (arr: any) => arr.reduce((acc: any, obj: any) => {
  acc[obj.id ?? obj._id] = obj;
  return acc;
}, {});

export const applyJson = (data: any, mods: { [x: string]: any; }) => {
  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (const path in mods) {
    let k = data;
    const steps = path.split('.');
    const last = steps.pop();
    // eslint-disable-next-line no-return-assign
    steps.forEach((e) => (k[e] = k[e] || {}) && (k = k[e]));
    // @ts-ignore
    k[last] = mods[path];
  }
  return data;
};

export const wakeUpCheck = (callback: () => void) => {
  let lastTime = (new Date()).getTime();

  setInterval(() => {
    const currentTime = (new Date()).getTime();
    if (currentTime > (lastTime + 2000 * 2)) { // ignore small delays
      callback();
    }
    lastTime = currentTime;
  }, 2000);
};

type TPositionStyles = {
  top?: string;
  bottom?: string;
  left?: string;
  right?: string;
};

export const showCloud = (
  el: HTMLElement,
  text: string,
  timeout = 1000,
  styles: TPositionStyles = {},
) => {
  if (el) {
    const cloud = document.createElement('div');
    cloud.className = 'cloud-element-toast';
    cloud.textContent = text;
    if (styles?.top) {
      cloud.style.top = styles?.top;
    }
    if (styles?.bottom) {
      cloud.style.bottom = styles?.bottom;
    }
    el.appendChild(cloud);
  }
  setTimeout(() => {
    const elements = document.getElementsByClassName('cloud-element-toast');
    while (elements.length > 0) {
      if (elements[0] && elements[0].parentNode) elements[0].parentNode.removeChild(elements[0]);
    }
  }, timeout);
};

export const isValidHex = (hex: string) => {
  const hexRegex = /^#?([A-Fa-f0-9]{3}|[A-Fa-f0-9]{6})$/;
  return hexRegex.test(hex);
};
