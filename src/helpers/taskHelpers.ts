export const PRIORITY_COLORS = [
  {
    hexcode: '#D82626',
    name: 'Critical',
  },
  {
    hexcode: '#D68103',
    name: 'High',
  },
  {
    hexcode: '#10B20D',
    name: 'Medium',
  },
  {
    hexcode: '#38E4B0',
    name: 'Low',
  },
];

export const ACTIVITY_COLORS = [
  {
    hexcode: '#7C4EFF',
    name: 'Backlog',
  },
  {
    hexcode: '#E54367',
    name: 'To do',
  },
  {
    hexcode: '#22A69A',
    name: 'In progress',
  },
  {
    hexcode: '#67738B',
    name: 'Done',
  },
];
