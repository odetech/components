/**
 * Ensures a given timestamp string is in ISO 8601 UTC format by appending 'Z' if it's not already present.
 * DOES NOT modify the timestamp value, just appends the "Z" optionally.
 *
 * @param timestamp - The timestamp string to validate and adjust.
 * @returns The timestamp in UTC format. If the input already includes 'Z', it is returned unchanged.
 *
 * @example
 * ensureUtcTimestamp("2025-01-14T10:00:00"); // "2025-01-14T10:00:00Z"
 * ensureUtcTimestamp("2025-01-14T10:00:00Z"); // "2025-01-14T10:00:00Z"
 */
export function ensureUtcTimestamp(timestamp: string): string {
  if (!timestamp) return '';
  return timestamp.includes('Z') ? timestamp : `${timestamp}Z`;
}
