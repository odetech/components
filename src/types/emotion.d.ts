import { ThemeProps } from '../styles/types';

declare module '@emotion/react' {
  interface Theme {
    brand: ThemeProps['brand'];
    text: ThemeProps['text'];
    background: ThemeProps['background'];
    success: ThemeProps['success'];
    error: ThemeProps['error'];
    info: ThemeProps['info'];
    outline: ThemeProps['outline'];
    visualisation: ThemeProps['visualisation'];
    gradient: ThemeProps['gradient'];
    shadow: ThemeProps['shadow'];
  }
}
