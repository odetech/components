import { createEvent, createEffect, sample } from 'effector';
import { $$socketModel } from '~/entities/ikinari';
import { Socket, Channel } from 'phoenix';
import {
  $projectTeamChannel, messageDeleted, messageEdited, messageGot, messageRead,
} from '../model';
import { TComment } from '../types';
import { teamSentFx, socketSent as teamSocketSent } from '../../team/model';
import { taskCreated, TaskCreatedPayload } from '../../tasks/model';

const teamConnected = createEvent();
const teamDisconnected = createEvent();

const failed = createEvent<unknown>();

export {
  teamConnected,
  teamDisconnected,
};

const connectChannelFx = createEffect((
  {
    socket, userId,
  }:
    { socket: Socket | null, userId: string | null },
) => {
  if (!socket) return null;

  const connectedChannel = socket.channel(`project_team:${userId}`);
  connectedChannel.onError(console.log);
  connectedChannel.join()
    .receive('ok', () => console.log('connected'))
    .receive('error', (e: unknown) => failed(e));
  connectedChannel.on('new_message', (response: { data: TComment }) => {
    messageGot(response.data);
  });
  connectedChannel.on('edit_message', (response: { data: TComment }) => {
    messageEdited(response.data);
  });
  connectedChannel.on('react_message', (response: { data: TComment }) => {
    messageEdited(response.data);
  });
  connectedChannel.on('delete_message', (response: { data: TComment }) => {
    messageDeleted(response.data);
  });
  connectedChannel.on('task_created', async (response: TaskCreatedPayload) => {
    if (response) {
      taskCreated({ payload: response })
    }
  })
  connectedChannel.on('read_message', (response: {
    data: { message: { _id: string }, userId: string, tags: string[] }
  }) => {
    messageRead(response.data);
  });
  return connectedChannel;
});

sample({
  clock: [teamConnected, $$socketModel.$isConnected],
  source: {
    socket: $$socketModel.$socket,
    userId: $$socketModel.$userId,
    isConnected: $$socketModel.$isConnected,
  },
  fn: ({
    socket, userId,
  }) => ({
    socket,
    userId,
  }),
  target: connectChannelFx,
});

sample({
  clock: connectChannelFx.doneData,
  target: $projectTeamChannel,
});

sample({
  clock: connectChannelFx.fail,
  fn: ({ params: { socket }, error }) => ({ socket, e: error }),
  target: createEffect(({ socket }: { socket: Socket | null, e: unknown }) => {
    if (socket) socket.disconnect();
  }),
});

sample({
  clock: [
    connectChannelFx.failData,
    failed,
  ],
  target: createEffect((error: unknown) => console.error(JSON.stringify(error))),
});

sample({
  clock: teamDisconnected,
  source: $projectTeamChannel,
  target: createEffect((channel: Channel | null) => {
    if (!channel) return false;
    channel.leave();
    return true;
  }),
});

sample({
  clock: teamSocketSent,
  source: {
    channel: $projectTeamChannel,
  },
  fn: ({
    channel,
  }, {
    type,
    payload,
    onSuccess,
    onError,
  }) => ({
    channel,
    type,
    payload,
    onSuccess,
    onError,
  }),
  target: teamSentFx,
});

