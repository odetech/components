import { createEvent, sample } from 'effector';
import { spread } from 'patronum';
import { $$channelsModel } from '~/entities/chat/model';
import { mapComment } from '../lib';
import { TComment } from '../types';
import { messageEdited, $lastChangedCommentId, $comments } from '../model';

export const editTaskMessage = createEvent<any>();

sample({
  clock: editTaskMessage,
  fn: (payload) => ({
    type: 'edit_message',
    payload,
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: messageEdited,
  source: $comments,
  fn: (comments, payload) => {
    const result = mapComment(payload);
    const commentsDataCopy: Map<string, TComment> = comments.ref.get(result.tags[0]) || new Map();

    let isReply = false;

    if (result.associatedId && result.tags[0] !== result.associatedId) {
      isReply = true;
    }

    const current = commentsDataCopy.get(result.associatedId);
    if (isReply && current) {
      const threadsCopy = { ...current.threads };
      if (result) {
        threadsCopy[result.id] = {
          ...threadsCopy[result.id],
          ...result,
        };
      }
      commentsDataCopy.set(result.associatedId, {
        ...current,
        threads: threadsCopy,
      });
    } else if (result) {
      commentsDataCopy.set(result.id, {
        ...commentsDataCopy.get(result.id),
        ...result,
      });
    }
    comments.ref.set(result.tags[0], commentsDataCopy);
    return {
      comments: { ref: comments.ref },
      lastChangedCommentId: result.id,
    };
  },
  target: spread({
    targets: {
      lastChangedCommentId: $lastChangedCommentId,
      comments: $comments,
    },
  }),
});
