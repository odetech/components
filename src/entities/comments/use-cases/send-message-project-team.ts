import { createEvent, sample } from 'effector';
import { spread } from 'patronum';
import { $$channelsModel } from '~/entities/chat/model';
import { mapComment } from '../lib';
import { TComment } from '../types';
import { $comments, $lastChangedCommentId, messageGot } from '../model';

export const sentTaskMessage = createEvent<any>();

sample({
  clock: sentTaskMessage,
  fn: (payload) => ({
    type: 'send_message',
    payload,
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: messageGot,
  source: $comments,
  fn: (comments, payload) => {
    const result = mapComment(payload);
    const commentsDataCopy: Map<string, TComment> = comments.ref.get(result.tags[0]) || new Map();
    const current = commentsDataCopy.get(result.associatedId);
    if (result.associatedId && result.tags[0] !== result.associatedId && current) {
      const threadsCopy = { ...current.threads };
      threadsCopy[result.id] = result;
      commentsDataCopy.set(result.associatedId, {
        ...current,
        threads: threadsCopy,
      });
    } else {
      commentsDataCopy.set(result.id, result);
    }
    comments.ref.set(result.tags[0], commentsDataCopy);
    return {
      comments: { ref: comments.ref },
      lastChangedCommentId: result.id,
    };
  },
  target: spread({
    targets: {
      lastChangedCommentId: $lastChangedCommentId,
      comments: $comments,
    },
  }),
});
