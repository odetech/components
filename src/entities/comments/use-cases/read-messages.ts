import { createEvent, sample } from 'effector';
import { spread } from 'patronum';
import { $$channelsModel } from '~/entities/chat/model';
import { $comments, messageRead, $lastChangedCommentId } from '../model';
import { TComment } from '../types';

export const readTaskMessages = createEvent<any>();

sample({
  clock: readTaskMessages,
  fn: (payload) => ({
    type: 'read_messages',
    payload: {
      messageIds: payload,
    },
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: messageRead,
  source: $comments,
  fn: (comments, payload) => {
    const commentsDataCopy: Map<string, TComment> = comments.ref.get(payload.tags[0]) || new Map();
    // @ts-ignore
    for (const el of Array.from(commentsDataCopy.values())) {
      if (el.id === payload.message._id) {
        // @ts-ignore
        commentsDataCopy.set(payload.message._id, {
          ...commentsDataCopy.get(payload.message._id),
          views: Array.from(new Set([...(commentsDataCopy.get(payload.message._id)?.views || []), payload.userId])),
        });

        break;
      }
      if (el.threads?.[payload.message._id]) {
        const threadsCopy = { ...el.threads };
        if (payload.message._id) {
          threadsCopy[payload.message._id] = {
            ...threadsCopy[payload.message._id],
            views: Array.from(new Set([...(threadsCopy[payload.message._id]?.views || []), payload.userId])),
          };
        }
        commentsDataCopy.set(el.id, {
          ...el,
          threads: threadsCopy,
        });
        break;
      }
    }

    comments.ref.set(payload.tags[0], commentsDataCopy);
    return {
      comments: { ref: comments.ref },
      lastChangedCommentId: payload.message._id,
    };
  },
  target: spread({
    targets: {
      lastChangedCommentId: $lastChangedCommentId,
      comments: $comments,
    },
  }),
});
