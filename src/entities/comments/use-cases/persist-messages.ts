import {
  createEffect, createStore, createEvent, sample,
} from 'effector';
import { $savedComments, CommentItem } from '../model';

type PersistRequest = {
  db: PouchDB.Database<CommentItem>;
  data: CommentItem;
};

const clearDbFx = createEffect(async (db: PouchDB.Database<CommentItem>) => {
  const commentsDB = await db.allDocs({ include_docs: true });
  commentsDB.rows.map((row) => row.doc).forEach((commentData: any) => {
    if (!((commentData?.date
      && (new Date().getDate() < new Date().setDate(new Date(commentData.date).getDate() + 7)))
      || commentData.comment !== '<p></p>')) {
      db.get(commentData._id).then((doc) => db.remove(doc._id, doc._rev));
    }
  });
});

const writeToPouchFx = createEffect(async ({ db, data }: PersistRequest) => {
  await clearDbFx(db);
  if (data.comment && data.comment !== '<p></p>') {
    try {
      const doc = await db.get(data._id);
      await db.put({ ...doc, ...data });
    } catch (_) {
      await db.put(data);
    }
  } else {
    try {
      const doc = await db.get(data._id);
      await db.remove(doc);
      // @ts-ignore
    } catch (_) {
      /* ignore */
    }
  }
});

export const updateCommentDB = createEvent<CommentItem>();

const $queue = createStore<CommentItem[]>([])
  .on(updateCommentDB, (queue, doc) => [...queue, doc])
  .on(writeToPouchFx.done, (queue) => queue.slice(1));

sample({
  clock: $queue,
  source: $savedComments,
  filter: (_, queue) => queue.length > 0 && !writeToPouchFx.pending.getState(),
  fn: (db, queue) => ({ db, data: queue[0] }),
  target: writeToPouchFx,
});
