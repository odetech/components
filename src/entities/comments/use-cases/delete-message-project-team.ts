import { createEvent, sample } from 'effector';
import { $$channelsModel } from '~/entities/chat/model';
import { mapComment } from '../lib';
import { messageDeleted, $comments } from '../model';
import { TComment } from '../types';

export const deleteTaskMessage = createEvent<any>();

sample({
  clock: deleteTaskMessage,
  fn: (payload) => ({
    type: 'delete_message',
    payload,
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: messageDeleted,
  source: $comments,
  fn: (comments, payload) => {
    const result = mapComment(payload);
    const copyComments: Map<string, TComment> = comments.ref.get(result.tags[0]) || new Map();
    if (result.associatedId && result.tags[0] !== result.associatedId) {
      const currentComment = Array.from(copyComments.values()).find((comment) => comment.threads?.[result.id]);
      if (currentComment) {
        const copyCurrentComment = { ...currentComment };
        const copyThreads = { ...copyCurrentComment.threads };
        delete copyThreads[result.id];

        copyCurrentComment.threads = copyThreads;
        copyComments.set(currentComment.id, copyCurrentComment);
      }
    } else {
      copyComments.delete(result.id);
    }
    comments.ref.set(result.tags[0], copyComments);
    return { ref: comments.ref };
  },
  target: $comments,
});
