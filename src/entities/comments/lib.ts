import { ensureUtcTimestamp } from '~/helpers';
import { TComment } from './types';

export const fromArrayToHash = (arr: any) => arr.reduce((acc: any, obj: any) => {
  acc[obj.id ?? obj._id] = obj;
  return acc;
}, {});

export const sortCommentsByDate = (
  comment1: TComment,
  comment2: TComment,
) => {
  if (!(comment1?.createdAt || comment2.createdAt)) return 1;
  return new Date(comment1.createdAt).getTime() - new Date(comment2.createdAt).getTime();
};

export const createCommentsWithThreads = (allComments: Map<string, TComment>) => {
  const threads: Record<string, TComment[]> = {};
  const comments: TComment[] = [];
  Array.from(allComments.keys()).forEach((key: string) => {
    const currentComment = allComments.get(key);
    if (currentComment) {
      if (allComments.get(currentComment.associatedId)?.id) {
        if (!Array.isArray(threads[currentComment.associatedId])) {
          threads[currentComment.associatedId] = [];
        }
        threads[currentComment.associatedId].push(currentComment);
      } else {
        comments.push(currentComment);
      }
    }
  });

  comments.sort(sortCommentsByDate);
  return fromArrayToHash(comments.map((comment) => ({
    ...comment,
    threads: fromArrayToHash(threads[comment.id]?.length ? threads[comment.id].sort(sortCommentsByDate) : []),
  })));
};

export const getMentionsFromValue = (value: string) => {
  const document = new DOMParser().parseFromString(value, 'text/html');
  const mentions = document.querySelectorAll('[data-mention]');
  const mentionUserIdList: string[] = [];
  mentions.forEach((mention) => {
    const id = mention.getAttribute('data-id');

    if (id && id !== 'channel') {
      mentionUserIdList.push(id);
    }
  });
  return mentionUserIdList;
};
export const mapComments = (response: any): Map<string, TComment> => {
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  const mappedCommentsData = response.data.map(mapComment);
  const sortedDataByCreatedAt = mappedCommentsData
    .sort((a: any, b: any) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());

  return new Map(Object.entries(fromArrayToHash(sortedDataByCreatedAt)));
};
export const mapComment = ({
  _id,
  files,
  text,
  rawText,
  data,
  createdAt,
  createdBy,
  associatedId,
  likes,
  fun,
  helpful,
  important,
  insightful,
  id,
  views,
  seen,
  done,
  links,
  tags,
  mentions,
}: any): TComment => {
  const { files: dataFiles } = data || {};
  let filesLocal = files;

  if (dataFiles?.length > 0) {
    filesLocal = dataFiles;
  }

  return ({
    id: _id || id,
    value: text,
    rawValue: rawText,
    createdAt: ensureUtcTimestamp(createdAt),
    createdBy,
    associatedId,
    reactions: {
      fun,
      helpful,
      important,
      insightful,
      likes,
      seen,
      done,
    },
    user: {
      name: (data?.createdBy?.profile?.firstName && data?.createdBy?.profile?.lastName)
        ? `${data?.createdBy?.profile.firstName} ${data?.createdBy?.profile.lastName}`
        : '',
      firstName: data?.createdBy?.profile?.firstName ?? '',
      lastName: data?.createdBy?.profile?.lastName ?? '',
      avatarUrl: data?.createdBy?.profile?.avatar?.secureUrl,
      id: data?.createdBy?._id,
    },
    views,
    files: filesLocal,
    links: Array.isArray(links) ? links : [],
    mentions,
    tags,
  });
};
