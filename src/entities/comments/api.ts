import { attach, createEffect } from 'effector';
import OdeCloud from '@odecloud/odecloud';
import { $api } from '~/config/init';
import { TComment } from './types';
import { mapComments, createCommentsWithThreads, mapComment } from './lib';

type FetchProps = { api: OdeCloud, taskId: string };
type CreateEditProps = { api: OdeCloud, payload: any };

const fetchCommentsApiFx = createEffect<FetchProps, Map<string, TComment>>(
  async ({ api, taskId }) => {
    const result = await api.tasks.getConversation(taskId, { limit: 99 });
    return new Map(Object.entries(
      createCommentsWithThreads(mapComments(result.data?.messages || [])),
    ));
  },
);

const createCommentApiFx = createEffect<CreateEditProps, TComment>(
  async ({ api, payload }) => {
    const requestResult = await api.messages.post({ ...payload }, {
      app: 'odetask',
      isChat: false,
    });
    return mapComment(requestResult);
  },
);

const editCommentApiFx = createEffect<CreateEditProps, TComment>(
  // @ts-ignore
  async ({ api, payload }) => {
    if (payload?.deletions?.length || payload?.insertions?.length) {
      await api.messages.patchFiles(
        payload._id,
        {
          message: payload.comment,
          deletions: payload.deletions || [],
          insertions: payload.insertions || [],
        },
        {
          app: 'odetask',
          isChat: false,
        },
      );
    } else {
      await api.messages.patch(
        payload._id,
        payload,
        {
          app: 'odetask',
          isChat: false,
        },
      );
    }
    return mapComment(payload);
  },
);

const deleteCommentApiFx = createEffect<{ id: string, api: OdeCloud, withThread?: boolean }, void>(
  // @ts-ignore
  async ({ api, id }) => {
    await api.messages.delete(id);
  },
);

export const fetchCommentsFx = attach({
  effect: fetchCommentsApiFx,
  source: $api,
  mapParams: (taskId: any, api) => ({ taskId, api: api! }),
});

export const editCommentFx = attach({
  effect: editCommentApiFx,
  source: $api,
  mapParams: ({ payload }: any, api) => ({ payload, api: api! }),
});

export const createCommentFx = attach({
  effect: createCommentApiFx,
  source: $api,
  mapParams: ({ payload }: any, api) => ({ payload, api: api! }),
});

export const deleteCommentFx = attach({
  effect: deleteCommentApiFx,
  source: $api,
  mapParams: ({ id, withThread }: any, api) => ({ id, withThread, api: api! }),
});
