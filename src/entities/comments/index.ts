import type { TComment } from './types';
import { deleteTaskMessage } from './use-cases/delete-message-project-team';
import { editTaskMessage } from './use-cases/edit-message-project-team';
import { sentTaskMessage } from './use-cases/send-message-project-team';
import { updateCommentDB } from './use-cases/persist-messages';
import { readTaskMessages } from './use-cases/read-messages';
import { teamConnected, teamDisconnected } from './use-cases/connect-project-team';
import {
  mapComment, mapComments, getMentionsFromValue, createCommentsWithThreads,
} from './lib';
import * as api from './api';
import * as model from './model';

export const $$commentsModel = {
  ...model,
  events: {
    teamDisconnected,
    teamConnected,
    updateCommentDB,
    deleteTaskMessage,
    editTaskMessage,
    sentTaskMessage,
    readTaskMessages,
  },
  api,
};

export const commentsHelpers = {
  mapComments,
  mapComment,
  getMentionsFromValue,
  createCommentsWithThreads,
};

export type { TComment };
