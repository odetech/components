import {
  createEvent, createStore, sample,
} from 'effector';
import { pending } from 'patronum';
import { Channel } from 'phoenix';
import PouchDB from 'pouchdb';
import {
  createCommentFx, deleteCommentFx, editCommentFx, fetchCommentsFx,
} from './api';
import { TComment } from './types';

type TaskId = string;
type CommentId = string;
type CommentsStore = { ref: Map<TaskId, Map<CommentId, TComment>> }

export type CommentItem = {
  _id: string;
  _rev?: string;
  comment: string;
  date: number;
}

const $comments = createStore<CommentsStore>({ ref: new Map() });
const $lastChangedCommentId = createStore<string | null>(null);

const fetched = createEvent<string>();
const created = createEvent<{ payload: any }>();
const edited = createEvent<{ payload: any }>();
const deleted = createEvent<{ withThread?: boolean, id: string }>();
const $savedComments = createStore(new PouchDB<CommentItem>('comments'));

sample({
  clock: fetched,
  target: fetchCommentsFx,
});

sample({
  clock: fetchCommentsFx.done,
  source: $comments,
  fn: (comments, { params, result }) => {
    comments.ref.set(params, result);
    return { ref: comments.ref };
  },
  target: $comments,
});

const $silentPending = pending({
  effects: [
    createCommentFx, deleteCommentFx, editCommentFx,
  ],
  of: 'some',
});

const $pending = pending({
  effects: [
    fetchCommentsFx,
  ],
  of: 'some',
});

const $projectTeamChannel = createStore<Channel | null>(null);
const messageGot = createEvent<TComment>();
const messageEdited = createEvent<TComment>();
const messageRead = createEvent<{ message: { _id: string }, userId: string, tags: string[] }>();
const messageDeleted = createEvent<TComment>();

export {
  $savedComments,
  $comments,
  $pending,
  $projectTeamChannel,
  $silentPending,
  fetched,
  messageRead,
  created,
  edited,
  deleted,
  messageGot,
  messageEdited,
  messageDeleted,
  $lastChangedCommentId,
};
