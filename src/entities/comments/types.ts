export type TComment = {
  id: string;
  value: string;
  rawValue: string;
  createdAt: string;
  tags: string[];
  createdBy: string;
  commentAssociatedId?: string;
  associatedId: string;
  user: {
    name: string;
    avatarUrl: string;
    firstName: string;
    lastName: string;
    id: string;
  },
  files: any;
  reactions: {
    likes: string[],
    fun: string[],
    seen: string[],
    done: string[],
    helpful: string[],
    important: string[],
    insightful: string[],
  }
  threads?: any,
  views: string[],
  links: string[],
  mentions: string[],
};
