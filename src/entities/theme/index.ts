import {
  combine, createEvent, createStore, sample,
} from 'effector';
import { persist } from 'effector-storage/local';
import { fromArrayToHash } from '~/helpers/globalHelpers';

type Theme = {
  odetask: {
    activities: Record<string, { value: number, color: string, name: string }>,
    priorities: Record<string, { value: number, color: string, name: string }>
  },
  name: string;
  _id: string;
};

const $themes = createStore<Record<string, Theme>>({});

persist({
  store: $themes,
  key: 'themes',
});


const $currentTheme = createStore<string>('main');

const $theme = combine($themes, $currentTheme, (themes, current) => themes[current]);

const themesStarted = createEvent<Theme[]>();

sample({
  clock: themesStarted,
  source: $themes,
  filter: (themes) => Object.keys(themes).length === 0,
  fn: (_, themes: unknown[]) => fromArrayToHash(themes),
  target: $themes,
});

export const $priorities = combine($theme, (theme) => theme?.odetask.priorities ?? {});
export const $activities = combine($theme, (theme) => theme?.odetask.activities ?? {});

export const $priorityOptions = combine($priorities, (priorites) => {
  if (!priorites) return [];
  return Object.values(priorites).map(({ value, name }) => ({
    value,
    label: name,
  }));
});

export const $activityOptions = combine($activities, (activities) => {
  if (!activities) return [];
  return Object.values(activities).map(({ value, name }) => ({
    value,
    label: name,
  }));
});

export const $$themeModel = {
  themesStarted,
  $theme,
  $priorities,
  $activities,
  $priorityOptions,
  $activityOptions,
};
