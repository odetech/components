import { createEffect } from 'effector';
import OdeCloud from '@odecloud/odecloud';
import { ChatDataProps, MemberDataProps, MessageWithMoreDataProps } from '~/entities/chat/types';
import { ensureUtcTimestamp, sortDates } from '~/helpers';
import { MessageDataProps } from '~/modules/chat/types';
import { chatsSorting, getRepliesData } from '../helpers';

export type Requested = {
  userId: string,
  limit?: number,
  pageNumber?: number,
  messagesPageNumber?: number,
  messagesLimit?: number,
}

type RequestWithData = Requested & {
  api: OdeCloud | null;
  chats: Record<string, ChatDataProps>;
  userId?: string;
  allChatRoomsIds: string[];
};

export const fetchChatsFxWithApi = createEffect(async ({
  api,
  userId,
  limit,
  messagesLimit,
  pageNumber,
  messagesPageNumber,
  allChatRoomsIds,
}: RequestWithData) => {
  if (!api) return null;
  let ids = [...allChatRoomsIds];
  /* get ids for allRooms */
  if (ids.length === 0) {
    const allChatRoomsData = await api.tags.getTags({
      // @ts-ignore
      isDirectChat: true,
      members: [userId],
      isPublic: false,
    });

    ids = allChatRoomsData.map((chatRoom: {
      _id: string,
      title: string,
    }) => (chatRoom._id));
  }

  const chatRoomsRequest: {
    data: ChatDataProps[],
    hasNextPage: boolean,
    pageNumber: number,
    pageSize: number,
    total: number,
  } = await api.tags.getChatRooms({
    isDirectChat: true,
    members: [userId],
    isPublic: false,
    data: ['members', 'files'],
    limit: limit || 15,
    pageNumber: pageNumber || 1,
    // @ts-ignore
    messagesPageNumber: messagesPageNumber || 1,
    messagesLimit: messagesLimit || 15,
  });

  const updatedChatsData = await Promise.all(chatRoomsRequest.data.map(async (chatRoom) => {
    const messagesData: MessageWithMoreDataProps[] = chatRoom.data.messages?.data || [];
    const updatedMessagesData = await getRepliesData({
      messages: messagesData,
    }, api);

    return ({
      ...chatRoom,
      data: {
        // members without your account data
        members: chatRoom.data.members?.filter((member: MemberDataProps) => member._id !== userId),
        messages: {
          ...chatRoom.data.messages,
          total: messagesData.length,
          data: updatedMessagesData
            ?.sort(sortDates)
            .map((message: any) => ({
              ...message,
              data: message.data ? {
                ...message.data,
                createdBy: chatRoom.data
                  .members?.find((member: MemberDataProps) => (member._id === message.createdBy)),
              } : {},
            }) || []),
        },
      },
    });
  }));

  const updatedChatData: {
    elements: ChatDataProps[],
    hasNextPage: boolean,
    pageNumber: number,
    pageSize: number,
    total: number,
    allChatRoomsIds?: string[],
  } = {
    elements: chatsSorting(updatedChatsData),
    hasNextPage: chatRoomsRequest.hasNextPage,
    pageNumber: chatRoomsRequest.pageNumber,
    pageSize: chatRoomsRequest.pageSize,
    total: chatRoomsRequest.total,
    allChatRoomsIds: ids,
  };
  return updatedChatData;
});

export const createChatFx = createEffect(async ({
  api,
  userId,
  chatPayload,
  setShowModal,
}: {
  api: OdeCloud | null;
  userId: string | null;
  setShowModal?: (showModal: boolean) => void;
  chatPayload: {
    title: string,
    members: string[],
  };
}) => {
  if (!api) return null;
  const postPayload = {
    owner: userId,
    title: chatPayload.title,
    members: chatPayload.members,
    isDirectChat: true,
    isPublic: false,
    data: ['members'],
  };

  if (userId) {
    postPayload.members.push(userId);
  }

  const postRequest = await api.tags.post(postPayload);

  const newChatData = {
    ...postRequest,
    data: {
      ...postRequest.data,
      members: postRequest.data.members?.filter((member: MemberDataProps) => member._id !== userId),
      messages: postRequest.data.articles || {
        data: [],
        hasNextPage: false,
        pageNumber: 1,
        pageSize: 26,
        total: 0,
      },
    },
  };

  if (setShowModal) {
    setShowModal(false);
  }
  return newChatData;
});

export const editChatFx = createEffect(async ({
  api,
  userId,
  updatedChatData,
}: {
  api: OdeCloud | null;
  userId: string | null;
  updatedChatData: {
    title?: string,
    members: string[],
    _id: string,
  };
}) => {
  if (!api) return null;
  // Payload data
  const patchPayload: {
    title?: string,
    members: string[],
    data: string[]
  } = {
    members: updatedChatData.members.concat(userId || ''),
    data: ['members'],
  };
  if (updatedChatData.title) {
    patchPayload.title = updatedChatData.title;
  }

  return api.tags.patch(updatedChatData._id, patchPayload);
});

export const leaveOrArchiveChatFx = createEffect(async ({
  api,
  userId,
  isArchive,
  activeChat,
}: {
  api: OdeCloud | null;
  userId: string | null;
  isArchive?: boolean;
  activeChat: ChatDataProps | null;
}) => {
  if (!api || !activeChat) return null;
  // Payload data
  const membersWithoutYou: string[] = activeChat?.members
    .filter((item: string) => item !== userId) || [];

  const patchPayload: {
    isArchive?: boolean,
    members?: string[],
  } = {};

  // Leave chat patch payload
  if (!isArchive) {
    patchPayload.members = membersWithoutYou;
  }
  // Archive chat patch payload
  if (isArchive) {
    patchPayload.isArchive = isArchive;
  }

  return api.tags.patch(activeChat?._id, patchPayload);
});

export const searchChatFx = createEffect(async ({
  api,
  userId,
  chats,
  chatSearchType,
  chatSearchInputValue,
  secondMemberId,
}: {
  api: OdeCloud | null;
  userId: string | null;
  chats: Record<string, ChatDataProps>;

  chatSearchType: string,
  chatSearchInputValue: string,
  secondMemberId?: string,
}) => {
  if (!api) return null;
  let filteredChatRooms: ChatDataProps[] = Object.values(chats);
  // Search chat room by title
  if (chatSearchType === 'chatroom' && chatSearchInputValue !== '' && userId) {
    const getChatRoomsPayload: {
      isDirectChat: boolean,
      isPublic: boolean,
      data: string[],
      limit: number,
      pageNumber: number,
      messagesPageNumber: number,
      messagesLimit: number,
      members: string[],
      title?: string,
    } = {
      isDirectChat: true,
      isPublic: false,
      data: ['members', 'files'],
      limit: 50,
      pageNumber: 1,
      messagesPageNumber: 1,
      messagesLimit: 15,
      members: [userId],
    };

    if (secondMemberId) {
      getChatRoomsPayload.members.push(secondMemberId);
    } else {
      getChatRoomsPayload.title = chatSearchInputValue;
    }

    const searchRequest = await api.searches.getSearchQueries({
      // eslint-disable-next-line @typescript-eslint/no-loss-of-precision
      limit: 50,
      searchStr: chatSearchInputValue,
      query: ['tags'],
      // @ts-ignore
      isDirectChat: true,
      isPublic: false,
    });
    const chatRoomsFoundBySearch = searchRequest.tags.values.map((searchValue: {
      data: ChatDataProps[],
    }) => ({
      ...searchValue.data,
    })).filter((chatRoomData: ChatDataProps) => (chatRoomData?.members
      ?.find((memberId) => memberId === userId)
      && (chatRoomData.isArchive !== undefined && !chatRoomData.isArchive)));
    const searchChatRoomsFullDataRequest = await Promise.all(chatRoomsFoundBySearch
      .map(async (chatRoomData: ChatDataProps) => {
        const ChatRoomFullDataRequest = await api.tags.getChatRoom(
          chatRoomData._id,
          {
            isDirectChat: true,
            members: [userId],
            isPublic: false,
            data: ['members', 'files'],
            limit: 15,
            pageNumber: 1,
            messagesLimit: 15,
            // @ts-ignore
            messagesPageNumber: 1,
          },
        );

        return ChatRoomFullDataRequest;
      }));

    const updatedFoundChatRooms = await Promise.all(searchChatRoomsFullDataRequest.map(async (chatRoom) => {
      const updatedMessagesData = await getRepliesData({
        messages: chatRoom.data.messages?.data || [],
      }, api);
      return ({
        ...chatRoom,
        data: {
          // members without your account data
          members: chatRoom.data.members?.filter((member: MemberDataProps) => member._id !== userId),
          messages: {
            ...chatRoom.data.messages,
            data: updatedMessagesData
              ?.sort(sortDates)
              .map((message: any) => ({
                ...message,
                data: message.data ? {
                  ...message.data,
                  createdBy: chatRoom.data
                    .members?.find((member: MemberDataProps) => (member._id === message.createdBy)),
                } : {},
              }) || []),
          },
        },
      });
    }));
    // Find chat rooms by members first name filter
    // const chatRoomsFoundMembers: ChatDataProps[] = [];
    // filteredChatRooms
    //   .forEach((chatRoom) => (chatRoom.data.members?.forEach((member) => {
    //     const foundMemberInfo = member.profile.firstName?.toLowerCase().includes(chatSearchInputValue.toLowerCase())
    //       || member.profile.lastName?.toLowerCase().includes(chatSearchInputValue.toLowerCase()) || "";
    //     if (foundMemberInfo !== "") {
    //       chatRoomsFoundMembers.push(chatRoom);
    //     }
    //   })));
    // filteredChatRooms = chatRoomsFoundTitle.concat(chatRoomsFoundMembers)
    //   .filter((chatRoom, index, array) => (array.indexOf(chatRoom) === index));
    filteredChatRooms = chatsSorting(updatedFoundChatRooms);
  } else if (chatSearchType === 'message' && chatSearchInputValue !== '') { // Search chat room by message
    const foundMessagesRequest = await api.messages.search({
      // @ts-ignore
      origin: 'odesocial',
      search: chatSearchInputValue,
      isDirectChat: true,
      isPublic: false,
    });
    const foundMessagesChatRooms: ChatDataProps[] = foundMessagesRequest.data.map((message: MessageDataProps) => ({
      messageSearchInfo: {
        _id: message._id,
        rawText: message.rawText,
        filesLength: message.data?.files?.length || 0,
        createdAt: message.createdAt,
        tag: message.tags[0],
        isAlreadyLoaded: !!filteredChatRooms.find((chatroom) => (chatroom.data.messages?.data
          .find((chatroomMessage) => (chatroomMessage._id === message._id)))),
      },
      ...filteredChatRooms.find((chatRoom) => (chatRoom._id === message.tags[0])),
    }));

    // Sort by message createdAt
    filteredChatRooms = foundMessagesChatRooms.sort((a, b) => {
      let dateA = '';
      let dateB = '';
      if (a.messageSearchInfo && b.messageSearchInfo) {
        dateA = ensureUtcTimestamp(a.messageSearchInfo.createdAt).replace('+00:00', '');
        dateB = ensureUtcTimestamp(b.messageSearchInfo.createdAt).replace('+00:00', '');
      }

      return new Date(dateB).getTime() - new Date(dateA).getTime();
    });
  }
  return filteredChatRooms;
});
