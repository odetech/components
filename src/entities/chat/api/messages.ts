import { createEffect } from 'effector';
import OdeCloud from '@odecloud/odecloud';
import { isBefore } from 'date-fns';
import { sortDates } from '~/helpers';
import { getRepliesData } from '../helpers';
import { ChatDataProps } from '../types';

export type Read = {
  currentUserId: string,
  messagesIds: string | string[],
}

export const fetchMessagesFx = createEffect(async ({
  createdAt,
  activeChat,
  api,
  chatId,
}: {
  createdAt?: string;
  chatId?: string;
  api: OdeCloud | null,
  activeChat: ChatDataProps | null;
}) => {
  const currentChatId = chatId || activeChat?._id;
  if (!currentChatId || !api) return null;
  const requestBody: {
    tags: string[],
    data: string[],
    loadBy: string[],
    limit?: number,
    createdAtEnd?: string,
    page?: number
  } = {
    tags: [currentChatId],
    data: ['files'],
    loadBy: ['chat'],
  };
  if (createdAt && activeChat) {
    if (activeChat.data.messages?.data[0].createdAt
      && isBefore(new Date(createdAt), new Date(activeChat.data.messages.data[0].createdAt))) {
      return null;
    }
    requestBody.createdAtEnd = createdAt;
    requestBody.limit = 10000;
  } else {
    if (activeChat) {
      requestBody.page = Math.ceil((activeChat.data.messages?.data.length || 0) / 15) + 1;
    } else {
      requestBody.page = 1;
    }
    requestBody.limit = 15;
  }
  const chatMessagesData = await api.messages.getMessages({
    // @ts-ignore
    origin: 'odesocial',
    tags: [currentChatId],
    limit: 15,
    pageNumber: activeChat ? (Math.ceil((activeChat.data.messages?.data.length || 0) / 15) + 1) : 1,
    data: ['files'],
    loadBy: ['chat'],
  });
  const chatMessagesUpdatedData = await getRepliesData({
    messages: chatMessagesData.data,
  }, api);
  chatMessagesUpdatedData?.sort(sortDates);
  // @ts-ignore
  return {
    data: chatMessagesUpdatedData,
    hasNextPage: chatMessagesData.hasNextPage,
  };
});
