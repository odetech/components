import { useEffect } from 'react';
import { useFormikContext } from 'formik';
import { getBase64 } from '~/components/editor/slate-editor.image-helpers';
import { ChatMessageWithMoreDataProps } from '~/modules/chat/types';
import { MessageWithMoreDataProps } from './types';

// export const imagePreviewButtonWrapper = (content: string, id: string) => {
//   const regex = /<img [^>]*src="[^"]*"[^>]*>/gi;
//   const regexPdf = /<img [^>]*src="[^"]*.+\.pdf"[^>]*>/gi;
//
//   let formattedContent = '';
//
//   formattedContent = content.replace(
//     regex,
//     ((match, group) => {
//       if (regexPdf.test(match)) return match;
//
//       return (
//         `<button
//           style="background: unset !important; border: unset;"
//           type="button"
//           class="${id}"
//         >
//           ${match.replace(group, '')}
//         </button>`
//       );
//     }),
//   );
//
//   formattedContent = formattedContent.replace(
//     regexPdf,
//     ((match) => {
//       const regexImg = /<img[^>]+src="(http:\/\/[^">]+)"/g;
//       const regexImgAlt = /<img[^>]+alt="([^">]+)"/g;
//       const imgAltGroup = regexImgAlt.exec(match);
//       const imgName = imgAltGroup?.[1];
//       const imgFromRegex = regexImg.exec(match);
//
//       return (
//         `<button
//           style="background: unset !important; border: unset;"
//           type="button"
//           class="${id}Pdf content-pdf-button"
//           data-pdf="${imgFromRegex?.[1]}"
//         >
//           <img width="100px" src="${pdfIcon}" alt="${imgAltGroup}" /><span>${imgName || ''}</span>
//         </button>`
//       );
//     }),
//   );
//
//   return formattedContent;
// };

// TODO delete when api sorting is added
export const chatsSorting = (chats: any[]) => chats.sort((a:any, b:any) => {
  // if there are no messages yet then use a date of the chat's creation
  const aDate = a.data.messages.data[a.data.messages.data.length - 1]?.createdAt || a.createdAt;
  const bDate = b.data.messages.data[b.data.messages.data.length - 1]?.createdAt || b.createdAt;
  if (new Date(aDate) < new Date(bDate)) return 1;
  return -1;
});

export const getUserSuggestions = (networksConnections: {
  _id: string,
  profile: {
    firstName: string,
    lastName: string,
  },
} []) => {
  if (!networksConnections) {
    return {};
  }

  const result: any = {};

  networksConnections.forEach((user: any) => {
    result[user._id] = {
      profile: user.profile,
    };
  });

  return result;
};

export const getAssociatedData = (message: MessageWithMoreDataProps) => {
  let preview;

  const filesLength = message.data?.files?.length || message.files?.length || 0;

  if (filesLength) {
    preview = `🖼️ ${message.rawText || ''}`;
  } else {
    preview = message.rawText || '';
  }

  return ({
    previewText: preview,
  });
};
export const AutoSaveDraftMessageInChatRoom = ({
  chatRoomId,
}: {
  chatRoomId: string,
}) => {
  const { values }: {
    values: {
      message: string,
      files: {
        file: File,
        uiId: string,
      }[],
      uiIds: string[],
    }
  } = useFormikContext();

  useEffect(() => {
    (async () => {
      const chatRoomsDraftData: {
        chatRoomsDrafts?: {
          id: string,
          message: string,
          files: any[],
          uiIds: string[],
        }[],
      } = JSON.parse(localStorage.getItem('chatRoomsDraftData') || 'null') || {
        chatRoomsDrafts: [],
      };

      // Files update to base64
      const base64Files = values.files.length && values.message.includes('data-name') ? await Promise.all(values.files
        .map(async (file) => {
          const base64File = await getBase64(file.file);
          return {
            file: base64File,
            uiId: file.uiId,
            fileName: file.file.name,
          };
        })) : [];

      const data: {
        id: string,
        message: string,
        files: any[],
        uiIds: string[],
      } = {
        id: chatRoomId,
        message: values?.message || '',
        files: base64Files,
        uiIds: values?.uiIds,
      };
      const chatRoomIndex = chatRoomsDraftData?.chatRoomsDrafts
        ?.findIndex((chatRoom) => (chatRoom.id === chatRoomId));

      if (chatRoomsDraftData?.chatRoomsDrafts && chatRoomIndex !== undefined && chatRoomIndex !== -1) {
        if (data.uiIds.length === 0 && data.files.length === 0 && data.message === '<p></p>') {
          chatRoomsDraftData.chatRoomsDrafts.splice(chatRoomIndex, 1);
        } else {
          chatRoomsDraftData.chatRoomsDrafts[chatRoomIndex] = data;
        }
      } else if (chatRoomsDraftData?.chatRoomsDrafts
        && ((data.uiIds.length !== 0 && data.files.length !== 0) || data.message !== '<p></p>')) {
        chatRoomsDraftData.chatRoomsDrafts.push(data);
      }

      localStorage.setItem('chatRoomsDraftData', JSON.stringify(chatRoomsDraftData));
    })();
  }, [values]);
  return null;
};

export const getReplyData = async ({
  message,
  allMessages,
}: {
  message: MessageWithMoreDataProps,
  allMessages: MessageWithMoreDataProps[],
}, api: any) => {
  let replyData;

  try {
    if (message.associatedId) {
      // Find Reply data in chat room data
      replyData = allMessages.find((messageData) => (messageData._id === message.associatedId));

      // Request and save Reply data if it hasn't been found up to this point
      if (!replyData) {
        const replyDataRequest = await api.messages.getMessage(
          message.associatedId,
          {
            data: ['createdBy', 'files'],
            origin: 'odesocial',
          },
        );

        replyData = replyDataRequest;
      }
    } else {
      replyData = null;
    }
  } catch (e) {
    // Handling the error of the request to receive Reply data
    replyData = {
      _id: message.associatedId,
      text: '<span>Deleted message</span>',
      rawText: 'Deleted message',
    };
  }

  if (replyData) {
    replyData = {
      ...replyData,
      ...getAssociatedData(replyData),
      data: {
        ...replyData.data,
        files: replyData.files,
      },
    };
  }

  const updatedMessage: ChatMessageWithMoreDataProps = {
    ...message,
    associatedMessageData: replyData,
  };

  return updatedMessage;
};

export const getRepliesData = async ({
  messages,
}: {
  messages: MessageWithMoreDataProps[],
}, api: any) => {
  const updatedMessages: ChatMessageWithMoreDataProps[] = [];

  const receivedRepliesCache: MessageWithMoreDataProps[] = [];

  await Promise.all(messages.map(async (message: MessageWithMoreDataProps) => {
    let replyData;

    try {
      if (message.associatedId) {
        // Find Reply data in chat room data
        replyData = messages.find((messageData) => (messageData._id === message.associatedId));

        // Find Reply data in previous requests for Reply data
        if (!replyData) {
          replyData = receivedRepliesCache.find((messageData) => (messageData._id === message.associatedId));
        }

        // Request and save Reply data if it hasn't been found up to this point
        if (!replyData) {
          const replyDataRequest = await api.messages.getMessage(
            message.associatedId,
            {
              data: ['createdBy', 'files'],
              origin: 'odesocial',
            },
          );

          // Cache Reply request data
          receivedRepliesCache.push(replyDataRequest);

          replyData = replyDataRequest;
        }
      } else {
        replyData = null;
      }
    } catch (e) {
      // Handling the error of the request to receive Reply data
      replyData = {
        _id: message.associatedId,
        text: '<span>Deleted message</span>',
        rawText: 'Deleted message',
      };
    }

    if (replyData) {
      replyData = {
        ...replyData,
        ...getAssociatedData(replyData),
        data: {
          ...replyData.data,
          files: replyData.files,
        },
      };
    }

    updatedMessages.push({
      ...message,
      associatedMessageData: replyData,
    });
  }));

  return updatedMessages;
};
