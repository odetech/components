import { pending } from 'patronum';
import { createEffect, createStore, sample } from 'effector';
import {
  fetchChatsFx,
} from './chats';

import {
  createChatFx, editChatFx,
  fetchMessagesFx, leaveOrArchiveChatFx, searchChatFx,
} from '../api';

export const $loading = pending({
  effects: [
    fetchChatsFx, createChatFx, editChatFx,
    fetchMessagesFx, leaveOrArchiveChatFx, searchChatFx,
  ],
  of: 'some',
});

export const $error = createStore<Error | null>(null);

const errorHandlerFx = createEffect(console.error);

sample({
  clock: [
    fetchChatsFx.failData, createChatFx.failData, editChatFx.failData,
    fetchMessagesFx.failData, leaveOrArchiveChatFx.failData, searchChatFx.failData,
  ],
  target: [errorHandlerFx, $error],
});
