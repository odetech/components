import { createEffect, sample } from 'effector';
import { $$messagesModel } from './messages';

sample({
  clock: $$messagesModel.$unreadMessagesLength,
  target: createEffect((unreadMessagesLength: number) => {
    const favicon = document.getElementById('favicon') as HTMLLinkElement;

    if (favicon) {
      if (unreadMessagesLength) {
        favicon.href = `${process.env.PUBLIC_URL}/img/main/OdeSocialIcon.svg`;
      } else {
        favicon.href = `${process.env.PUBLIC_URL}/img/main/OdeSocialIcon.svg`;
      }
    }
  }),
});
