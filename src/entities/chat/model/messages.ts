import {
  combine,
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { extractHTMLContent, filesUpload } from '~/helpers';
import { getMentionsFromValue } from '~/components/editor';
import { getAssociatedData } from '~/modules/chat/helpers';
import { ChatDataProps } from '~/modules/chat/types';
import { $$socketModel } from '~/entities/ikinari';
import { $api, $currentUser } from '~/config';
import { $$reactionModel } from '~/entities/reactions/model';
import { $$chatsModel } from './chats';
import { $$channelsModel, unreadMessageCount } from './channels';
import { ChatMessageWithMoreDataProps } from '../types';
import { $$chatModel } from './chat';
import { fetchMessagesFx, Read } from '../api';

type Deleted = {
  _id: string,
  withoutRequest: boolean,
}

type Searched = {
  tagId: string,
  messageCreatedAtDate: string,
}

type Sent = {
  associatedId?: string,
  message: string,
  files: { file: File, uiId: string }[],
  uiIds: string[],
}

type QueueItem = {
  type: string;
  payload: Record<string, unknown>;
  channelName: string | null;
  ts: string | number;
}

const $queue = createStore<QueueItem[]>([]);

const queuePush = createEvent<QueueItem>();
const queuePop = createEvent<string | number>();

$queue.on(queuePush, (state, item) => [...state, item]);
$queue.on(queuePop, (state, idx) => state.filter((t) => t.ts !== idx));

const $unreadMessages = createStore<Record<string, ChatMessageWithMoreDataProps[]>>({});

const $unreadMessageCount = createStore<Record<string, {
  total: number,
  tags: any,
}>>({});

const $unreadMessagesLength = combine(
  $unreadMessages,
  (values) => Object.values(values).reduce((acc, c) => acc + c.length, 0),
);

sample({
  clock: $$chatsModel.$usersChats, // if we have new messages
  source: $currentUser,
  filter: (user) => Boolean(user?._id),
  fn: (user, chats: Record<string, ChatDataProps>) => {
    const unreadMessages: Record<string, ChatMessageWithMoreDataProps[]> = {};
    Object.values(chats).forEach((chat) => {
      chat.data.messages?.data.forEach((message) => { // do loop by all messages for each tag
        // @ts-ignore
        if (message?.views && message?.views.findIndex((m) => m === user._id) === -1) { // find unread messages
          if (!unreadMessages[chat._id]) {
            unreadMessages[chat._id] = [];
          }
          // @ts-ignore
          unreadMessages[chat._id].push(message);
        }
      });
    });
    return unreadMessages;
  },
  target: $unreadMessages,
});

const requested = createEvent<{
  chatId: string;
  createdAt?: string;
}>();
const searched = createEvent<Searched>();
const read = createEvent<Read>();
const sent = createEvent<Sent>();
const sentReply = createEvent<Sent>();
const edited = createEvent<ChatMessageWithMoreDataProps>();
const deleted = createEvent<Deleted>();

export const $$messagesModel = {
  $unreadMessages,
  $unreadMessageCount,
  requested,
  searched,
  edited,
  $unreadMessagesLength,
  read,
  sent,
  sentReply,
  deleted,
};

// region effects
export const sendMessageFx = createEffect(async ({
  channelName,
  values,
  userId,
  userIds,
  activeChat,
}: {
  channelName: string | null,
  userId: string | null,
  userIds: string[],
  values: Sent,
  activeChat: ChatDataProps | null,
}) => {
  let previewText = {};
  // upload images
  let uploadedFiles: { cloudinary: any, uiId: string }[] = [];
  if (values.files?.length) {
    const uniqUnids: string[] = [];
    const uniqFiles: { file: File, uiId: string }[] = [];
    values.files.filter((file) => values.uiIds.includes(file.uiId)).forEach((file) => {
      if (!uniqUnids.includes(file.uiId)) {
        uniqFiles.push(file);
        uniqUnids.push(file.uiId);
      }
    });
    uploadedFiles = await filesUpload(uniqFiles);
    uploadedFiles = uploadedFiles.map((file) => ({
      _id: null,
      associatedId: null,
      createdBy: userId,
      createdAt: null,
      ...file,
    }));
  }

  if (values.associatedId) {
    const associatedData = activeChat?.data
      .messages?.data.find((msg) => msg._id === values.associatedId);

    if (associatedData) {
      previewText = getAssociatedData(associatedData);
    }
  }

  // prepare message
  const formattedMessage: any = {
    text: values.message.replace(/src="(data[^"]+?)"/g, 'src=""'),
    createdBy: userId!,
    isChat: true,
    ...previewText,
    associatedId: values.associatedId || null,
    app: 'chat',
    secondLevel: null,
    isDraft: false,
    files: uploadedFiles,
    rawText: extractHTMLContent(values.message),
    mentions: getMentionsFromValue(values.message, userIds),
    ts: Date.now(),
    tags: [channelName!],
    projectId: null,
    followers: [],
  };
  return {
    type: 'send_message',
    payload: formattedMessage,
    channelName,
    ts: formattedMessage.ts,
  };
});

export const editMessageFx = createEffect(async ({
  channelName,
  values,
  userId,
  userIds,
}: {
  channelName: string | null,
  userIds: string[],
  userId: string | null,
  values: ChatMessageWithMoreDataProps,
}) => {
  // c// upload images
  let uploadedFiles: { cloudinary: any, uiId: string }[] = [];
  if (values.files?.length) {
    const uniqUnids: string[] = [];
    const uniqFiles: { file: File, uiId: string }[] = [];
    const uids: string[] = values.files.map((t) => t.uiId);
    values.files.filter((file) => uids.includes(file.uiId) && values.text.includes(file.uiId)).forEach((file) => {
      if (!uniqUnids.includes(file.uiId)) {
        uniqFiles.push(file);
        uniqUnids.push(file.uiId);
      }
    });
    uploadedFiles = await filesUpload(uniqFiles);
    uploadedFiles = uploadedFiles.map((file) => ({
      _id: null,
      associatedId: values._id,
      createdBy: userId,
      createdAt: null,
      ...file,
    }));
  }
  const messageData = {
    ...values,
    text: values.text,
    files: uploadedFiles,
    isChat: true,
    app: 'chat',
    secondLevel: null,
    isDraft: false,
    rawText: extractHTMLContent(values.text),
    mentions: getMentionsFromValue(values.text, userIds),
    tags: [channelName],
    projectId: null,
    followers: [],
    _id: values._id,
  };
  return {
    type: 'edit_message',
    payload: {
      ...messageData,
    },
    channelName,
    ts: messageData.ts || messageData._id,
  };
});

export const readMessageFx = createEffect(async ({
  channelName,
  messagesIds,
}: {
  channelName: string | null,
  messagesIds: string | string[],
}) => ({
  type: 'read_messages',
  payload: {
    messageIds: Array.isArray(messagesIds) ? messagesIds : [messagesIds],
  },
  channelName,
}));

export const deletedMessageFx = createEffect(async ({
  channelName, payload,
}: {
  channelName: string | null;
  payload: Deleted;
}) => ({
  type: 'delete_message',
  channelName,
  payload: {
    _id: payload._id,
  },
  ts: payload._id,
}));
// end region
sample({
  clock: sent,
  source: {
    activeChat: $$chatModel.$activeChat,
    suggestionsModel: $$chatModel.$suggestionsModel,
    userId: $$socketModel.$userId,
  },
  fn: ({
    activeChat, userId, suggestionsModel,
  }, payload) => ({
    channelName: activeChat?._id || null,
    userIds: suggestionsModel.userIds,
    userId,
    activeChat,
    values: payload,
  }),
  target: sendMessageFx,
});

sample({
  clock: edited,
  source: {
    activeChat: $$chatModel.$activeChat,
    userId: $$socketModel.$userId,
    suggestionsModel: $$chatModel.$suggestionsModel,
  },
  fn: ({ userId, activeChat, suggestionsModel }, payload) => ({
    values: payload,
    userId,
    userIds: suggestionsModel.userIds,
    channelName: activeChat?._id || null,
  }),
  target: editMessageFx,
});

sample({
  clock: deleted,
  source: {
    activeChat: $$chatModel.$activeChat,
  },
  fn: ({ activeChat }, payload) => ({
    payload,
    channelName: activeChat?._id || null,
  }),
  target: deletedMessageFx,
});

sample({
  clock: [
    sendMessageFx.doneData,
    editMessageFx.doneData,
    deletedMessageFx.doneData,
  ],
  target: queuePush,
});

sample({
  clock: $$channelsModel.$channel,
  source: $queue,
  filter: (queue) => queue.length > 0,
  target: createEffect((queue: QueueItem[]) => {
    queue.forEach($$channelsModel.socketSent);
  }),
});

sample({
  // @ts-ignore
  clock: queuePush,
  target: $$channelsModel.socketSent,
});

sample({
  clock: readMessageFx.doneData,
  target: $$channelsModel.socketSent,
});

// for optimistic update
sample({
  clock: sendMessageFx.doneData,
  fn: (data) => data.payload as ChatMessageWithMoreDataProps,
  target: $$channelsModel.messageGot,
});

// region fetch old messages
sample({
  clock: requested,
  source: {
    api: $api,
    activeChat: $$chatModel.$activeChat,
  },
  fn: ({ api, activeChat }, {
    chatId, createdAt,
  }) => ({
    activeChat,
    api,
    chatId,
    createdAt,
  }),
  target: fetchMessagesFx,
});

sample({
  clock: fetchMessagesFx.done,
  source: { chats: $$chatsModel.$usersChats, activeChatId: $$chatModel.$activeChatId },
  fn: ({ chats, activeChatId }, { params, result }) => {
    const currentChatId = params.chatId || activeChatId;
    if (!currentChatId || !result) return { chat: null };
    const copyChat: any = structuredClone(chats[currentChatId]);
    // insert to first places because sorting
    copyChat.data.messages.data.unshift(...result.data);
    copyChat.data.messages.hasNextPage = result.hasNextPage;
    return {
      _id: copyChat._id,
      chat: copyChat,
    };
  },
  target: $$chatsModel.updated,
});

// read message, get ids messages which should be read, and send it for each message edit.
sample({
  clock: read,
  source: $$chatModel.$activeChat,
  fn: (activeChat, readMessagesData) => {
    if (!activeChat) {
      return {
        channelName: null,
        messagesIds: [],
      };
    }
    return {
      channelName: activeChat?._id || null,
      messagesIds: readMessagesData.messagesIds,
    };
  },
  target: readMessageFx,
});

sample({
  clock: [$$channelsModel.messageGot, $$channelsModel.messageEdited, $$channelsModel.messageDeleted],
  filter: (data) => Boolean(data._id),
  fn: (data) => data.ts || data._id,
  target: queuePop,
});

sample({
  clock: $$channelsModel.getUnreadMessages,
  source: $unreadMessages,
  fn: (unreads, data) => {
    const unreadMessages: Record<string, ChatMessageWithMoreDataProps[]> = structuredClone(unreads);
    data.forEach((message) => {
      const tag = message.tags[0];
      const currentMessages = unreadMessages[tag] || [];
      currentMessages.push(message);
      const messagesObject = currentMessages.reduce((acc: any, obj: any) => {
        acc[obj._id ?? obj.ts] = obj;
        return acc;
      }, {});
      unreadMessages[tag] = Object.values(messagesObject);
    });
    return unreadMessages;
  },
  target: $unreadMessages,
});

sample({
  clock: $$reactionModel.reacted,
  fn: ({ messageId, reaction }) => ({
    type: 'react_message',
    payload: {
      reaction: reaction.label,
      messageId,
      action: reaction.value ? 'react' : 'unreact',
    },
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: unreadMessageCount,
  target: $$messagesModel.$unreadMessageCount,
});
