import {
  attach,
  combine, createEffect, createEvent, createStore, sample,
} from 'effector';
import { spread } from 'patronum';
import OdeCloud from '@odecloud/odecloud';
import { trackNetworkStatus } from '@withease/web-api';
import { persist } from 'effector-storage/broadcast';
import { ensureUtcTimestamp, sortDates } from '~/helpers';
import { $$socketModel } from '~/entities/ikinari';
import { $$usersModel } from '~/entities/users';
import { $$messagesModel } from '~/entities/chat/model/messages';
import { $api, $currentUser, componentsStarted } from '~/config';
import { wakeUpCheck } from '~/helpers/globalHelpers';
import { getReplyData } from '../helpers';
import { $$channelsModel } from './channels';
import { ChatDataProps, ChatMessageWithMoreDataProps } from '../types';
import {
  Requested, fetchChatsFxWithApi, searchChatFx,
} from '../api';

type Search = {
  chatSearchType: string;
  chatSearchInputValue: string;
  secondMemberId?: string;
}

type Payload = {
  chats: Record<string, ChatDataProps>;
  data: ChatMessageWithMoreDataProps;
  api: OdeCloud | null;
}

type PayloadRead = {
  chats: Record<string, ChatDataProps>;
  data: { userId: string, message: { _id: string }, tags: string[] };
}

const $usersChats = createStore<Record<string, ChatDataProps>>({}, { name: 'usersChats' });
persist({ store: $usersChats, key: 'currentChats' });
const $searchedChats = createStore<ChatDataProps[]>([], { name: 'searchedChats' });
const $allChatRoomsIds = combine($usersChats, (chats) => Object.keys(chats));
const $chatRoomsIds = createStore<string[]>([]);
const chatRoomsIdsAdded = createEvent<string>();
const chatRoomsIdsRemoved = createEvent<string>();
$chatRoomsIds
  .on(chatRoomsIdsAdded, (state, newRoom) => [...state, newRoom])
  .on(chatRoomsIdsRemoved, (state, oldRoom) => state.filter((t) => t !== oldRoom));

const $hasNextPage = createStore<boolean>(false);
const $page = createStore<number>(1);

const $unreadMessagesCounterLength = createStore<Record<string, number>>({});
const unreadMessagesUpdated = createEvent<Record<string, number>>();

$unreadMessagesCounterLength.on(unreadMessagesUpdated, (_, values) => values);

const requested = createEvent<Requested>();
const refetched = createEvent();
const searched = createEvent<Search>();
const updated = createEvent<{ _id?: string, chat: ChatDataProps | null }>();
const userChatsUpdated = createEvent<Record<string, ChatDataProps>>();
const newChatAdded = createEvent<{ isNew: boolean, _id: string }>();
const updatedChat = createEvent<{ type: string, data: ChatDataProps }>();
const deletedChat = createEvent<string>('deletedChat');

const {
  online,
} = trackNetworkStatus({
  setup: componentsStarted,
});

const $chats = combine($usersChats, $searchedChats, (chats, searches) => {
  if (Object.keys(searches).length !== 0) {
    return searches;
  }
  return chats;
});

export const $$chatsModel = {
  requested,
  searched,
  updated,
  deletedChat,
  $unreadMessagesCounterLength,
  newChatAdded,
  $chats,
  $hasNextPage,
  $page,
  $usersChats,
  $allChatRoomsIds,
  $searchedChats,
};

export const fetchChatsFx = attach({
  source: {
    api: $api,
    userId: $$socketModel.$userId,
    chats: $usersChats,
    allChatRoomsIds: $allChatRoomsIds,
  },
  effect: fetchChatsFxWithApi,
  mapParams: (params: Requested, {
    userId,
    api,
    chats,
    allChatRoomsIds,
  }) => ({
    ...params,
    api,
    chats,
    userId: userId!,
    allChatRoomsIds,
  }),
});

sample({
  clock: requested,
  source: $api,
  filter: (api) => api !== null,
  fn: (_, params) => params,
  target: fetchChatsFx,
});

sample({
  clock: fetchChatsFx.doneData,
  source: $usersChats,
  fn: (chats, data) => {
    if (!data) return chats;
    const newChats = {
      ...chats,
    };
    data.elements.forEach((element) => {
      const copyElement = { ...element };
      // @ts-ignore
      copyElement.data.messages.data.sort(sortDates); // should be sorted, because messages doesn't sort on backend
      newChats[element._id] = copyElement;
    });
    return {
      chats: newChats,
      channels: newChats,
      page: data.pageNumber,
      hasNextPage: data.hasNextPage,
      ids: data.allChatRoomsIds,
    };
  },
  target: spread({
    targets: {
      chats: $usersChats,
      page: $page,
      hasNextPage: $hasNextPage,
      ids: $chatRoomsIds,
    },
  }),
});

sample({
  clock: $chatRoomsIds,
  fn: (data) => ({
    type: 'get_unread_message_count_in_tags',
    payload: {
      tagIds: data,
    },
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: $chatRoomsIds,
  fn: () => ({
    type: 'get_unread_messages',
    payload: {},
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: [$chatRoomsIds, $$channelsModel.messageGot, $$channelsModel.messageDeleted, $$channelsModel.getReadMessage],
  fn: () => ({
    type: 'get_unread_message_count',
    payload: {},
  }),
  target: $$channelsModel.socketSent,
});

const addMessageFx = createEffect(async ({
  chats,
  data,
  api,
}: Payload) => {
  const newChats = {
    ...chats,
  };
  const chat = newChats[data.tags[0]];
  if (chat) {
    const fixedDate = data.createdAt ? (ensureUtcTimestamp(data.createdAt)) : data.ts;
    const createdAt = new Date(fixedDate!).toISOString().replace('Z', '');
    let updatedMessage: any = {
      ...data,
      createdAt,
    };
    updatedMessage = await getReplyData(
      {
        message: updatedMessage,
        allMessages: chat.data.messages.data || [],
      },
      api,
    );
    if (data.ts) {
      const messageIndex = chat.data.messages.data.findIndex((m) => m.ts === data.ts);
      if (messageIndex !== -1) {
        chat.data.messages.data[messageIndex] = updatedMessage;
        return newChats;
      }
    }
    chat.data.messages.data.push(updatedMessage);
  }
  return newChats;
});

const readMessageFx = createEffect(async ({
  chats,
  data,
}: PayloadRead) => {
  const newChats = {
    ...chats,
  };
  const chat = newChats[data.tags[0]];
  if (chat && data.message._id) {
    const messageIndex = chat.data.messages.data.findIndex((m) => m._id === data.message._id);
    if (messageIndex !== -1) {
      chat.data.messages.data[messageIndex].views = Array.from(new Set([
        ...chat.data.messages.data[messageIndex].views,
        data.userId,
      ]));
      return newChats;
    }
  }
  return newChats;
});

const deleteMessageFx = createEffect(({ chats, data }: any) => {
  const newChats = {
    ...chats,
  };

  const chat = newChats[data.tags[0]];
  if (chat && data._id) {
    // @ts-ignore
    chat.data.messages.data = chat.data.messages.data.filter((message) => message._id !== data._id);
    newChats[data.tags[0]] = chat;
  }
  return newChats;
});

const updateChatFx = createEffect(async ({
  payload, users, chats, type,
}: {
  payload: ChatDataProps;
  users: Record<string, any>;
  type: string;
  chats: Record<string, ChatDataProps>;
}) => {
  const copyChat: any = structuredClone(payload);
  if (chats[payload._id]) {
    copyChat.data = chats[payload._id].data;
  } else if (!copyChat.data) {
    copyChat.data = { members: [], messages: { data: [], hasNextPage: false } };
  }
  copyChat.data.members = await Promise.all(copyChat.members.filter(Boolean).map(async (member: any) => {
    if (users[member]) {
      const { email, ...profile } = users[member];
      return { profile, emails: [email], _id: member };
    }
    const { email, ...profile }: any = await $$usersModel.api.fetchUserFx({ userId: member });
    return { profile, emails: [email], _id: member };
  }));
  if (type === 'edit_tag' && copyChat.data.messages.data.length === 0) {
    setTimeout(() => {
      $$messagesModel.requested({ chatId: payload._id });
    }, 500);
  }
  if (!chats[payload._id]) {
    chatRoomsIdsAdded(payload._id);
  }
  return { _id: payload._id, chat: copyChat };
});

sample({
  clock: [$$channelsModel.messageGot, $$channelsModel.messageEdited],
  source: { chats: $usersChats, api: $api },
  fn: ({ chats, api }, data) => ({
    chats, api, data,
  }),
  target: addMessageFx,
});

sample({
  clock: $$channelsModel.getReadMessage,
  source: $usersChats,
  fn: (chats, data) => ({ chats, data }),
  target: readMessageFx,
});

sample({
  clock: $$channelsModel.messageDeleted,
  source: $usersChats,
  fn: (chats, data) => ({ chats, data }),
  target: deleteMessageFx,
});

sample({
  clock: searched,
  source: {
    chats: $usersChats,
    userId: $$socketModel.$userId,
    api: $api,
  },
  fn: ({ userId, api, chats }, payload) => ({
    ...payload,
    userId,
    chats,
    api,
  }),
  target: searchChatFx,
});

sample({
  clock: [userChatsUpdated, readMessageFx.doneData],
  target: $usersChats,
});

sample({
  clock: [
    addMessageFx.done,
    deleteMessageFx.done,
  ],
  fn: ({ result, params }) => {
    const newChats: Record<string, ChatDataProps> = {
      ...result,
    };
    const chat = newChats[params.data.tags[0]];
    const messagesObject = chat.data.messages.data.reduce((acc: any, obj: any) => {
      acc[obj._id ?? obj.ts] = obj;
      return acc;
    }, {});
    const messagesArray = Object.values(messagesObject);
    chat.data.messages.data = messagesArray.sort(sortDates) as ChatMessageWithMoreDataProps[];
    newChats[params.data.tags[0]] = chat;
    return newChats;
  },
  target: userChatsUpdated,
});

sample({
  clock: updated,
  source: { chats: $usersChats, currentUser: $currentUser },
  fn: ({ chats, currentUser }, payload) => {
    const copyChats = structuredClone(chats);
    let isNew = false;
    if (payload._id && payload.chat) {
      if (
        !copyChats[payload._id]
        && payload.chat.data.messages.data.length === 0
        && payload.chat.createdBy === currentUser?._id
      ) isNew = true;
      copyChats[payload._id] = { ...copyChats[payload._id], ...payload.chat };
    }
    return { chats: copyChats, newChat: { _id: payload._id!, isNew } };
  },
  target: spread({
    targets: {
      chats: userChatsUpdated,
      newChat: newChatAdded,
    },
  }),
});

sample({
  clock: updatedChat,
  source: { users: $$usersModel.$users, chats: $usersChats },
  fn: ({ users, chats }, { data, type }) => ({
    users,
    payload: data,
    type,
    chats,
  }),
  target: updateChatFx,
});

sample({
  clock: searchChatFx.doneData,
  source: $usersChats,
  fn: (chats, searchedChats) => {
    const copyChats: ChatDataProps[] = searchedChats || [];
    const usersChats: Record<string, ChatDataProps> = structuredClone(chats);
    if (searchedChats) {
      searchedChats.forEach((chat) => {
        if (!(chat._id in usersChats)) {
          usersChats[chat._id] = chat;
        }
      });
    }
    return {
      searches: copyChats,
      chats: usersChats,
    };
  },
  target: spread({
    targets: {
      searches: $searchedChats,
      chats: $usersChats,
    },
  }),
});

sample({
  clock: updateChatFx.doneData,
  target: updated,
});

sample({
  clock: $$channelsModel.chatUpdated,
  target: updatedChat,
});

sample({
  clock: [$$channelsModel.chatDeleted, deletedChat],
  source: $usersChats,
  fn: (chats, chatId) => {
    const newChats = structuredClone(chats);
    delete newChats[chatId];
    return {
      chats: newChats,
      chatId,
    };
  },
  target: spread({
    chats: $usersChats,
    chatId: chatRoomsIdsRemoved,
  }),
});

sample({
  clock: [online, refetched],
  source: $currentUser,
  fn: (userData) => ({
    userId: userData._id,
    limit: 15,
    pageNumber: 1,
  }),
  target: requested,
});

sample({
  clock: $$channelsModel.unreadCountUpdated,
  target: unreadMessagesUpdated,
});

wakeUpCheck(() => refetched());
