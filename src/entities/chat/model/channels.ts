import {
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { Channel, Socket } from 'phoenix';
import { $$socketModel } from '~/entities/ikinari';
import { showToast } from '~/components/default-components';
import { ChatDataProps, ChatMessageWithMoreDataProps } from '../types';

const failed = createEvent<unknown>();
const connect = createEvent();
const messageGot = createEvent<ChatMessageWithMoreDataProps>('Got new_message from ikinari');
const chatDeleted = createEvent<string>('Got deleted_tag');
const getReadMessage = createEvent<{ userId: string, message: { _id: string }, tags: string[]}>();
const chatUpdated = createEvent<{type: string, data: ChatDataProps}>('Got update_tag');
const unreadCountUpdated = createEvent<Record<string, number>>();
export const unreadMessageCount = createEvent<Record<string, {
  total: number,
  tags: any,
}>>();
const getUnreadMessages = createEvent<ChatMessageWithMoreDataProps[]>();
const messageEdited = createEvent<ChatMessageWithMoreDataProps>('Got update_tag with a create/edit message');
const messageDeleted = createEvent<ChatMessageWithMoreDataProps>('Got update_tag with a deleted message');
const socketSent = createEvent<{
  type: string,
  payload: any,
  channelName?: string | null;
}>();

const $channel = createStore<Channel | null>(null);

export const $$channelsModel = {
  $channel,
  connect,
  socketSent,
  unreadCountUpdated,
  unreadMessageCount,
  messageGot,
  messageEdited,
  messageDeleted,
  getReadMessage,
  getUnreadMessages,
  chatUpdated,
  chatDeleted,
};

const connectChannelFx = createEffect((
  {
    socket, userId,
  }:
    { socket: Socket | null, userId: string | null },
) => {
  if (!socket) return null;
  const connectedChannel = socket.channel(`members:${userId}`);
  connectedChannel.onError(console.log);
  connectedChannel.onClose(console.log);
  connectedChannel.join()
    .receive('ok', () => console.log(`connect to 'members:${userId}'`))
    .receive('error', (e: unknown) => failed(e));

  connectedChannel.on('new_tag', (response) => {
    chatUpdated({ type: 'new_tag', data: response.data });
  });

  connectedChannel.on('edit_tag', (response) => {
    chatUpdated({ type: 'edit_tag', data: response.data });
  });
  connectedChannel.on('delete_tag', (response) => {
    chatDeleted(response.data._id);
  });

  connectedChannel.on('unread_message_count_in_tags', (response) => {
    unreadCountUpdated(response.data);
  });

  connectedChannel.on('unread_message_count', (response) => {
    unreadMessageCount(response.data);
  });

  connectedChannel.on('unread_messages_in_tag', (response) => {
    getUnreadMessages(response.data);
  });

  connectedChannel.on('unread_messages', (response) => {
    getUnreadMessages(response.data);
  });

  connectedChannel.on('read_message', (response) => {
    getReadMessage(response.data);
  });

  connectedChannel.on('new_message', (response) => {
    messageGot(response.data);
  });

  connectedChannel.on('delete_message', (response: any) => {
    if (response.status === 'error') {
      showToast({
        message: 'Something wrong with deleted message',
        type: 'error',
      });
    } else {
      messageDeleted(response.data);
    }
  });
  connectedChannel.on('edit_message', (response: any) => {
    if (response.status === 'error') {
      showToast({
        message: 'Something wrong with edited message',
        type: 'error',
      });
    } else {
      messageEdited(response.data);
    }
  });
  connectedChannel.on('react_message', (response: any) => {
    messageEdited(response.data);
  });
  return connectedChannel;
});

const sentFx = createEffect(async ({
  channel,
  socket,
  channelName,
  type,
  payload,
}: {
  channel: Channel | null;
  socket: Socket | null;
  channelName?: string | null;
  type: string;
  payload: Record<string, unknown>;
}) => {
  if (!socket) return false;
  if (!channel || (
    channel
    && !['joining', 'joined'].includes(channel.state)
  )) return false;
  if (Array.isArray(payload)) {
    channel.push(type, payload);
  } else {
    channel.push(type, {
      ...payload,
      channelName,
    });
  }
  return true;
});

sample({
  clock: [connect, $$socketModel.$isConnected],
  source: {
    socket: $$socketModel.$socket,
    userId: $$socketModel.$userId,
    isConnected: $$socketModel.$isConnected,
  },
  filter: ({ isConnected }) => isConnected,
  fn: ({
    socket, userId,
  }) => ({
    socket,
    userId,
  }),
  target: connectChannelFx,
});

sample({
  clock: connectChannelFx.doneData,
  target: $channel,
});

sample({
  clock: connectChannelFx.fail,
  fn: ({ params: { socket }, error }) => ({ socket, e: error }),
  target: createEffect(({ socket }: { socket: Socket | null, e: unknown }) => {
    if (socket) socket.disconnect();
  }),
});

sample({
  clock: [
    connectChannelFx.failData,
    failed,
  ],
  target: createEffect((error: unknown) => console.error(JSON.stringify(error))),
});

sample({
  clock: socketSent,
  source: {
    channel: $$channelsModel.$channel,
    socket: $$socketModel.$socket,
  },
  fn: ({
    channel,
    socket,
  }, {
    type,
    payload,
    channelName,
  }) => ({
    channel,
    socket,
    channelName,
    type,
    payload,
  }),
  target: sentFx,
});

sample({
  clock: sentFx.doneData,
  filter: (d) => !d,
  target: connect,
});
