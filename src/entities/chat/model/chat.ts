import {
  combine, createEvent, createStore, sample,
} from 'effector';
import { $currentUser } from '~/config';
import { spread } from 'patronum';
import { $$chatsModel } from './chats';
import { $$channelsModel } from './channels';

type Chat = {
  title: string,
  members: string[],
  appErrorHandler?: (
    error: any,
    options?: {
      userRoles?: {
        _id?: string,
        isStaff?: boolean,
        isDev?: boolean,
      } | null,
      noToast?: boolean,
      toastMessage?: string,
    },
  ) => void,
}

type LeavedOrArchived = {
  _id: string,
  isArchive?: boolean,
}

const created = createEvent<Chat>();
const edited = createEvent<{ title?: string, members: string[], _id: string}>();
const leavedOrArchived = createEvent<LeavedOrArchived>();
const activated = createEvent<string | null>();
const privateBecame = createEvent<{ _id: string, callback: any }>();

const $activeChatId = createStore<string | null>(null);

const $activeChat = combine(
  $$chatsModel.$usersChats,
  $activeChatId,
  (chats, id) => {
    if (id && chats[id]) {
      return chats[id];
    }
    return null;
  },
);

const $suggestionsModel = combine($activeChat, (activeChat) => {
  const suggestionsData = activeChat?.data.members || [];
  const userIdsData: string[] = suggestionsData.map((suggestion) => suggestion._id);
  return {
    suggestions: suggestionsData,
    userIds: userIdsData,
  };
});

export const $$chatModel = {
  created,
  edited,
  leavedOrArchived,
  $activeChat,
  $activeChatId,
  $suggestionsModel,
  activated,
  privateBecame,
};

/* changed active chat */
sample({ clock: activated, target: $activeChatId });

sample({
  clock: created,
  source: $currentUser,
  fn: (currentUser, payload) => ({
    type: 'new_tag',
    payload: {
      ...payload,
      members: Array.from(new Set([...payload.members, currentUser._id])),
    },
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: edited,
  source: $currentUser,
  fn: (currentUser, payload) => ({
    type: 'edit_tag',
    payload: {
      ...payload,
      members: Array.from(new Set([...payload.members, currentUser._id])),
    },
  }),
  target: $$channelsModel.socketSent,
});

sample({
  clock: leavedOrArchived,
  source: { chat: $activeChat, currentUser: $currentUser },
  fn: ({ chat, currentUser }, { isArchive }) => ({
    socket: {
      type: 'edit_tag',
      payload: {
        isArchive,
        members: (chat?.data?.members || []).map((m) => m._id).filter((m) => m !== currentUser._id),
        _id: chat!._id,
      },
    },
    deletedChat: chat!._id,
  }),
  target: spread({
    targets: {
      socket: $$channelsModel.socketSent,
      deletedChat: $$chatsModel.deletedChat,
    },
  }),
});

sample({
  clock: $$channelsModel.chatDeleted,
  source: $activeChatId,
  filter: (currentChatId, deletedChatId) => currentChatId === deletedChatId,
  fn: () => null,
  target: $$chatModel.activated,
});

sample({
  clock: leavedOrArchived,
  fn: () => null,
  target: $$chatModel.activated,
});

sample({
  clock: $$chatsModel.newChatAdded,
  filter: ({ isNew }) => isNew,
  fn: ({ _id }) => _id,
  target: $$chatModel.activated,
});
