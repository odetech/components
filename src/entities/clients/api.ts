import { createEffect } from 'effector';
import { attachWithApi } from '~/config/init';
import type { Api } from '~/config/init';
import { TOrganization, TClientContact } from './types';
import {
  mapOrganizations, mapOrg, mapClientContact, mapClientContacts,
} from './lib';

type FetchProps = { currentUserId: string };
type FetchClientProps = { clientId: string };
type EditProps = { orgId: string, payload: any, hash?: string };
type CreateProps = { payload: any };
type ArchiveProps = { orgId: string };

type FetchContactsProps = { associatedId: string };
type EditContactProps = { associatedId: string, payload: any };
type CreateContactProps = { payload: any };
type ArchiveContactProps = { clientId: string, orgId: string };

const fetchOrgsApiFx = createEffect<FetchProps & Api, Record<string, TOrganization>>(
  async ({ api, currentUserId }) => {
    const orgs = await api.orgs.getOrgs({
      projectAssignedToUserId: currentUserId,
      // @ts-ignore
      data: ['contacts'],
      limit: 100,
    });
    return mapOrganizations(orgs);
  },
);

const fetchOrgApiFx = createEffect<FetchClientProps & Api, TOrganization>(
  async ({ api, clientId }) => {
    const org = await api.orgs.getOrg(clientId);
    return mapOrg(org);
  },
);

const editOrgApiFx = createEffect<EditProps & Api, TOrganization>(async ({ orgId, payload, api }) => {
  const org = await api.orgs.patch(orgId, payload);
  return mapOrg(org);
});

const createOrgApiFx = createEffect<CreateProps & Api, TOrganization>(async ({ payload, api }) => {
  const org = await api.orgs.post(payload);
  return mapOrg(org);
});

const archiveOrgApiFx = createEffect<ArchiveProps & Api, TOrganization>(async ({ orgId, api }) => {
  const org = await api.orgs.patch(orgId, { isActive: false });
  return mapOrg(org);
});

const fetchClientContactsApiFx = createEffect<FetchContactsProps & Api, Record<string, TClientContact>>(
  async ({ associatedId, api }) => {
    const clientContactsResult = await api.clientContact.getClientContacts({ associatedId });
    return mapClientContacts(clientContactsResult);
  },
);

const editClientContactsApiFx = createEffect<EditContactProps & Api, TClientContact>(
  async ({ associatedId, payload, api }) => {
    const clientContactResult = await api.clientContact.patch(associatedId, payload);
    return mapClientContact(clientContactResult);
  },
);

const createClientContactApiFx = createEffect<CreateContactProps & Api, TClientContact>(
  async ({ payload, api }) => {
    const clientContactResult = await api.clientContact.post(payload);
    return mapClientContact(clientContactResult);
  },
);

const archiveClientContactApiFx = createEffect<ArchiveContactProps & Api, ArchiveContactProps>(
  async ({ clientId, orgId, api }) => {
    await api.clientContact.delete(clientId);
    return { clientId, orgId };
  },
);

export const fetchOrgsFx = attachWithApi<FetchProps, Record<string, TOrganization>>(fetchOrgsApiFx);
export const fetchOrgFx = attachWithApi<FetchClientProps, TOrganization>(fetchOrgApiFx);
export const createOrgFx = attachWithApi<CreateProps, TOrganization>(createOrgApiFx);
export const editOrgFx = attachWithApi<EditProps, TOrganization>(editOrgApiFx);
export const archiveOrgFx = attachWithApi<ArchiveProps, TOrganization>(archiveOrgApiFx);

export const fetchClientContactsFx = attachWithApi<
  FetchContactsProps, Record<string, TClientContact>
>(fetchClientContactsApiFx);
export const createClientContactFx = attachWithApi<CreateContactProps, TClientContact>(createClientContactApiFx);
export const editClientContactsFx = attachWithApi<EditContactProps, TClientContact>(editClientContactsApiFx);
export const archiveClientContactFx = attachWithApi<
  ArchiveContactProps, ArchiveContactProps
>(archiveClientContactApiFx);
