import { createEffect, sample } from 'effector';
import { showToast } from '~/components/default-components';
import {
  archiveOrgFx, createOrgFx, editOrgFx, fetchOrgsFx,
} from '../api';

sample({
  clock: [fetchOrgsFx.failData, editOrgFx.failData, createOrgFx.failData, archiveOrgFx.failData],
  target: createEffect(async (e: any) => {
    const errorData = await e.response.json();
    if (errorData?.detail) {
      showToast({
        message: errorData?.detail,
        type: 'error',
      });
    }
  }),
});
