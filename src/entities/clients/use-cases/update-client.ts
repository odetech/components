import { sample } from 'effector';
import { spread } from 'patronum';
import { editOrgFx } from '../api';
import { $clients, clientChanged, safetyEdited } from '../model';

sample({
  clock: clientChanged,
  source: $clients,
  fn: (clients, payload) => {
    const hash = (crypto as any).randomUUID();
    return {
      effect: {
        ...payload, hash,
      },
      savedParams: { client: clients[payload.orgId], hash },
    };
  },
  target: spread({
    targets: {
      effect: editOrgFx,
      savedParams: safetyEdited,
    },
  }),
});

sample({
  clock: clientChanged,
  source: $clients,
  fn: (current, { payload, orgId }) => ({ ...current, [orgId]: { ...current[orgId], ...payload } }),
  target: $clients,
});
