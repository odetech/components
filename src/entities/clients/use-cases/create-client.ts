import { sample } from 'effector';
import { clientCreated } from '../model';
import { createOrgFx } from '../api';

sample({
  clock: clientCreated,
  target: createOrgFx,
});
