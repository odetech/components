import { sample } from 'effector';
import { $currentUser } from '~/config';
import { $clients, clientsRequested } from '../model';
import { fetchOrgsFx } from '../api';

sample({
  clock: clientsRequested,
  source: $currentUser,
  fn: (user) => ({ currentUserId: user._id }),
  target: fetchOrgsFx,
});

sample({
  clock: fetchOrgsFx.doneData,
  source: $clients,
  fn: (_, newResults) => ({ ...newResults }),
  target: $clients,
});
