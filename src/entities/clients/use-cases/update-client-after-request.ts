import { sample } from 'effector';
import { archiveOrgFx, createOrgFx } from '../api';
import { $clients } from '../model';

sample({
  clock: [createOrgFx.doneData, archiveOrgFx.doneData],
  source: $clients,
  fn: (current, newResult) => ({ ...current, [newResult.id]: newResult }),
  target: $clients,
});
