import { pending } from 'patronum';
import {
  archiveOrgFx, createOrgFx, editOrgFx, fetchOrgsFx,
} from '../api';

export const $clientsLoading = pending({
  effects: [fetchOrgsFx, archiveOrgFx, createOrgFx, editOrgFx],
  of: 'some',
});
