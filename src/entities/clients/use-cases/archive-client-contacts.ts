import { sample } from 'effector';
import { spread } from 'patronum';
import { archiveClientContactFx } from '../api';
import { $clientContacts, $clients } from '../model';

sample({
  clock: archiveClientContactFx.doneData,
  source: { clients: $clients, contacts: $clientContacts },
  fn: ({ clients, contacts }, { orgId, clientId }) => {
    const newContacts = { ...contacts };
    delete newContacts[clientId];
    const copyClients = { ...clients };
    copyClients[orgId].contacts = Object.values(newContacts);
    return {
      contacts: newContacts,
      clients: copyClients,
    };
  },
  target: spread({
    targets: {
      contacts: $clientContacts,
      clients: $clients,
    },
  }),
});
