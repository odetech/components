import { sample } from 'effector';
import { spread } from 'patronum';
import { editOrgFx } from '../api';
import { safetyEdited, $previousStageClients, $clients } from '../model';

sample({
  clock: safetyEdited,
  source: $previousStageClients,
  fn: (previousStageClients, { client, hash }) => ({ ...previousStageClients, [hash]: client }),
  target: $previousStageClients,
});

sample({
  clock: editOrgFx.done,
  source: $previousStageClients,
  fn: (previousStageClients, { params }) => {
    const previous = { ...previousStageClients };
    if (params.hash) {
      delete previous[params.hash];
    }
    return { ...previous };
  },
  target: $previousStageClients,
});

sample({
  clock: editOrgFx.fail,
  source: { previousStageClients: $previousStageClients, clients: $clients },
  fn: ({ previousStageClients, clients }, { params }) => {
    const previous = { ...previousStageClients };
    if (params.hash) {
      const currentProject = { ...previous[params.hash] };
      delete previous[params.hash];
      return { previous, clients: { ...clients, [currentProject.id]: currentProject } };
    }
    return { previous, clients };
  },
  target: spread({
    targets: {
      previous: $previousStageClients,
      clients: $clients,
    },
  }),
});
