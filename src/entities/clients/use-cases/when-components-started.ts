import { sample } from 'effector';
import { componentsStarted } from '~/config';
import { fetchOrgsFx } from '../api';

sample({
  clock: componentsStarted,
  fn: (user) => ({ currentUserId: user._id }),
  target: fetchOrgsFx,
});
