import { persist } from 'effector-storage/local';
import { $clients } from '../model';

persist({
  store: $clients,
  key: 'clients',
});
