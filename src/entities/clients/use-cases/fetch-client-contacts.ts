import { sample } from 'effector';
import { spread } from 'patronum';
import { fetchClientContactsFx } from '../api';
import { $clients, $clientContacts, clientContactsFetched } from '../model';

sample({ clock: clientContactsFetched, target: fetchClientContactsFx });

sample({
  clock: fetchClientContactsFx.done,
  source: $clients,
  fn: (clients, {
    params,
    result,
  }) => {
    const newClients = { ...result };
    const copyClients = { ...clients };
    copyClients[params.associatedId].contacts = Object.values(newClients);
    return {
      contacts: newClients,
      clients: copyClients,
    };
  },
  target: spread({
    targets: {
      contacts: $clientContacts,
      clients: $clients,
    },
  }),
});
