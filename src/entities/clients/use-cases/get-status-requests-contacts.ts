import { pending } from 'patronum';
import {
  fetchClientContactsFx, createClientContactFx, editClientContactsFx, archiveClientContactFx,
} from '../api';

export const $clientContactsLoading = pending({
  effects: [fetchClientContactsFx, createClientContactFx, editClientContactsFx, archiveClientContactFx],
  of: 'some',
});
