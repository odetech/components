import { sample } from 'effector';
import { $clients, clientRequested } from '../model';
import { fetchOrgFx } from '../api';

sample({
  clock: clientRequested,
  fn: (clientId) => ({ clientId }),
  target: fetchOrgFx,
});

sample({
  clock: fetchOrgFx.doneData,
  source: $clients,
  fn: (currentResults, newResult) => ({ ...currentResults, [newResult.id]: newResult }),
  target: $clients,
});
