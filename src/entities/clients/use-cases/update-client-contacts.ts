import { sample } from 'effector';
import { spread } from 'patronum';
import { createClientContactFx, editClientContactsFx } from '../api';
import { $clientContacts, $clients } from '../model';

sample({
  clock: [createClientContactFx.doneData, editClientContactsFx.doneData],
  source: { clients: $clients, contacts: $clientContacts },
  fn: ({ clients, contacts }, newResult) => {
    const newContacts = { ...contacts, [newResult.id]: newResult };
    const copyClients = { ...clients };
    if (!copyClients[newResult.associatedId].contacts) {
      copyClients[newResult.associatedId].contacts = [];
    }
    copyClients[newResult.associatedId].contacts = Object.values(newContacts);
    return {
      contacts: newContacts,
      clients: copyClients,
    };
  },
  target: spread({
    targets: {
      clients: $clients,
      contacts: $clientContacts,
    },
  }),
});
