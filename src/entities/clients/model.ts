import { createEvent, createStore, Store } from 'effector';
import { fromArrayToHash } from '~/helpers/globalHelpers';
import { TClientContact, TOrganization } from './types';

const $clients = createStore<Record<string, TOrganization>>({});
const $previousStageClients = createStore<Record<string, TOrganization>>({});

const $clientsActive = $clients.map((projects) => fromArrayToHash(Object.values(projects)
  .filter((client) => client.isActive))) as Store<Record<string, TOrganization>>;

const $clientsOptions = $clientsActive
  .map((clientanizationsData) => Object.values(clientanizationsData).map((client) => ({
    value: client.id,
    label: client.name,
  })));
const $clientContacts = createStore<Record<string, TClientContact>>({});

// events
const clientsRequested = createEvent();
const clientRequested = createEvent<string>();
const clientChanged = createEvent<{ orgId: string, payload: any }>();

const safetyEdited = createEvent<{ client: TOrganization, hash: string }>();
const clientCreated = createEvent<{ payload: any }>();
const clientContactsFetched = createEvent<{ associatedId: string }>();

export {
  $clientsActive,
  $clients,
  $previousStageClients,
  $clientsOptions,
  $clientContacts,
  clientRequested,
  clientContactsFetched,
  clientsRequested,
  clientCreated,
  clientChanged,
  safetyEdited,
};
