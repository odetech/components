export type TClientContact = {
  firstName: string;
  lastName: string;
  email: string;
  associatedId: string;
  about: string;
  id: string;
  position: string;
  name?: string,
  shortName?: string,
};

export type TOrganization = {
  name: string,
  id: string;
  isActive?: boolean;
  description: string;
  geoInfo: {
    address: string;
    city: string;
    state: string;
    zip: string;
    country: string;
  },
  website: string;
  createdBy: string;
  contacts: TClientContact[];
};
