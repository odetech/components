import {
  $clientsActive,
  $clientsOptions,
  $clients,
  $clientContacts,
  clientsRequested,
  clientCreated,
  clientChanged,
  clientRequested,
  clientContactsFetched,
  safetyEdited,
} from './model';
import * as api from './api';
import './use-cases/save-to-cache';
import './use-cases/show-toast-with-error';
import './use-cases/when-components-started';
import './use-cases/fetch-clients';
import './use-cases/fetch-client';
import './use-cases/fetch-client-contacts';
import './use-cases/create-client';
import './use-cases/update-client';
import './use-cases/update-client-after-request';
import './use-cases/archive-client-contacts';
import './use-cases/update-client-contacts';
import './use-cases/safety-edit';
import * as ClientsTypes from './types';
import { $clientsLoading } from './use-cases/get-status-requests';
import { $clientContactsLoading } from './use-cases/get-status-requests-contacts';

export const $$clientsModel = {
  $clientsActive,
  $clientsOptions,
  $clients,
  $clientContacts,
  $clientsLoading,
  $clientContactsLoading,
  events: {
    clientsRequested,
    clientCreated,
    clientChanged,
    clientRequested,
    clientContactsFetched,
    safetyEdited,
  },
  api,
};

export { ClientsTypes };
