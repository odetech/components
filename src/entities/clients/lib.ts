import { fromArrayToHash } from '~/helpers/globalHelpers';
import { TOrganization, TClientContact } from './types';

export const mapClientContact = ({
  _id,
  associatedId,
  memberInfo,
  memberEmail,
}: any) : TClientContact => ({
  id: _id,
  firstName: memberInfo.firstName,
  lastName: memberInfo.lastName,
  email: memberEmail,
  associatedId: associatedId,
  about: memberInfo.description,
  position: memberInfo.position,
  name: (memberInfo.firstName && memberInfo.lastName)
    ? `${memberInfo.firstName} ${memberInfo.lastName}`
    : '',
  shortName: (memberInfo.firstName && memberInfo.lastName)
    ? `${memberInfo.firstName} ${memberInfo.lastName?.[0] || ''}.`
    : '',
});

export const mapOrg = ({
  _id,
  name,
  geoInfo,
  website,
  isActive,
  description,
  createdBy,
  data,
}: any): TOrganization => ({
  id: _id,
  name,
  description,
  geoInfo: {
    address: geoInfo.address || '',
    city: geoInfo.city || '',
    state: geoInfo.state || '',
    zip: geoInfo.zip || '',
    country: geoInfo.country || '',
  },
  website: website || '',
  createdBy,
  // @ts-ignore
  contacts: data?.contacts?.length ? data.contacts.map(mapClientContact) : [],
  isActive,
});

export const mapOrganizations = (response: any) => {
  const flattenResponse = response.flat();
  const mappedOrgsData = flattenResponse.map(mapOrg) as TOrganization[];

  return fromArrayToHash(mappedOrgsData);
};

export const mapClientContacts = (response: any) => {
  const mappedNotesData = response.map(mapClientContact);

  return fromArrayToHash(mappedNotesData);
};
