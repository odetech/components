import { socketSent, teamSentFx } from "./model";

export const $$teamModel = {
  events: {
    socketSent,
    teamSentFx
  },
};

