import { createEffect, createEvent } from "effector";
import { Channel } from "phoenix";

const teamSentFx = createEffect(async ({
  channel,
  type,
  payload,
  onSuccess = () => { },
  onError = () => { }
}: {
  channel: Channel | null;
  type: string;
  payload: Record<string, unknown>;
  onSuccess?: (response: unknown) => void;
  onError?: (response: unknown, reason: 'error' | 'timeout') => void;
}) => {
  if (!channel) return;
  const push = channel.push(type, payload)
  push
    .receive("ok", (data?: unknown) => onSuccess(data))
    .receive("error", (error?: unknown) => { onError(error, 'error') })
    .receive("timeout", (error?: unknown) => { onError(error, 'timeout') });
});

const socketSent = createEvent<{
  type: string,
  payload: Record<string, unknown>,
  onSuccess?: (response: unknown) => void;
  onError?: (response: unknown, reason: 'error' | 'timeout') => void;
}>();

export {
  socketSent,
  teamSentFx
}
