export type TProject = {
  title: string;
  orgId: string;
  avatar?: string;
  description: string;
  assignedToUserId: string[];
  hashtags: string[];
  team: {
    members: Record<string, {
      role: { isIC: boolean; isAdmin: boolean; isManager: boolean; };
      position: string;
      email: string;
      userId: string;
    }>;
    metadata: {
      assignedUserIds: string[];
    }
  };
  id: string;
  uuid?: string;
  isActive: boolean;
  isQA: boolean;
  startDate: string;
  dueDate: string;
  createdBy: string;
};
