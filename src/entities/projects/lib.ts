import { fromArrayToHash } from '~/helpers/globalHelpers';

export const mapProject = ({
  _id,
  title,
  orgId,
  description,
  assignedToUserId,
  dueDate,
  team,
  startDate,
  hashtags,
  createdBy,
  avatar,
  uuid,
  isActive,
  isQA,
}: any) => ({
  title,
  orgId,
  description,
  assignedToUserId,
  id: _id,
  dueDate,
  hashtags: hashtags?.map((str: string) => (str[0] === '#' ? str.substring(1) : str)) || [],
  startDate,
  avatar,
  team,
  createdBy,
  uuid: uuid?.strValue,
  isActive,
  isQA,
});

// TODO: add types from odecloud
export const mapProjects = (response: any) => {
  const mappedProjectsData = response.map(mapProject);

  return fromArrayToHash(mappedProjectsData);
};
