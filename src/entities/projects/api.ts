import { createEffect } from 'effector';
import { type Api, attachWithApi } from '~/config/init';
import { TProject } from './types';
import { mapProject, mapProjects } from './lib';

type FetchProps = { currentUserId: string | null };
type FetchProjectProps = { projectId: string };
type CreateProps = { payload: any };
type EditTagsProps = { project: any, values: any, hash?: string };
type EditProps = { projectId: string, payload: any, hash?: string };
type ArchiveProps = { projectId: string };

const fetchProjectsApiFx = createEffect<FetchProps & Api, Record<string, TProject>>(
  async ({ api, currentUserId }) => {
    const projects = await api.projects.getProjects({
      // @ts-ignore
      assignedToUserId: currentUserId, limit: 100,
    });
    return mapProjects(projects);
  },
);

const fetchProjectApiFx = createEffect<FetchProjectProps & Api, TProject>(
  async ({ api, projectId }) => {
    const projects = await api.projects.getProject(projectId);
    return mapProjects(projects);
  },
);

const editTagsApiFx = createEffect<EditTagsProps & Api, TProject>(
  async ({
    api, project, values,
  }) => {
    let copyProject: any = { ...project };
    const body = {
      ...values,
      orgId: copyProject.orgId,
      uuid: {
        strValue: copyProject.uuid,
      },
    };

    if (body.hashtags?.length) {
      body.hashtags = Array.from(new Set(body.hashtags.map((str: string) => (str[0] === '#' ? str : `#${str}`))));
    }

    await api.projects.patch(
      copyProject.id,
      {
        ...body,
      },
    );
    copyProject = {
      ...copyProject,
      hashtags: body.hashtags.map((str: string) => (str[0] === '#' ? str.substring(1) : str)),
    };
    return copyProject;
  },
);

const createProjectApiFx = createEffect<CreateProps & Api, TProject>(async ({ payload, api }) => {
  const project = await api.projects.post(payload);
  return mapProject(project);
});

const editProjectApiFx = createEffect<EditProps & Api, TProject>(
  async ({ projectId, payload, api }) => {
    const project = await api.projects.patch(projectId, payload);
    return mapProject(project);
  },
);

const archiveProjectApiFx = createEffect<ArchiveProps & Api, TProject>(async ({ projectId, api }) => {
  const project = await api.projects.archive(projectId);
  return mapProject(project);
});

export const fetchProjectsFx = attachWithApi<FetchProps, Record<string, TProject>>(fetchProjectsApiFx);
export const fetchProjectFx = attachWithApi<FetchProjectProps, TProject>(fetchProjectApiFx);
export const editTagsFx = attachWithApi<EditTagsProps, TProject>(editTagsApiFx);
export const createProjectFx = attachWithApi<CreateProps, TProject>(createProjectApiFx);
export const editProjectFx = attachWithApi<EditProps, TProject>(editProjectApiFx);
export const archiveProjectFx = attachWithApi<ArchiveProps, TProject>(archiveProjectApiFx);
