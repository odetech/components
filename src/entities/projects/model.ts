import {
  createEvent, createStore, Store,
} from 'effector';
import { fromArrayToHash } from '~/helpers/globalHelpers';
import { TProject } from './types';

const $projects = createStore<Record<string, TProject>>({});
const $previousStageProjects = createStore<Record<string, TProject>>({});
const $projectsActive = $projects.map((projects) => fromArrayToHash(Object.values(projects)
  .filter((project) => project.isActive))) as Store<Record<string, TProject>>;

// events
const tagsUpdated = createEvent<{ project: TProject, values: any, callback?: any }>();
const projectsRequested = createEvent();
const projectRequested = createEvent<string>();
const projectCreated = createEvent<{ payload: Partial<TProject> }>();
const projectEdited = createEvent<{ projectId: string, payload: Partial<TProject> }>();
const projectArchived = createEvent<{ projectId: string }>();
const safetyEdited = createEvent<{ project: TProject, hash: string }>();

export {
  $projectsActive,
  $previousStageProjects,
  $projects,
  projectsRequested,
  projectCreated,
  projectEdited,
  projectRequested,
  projectArchived,
  tagsUpdated,
  safetyEdited,
};
