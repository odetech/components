import { sample } from 'effector';
import { projectArchived } from '../model';
import { archiveProjectFx } from '../api';

sample({
  clock: projectArchived,
  target: archiveProjectFx,
});
