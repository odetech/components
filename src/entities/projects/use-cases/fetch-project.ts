import { sample } from 'effector';
import { fetchProjectFx } from '../api';
import { $projects, projectRequested } from '../model';

sample({
  clock: projectRequested,
  fn: (projectId) => ({ projectId }),
  target: fetchProjectFx,
});

sample({
  clock: fetchProjectFx.doneData,
  source: $projects,
  fn: (oldProjects, newResult) => ({ ...oldProjects, [newResult.id]: newResult }),
  target: $projects,
});
