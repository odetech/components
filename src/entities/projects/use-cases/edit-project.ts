import { sample } from 'effector';
import { spread } from 'patronum';
import { TProject } from '../types';
import { editProjectFx } from '../api';
import { $projects, projectEdited, safetyEdited } from '../model';

sample({
  clock: editProjectFx.done,
  source: $projects,
  fn: (current, { params, result }) => ({
    ...current,
    [params.projectId]: result,
  }),
  target: $projects,
});

sample({
  clock: projectEdited,
  source: $projects,
  fn: (projects, { projectId, payload }) => {
    const hash = (crypto as any).randomUUID();
    return {
      effect: { projectId, payload, hash },
      savedParams: { project: projects[projectId], hash },
    };
  },
  target: spread({
    targets: {
      effect: editProjectFx,
      savedParams: safetyEdited,
    },
  }),
});

sample({
  clock: projectEdited,
  source: $projects,
  fn: (projects, { projectId, payload }) => {
    const copyProjects: Record<string, TProject> = { ...projects };
    copyProjects[projectId] = {
      ...copyProjects[projectId],
      ...payload,
    };
    return copyProjects;
  },
  target: $projects,
});
