import { sample } from 'effector';
import { componentsStarted } from '~/config';
import { fetchProjectsFx } from '../api';

sample({
  clock: componentsStarted,
  fn: (user) => ({ currentUserId: user._id }),
  target: fetchProjectsFx,
});
