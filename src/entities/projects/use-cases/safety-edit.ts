import { sample } from 'effector';
import { spread } from 'patronum';
import { editTagsFx, editProjectFx } from '../api';
import { safetyEdited, $previousStageProjects, $projects } from '../model';

sample({
  clock: safetyEdited,
  source: $previousStageProjects,
  fn: (previousStageProjects, { project, hash }) => ({ ...previousStageProjects, [hash]: project }),
  target: $previousStageProjects,
});

sample({
  clock: [editTagsFx.done, editProjectFx.done],
  source: $previousStageProjects,
  fn: (previousStageProjects, { params }) => {
    const previous = { ...previousStageProjects };
    if (params.hash) {
      delete previous[params.hash];
    }
    return { ...previous };
  },
  target: $previousStageProjects,
});

sample({
  clock: [editTagsFx.fail, editProjectFx.fail],
  source: { previousStageProjects: $previousStageProjects, projects: $projects },
  fn: ({ previousStageProjects, projects }, { params }) => {
    const previous = { ...previousStageProjects };
    if (params.hash) {
      const currentProject = { ...previous[params.hash] };
      delete previous[params.hash];
      return { previous, projects: { ...projects, [currentProject.id]: currentProject } };
    }
    return { previous, projects };
  },
  target: spread({
    targets: {
      previous: $previousStageProjects,
      projects: $projects,
    },
  }),
});
