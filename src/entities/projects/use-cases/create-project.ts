import { sample } from 'effector';
import { projectCreated } from '../model';
import { createProjectFx } from '../api';

sample({
  clock: projectCreated,
  target: createProjectFx,
});
