import { sample } from 'effector';
import { createProjectFx, archiveProjectFx } from '../api';
import { $projects } from '../model';

sample({
  clock: [createProjectFx.doneData, archiveProjectFx.doneData],
  source: $projects,
  fn: (current, newResult) => ({ ...current, [newResult.id]: newResult }),
  target: $projects,
});
