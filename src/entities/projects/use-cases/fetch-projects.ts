import { sample } from 'effector';
import { $currentUser } from '~/config';
import { fetchProjectsFx } from '../api';
import { $projects, projectsRequested } from '../model';

sample({
  clock: projectsRequested,
  source: $currentUser,
  fn: (user) => ({ currentUserId: user._id }),
  target: fetchProjectsFx,
});

sample({
  clock: fetchProjectsFx.doneData,
  source: $projects,
  fn: (_, newResults) => ({ ...newResults }),
  target: $projects,
});
