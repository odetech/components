import { persist } from 'effector-storage/local';
import { $projects } from '../model';

persist({
  store: $projects,
  key: 'projects',
});
