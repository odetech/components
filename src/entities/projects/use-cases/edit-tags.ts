import { sample } from 'effector';
import { spread } from 'patronum';
import { editTagsFx } from '../api';
import { $projects, tagsUpdated, safetyEdited } from '../model';

sample({
  clock: editTagsFx.done,
  source: $projects,
  fn: (current, { params, result }) => ({
    ...current,
    [params.project.id]: result,
  }),
  target: $projects,
});

sample({
  clock: tagsUpdated,
  source: $projects,
  fn: (projects, { project, values }) => {
    const hash = (crypto as any).randomUUID();
    return {
      effect: { project, values, hash },
      savedParams: { project: projects[project.id], hash },
    };
  },
  target: spread({
    targets: {
      effect: editTagsFx,
      savedParams: safetyEdited,
    },
  }),
});

sample({
  clock: tagsUpdated,
  source: $projects,
  fn: (projects, { project, values, callback }) => {
    let oldTag = null;
    let newTag = null;
    const newTags = values.hashtags.map((str: string) => (str[0] === '#' ? str.substring(1) : str));
    for (let i = 0; i < project.hashtags.length; i += 1) {
      if (project.hashtags[i] !== newTags[i]) {
        oldTag = project.hashtags[i];
        newTag = newTags[i];
      }
    }
    if (callback) callback({ oldTag, newTag, projectId: project.id });

    const copyProjects = { ...projects };
    copyProjects[project.id] = {
      ...copyProjects[project.id],
      ...project,
      ...values,
    };
    return copyProjects;
  },
  target: $projects,
});
