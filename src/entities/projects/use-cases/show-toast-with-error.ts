import { createEffect, sample } from 'effector';
import { showToast } from '~/components/default-components';
import {
  archiveProjectFx, fetchProjectsFx, createProjectFx, editProjectFx, editTagsFx,
} from '../api';

sample({
  clock: [
    archiveProjectFx.failData, fetchProjectsFx.failData,
    createProjectFx.failData, editProjectFx.failData, editTagsFx.failData,
  ],
  target: createEffect(async (e: any) => {
    const errorData = await e.response.json();
    if (errorData?.detail) {
      showToast({
        message: errorData?.detail,
        type: 'error',
      });
    }
  }),
});
