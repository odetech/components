import { pending } from 'patronum';
import {
  fetchProjectsFx, createProjectFx, editProjectFx, archiveProjectFx,
} from '../api';

export const $projectsLoading = pending({
  effects: [fetchProjectsFx, createProjectFx, editProjectFx, archiveProjectFx],
  of: 'some',
});
