import { sample } from 'effector';
import { $$usersModel } from '~/entities/users';
import { fetchProjectsFx } from '../api';

sample({
  clock: fetchProjectsFx.doneData,
  fn: (projects) => {
    const usersIds = Object.values(projects)
      .reduce<string[]>((accum, { assignedToUserId }) => [...accum, ...assignedToUserId], []);
    return Array.from(new Set(usersIds));
  },
  target: $$usersModel.events.usersRequested,
});
