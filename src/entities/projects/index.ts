import {
  $projectsActive,
  $projects,
  projectsRequested,
  projectCreated,
  projectEdited,
  projectArchived,
  tagsUpdated,
  safetyEdited,
  projectRequested,
} from './model';
import * as api from './api';
import type { TProject } from './types';
import { mapProject, mapProjects } from './lib';

import './use-cases/when-components-started';
import './use-cases/fetch-projects';
import './use-cases/edit-tags';
import './use-cases/save-to-cache';
import './use-cases/show-toast-with-error';
import './use-cases/update-project-after-request';
import './use-cases/safety-edit';
import './use-cases/fetch-project';
import './use-cases/create-project';
import './use-cases/edit-project';
import './use-cases/archive-project';
import './use-cases/fetch-users-after-projects';
import { $projectsLoading } from './use-cases/get-status-requests';

export const $$projectsModel = {
  $projectsActive,
  $projects,
  $projectsLoading,
  events: {
    projectsRequested,
    projectCreated,
    projectEdited,
    projectRequested,
    projectArchived,
    tagsUpdated,
    safetyEdited,
  },
  api,
};

export { TProject, mapProjects, mapProject };
