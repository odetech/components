import { createEffect } from 'effector';
import { Api, attachWithApi } from '~/config/init';
import { TSmallUser } from './types';
import { mapAssignedUser, mapAssignedUsers } from './lib';

type FetchProps = { usersIds: string[] };
type GetProps = { userId: string };

const fetchUsersApiFx = createEffect<FetchProps & Api, Record<string, TSmallUser>>(
  async ({ api, usersIds }) => {
    // @ts-ignore
    // eslint-disable-next-line prefer-spread
    const chunks = [].concat.apply(
      [],
      // @ts-ignore
      usersIds.map((_, i) => (i % 20 ? [] : [usersIds.slice(i, i + 20)])),
    );
    const usersPromises = await Promise.all(
      chunks.map(async (users) => {
        const { data } = await api.users.getUsers({ _id: users, limit: 20 });
        return mapAssignedUsers(data);
      }),
    );
    return Object.assign({}, ...usersPromises);
  },
);

const fetchUserApiFx = createEffect<GetProps & Api, any>(
  async ({ api, userId }) => {
    const user = await api.users.getUser(userId);
    return mapAssignedUser(user);
  },
);

export const fetchUserFx = attachWithApi<GetProps, TSmallUser>(fetchUserApiFx);
export const fetchUsersFx = attachWithApi<FetchProps, Record<string, TSmallUser>>(fetchUsersApiFx);
