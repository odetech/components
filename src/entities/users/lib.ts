import { fromArrayToHash } from '~/helpers/globalHelpers';

export const mapAssignedUser = ({ profile, emails, _id }: any): any => ({
  name: (profile?.firstName && profile?.lastName) ? `${profile.firstName} ${profile.lastName}` : '',
  shortName: (profile?.firstName && profile?.lastName) ? `${profile.firstName} ${profile.lastName?.[0] || ''}.` : '',
  firstName: profile?.firstName,
  lastName: profile?.lastName,
  avatarUrl: profile?.avatar?.secureUrl,
  avatar: profile?.avatar,
  id: _id,
  _id,
  email: emails?.length === 1 ? emails[0].address : '',
});

export const mapAssignedUsers = (responses: any): Record<string, any> => (
  responses
    ? fromArrayToHash(responses.map(mapAssignedUser))
    : {}
);
