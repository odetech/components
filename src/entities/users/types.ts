export type MasterNotificationsProps = {
  _id: string,
  userId: string,
  odetask: {
    frequencies: string[],
    muteOptions: string[],
  }
  odesocial: {
    frequencies: string[],
    muteOptions: string[],
  }
}

export type Skill = {
  value: string;
  rating: number;
  singleOption?: number;
  multipleOptions?: number[];
  freeAnswer?: string;
};

export type AppStoreProps ={
  _id: string,
  apps: string[],
  userId: string,
  tokens?: {
    secret: string,
    when: number,
  }[]
}

export type WorkExperience = {
  company: string,
  description: string,
  position: string,
  location: string,
  startDate: string | null | Date,
  endDate: string | null | Date,
};

export type Project = {
  company: string,
  name: string,
  role?: string,
  description?: string,
  bulletPoints: string[],
  isNDA: boolean,
  autoAdded: boolean,
  startDate?: string | null | Date,
  endDate?: string | null | Date,
};

export type TSmallUser = {
  name: string | null;
  firstName?: string;
  shortName?: string;
  lastName?: string;
  id: string;
  avatarUrl?: string;
  email?: string;
};

export type UserDataProps = {
  createdAt: string,
  emails: { address: string, verified: boolean }[],
  profile: {
    aboutMe: string | null,
    isPublic?: boolean,
    avatar: null | {
      secureUrl: string | null,
      _id: string | null,
    },
    createdBy: string | null,
    createdFrom: string | null,
    firstName: string | null,
    lastName: string | null,
    masterNotifications?: MasterNotificationsProps,
    withNext?: boolean,
    onboardings: null,
    roles: {
      _id: string,
      isStaff: boolean,
      isDev?: boolean,
    } | null,
    skillsets: null | {
      values: string[]
      _id: string,
    },
    articles: null | {
      image?: null | string,
      title?: null | string,
      url: null | string,
    }[],
    tvLinks: null | {
      image?: null | string,
      title?: null | string,
      url: null | string,
    }[],
    snsLinks: null | {
      instagram?: string,
      facebook?: string,
      twitter?: string,
      linkedin?: string,
      tiktok?: string,
    },
    expertise: {
      expertise: Record<string, Skill[]>,
      _id: string,
    },
    appStore: AppStoreProps,
    tagline: string | null,
    profileCompletion?: {
      notes: {
        'profile.avatars'?: string,
        'profile.skillsets'?: string,
        'profile.tagline'?: string,
      },
      progress: number,
    },
    // eslint-disable-next-line camelcase
    workExperiences: WorkExperience[],
    // eslint-disable-next-line camelcase
    projectExperiences: Project[],
    pinnedTags: null | string[],
    wallet?: {
      userId: string,
      receipts: any,
      wallet: 0,
      rank: string,
      history: any[],
      messagesUnit: any,
      totalMpsUnit?: number | null,
      createdAt: null,
      updatedAt: null,
      completion: any,
    },
    rank?: string,
    timezone?: {
      gmtFormat: string,
      offset: number,
      timezoneName: string,
    } | null,
  },
  username: string,
  _id: string,
};
