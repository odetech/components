import { sample } from 'effector';
import { $users, usersRequested } from '../model';
import { TSmallUser } from '../types';
import { fetchUsersFx } from '../api';

sample({
  clock: usersRequested,
  fn: (users) => ({ usersIds: users }),
  target: fetchUsersFx,
});

sample({
  clock: fetchUsersFx.doneData,
  source: $users,
  fn: (users, results: Record<string, TSmallUser>) => ({ ...users, ...results }),
  target: $users,
});
