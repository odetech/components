import { createEffect, sample } from 'effector';
import { showToast } from '~/components/default-components';
import {
  fetchUsersFx, fetchUserFx,
} from '../api';

sample({
  clock: [fetchUsersFx.failData, fetchUserFx.failData],
  target: createEffect(async (e: any) => {
    const errorData = await e.response.json();
    if (errorData?.detail) {
      showToast({
        message: errorData?.detail,
        type: 'error',
      });
    }
  }),
});
