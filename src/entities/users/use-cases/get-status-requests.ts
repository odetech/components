import { pending } from 'patronum';
import {
  fetchUsersFx, fetchUserFx,
} from '../api';

export const $usersPending = pending({
  effects: [fetchUsersFx, fetchUserFx],
  of: 'some',
});
