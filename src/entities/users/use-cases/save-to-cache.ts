import { persist } from 'effector-storage/local';
import { $users } from '../model';

persist({
  store: $users,
  key: 'users',
});
