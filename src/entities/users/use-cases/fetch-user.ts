import { sample } from 'effector';
import { $users, userRequested } from '../model';
import { TSmallUser } from '../types';
import { fetchUserFx } from '../api';

sample({
  clock: userRequested,
  fn: (user) => ({ userId: user }),
  target: fetchUserFx,
});

sample({
  clock: fetchUserFx.doneData,
  source: $users,
  fn: (users, result: TSmallUser) => ({ ...users, [result.id]: result }),
  target: $users,
});
