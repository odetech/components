import {
  $users,
  userRequested,
  usersRequested,
} from './model';
import * as UsersTypes from './types';
import * as api from './api';
import './use-cases/save-to-cache';
import './use-cases/fetch-user';
import './use-cases/fetch-users';
import { $usersPending } from './use-cases/get-status-requests';

export const $$usersModel = {
  $users,
  $usersPending,
  events: {
    userRequested,
    usersRequested,
  },
  api,
};

export { UsersTypes };
