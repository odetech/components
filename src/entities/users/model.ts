import {
  createEvent,
  createStore,
} from 'effector';
import { TSmallUser } from './types';

const $users = createStore<Record<string, TSmallUser>>({});
const userRequested = createEvent<string>();
const usersRequested = createEvent<string[]>();

export {
  $users,
  userRequested,
  usersRequested,
};
