import { fromArrayToHash } from '~/helpers/globalHelpers';

export const mapSubTasks = (response: any, subTasksOrder: string[] | null) => {
  const { data } = response;

  data.sort((a: any, b: any) => {
    if (subTasksOrder?.length) {
      return subTasksOrder.indexOf(a._id) - subTasksOrder.indexOf(b._id);
    }

    return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
  });

  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  return fromArrayToHash(data.map(mapSubTask));
};
export const mapSubTask = ({ _id, title, activity }: any) => ({
  id: _id,
  title: title || 'Unnamed subtask',
  activity,
});
