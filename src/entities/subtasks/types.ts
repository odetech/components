export type TSubTask = {
  id: string;
  title: string;
  activity: number;
};
