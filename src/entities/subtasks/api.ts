import { attach, createEffect } from 'effector';
import OdeCloud from '@odecloud/odecloud';
import { $api } from '~/config/init';
import { mapSubTask, mapSubTasks } from '~/entities/subtasks/lib';

type CreateProps = { api: OdeCloud, payload: any };
type EditProps = { api: OdeCloud, payload: any, taskId: string };
type DeleteProps = { api: OdeCloud, taskId: string };
type FetchProps = { api: OdeCloud, taskId: string, taskOrder: string[] };

const createSubApiTask = createEffect<CreateProps, any>(
  async ({ api, payload }) => {
    const newSubTask = await api.tasks.post(payload);

    return mapSubTask(newSubTask);
  },
);

const fetchSubApiTasks = createEffect<FetchProps, any>(
  async ({ api, taskId, taskOrder }) => {
    // @ts-ignore
    const newSubTask: any = await api.tasks.getTasks({ subTaskId: taskId });

    return mapSubTasks(newSubTask, taskOrder);
  },
);

const editSubApiTask = createEffect<EditProps, any>(
  async ({ api, payload, taskId }) => {
    const newSubTask: any = await api.tasks.patch(taskId, payload);

    return mapSubTask(newSubTask);
  },
);

const deleteSubApiTask = createEffect<DeleteProps, any>(
  async ({ api, taskId }) => api.tasks.delete(taskId),
);

export const createSubTaskFx = attach({
  effect: createSubApiTask,
  source: $api,
  mapParams: ({ payload }: any, api) => ({ payload, api: api! }),
});

export const fetchSubTasksFx = attach({
  effect: fetchSubApiTasks,
  source: $api,
  mapParams: ({ taskId, taskOrder }: any, api) => ({ taskId, taskOrder, api: api! }),
});

export const editSubTaskFx = attach({
  effect: editSubApiTask,
  source: $api,
  mapParams: ({ payload, taskId }: any, api) => ({ payload, taskId, api: api! }),
});

export const deleteSubTaskFx = attach({
  effect: deleteSubApiTask,
  source: $api,
  mapParams: ({ taskId }: any, api) => ({ taskId, api: api! }),
});
