import {
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { debug, pending } from 'patronum';
import { showToast } from '~/components/default-components';
import { TSubTask } from './types';
import {
  createSubTaskFx, deleteSubTaskFx, editSubTaskFx, fetchSubTasksFx,
} from './api';

const $subtasks = createStore<Record<string, TSubTask>>({});
const fetched = createEvent<{ taskId: string; taskOrder: string[] | null }>();
const created = createEvent<{ payload: any }>();
const edited = createEvent<{ payload: any; taskId: string; }>();
const deleted = createEvent<{ taskId: string }>();

sample({ clock: fetched, target: fetchSubTasksFx });
sample({ clock: created, target: createSubTaskFx });
sample({ clock: edited, target: editSubTaskFx });
sample({ clock: deleted, target: deleteSubTaskFx });

sample({ clock: fetchSubTasksFx.doneData, target: $subtasks });
sample({
  clock: [createSubTaskFx.doneData, editSubTaskFx.doneData],
  source: $subtasks,
  fn: (subtasks, result) => ({
    ...subtasks,
    [result.id]: result,
  }),
  target: $subtasks,
});

sample({
  clock: deleteSubTaskFx.done,
  source: $subtasks,
  fn: (subtasks, { params }) => {
    const copySubtasks: Record<string, TSubTask> = structuredClone(subtasks);
    delete copySubtasks[params.taskId];
    return copySubtasks;
  },
  target: $subtasks,
});

sample({
  clock: deleteSubTaskFx.done,
  target: createEffect(() => {
    showToast({
      message: 'Subtask deleted',
      type: 'success',
    });
  }),
});

const $pending = pending({
  effects: [
    createSubTaskFx,
    editSubTaskFx,
    deleteSubTaskFx,
    fetchSubTasksFx,
  ],
  of: 'some',
});

export {
  $subtasks,
  $pending,
  fetched,
  deleted,
  edited,
  created,
};
