import { createEffect } from 'effector';
import { Api, attachWithApi } from '~/config/init';
import { mapTask, mapTasks } from '~/entities/tasks/lib';
import { TFilterTasks, TSearchFilter, TTask } from '~/entities/tasks/types';
import { createTask } from './model';

type FetchProps = { taskId: string };
type DeleteProps = { taskId: string };

type ExtraOptions = {
  data: string[];
  limit: number;
  sortBy?: string;
  orderBy?: string;
  pageNumber?: number;
};
type FetchManyProps = {
  filter: TSearchFilter;
  allActivities?: boolean;
};
export type FetchTasksManyProps = FetchManyProps & {
  fetchAdditionalTasks?: boolean;
  filter?: TFilterTasks;
  pageNumber: number;
  limit?: number;
  sortBy?: string;
  orderBy?: string;
};
type TasksResponses = {
  data: Record<string, never>[];
  hasNextPage: boolean;
  pageNumber: number;
  pageSize: number;
  total: number;
};
type EditProps = {
  payload: Partial<TTask>;
  userId: string;
  taskId: string;
  hash?: string;
  updatedIndexes?: { taskId: string }[];
};

type TasksMapings = {
  elements: Record<string, TTask>;
  hasNextPage: boolean;
  pageNumber: number;
  pageSize: number;
  total: { [key: number]: number };
};

const fetchTaskApiFx = createEffect<FetchProps & Api, TTask>(
  async ({ api, taskId }) => {
    const taskResult = await api.tasks.getTask(taskId);
    return mapTask(taskResult);
  },
);

const searchTasksApiFx = createEffect<FetchManyProps & Api, TasksMapings>(
  async ({ api, filter, allActivities }) => {
    const extraOptions: ExtraOptions = {
      data: [
        'assignedToUserId',
        'commentsCount',
        'subTaskStats',
        'notificationSettings',
      ],
      limit: 10,
    };
    const copyFilter: ExtraOptions & TSearchFilter = {
      ...filter,
      ...extraOptions,
    };
    if (allActivities) {
      if (Array.isArray(copyFilter.projectId)) delete copyFilter.projectId;
      const tasksResults: TasksResponses[] = await Promise.all([
        api.tasks.search({ activity: 1, ...copyFilter }),
        api.tasks.search({ activity: 2, ...copyFilter }),
        api.tasks.search({ activity: 3, ...copyFilter }),
        api.tasks.search({ activity: 4, ...copyFilter }),
      ]);
      const allTasks = [
        ...tasksResults[0].data,
        ...tasksResults[1].data,
        ...tasksResults[2].data,
        ...tasksResults[3].data,
      ];
      const response = {
        data: allTasks,
        hasNextPage:
          tasksResults[0].hasNextPage
          || tasksResults[1].hasNextPage
          || tasksResults[2].hasNextPage
          || tasksResults[3].hasNextPage,
        pageNumber: tasksResults[0].pageNumber,
        pageSize: tasksResults[0].pageSize,
        total: {
          0: tasksResults[0].total,
          1: tasksResults[1].total,
          2: tasksResults[2].total,
          3: tasksResults[3].total,
        },
      };
      return mapTasks(response);
    }
    if (Array.isArray(copyFilter.projectId)) delete copyFilter.projectId;
    const tasksResult = await api.tasks.search({ ...copyFilter });
    return mapTasks(tasksResult);
  },
);

const editTaskApiFx = createEffect<EditProps & Api, TTask>(
  async ({
    api, payload, userId, taskId,
  }) => {
    const extraOptions: any = {
      data: [
        'assignedToUserId',
        'commentsCount',
        'subTaskStats',
        'notificationSettings',
      ],
      limit: 10,
    };
    const body = { ...payload, ...extraOptions, updatedBy: userId };

    if (body.hashtags?.length) {
      body.hashtags = body.hashtags.map((str: string) => (str[0] === '#' ? str : `#${str}`));
    }
    const result: any = await api.tasks.patch(taskId, body);
    return mapTask(result);
  },
);

const deleteTaskApiFx = createEffect<DeleteProps & Api, void>(
  async ({ api, taskId }) => {
    await api.tasks.delete(taskId);
  },
);

const fetchTasksApiFx = createEffect<FetchTasksManyProps & Api, TasksMapings>(
  async ({
    api,
    fetchAdditionalTasks,
    pageNumber,
    limit = 10,
    sortBy,
    orderBy,
    filter,
    allActivities,
  }) => {
    const extraOptions: ExtraOptions = {
      data: [
        'assignedToUserId',
        'commentsCount',
        'subTaskStats',
        'notificationSettings',
      ],
      limit,
    };

    if (sortBy && orderBy) {
      extraOptions.sortBy = sortBy;
      extraOptions.orderBy = orderBy;
    }

    if (fetchAdditionalTasks) {
      extraOptions.pageNumber = pageNumber + 1;
    }
    const requiredFilter = { ...filter, ...extraOptions };
    if (allActivities && requiredFilter.activity === undefined) {
      const tasksResults = await Promise.all([
        api.tasks.getTasks({ activity: 1, ...requiredFilter }),
        api.tasks.getTasks({ activity: 2, ...requiredFilter }),
        api.tasks.getTasks({ activity: 3, ...requiredFilter }),
        api.tasks.getTasks({ activity: 4, ...requiredFilter }),
      ]);
      const allTasks = [
        ...tasksResults[0].data,
        ...tasksResults[1].data,
        ...tasksResults[2].data,
        ...tasksResults[3].data,
      ];
      const response = {
        data: allTasks,
        hasNextPage:
          tasksResults[0].hasNextPage
          || tasksResults[1].hasNextPage
          || tasksResults[2].hasNextPage
          || tasksResults[3].hasNextPage,
        pageNumber: tasksResults[0].pageNumber,
        pageSize: tasksResults[0].pageSize,
        total: {
          0: tasksResults[0].total,
          1: tasksResults[1].total,
          2: tasksResults[2].total,
          3: tasksResults[3].total,
        },
      };
      return mapTasks(response);
    }
    const tasksResult = await api.tasks.getTasks({
      ...filter,
      ...extraOptions,
    });
    return mapTasks(tasksResult);
  },
);

const fetchConversationsTasksApiFx = createEffect<
  FetchTasksManyProps & Api,
  TasksMapings
>(
  async ({
    api,
    fetchAdditionalTasks,
    pageNumber,
    limit = 10,
    sortBy,
    orderBy,
    filter,
  }) => {
    const extraOptions: ExtraOptions = {
      data: [
        'assignedToUserId',
        'commentsCount',
        'subTaskStats',
        'notificationSettings',
      ],
      limit,
    };

    if (sortBy && orderBy) {
      extraOptions.sortBy = sortBy;
      extraOptions.orderBy = orderBy;
    }

    if (fetchAdditionalTasks) {
      extraOptions.pageNumber = pageNumber + 1;
    }
    const copiedFilter = { ...filter, ...extraOptions };
    const tasksResult: any = await api.projects.getMagic({
      ...copiedFilter,
      ...extraOptions,
    });
    return mapTasks(tasksResult.tasks);
  },
);

const fetchConversationsApiFx = createEffect<
  FetchTasksManyProps & Api & { queryTask?: string },
  TasksMapings
>(
  async ({
    api,
    fetchAdditionalTasks,
    pageNumber,
    limit = 10,
    sortBy,
    orderBy,
    queryTask,
    filter,
  }) => {
    const extraOptions: ExtraOptions = {
      data: [
        'assignedToUserId',
        'commentsCount',
        'subTaskStats',
        'notificationSettings',
      ],
      limit,
    };

    if (sortBy && orderBy) {
      extraOptions.sortBy = sortBy;
      extraOptions.orderBy = orderBy;
    }

    if (fetchAdditionalTasks) {
      extraOptions.pageNumber = pageNumber + 1;
    }
    let tasksResult = null;
    const copiedFilter = { ...filter, ...extraOptions };
    if (queryTask) {
      const magicResult: any = await api.projects.getMagic({ ...copiedFilter });
      const requiredTask = await api.tasks.getTask(queryTask, {
        ...copiedFilter,
      });
      tasksResult = {
        ...magicResult.tasks,
      };
      tasksResult.data.unshift(requiredTask);
    } else {
      tasksResult = await api.projects.getMagic({ ...copiedFilter });
      tasksResult = tasksResult.tasks;
    }
    return mapTasks(tasksResult);
  },
);

export const searchTasksFx = attachWithApi<FetchManyProps, TasksMapings>(
  searchTasksApiFx,
);
export const editTaskFx = attachWithApi<EditProps, TTask>(editTaskApiFx);

export const fetchTaskFx = attachWithApi<FetchProps, TTask>(fetchTaskApiFx);
export const fetchTasksFx = attachWithApi<FetchTasksManyProps, TasksMapings>(
  fetchTasksApiFx,
);
export const fetchConversationsTasksFx = attachWithApi<
  FetchTasksManyProps,
  TasksMapings
>(fetchConversationsTasksApiFx);
export const fetchConversationsFx = attachWithApi<
  FetchTasksManyProps & { queryTask?: string },
  TasksMapings
>(fetchConversationsApiFx);
export const deleteTaskFx = attachWithApi<DeleteProps, void>(deleteTaskApiFx);
