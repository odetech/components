import { sample } from 'effector';
import { tasksRequested } from '../model';
import { fetchTasksFx } from '../api';

sample({
  clock: tasksRequested,
  target: fetchTasksFx,
});
