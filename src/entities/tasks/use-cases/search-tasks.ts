import { sample } from 'effector';
import { spread } from 'patronum';
import {
  $tasks,
  $tasksPageNumber,
  $tasksHasNextPage,
  $tasksPageSize,
  $tasksTotal,
  tasksSearched,
} from '../model';
import { searchTasksFx } from '../api';

sample({
  clock: tasksSearched,
  target: searchTasksFx,
});

sample({
  clock: searchTasksFx.doneData,
  target: spread({
    targets: {
      elements: $tasks,
      hasNextPage: $tasksHasNextPage,
      pageNumber: $tasksPageNumber,
      pageSize: $tasksPageSize,
      total: $tasksTotal,
    },
  }),
});
