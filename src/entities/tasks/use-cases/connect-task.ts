import { createEvent, createEffect, sample } from 'effector';
import { $$socketModel } from '~/entities/ikinari';
import { Socket, Channel } from 'phoenix';
import {
  $taskChannel, socketSent, taskUpdated,
} from '../model';

const taskConnected = createEvent<string>();
const taskDisconnected = createEvent();

const failed = createEvent<unknown>();

export {
  taskConnected,
  taskDisconnected,
};

const connectChannelFx = createEffect((
  {
    socket, taskId,
  }:
    { socket: Socket | null, taskId: string },
) => {
  if (!socket) return null;
  const connectedChannel = socket.channel(`tasks:${taskId}`, {});
  connectedChannel.onError(console.log);
  connectedChannel.join()
    .receive('ok', () => console.log('connected'))
    .receive('error', (e: unknown) => failed(e));
  connectedChannel.on('update_task', (response: any) => {
    taskUpdated(response.data);
  });
  return connectedChannel;
});

sample({
  clock: taskConnected,
  source: {
    socket: $$socketModel.$socket,
    isConnected: $$socketModel.$isConnected,
  },
  filter: ({ isConnected }) => isConnected,
  fn: ({ socket }, taskId) => ({
    socket,
    taskId,
  }),
  target: connectChannelFx,
});

sample({
  clock: connectChannelFx.doneData,
  target: $taskChannel,
});

sample({
  clock: connectChannelFx.fail,
  fn: ({ params: { socket }, error }) => ({ socket, e: error }),
  target: createEffect(({ socket }: { socket: Socket | null, e: unknown }) => {
    if (socket) socket.disconnect();
  }),
});

sample({
  clock: [
    connectChannelFx.failData,
    failed,
  ],
  target: createEffect((error: unknown) => console.error(JSON.stringify(error))),
});

sample({
  clock: taskDisconnected,
  source: $taskChannel,
  target: createEffect((channel: Channel | null) => {
    if (!channel) return false;
    channel.leave();
    return true;
  }),
});

const sentFx = createEffect(async ({
  channel,
  type,
  payload,
}: {
  channel: Channel | null;
  type: string;
  payload: Record<string, unknown>;
}) => {
  if (!channel) return;
  channel.push(type, payload);
});

sample({
  clock: socketSent,
  source: {
    channel: $taskChannel,
  },
  fn: ({
    channel,
  }, {
    type,
    payload,
  }) => ({
    channel,
    type,
    payload,
  }),
  target: sentFx,
});
