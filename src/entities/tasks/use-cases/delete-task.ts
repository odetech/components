import { sample } from 'effector';
import { TTask } from '~/entities/tasks/types';
import {
  $tasks,
  taskDeleted,
} from '../model';
import { deleteTaskFx } from '../api';

sample({
  clock: taskDeleted,
  fn: (taskId) => ({ taskId }),
  target: deleteTaskFx,
});

sample({
  clock: taskDeleted,
  source: $tasks,
  fn: (tasks, taskId) => {
    const copyTasks: Record<string, TTask> = { ...tasks };
    delete copyTasks[taskId];
    return copyTasks;
  },
  target: $tasks,
});
