import { sample } from 'effector';
import { $tasks, createTask, createTaskDone, taskCreated } from '../model';
import { $$teamModel } from '../../team';
import { mapTask } from '../../tasks/lib';
import { subtasksCreated } from '../../../modules/task-drawer/model/subtasks'
import { $$trackerCreateTaskModel } from '../../../modules/task-drawer/ui/widgets/time-tracker/model'

sample({
  clock: taskCreated,
  source: $tasks,
  filter: (_tasks, { payload }) => {
    return !!payload._id
  },
  fn: (tasks, { payload }) => {
    const newTask = mapTask(payload)
    const id = newTask.id

    const updatedTasks = {
      ...tasks,
      [id]: newTask
    }

    return updatedTasks
  },
  target: $tasks,
});

sample({
  clock: createTask,
  fn: ({ payload }) => {
    return {
      type: 'create_task',
      payload,
      onSuccess: (response: any) => {
        if (response && response.id) {
          const id = response.id as string;

          subtasksCreated({ projectId: payload.projectId!, taskId: id });
          $$trackerCreateTaskModel.taskCreated(id);
          createTaskDone({ id })
        }
      },
      onError: (response: unknown, reason: string) => {
        console.error("Couldn't create task with ikinari", { response, reason })
        createTaskDone({})
      }
    }
  },
  target: $$teamModel.events.socketSent,
});

