import { pending, or } from 'patronum';
import {
  editTaskFx,
  searchTasksFx,
  fetchConversationsFx,
  fetchTasksFx,
  fetchTaskFx,
  fetchConversationsTasksFx,
  deleteTaskFx,
} from '../api';
import { createStore, sample } from 'effector';
import { createTask, createTaskDone } from '../model';

export const $submittingTaskForm = createStore(false)

sample({
  clock: createTask,
  source: $submittingTaskForm,
  fn: () => {
    return true;
  },
  target: $submittingTaskForm
});

sample({
  clock: createTaskDone,
  source: $submittingTaskForm,
  fn: () => {
    return false;
  },
  target: $submittingTaskForm
});

export const $tasksLoading = or(
  pending({
    effects: [
      searchTasksFx,
      fetchConversationsFx,
      fetchTasksFx,
      fetchTaskFx,
      fetchConversationsTasksFx,
      deleteTaskFx,
    ],
    of: 'some',
  }),
  $submittingTaskForm
);

export const $tasksSilentLoading = pending({
  effects: [editTaskFx],
  of: 'some',
});

export const $tasksAdditionalLoading = pending({
  effects: [],
  of: 'some',
});

export const $taskLoading = pending({
  effects: [],
  of: 'some',
});
