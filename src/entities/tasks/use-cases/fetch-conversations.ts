import { sample } from 'effector';
import { conversationsRequested } from '../model';
import { fetchConversationsFx } from '../api';

sample({
  clock: conversationsRequested,
  target: fetchConversationsFx,
});
