import { sample } from 'effector';
import { $currentUser } from '~/config';
import { spread } from 'patronum';
import {
  $tasks,
  taskEdited,
  safetyEdited,
  taskUpdated,
} from '../model';
import { editTaskFx } from '../api';
import { mapTask } from '../lib';

sample({
  clock: taskEdited,
  source: { user: $currentUser, tasks: $tasks },
  fn: ({ user, tasks }, { payload, ...rest }) => {
    const changesTask = { ...payload };
    if (changesTask?.hashtags?.length) {
      changesTask.hashtags = Array.from(
        new Set(changesTask.hashtags.map((str: string) => (str[0] === '#' ? str : `#${str}`))),
      );
    }
    const hash = (crypto as any).randomUUID();
    return {
      effect: {
        ...rest, payload, userId: user._id, hash,
      },
      savedParams: { task: tasks[rest.taskId], hash },
    };
  },
  target: spread({
    targets: {
      effect: editTaskFx,
      savedParams: safetyEdited,
    },
  }),
});

sample({
  clock: taskEdited,
  source: $tasks,
  fn: (tasks, { taskId, payload, updatedIndexes }) => {
    const copyTasks = { ...tasks };
    copyTasks[taskId] = {
      ...copyTasks[taskId],
      ...payload,
    };

    if (updatedIndexes?.length) {
      updatedIndexes.forEach((item, index) => {
        copyTasks[item.taskId] = { ...copyTasks[item.taskId], index };
      });
    }
    return copyTasks;
  },
  target: $tasks,
});

sample({
  clock: taskUpdated,
  source: $tasks,
  fn: (tasks, payload) => {
    const task = mapTask(payload);
    // @ts-ignore
    delete task.uuid;
    const copyTasks = { ...tasks };
    copyTasks[task.id] = {
      ...copyTasks[task.id],
      ...task,
    };
    return copyTasks;
  },
  target: $tasks,
});
