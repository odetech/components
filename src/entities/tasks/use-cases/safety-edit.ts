import { sample } from 'effector';
import { spread } from 'patronum';
import { editTaskFx } from '../api';
import { safetyEdited, $previousStageTasks, $tasks } from '../model';

sample({
  clock: safetyEdited,
  source: $previousStageTasks,
  fn: (previousStageTasks, { task, hash }) => ({ ...previousStageTasks, [hash]: task }),
  target: $previousStageTasks,
});

sample({
  clock: editTaskFx.done,
  source: $previousStageTasks,
  fn: (previousStageTasks, { params }) => {
    const previous = { ...previousStageTasks };
    if (params.hash) {
      delete previous[params.hash];
    }
    return { ...previous };
  },
  target: $previousStageTasks,
});

sample({
  clock: editTaskFx.fail,
  source: { previousStageTasks: $previousStageTasks, tasks: $tasks },
  fn: ({ previousStageTasks, tasks }, { params }) => {
    const previous = { ...previousStageTasks };
    if (params.hash) {
      const currentTask = { ...previous[params.hash] };
      delete previous[params.hash];
      return { previous, tasks: { ...tasks, [currentTask.id]: currentTask } };
    }
    return { previous, tasks };
  },
  target: spread({
    targets: {
      previous: $previousStageTasks,
      tasks: $tasks,
    },
  }),
});
