import { sample } from 'effector';
import { conversationsTasksRequested } from '../model';
import { fetchConversationsTasksFx } from '../api';

sample({
  clock: conversationsTasksRequested,
  target: fetchConversationsTasksFx,
});
