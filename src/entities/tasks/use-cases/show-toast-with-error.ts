import { createEffect, sample } from 'effector';
import { showToast } from '~/components/default-components';
import {
  fetchTaskFx,
  deleteTaskFx,
  editTaskFx,
  searchTasksFx,
  fetchConversationsFx,
  fetchTasksFx,
  fetchConversationsTasksFx,
} from '../api';

sample({
  clock: [
    searchTasksFx.failData,
    editTaskFx.failData,
    deleteTaskFx.failData,
    fetchTaskFx.failData,
    fetchTasksFx.failData,
    fetchConversationsTasksFx.failData,
    fetchConversationsFx.failData,
  ],
  target: createEffect(async (e: any) => {
    const errorData = await e.response.json();
    if (errorData?.detail) {
      showToast({
        message: errorData?.detail,
        type: 'error',
      });
    }
  }),
});
