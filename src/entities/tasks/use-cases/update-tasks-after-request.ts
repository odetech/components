import { sample } from 'effector';
import { spread } from 'patronum';
import { fetchConversationsFx, fetchConversationsTasksFx, fetchTasksFx } from '../api';
import {
  $tasks, $tasksHasNextPage, $tasksPageNumber, $tasksPageSize, $tasksTotal,
} from '../model';

sample({
  clock: [
    fetchConversationsFx.done, fetchConversationsTasksFx.done, fetchTasksFx.done,
  ],
  source: $tasks,
  fn: (tasks, { params, result }) => {
    if (params.fetchAdditionalTasks) {
      return ({
        elements: {
          ...tasks,
          ...result.elements,
        },
        hasNextPage: result.hasNextPage,
        pageNumber: result.pageNumber,
        pageSize: result.pageSize,
        total: result.total,
      });
    }

    return {
      elements: result.elements,
      hasNextPage: result.hasNextPage,
      pageNumber: result.pageNumber,
      pageSize: result.pageSize,
      total: result.total,
    };
  },
  target: spread({
    targets: {
      elements: $tasks,
      hasNextPage: $tasksHasNextPage,
      pageNumber: $tasksPageNumber,
      pageSize: $tasksPageSize,
      total: $tasksTotal,
    },
  }),
});
