import { sample } from 'effector';
import { TTask } from '../types';
import { $tasks, taskRequested } from '../model';
import { fetchTaskFx } from '../api';

sample({
  clock: taskRequested,
  fn: (taskId) => ({ taskId }),
  target: fetchTaskFx,
});

sample({
  clock: fetchTaskFx.doneData,
  source: $tasks,
  fn: (tasks, result: TTask) => ({ ...tasks, [result.id]: result }),
  target: $tasks,
});
