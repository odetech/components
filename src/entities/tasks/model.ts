import { createEvent, createStore } from 'effector';
import { Channel } from 'phoenix';
import { FetchTasksManyProps } from './api';
import { TSearchFilter, TTask } from './types';

const $tasks = createStore<Record<string, TTask>>({});
const $previousStageTasks = createStore<Record<string, TTask>>({});
const $tasksHasNextPage = createStore<boolean>(false);
const $tasksPageNumber = createStore<number>(0);
const $tasksPageSize = createStore<number>(0);
const $tasksTotal = createStore<{ [key: number]: number }>({
  0: 0, 1: 0, 2: 0, 3: 0,
});

const tasksSearched = createEvent<{ filter: TSearchFilter, allActivities?: boolean }>();
const tasksRequested = createEvent<FetchTasksManyProps>();
const conversationsRequested = createEvent<FetchTasksManyProps & { queryTask?: string }>();
const conversationsTasksRequested = createEvent<FetchTasksManyProps>();
const taskRequested = createEvent<string>();
const taskEdited = createEvent<{ taskId: string, payload: Partial<TTask>, updatedIndexes?: { taskId: string }[] }>();
const taskDeleted = createEvent<string>();

/**
 * Emitted with the intent of creating a new task.
 */
const createTask = createEvent<{ payload: Partial<TTask>, shouldUpdate?: boolean, isDuplicate?: boolean }>();

export type TaskCreatedPayload = {
  _id: string,
  title: string,
  orgId: string,
  description: string,
  priority: number,
  activity: number,
  data: {},
  assignedBy: string,
  requestedBy: string,
  dueDate: Date,
  projectId: string,
  createdBy: string,
  createdAt: Date,
  updatedAt: Date,
  assignedToUserId: string,
  subTasksOrder: string,
  estimatedTime: number,
  lastMessage: string,
  hashtags: string[],
  followers: string[],
  currentTime: number,
  uuid: {},
}
/**
 * Emitted when a new task has been created.
 * Can be created by other users.
 */
const taskCreated = createEvent<{ payload: TaskCreatedPayload }>();


/**
 * Emitted when a task, created by the logged-in user, has been created.
 * Only emitted for the current tab after the currently logged-in user finished creating a task.
 *
 * This event helps us close the task drawer upon creating a new task.
 */
const createTaskDone = createEvent<{ id?: string }>();

const safetyEdited = createEvent<{ task: TTask, hash: string }>();

const $taskChannel = createStore<Channel | null>(null);
const taskUpdated = createEvent<TTask>();
const socketSent = createEvent<{
  type: string,
  payload: Record<string, unknown>,
}>();

export {
  $tasks,
  $taskChannel,
  taskUpdated,
  $previousStageTasks,
  $tasksTotal,
  $tasksPageNumber,
  $tasksHasNextPage,
  $tasksPageSize,
  tasksSearched,
  taskRequested,
  taskDeleted,
  taskEdited,
  tasksRequested,
  socketSent,
  conversationsRequested,
  conversationsTasksRequested,
  taskCreated,
  createTask,
  createTaskDone,
  safetyEdited,
};
