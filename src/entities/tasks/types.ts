export type TAssignedToUser = {
  name: string | null;
  firstName?: string;
  shortName?: string;
  lastName?: string;
  id: string;
  avatarUrl?: string;
  email?: string;
};

export type TTask = {
  title: string,
  orgId: string,
  description: string;
  assignedToUserEntity: TAssignedToUser;
  assignedByUserEntity: TAssignedToUser;
  requestedByUserEntity: TAssignedToUser;
  assignedToUserId: string;
  id: string;
  priority: number;
  activity: number;
  dueDate: string;
  commentsCount: number;
  projectId: string;
  createdBy: string;
  hashtags: string[];
  followers?: string[];
  requestedBy: string;
  assignedBy: string;
  subTaskStats: {
    finished: number;
    ongoing: number;
  },
  notificationSettings: {
    id: string;
    currentFrequencies: string[];
  } | null,
  subTasksOrder: string[] | null;
  lastMessage?: { savedAt: string };
  updatedAt: string;
  uuid: string;
  createdAt: string | null;
  subTasks?: Record<string, TSubTask>;
  index?: number,
  estimatedTime?: number;
  currentTime?: number;
};

export type TSubTask = {
  id: string;
  title: string;
  activity: number;
};

export type TFilter = {
  orgId?: string;
  projectId?: string;
  assignedToUserId?: string;
  activity?: number;
  priority?: number;
  createdAt?: string;
  createdAtStart?: string;
  createdAtEnd?: string;
  dueDate?: string;
  dueDateStart?: string;
  dueDateEnd?: string;
  search?: string;
  hashtags?: string[];
};

export type TFilterTasks = TFilter & {
  projectId?: string[];
};

export type TSearchFilter = TFilter & Required<Pick<TFilter, 'search'>>;
