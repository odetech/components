import * as Yup from 'yup';
import { $api } from '~/config';
import { showToast } from '~/components/default-components';
import { fromArrayToHash, showCloud } from '~/helpers/globalHelpers';
import copy from 'copy-to-clipboard';
import { ensureUtcTimestamp } from '~/helpers';

export const mapTask = ({
  _id,
  title,
  orgId,
  description,
  priority,
  activity,
  data = {},
  assignedBy,
  requestedBy,
  dueDate,
  projectId,
  createdBy,
  createdAt,
  updatedAt,
  assignedToUserId,
  subTasksOrder,
  estimatedTime,
  lastMessage,
  hashtags,
  followers,
  currentTime,
  uuid,
}: any) => {
  const assignedToUserName = data.assignedToUserId
    ? `${data.assignedToUserId?.profile?.firstName} ${data.assignedToUserId?.profile?.lastName}`
    : null;
  const assignedByUserName = data.assignedByUserId
    ? `${data.assignedByUserId?.profile?.firstName} ${data.assignedByUserId?.profile?.lastName}`
    : null;
  const requestedByUserName = data.requestedByUserId
    ? `${data.requestedByUserId?.profile?.firstName} ${data.requestedByUserId?.profile?.lastName}`
    : null;
  return ({
    title,
    orgId,
    followers: followers?.length ? followers : [],
    hashtags: hashtags.map((str: string) => (str[0] === '#' ? str.substring(1) : str)),
    description,
    assignedToUserEntity: {
      name: assignedToUserName,
      firstName: data.assignedToUserId?.profile?.firstName,
      lastName: data.assignedToUserId?.profile?.lastName,
      avatarUrl: data.assignedToUserId?.profile?.avatar?.secureUrl,
      id: data.assignedToUserId?._id,
    },
    assignedByUserEntity: {
      name: assignedByUserName,
      firstName: data.assignedByUserId?.profile?.firstName,
      lastName: data.assignedByUserId?.profile?.lastName,
      avatarUrl: data.assignedByUserId?.profile?.avatar?.secureUrl,
      id: data.assignedByUserId?._id,
    },
    requestedByUserEntity: {
      name: requestedByUserName,
      firstName: data.createdBy?.profile?.firstName,
      lastName: data.createdBy?.profile?.lastName,
      avatarUrl: data.createdBy?.profile?.avatar?.secureUrl,
      id: data.createdBy?._id,
    },
    assignedToUserId,
    id: _id,
    priority,
    activity,
    commentsCount: data?.commentsCount,
    dueDate: (dueDate || '').includes('Z') ? dueDate.slice(0, -1) : dueDate, // because it's setting time, and we need to set it directly to required date without timezone or something else.
    projectId,
    estimatedTime,
    currentTime,
    createdBy,
    assignedBy,
    requestedBy,
    createdAt: createdAt ? ensureUtcTimestamp(createdAt) : null,
    lastMessage,
    updatedAt: ensureUtcTimestamp(updatedAt),
    subTaskStats: data?.subTaskStats,
    notificationSettings: data?.notificationSettings ? {
      id: data?.notificationSettings?._id,
      currentFrequencies: data?.notificationSettings?.currentFrequencies,
    } : null,
    subTasksOrder,
    uuid: uuid ? `${uuid.strValue}-${uuid.intValue}` : 'Without TASK UUID',
  });
};

export const mapTasks = (response: any, tasksData?: any) => {
  const mappedTasksData = response.data.map(mapTask);
  if (tasksData) {
    return ({
      elements: {
        ...tasksData.elements,
        ...fromArrayToHash(mappedTasksData),
      },
      hasNextPage: response.hasNextPage,
      pageNumber: response.pageNumber,
      pageSize: response.pageSize,
      total: response.total,
    });
  }

  return ({
    elements: fromArrayToHash(mappedTasksData),
    hasNextPage: response.hasNextPage,
    pageNumber: response.pageNumber,
    pageSize: response.pageSize,
    total: response.total,
  });
};

export const newTaskSchema = Yup.object().shape({
  title: Yup.string()
    .required('Title is required'),
  projectId: Yup.object().shape({}).nullable().required('Project is required'),
  orgId: Yup.object().shape({}).nullable().required('Client is required'),
});

export const copyLinkOnTask = async (
  taskId: string,
  isModal?: boolean,
  currentUserId?: string,
  el?: HTMLElement | null,
  props?: any,
) => {
  try {
    const payload = {
      link: isModal ? window.location.href : `${window.location.origin.toString()}/conversations?activeTask=${taskId}`,
      sharedBy: [currentUserId!],
      extra: {
        message: {
          _id: taskId,
        },
      },
    };

    const response = await $api.getState()?.links.post(payload, { origin: 'odetask' });
    copy(`${response?.shortLink}${currentUserId}`);
    if (el) showCloud(el, 'Copied', ...props);
    return `${response?.shortLink}${currentUserId}`;
  } catch (e: any) {
    showToast({
      message: e.toString(),
      type: 'error',
    });
    return e.toString();
  }
};
