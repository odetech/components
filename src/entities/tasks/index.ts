import {
  $tasks,
  $tasksTotal,
  $tasksPageSize,
  $tasksHasNextPage,
  $tasksPageNumber,
  $taskChannel,
  tasksSearched,
  taskUpdated,
  taskRequested,
  taskDeleted,
  taskEdited,
  tasksRequested,
  conversationsRequested,
  taskCreated,
  createTask,
  createTaskDone,
  socketSent,
  safetyEdited,
  conversationsTasksRequested,
} from './model';
import * as api from './api';
import {
  $tasksSilentLoading, $taskLoading, $tasksAdditionalLoading, $tasksLoading,
  $submittingTaskForm,
} from './use-cases/get-status-requests';
import './use-cases/edit-task';
import './use-cases/fetch-task';
import './use-cases/search-tasks';
import './use-cases/create-task';
import { taskConnected, taskDisconnected } from './use-cases/connect-task';
import './use-cases/fetch-conversations';
import './use-cases/fetch-conversations-tasks';
import './use-cases/fetch-tasks';
import './use-cases/update-tasks-after-request';
import './use-cases/show-toast-with-error';
import './use-cases/safety-edit';
import './use-cases/delete-task';
import { copyLinkOnTask, newTaskSchema } from './lib';

export const $$tasksModel = {
  $tasks,
  $taskChannel,
  $tasksTotal,
  $tasksPageSize,
  $tasksHasNextPage,
  $tasksPageNumber,
  $submittingTaskForm,
  $tasksSilentLoading,
  $taskLoading,
  $tasksAdditionalLoading,
  $tasksLoading,
  events: {
    socketSent,
    taskUpdated,
    taskDisconnected,
    taskConnected,
    tasksSearched,
    taskRequested,
    taskDeleted,
    taskEdited,
    tasksRequested,
    conversationsRequested,
    taskCreated,
    createTask,
    createTaskDone,
    safetyEdited,
    conversationsTasksRequested,
  },
  api,
};

export {
  copyLinkOnTask,
  newTaskSchema,
};
