import { attach, createEffect } from 'effector';
import OdeCloud from '@odecloud/odecloud';
import { $api, $currentUser } from '~/config/init';
import { NetworksDataProps } from '~/entities/networks/types';

type FetchProps = { api: OdeCloud, payload: any };
type DeleteProps = { api: OdeCloud, networkId: string | null };

const fetchNetworksApiFx = createEffect<FetchProps, NetworksDataProps[]>(
  async ({ api, payload }) => api.networks.getNetworks(payload),
);

const deleteNetworksApiFx = createEffect<DeleteProps, string | null>(
  async ({ api, networkId }) => {
    if (!networkId) return null;
    await api.networks.delete(networkId);
    return networkId;
  },
);

export const fetchNetworksFx = attach({
  effect: fetchNetworksApiFx,
  source: { api: $api, user: $currentUser },
  mapParams: (payload: any, { api }) => ({ api: api!, payload }),
});

export const fetchNetworkByIdFx = attach({
  effect: fetchNetworksApiFx,
  source: { api: $api, user: $currentUser },
  mapParams: (payload: any, { api }) => ({ api: api!, payload }),
});

export const deleteNetworksFx = attach({
  effect: deleteNetworksApiFx,
  source: { api: $api },
  mapParams: (networkId: string | null, { api }) => ({ api: api!, networkId }),
});
