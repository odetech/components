import {
  combine,
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { NetworksDataProps } from '~/entities/networks/types';
import {
  deleteNetworksFx, fetchNetworkByIdFx, fetchNetworksFx,
} from '~/entities/networks/api';
import { $currentUser, componentsStarted } from '~/config';
import { spread } from 'patronum';
import { fromArrayToHash } from '~/helpers/globalHelpers';
import { $$usersModel } from '~/entities/users';
import {
  socketSent, inviteGot, acceptedGot, outgoingGot,
} from './events';

type Networks = {
  networks: NetworksDataProps[],
  count: number,
}

const updateNetworksFx = createEffect(async ({
  networks, users, currentUserId,
}: {
  type: string,
  networks: NetworksDataProps[];
  users: Record<string, any>;
  currentUserId: string;
}) => {
  let copyNetworks: NetworksDataProps[] = structuredClone(networks);
  copyNetworks = await Promise.all(copyNetworks.map(async (network: NetworksDataProps) => {
    let keyId: keyof NetworksDataProps = 'userId2';
    let key: keyof NetworksDataProps = 'user2';
    if (currentUserId === network.userId2) {
      keyId = 'userId1';
      key = 'user1';
    }
    if (users[network[keyId]]) {
      const { _id, ...profile } = users[network[keyId]];
      return { ...network, [key]: { profile, _id } };
    }
    const { _id, ...profile }: any = await $$usersModel.api.fetchUserFx({ userId: network[keyId] });
    return { ...network, [key]: { profile, _id } };
  }));

  return copyNetworks;
});

const $networks = createStore<Networks>({ count: 0, networks: [] });
// const $connections = createStore<NetworkConnections>({ connectionCount: 0, connections: [] });
const $outgoingNetworks = createStore<NetworksDataProps[]>([]);
const $incomingNetworks = createStore<NetworksDataProps[]>([]);
const fetched = createEvent<any>();
const fetchNetworkByUserId = createEvent<string>();
const deleted = createEvent<string>();
const networkOutgoingGot = createEvent<NetworksDataProps>();
const networkIncomingGot = createEvent<NetworksDataProps>();

sample({
  clock: fetched,
  target: fetchNetworksFx,
});

sample({
  clock: fetchNetworkByUserId,
  source: $currentUser,
  fn: (currentUser, userId) => ({
    userId1: currentUser?._id || '', userId2: userId, isPending: false, limit: 1, pageNumber: 1,
  }),
  target: fetchNetworkByIdFx,
});

sample({
  clock: fetchNetworkByIdFx.doneData,
  source: $networks,
  fn: (networks, results) => {
    const newNetworks = Object.values(fromArrayToHash([...networks.networks, results[0]])) as NetworksDataProps[];
    return {
      networks: newNetworks,
      count: newNetworks.length,
    };
  },
  target: $networks,
});

sample({
  clock: deleted,
  source: $networks,
  fn: (networks, userId) => {
    const network = networks.networks.find((n) => n.userId2 === userId);
    if (network) {
      return network._id;
    }
    return null;
  },
  target: deleteNetworksFx,
});

sample({
  clock: [networkOutgoingGot, outgoingGot],
  source: { users: $$usersModel.$users, currentUser: $currentUser, networks: $outgoingNetworks },
  fn: ({ users, currentUser, networks }, result) => ({
    networks: Object.values(fromArrayToHash([...networks, result])) as NetworksDataProps[],
    type: 'outgoing',
    users,
    currentUserId: currentUser._id,
  }),
  target: updateNetworksFx,
});

sample({
  clock: [networkIncomingGot, inviteGot],
  source: { users: $$usersModel.$users, currentUser: $currentUser, networks: $incomingNetworks },
  fn: ({ users, currentUser, networks }, result) => ({
    networks: Object.values(fromArrayToHash([...networks, result])) as NetworksDataProps[],
    type: 'incoming',
    users,
    currentUserId: currentUser._id,
  }),
  target: updateNetworksFx,
});

const networkCreated = createEvent<{payloadData: {
    inviteEmail?: string,
    userId?: string,
  }}>();

const networkAccepted = createEvent<{payloadData: {
    userId1: string,
    userId2: string,
    _id?: string,
  }}>();

sample({
  clock: networkCreated,
  fn: (payload) => ({
    type: 'send_network_invite',
    payload: payload.payloadData,
  }),
  target: socketSent,
});

sample({
  clock: networkAccepted,
  fn: (payload) => ({
    type: 'accept_network_invite',
    payload: payload.payloadData,
  }),
  target: socketSent,
});

sample({
  clock: acceptedGot,
  source: {
    networks: $networks,
    currentUser: $currentUser,
    users: $$usersModel.$users,
    outgoing: $outgoingNetworks,
    incoming: $incomingNetworks,
  },
  fn: ({
    networks, incoming, outgoing, users, currentUser,
  }, data) => {
    const copyNetworks: NetworksDataProps[] = structuredClone(networks.networks);
    const copyIncomingNetworks: NetworksDataProps[] = structuredClone(incoming);
    const copyOutgoingNetworks: NetworksDataProps[] = structuredClone(outgoing);
    // eslint-disable-next-line max-len
    const incomingIndex = copyIncomingNetworks.findIndex((t) => t.userId2 === data.userId1 && t.userId1 === data.userId2);
    // eslint-disable-next-line max-len
    const outgoingIndex = copyOutgoingNetworks.findIndex((t) => t.userId1 === data.userId1 && t.userId2 === data.userId2);
    if (incomingIndex !== -1) {
      copyIncomingNetworks.splice(incomingIndex, 1);
    }
    if (outgoingIndex !== -1) {
      copyOutgoingNetworks.splice(outgoingIndex, 1);
    }
    // @ts-ignore
    copyNetworks.push(data);
    return {
      networks: {
        networks: copyNetworks,
        type: 'networks',
        users,
        currentUserId: currentUser._id,
      },
      outgoing: copyOutgoingNetworks,
      incoming: copyIncomingNetworks,
    };
  },
  target: spread({
    targets: {
      networks: updateNetworksFx,
      outgoing: $outgoingNetworks,
      incoming: $incomingNetworks,
    },
  }),
});

sample({
  clock: fetchNetworksFx.done,
  filter: ({ params }) => !params.isPending,
  fn: (data) => ({
    count: data.result.length,
    networks: data.result,
  }),
  target: $networks,
});

sample({
  clock: fetchNetworksFx.done,
  source: $currentUser,
  filter: (currentUser, { params }) => params.isPending && params.userId1 === currentUser._id,
  fn: (_, { result }) => result,
  target: $outgoingNetworks,
});

sample({
  clock: fetchNetworksFx.done,
  source: { users: $$usersModel.$users, currentUser: $currentUser },
  filter: (
    { currentUser },
    { params },
  ) => Boolean(currentUser._id && params.isPending && params.userId2 === currentUser._id),
  fn: ({ users, currentUser }, { result }) => ({
    networks: result,
    type: 'incoming',
    users,
    currentUserId: currentUser._id,
  }),
  target: updateNetworksFx,
});

sample({
  clock: updateNetworksFx.done,
  filter: ({ params }) => params.type === 'incoming',
  fn: ({ result }) => result,
  target: $incomingNetworks,
});
sample({
  clock: updateNetworksFx.done,
  filter: ({ params }) => params.type === 'outgoing',
  fn: ({ result }) => result,
  target: $outgoingNetworks,
});

sample({
  clock: updateNetworksFx.done,
  filter: ({ params }) => params.type === 'networks',
  fn: ({ result }) => ({
    networks: result,
    count: result.length,
  }),
  target: $networks,
});

sample({
  clock: componentsStarted,
  source: $currentUser,
  fn: (user) => ({
    userId1: user._id, returnUser2: true, isPending: false, limit: 25, pageNumber: 1,
  }),
  target: fetched,
});

sample({
  clock: componentsStarted,
  source: $currentUser,
  fn: (user) => ({
    userId2: user._id, isPending: true, limit: 25, pageNumber: 1,
  }),
  target: fetched,
});
sample({
  clock: componentsStarted,
  source: $currentUser,
  fn: (user) => ({
    userId1: user._id, isPending: true, limit: 25, pageNumber: 1,
  }),
  target: fetched,
});

sample({
  clock: deleteNetworksFx.doneData,
  filter: (_, id) => id !== null,
  source: $networks,
  fn: (networks, id) => {
    if (!id) {
      return networks;
    }
    const newNetworks = networks.networks.filter((network) => network._id !== id);
    return { count: newNetworks.length, networks: newNetworks };
  },
  target: $networks,
});

const $pending = fetchNetworksFx.pending;
const $pendingNetworkByUserId = fetchNetworkByIdFx.pending;

const $connections = combine($networks, (networks) => {
  const list = networks.networks.map((n) => ({
    _id: n.user2?._id,
    profile: n.user2?.profile,
  }));
  return {
    count: list.length,
    networks: list,
  };
});

const $sortedConnections = combine($connections, (connections) => {
  const sortedConnections = [...connections.networks].sort((a, b) => {
    if (a.profile?.firstName && b.profile?.firstName) {
      if (a.profile.firstName > b.profile.firstName) return 1;
      if (a.profile.firstName < b.profile.firstName) return -1;
    }
    return 0;
  });
  return {
    count: sortedConnections.length,
    networks: sortedConnections,
  };
});

export {
  networkCreated,
  networkAccepted,
  fetched,
  fetchNetworkByUserId,
  deleted,
  $sortedConnections,
  $pending,
  $pendingNetworkByUserId,
  $connections,
  networkIncomingGot,
  networkOutgoingGot,
  $outgoingNetworks,
  $incomingNetworks,
};
