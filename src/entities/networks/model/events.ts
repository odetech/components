import {
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { Channel, Socket } from 'phoenix';
import { $$socketModel } from '~/entities/ikinari';
import { showToast } from '~/components/default-components';

const failed = createEvent<unknown>();
const inviteGot = createEvent<{ _id: string, isPending: boolean, userId1: string, userId2: string }>();
const acceptedGot = createEvent<{ _id: string, isPending: boolean, userId1: string, userId2: string }>();
const outgoingGot = createEvent<{ _id: string, isPending: boolean, userId1: string, userId2: string }>();
const socketSent = createEvent<{
  type: string,
  payload: Record<string, unknown>,
  channelName?: string | null;
}>();

const $channel = createStore<Channel | null>(null);

export {
  socketSent,
  inviteGot,
  acceptedGot,
  outgoingGot,
};

const connectChannelFx = createEffect((
  {
    socket, userId,
  }:
    { socket: Socket | null, userId: string | null },
) => {
  if (!socket) return null;
  const connectedChannel = socket.channel(`networks:${userId}`);
  connectedChannel.onError(console.log);
  connectedChannel.join()
    .receive('ok', () => console.log('connected'))
    .receive('error', (e: unknown) => failed(e));

  connectedChannel.on('network_invite_failed', (response) => {
    showToast({
      message: 'Something wrong with deleted message',
      type: response.message,
    });
  });

  connectedChannel.on('network_accept_failed', (response) => {
    showToast({
      message: 'Something wrong with deleted message',
      type: response.message,
    });
  });

  connectedChannel.on('network_invite_sent', (response) => {
    outgoingGot(response.data);
  });

  connectedChannel.on('network_invite_recieved', (response) => {
    inviteGot(response.data);
  });

  connectedChannel.on('network_accepted', (response) => {
    acceptedGot(response.data);
  });

  return connectedChannel;
});

const sentFx = createEffect(async ({
  channel,
  channelName,
  type,
  payload,
}: {
  channel: Channel | null;
  channelName?: string | null;
  type: string;
  payload: Record<string, unknown>;
}) => {
  if (!channel) return;
  channel.push(type, {
    ...payload,
    channelName,
  });
});

sample({
  clock: $$socketModel.$isConnected,
  source: {
    socket: $$socketModel.$socket,
    userId: $$socketModel.$userId,
    isConnected: $$socketModel.$isConnected,
  },
  fn: ({
    socket, userId,
  }) => ({
    socket,
    userId,
  }),
  target: connectChannelFx,
});

sample({
  clock: connectChannelFx.doneData,
  target: $channel,
});

sample({
  clock: connectChannelFx.fail,
  fn: ({ params: { socket }, error }) => ({ socket, e: error }),
  target: createEffect(({ socket }: { socket: Socket | null, e: unknown }) => {
    if (socket) socket.disconnect();
  }),
});

sample({
  clock: [
    connectChannelFx.failData,
    failed,
  ],
  target: createEffect((error: unknown) => console.error(JSON.stringify(error))),
});

sample({
  clock: socketSent,
  source: {
    channel: $channel,
  },
  fn: ({
    channel,
  }, {
    type,
    payload,
    channelName,
  }) => ({
    channel,
    channelName,
    type,
    payload,
  }),
  target: sentFx,
});
