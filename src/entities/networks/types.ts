import { UsersTypes } from '~/entities/users';

export type NetworksDataProps = {
  createdAt: string,
  isPending: boolean,
  updatedAt: string,
  userId1: string,
  userId2: string,
  _id: string,
  user1?: UsersTypes.UserDataProps,
  user2?: UsersTypes.UserDataProps,
};

export type NetworksCountProps = {
  _id?: string,
  value: number,
};

export type NetworkConnections = {
  connections: {_id: string, profile: any }[],
  connectionCount: number,
}
