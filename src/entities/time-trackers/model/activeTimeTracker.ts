/* eslint-disable no-param-reassign */
import { TTimeTracker } from '~/entities/time-trackers';
import {
  combine,
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { mapTimeTracker } from '~/modules/time-tracker/helpers';
import OdeCloud from '@odecloud/odecloud';
import { $api, $currentUser } from '~/config';

const $activeTimeTracker = createStore<TTimeTracker | null>(null);
const activeTimeTrackerChanged = createEvent<TTimeTracker | null>();
const activeTimeTrackerUpdated = createEvent<boolean>();

const $isPause = combine($activeTimeTracker, (timeTracker) => {
  if (timeTracker) {
    const { end, start } = timeTracker.times[timeTracker.times.length - 1] || {};
    return Boolean(start && end);
  }
  return false;
});

$activeTimeTracker.on(activeTimeTrackerChanged, (_, timeTracker) => timeTracker);

const updateActiveTimeTrackerFx = createEffect(async ({
  current,
  currentUserId,
  api,
  isStop = false,
}: {
  current: TTimeTracker | null;
  currentUserId: string | null;
  api: OdeCloud | null;
  isStop: boolean;
}) => {
  if (!api || !current) return null;
  const filter: { data: string[]} = {
    data: ['orgId', 'projectId', 'taskId', 'trackedBy'],
  };

  const response = await api.timeTrackers.patch(current.id, {
    updatedBy: currentUserId,
    ...filter,
  });
  return isStop ? null : mapTimeTracker(response);
});

sample({
  clock: activeTimeTrackerUpdated,
  source: { current: $activeTimeTracker, api: $api, currentUser: $currentUser },
  fn: ({ current, api, currentUser }, isStop) => ({
    current,
    api,
    currentUserId: currentUser?._id,
    isStop,
  }),
  target: updateActiveTimeTrackerFx,
});

sample({
  clock: updateActiveTimeTrackerFx.doneData,
  target: $activeTimeTracker,
});

const $activeTimeTrackerLoading = updateActiveTimeTrackerFx.pending;

export {
  $activeTimeTracker,
  $isPause,
  $activeTimeTrackerLoading,
  activeTimeTrackerChanged,
  activeTimeTrackerUpdated,
  updateActiveTimeTrackerFx,
};
