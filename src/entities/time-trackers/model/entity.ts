import {
  combine, createEvent, createStore, sample,
} from 'effector';
import { debug } from 'patronum';
import {
  createAndRunTimeTrackerFx, FetchProps, fetchTimeTrackersFx, finishTimeTrackerFx,
  removeTimeTrackerFx, editTimeTrackerFx, createTimeTrackerFx,
} from '../api';
import { updateActiveTimeTrackerFx } from './activeTimeTracker';
import { TTimeTrackers } from '../types';

const $timeTrackers = createStore<TTimeTrackers>({});
const $allTrackedTime = combine($timeTrackers, (timeTrackers) => {
  const timeTrackersArr = timeTrackers ? Object.values(timeTrackers) : [];
  return timeTrackersArr.reduce((acc, timeTracker) => acc + timeTracker.accumulative, 0);
});
const fetched = createEvent<Omit<FetchProps, 'api'>>();
const removed = createEvent<string | string[]>();
const updateMessage = createEvent<{ id: string, message: { messageId: string, text: string }}>();
const createdAndRan = createEvent<string>();
const finished = createEvent<{
  timeTrackerId?:string;
  currentTrackedId?:string;
}>();

sample({
  clock: fetched,
  target: fetchTimeTrackersFx,
});

sample({
  clock: createdAndRan,
  target: createAndRunTimeTrackerFx,
});

sample({
  clock: finished,
  target: finishTimeTrackerFx,
});

sample({
  clock: [
    createAndRunTimeTrackerFx.doneData,
    finishTimeTrackerFx.doneData,
    createTimeTrackerFx.doneData,
    updateActiveTimeTrackerFx.doneData,
  ],
  source: $timeTrackers,
  fn: (trackers, result) => {
    const copyItems: TTimeTrackers = structuredClone(trackers);
    if (result) {
      copyItems[result.id] = result;
    }
    return copyItems;
  },
  target: $timeTrackers,
});

sample({
  clock: editTimeTrackerFx.doneData,
  source: $timeTrackers,
  fn: (trackers, result) => {
    const copyItems: TTimeTrackers = structuredClone(trackers);
    if (result) {
      copyItems[result.timeTracker.id] = {
        ...result.timeTracker,
        message: result.message,
      };
    }
    return copyItems;
  },
  target: $timeTrackers,
});

sample({
  clock: removed,
  target: removeTimeTrackerFx,
});

sample({
  clock: fetchTimeTrackersFx.doneData,
  target: $timeTrackers,
});

sample({
  clock: removeTimeTrackerFx.done,
  source: $timeTrackers,
  fn: (trackers, { params }) => {
    const copyItems: TTimeTrackers = structuredClone(trackers);
    const items: string[] = Array.isArray(params) ? params : [params];
    items.forEach((id: string) => {
      delete copyItems[id];
    });
    return copyItems;
  },
  target: $timeTrackers,
});

sample({
  clock: updateMessage,
  source: $timeTrackers,
  fn: (trackers, { id, message }) => {
    const copyItems: TTimeTrackers = structuredClone(trackers);
    copyItems[id] = {
      ...copyItems[id],
      message,
    };
    return copyItems;
  },
  target: $timeTrackers,
});

export {
  $timeTrackers,
  $allTrackedTime,
  fetched,
  updateMessage,
  finished,
  removed,
  createdAndRan,
};
