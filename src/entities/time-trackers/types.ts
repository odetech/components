export type TTimeTracker = {
  id: string;
  taskId: string;
  accumulative: number;
  updatedAt: string;
  times: TTimes[];
  createdBy: string;
  createdAt: string;
  orgName: string;
  message: {
    text?: string;
    messageId?: string;
  };
  projectName: string;
  orgId: string;
  projectId: string;
  task: {
    title: string;
    id?: string;
    _id?: string;
    description: string;
    priority: number;
    titleWithProject?: string;
  },
  trackedBy: {
    id: string;
    name: string;
    avatarUrl: string;
  }
};

type TTimes = {
  start: number;
  end: number | null;
  id?: string;
};

export type TTimeTrackers = Record<string, TTimeTracker>;
