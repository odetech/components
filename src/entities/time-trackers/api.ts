import { attach, createEffect } from 'effector';
import OdeCloud from '@odecloud/odecloud';
import { $api, $currentUser } from '~/config/init';
import { $activeTimeTracker, updateActiveTimeTrackerFx } from './model/activeTimeTracker';
import { TTimeTracker, TTimeTrackers } from './types';
import { mapTimeTracker, mapTimeTrackers } from './lib';

export type FetchProps = {
  api: OdeCloud;
  taskId?: string | null;
  trackedBy?: string;
  dateFilter?: { startDate: string, endDate?: string };
  limit?: number;
};

export type CreateProps = {
  api: OdeCloud;
  taskId?: string | null;
  times: any;
  userId: string;
};

export type CreateAndRunProps = Omit<CreateProps, 'times'>;

export type DeleteProps = {
  api: OdeCloud;
  activeTimeTracker: TTimeTracker | null;
  entityId: string | string[];
  userId: string | null;
};

export type FinishProps = {
  api: OdeCloud;
  timeTrackerId?: string | undefined;
  currentTrackedId?: string;
  userId: string | null;
};

export type EditProps = {
  api: OdeCloud;
  trackerId: string;
  payload: any;
};

const fetchTimeTrackersApiFx = createEffect<FetchProps, TTimeTrackers>(
  async ({
    api, limit, taskId, trackedBy, dateFilter,
  }) => {
    const filter: any = {
      data: ['orgId', 'projectId', 'taskId', 'message'],
      limit: limit || 100,
    };

    if (taskId) {
      filter.taskId = taskId;
    }
    if (trackedBy) {
      filter.trackedBy = trackedBy;
    }

    const timeTrackersResult: any = await api.timeTrackers.getTimeTrackers({ ...filter, ...dateFilter });
    return mapTimeTrackers(timeTrackersResult).elements;
  },
);

const createTimeTrackerApiFx = createEffect<CreateProps, TTimeTracker>(
  async ({
    api, taskId, times, userId,
  }) => {
    const preset = times[0].startedAt;
    const filter: any = {
      data: ['orgId', 'projectId', 'taskId'],
    };

    const timeTrackersResult = await api.timeTrackers.post({
      taskId, trackedBy: userId, times, preset, ...filter,
    });
    return mapTimeTracker(timeTrackersResult);
  },
);

const createAndRunTimeTrackerApiFx = createEffect<CreateAndRunProps, TTimeTracker>(
  async ({
    api, taskId, userId,
  }) => {
    const filter: any = {
      data: ['orgId', 'projectId', 'taskId'],
    };

    const tracker: any = await api.timeTrackers.post({
      taskId, trackedBy: userId, ...filter,
    });
    const timeTrackersResult = await api.timeTrackers.patch(
      tracker._id,
      { updatedBy: userId, ...filter },
    );
    return mapTimeTracker(timeTrackersResult);
  },
);

const finishTimeTrackerApiFx = createEffect<FinishProps, TTimeTracker | null>(
  async ({
    api, currentTrackedId, timeTrackerId, userId,
  }) => {
    if (!api) return null;
    const filter: any = {
      data: ['orgId', 'projectId', 'taskId', 'trackedBy'],
    };
    if ((currentTrackedId && currentTrackedId !== timeTrackerId) || timeTrackerId) {
      const timeTrackersResult = await api.timeTrackers.patch(
        (currentTrackedId && currentTrackedId !== timeTrackerId ? currentTrackedId : timeTrackerId) as string,
        { updatedBy: userId, ...filter },
      );
      return mapTimeTracker(timeTrackersResult);
    }
    return null;
  },
);

const editTimeTrackerApiFx = createEffect<
  EditProps,
  { timeTracker: TTimeTracker, message: { _id: string, text: string }} | null
>(
  async ({
    api, trackerId, payload,
  }) => {
    if (!api) return null;
    const response = await api.timeTrackers.patchEdit(trackerId, payload);
    return {
      timeTracker: mapTimeTracker(response.timeTracker),
      message: response.message,
    };
  },
);

const removeTimeTrackerApiFx = createEffect<DeleteProps, string[] | null>(
  async ({
    api, entityId, activeTimeTracker, userId,
  }) => {
    if (!api) return null;
    const items: string[] = Array.isArray(entityId) ? entityId : [entityId];
    if (activeTimeTracker?.id) {
      await updateActiveTimeTrackerFx({
        api,
        currentUserId: userId,
        isStop: true,
        current: activeTimeTracker,
      });
    }
    const promises = items.map(async (it) => {
      await api.timeTrackers.delete(it);
    });
    await Promise.all(promises);
    return items;
  },
);

export const fetchTimeTrackersFx = attach({
  effect: fetchTimeTrackersApiFx,
  source: $api,
  mapParams: ({
    taskId, trackedBy, dateFilter, limit,
  }: any, api) => ({
    api: api!,
    taskId,
    trackedBy,
    dateFilter,
    limit,
  }),
});

export const createTimeTrackerFx = attach({
  effect: createTimeTrackerApiFx,
  source: { api: $api, user: $currentUser },
  mapParams: ({
    taskId, times,
  }: any, { api, user }) => ({
    api: api!,
    taskId,
    times,
    userId: user._id,
  }),
});

export const createAndRunTimeTrackerFx = attach({
  effect: createAndRunTimeTrackerApiFx,
  source: { api: $api, user: $currentUser },
  mapParams: (taskId: any, { api, user }) => ({
    api: api!,
    taskId,
    userId: user._id,
  }),
});

export const finishTimeTrackerFx = attach({
  effect: finishTimeTrackerApiFx,
  source: { api: $api, user: $currentUser },
  mapParams: ({
    timeTrackerId,
    currentTrackedId,
  }: any, { api, user }) => ({
    api: api!,
    timeTrackerId,
    currentTrackedId,
    userId: user._id,
  }),
});

export const editTimeTrackerFx = attach({
  effect: editTimeTrackerApiFx,
  source: { api: $api },
  mapParams: ({
    trackerId,
    payload,
  }: any, { api }) => ({
    api: api!,
    trackerId,
    payload,
  }),
});

export const removeTimeTrackerFx = attach({
  effect: removeTimeTrackerApiFx,
  source: { api: $api, user: $currentUser, activeTimeTracker: $activeTimeTracker },
  mapParams: (entityId: any, { api, user, activeTimeTracker }) => ({
    api: api!,
    entityId,
    activeTimeTracker,
    userId: user._id,
  }),
});
