import { ensureUtcTimestamp } from '~/helpers';
import { fromArrayToHash } from '~/helpers/globalHelpers';

export const mapTimeTracker = ({
  _id, id, taskId, accumulative, updatedAt, times, createdBy, createdAt, data, trackedBy,
}: any) => ({
  id: _id || id,
  taskId,
  accumulative,
  times: times.map((time: any) => ({
    start: Math.trunc(time.startedAt) * 1000,
    end: time.endedAt ? Math.trunc(time.endedAt) * 1000 : null,
    id: time._id,
  })),
  createdBy,
  createdAt: ensureUtcTimestamp(createdAt).replace('+00:00', ''),
  updatedAt,
  orgName: data?.orgId?.name,
  orgId: data?.orgId?._id,
  projectName: data?.projectId?.title,
  projectId: data?.projectId?._id,
  message: data?.message && {
    text: data?.message?.text,
    messageId: data?.message?._id,
  },
  task: {
    title: data?.taskId?.title,
    description: data?.taskId?.description,
    priority: data?.taskId?.priority,
    id: _id,
    titleWithProject: `${data?.projectId?.title} - ${data?.taskId?.title}`,
  },
  trackedBy: {
    id: trackedBy,
    name: `${data?.trackedBy?.profile?.firstName} ${data?.trackedBy?.profile?.lastName}`,
    avatarUrl: data?.trackedBy?.profile?.avatar?.secureUrl,
  },
});

export const mapTimeTrackers = (response: any) => {
  response.data.sort((a: any, b: any) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime());
  return ({
    elements: fromArrayToHash(response.data.map(mapTimeTracker)),
    hasNextPage: response.hasNextPage,
    pageNumber: response.pageNumber,
    pageSize: response.pageSize,
    total: response.total,
  });
};
