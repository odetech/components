import { createEffect, createStore, sample } from 'effector';
import { toast } from 'react-toastify';
import { Button, showToast } from '~/components/default-components';
import * as React from 'react';

const clearToastFx = createEffect((id: number | string | null) => {
  if (id) toast.dismiss(id);
});

const C = ({ callback }: any) => (
  <div style={{
    display: 'flex', justifyContent: 'space-between', alignItems: 'center', gap: '5px',
  }}
  >
    <span>Trying to reconnect to your chat...</span>
    {callback && (
      <Button
        type="button"
        onClick={callback}
      >
        Reconnect
      </Button>
    )}
  </div>
);

const showOfflineToastFx = createEffect((callback?: () => void) => {
  const node = <C callback={callback} />;
  return showToast(
    {
      message: node,
      type: 'warning',
      position: 'top-center',
      autoClose: false,
      closeOnClick: false,
      closeOnEsc: false,
      draggable: false,
      pauseOnHover: false,
    },
  );
});

const updateToastFx = createEffect(({ id, callback }: {id: number | string | null, callback?: () => void}) => {
  toast.update(id!, { render: <C callback={callback} /> });
});

const $toast = createStore<number | string | null>(null);

sample({ clock: showOfflineToastFx.doneData, target: $toast });
sample({ clock: clearToastFx.doneData, fn: () => null, target: $toast });

export {
  showOfflineToastFx,
  clearToastFx,
  updateToastFx,
  $toast,
};
