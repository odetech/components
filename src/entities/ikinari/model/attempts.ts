import { createEvent, createStore } from 'effector';

const $attempts = createStore(0);
const increaseAttempt = createEvent();
const clearAttempt = createEvent();
$attempts.on(increaseAttempt, (a) => a + 1).on(clearAttempt, () => 0);

export {
  $attempts,
  increaseAttempt,
  clearAttempt,
};
