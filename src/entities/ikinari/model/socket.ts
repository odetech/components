import {
  combine, createEffect, createEvent, createStore, sample,
} from 'effector';
import { Socket } from 'phoenix';
import { trackNetworkStatus, trackPageVisibility } from '@withease/web-api';
import { $currentUser, componentsStarted } from '~/config';
import { $attempts, clearAttempt, increaseAttempt } from './attempts';
import { ROOT_URL, TOKEN, WS } from '../lib';
import {
  showOfflineToastFx, clearToastFx, updateToastFx, $toast,
} from './toast';

type Connect = {
  userId: string | null;
  isOnline: boolean;
  attempts: number;
  currentSocket: Socket | null;
};

const $socket = createStore<Socket | null>(null);
const connected = createEvent();
const connectedSocket = createEvent();
const disconnected = createEvent();
const $isConnected = createStore(false);
$isConnected.on(connected, () => true).on(disconnected, () => false);

const $userId = combine($currentUser, (user) => (user ? user._id : null));

const {
  offline, $online, online,
} = trackNetworkStatus({
  setup: componentsStarted,
});

const { visible } = trackPageVisibility({
  setup: componentsStarted,
});

export const $$socketModel = {
  $isConnected,
  $socket,
  $userId,
  $attempts,
  connectedSocket,
};

// When socket connected, we save it on $socket.
export const connectFx = createEffect(
  async ({
    currentSocket, userId, isOnline, attempts,
  }: Connect) => {
    if (!userId) return null;
    if (currentSocket !== null) currentSocket.disconnect();

    // TODO remove hardcode
    const socket = new Socket(WS!, {
      params: {
        token: TOKEN,
        rootUrl: ROOT_URL,
        userId,
      },
      heartbeatIntervalMs: 20000,
    });
    socket.onOpen(() => {
      connected();
      clearAttempt();
    });
    socket.onClose(() => {
      disconnected();
      if (isOnline && attempts < 5) {
        increaseAttempt();
        connectedSocket();
      }
    });
    socket.onError((
      error: Event | string | number,
      transport: new(endpoint: string) => object,
      establishedConnections: number,
    ) => {
      console.log(error, transport, establishedConnections);
    });
    socket.connect();
    return socket;
  },
);
// When user set, we run connection.
sample({
  clock: [$userId, connectedSocket],
  source: {
    currentSocket: $socket, userId: $userId, isOnline: $online, attempts: $attempts,
  },
  target: connectFx,
});
sample({ clock: connectFx.doneData, filter: (res) => Boolean(res), target: $socket });

sample({
  clock: visible,
  source: {
    currentSocket: $socket,
    userId: $userId,
    isOnline: $online,
    isConnected: $isConnected,
    attempts: $attempts,
  },
  filter: ({ isConnected }) => !isConnected,
  target: connectFx,
});

// offline logic
// if toast doesn't exist and we have less than 5 attempts
sample({
  clock: [offline, $isConnected],
  source: { t: $toast, isConnected: $isConnected, attempts: $attempts },
  filter: ({ t, isConnected, attempts }) => Boolean(!t) && !isConnected && attempts < 5,
  fn: () => undefined,
  target: showOfflineToastFx,
});

// if toast doesn't exist and we have less than 5 attempts
sample({
  clock: [offline, $isConnected],
  source: { t: $toast, isConnected: $isConnected, attempts: $attempts },
  filter: ({ t, isConnected, attempts }) => Boolean(!t) && !isConnected && attempts >= 5,
  fn: () => connectedSocket,
  target: showOfflineToastFx,
});

// if toast exist and we need to update to than 5 attempts
sample({
  clock: [offline, $isConnected, $attempts],
  source: { t: $toast, isConnected: $isConnected, attempts: $attempts },
  filter: ({ t, isConnected, attempts }) => Boolean(t) && !isConnected && attempts >= 5,
  fn: ({ t }) => ({
    callback: connectedSocket,
    id: t,
  }),
  target: updateToastFx,
});

// clear toast if we connected
sample({
  clock: [online, $isConnected],
  source: { t: $toast, isConnected: $isConnected },
  filter: ({ t, isConnected }) => Boolean(t) && isConnected,
  fn: ({ t }) => t,
  target: clearToastFx,
});
