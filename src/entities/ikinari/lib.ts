const WS = process.env.REACT_APP_IKINARI_URL;
const TOKEN = process.env.REACT_APP_IKINARI_TOKEN;
const ROOT_URL = process.env.REACT_APP_IKINARI_ROOT_URL;

export {
  WS,
  TOKEN,
  ROOT_URL,
};
