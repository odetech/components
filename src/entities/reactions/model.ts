import { createEvent, createStore, sample } from 'effector';
import { componentsStarted } from '~/config';
import { fetchReactionsFx, ReactionData } from './api';

const reacted = createEvent<{
  itemType: string | 'article' | 'comment' | 'reply' | 'chat',
  itemId: string,
  messageId: string,
  commentId?: string,
  reaction: {
    label: string | 'likes' | 'fun' | 'insightful' | 'important' | 'helpful' | 'seen' | 'done',
    value: boolean,
  },
  previousReactionLabel?: string | 'likes' | 'fun' | 'insightful' | 'important' | 'helpful' | 'seen' | 'done',
}>();
const fetched = createEvent();

const $reactions = createStore<ReactionData[]>([
  {
    value: 'likes',
    label: 'Like',
    staticSvg: '',
    lottieAnimation: null,
  },
  {
    value: 'important',
    label: 'Love it', // !!! On the backend it's called 'important', but here it's 'Love it' !!!
    staticSvg: '',
    lottieAnimation: null,
  },
  {
    value: 'fun',
    label: 'Fun',
    staticSvg: '',
    lottieAnimation: null,
  },
  {
    value: 'helpful',
    label: 'Thank you', // !!! On the backend it's called 'helpful', but here it's 'Thank you' !!!
    staticSvg: '',
    lottieAnimation: null,
  },
  {
    value: 'insightful',
    label: 'Insightful',
    staticSvg: '',
    lottieAnimation: null,
  },
  {
    value: 'seen',
    label: 'Seen',
    staticSvg: '',
    lottieAnimation: null,
  },
  {
    value: 'done',
    label: 'Done',
    staticSvg: '',
    lottieAnimation: null,
  },
]);

export const $$reactionModel = {
  reacted,
  $reactions,
};

sample({
  clock: componentsStarted,
  target: fetched,
});

sample({
  clock: fetched,
  source: $reactions,
  fn: (reactionsData) => ({
    reactionsData,
  }),
  target: fetchReactionsFx,
});

sample({
  clock: fetchReactionsFx.doneData,
  target: $reactions,
});
