/* eslint-disable no-underscore-dangle */
import React, { Fragment, useEffect, useState } from 'react';
import { ReactionsIcon } from '~/components/icons';
import { useUnit } from 'effector-react/effector-react.umd';
import { $currentUser } from '~/config';
import {
  OtherReaction,
  OtherReactionsWrapper,
  OtherReactionsWrapperButton,
  ReactedBoldText,
  ReactedText,
  ReactionButton,
  ReactionFlexWrapper,
  ReactionLabelUncontrolledTooltip,
  ReactionsButton,
  ReactionsUncontrolledTooltip,
} from './styles';
import ReactionsModal from './reactions-modal';
import { Reaction } from './reaction';
import { $$reactionModel } from '../model';

export type ReactionsProps = {
  itemType: string | 'article' | 'comment' | 'reply' | 'chat' | 'tasks',
  itemData: {
    _id: string,
    id?: string,
    likes: string[],
    fun: string[],
    helpful: string[],
    important: string[],
    insightful: string[],
    seen?: string[],
    done?: string[],
  },
  articleStatus?: 'editing' | 'deleting' | 'creating',
  messageId: string,
  commentId?: string,
  setProfilePreviewUserData?: (isProfileCardOpen: { _id: string } | null) => void,
  margin?: string,
  shortInfo?: boolean,
  isSmall?: boolean,
  position?: 'right' | 'left' | string,
  justifyReverse?: boolean,
};

export const Reactions = ({
  // Default props
  itemType,
  itemData,
  articleStatus,
  messageId,
  commentId,
  setProfilePreviewUserData,
  margin,
  justifyReverse,
  shortInfo,
  position,
  isSmall,
}: ReactionsProps) => {
  const userData = useUnit($currentUser);
  const reactionsData = useUnit($$reactionModel.$reactions);
  const reacted = useUnit($$reactionModel.reacted);

  const [reactedType, setReactedType] = useState<string>('');
  const [allReactions, setAllReactions] = useState<string[]>([]);
  const [reactedTypes, setReactedTypes] = useState<string[]>([]);

  useEffect(() => {
    let foundReactedType = '';
    let allReactionsData: string[] = [];
    const allReactedTypes: string[] = [];

    reactionsData.forEach((reactionType) => {
      // @ts-ignore
      if (reactionType.value && itemData?.[reactionType.value]?.find(
        (userId: string) => (userData._id && userId === userData._id),
      )) {
        foundReactedType = reactionType.value;
      }
      // @ts-ignore
      if (itemData?.[reactionType.value]) {
        // @ts-ignore
        allReactionsData = [...allReactionsData, ...itemData[reactionType.value]];
      }
      // @ts-ignore
      if (itemData?.[reactionType.value]?.length) {
        // @ts-ignore
        allReactedTypes.push(reactionType.value);
      }
    });

    setReactedType(foundReactedType);
    setAllReactions(allReactionsData);
    setReactedTypes(allReactedTypes);
  }, [itemData, userData]);

  const [showReactionsModal, setShowReactionsModal] = useState(false);

  const reactedData = reactionsData.find((item) => (item.value === reactedType));

  return (
    <Fragment>
      <ReactionFlexWrapper
        margin={margin}
        reversed={shortInfo}
        isSmall={isSmall}
        isEmpty={allReactions.length === 0}
        justifyReverse={justifyReverse}
      >
        {allReactions.length !== 0 && ( // user submitted reactions
          <Fragment>
            <OtherReactionsWrapperButton
              isSmall={isSmall}
              view="text"
              onClick={() => {
                setShowReactionsModal(true);
              }}
              id={`viewReactions${itemData._id ?? itemData.id}`}
              margin={shortInfo ? '0' : '0 2px 0 0'}
            >
              {shortInfo ? (
                <ReactionFlexWrapper>
                  <OtherReactionsWrapper isSmall={isSmall}>
                    {reactedTypes.slice(0, 3).map((reactionType) => {
                      const reactionData = reactionsData.find((item) => (item.value === reactionType));

                      if (reactionData) {
                        return (
                          <OtherReaction
                            key={`viewReactions${itemData._id ?? itemData.id}`}
                          >
                            <Reaction
                              // @ts-ignore
                              reactionData={reactionData}
                            />
                          </OtherReaction>
                        );
                      }
                      return null;
                    })}
                  </OtherReactionsWrapper>
                  <ReactedText
                    smallSize={!shortInfo}
                  >
                    {`${allReactions.length} ${allReactions.length
                    && (allReactions.length === 1 ? 'reaction' : 'reactions')}`}
                  </ReactedText>
                </ReactionFlexWrapper>
              ) : (
                <Fragment>
                  <OtherReactionsWrapper isSmall={isSmall}>
                    {reactedTypes.map((reactionType) => {
                      const reactionData = reactionsData.find((item) => (item.value === reactionType));

                      if (reactionData) {
                        return (
                          <OtherReaction
                            key={`viewReactions${itemData._id ?? itemData.id}`}
                          >
                            <Reaction
                              // @ts-ignore
                              reactionData={reactionData}
                            />
                          </OtherReaction>
                        );
                      }
                      return null;
                    })}
                  </OtherReactionsWrapper>
                  {!isSmall && (reactedType !== '' ? (
                    <ReactionFlexWrapper>
                      <ReactedBoldText
                        smallSize
                      >
                        You
                      </ReactedBoldText>
                      <ReactedText
                        smallSize
                      >
                        {allReactions.length > 1 ? `and ${allReactions.length - 1} ${(allReactions.length - 1) === 1
                          ? 'other' : 'others'}` : 'reacted'}
                      </ReactedText>
                    </ReactionFlexWrapper>
                  ) : (
                    <ReactedText
                      smallSize
                    >
                      {`${allReactions.length} reacted`}
                    </ReactedText>
                  )
                  )}
                </Fragment>
              )}
            </OtherReactionsWrapperButton>
            <ReactionLabelUncontrolledTooltip
              placement="top"
              target={`viewReactions${itemData._id ?? itemData.id}`}
              autohide={false}
              hideArrow
            >
              View reactions
            </ReactionLabelUncontrolledTooltip>
          </Fragment>
        )}
        {/* Reactions button */}
        <ReactionsButton
          id={`ReactionsButton${itemData._id ?? itemData.id}`}
          active={reactedType !== ''}
          shortInfo={shortInfo}
          view="text"
          disabled={articleStatus === 'editing'}
          onClick={() => {
            reacted({
              itemType,
              itemId: itemData._id ?? itemData.id,
              messageId,
              commentId,
              reaction: {
                label: reactedType || 'likes',
                value: !(reactedType !== ''),
              },
              previousReactionLabel: reactedType,
            });
          }}
          margin={shortInfo ? '0 2px 0 0' : '0'}
        >
          {reactedData ? (
            <Reaction
              // @ts-ignore
              reactionData={reactedData}
            />
          ) : (<ReactionsIcon />)}
        </ReactionsButton>
        <ReactionsUncontrolledTooltip
          placement="top"
          target={`ReactionsButton${itemData._id ?? itemData.id}`}
          autohide={false}
          delay={200}
          hideArrow
          position={position || 'right'}
        >
          {reactionsData.map((reactionType) => {
            const reactionData = reactionsData
              .find((item) => (item.value === reactionType.value));

            return (
              <Fragment
                key={`${reactionType.value}Reaction${itemData._id ?? itemData.id}`}
              >
                <ReactionButton
                  id={`${reactionType.value}Reaction${itemData._id ?? itemData.id}`}
                  view="text"
                  disabled={articleStatus === 'editing'}
                  onClick={() => {
                    reacted({
                      itemType,
                      itemId: itemData._id ?? itemData.id,
                      messageId,
                      commentId,
                      reaction: {
                        label: reactionType.value,
                        value: !(reactedType === reactionType.value),
                      },
                      previousReactionLabel: reactedType,
                    });
                  }}
                  reactionLabel={reactionType.value.toLowerCase()}
                >
                  <Reaction
                    // @ts-ignore
                    reactionData={reactionData}
                    tooltip
                  />
                </ReactionButton>
                <ReactionLabelUncontrolledTooltip
                  placement="top"
                  target={`${reactionType.value}Reaction${itemData._id ?? itemData.id}`}
                  autohide={false}
                  hideArrow
                >
                  {reactionType.label}
                </ReactionLabelUncontrolledTooltip>
              </Fragment>
            );
          })}
        </ReactionsUncontrolledTooltip>
      </ReactionFlexWrapper>
      {setProfilePreviewUserData && showReactionsModal && ( // reactions modal window
        <ReactionsModal
          showModal={showReactionsModal}
          setShowModal={setShowReactionsModal}
          reactionsData={{
            likes: itemData.likes,
            fun: itemData.fun,
            helpful: itemData.helpful,
            important: itemData.important,
            insightful: itemData.insightful,
            seen: itemData.seen || [],
            done: itemData.done || [],
          }}
          allReactionsAmount={allReactions.length}
          setProfilePreviewUserData={setProfilePreviewUserData}
          availableReactionTypesArray={reactionsData}
        />
      )}
    </Fragment>
  );
};

export default Reactions;
