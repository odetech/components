import styled from '@emotion/styled';
import { transparentize } from 'polished';
import isPropValid from '@emotion/is-prop-valid';
import { Button } from '~/components/default-components';
import { StyledTooltip } from '~/styles';
import { UncontrolledTooltip } from 'reactstrap';
import { defaultTheme } from '../../../styles/themes';

export const CenterPositionWrapper = styled('div')`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const InvisibleProfilePreviewButton = styled(Button)`
  max-height: unset;
  height: fit-content;
  padding: 0;
  background: transparent;
  
  &:hover, &:active, &:focus {
    background: transparent;
  }
`;

export const ReactionsModalInfoText = styled('div')`
  font-weight: 500;
  font-size: 20px;
  line-height: 30px;
  margin: 0 0 20px 0;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};  
`;

export const ReactionTabsWrapper = styled('div')`
  display: flex;
  position: relative;
  height: 40px;
  margin: 0 0 16px 0;
`;

export const ReactionTabItemsWrapper = styled('div')`
  span {
    &:not(:last-of-type) {
      margin: 0 4px 0 0;
    }
  }
`;

type ReactionTabProps = {
  isActive?: boolean,
};

export const ReactionTab = styled(Button)<ReactionTabProps>`
  position: relative;
  padding: 0 2px 5px 2px;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  border-radius: unset;
  border-bottom: unset;
  transition: 0.1ms;
  z-index: 2;

  img {
    width: 24px;
    min-width: 24px;
    height: 24px;
  }

  svg {
    width: 24px;
    height: 24px;
  }

  ${({ isActive, theme }) => (isActive && `
    font-weight: 600;
    color: ${theme?.brand?.primary || defaultTheme.brand.primary};
    border-bottom: 3px solid ${theme?.brand?.primary || defaultTheme.brand.primary};
    padding: 0 2px 2px 2px;
  `)}

  &:hover {
    ${({ isActive, theme }) => (!isActive && `
      color: ${theme?.brand?.primary || defaultTheme.brand.primary};
      border-bottom: 3px solid ${theme?.brand?.primary || defaultTheme.brand.primary};
      padding: 0 2px 2px 2px;
    `)}
  }

  &:active, &:focus {
    background: transparent;
  }

  &:not(:last-of-type) {
    margin: 0 28px 0 0;
  }
`;

export const ReactionTabsWrapperHR = styled('div')`
  position: absolute;
  bottom: 1px;
  width: 100%;
  height: 1px;
  background: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  z-index: 1;
`;

export const ReactedUsers = styled('div')`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -35px 0 0;
  max-height: 424px;
`;

export const ReactedInvisibleProfilePreviewButton = styled(InvisibleProfilePreviewButton)`
  margin: 0 34px 12px 0;
`;

export const ReactionUserWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  width: 70px;
  height: 110px;
`;

export const ReactionUserAvatarWrapper = styled('div')`
  position: relative;
  width: 70px;
  height: 70px;
  margin: 0 0 8px 0;
`;

export const ReactionUserAvatarType = styled('div')`
  position: absolute;
  bottom: 0;
  right: 0;
  
  img {
    width: 24px;
    min-width: 24px;
    height: 24px;
  }
  
  svg {
    width: 24px;
    height: 24px;
  }
`;

export const ReactionUserName = styled('div')`
  font-weight: 500;
  font-size: 12px;
  line-height: 16px;
  text-align: center;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

type ReactionsButtonProps = {
  active: boolean,
  id?: string,
  shortInfo?: boolean,
  margin?: string,
};

export const ReactionsButton = styled(Button)<ReactionsButtonProps>`
  padding: 4px 8px;
  border-radius: 21px;
  margin: ${({ margin }) => (margin || '0')};

  svg {
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    width: 24px;
    height: 24px;
  }

  img {
    width: 24px;
    min-width: 24px;
    height: 24px;
  }

  ${({ active, shortInfo, theme }) => active && !shortInfo && `
    background: ${theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar} !important;
    border: 1px solid ${theme?.outline?.[100] || defaultTheme.outline[100]};
  `}

  ${({ active, shortInfo, theme }) => active && shortInfo && `
      background: ${transparentize(0.85, theme?.info?.primary || defaultTheme.info.primary)};
      border: 1px solid ${theme?.info?.primary || defaultTheme.info.primary};
    `}

  &:focus {
    background: transparent;
  }
  
  span {
    font-weight: 500;
    font-size: 15px;
    line-height: 22px;    
    color: ${({ active, shortInfo, theme }) => (active && shortInfo
    ? (theme?.info?.primary || defaultTheme.info.primary)
    : (theme?.text?.onSurface || defaultTheme.text.onSurface))};
    margin: 0 0 0 8px;

  };
`;

type ReactionButtonProps = {
  reactionLabel?: string | null,
};

export const ReactionButton = styled(Button)<ReactionButtonProps>`
  width: 32px;
  height: 32px;
  padding: 2px;

  svg {
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }

  img {
    width: 28px;
    height: 28px;
  }

  [id=${({ reactionLabel }) => (`${reactionLabel}-icon-animated`)}] {
    display: none;

    div {
      width: 100px;
      height: 100px;
    }
  }

  &:hover {
    [id=${({ reactionLabel }) => (`${reactionLabel}-icon`)}] {
      display: none;
    }
    [id=${({ reactionLabel }) => (`${reactionLabel}-icon-animated`)}] {
      display: unset;
    }
  }

  &:active, &:hover {
    background: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  }
  
  &:focus {
    background: transparent;
  }
`;

type ReactionsUncontrolledTooltipProps = {
  position?: 'right' | 'left' | string,
};

export const ReactionsUncontrolledTooltip = styled(UncontrolledTooltip)<ReactionsUncontrolledTooltipProps>`
  .tooltip {
    opacity: unset !important;
    left: ${({ position }) => (position === 'right' ? '138px' : '-138px')}!important;
    z-index: 1200 !important;
  }

  .tooltip-inner {
    display: flex;
    gap: 12px;
    padding: 6px 12px;
    max-width: unset;
    background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};          
    border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};    
    border-radius: 35px;
    box-shadow: ${({ theme }) => theme?.shadow?.inner?.[1] || defaultTheme.shadow.inner[1]};
  }
`;

export const ReactionLabelUncontrolledTooltip = styled(StyledTooltip)`
  .tooltip {    
    top: -10px !important;
    z-index: 0;
  }
`;

type ReactionFlexWrapperProps = {
  margin?: string;
  reversed?: boolean;
  isSmall?: boolean;
  isEmpty?: boolean;
  justifyReverse?: boolean;
};

export const ReactionFlexWrapper = styled('div', {
  shouldForwardProp: isPropValid,
})<ReactionFlexWrapperProps>`
  display: flex;
  align-items: center;
  margin: ${({ margin }) => (margin || '0')};

  ${({ reversed }) => (reversed && `
    flex-direction: row-reverse;
  `)}
    
  ${({ isSmall }) => isSmall && 'justify-content: space-between;'};
  ${({ isEmpty, justifyReverse }) => isEmpty && (
    justifyReverse ? 'justify-content: flex-start;' : 'justify-content: flex-end;'
  )};  
`;

type OtherReactionsWrapperButtonProps = {
  margin?: string;
  isSmall?: boolean;
};

export const OtherReactionsWrapperButton = styled(Button)<OtherReactionsWrapperButtonProps>`
  display: flex;
  align-items: center;
  padding: 6px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};   
  margin: ${({ margin }) => (margin || '0')};
  ${({ isSmall }) => isSmall && 'padding: 0;'}  
`;

export const OtherReactionsWrapper = styled(ReactionFlexWrapper)<{ isSmall?: boolean }>`
  margin: ${({ isSmall }) => (isSmall ? '0' : '0 8px 0 0')};
`;

export const OtherReaction = styled('div')`
  width: 28px;
  height: 28px;
  border-radius: 50%;  
  background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};   
  border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};           
  padding: 6px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    width: 16px;
    min-width: 16px;
    height: 16px;
  }

  svg {
    width: 16px;
    height: 16px;
  }

  &:not(:last-child) {
    margin: 0 -6px 0 0;
  }
`;

type ReactedTextProps = {
  smallSize?: boolean,
  margin?: string,
};

export const ReactedText = styled('p', {
  shouldForwardProp: isPropValid,
})<ReactedTextProps>`
  margin: ${({ margin }) => (margin || '0')};
  white-space: nowrap;
  
  ${({ smallSize }) => (smallSize ? `
    font-weight: 400;
    font-size: 12px;
    line-height: 18px;
  ` : `
    font-weight: 500;
    font-size: 15px;
    line-height: 22px;
  `)}
`;

export const ReactedBoldText = styled(ReactedText)`
  font-weight: bold;
  margin: 0 4px 0 0;
`;

export const ReactionIconWrapper = styled('div')`
  pointer-events: none;
`;
