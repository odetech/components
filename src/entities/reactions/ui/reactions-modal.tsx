import React, { useEffect, useState } from 'react';
import { transparentize } from 'polished';
import { Modal } from '~/components/default-components';
import { Avatar } from '~/components/avatar';
import { ModalItemsWrapper } from '~/components/default-components/Modal/ModalStyles';
import { BeatLoader } from 'react-spinners';
import { useUnit } from 'effector-react';
import { $api } from '~/config';
import { useTheme } from '@storybook/theming';
import { defaultTheme } from '../../../styles/themes';
import {
  ReactedInvisibleProfilePreviewButton,
  ReactedUsers,
  ReactionsModalInfoText,
  ReactionTab,
  ReactionTabItemsWrapper,
  ReactionTabsWrapper,
  CenterPositionWrapper,
  ReactionTabsWrapperHR, ReactionUserAvatarType, ReactionUserAvatarWrapper, ReactionUserName,
  ReactionUserWrapper,
} from './styles';
import { Reaction } from './reaction';

type MemberDataProps = {
  _id: string,
  profile: {
    firstName: string,
    lastName: string,
    avatar: {
      secureUrl: string | null,
      _id: string | null,
    } | null,
  }
};

type reactionDataWithUsersProps = MemberDataProps & {
  reactionType: string,
};

type reactionsDataWithUsersProps = {
  all: reactionDataWithUsersProps[],
  likes: reactionDataWithUsersProps[],
  fun: reactionDataWithUsersProps[],
  helpful: reactionDataWithUsersProps[],
  important: reactionDataWithUsersProps[],
  insightful: reactionDataWithUsersProps[],
  seen: reactionDataWithUsersProps[],
  done: reactionDataWithUsersProps[],
};

type ReactionsModal = {
  showModal: boolean;
  setShowModal: (showModal: boolean) => void;
  reactionsData: {
    likes: string[],
    fun: string[],
    helpful: string[],
    important: string[],
    insightful: string[],
    seen?: string[],
    done?: string[],
  };
  allReactionsAmount: number,
  setProfilePreviewUserData: (profilePreviewUserData: { _id: string } | null) => void,
  availableReactionTypesArray: {
    label: string,
    value: string,
  }[],
};

const ReactionsModal = ({
  showModal, setShowModal, reactionsData, allReactionsAmount, setProfilePreviewUserData, availableReactionTypesArray,
}: ReactionsModal) => {
  const theme = useTheme();
  const api = useUnit($api);
  const tabsArray = [{
    value: 'all',
    label: 'All',
  }].concat(availableReactionTypesArray);

  const [currentTab, setCurrentTab] = useState<
    string | 'all' | 'fun' | 'helpful' | 'important' | 'insightful' | 'likes' | 'seen' | 'done'
  >('all');

  const [reactionsDataWithUsers, setReactionsDataWithUsers] = useState<reactionsDataWithUsersProps
    | null>(null);

  useEffect(() => {
    const updatedReactionsData: reactionsDataWithUsersProps = {
      all: [],
      likes: [],
      fun: [],
      helpful: [],
      important: [],
      insightful: [],
      seen: [],
      done: [],
    };

    ((async () => {
      await Promise.all(availableReactionTypesArray.map(async (reactionType) => {
        // @ts-ignore
        await Promise.all((reactionsData[reactionType.value] || []).map(async (userId: string) => {
          const userData: MemberDataProps = await api?.users.getUser(
            userId,
          );
          // @ts-ignore
          updatedReactionsData[reactionType.value].push({
            ...userData,
            reactionType: reactionType.value,
          });
        }));
      }));

      const allUpdatedReactionsData = [...updatedReactionsData.likes, ...updatedReactionsData.fun,
        ...updatedReactionsData.helpful, ...updatedReactionsData.important, ...updatedReactionsData.insightful,
        ...updatedReactionsData.seen, ...updatedReactionsData.done];
      setReactionsDataWithUsers({
        ...updatedReactionsData,
        all: allUpdatedReactionsData,
      });
    })());
  }, []);

  return (
    <Modal
      isOpen={showModal}
      onClose={() => setShowModal(false)}
      contentStyles={{
        maxWidth: 'fit-content',
        maxHeight: 650,
        minWidth: 200,
        minHeight: 100,
      }}
      overlayStyles={{
        background: `${transparentize(0.3, theme?.background?.surface || defaultTheme.background.surface)}`,
      }}
      itemsWrapperStyles={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <ReactionsModalInfoText>
        Reactions
      </ReactionsModalInfoText>
      {reactionsDataWithUsers ? (
        <div>
          <ReactionTabsWrapper>
            {tabsArray.map((tabData) => {
              const reactionData = availableReactionTypesArray
                .find((item) => (item.value === tabData.value));

              return (
                <ReactionTab
                  key={tabData.value}
                  view="text"
                  onClick={() => {
                    setCurrentTab(tabData.value);
                  }}
                  isActive={currentTab === tabData.value}
                >
                  <ReactionTabItemsWrapper>
                    <span>
                      {tabData.value === 'all' ? tabData.label : (
                        <Reaction
                          // @ts-ignore
                          reactionData={reactionData}
                        />
                      )}
                    </span>
                    <span>
                      {/* @ts-ignore */}
                      {tabData.value === 'all' ? allReactionsAmount : reactionsData[tabData.value]?.length}
                    </span>
                  </ReactionTabItemsWrapper>
                </ReactionTab>
              );
            })}
            <ReactionTabsWrapperHR />
          </ReactionTabsWrapper>
          <ModalItemsWrapper>
            <ReactedUsers>
              {/* @ts-ignore */}
              {reactionsDataWithUsers[currentTab]?.map((userData: reactionDataWithUsersProps) => {
                const reactionData = availableReactionTypesArray
                  .find((item) => (item.value === userData.reactionType));

                return (
                  <ReactedInvisibleProfilePreviewButton
                    onClick={() => {
                      setProfilePreviewUserData({
                        _id: userData._id,
                      });
                    }}
                  >
                    <ReactionUserWrapper>
                      <ReactionUserAvatarWrapper>
                        <Avatar
                          user={userData}
                          size={70}
                          avatarPlaceholderFontSize={30}
                        />
                        <ReactionUserAvatarType>
                          <Reaction
                            // @ts-ignore
                            reactionData={reactionData}
                          />
                        </ReactionUserAvatarType>
                      </ReactionUserAvatarWrapper>
                      <ReactionUserName>
                        {userData.profile.firstName}
                      </ReactionUserName>
                      <ReactionUserName>
                        {userData.profile.lastName}
                      </ReactionUserName>
                    </ReactionUserWrapper>
                  </ReactedInvisibleProfilePreviewButton>
                );
              })}
            </ReactedUsers>
          </ModalItemsWrapper>
        </div>
      ) : (
        <CenterPositionWrapper>
          <BeatLoader color={theme?.brand?.primary || defaultTheme.brand.primary} />
        </CenterPositionWrapper>
      )}
    </Modal>
  );
};

export default ReactionsModal;
