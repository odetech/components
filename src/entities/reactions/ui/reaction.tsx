import Lottie from 'lottie-react';
import React from 'react';
import { AnyReactionIcon } from '~/components/icons';
import { ReactionIconWrapper } from './styles';

export const Reaction = ({
  reactionData,
  tooltip,
}: {
  reactionData: {
    value: string,
    label: string,
    staticSvg: string,
    lottieAnimation: any,
  },
  tooltip?: boolean,
}) => (!tooltip ? (
  <AnyReactionIcon
    src={`data:image/svg+xml;utf8,${encodeURIComponent(reactionData.staticSvg)}`}
  />
) : (
  <ReactionIconWrapper>
    <div
      id={`${reactionData.value.toLowerCase()}-icon`}
    >
      <AnyReactionIcon
        src={`data:image/svg+xml;utf8,${encodeURIComponent(reactionData.staticSvg)}`}
      />
    </div>
    <div
      id={`${reactionData.value.toLowerCase()}-icon-animated`}
    >
      <Lottie animationData={JSON.parse(JSON.stringify(reactionData.lottieAnimation))} />
    </div>
  </ReactionIconWrapper>
));
