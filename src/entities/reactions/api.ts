import { attach, createEffect } from 'effector';
import { $api } from '~/config';
import OdeCloud from '@odecloud/odecloud';

export type ReactionData = {
  value: string,
  label: string,
  staticSvg: string,
  lottieAnimation: any,
}

type FetchReactionsProps = {
  api: OdeCloud;
  reactionsData: ReactionData,
};

const fetchReactionsWithApiFx = createEffect<FetchReactionsProps, ReactionData[]>(
  // @ts-ignore
  async ({
    api, reactionsData,
  }) => {
    let updatedReactionsData: ReactionData[] = structuredClone(reactionsData);

    const cacheReactions = localStorage.getItem('reactions-lib') || null;
    if (cacheReactions
      && JSON.parse(cacheReactions || '')?.length === updatedReactionsData.length) {
      updatedReactionsData = JSON.parse(cacheReactions || '');
    } else {
      await Promise.all(updatedReactionsData.map(async (reactionData, index) => {
        const reactionValue = reactionData.value === 'likes' ? 'like' : reactionData.value;
        const svg = await fetch(
          `${api._endpoint?.replace('/api/v1', '')}/static/reactions/svg/${reactionValue}.svg`,
        );
        const animation = await fetch(
          `${api._endpoint?.replace('/api/v1', '')}/static/reactions/json/${reactionValue}.json`,
        );
        updatedReactionsData[index].staticSvg = await svg.text();
        updatedReactionsData[index].lottieAnimation = await animation.json();
      }));

      localStorage.setItem('reactions-lib', JSON.stringify(updatedReactionsData));
    }

    return updatedReactionsData;
  },
);

export const fetchReactionsFx = attach({
  effect: fetchReactionsWithApiFx,
  source: $api,
  mapParams: ({ reactionsData }, api) => ({ api: api!, reactionsData }),
});
