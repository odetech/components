import { createEvent, createStore, sample } from 'effector';
import { combineEvents } from 'patronum';
import { $$themeModel } from '~/entities/theme';
import { $$usersModel } from '~/entities/users';

const $isComponentsInitted = createStore(false);

const statusUpdated = createEvent<any>();

combineEvents({
  events: [
    $$themeModel.themesStarted,
    $$usersModel.api.fetchUsersFx.doneData,
  ],
  target: statusUpdated,
});

sample({
  clock: statusUpdated,
  fn: () => true,
  target: $isComponentsInitted,
});

export {
  $isComponentsInitted,
};
