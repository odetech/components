import {
  attach, createEvent, createStore, Effect, sample,
} from 'effector';
import OdeCloud from '@odecloud/odecloud';
import { spread } from 'patronum';
import { Database } from 'firebase/database';
import { applyJson } from '~/helpers/globalHelpers';

const componentsInit = createEvent<{
  api: OdeCloud | null,
  currentUser: any,
  firebase: Database | null,
}>();
const componentsStarted = createEvent<any>();

const $api = createStore<OdeCloud | null>(null);
const $currentUser = createStore<any>(null);
const currentUserUpdated = createEvent<Record<string, any>>();
const $firebase = createStore<Database | null>(null);

sample({
  clock: componentsInit,
  target: spread({
    targets: {
      api: $api,
      currentUser: $currentUser,
      firebase: $firebase,
    },
  }),
});

sample({
  clock: currentUserUpdated,
  source: $currentUser,
  fn: (user, data) => applyJson({ ...user }, data),
  target: $currentUser,
});

sample({
  clock: $api,
  source: $currentUser,
  filter: (api) => api !== null,
  target: componentsStarted,
});

export type Api = { api: OdeCloud };

// Общий хелпер для attach
const attachWithApi = <Params, Result>(effect: Effect<Params & Api, Result>) => attach({
  source: $api,
  effect,
  mapParams: (params: Params, api) => ({ ...params, api }) as Params & Api,
});

export {
  $api,
  $firebase,
  $currentUser,
  componentsInit,
  currentUserUpdated,
  componentsStarted,
  attachWithApi,
};
