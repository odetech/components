import {
  createEffect, createEvent, createStore, sample,
} from 'effector';
import { onValue, ref } from 'firebase/database';
import { $firebase } from '~/config';

type IsOnlineRecord = {
  status: boolean;
  unsubscribe: () => void;
};

function getDomain() {
  if (window.origin.includes('beta')) return '-beta';
  if (window.origin.includes('alpha')) return '-alpha';
  if (window.origin.includes('localhost')) return '-beta';
  return '';
}

// Store for user's status
const $isOnlineUsers = createStore<Record<string, IsOnlineRecord>>({});

// We should subscribe on changes, update store, and unsubscribed
const subscribed = createEvent<string>();
export const updated = createEvent<{ userId: string, status: boolean }>();
const unsubscribed = createEvent<string>();

// subscribe and unsubscribe for firebase
export const subscribeFx = createEffect(({ userId, firebase }: { userId: string, firebase: any }) => ({
  unsubscribe: onValue(ref(firebase, `/presences/${userId}/tasks${getDomain()}`).ref, (snapshot) => {
    const data = snapshot.val();
    updated({
      userId,
      status: data?.isOnline || false,
    });
  }),
  userId,
}));

const unsubscribeFx = createEffect(({ userId, data }: { userId: string, data: Record<string, IsOnlineRecord> }) => {
  if (data[userId]) {
    data[userId].unsubscribe();
  }
  return userId;
});

// on subscribe we call subscribe function only if userId doesn't exist in store
sample({
  clock: subscribed,
  source: {
    isOnlineUsers: $isOnlineUsers,
    firebase: $firebase,
  },
  filter: ({ isOnlineUsers, firebase }, userId) => isOnlineUsers[userId] === undefined && firebase !== null,
  fn: ({ firebase }, userId) => ({ userId, firebase }),
  target: subscribeFx,
});

// on unsubscribe we call unsubscribe function if userId doesn't exist in store
sample({
  clock: unsubscribed,
  source: {
    isOnlineUsers: $isOnlineUsers,
    firebase: $firebase,
  },
  filter: ({ isOnlineUsers, firebase }, userId) => isOnlineUsers[userId] !== undefined && firebase !== null,
  fn: ({ isOnlineUsers }, removeId) => ({
    data: isOnlineUsers,
    userId: removeId,
  }),
  target: unsubscribeFx,
});

// if subscribe is success we add unsubscribe function to store
sample({
  clock: subscribeFx.doneData,
  source: $isOnlineUsers,
  fn: (currentUsers, newSubscriber) => ({
    ...currentUsers,
    [newSubscriber.userId]: {
      status: false,
      unsubscribe: newSubscriber.unsubscribe,
    },
  }),
  target: $isOnlineUsers,
});

sample({
  clock: unsubscribeFx.doneData,
  source: $isOnlineUsers,
  fn: (currentUsers, removeId) => {
    const copy = { ...currentUsers };
    delete copy[removeId];
    return copy;
  },
  target: $isOnlineUsers,
});

sample({
  clock: updated,
  source: $isOnlineUsers,
  fn: (currentUsers, newSubscriber) => ({
    ...currentUsers,
    [newSubscriber.userId]: {
      ...currentUsers[newSubscriber.userId],
      status: newSubscriber.status,
    },
  }),
  target: $isOnlineUsers,
});

export const $$isOnlineModel = {
  $isOnlineUsers,
  subscribed,
  unsubscribed,
};
