import { useStoreMap } from "effector-react";
import { useEffect } from "react";
import { $$isOnlineModel } from "./model";

export const useIsOnline = (currentUserId: string) => {
  const isOnlineUser = useStoreMap({
    store: $$isOnlineModel.$isOnlineUsers,
    keys: [currentUserId],
    fn: (users, [id]) => users[id!],
  });

  useEffect(() => {
    if (currentUserId) {
      $$isOnlineModel.subscribed(currentUserId);
      return () => $$isOnlineModel.unsubscribed(currentUserId);
    }
    return () => {};
  }, [currentUserId]);

  return isOnlineUser;
};
