import React, {
  ChangeEvent, useCallback,
  useEffect, useMemo, useRef, useState,
} from 'react';

type EditableTextProps = {
  onChange: (newText: string) => void;
  initialText?: string;
  className?: string;
  onEdit?: (newText: string) => void;
  setIsEdit?: (isEdit: boolean) => void;
  autoHeight?: boolean;
  isEdit?: boolean;
  StaticComponent: React.FunctionComponent<any>;
  EditComponent: React.FunctionComponent<any>;
  isEditOnEmptyInput?: boolean;
  isFocused?: boolean;
  isControllable?: boolean;
  renderKey?: string;
  onChangeSize?: (value: number) => void;
  [key: string]: any;
};

export const EditableText = ({
  onChange,
  initialText,
  StaticComponent,
  EditComponent,
  onEdit,
  setIsEdit,
  isEdit,
  isControllable,
  autoHeight,
  className,
  isEditOnEmptyInput,
  renderKey,
  isFocused,
  onChangeSize,
  ...rest
} : EditableTextProps) => {
  const textStaticRef = useRef<any>(null);
  const [isInnerEdit, setIsInnerEdit] = useState<boolean>(false);
  const [text, setText] = useState<string>('');
  const [elementRect, setElementRect] = useState<HTMLInputElement | null>(null);

  const handleRect = useCallback((node) => {
    setElementRect(node);
  }, []);

  const onChangeText = (value: string) => {
    setText(value);
    if (isControllable && onEdit) {
      onEdit(value);
    }
  };

  const setIsEditMode = useMemo(() => (isControllable && setIsEdit ? setIsEdit : setIsInnerEdit
  ), [isControllable, setIsEdit]);

  const isEditMode = useMemo(() => (isControllable ? isEdit : isInnerEdit), [isControllable, isEdit, isInnerEdit]);

  const onChangeTaskTitle = (event: ChangeEvent<HTMLInputElement> | string) => {
    if (typeof event !== 'string') {
      onChangeText(event.currentTarget.value);
      if (autoHeight) {
        if (event.currentTarget.value) {
          // eslint-disable-next-line no-param-reassign
          event.currentTarget.style.height = `${event.currentTarget.scrollHeight}px`;
        } else {
          // eslint-disable-next-line no-param-reassign
          event.currentTarget.style.height = '38px';
        }
      }
    } else {
      onChangeText(event);
    }
  };

  const onBlurTaskTitle = () => {
    if (text !== initialText && !isControllable) {
      onChange(text);
    }
    if (!(isEditOnEmptyInput && text === '') && !isControllable) {
      setIsEditMode(false);
    }
  };

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    if (!textStaticRef.current) return () => {};
    const resizeObserver = new ResizeObserver(() => {
      if (onChangeSize && textStaticRef.current) onChangeSize(textStaticRef.current.offsetHeight);
    });
    resizeObserver.observe(textStaticRef.current);
    return () => resizeObserver.disconnect();
  }, [textStaticRef.current]);

  useEffect(() => {
    if (initialText) {
      onChangeText(initialText);
      setIsEditMode(false);
    }
  }, [initialText]);

  useEffect(() => {
    if (!isEdit) {
      onChangeText(initialText || '');
    }
  }, [isEdit]);

  useEffect(() => {
    onChangeText(initialText ?? '');
    setIsEditMode(false);
  }, [initialText, renderKey]);

  useEffect(() => {
    if (isEditOnEmptyInput && text === '' && !isEditMode) {
      setIsEditMode(true);
    }
  }, [isEditOnEmptyInput, text, isEditMode]);

  useEffect(() => {
    if (isEditMode) {
      if (elementRect) {
        setTimeout(() => {
          if (elementRect) {
            elementRect.style.height = `${elementRect.scrollHeight}px`;
            elementRect.focus();
          }
        }, 0);
      }
    }
  }, [isEditMode, elementRect]);

  useEffect(() => {
    if (isFocused && initialText === '' && text === '') {
      setIsEditMode(true);
      setTimeout(() => {
        if (elementRect) {
          elementRect.focus();
        }
      }, 300);
    }
  }, [isFocused, elementRect]);

  if (isEditMode) {
    return (
      <EditComponent
        value={text!}
        onChange={onChangeTaskTitle}
        onBlur={onBlurTaskTitle}
        ref={handleRect}
        className={className}
        autofocus
        {...rest}
      />
    );
  }

  return (
    <StaticComponent
      ref={textStaticRef}
      onClick={() => setIsEditMode(true)}
      type="button"
      value={text}
      className={className}
    >
      {text}
    </StaticComponent>
  );
};
