import { jsx } from 'slate-hyperscript';
import escapeHtml from 'escape-html';
import { Descendant, Text } from 'slate';
import imageExtensions from 'image-extensions';
import isUrl from 'is-url';
import { ArticleFileProps } from './types';

// @ts-ignore
export const serialize = (node) => {
  if (Text.isText(node)) {
    let string = escapeHtml(node.text);
    // @ts-ignore
    if (node.bold) {
      string = `<strong>${string}</strong>`;
    }
    // @ts-ignore
    if (node.italic) {
      string = `<em>${string}</em>`;
    }
    // @ts-ignore
    if (node.strikethrough) {
      string = `<del>${string}</del>`;
    }
    // @ts-ignore
    if (node.underline) {
      string = `<ins>${string}</ins>`;
    }
    // @ts-ignore
    if (node.code) {
      string = `<code>${string}</code>`;
    }
    return string;
  }

  // @ts-ignore
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const children = node.children.map((n) => serialize(n)).join('');
  switch (node.type) {
    case 'paragraph':
      return `<p>${children}</p>`;
    case 'header1':
      return `<h1>${children}</h1>`;
    case 'header2':
      return `<h2>${children}</h2>`;
    case 'header3':
      return `<h3>${children}</h3>`;
    case 'header4':
      return `<h4>${children}</h4>`;
    case 'header5':
      return `<h5>${children}</h5>`;
    case 'header6':
      return `<h6>${children}</h6>`;
    case 'list-item':
      return `<li>${children}</li>`;
    case 'bulleted-list':
      return `<ul>${children}</ul>`;
    case 'numbered-list':
      return `<ol>${children}</ol>`;
    case 'code-block':
      return `<pre>${children}</pre>`;
    case 'code-line':
      return `${children}\n`;
    case 'link':
      return `<a href="${escapeHtml(node.url)}">${children}</a>`;
    case 'mention':
      // eslint-disable-next-line max-len,no-underscore-dangle
      return `<span data-mention data-id="${node.character._id}" data-firstname="${node.character.profile.firstName}" data-lastname="${node.character.profile.lastName}" href="${escapeHtml(node.url)}">@${node.character.profile.firstName} ${node.character.profile.lastName}</span>`;
    case 'hashtag':
      // eslint-disable-next-line max-len
      return `<span data-hashtag data-id="${node.character._id}" data-value="${node.character.value}" href="${escapeHtml(node.url)}">#${node.character.value}</span>`;
    case 'image':
      // eslint-disable-next-line max-len
      return `<img src="${isUrl(node.url) ? escapeHtml(node.url) : node.url}" data-name="${escapeHtml(node.name)}" data-uiid="${escapeHtml(node.uiId)}" alt="${escapeHtml(node.alt)}">${children}</img>`;
    default:
      return children;
  }
};

export const findIndexFirstNotEmpty = (formattedValues: any) => formattedValues.findIndex((item: any) => !(
  item.type === 'paragraph' && item.children.filter((child: any) => !child.text && !child.type).length
));

export const normalize = (values: any, isDeep?: boolean) => {
  const filteredValues = isDeep ? values : values.filter((el: any) => el.text !== '\n');

  const formattedValues: any[] = [];
  filteredValues.forEach((el: any) => {
    if (!isDeep && el.text) {
      formattedValues.push({ type: 'paragraph', children: [{ text: el.text }] });
    } else if (!isDeep && el.type === 'image') {
      formattedValues.push({ type: 'paragraph', children: [el] });
    } else if (el.type) {
      const normalizedChildren = normalize(el.children, true);
      let children = normalizedChildren;

      if (el.type === 'code-block') {
        children = [];
        normalizedChildren.forEach((child) => {
          if (child.text) {
            children.push(...child.text.split('\n').filter(Boolean).map((c: string) => ({
              children: [{ text: c }],
              type: 'code-line',
            })));
          } else {
            children.push(child);
          }
        });
      }

      const formattedElement = { ...el, children };
      formattedValues.push(formattedElement);
    } else {
      formattedValues.push(el);
    }
  });

  // If there is a reason to remove empty lines, then uncomment this code
  // if (!isDeep) {
  //   const startIndex = findIndexFirstNotEmpty(formattedValues);
  //   const endIndex = findIndexFirstNotEmpty([...formattedValues].reverse());
  //
  //   if (endIndex >= 4) {
  //     formattedValues.splice((startIndex + 3), ((endIndex - 2) - 1));
  //   }
  // }

  return formattedValues;
};

const ELEMENT_TAGS = {
  BLOCKQUOTE: () => ({ type: 'paragraph' }),
  // @ts-ignore
  A: (el) => {
    if (el.getAttribute('href')?.includes('?mention=@')) {
      return ({
        type: 'mention',
        character: {
          _id: el.getAttribute('href').split('?mention=@')[1],
          profile: {
            firstName: el.getAttribute('data-value'),
            lastName: '',
          },
        },
      });
    }
    return ({ type: 'link', url: el.getAttribute('href') });
  },
  // @ts-ignore
  H: (el) => {
    if (el.getAttribute('href')?.includes('?hashtag=#')) {
      return ({
        type: 'hashtag',
        character: {
          _id: el.getAttribute('href').split('?hashtag=#')[1],
          value: el.getAttribute('data-value'),
        },
      });
    }
    return ({ type: 'link', url: el.getAttribute('href') });
  },
  H1: () => ({ type: 'header1' }),
  H2: () => ({ type: 'header2' }),
  H3: () => ({ type: 'header3' }),
  H4: () => ({ type: 'header4' }),
  H5: () => ({ type: 'header5' }),
  H6: () => ({ type: 'header6' }),
  // @ts-ignore
  IMG: (el, files: ArticleFileProps[]) => {
    const file = files.find((f) => f.uiId === el.getAttribute('data-uiid'));
    // @ts-ignore
    const url = file?.cloudinary?.secure_url || file?.url;
    return {
      type: 'image',
      url: el.getAttribute('data-uiid')
        ? url || el.getAttribute('src') : el.getAttribute('src'),
      name: el.getAttribute('data-name') || el.getAttribute('alt'),
      uiId: el.getAttribute('data-uiid'),
      alt: el.getAttribute('alt') || el.getAttribute('data-name'),
    };
  },
  LI: () => ({ type: 'list-item' }),
  OL: () => ({ type: 'numbered-list' }),
  UL: () => ({ type: 'bulleted-list' }),
  PRE: () => ({ type: 'code-block' }),
  P: () => ({ type: 'paragraph' }),
};

const TEXT_TAGS = {
  DEL: () => ({ strikethrough: true }),
  EM: () => ({ italic: true }),
  STRONG: () => ({ bold: true }),
  INS: () => ({ underline: true }),
  CODE: () => ({ code: true }),
};

// @ts-ignore
export const deserialize = (el, files: ArticleFileProps[]) => {
  if (el.nodeType === 3) {
    return el.textContent;
  } if (el.nodeType !== 1) {
    return null;
  }
  // @ts-ignore
  let children = Array.from(el.childNodes).map((element) => deserialize(element, files)).flat();
  if (children.length === 0) {
    children = [{ text: '' }];
  }

  if (el.nodeName === 'BODY') {
    return jsx('fragment', {}, children);
  }

  if (el.nodeName === 'SPAN') {
    if (el.hasAttribute('data-mention')) {
      return jsx(
        'element',
        {
          type: 'mention',
          character: {
            _id: el.getAttribute('data-id'),
            profile: {
              firstName: el.getAttribute('data-firstname'),
              lastName: el.getAttribute('data-lastname'),
            },
          },
        },
        [{ text: '' }],
      );
    }
    if (el.hasAttribute('data-hashtag')) {
      return jsx(
        'element',
        {
          type: 'hashtag',
          character: {
            _id: el.getAttribute('data-id'),
            value: el.getAttribute('data-value'),
          },
        },
        [{ text: '' }],
      );
    }
  }

  if (el.nodeName === 'BR') {
    return jsx('text', { text: '\n' }, children);
  }

  // @ts-ignore
  if (ELEMENT_TAGS[el.nodeName]) {
    // @ts-ignore
    const attrs = ELEMENT_TAGS[el.nodeName](el, files);
    return jsx('element', attrs, children);
  }

  // @ts-ignore
  if (TEXT_TAGS[el.nodeName]) {
    // @ts-ignore
    const attrs = TEXT_TAGS[el.nodeName](el);
    // filter 'cuz the cropper of content for 'show more' in articles doesn't close tags
    // @ts-ignore
    return children.filter((child) => !child?.type).map((child) => jsx('text', attrs, child));
  }

  return children;
};

export const formateValue = (html: string, files: ArticleFileProps[]) => {
  const document = new DOMParser().parseFromString(html, 'text/html');
  return deserialize(document.body, files);
};

export const getMentionsFromValue = (value: string, userIds?: string[]) => {
  const document = new DOMParser().parseFromString(value, 'text/html');
  const mentions = document.querySelectorAll('[data-mention]');
  const mentionUserIdList: string[] = [];
  mentions.forEach((mention) => {
    const id = mention.getAttribute('data-id');

    if (id && id !== 'channel') {
      mentionUserIdList.push(id);
    }
    if (id && id === 'channel' && userIds?.length) {
      mentionUserIdList.push(...userIds);
    }
  });
  const setList = new Set(mentionUserIdList);
  // @ts-ignore
  return [...setList];
};

export const getHashtagsFromValue = (value: string) => {
  const document = new DOMParser().parseFromString(value, 'text/html');
  const hashtags = document.querySelectorAll('[data-hashtag]');
  const hashtagUserIdList: string[] = [];
  hashtags.forEach((hashtag) => {
    const id = hashtag.getAttribute('data-id');

    hashtagUserIdList.push(id || '');
  });
  const setList = new Set(hashtagUserIdList);
  // @ts-ignore
  return [...setList];
};

export const getFiles = (value: Descendant[]) => {
  const files: {
    url: string, uiId: string, isImg: boolean, name: string,
  }[] = [];
  value.forEach((element: any) => {
    // @ts-ignore
    if (element.type === 'image') {
      // @ts-ignore
      const extension = element.url.split(/[#?]/)[0].split('.').pop().trim();
      // @ts-ignore
      const extensionName = element.name?.split(/[#?]/)[0].split('.').pop().trim();
      const isImg = imageExtensions.includes(extension.toLowerCase())
        || imageExtensions.includes(extensionName.toLowerCase());
      files.push({
        url: element.url, uiId: element.uiId, isImg, name: element.name,
      });
    }
    // @ts-ignore
    if (element.children?.length) {
      // @ts-ignore
      files.push(...getFiles(element.children));
    }
  });

  return files;
};

export const IS_SAFARI = typeof navigator !== 'undefined'
  && /Version\/[\d\.]+.*Safari/.test(navigator.userAgent);
