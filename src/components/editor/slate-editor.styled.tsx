import React from 'react';
import styled from '@emotion/styled';
import { Editable } from 'slate-react';
import { transparentize } from 'polished';
import { getVerticalScrollbarStyles } from '../../styles';
import { defaultTheme } from '../../styles/themes';

type StyledEditorProps = {
  maxInputHeight: number | string,
};

export const StyledEditor = styled('input')<StyledEditorProps>`
  padding: 12px 20px;
  max-height: ${(props) => (
    Number.isInteger(props.maxInputHeight)
      ? `${props.maxInputHeight || 500}px`
      : props.maxInputHeight
  )};
  overflow: auto;
   fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};        
  ${({ theme }) => getVerticalScrollbarStyles(theme)}
`.withComponent(Editable);

type EditorWrapProps = {
  readOnly?: boolean,
  hideToolbar?: boolean,
};

export const EditorWrap = styled('div')<EditorWrapProps>`
  width: 100%;
  border: ${({ hideToolbar, readOnly, theme }) => {
    if (readOnly) {
      return 'none';
    }
    const borderColor = hideToolbar
      ? theme?.outline?.[100] || defaultTheme.outline[100]
      : theme?.outline?.[200] || defaultTheme.outline[200];

    return `1px solid ${borderColor}`;
  }};
  border-radius: 6px;
  position: relative;
  white-space: initial;
  word-break: break-word;
  font-weight: 500;
  line-height: 22px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};        

  &:hover {
    border: ${({ readOnly, theme }) => (readOnly
    ? 'none' : `1px solid ${theme?.outline?.[200] || defaultTheme.outline[200]}`)};
  }

  p {
    margin-top: 0;
    margin-bottom: 4px;
    
    &:last-child {
      margin-bottom: 0;
    }
  }

  ul {
    margin-bottom: 4px;
    padding-left: 16px;
  }

  ol {
    margin-bottom: 4px;
    padding-left: 16px;
  }
  
  code {
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    background-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
    padding: 0 2px;
    border-radius: 2px;
  }

  pre {
    border-radius: 4px;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    background-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
    padding: 4px 8px;
    margin-bottom: 4px;
    white-space: pre-wrap;
    word-break: break-word;
  }

  a {
    color: ${({ theme }) => theme?.info?.primary || defaultTheme.info.primary};

    &:hover {
      text-decoration: underline;
    }
  }
  
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    
    &:not(:first-of-type) {
      margin-top: 28px;
    }    
    
    &:not(:last-child) {
      margin-bottom: 16px;
    }
  }
  
  *[data-slate-placeholder="true"] {
    width: auto !important;
  }
  
  img {
    max-width: 100%;
  }
`;

type ButtonToolbarWrapProps = {
  isHidden?: boolean,
};

export const ButtonToolbarWrap = styled((props: any) => (
  <div
    {...props}
    className={`${props.className} odecloud-editor-toolbar`}
  />
))<ButtonToolbarWrapProps>`
  display: ${({ isHidden }) => (isHidden ? 'none' : 'flex')};
  justify-content: space-between;
  border-top: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  padding: 0 4px;
`;

export const ButtonToolbar = styled('div')`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  position: relative;
  
  @media screen and (max-width: 549px) {
    & > *:not(:nth-of-type(1)):not(:nth-of-type(2)):not(:nth-of-type(3)):not(:nth-of-type(4)) {
      display: none;
    }
  }
    
  &:last-of-type {
    margin-left: auto;
    justify-content: flex-end;
  }
`;

type EditorButtonProps = {
  active?: boolean;
  actionButtonClassName?: string;
};

export const EditorButton = styled((props: any) => (
  <button
    type="button"
    {...props}
    className={`${props.className} ${props.actionButtonClassName || ''}`}
  />
))<EditorButtonProps>`
  height: 36px;
  width: 36px;
  background: transparent;
  border: none;
  cursor: pointer;

  svg {
    transition: 0.3s;
    height: 24px;
    width: 24px;
    opacity: ${(props) => (props.active ? 1 : 0.5)};
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};      
  }

  &:hover svg {    
    fill: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    opacity: 1;
  }
`;

export const Dropdown = styled('div')`
  position: relative;
`;

type MenuProps = {
  position?: 'top' | 'bottom',
}

export const DropdownMenu = styled('div')<MenuProps>`
  position: absolute;
  ${(props) => props.position || 'bottom'}: 100%;
  right: 0;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  border-radius: 4px;
`;

export const LinkDropdownMenu = styled(DropdownMenu)`
  padding: 8px;
  display: flex;
  flex-direction: column;
  align-items: center;

  .odecloud-input-wrapper {
    margin: 0 0 5px 0;
  }
`;

export const EmojisPickerWrap = styled(DropdownMenu)`
  z-index: 1103;
  .emoji-mart {
    background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
    padding: 0;
    border: none;
    border-radius: 0;
  }
  
  .emoji-mart-search-icon {
    top: 4px
  }
  
  .emoji-mart-search input {
    font-size: 14px;
    background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }
  
  .emoji-mart-anchor {
    padding: 8px 2px;
    color: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
    
    svg {
      height: 16px;
      width: 16px;
    }

    &-selected {
      color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary} !important;
    }
  }

  .emoji-mart-category-label {
    font-size: 14px;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};

    span {
      background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
    }
  }

  .emoji-mart-bar:last-of-type {
    display: none;
  }

  .emoji-mart-category {
    padding: 12px 0;
  }

  .emoji-mart .emoji-mart-emoji {
    height: 32px;
    width: 32px;
    padding: 0;
    margin: 0 4px 4px 0;
  }
  
  .emoji-mart-category-list {
    padding: 0 !important;
  }

  .emoji-mart .emoji-mart-emoji span {
    font-size: 24px !important;
    width: auto !important;
    height: auto !important;
  }

  .emoji-mart-no-results {
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }
  
  .emoji-mart-scroll {
    ${({ theme }) => getVerticalScrollbarStyles(theme)}
  }
`;

export const EditHeader = styled('div')`
  display: flex;
  justify-content: space-between;
  background-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  padding: 5px 20px;
  border-radius: 6px 6px 0 0;

  span, button {
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }
  
  button {
    padding: 0;
    border: none;
    background: transparent;
    transition: 0.3s;
    
    &:hover {
      color: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
    }
  }
`;

type ImageProps = {
  selected?: boolean;
  focused?: boolean;
};

export const Image = styled('img')<ImageProps>`
  max-width: 100%;
  border: 2px dashed ${({ selected, focused, theme }) => (selected
    && focused ? theme?.brand?.secondary || defaultTheme.brand.secondary : 'transparent')};
  border-radius: 10px;
`;

export const ImageButton = styled('button')`
  padding: 0;
  border: none;
  cursor: pointer;
  background: transparent;
  display: block;
  margin-bottom: 4px;
`;

export const PdfButton = styled('button')`
  padding: 0;
  border: none;
  cursor: pointer;
  background: transparent;
  display: flex;
  transition: 0.3s;
  text-align: left;
  width: 100%;
  margin-bottom: 4px;

  span {
    white-space: wrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  
  img {
    height: 32px;
    margin-right: 8px;
  }
  
  &:hover {
    color: ${({ theme }) => theme?.info?.primary || defaultTheme.info.primary};
  }
`;

export const PdfLink = styled(PdfButton)`
`.withComponent('a');

export const PdfWrap = styled(PdfButton)`
`.withComponent('span');

type MentionProps = {
  clickable: boolean,
};

export const Mention = styled('span')<MentionProps>`
  vertical-align: baseline;
  display: inline-block;
  color: ${({ theme }) => theme?.info?.primary || defaultTheme.info.primary};

  ${({ clickable }) => (clickable && 'cursor: pointer;')}
`;

type HashtagProps = {
  clickable: boolean,
};

export const Hashtag = styled('span')<HashtagProps>`
  vertical-align: baseline;
  display: inline-block;
  color: ${({ theme }) => theme?.info?.primary || defaultTheme.info.primary};
  font-weight: bold;

  ${({ clickable }) => (clickable && 'cursor: pointer;')}
`;

export const MenuButton = styled('button')<EditorButtonProps>`
  border: none;
  padding: 8px 12px;
  background: transparent;
  transition: 0.3s;
  cursor: pointer;
  white-space: nowrap;
  opacity: ${(props) => (props.active ? 1 : 0.5)};
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  
  &:hover {
    opacity: 1;
    color: ${({ theme }) => theme?.info?.primary || defaultTheme.info.primary};
  }
`;

type MentionListWrapProps = {
  position?: 'top' | 'bottom',
}

export const MentionListWrap = styled('div')<MentionListWrapProps>`
  top: -9999px;
  left: -9999px;
  position: absolute;
  z-index: 1102;
  background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  border-radius: 4px;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
`;

export const MentionListContent = styled('div')`
  max-height: 200px;
  min-width: 240px;
  
  ${({ theme }) => getVerticalScrollbarStyles(theme)}
`;

type MentionItemProps = {
  active: boolean,
}

export const MentionItem = styled('button')<MentionItemProps>`
  width: 100%;
  display: flex;
  padding: 8px 12px;
  border: none;
  background: ${({ active, theme }) => (active ? transparentize(
    0.8,
    theme?.brand?.primary || defaultTheme.brand.primary,
  ) : 'transparent')};
  color: ${({ active, theme }) => (active
    ? theme?.brand?.primary || defaultTheme.brand.primary
    : theme?.text?.onSurface || defaultTheme.text.onSurface)};
  text-align: left;
  align-items: center;

  & > div {
    margin-right: 8px;
  }
`;

export const OriginalReplyMessageWrap = styled('div')`
  display: flex;
  overflow: hidden;
  outline: unset;
  border: unset;
  fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};        
  border-left: 2px solid ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  padding-left: 4px;
  width: 100%;
  justify-content: space-between;
  margin-bottom: 8px;
  background: transparent;
`;

export const OriginalReplyMessageItemsWrap = styled('div')`
  width: calc(100% - 24px);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const ReplyPerson = styled('div')`
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSecondary};
  font-size: 12px;
  font-weight: 500;
  
  svg {
    width: 16px;
    height: 16px;
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSecondary};
    margin-right: 4px;
  }
`;

export const OriginalReplyMessage = styled('p')<{ wrapReply?: boolean }>`
  margin: 0;
  white-space: ${({ wrapReply }) => (wrapReply ? 'wrap' : 'nowrap')};
  overflow: hidden;
  text-overflow: ellipsis;
  font-size: 12px;
  width: 100%;
  text-align: start;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
`;

export const CloseReplyButton = styled('button')`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 20px;
  background: transparent;
  border: none;
  padding: 0;
  
  svg {
    height: 16px;
    width: 16px;
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  }
  
  &:hover {
    cursor: pointer;
    
    svg {
      fill: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
    }
  }
`;

export const PlayerVideosWrapper = styled('button')`
  margin-top: 20px;
  
  & > *:not(:last-child) {
    margin-bottom: 10px;
  }
`;
