import isUrl from 'is-url';
import {
  Editor, Node, Path, Range, Transforms,
} from 'slate';
import imageExtensions from 'image-extensions';
import { CustomEditor } from './buttons';
import { formateValue, normalize, IS_SAFARI } from './slate-editor.helpers';
import { pdfIcon } from './pdfIcon';
import { generateUUID, getBase64 } from './slate-editor.image-helpers';

const isImageUrl = (url: string) => {
  const ext: string = new URL(url).pathname.split('.').pop() || '';
  return imageExtensions.includes(ext.toLowerCase());
};

// @ts-ignore
const withCustom = (editor, callBack) => {
  const {
    insertData, insertText, isInline, isVoid,
  } = editor;
  // @ts-ignore
  // eslint-disable-next-line no-param-reassign
  editor.isInline = (element) => (['link', 'mention', 'hashtag'].includes(element.type) ? true : isInline(element));

  // @ts-ignore
  // eslint-disable-next-line no-param-reassign
  editor.isVoid = (element) => (['image'].includes(element.type) ? true : isVoid(element));

  const { deleteBackward, insertBreak } = editor;

  // if current selection is void node, insert a default node below
  // @ts-ignore
  // eslint-disable-next-line consistent-return,no-param-reassign
  editor.insertBreak = () => {
    if (!editor.selection || !Range.isCollapsed(editor.selection)) {
      return insertBreak();
    }

    const selectedNodePath = Path.parent(editor.selection.anchor.path);
    const selectedNode = Node.get(editor, selectedNodePath);
    if (Editor.isVoid(editor, selectedNode)) {
      Editor.insertNode(editor, {
        // @ts-ignore
        type: 'paragraph',
        children: [{ text: '' }],
      });
      // eslint-disable-next-line consistent-return
      return;
    }

    insertBreak();
  };

  // if prev node is a void node, remove the current node and select the void node
  // @ts-ignore
  // eslint-disable-next-line consistent-return,no-param-reassign
  editor.deleteBackward = (unit) => {
    if (!editor.selection || !Range.isCollapsed(editor.selection) || editor.selection.anchor.offset !== 0) {
      return deleteBackward(unit);
    }

    const parentPath = Path.parent(editor.selection.anchor.path);
    const parentNode = Node.get(editor, parentPath);
    const parentIsEmpty = Node.string(parentNode).length === 0;

    if (parentIsEmpty && Path.hasPrevious(parentPath)) {
      const prevNodePath = Path.previous(parentPath);
      const prevNode = Node.get(editor, prevNodePath);
      if (Editor.isVoid(editor, prevNode)) {
        return Transforms.removeNodes(editor);
      }
    }

    deleteBackward(unit);
  };

  // @ts-ignore
  // eslint-disable-next-line no-param-reassign
  editor.insertText = (text) => {
    if (text && isUrl(text)) {
      CustomEditor.wrapLink(editor, text);
    } else {
      insertText(text);
    }
  };

  // @ts-ignore
  // eslint-disable-next-line no-param-reassign
  editor.insertData = async (data) => {
    const html = data.getData('text/html');
    const text = data.getData('text/plain');

    let { files } = data;

    if (html.includes('blob:http') && IS_SAFARI) {
      const div = document.createElement('div');
      div.innerHTML = html.trim();
      const src = (div.firstChild as HTMLImageElement)?.getAttribute('src');
      if (src) {
        const uiId = generateUUID();
        await fetch(src!)
          .then((res) => res.blob())
          .then((blob) => {
            files = [new File([blob], `${uiId}.png`, blob)];
          });
      }
    }
    if (files && files.length > 0) {
      await Promise.all(Array.from(files).map(async (file: any) => {
        const uiId = generateUUID();
        // @ts-ignore
        const { name } = file;
        if (callBack) {
          callBack(file, uiId);
        }
        const extensionName = (name.split(/[#?]/)[0].split('.').pop() || '').trim();
        if (!imageExtensions.includes(extensionName.toLowerCase())) {
          CustomEditor.insertFile(editor, pdfIcon, name, uiId);
        } else {
          const result: any = await getBase64(file);
          if (result) {
            CustomEditor.insertFile(editor, result, name, uiId);
          }
        }
      }));
    } else if (text && isUrl(text.trim())) {
      if (isImageUrl(text.trim())) {
        const name = text.trim().split('/').reverse()[0];
        CustomEditor.insertFile(editor, text.trim(), name, '');
      } else {
        CustomEditor.wrapLink(editor, text.trim());
      }
    } else if (html && !html.includes('data-slate-node')) {
      const fragment = normalize(formateValue(html, []));
      Transforms.insertFragment(editor, fragment);
    } else {
      insertData(data);
    }
  };

  return editor;
};

export default withCustom;
