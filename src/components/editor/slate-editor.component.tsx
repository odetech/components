import React, {
  useMemo, useCallback, useState, useEffect, Fragment,
} from 'react';
import {
  createEditor, Editor, Descendant, Range, Transforms, BasePoint,
} from 'slate';
import {
  ReactEditor, Slate, withReact,
} from 'slate-react';
import { withHistory } from 'slate-history';
import isHotkey from 'is-hotkey';
import isUrl from 'is-url';
import { getUserName } from '~/helpers';
import {
  ArrowLeftTopIcon, CloseIcon,
} from '~/modules/chat/icons';
import {
  getAssociatedData,
} from '~/modules/chat/helpers';
import {
  CustomEditor,
  ImageButton,
  MarkButton,
  LinkButton,
  EmojiButton,
  BlockButton,
  SizeButton,
} from './buttons';
import { Element, Leaf } from './slate-editor.elements';
import {
  formateValue, getFiles, normalize, serialize,
} from './slate-editor.helpers';
import withCustom from './withCustom';
import {
  CodeTagsSvg, FileCodeOutlineSvg,
  FormatBoldSvg,
  FormatItalicSvg,
  FormatListBulletedSvg,
  FormatListNumberedSvg,
  FormatStrikethroughSvg,
  FormatUnderlineSvg,
  SendOutlineSvg,
} from './icons';
import {
  ButtonToolbar,
  ButtonToolbarWrap,
  CloseReplyButton,
  EditHeader,
  EditorButton,
  EditorWrap, OriginalReplyMessage, OriginalReplyMessageItemsWrap, OriginalReplyMessageWrap,
  ReplyPerson,
  StyledEditor,
} from './slate-editor.styled';
import SlateEditorMentionList from './buttons/slate-editor.mention-list';
import { ArticleFileProps } from './types';
import { dataURLtoFile } from './slate-editor.image-helpers';

// TODO bullets bug
// TODO color ?

const INITIAL_VALUE = [{ children: [{ text: '' }], type: 'paragraph' }];

type SlateEditorProps = {
  setIsOpenLightbox?: (isOpenLightbox: boolean) => void,
  setPhotoIndex?: (photoIndex: number) => void,
  setImages?: (images: string[]) => void,
  field: string,
  setFieldValue: (field: string, value: any) => void,
  value: string,
  editValue?: string,
  isMinimal?: boolean,
  wrapReply?: boolean,
  isWithoutSending?: boolean,
  uploadImageCallBack: (file: File, uiId: string, src?: string) => void,
  closeEdit?: () => void,
  handleSubmit: () => void,
  userSuggestions?: {
    _id: string,
    profile: {
      firstName: string,
      lastName: string,
    }
  }[],
  files: ArticleFileProps[],
  isChannelMention?: boolean,
  disabled?: boolean,
  replyData?: any,
  submitOnEnter?: boolean,
  withAutoFocus?: boolean,
  position?: 'top' | 'bottom',
  placeholder?: string,
  valueFiles: {
    file: File,
    uiId: string,
  }[],
  hideSubmitButton?: boolean,
  hideAttachButton?: boolean,
  maxInputHeight?: number | string,
  onFocus?: (isFocused: boolean) => void,
  hideToolbar?: boolean,
};

// define word character as all EN letters, numbers, and dash
const wordRegexp = /[0-9a-zA-Z-]/;

const getLeftChar = (editor: ReactEditor, point: BasePoint) => {
  const end = Range.end(editor.selection as Range);
  return Editor.string(editor, {
    anchor: {
      path: end.path,
      offset: point.offset - 1,
    },
    focus: {
      path: end.path,
      offset: point.offset,
    },
  });
};

const getRightChar = (editor: ReactEditor, point: BasePoint) => {
  const end = Range.end(editor.selection as Range);
  return Editor.string(editor, {
    anchor: {
      path: end.path,
      offset: point.offset,
    },
    focus: {
      path: end.path,
      offset: point.offset + 1,
    },
  });
};

export const getCurrentWord = (editor: ReactEditor) => {
  const { selection } = editor; // selection is Range type

  if (selection) {
    const end = Range.end(selection); // end is a Point
    let currentWord = '';
    const currentPosition = JSON.parse(JSON.stringify(end));
    let startOffset = end.offset;
    let endOffset = end.offset;

    // go left from cursor until it finds the non-word character
    while (
      currentPosition.offset >= 0
      && getLeftChar(editor, currentPosition).match(wordRegexp)
    ) {
      currentWord = getLeftChar(editor, currentPosition) + currentWord;
      startOffset = currentPosition.offset - 1;
      // eslint-disable-next-line no-plusplus
      currentPosition.offset--;
    }

    // go right from cursor until it finds the non-word character
    currentPosition.offset = end.offset;
    while (
      currentWord.length
      && getRightChar(editor, currentPosition).match(wordRegexp)
    ) {
      currentWord += getRightChar(editor, currentPosition);
      endOffset = currentPosition.offset + 1;
      // eslint-disable-next-line no-plusplus
      currentPosition.offset++;
    }

    const currentRange: Range = {
      anchor: {
        path: end.path,
        offset: startOffset,
      },
      focus: {
        path: end.path,
        offset: endOffset,
      },
    };

    return {
      currentWord,
      currentRange,
    };
  }

  return {};
};

export const getPreviousWord = (editor: ReactEditor, currentRange: Range) => {
  const point = currentRange.anchor;
  let word = '';
  const currentPosition = JSON.parse(JSON.stringify(point));

  if (!getLeftChar(editor, currentPosition).match(wordRegexp)) {
    word = getLeftChar(editor, currentPosition);
    // eslint-disable-next-line no-plusplus
    currentPosition.offset--;
  } else {
    // go left from cursor until it finds the non-word character
    while (
      currentPosition.offset >= 0
      && getLeftChar(editor, currentPosition).match(wordRegexp)
    ) {
      word = getLeftChar(editor, currentPosition) + word;
      // eslint-disable-next-line no-plusplus
      currentPosition.offset--;
    }
  }

  const range: Range = {
    anchor: {
      path: point.path,
      offset: currentPosition.offset,
    },
    focus: {
      path: point.path,
      offset: point.offset,
    },
  };

  return {
    word,
    range,
  };
};

const SlateEditor = ({
  value: externalValue, editValue, field, setFieldValue, disabled,
  closeEdit, handleSubmit, userSuggestions, isMinimal, uploadImageCallBack,
  setImages, setPhotoIndex, setIsOpenLightbox, files, isChannelMention,
  submitOnEnter, withAutoFocus, placeholder, position, valueFiles, replyData,
  hideSubmitButton, maxInputHeight, onFocus, hideToolbar, hideAttachButton, wrapReply,
}: SlateEditorProps) => {
  // @ts-ignore
  const editor = useMemo(() => withCustom(withReact(withHistory(createEditor())), uploadImageCallBack), []);
  const [value, setValue] = useState<Descendant[]>(INITIAL_VALUE);
  const [mentionTarget, setMentionTarget] = useState<Range | null | undefined>(null);
  const [hashtagTarget, setHashtagTarget] = useState<Range | null | undefined>(null);
  const [mentionListItemIndex, setMentionListItemIndex] = useState(0);
  const [search, setSearch] = useState('');
  const [editorImages, setEditorImages] = useState<string[]>([]);
  const [editorFiles, setEditorFiles] = useState<string[]>([]);

  const replaceValue = (newValue: Descendant[]) => {
    Transforms.deselect(editor);
    const filteredValue = normalize(newValue);
    editor.children = filteredValue.length ? filteredValue : INITIAL_VALUE;
    Editor.normalize(editor, { force: true });
    setValue(editor.children);
    if (withAutoFocus || editValue) {
      Transforms.select(editor, Editor.end(editor, []));
    }
  };

  useEffect(() => {
    if (externalValue !== value.map(serialize).join('') && !editValue) {
      replaceValue(externalValue ? formateValue(externalValue, files) : INITIAL_VALUE);
    }
  }, [externalValue]);

  useEffect(() => {
    if (editValue) {
      replaceValue(formateValue(editValue, files));
    }
  }, [editValue]);

  useEffect(() => {
    const newFiles = getFiles(value);
    if (setImages) {
      if (JSON.stringify(newFiles) !== JSON.stringify(editorFiles)) {
        setEditorImages(newFiles?.filter((file) => file.isImg)
          ?.map(((image) => image.url)) || []);
        setEditorFiles(newFiles?.map(((file) => file.url)) || []);
      }
    }

    setFieldValue('uiIds', newFiles.map((file) => file.uiId));

    const pastedImages = newFiles
      ?.filter((file) => file.uiId && !files.find((fileData) => fileData.uiId === file.uiId))
      ?.filter((file) => !valueFiles.find((fileData) => fileData.uiId === file.uiId))
      ?.filter((file) => !isUrl(file.url));
    pastedImages?.forEach((file) => {
      if (file.url && file.name && file.uiId) {
        const createFile = dataURLtoFile(file.url, file.name);
        uploadImageCallBack(createFile, file.uiId);
      }
    });
  }, [value]);

  const onEditClose = () => {
    if (closeEdit) {
      closeEdit();
    }
  };

  const handleChange = (newValue: Descendant[]) => {
    setValue(newValue);
    setFieldValue(field, newValue.map(serialize).join(''));

    const { selection } = editor;

    if (selection && Range.isCollapsed(selection)) {
      const [start] = Range.edges(selection);
      const wordBefore = Editor.before(editor, start, { unit: 'word' });
      const before = wordBefore && Editor.before(editor, wordBefore);
      const beforeRange = before && Editor.range(editor, before, start);
      const beforeText = beforeRange && Editor.string(editor, beforeRange);
      const mentionBeforeMatch = beforeText && beforeText.match(/^@(.*)?$/);
      const hashTagBeforeMatch = beforeText && beforeText.match(/^#(.*)?$/);
      const after = Editor.after(editor, start);
      const afterRange = Editor.range(editor, start, after);
      const afterText = Editor.string(editor, afterRange);
      const afterMatch = afterText.match(/^(\s|$)/);

      if (mentionBeforeMatch && afterMatch) {
        setMentionTarget(beforeRange || null);
        setSearch(mentionBeforeMatch[1]);
        setMentionListItemIndex(0);
        return;
      }

      const pointBefore = Editor.before(editor, start);
      const pointRange = pointBefore && Editor.range(editor, pointBefore, start);
      const characterBefore = pointRange && Editor.string(editor, pointRange);

      // mention logic
      if (characterBefore === '@') {
        setMentionTarget(pointRange);
        setSearch('');
        setMentionListItemIndex(0);
        return;
      }

      // hashtag logic
      if (characterBefore === '#') {
        setHashtagTarget(pointRange);
      }
      const hashtagValue = hashTagBeforeMatch?.[1]?.replace(/\s+$/, '') || '';
      if (hashTagBeforeMatch?.length && hashTagBeforeMatch?.[0]?.includes('#')
        && (characterBefore === '' || characterBefore === ' ')
        && hashtagTarget && beforeRange && afterMatch && hashtagValue) {
        Transforms.select(editor, {
          ...beforeRange,
          focus: {
            offset: beforeRange.focus.offset,
            path: beforeRange.focus.path,
          },
        });
        Transforms.insertNodes(editor, {
          // @ts-ignore
          type: 'hashtag',
          character: {
            _id: hashTagBeforeMatch?.[1].replace(/\s+$/, '') || '',
            value: hashTagBeforeMatch?.[1].replace(/\s+$/, '') || '',
          },
          children: [{ text: '' }],
        });
        Transforms.move(editor, { unit: 'offset' });
        Transforms.insertText(editor, ' ');
        setHashtagTarget(null);
      }
    }

    setMentionTarget(null);
  };

  const onSubmit = () => {
    if (handleSubmit) {
      handleSubmit();
    }
  };

  const addEmoji = (emoji: string) => {
    setTimeout(() => {
      ReactEditor.focus(editor);
    }, 100);
    editor.insertText(emoji);
  };

  const onImageClick = () => {
    if (setImages && setIsOpenLightbox && setPhotoIndex) {
      setImages(editorImages);
      setIsOpenLightbox(true);
      setPhotoIndex(0);
    }
  };

  const renderElement = useCallback((props: any) => (
    <Element
      {...props}
      onImageClick={onImageClick}
    />
  ), [editorFiles]);

  const renderLeaf = useCallback((props: any) => <Leaf {...props} />, []);

  // for links
  const saveSelection = () => {
    editor.savedSelection = editor.selection;
  };

  const availableMentionOptions = useMemo(() => {
    const suggestions = userSuggestions ? [...userSuggestions] : [];
    if (isChannelMention) {
      suggestions.splice(0, 0, {
        _id: 'channel',
        profile: {
          firstName: 'channel',
          lastName: '',
        },
      });
    }

    const uniquePersonsIds: string[] = [];

    return suggestions
      .filter((c: {
        _id: string,
        profile: {
          firstName: string,
          lastName: string,
        }
      }) => {
        if (!uniquePersonsIds.includes(c._id)) {
          uniquePersonsIds.push(c._id);
          return c?.profile?.firstName?.toLowerCase()?.startsWith((search || '')?.toLowerCase())
            || c?.profile?.lastName?.toLowerCase()?.startsWith((search || '')?.toLowerCase());
        }
        return false;
      });
  }, [search, userSuggestions]);

  const onMentionHover = (newIndex: number) => {
    if (mentionTarget) {
      setMentionListItemIndex(newIndex);
    }
  };

  const onMentionClick = (newIndex: number) => (event: Event) => {
    if (mentionTarget) {
      event.preventDefault();
      Transforms.select(editor, mentionTarget);
      CustomEditor.insertMention(editor, availableMentionOptions[newIndex]);
    }
    setMentionTarget(null);
  };

  const isDisabled = !(/<[^>]*>([^<]+)<\/[^>]*>/i.test(externalValue) || editorFiles.length) || disabled;

  const onKeyDown = useCallback(
    (event: any) => {
      if (mentionTarget) {
        switch (event.key) {
          case 'ArrowDown':
            event.preventDefault();
            setMentionListItemIndex(mentionListItemIndex >= availableMentionOptions.length - 1 ? 0
              : mentionListItemIndex + 1);
            break;
          case 'ArrowUp':
            event.preventDefault();
            setMentionListItemIndex(mentionListItemIndex <= 0 ? availableMentionOptions.length - 1
              : mentionListItemIndex - 1);
            break;
          case 'Tab':
          case 'Enter':
            if (availableMentionOptions[mentionListItemIndex]) {
              event.preventDefault();
              Transforms.select(editor, mentionTarget);
              CustomEditor.insertMention(editor, availableMentionOptions[mentionListItemIndex]);
            }
            setMentionTarget(null);
            break;
          case 'Escape':
            event.preventDefault();
            setMentionTarget(null);
            break;
          default:
            break;
        }
      } else if (hashtagTarget) {
        switch (event.key) {
          case 'Enter':
            event.preventDefault();
            // eslint-disable-next-line no-case-declarations
            const { selection } = editor;

            if (selection && Range.isCollapsed(selection)) {
              const [start] = Range.edges(selection);
              const wordBefore = Editor.before(editor, start, { unit: 'word' });
              const before = wordBefore && Editor.before(editor, wordBefore);
              const beforeRange = before && Editor.range(editor, before, start);
              const beforeText = beforeRange && Editor.string(editor, beforeRange);

              const hashTagBeforeMatch = beforeText && beforeText.match(/^#(.*)?$/);
              const hashtagValue = hashTagBeforeMatch?.[1] || '';
              if (
                hashTagBeforeMatch?.length
                && hashTagBeforeMatch?.[0]?.includes('#')
                && hashtagTarget && beforeRange && hashtagValue) {
                Transforms.select(editor, {
                  ...beforeRange,
                  focus: {
                    offset: beforeRange.focus.offset,
                    path: beforeRange.focus.path,
                  },
                });
                Transforms.insertNodes(editor, {
                  // @ts-ignore
                  type: 'hashtag',
                  character: {
                    _id: hashTagBeforeMatch?.[1] || '',
                    value: hashTagBeforeMatch?.[1] || '',
                  },
                  children: [{ text: '' }],
                });
                Transforms.move(editor, { unit: 'offset' });
                Transforms.insertText(editor, '\n');
                setHashtagTarget(null);
              }
            }
            setHashtagTarget(null);
            break;
          default:
            break;
        }
      } else if (!hideSubmitButton && submitOnEnter && event.keyCode === 13
        && !isHotkey('shift+enter', event) && !isHotkey('mod+enter', event)) {
        event.preventDefault();
        if (!isDisabled) {
          onSubmit();
        }
      } else {
        CustomEditor.onHotKey(editor, event);
      }
    },
    [mentionListItemIndex, search, mentionTarget, hashtagTarget, isDisabled],
  );

  return (
    <Fragment>
      {replyData && replyData.data && (
        <OriginalReplyMessageWrap>
          <OriginalReplyMessageItemsWrap>
            <ReplyPerson>
              <ArrowLeftTopIcon />
              <span>
                Replying to
                {' '}
                {getUserName(replyData.data.createdBy)}
              </span>
            </ReplyPerson>
            <OriginalReplyMessage wrapReply={wrapReply}>
              {getAssociatedData(replyData).previewText}
            </OriginalReplyMessage>
          </OriginalReplyMessageItemsWrap>
          <CloseReplyButton type="button" onClick={onEditClose}>
            <CloseIcon />
          </CloseReplyButton>
        </OriginalReplyMessageWrap>
      )}
      <EditorWrap
        hideToolbar={hideToolbar}
      >
        {editValue && !replyData && (
          <EditHeader>
            <span>Edit</span>
            <button type="button" onClick={onEditClose}>
              Close
            </button>
          </EditHeader>
        )}
        <Slate
          editor={editor}
          value={value}
          onChange={handleChange}
        >
          <StyledEditor
            renderElement={renderElement}
            renderLeaf={renderLeaf}
            onKeyDown={onKeyDown}
            spellCheck="true"
            onBlur={() => {
              saveSelection();
              if (onFocus) {
                onFocus(false);
              }
            }}
            onFocus={() => {
              if (onFocus) {
                onFocus(true);
              }
            }}
            placeholder={placeholder}
            // @ts-ignore
            maxInputHeight={maxInputHeight || 0}
          />
          <ButtonToolbarWrap
            isHidden={hideToolbar}
          >
            {!isMinimal && (
              <ButtonToolbar>
                <MarkButton format="bold" icon={<FormatBoldSvg />} actionButtonClassName="bold-button" />
                <MarkButton format="italic" icon={<FormatItalicSvg />} actionButtonClassName="italic-button" />
                <MarkButton format="underline" icon={<FormatUnderlineSvg />} actionButtonClassName="underline-button" />
                <MarkButton
                  format="strikethrough"
                  icon={<FormatStrikethroughSvg />}
                  actionButtonClassName="strikethrough-button"
                />

                <MarkButton format="code" icon={<CodeTagsSvg />} actionButtonClassName="code-tags-button" />
                <BlockButton
                  format="code-block"
                  icon={<FileCodeOutlineSvg />}
                  actionButtonClassName="code-outline-button"
                />

                <SizeButton position={position} actionButtonClassName="size-button" />

                <BlockButton
                  format="bulleted-list"
                  icon={<FormatListBulletedSvg />}
                  actionButtonClassName="bulleted-list-button"
                />
                <BlockButton
                  format="numbered-list"
                  icon={<FormatListNumberedSvg />}
                  actionButtonClassName="numbered-list-button"
                />
                <LinkButton position={position} actionButtonClassName="link-button" />
              </ButtonToolbar>
            )}
            <ButtonToolbar>
              <EmojiButton onSelect={addEmoji} position={position} actionButtonClassName="emoji-button" />
              {!hideAttachButton && (
                <ImageButton uploadImageCallBack={uploadImageCallBack} actionButtonClassName="image-button" />
              )}
              {!hideSubmitButton && (
                <EditorButton
                  active={!isDisabled}
                  type="submit"
                  onSubmit={onSubmit}
                  disabled={isDisabled}
                  actionButtonClassName="submit-button"
                >
                  <SendOutlineSvg />
                </EditorButton>
              )}
            </ButtonToolbar>
          </ButtonToolbarWrap>
          <SlateEditorMentionList
            editor={editor}
            target={mentionTarget}
            index={mentionListItemIndex}
            search={search}
            mentionOptions={availableMentionOptions}
            onClick={onMentionClick}
            onHover={onMentionHover}
            position={position}
          />
        </Slate>
      </EditorWrap>
    </Fragment>
  );
};

export { SlateEditor };

export default SlateEditor;
