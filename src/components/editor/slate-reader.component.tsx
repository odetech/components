import React, {
  useMemo, useCallback, useState, useEffect,
} from 'react';
import {
  createEditor, Descendant, Editor, Transforms,
} from 'slate';
import { Editable, Slate, withReact } from 'slate-react';
import { withHistory } from 'slate-history';
import { Element, Leaf } from './slate-editor.elements';
import {
  formateValue, getFiles, normalize, serialize,
} from './slate-editor.helpers';
import withCustom from './withCustom';
import { ArticleFileProps } from './types';
import { EditorWrap, PlayerVideosWrapper } from './slate-editor.styled';

const INITIAL_VALUE = [{ children: [{ text: '' }], type: 'paragraph' }];

type SlateEditorProps = {
  setIsOpenLightbox: (isOpenLightbox: boolean) => void,
  setPhotoIndex: (photoIndex: number) => void,
  setImages: (images: string[]) => void,
  setShowModal: (isShow: boolean) => void,
  setPdfFile: (pdfFile: string) => void,
  value: string,
  files: ArticleFileProps[],
  links?: string[],
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
};

const SlateReader = ({
  value: externalValue, setImages, setPhotoIndex, setIsOpenLightbox, setPdfFile, setShowModal, files, onMentionClick,
  links, onHashtagClick,
}: SlateEditorProps) => {
  // @ts-ignore
  const editor = useMemo(() => withCustom(withReact(withHistory(createEditor()))), []);
  const [value, setValue] = useState<Descendant[]>(
    externalValue ? normalize(formateValue(externalValue, files)) : INITIAL_VALUE,
  );
  const [images, setStateImages] = useState(
    files && files.length !== 0
      ? getFiles(value).filter((image) => image.isImg).map((image) => image.url) : [],
  );

  const replaceValue = (newValue: string) => {
    Transforms.deselect(editor);
    const filteredValue = normalize(newValue);
    editor.children = filteredValue?.length ? filteredValue : INITIAL_VALUE;
    Editor.normalize(editor, { force: true });
    setValue(editor.children);
  };

  useEffect(() => {
    if (externalValue !== value.map(serialize).join('')) {
      replaceValue(externalValue ? formateValue(externalValue, files) : INITIAL_VALUE);
    }
  }, [externalValue]);

  useEffect(() => {
    if (files && files.length !== 0) {
      setStateImages(getFiles(value).filter((image) => image.isImg).map((image) => image.url));
    }
  }, [value]);

  const onImageClick = (imgData: any) => {
    setImages(images);
    setIsOpenLightbox(true);
    setPhotoIndex(images.findIndex((img) => img === imgData.url) || 0);
  };

  const onPdfClick = (link: string) => () => {
    setShowModal(true);
    setPdfFile(link);
  };

  const renderElement = useCallback((props: any) => (
    <Element
      {...props}
      onImageClick={(e: any) => {
        e.preventDefault();
        if (images.length !== 0) {
          onImageClick(props?.element);
        }
      }}
      onPdfClick={onPdfClick}
      onMentionClick={onMentionClick}
      onHashtagClick={onHashtagClick}
      readOnly
    />
  ), [images]);

  const renderLeaf = useCallback((props: any) => <Leaf {...props} />, []);

  const renderVideoPlayers = () => links!.map((link) => {
    if (link.includes('loom.com/share')) {
      return (
        <iframe
          src={link.replace('share', 'embed')}
          frameBorder="0"
          key={link}
          allowFullScreen
          style={{ width: '100%', height: '200px' }}
        />
      );
    }
    return null;
  });

  return (
    <EditorWrap readOnly>
      <Slate
        editor={editor}
        value={value}
        onChange={(val) => setValue(val)}
      >
        <Editable
          renderElement={renderElement}
          renderLeaf={renderLeaf}
          readOnly
        />
        {links && <PlayerVideosWrapper>{renderVideoPlayers()}</PlayerVideosWrapper>}
      </Slate>
    </EditorWrap>
  );
};

export { SlateReader };

export default SlateReader;
