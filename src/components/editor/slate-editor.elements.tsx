import {
  useFocused, useSelected,
} from 'slate-react';
import React, { Fragment, ReactNode } from 'react';
import imageExtensions from 'image-extensions';
import isUrl from 'is-url';
import { useTheme } from '@storybook/theming';
import {
  ImageButton, Mention, PdfLink, PdfWrap, Hashtag,
} from './slate-editor.styled';
import { pdfIcon } from './pdfIcon';
import { defaultTheme } from '../../styles/themes';

// Put this at the start and end of an inline component to work around this Chromium bug:
// https://bugs.chromium.org/p/chromium/issues/detail?id=1249405
const InlineChromiumBugfix = () => {
  const space = String.fromCodePoint(160); // Non-breaking space
  return (
    <span
      contentEditable={false}
      style={{ fontSize: 0 }}
    >
      $
      {space}
    </span>
  );
};

type ElementProps = {
  attributes: any,
  element: any,
  children: ReactNode,
  onImageClick?: () => void,
  onPdfClick?: (url: string) => void,
  onMentionClick?: (userId: string) => void,
  onHashtagClick?: (hashtagValue: string) => void,
  readOnly?: boolean,
}

const ImageElement = ({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  attributes, children, element, onImageClick, onPdfClick, readOnly,
}: ElementProps) => {
  const theme = useTheme();
  const selected = useSelected();
  const focused = useFocused();

  const extension = element.url?.split(/[#?]/)[0].split('.').pop().trim() || '';
  const extensionName = element.name?.split(/[#?]/)[0].split('.').pop().trim() || '';
  // eslint-disable-next-line @typescript-eslint/no-empty-function

  if (extension && extensionName
    && !imageExtensions.includes(extension.toLowerCase()) && !imageExtensions.includes(extensionName.toLowerCase())) {
    if (isUrl(element.url) && readOnly) {
      return (
        // eslint-disable-next-line react/jsx-no-target-blank
        <PdfLink
          {...attributes}
          contentEditable={false}
          href={element.url || ''}
          target="_blank"
        >
          {children}
          <span contentEditable={false}>
            <img src={pdfIcon} alt={element.alt} />
            {element.name}
          </span>
        </PdfLink>
      );
    }

    return (
      <PdfWrap
        {...attributes}
        contentEditable={false}
        style={{
          boxShadow: selected && focused && !readOnly
            ? `0 0 0 2px ${theme?.info?.primaryContainer || defaultTheme.info.primaryContainer}` : 'none',
        }}
      >
        {children}
        <span contentEditable={false}>
          <img src={pdfIcon} alt={element.alt} />
          {element.name}
        </span>
      </PdfWrap>
    );
  }

  return (
    <ImageButton
      {...attributes}
      type="button"
      onClick={onImageClick}
    >
      {children}
      <div contentEditable={false}>
        <img
          src={element.url}
          alt={element.alt}
          style={{
            borderRadius: '4px',
            boxShadow: selected && focused && !readOnly
              ? `0 0 0 2px ${theme?.info?.primaryContainer || defaultTheme.info.primaryContainer}` : 'none',
          }}
        />
      </div>
    </ImageButton>
  );
};

const LinkElement = ({
  attributes, children, element, readOnly,
}: ElementProps) => {
  const theme = useTheme();
  const selected = useSelected();

  return (
    <a
      {...attributes}
      href={element.url}
      style={selected && !readOnly
        ? { boxShadow: `0 0 0 2px ${theme?.info?.primary || defaultTheme.info.primary}`, borderRadius: '2px' } : {}}
    >
      {!readOnly && (<InlineChromiumBugfix />)}
      {children}
      {!readOnly && (<InlineChromiumBugfix />)}
    </a>
  );
};

const MentionElement = ({
  attributes, element, children, readOnly, onMentionClick,
}: ElementProps) => {
  const theme = useTheme();
  const selected = useSelected();
  const focused = useFocused();

  return (
    <Mention
      {...attributes}
      contentEditable={false}
      // eslint-disable-next-line no-underscore-dangle
      data-cy={`mention-${element.character._id}`}
      style={{
        boxShadow: selected && focused && !readOnly
          ? `0 0 0 2px ${theme?.info?.primaryContainer || defaultTheme.info.primaryContainer}` : 'none',
      }}
      onClick={() => {
        if (onMentionClick && element.character._id !== 'channel') {
          onMentionClick(element.character._id);
        }
      }}
      clickable={!!onMentionClick && element.character._id !== 'channel'}
    >
      {element.character.profile.firstName && '@'}
      {element.character.profile.firstName}
      {' '}
      {element.character.profile.lastName}
      <span style={element.character.profile.firstName && { display: 'none' }}>
        {children}
      </span>
    </Mention>
  );
};

const HashtagElement = ({
  attributes, element, children, readOnly, onHashtagClick,
}: ElementProps) => {
  const theme = useTheme();
  const selected = useSelected();
  const focused = useFocused();
  return (
    <Hashtag
      {...attributes}
      contentEditable={false}
      data-cy={`hashtag-${element.character._id}`}
      style={{
        boxShadow: selected && focused && !readOnly
          ? `0 0 0 2px ${theme?.info?.primaryContainer || defaultTheme.info.primaryContainer}` : 'none',
      }}
      onClick={() => {
        if (onHashtagClick) {
          onHashtagClick(element.character.value);
        }
      }}
      clickable={!!onHashtagClick}
    >
      {element.character.value && '#'}
      {element.character.value}
      <span style={element.character.value ? { display: 'none' } : {}}>
        {children}
      </span>
    </Hashtag>
  );
};

const Element = (props: ElementProps) => {
  const { attributes, children, element } = props;

  switch (element.type) {
    case 'mention':
      return (
        <MentionElement {...props} />
      );
    case 'hashtag':
      return (
        <HashtagElement {...props} />
      );
    case 'link':
      return (
        <LinkElement {...props} />
      );
    case 'image':
      return (
        <ImageElement {...props} />
      );
    case 'bulleted-list':
      return <ul {...attributes}>{children}</ul>;
    case 'list-item':
      return <li {...attributes}>{children}</li>;
    case 'numbered-list':
      return <ol {...attributes}>{children}</ol>;
    case 'header1':
      return <h1 {...attributes}>{children}</h1>;
    case 'header2':
      return <h2 {...attributes}>{children}</h2>;
    case 'header3':
      return <h3 {...attributes}>{children}</h3>;
    case 'header4':
      return <h4 {...attributes}>{children}</h4>;
    case 'header5':
      return <h5 {...attributes}>{children}</h5>;
    case 'header6':
      return <h6 {...attributes}>{children}</h6>;
    case 'code-block':
      return <pre {...attributes}>{children}</pre>;
    case 'code-line':
      return (
        <Fragment>
          {children}
          {element.children[0].text && '\n'}
        </Fragment>
      );
    default:
      return (
        <p {...attributes}>{children}</p>
      );
  }
};

type LeafElement = {
  leaf: any,
  attributes: any,
  children: ReactNode,
}

const Leaf = ({ leaf, attributes, children }: LeafElement) => {
  if (leaf.bold) {
    // eslint-disable-next-line no-param-reassign
    children = <strong {...attributes}>{children}</strong>;
  }

  if (leaf.italic) {
    // eslint-disable-next-line no-param-reassign
    children = <em {...attributes}>{children}</em>;
  }

  if (leaf.underline) {
    // eslint-disable-next-line no-param-reassign
    children = <ins {...attributes}>{children}</ins>;
  }

  if (leaf.strikethrough) {
    // eslint-disable-next-line no-param-reassign
    children = <del {...attributes}>{children}</del>;
  }

  if (leaf.code) {
    // eslint-disable-next-line no-param-reassign
    children = <code {...attributes}>{children}</code>;
  }

  return <span {...attributes}>{children}</span>;
};

export {
  Element,
  Leaf,
};
