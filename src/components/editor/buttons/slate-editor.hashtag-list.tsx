/* eslint-disable no-underscore-dangle */
import React, {
  ReactNode, useEffect, useRef, useState,
} from 'react';
import { ReactEditor } from 'slate-react';
import ReactDOM from 'react-dom';
import { getUserName } from '~/helpers';
import { MentionItem, MentionListContent, MentionListWrap } from '../slate-editor.styled';

export const Portal = ({ children }: { children: ReactNode }) => (typeof document === 'object'
  ? ReactDOM.createPortal(children, document.body) : null);

const SlateHashtagList = ({
  editor, target, index, search, hashtagOptions, onClick, onHover, position,
}: {
  editor: any,
  target: any,
  index: any,
  search: any,
  hashtagOptions: any,
  onClick: any,
  onHover: any,
  position: any,
}) => {
  const ref = useRef<HTMLDivElement | null>();
  const [isHovered, setIsHovered] = useState(false);

  const onMouseEnter = (charIndex: number) => () => {
    onHover(charIndex);
    setIsHovered(true);
  };

  const onMouseLeave = () => setIsHovered(false);

  useEffect(() => {
    if (target && hashtagOptions.length > 0) {
      const el = ref.current;
      const domRange = ReactEditor.toDOMRange(editor, target);
      const rect = domRange.getBoundingClientRect();
      if (el) {
        el.style.top = `${rect.top + window.pageYOffset + 24}px`;
        el.style.left = `${rect.left + window.pageXOffset}px`;
      }
    }
  }, [hashtagOptions.length, editor, index, search, target]);

  useEffect(() => {
    if (!isHovered && hashtagOptions[index]?._id) {
      const element = document.getElementById(hashtagOptions[index]._id);
      if (element) {
        element.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      }
    }
  }, [index]);

  if (!(target && hashtagOptions.length > 0)) return null;

  return (
    <Portal>
      <MentionListWrap
        // @ts-ignore
        ref={ref}
        data-cy="mentions-portal"
        position={position}
      >
        <MentionListContent>
          {/* @ts-ignore */}
          {chars.map((char, charIndex) => (
            <MentionItem
              key={char._id}
              id={char._id}
              type="button"
              onMouseEnter={onMouseEnter(charIndex)}
              onMouseLeave={onMouseLeave}
              onClick={onClick(charIndex)}
              active={charIndex === index}
            >
              <span>
                {getUserName(char)}
              </span>
            </MentionItem>
          ))}
        </MentionListContent>
      </MentionListWrap>
    </Portal>
  );
};

export default SlateHashtagList;
