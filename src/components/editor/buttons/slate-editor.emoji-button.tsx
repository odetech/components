import React, { useState } from 'react';
import { Picker } from 'emoji-mart';
import { EmoticonOutlineSvg } from '../icons';
import { EditorButton, EmojisPickerWrap } from '../slate-editor.styled';
import { Dropdown } from './dropdown.component';

import 'emoji-mart/css/emoji-mart.css';

type EmojiButtonProps = {
  onSelect: (value: any) => void,
  position?: 'top' | 'bottom',
  actionButtonClassName?: string,
};

export const EmojiButton = ({ onSelect, position, actionButtonClassName }: EmojiButtonProps) => {
  const [isOpen, setIsOpen] = useState(false);

  const onHandleSelect = (value: any) => {
    onSelect(value.native);
  };

  const toggle = () => setIsOpen((state) => !state);

  return (
    <Dropdown toggle={toggle} isOpen={isOpen} isStatic>
      <EditorButton
        active
        type="button"
        onClick={toggle}
        actionButtonClassName={actionButtonClassName}
      >
        <EmoticonOutlineSvg />
      </EditorButton>
      {isOpen && (
        <EmojisPickerWrap position={position}>
          <Picker
            onSelect={onHandleSelect}
            native
            showPreview={false}
            sheetSize={16}
            emoji=""
            title=""
          />
        </EmojisPickerWrap>
      )}
    </Dropdown>
  );
};
