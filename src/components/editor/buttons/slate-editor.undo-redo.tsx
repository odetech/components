import React, { Fragment } from 'react';
import { EditorButton } from '../slate-editor.styled';

export const UndoRedo = ({ editor }: { editor: any }) => (
  <Fragment>
    <EditorButton
      type="button"
      color="blue"
      onClick={() => editor.undo(editor)}
      disabled={!editor.history.undos.length}
      active
    >
      {/* <UndoVariantIcon /> */}
    </EditorButton>
    <EditorButton
      type="button"
      color="blue"
      onClick={() => editor.redo(editor)}
      disabled={!editor.history.redos.length}
      active
    >
      {/* <RedoVariantIcon /> */}
    </EditorButton>
  </Fragment>
);
