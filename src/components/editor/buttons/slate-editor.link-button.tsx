import React, { useState } from 'react';
import { useSlate } from 'slate-react';
import { Button, Input } from '../../default-components';
import { LinkSvg } from '../icons';
import { EditorButton, LinkDropdownMenu } from '../slate-editor.styled';
import { CustomEditor } from './slate-editor.custom-editor';
import { Dropdown } from './dropdown.component';

type LinkButtonProps = {
  position?: 'top' | 'bottom',
  actionButtonClassName?: string,
};

export const LinkButton = ({ position, actionButtonClassName }: LinkButtonProps) => {
  const editor = useSlate();
  const [isOpen, setIsOpen] = useState(false);
  const [url, setUrl] = useState('');

  const toggle = () => {
    if (CustomEditor.isLinkActive(editor)) {
      CustomEditor.unwrapLink(editor);
    } else {
      setIsOpen((prevState) => !prevState);
    }
  };

  const onApply = () => {
    CustomEditor.insertLink(editor, url);
    setIsOpen(false);
    setUrl('');
  };

  return (
    <Dropdown toggle={toggle} isOpen={isOpen}>
      <EditorButton
        type="button"
        color="blue"
        active={CustomEditor.isLinkActive(editor)}
        onClick={toggle}
        actionButtonClassName={actionButtonClassName}
      >
        <LinkSvg />
      </EditorButton>
      {isOpen && (
        <LinkDropdownMenu position={position}>
          <p>Enter the URL of the link:</p>
          <Input name="editor-add-link-input" value={url} onChange={(e) => setUrl(e.target.value)} />
          <Button type="button" onClick={onApply}>Add link</Button>
        </LinkDropdownMenu>
      )}
    </Dropdown>
  );
};
