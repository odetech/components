import isHotkey from 'is-hotkey';
import {
  Editor, Element as SlateElement, Transforms, Range, BaseEditor,
} from 'slate';

export type EditorProps = BaseEditor;

const LIST_TYPES = ['numbered-list', 'bulleted-list'];

const HOTKEYS = {
  'mod+b': 'bold',
  'mod+i': 'italic',
  'mod+u': 'underline',
};

// @ts-ignore
export const CustomEditor = {
  isMarkActive(editor: EditorProps, markName: string) {
    const marks = Editor.marks(editor);
    // @ts-ignore
    return marks ? marks[markName] === true : false;
  },

  isBlockActive(editor: EditorProps, type: string) {
    const { selection } = editor;
    if (!selection) return false;

    // @ts-ignore
    const [match] = Editor.nodes(editor, {
      at: Editor.unhangRange(editor, selection),
      // @ts-ignore
      match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === type,
    });

    return !!match;
  },

  isLinkActive(editor: EditorProps) {
    // @ts-ignore
    const [link] = Editor.nodes(editor, {
      // @ts-ignore
      match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === 'link',
    });
    return !!link;
  },

  toggleMark(editor: EditorProps, format: string) {
    const isActive = CustomEditor.isMarkActive(editor, format);

    if (isActive) {
      Editor.removeMark(editor, format);
    } else {
      Editor.addMark(editor, format, true);
    }
  },

  toggleBlock(editor: EditorProps, type: string) {
    const isActive = CustomEditor.isBlockActive(editor, type);
    const isList = LIST_TYPES.includes(type);
    const isCode = type === 'code-block';

    Transforms.unwrapNodes(editor, {
      // @ts-ignore
      // eslint-disable-next-line max-len
      match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && (LIST_TYPES.includes(n.type) || n.type === 'code-block'),
      split: true,
    });
    let formattedType = 'paragraph';

    if (!isActive) {
      formattedType = type;

      if (isList) {
        formattedType = 'list-item';
      } else if (isCode) {
        formattedType = 'code-line';
      }
    }

    Transforms.setNodes(
      editor,
      // @ts-ignore
      { type: isActive ? null : formattedType },
    );

    if (!isActive && isList) {
      const block = { type, children: [] };
      Transforms.wrapNodes(editor, block);
    }

    if (!isActive && isCode) {
      const block = { type, children: [] };
      Transforms.wrapNodes(editor, block);
    }
  },

  insertFile(editor: any, url: string, name: string, uiId: string) {
    const text = { text: '' };
    const file = {
      type: 'image', url, name, children: [text], uiId,
    };
    Transforms.insertNodes(editor, [file]);
  },

  // @ts-ignore
  insertLink(editor, url) {
    if (editor.savedSelection) {
      Transforms.select(editor, editor.savedSelection);
      CustomEditor.wrapLink(editor, url);
    }
  },

  unwrapLink(editor: EditorProps) {
    Transforms.unwrapNodes(editor, {
      // @ts-ignore
      match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === 'link',
    });
  },

  wrapLink(editor: EditorProps, url: string) {
    if (CustomEditor.isLinkActive(editor)) {
      CustomEditor.unwrapLink(editor);
    }

    const { selection } = editor;

    const isCollapsed = selection && Range.isCollapsed(selection);
    const link = {
      type: 'link',
      url,
      children: isCollapsed ? [{ text: url }] : [],
    };

    if (isCollapsed) {
      Transforms.insertNodes(editor, link);
      Transforms.move(editor, { unit: 'offset' });
      Transforms.insertText(editor, ' ');
    } else {
      Transforms.wrapNodes(editor, link, { split: true });
      Transforms.collapse(editor, { edge: 'end' });
    }
  },

  insertMention(editor: EditorProps, character: any) {
    const mention = {
      type: 'mention',
      character,
      children: [{ text: '' }],
    };
    Transforms.insertNodes(editor, mention);
    Transforms.move(editor, { unit: 'offset' });
    Transforms.insertText(editor, ' ');
  },

  // @ts-ignore
  onHotKey(editor, e) {
    const { selection } = editor;

    // Default left/right behavior is unit:'character'.
    // This fails to distinguish between two cursor positions, such as
    // <inline>foo<cursor/></inline> vs <inline>foo</inline><cursor/>.
    // Here we modify the behavior to unit:'offset'.
    // This lets the user step into and out of the inline without stepping over characters.
    // You may wish to customize this further to only use unit:'offset' in specific cases.
    if (selection && Range.isCollapsed(selection)) {
      const { nativeEvent } = e;
      if (isHotkey('left', nativeEvent)) {
        e.preventDefault();
        Transforms.move(editor, { unit: 'offset', reverse: true });
        return;
      }
      if (isHotkey('right', nativeEvent)) {
        e.preventDefault();
        Transforms.move(editor, { unit: 'offset' });
        return;
      }
    }

    Object.keys(HOTKEYS).forEach((hotkey) => {
      if (isHotkey(hotkey, e)) {
        e.preventDefault();
        // @ts-ignore
        const mark = HOTKEYS[hotkey];
        CustomEditor.toggleMark(editor, mark);
      }
    });

    if (e.keyCode === 8 && CustomEditor.isBlockActive(editor, 'image')) {
      e.preventDefault();
      Transforms.removeNodes(editor);
    }

    if (e.keyCode === 13 && CustomEditor.isBlockActive(editor, 'image')) {
      e.preventDefault();
      // @ts-ignore
      Transforms.insertNodes(editor, { type: 'paragraph', children: [{ text: '' }] });
    }
  },
};
