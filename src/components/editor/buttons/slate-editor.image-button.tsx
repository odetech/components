import React, { useRef, useState } from 'react';
import { useSlate } from 'slate-react';
// @ts-ignore
import imageExtensions from 'image-extensions';
import { CustomEditor } from './slate-editor.custom-editor';
import { EditorButton } from '../slate-editor.styled';
import { PaperClipSvg } from '../icons';
import { pdfIcon } from '../pdfIcon';
import { generateUUID, getBase64 } from '../slate-editor.image-helpers';

type ImageButtonProps = {
  uploadImageCallBack: any,
  actionButtonClassName?: string,
};

export const ImageButton = ({ uploadImageCallBack, actionButtonClassName }: ImageButtonProps) => {
  const editor = useSlate();
  const ref = useRef<HTMLDivElement | null>(null);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [savedEditor, setSavedEditor] = useState(editor);

  // @ts-ignore
  const onImageChange = async (e) => {
    const { length } = e.target.files;
    const { files } = e.target;
    // IE11 call change event twice
    if (length) {
      await Promise.all(Object.entries(files).map(async ([, file]) => {
        const uiId = generateUUID();
        // @ts-ignore
        const { name } = file;
        const extensionName = name.split(/[#?]/)[0].split('.').pop().trim();
        if (!imageExtensions.includes(extensionName.toLowerCase())) {
          // @ts-ignore
          CustomEditor.insertFile(savedEditor, pdfIcon, name, uiId);
          uploadImageCallBack(file, uiId);
        } else {
          // @ts-ignore
          const result = await getBase64(file);
          if (result) {
            // @ts-ignore
            CustomEditor.insertFile(savedEditor, result, name, uiId);
          }
          // @ts-ignore
          file.src = result;
          uploadImageCallBack(file, uiId);
        }
      }));
    }
  };

  // @ts-ignore
  const clickToFileInput = () => ref.current.click();

  const handleClick = (e: Event) => {
    e.preventDefault();
    setSavedEditor(editor);
    clickToFileInput();
  };

  return (
    <div style={{ position: 'relative' }}>
      <EditorButton active type="button" onClick={handleClick} actionButtonClassName={actionButtonClassName}>
        <PaperClipSvg />
      </EditorButton>
      <input
        type="file"
        onChange={onImageChange}
        value=""
        // @ts-ignore
        ref={ref}
        hidden
        multiple
      />
    </div>
  );
};
