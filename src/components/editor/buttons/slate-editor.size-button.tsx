import React, { useState } from 'react';
import { useSlate } from 'slate-react';
import { FormatSizeSvg } from '../icons';
import { EditorButton, LinkDropdownMenu, MenuButton } from '../slate-editor.styled';
import { CustomEditor } from './slate-editor.custom-editor';
import { Dropdown } from './dropdown.component';

type SizeButtonProps = {
  position?: 'top' | 'bottom',
  actionButtonClassName?: string,
};

export const SizeButton = ({ position, actionButtonClassName }: SizeButtonProps) => {
  const editor = useSlate();
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    if (CustomEditor.isLinkActive(editor)) {
      CustomEditor.unwrapLink(editor);
    } else {
      setIsOpen((prevState) => !prevState);
    }
  };

  const onApply = (header: string) => () => {
    CustomEditor.toggleBlock(editor, header);
    setIsOpen(false);
  };

  const isActive = CustomEditor.isBlockActive(editor, 'header1')
    || CustomEditor.isBlockActive(editor, 'header2')
    || CustomEditor.isBlockActive(editor, 'header3')
    || CustomEditor.isBlockActive(editor, 'header4')
    || CustomEditor.isBlockActive(editor, 'header5')
    || CustomEditor.isBlockActive(editor, 'header6');

  return (
    <Dropdown toggle={toggle} isOpen={isOpen}>
      <EditorButton
        type="button"
        active={isActive}
        onClick={toggle}
        actionButtonClassName={actionButtonClassName}
      >
        <FormatSizeSvg />
      </EditorButton>
      {isOpen && (
        <LinkDropdownMenu position={position}>
          <MenuButton
            type="button"
            onClick={onApply('header1')}
            active={CustomEditor.isBlockActive(editor, 'header1')}
          >
            Heading 1
          </MenuButton>
          <MenuButton
            type="button"
            onClick={onApply('header2')}
            active={CustomEditor.isBlockActive(editor, 'header2')}
          >
            Heading 2
          </MenuButton>
          <MenuButton
            type="button"
            onClick={onApply('header3')}
            active={CustomEditor.isBlockActive(editor, 'header3')}
          >
            Heading 3
          </MenuButton>
          <MenuButton
            type="button"
            onClick={onApply('header4')}
            active={CustomEditor.isBlockActive(editor, 'header4')}
          >
            Heading 4
          </MenuButton>
          <MenuButton
            type="button"
            onClick={onApply('header5')}
            active={CustomEditor.isBlockActive(editor, 'header5')}
          >
            Heading 5
          </MenuButton>
          <MenuButton
            type="button"
            onClick={onApply('header6')}
            active={CustomEditor.isBlockActive(editor, 'header6')}
          >
            Heading 6
          </MenuButton>
        </LinkDropdownMenu>
      )}
    </Dropdown>
  );
};
