import React, { ReactNode, useEffect, useState } from 'react';
import { useSlate } from 'slate-react';
import { EditorButton } from '../slate-editor.styled';
import { CustomEditor } from './slate-editor.custom-editor';

type BlockButtonProps = {
  format: string;
  icon: ReactNode;
  actionButtonClassName?: string;
};

const BlockButton = ({ format, icon, actionButtonClassName }: BlockButtonProps) => {
  const editor = useSlate();
  const onToggleBlockClick = (e: any) => {
    e.preventDefault();
    CustomEditor.toggleBlock(editor, format);
  };

  return (
    <EditorButton
      color="blue"
      type="button"
      onMouseDown={onToggleBlockClick}
      active={CustomEditor.isBlockActive(editor, format)}
      actionButtonClassName={actionButtonClassName}
    >
      {icon}
    </EditorButton>
  );
};

type MarkButtonProps = {
  format: string;
  icon: ReactNode;
  actionButtonClassName?: string;
};

const MarkButton = ({
  format, icon, actionButtonClassName,
}: MarkButtonProps) => {
  const [isActive, setIsActive] = useState(false);
  const editor = useSlate();

  const onToggleMarkClick = (e: any) => {
    e.preventDefault();
    CustomEditor.toggleMark(editor, format);
    setIsActive((state) => !state);
  };

  const isMarkActive = CustomEditor.isMarkActive(editor, format);

  useEffect(() => {
    setIsActive(isMarkActive);
  }, [isMarkActive]);

  return (
    <EditorButton
      type="button"
      onMouseDown={onToggleMarkClick}
      active={isActive}
      actionButtonClassName={actionButtonClassName}
    >
      {icon}
    </EditorButton>
  );
};

export {
  MarkButton,
  BlockButton,
};
