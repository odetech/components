import React, { ReactNode, useEffect, useRef } from 'react';

type DropdownProps = {
  toggle: () => void;
  isOpen: boolean;
  children: ReactNode;
  isStatic?: boolean;
}

export const Dropdown = ({
  children, toggle, isOpen, isStatic,
}: DropdownProps) => {
  const ref = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event: Event) => {
      // @ts-ignore
      if (ref.current && !ref.current.contains(event.target) && isOpen) {
        toggle();
      }
    };

    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref, isOpen]);

  return (
    <div style={{ position: isStatic ? 'static' : 'relative' }} ref={ref}>
      {children}
    </div>
  );
};
