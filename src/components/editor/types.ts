export type ArticleFileProps = {
  _id: string,
  associatedId: string,
  createdBy: string,
  createdAt:string,
  cloudinary: {
    // eslint-disable-next-line camelcase
    public_id: string,
    format:string,
    bytes: string,
    url: string,
    // eslint-disable-next-line camelcase
    secure_url: string
  },
  uiId: string
};
