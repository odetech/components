import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import SlateEditor from '../slate-editor.component';

const meta: Meta<typeof SlateEditor> = {
  title: 'Components/Editor/DefaultEditor',
  component: SlateEditor,
  argTypes: {},
  args: {
    disabled: false,
  },
};
export default meta;

export const Default: StoryFn<typeof SlateEditor> = (args) => (
  <SlateEditor
    {...args}
    setFieldValue={() => ({})}
    value=""
    valueFiles={[]}
    editValue=""
    closeEdit={() => ({})}
    field="message"
    handleSubmit={() => ({})}
    userSuggestions={[]}
    isMinimal={false}
    isChannelMention
    uploadImageCallBack={(file: File, uiId: string) => {
      console.log('file', file);
      console.log('uiId', uiId);
    }}
    setImages={() => ({})}
    setIsOpenLightbox={() => ({})}
    setPhotoIndex={() => ({})}
    files={[]}
    submitOnEnter
    withAutoFocus
    placeholder="What’s on your mind?"
  />
);
