/* eslint-disable max-len */
import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import SlateReader from '../slate-reader.component';

const meta: Meta<typeof SlateReader> = {
  title: 'Components/Editor/SlateReader',
  component: SlateReader,
  decorators: [
    (story) => (
      <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
        {story()}
      </MemoryRouter>
    ),
  ],
};
export default meta;

export const Default: StoryFn<typeof SlateReader> = (args) => (
  <SlateReader
    {...args}
    value="<p>Test message <a href='https://www.loom.com/share/3799f2c562694f2ba6c7bc103fb4dac6'>https://www.loom.com/share/3799f2c562694f2ba6c7bc103fb4dac6</a></p>"
    setImages={() => ({})}
    setIsOpenLightbox={() => ({})}
    setPhotoIndex={() => ({})}
    setShowModal={() => ({})}
    setPdfFile={() => ({})}
    files={[]}
    links={['https://www.loom.com/share/3799f2c562694f2ba6c7bc103fb4dac6']}
  />
);
