import { MouseEvent, ButtonHTMLAttributes } from 'react';
import { Theme } from '@storybook/theming';
import { defaultTheme } from '../../../styles/themes';
import { Button } from './ButtonStyles';

export const chooseColor = (color: string, theme?: Theme) => {
  let buttonColor;

  switch (color) {
    case 'orange': {
      buttonColor = theme?.brand?.primary || defaultTheme.brand.primary;
      break;
    }
    case 'green': {
      buttonColor = theme?.success?.primary || defaultTheme.success.primary;
      break;
    }
    case 'red': {
      buttonColor = theme?.error?.primary || defaultTheme.error.primary;
      break;
    }
    case 'pink': {
      buttonColor = theme?.brand?.secondary || defaultTheme.brand.secondary;
      break;
    }
    case 'yellow': {
      buttonColor = theme?.visualisation?.yellow || defaultTheme.visualisation.yellow;
      break;
    }
    case 'blue': {
      buttonColor = theme?.info?.primary || defaultTheme.info.primary;
      break;
    }
    case 'purple': {
      buttonColor = theme?.visualisation?.purple || defaultTheme.visualisation.purple;
      break;
    }
    case 'black': {
      buttonColor = theme?.visualisation?.black || defaultTheme.visualisation.black;
      break;
    }
    case 'grey': {
      buttonColor = theme?.text?.onSurface || defaultTheme.text.onSurface;
      break;
    }

    default: {
      buttonColor = theme?.brand?.primary || defaultTheme.brand.primary;
    }
  }

  return buttonColor;
};

export type ButtonProps = {
  onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
  type?: 'button' | 'reset' | 'submit';
  size?: 'lg' | 'sm' | 'md';
  view?: 'default' | 'text';
  color?: string | 'orange' | 'green' | 'red' | 'pink' | 'yellow' | 'blue' | 'purple' | 'black';
} & ButtonHTMLAttributes<HTMLButtonElement>;

export default Button;
