/* eslint-disable no-nested-ternary */
import React from 'react';
import styled from '@emotion/styled';
import { transparentize, darken } from 'polished';
import isPropValid from '@emotion/is-prop-valid';
import { defaultTheme } from '../../../styles/themes';
import { chooseColor } from './index';

type StyledButtonProps = {
  disabled?: boolean;
  size?: 'lg' | 'sm' | 'md';
  view?: 'default' | 'text';
  color?: string | 'orange' | 'green' | 'red' | 'pink' | 'yellow' | 'blue' | 'purple' | 'black';
};

export const Button = styled(
  (props: any) => (
    <button
      type="button"
      {...props}
      className={`${props.className} odecloud-button`}
    />
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<StyledButtonProps>`
  font-size: 16px;
  transition: 0.3s;
  cursor: pointer;
  outline: none;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ view, color }) => (view === 'text' ? 'transparent' : chooseColor(color))} none;
  border: none;
  border-radius: 7px;
  color: ${({ view, color, theme }) => (view === 'text'
    ? chooseColor(color, theme)
    : (color === 'grey'
      ? (theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar)
      : (theme?.background?.surface || defaultTheme.background.surface)))};
  font-weight: 500;

  // Size of the Button (small(sm), large(lg), medium(default/md))
  ${({ size }) => {
    switch (size) {
      case 'sm':
        return {
          padding: '4px 20px',
        };
      case 'lg':
        return {
          padding: '12px 32px',
        };
      default:
        return {
          padding: '8px 24px',
        };
    }
  }};

  svg {
    fill: ${({ view, color, theme }) => (view === 'text'
    ? chooseColor(color) : theme?.visualisation?.white || defaultTheme.visualisation.white)};
  }

  &:focus {
    outline: none;
    background: ${({ view, color, theme }) => (view === 'text'
    ? (theme?.brand?.primaryContainerVar || defaultTheme.brand.primaryContainerVar)
    : darken(0.15, (chooseColor(color))))};
  }

  &:hover,
  &:active {
    background: ${({ view, color, theme }) => (view === 'text'
    ? theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer
    : transparentize(0.25, chooseColor(color)))};
  }

  &:disabled {
    cursor: not-allowed;
    background: ${({ theme }) => theme?.background?.surfaceContainerVar
      || defaultTheme.background.surfaceContainerVar}; 
    color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled};

    svg {
      fill: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled};    
    }
  }
`;
