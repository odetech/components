import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import Button from '../index';

const meta: Meta<typeof Button> = {
  title: 'Components/Default Components/Button',
  component: Button,
  argTypes: {
    text: { name: 'label', control: 'text' },
    color: {
      control: 'select',
      options: [
        'orange',
        'green',
        'red',
        'pink',
        'yellow',
        'blue',
        'purple',
        'black',
      ],
    },
  },
  args: {
    text: 'Button text',
    onClick: () => {
      console.log('Button click');
    },
  },
};

export default meta;

export const Default: StoryFn<typeof Button> = (args) => {
  const { text } = args;

  return (
    <Button
      {...args}
    >
      {text}
    </Button>
  );
};

export const TextButton = Default.bind({});
TextButton.args = {
  view: 'text',
  color: 'red',
};
