import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import NavLink from '../index';

const meta: Meta<typeof NavLink> = {
  title: 'Components/Default Components/NavLink',
  component: NavLink,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.width = '100%';
      }
      return (
        <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
          {story()}
        </MemoryRouter>
      );
    },
  ],
  argTypes: {
    to: { control: 'text' },
  },
  args: {
    disabled: false,
    to: 'profile/123qweasdzxc',
  },
};
export default meta;

export const Default: StoryFn<typeof NavLink> = (args) => (
  <NavLink
    {...args}
  >
    Storybook NavLink
  </NavLink>
);
