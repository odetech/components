import { MouseEvent, ReactNode } from 'react';
import { StyledNavLink as NavLink } from './NavLinkStyles';

export type LinkProps = {
  to: string;
  children: ReactNode;
  disabled?: boolean;
  onClick?: (e: MouseEvent<HTMLAnchorElement>) => void;
};

export default NavLink;
