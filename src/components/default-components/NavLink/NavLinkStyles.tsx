import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from '@emotion/styled';
import { transparentize } from 'polished';
import { defaultTheme } from '../../../styles/themes';

type NavLinkProps = {
  disabled?: boolean;
};

export const StyledNavLink = styled(
  (props: any) => (
    <NavLink
      {...props}
      className={`${props.className} odecloud-link`}
    />
  ),
)<NavLinkProps>`
  color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  text-decoration: none;
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;

  &:hover {
    color: ${({ theme }) => transparentize(0.2, theme?.brand?.primary || defaultTheme.brand.primary)};
  }

  ${({ disabled }) => (disabled && `
    opacity: 0.5;
    pointer-events: none;
  `)}
`;
