import styled from '@emotion/styled';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { defaultTheme } from '../../../styles/themes';

type StyledToastProps = {
  type: 'default' | 'success' | 'error' | string,
}

export const StyledToast = styled(ToastContainer)<StyledToastProps>`
  &.Toastify__toast-container {
    width: fit-content !important;
    max-width: 792px !important;
  }

  .Toastify__toast {
    font-family: 'Poppins', sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 20px;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    box-shadow: 0px 0.78184px 1.47681px 0px rgba(64, 64, 64, 0.05), 0px 1.59488px 11px 0px rgba(64, 64, 64, 0.04),
     0px 3.28515px 15px 0px rgba(64, 64, 64, 0.06), 0px 9px 31px 0px rgba(64, 64, 64, 0.11);
    padding: 14px 16px;
    
    button{
      svg {
        fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
      }
    }
    
    .Toastify__toast-body {
      margin: 0;
      padding: 0;

      .Toastify__toast-icon {
        width: unset;
        margin: 0 12px 0 0;
        svg {
          width: 24px;
          height: 24px;
        }
      }
    }
  }

  .Toastify__progress-bar--default {
    background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  }

  .Toastify__toast--default {
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }

  .Toastify__toast--info {
    background-color: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};

    .Toastify__toast-icon {
      svg {
        fill: ${({ theme }) => theme?.info?.primary || defaultTheme.info.primary};
      }
    }
  }

  .Toastify__toast--success {
    background-color: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};

    .Toastify__toast-icon {
      svg {      
        fill: ${({ theme }) => theme?.success?.primary || defaultTheme.success.primary};
      }
    }
  }

  .Toastify__toast--warning {
    background-color: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};

    .Toastify__toast-icon {
      svg {
        fill: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
      }
    }
  }

  .Toastify__toast--error {
    background-color: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};

    .Toastify__toast-icon {
      svg {
        fill: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};
      }
    }
  }
`;
