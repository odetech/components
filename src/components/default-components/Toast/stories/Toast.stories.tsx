import React, { Fragment } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { Toast, showToast } from '~/index';
import { Button } from '../../index';

const meta: Meta<typeof Toast> = {
  title: 'Components/Default Components/Toast',
  component: Toast,
  argTypes: {},
  args: {
    disabled: false,
    checked: true,
    position: 'top-center',
    closeOnClick: true,
    pauseOnHover: true,
  },
};
export default meta;

export const Default: StoryFn<typeof Toast> = (args) => (
  <Fragment>
    <Button
      onClick={() => {
        showToast({
          message: 'This is an info message',
          type: 'info',
        });
      }}
      color="blue"
    >
      Info toast
    </Button>
    <div
      style={{
        margin: '10px 0',
      }}
    />
    <Button
      onClick={() => {
        showToast({
          message: 'This is a success message',
          type: 'success',
        });
      }}
      color="green"
    >
      Success toast
    </Button>
    <div
      style={{
        margin: '10px 0',
      }}
    />
    <Button
      onClick={() => {
        showToast({
          message: 'This is a warning message',
          type: 'warning',
        });
      }}
      color="orange"
    >
      Warning toast
    </Button>
    <div
      style={{
        margin: '10px 0',
      }}
    />
    <Button
      onClick={() => {
        showToast({
          message: 'This is an error message',
          type: 'error',
        });
      }}
      color="red"
    >
      Error toast
    </Button>
    <Toast
      icon
      {...args}
    />
  </Fragment>
);
