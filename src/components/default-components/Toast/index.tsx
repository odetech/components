import React from 'react';
import {
  ErrorOutlineIcon, InfoOutlineIcon, SuccessOutlineIcon, WarningOutlineIcon,
} from '~/components/icons';
import { toast } from 'react-toastify';
import { StyledToast } from './ToastStyles';

export const showToast = (options: any) => {
  switch (options.type) {
    case 'info': {
      return toast.info(options.message, {
        icon: <InfoOutlineIcon />,
        ...options,
      });
    }
    case 'success': {
      return toast.success(options.message, {
        icon: <SuccessOutlineIcon />,
        ...options,
      });
    }
    case 'warning': {
      return toast.warning(options.message, {
        icon: <WarningOutlineIcon />,
        ...options,
      });
    }
    case 'error': {
      return toast.error(options.message, {
        icon: <ErrorOutlineIcon />,
        ...options,
      });
    }
    default: {
      return toast.info(options.message, {
        icon: <InfoOutlineIcon />,
        ...options,
      });
    }
  }
};

export type ToastProps = {
  type?: 'info' | 'success' | 'warning' | 'error' | string,
  position?: 'top-right' | 'top-center' | 'top-left' | 'bottom-right' | 'bottom-center' | 'bottom-left',
  autoClose?: number,
  hideProgressBar?: boolean,
  closeOnClick?: boolean,
  newestOnTop?: boolean,
  rtl?: boolean,
  pauseOnFocusLoss?: boolean,
  draggable?: boolean,
  pauseOnHover?: boolean,
  isDev?: boolean,
  disabled?: boolean,
  checked?: boolean,
  icon?: boolean,
  closeButton?: boolean,
};

export const Toast = ({
  // Default props
  position,
  type = 'info',
  autoClose = 3000,
  hideProgressBar = true,
  isDev = false,
  closeButton = false,
  icon = false,
  ...props
}: ToastProps) => (
  <StyledToast
    className="odecloud-toast"
    type={type}
    position={position}
    autoClose={isDev ? 10000 : autoClose}
    hideProgressBar={isDev ? false : hideProgressBar}
    icon={icon}
    closeButton={closeButton}
    {...props}
  />
);

export default Toast;
