import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { CalendarRangeOutlineIcon } from '../../icons';
import { DatePickerWrapper, StyledLabel, DatePickerIconWrapper } from './DatePickerStyles';

export type DatePickerProps = {
  label?: string;
  disabled?: boolean;
  showIcon?: boolean;
  startDate: Date;
  name: string;
  id?: string;
  margin?: string;
  placeholder?: string;
  showPopperArrow?: boolean;
  minDate?: Date;
  minTime?: Date;
  maxTime?: Date;
  maxDate?: Date;
  filterTime?: (date: Date) => boolean;
  onChange: (date: Date) => void;
  dateFormat?: string;
  showMonthYearPicker?: boolean;
  timeFormat?: string | undefined;
  timeCaption?: string | undefined;
  showTimeSelect?: boolean | undefined;
  timeIntervals?: number | undefined;
  width?: number;
};

export const CustomDatePicker = ({
  // Default props
  disabled = false,
  label,
  margin = '0 0 0 0',
  startDate,
  onChange,
  placeholder,
  showIcon,
  dateFormat, timeFormat, timeCaption, showTimeSelect, timeIntervals, width,
  ...props
}: DatePickerProps) => (
  <DatePickerWrapper
    className="odecloud-date-picker"
    margin={margin}
    showIcon={showIcon}
    width={width}
  >
    {label && <StyledLabel>{label}</StyledLabel>}
    {showIcon && (
    <DatePickerIconWrapper>
      <CalendarRangeOutlineIcon />
    </DatePickerIconWrapper>
    )}
    <DatePicker
      selected={startDate}
      onChange={onChange}
      disabled={disabled}
      placeholderText={placeholder}
      showTimeSelect={showTimeSelect}
      timeIntervals={timeIntervals || 15}
      timeFormat={timeFormat || 'HH:mm'}
      timeCaption={timeCaption || 'time'}
      dateFormat={dateFormat || 'MMMM d, yyyy h:mm aa'}
      peekNextMonth
      showMonthDropdown
      showYearDropdown
      dropdownMode="select"
      {...props}
    />
  </DatePickerWrapper>
);

export default CustomDatePicker;
