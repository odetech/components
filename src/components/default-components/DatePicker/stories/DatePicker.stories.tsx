import React, { useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { configureStore } from '@reduxjs/toolkit';
import DatePicker from '../index';

const store = configureStore({
  reducer: {},
});

const meta: Meta<typeof DatePicker> = {
  title: 'Components/Default Components/DatePicker',
  component: DatePicker,
  decorators: [
    (story) => (
      <Provider store={store}>
        <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
          {story()}
        </MemoryRouter>
      </Provider>
    ),
  ],
  argTypes: {},
  args: {
    disabled: false,
    showTimeSelect: true,
  },
};
export default meta;

export const Default: StoryFn<typeof DatePicker> = (args) => {
  const [startDate, setStartDate] = useState<Date>(
    new Date(),
  );

  return (
    <DatePicker
      {...args}
      startDate={startDate}
      onChange={(date: Date) => {
        setStartDate(date);
      }}
      showIcon
    />
  );
};

// 👇 Each story then reuses that template
export const MonthPicker = Default.bind({});
MonthPicker.args = {
  dateFormat: 'MMMM, yyyy',
  showMonthYearPicker: true,
  showTimeSelect: undefined,
  timeFormat: undefined,
  timeCaption: undefined,
};
