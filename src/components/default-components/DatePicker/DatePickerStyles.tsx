import styled from '@emotion/styled';
import { transparentize } from 'polished';
import isPropValid from '@emotion/is-prop-valid';
import { getVerticalScrollbarStyles } from '../../../styles';
import { defaultTheme } from '../../../styles/themes';

export const StyledLabel = styled('p')`
  transition: 0.3s;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};  
`;

export const DatePickerIconWrapper = styled('div')`
  position: relative;
  z-index: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  
  svg {
    position: absolute;
    top: 8px;
    left: 10px;
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
    width: 20px;
    height: 20px;
  }
`;

type InputWrapperProps = {
  margin?: string;
  showIcon?: boolean;
  width?: number;
};

export const DatePickerWrapper = styled('label', {
  shouldForwardProp: isPropValid,
})<InputWrapperProps>`
  width: 100%;
  margin: ${({ margin }) => (margin)};
  background: transparent;
  border-radius: 3px;

  input {
    width: 100%;
    position: relative;
    font-size: 14px;
    font-weight: 400;
    line-height: 20px;
    padding: ${({ showIcon }) => (showIcon ? '8px 16px 8px 38px' : '8px 16px')};
    max-width: ${({ width }) => (width || '475')}px;
    min-width: 200px;
    height: 40px;
    transition: 0.3s;
    text-overflow: ellipsis;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    border: unset;
    border-radius: 7px;
    border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
    outline: unset !important;
    background: ${({ theme }) => theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
  }

  .react-datepicker-wrapper {
    width: 100%;
  }

  &:focus {
    p {
      color: ${({ theme }) => transparentize(0.5, theme?.brand?.primary || defaultTheme.brand.primary)};      
    }
    
    border-color: ${({ theme }) => transparentize(0.5, theme?.brand?.primary || defaultTheme.brand.primary)};    
    
    input, .react-datepicker-ignore-onclickoutside {
      border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
    }

    svg {
      fill: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary} !important;      
    }
  }

  &:active, &:hover {
    p {
      color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    }

    border-color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};

    input {
      background-color: 
        ${({ theme }) => transparentize(0.9, theme?.brand?.primary || defaultTheme.brand.primary)};            
    }

    svg {
      fill: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary} !important;
    }
  }

  &:disabled {
    p {
      color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled};
    }

    input {
      background-color: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};
    }
  }

  // Datepicker styles
  .react-datepicker {
    background: transparent;
    border: 1px solid ${({ theme }) => theme?.outline?.[300] || defaultTheme.outline[300]};
  }
    
  .react-datepicker__header {
    background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};
    color:  ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    margin: 0 6px;
    border-bottom-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  }

  .react-datepicker__current-month, .react-datepicker-time__header {
    font-weight: 500;
    font-size: 14px;
    line-height: 21px;
    color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  }
  
  .react-datepicker__navigation {
    outline: unset !important;
  }

  .react-datepicker__navigation-icon {
    border-width: 2px 2px 0 0;
    width: 8px;
    height: 8px;
  }

  .react-datepicker__time-list {
    height: 100%;
    ${({ theme }) => getVerticalScrollbarStyles(theme)}

    &-item {
      &:hover {
        background: ${({ theme }) => transparentize(0.9, theme?.brand?.primary || defaultTheme.brand.primary)} 
         !important;        
        color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary} !important;
      }

      &--disabled {      
        color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled} !important; 
        cursor: not-allowed !important;         
        opacity: 0.5;  
      }
        
      &--selected {
        background: transparent !important;
        color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary} !important;
      }
    }    
  }

  .react-datepicker__day {
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    font-weight: 500;
    font-size: 14px;

    &:hover {
      background: ${({ theme }) => transparentize(0.9, theme?.brand?.primary || defaultTheme.brand.primary)};      
    }

    &--selected {
      background-color: transparent;
      font-weight: 600;
      font-size: 14px;
      color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary} !important;
      position: relative;

      &::before {
        content: '';
        position: absolute;
        width: 0;
        height: 0;
        border-left: 3px solid transparent;
        border-right: 3px solid transparent;
        border-bottom: 4px solid ${({ theme }) => theme?.outline?.[300] || defaultTheme.outline[300]};
        bottom: 0;
        left: 11px;
      }
    }  
      
    &--keyboard-selected {
      color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
      background: ${({ theme }) => transparentize(0.9, theme?.brand?.primary || defaultTheme.brand.primary)};    
    } 

    &--disabled {      
      color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled}; 
      cursor: not-allowed;         
      opacity: 0.5;  
    }
  }
  
  .react-datepicker__month-select, .react-datepicker__year-select {
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    padding: 2px 4px;
    border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
    border-radius: 7px;
    background: transparent;
    outline: unset !important;
    
    ${({ theme }) => getVerticalScrollbarStyles(theme)}
  }
  
  .react-datepicker__month-container, .react-datepicker__time-container, .react-datepicker__time {
    border-right: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
    background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};
    color:  ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};        
  }

  .react-datepicker__time-container {
    border: unset;
  }

  .react-datepicker__month-text {    
    &-keyboard-selected {
      background: ${({ theme }) => transparentize(0.7, theme?.brand?.primary || defaultTheme.brand.primary)} 
      !important;    
    }
    
    &:hover {
      background: ${({ theme }) => transparentize(0.9, theme?.brand?.primary || defaultTheme.brand.primary)};    
    }
  }  
`;
