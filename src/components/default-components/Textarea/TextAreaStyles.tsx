import React from 'react';
import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';
import { transparentize } from 'polished';
import { getVerticalScrollbarStyles } from '~/styles';
import { defaultTheme } from '../../../styles/themes';

type TextareaProps = {
  view: 'open' | 'closed' | string;
  minHeight?: string;
  maxHeight?: string;
}

export const StyledTextarea = styled('textarea', {
  shouldForwardProp: isPropValid,
})<TextareaProps>`
  resize: none;
  position: relative;
  font-weight: 500;
  font-size: 17px;
  line-height: inherit;
  padding: 8px 16px;
  width: 100%;
  min-width: 200px;
  min-height: ${({ minHeight }) => minHeight || '213px'};
  ${({ maxHeight }) => (maxHeight ? `max-height: ${maxHeight};` : '')}
  transition: 0.3s;
  text-overflow: ellipsis;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  border-radius: 7px;

  background: transparent;
  border: ${({ view, theme }) => (view === 'closed'
    ? `1px solid ${theme?.outline?.[100] || defaultTheme.outline[100]}` : 'none')};
  border-bottom: ${({ view, theme }) => (view === 'open' && `${theme?.outline?.[100] || defaultTheme.outline[100]}`)};

  &::placeholder {
    color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  }

  &:focus {
    box-shadow: none;
    outline: none;
  }

  &:disabled {
    color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled};
    border-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]} !important;    
    background-color: ${({ theme }) => theme?.background?.surfaceContainerVar
     || defaultTheme.background.surfaceContainerVar} !important;
    cursor: not-allowed;
  }

  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus {
    border-color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    -webkit-text-fill-color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    -webkit-box-shadow: 0 0 0 1000px ${({ theme }) => transparentize(
    0.6,
    theme?.brand?.primary || defaultTheme.brand.primary,
  )} inset;
    transition: background-color 5000s ease-in-out 0s;
    font-weight: 500;
    font-size: 17px;
  }

  overflow-x: hidden;
  overflow-y: auto;
  ${({ theme }) => getVerticalScrollbarStyles(theme)}
`;

export const StyledLabel = styled('p')`
  transition: 0.3s;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
`;

type TextareaWrapperProps = {
  margin?: string;
};

export const TextareaWrapper = styled(
  (props: any) => (
    <div
      {...props}
      className={`${props.className} odecloud-textarea-wrapper`}
    />
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<TextareaWrapperProps>`
  width: 100%;
  margin: ${({ margin }) => (margin)};

  &:focus {
    p {
      color: ${({ theme }) => transparentize(0.5, theme?.brand?.primary || defaultTheme.brand.primary)};      
    }
    
    input {
      border-color: ${({ theme }) => transparentize(0.5, theme?.brand?.primary || defaultTheme.brand.primary)};      
    }
  }

  &:active, &:hover {
    p {
      color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    }
    
    input {
      border-color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
      background-color: ${({ theme }) => transparentize(
    0.9,
    theme?.brand?.primary || defaultTheme.brand.primary,
  )};      
    }
  }

  &:disabled {
    p {
      color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled};
    }

    input {
      border-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
      background-color: ${({ theme }) => theme?.background?.surfaceContainerVar
        || defaultTheme.background.surfaceContainerVar};      
    }
  }
`;
