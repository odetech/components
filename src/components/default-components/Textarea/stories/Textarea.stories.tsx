import React, { useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import Textarea from '../index';

const meta: Meta<typeof Textarea> = {
  title: 'Components/Default Components/Textarea',
  component: Textarea,
  argTypes: {
    label: { control: 'text' },
    disabled: { control: 'boolean' },
  },
  args: {
    label: 'Label',
    disabled: false,
    placeholder: 'Placeholder text',
  },
};
export default meta;

export const Default: StoryFn<typeof Textarea> = (args) => {
  const [value, setValue] = useState<string>(
    '',
  );

  return (
    <Textarea
      {...args}
      value={value}
      onChange={(e) => {
        setValue(e.target.value);
      }}
    />
  );
};
