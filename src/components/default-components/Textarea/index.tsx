import React, { ChangeEvent, forwardRef } from 'react';
import { TextareaWrapper, StyledTextarea, StyledLabel } from './TextAreaStyles';

export type TextareaProps = {
  label?: string;
  disabled?: boolean;
  name: string;
  id?: string;
  value?: string;
  placeholder?: string;
  margin?: string;
  view?: 'open' | 'closed' | string;
  minHeight?: string;
  onChange?: (e: ChangeEvent<HTMLTextAreaElement>) => void;
};

export const Textarea = forwardRef<HTMLTextAreaElement, TextareaProps>(({
  // Default props
  disabled = false,
  label,
  view = 'closed',
  margin = '0 0 0 0',
  ...props
}, ref) => (
  <TextareaWrapper
    margin={margin}
  >
    {label && <StyledLabel>{label}</StyledLabel>}
    <StyledTextarea
      className="odecloud-textarea"
      view={view}
      ref={ref}
      disabled={disabled}
      {...props}
    />
  </TextareaWrapper>
));

export default Textarea;
