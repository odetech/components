import React, { ChangeEvent, ReactNode } from 'react';
import {
  RadioButtonWrapper, RadioButtonLabel,
} from './RadioButtonStyles';

export type RadioButtonProps = {
  id: string,
  name?: string,
  checked: boolean,
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  disabled?: boolean,
  label?: string | ReactNode,
  wrapperId?: string,
  value: string,
  margin?: string,
};

const RadioButton = ({
  id,
  checked,
  onChange,
  name,
  label,
  wrapperId,
  value,
  margin,
  ...props
}: RadioButtonProps) => (
  <RadioButtonWrapper
    htmlFor={id}
    id={wrapperId}
    margin={margin}
  >
    <input
      id={id}
      name={name}
      onChange={onChange}
      value={value}
      type="radio"
      checked={checked}
      {...props}
    />
    {label && <RadioButtonLabel>{label}</RadioButtonLabel>}
  </RadioButtonWrapper>
);

export default RadioButton;
