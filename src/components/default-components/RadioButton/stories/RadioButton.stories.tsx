/* eslint-disable react/destructuring-assignment */
import React, { useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import RadioButton, { RadioButtonProps } from '../index';

const meta: Meta<typeof RadioButton & {
  radioButtons: {
    label: string,
    id: string,
    value: string,
  }[],
}> = {
  title: 'Components/Default Components/RadioButton',
  component: RadioButton,
  argTypes: {},
  args: {
    disabled: false,
  },
};
export default meta;

export const Default: StoryFn<typeof RadioButton> = (args: RadioButtonProps) => {
  const [selectedValue, setSelectedValue] = useState('first');

  const radioButtons = [
    {
      label: 'First RadioButton',
      id: 'first',
      value: 'first',
    },
    {
      label: 'Second RadioButton',
      id: 'second',
      value: 'second',
    },
  ];
  return (
    <div
      style={{
        minWidth: 200,
      }}
    >
      {radioButtons.map((radioButtonData: {
        label: string,
        id: string,
        value: string,
      }) => (
        <RadioButton
          id={radioButtonData.id}
          checked={radioButtonData.value === selectedValue}
          value={radioButtonData.value}
          label={radioButtonData.label}
          onChange={(e) => {
            setSelectedValue(e.target.value);
            if (args.onChange) {
              args.onChange(e);
            }
          }}
        />
      ))}
    </div>
  );
};
