import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';
import React from 'react';
import { defaultTheme } from '../../../styles/themes';

type RadioButtonWrapperProps = {
  margin?: string;
};

export const RadioButtonWrapper = styled(
  (props: any) => (
    <label
      htmlFor={props.htmlFor}
      {...props}
      className={`${props.className} odecloud-radio-button`}
    >
      {props.children}
    </label>
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<RadioButtonWrapperProps>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: fit-content;
  margin: ${({ margin }) => (margin || '0 0 10px 0')};
  cursor: pointer;

  input[type="radio"] {
    appearance: none;
    background-color: transparent;
    font: inherit;
    width: 20px;
    height: 20px;
    border: 1.5px solid ${({ theme }) => theme?.outline?.[300] || defaultTheme.outline[300]};
    border-radius: 50%;
    margin: 0 8px 0 0;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  input[type="radio"]::before {
    content: "";
    width: 10px;
    height: 10px;
    border-radius: 50%;
    transform: scale(0);
    transition: 120ms transform ease-in-out;
    box-shadow: inset 1em 1em ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  }

  input[type="radio"]:checked::before {
    transform: scale(1);
  }

  input[type="radio"]:hover {
    border-color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  }

  input[type="radio"]:checked {
    border-color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  }
`;

export const RadioButtonLabel = styled('span')`
  font-weight: 500;
  font-size: 15px;
  line-height: 22px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
`;
