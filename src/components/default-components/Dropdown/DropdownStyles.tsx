import React from 'react';
import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';
import {
  StyledDropdownToggle,
} from '../../../styles/common';
import { defaultTheme } from '../../../styles/themes';

type DropdownWrapperProps = {
  margin?: string;
  disabled?: boolean;
};

export const DropdownWrapper = styled(
  (props: any) => (
    <div
      {...props}
      className={`${props.className} odecloud-dropdown-wrapper`}
    />
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<DropdownWrapperProps>`
  width: 100%;
  margin: ${({ margin }) => (margin)};

  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

export const StyledLabel = styled('p')`
  transition: 0.3s;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};  
`;

type CustomDropdownToggleProps = {
  isOpen: boolean,
};

export const CustomDropdownToggle = styled(StyledDropdownToggle)<CustomDropdownToggleProps>`
  display: flex;
  align-items: center;
  justify-content: space-between;

  svg {
    width: 24px;
    height: 24px;

    ${({ isOpen }) => isOpen && `
      transform: rotate(180deg);
    `}
  }
`;

export const MainChildrenWrapper = styled('div')`
  margin: 0 10px 0 0;
`;
