import React, {
  ReactNode, useState,
} from 'react';
import {
  CustomDropdownToggle, DropdownWrapper, MainChildrenWrapper, StyledLabel,
} from './DropdownStyles';
import {
  StyledDropdown, StyledDropdownItem, StyledDropdownMenu,
} from '../../../styles/common';
import { ChevronDownIcon } from '../../../modules/chat/icons';

export type DropdownProps = {
  mainChildren: ReactNode,
  itemList: {
    onClick?: () => void,
    itemChildren: ReactNode,
  }[],
  label?: string,
  margin?: string,
  disabled?: boolean,
};

export const CustomDropdown = ({
  // Default props
  label,
  margin = '0 0 15px 0',
  mainChildren,
  itemList,
  disabled,
}: DropdownProps) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <DropdownWrapper
      margin={margin}
      disabled={disabled}
    >
      {label && <StyledLabel>{label}</StyledLabel>}
      <StyledDropdown
        isOpen={isMenuOpen}
        toggle={() => {
          setIsMenuOpen(!isMenuOpen);
        }}
      >
        <CustomDropdownToggle
          isOpen={isMenuOpen}
        >
          <MainChildrenWrapper>
            {mainChildren}
          </MainChildrenWrapper>
          <ChevronDownIcon />
        </CustomDropdownToggle>
        <StyledDropdownMenu>
          {itemList.map((item) => (
            <StyledDropdownItem
              onClick={item.onClick || (() => ({}))}
            >
              {item.itemChildren}
            </StyledDropdownItem>
          ))}
        </StyledDropdownMenu>
      </StyledDropdown>
    </DropdownWrapper>
  );
};

export default CustomDropdown;
