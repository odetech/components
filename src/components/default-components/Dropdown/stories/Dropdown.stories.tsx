import React, { Fragment } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import Dropdown from '../index';

const meta: Meta<typeof Dropdown> = {
  title: 'Components/Default Components/Dropdown',
  component: Dropdown,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.width = '100%';
      }
      return (
        <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
          {story()}
        </MemoryRouter>
      );
    },
  ],
  argTypes: {
    itemList: { control: 'text' },
    label: { control: 'text' },
    margin: { control: 'text' },
    disabled: { control: 'boolean' },
  },
  args: {
    label: 'Label',
    disabled: false,
    mainChildren: (
      <Fragment>
        Default dropdown
      </Fragment>
    ),
    itemList: [
      {
        onClick: () => {
          console.log('1st item');
        },
        itemChildren: (
          <Fragment>
            1st item
          </Fragment>
        ),
      },
      {
        onClick: () => {
          console.log('2ed item');
        },
        itemChildren: (
          <Fragment>
            2ed item
          </Fragment>
        ),
      },
      {
        onClick: () => {
          console.log('3ed item');
        },
        itemChildren: (
          <Fragment>
            3ed item
          </Fragment>
        ),
      },
    ],
  },
};
export default meta;

// 👇 We create a “template” of how args map to rendering
export const Default: StoryFn<typeof Dropdown> = (args) => (
  <div
    style={{
      width: '100%',
      maxWidth: 500,
    }}
  >
    <Dropdown
      {...args}
    />
  </div>
);
