import React from 'react';
import Modal from 'react-modal';
import { CustomModal } from '../index';
import {
  ModalInfoText, ModalButtonsWrapper, ModalTitle, ModalItemsWrapper, ModalSubmitButton, ModalCancelButton,
} from '../ModalStyles';

Modal.setAppElement('body');

type DeleteModalProps = {
  showModal: boolean;
  setShowModal: (showModal: boolean) => void;
  onSubmit: () => void;
  title: string;
  infoText: string;
};

const DeleteModal: React.FC<DeleteModalProps> = ({
  showModal, setShowModal, onSubmit, infoText, title,
}) => (
  <CustomModal
    isOpen={showModal}
    onClose={() => setShowModal(false)}
    size="sm"
  >
    <ModalTitle>
      {title}
    </ModalTitle>
    <ModalItemsWrapper>
      <ModalInfoText>
        {infoText}
      </ModalInfoText>
    </ModalItemsWrapper>
    <ModalButtonsWrapper>
      <ModalCancelButton
        type="button"
        color="black"
        onClick={() => {
          setShowModal(false);
        }}
        view="text"
      >
        Cancel
      </ModalCancelButton>
      <ModalSubmitButton
        type="button"
        onClick={() => {
          setShowModal(false);
          onSubmit();
        }}
        color="red"
      >
        Delete
      </ModalSubmitButton>
    </ModalButtonsWrapper>
  </CustomModal>
);

export default DeleteModal;
