import styled from '@emotion/styled';
import Modal from 'react-modal';
import { SerializedStyles } from '@emotion/react';
import { Button } from '~/components/default-components';
import { getVerticalScrollbarStyles } from '~/styles';
import { defaultTheme } from '../../../styles/themes';

type StyledModalProps = {
  isOpen: boolean;
};

export const StyledModal = styled(Modal)<StyledModalProps>`
  font-family: 'Poppins', sans-serif;
  position: absolute;
  margin: auto;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: ${({ theme }) => theme?.background?.surfaceContainerHigh
    || defaultTheme.background.surfaceContainerHigh};          
  border-radius: 15px;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[3] || defaultTheme.shadow.drop[3]};    
  padding: 24px;
  animation: fadeIn 0.3s;
  width: 100%;
  height: 100%;

  display: flex;
  flex-direction: column;

  &:focus {
    outline: none;
  }

  @keyframes fadeIn {
    0% { opacity: 0; }
    100% { opacity: 1; }
  }
`;

type ModalItemsWrapperProps = {
  margin?: string,
};

export const ModalItemsWrapper = styled('div')<ModalItemsWrapperProps>`
  height: 100%;
  margin: ${({ margin }) => (margin || '0 -20px 24px 0')};
  padding: 0 20px 0 0;

  overflow-y: auto;
  overflow-x: hidden;

  ${({ theme }) => getVerticalScrollbarStyles(theme)}
`;

type CloseButtonProps = {
  buttonStyles?: SerializedStyles,
};

export const CloseButton = styled('button')<CloseButtonProps>`
  background: none;
  border: none;
  position: absolute;
  right: 24px;
  top: 24px;
  padding: 0;

  svg {
    fill: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};              
    width: 24px;
    height: 24px;
  }

  ${({ buttonStyles }) => buttonStyles};
`;

export const ModalButtonsWrapper = styled('div')`
  display: flex;
  margin: 0 0 0 auto;
  gap: 16px;
`;

export const ModalTitle = styled('p')`
  font-size: 20px;
  font-weight: 500;
  line-height: 24px;
  margin: 0 0 20px 0;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
`;

type ModalInfoText = {
  margin?: string,
};

export const ModalInfoText = styled('div')<ModalInfoText>`
  font-size: 16px;
  font-weight: 400;
  line-height: 24px;
  margin: ${({ margin }) => (margin || '0')};
  color: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
`;

export const ModalCancelButton = styled(Button)`
  font-size: 16px;
  font-weight: 500;
  line-height: 24px;
  padding: 12px 16px;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
`;

export const ModalSubmitButton = styled(Button)`
  font-size: 16px;
  font-weight: 500;
  line-height: 24px;
  padding: 12px 16px;
  color: ${({ theme }) => theme?.text?.onPrimary || defaultTheme.text.onPrimary};
`;
