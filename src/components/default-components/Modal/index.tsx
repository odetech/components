import React, { ReactNode, CSSProperties } from 'react';
import Modal from 'react-modal';
import { SerializedStyles } from '@emotion/react';
import { transparentize } from 'polished';
import { useTheme } from '@storybook/theming';
import { defaultTheme } from '../../../styles/themes';
import { StyledModal, CloseButton } from './ModalStyles';
import { CrossIcon } from '../../icons';

Modal.setAppElement('body');

const chooseModalWidthSize = (size: string | 'md' | 'sm' | 'lg' | 'elg') => {
  let width;
  switch (size) {
    case 'sm':
      width = 400;
      break;
    case 'lg':
      width = 760;
      break;
    case 'elg':
      width = 1140;
      break;
    default:
      width = 560;
      break;
  }
  return width;
};

export type ModalProps = {
  onClose: () => void;
  isOpen: boolean;
  children: ReactNode;
  ariaHideApp?: boolean;
  size?: string | 'md' | 'sm' | 'lg' | 'elg';
  buttonStyles?: SerializedStyles;
  overlayStyles?: CSSProperties;
  contentStyles?: CSSProperties;
  itemsWrapperStyles?: CSSProperties;
  closeIcon?: ReactNode;
};

export const CustomModal = ({
  // Default props
  onClose,
  children,
  overlayStyles,
  contentStyles,
  buttonStyles,
  closeIcon,
  size = 'md',
  ...props
}: ModalProps) => {
  const theme = useTheme();

  return (
    <StyledModal
      className="odecloud-modal"
      appElement={document.getElementById('body') as HTMLElement}
      {...props}
      style={{
        overlay: {
          zIndex: 2000,
          background: `${transparentize(0.3, theme?.background?.surface || defaultTheme.background.surface)}`,
          ...overlayStyles,
        },
        content: {
          maxWidth: chooseModalWidthSize(size),
          height: 'fit-content',
          maxHeight: 800,
          ...contentStyles,
        },
      }}
    >
      <CloseButton
        buttonStyles={buttonStyles}
        type="button"
        onClick={onClose}
      >
        {closeIcon || <CrossIcon />}
      </CloseButton>
      {children}
    </StyledModal>
  );
};
export default CustomModal;
