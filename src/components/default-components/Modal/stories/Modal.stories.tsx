/* eslint-disable max-len */
import React, { useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import {
  Button, DeleteModal, ModalButtonsWrapper, ModalCancelButton, ModalInfoText, ModalSubmitButton, ModalTitle,
} from '~/index';
import CustomModal from '../index';
import { ModalItemsWrapper } from '../ModalStyles';

const meta: Meta<typeof CustomModal> = {
  title: 'Components/Default Components/Modal',
  component: CustomModal,
  argTypes: {},
  args: {},
} as Meta<typeof CustomModal>;
export default meta;

export const Default: StoryFn<typeof CustomModal> = (args) => {
  const [isOpen, setIsOpen] = useState<boolean>(
    false,
  );
  const [isOpenDeleteModal, setIsOpenDeleteModal] = useState<boolean>(
    false,
  );

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: '10px',
      }}
    >
      <Button
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        Default Modal Window
      </Button>
      <Button
        onClick={() => {
          setIsOpenDeleteModal(!isOpenDeleteModal);
        }}
        color="red"
      >
        Delete Modal Window
      </Button>
      {isOpen && (
        <CustomModal
          {...args}
          ariaHideApp={false}
          isOpen={isOpen}
          onClose={() => {
            setIsOpen(false);
          }}
        >

          <ModalTitle>
            Default
          </ModalTitle>
          <ModalItemsWrapper>
            <ModalInfoText>
              That’s great information you provided. By the way, you just got a new achievement!
            </ModalInfoText>
          </ModalItemsWrapper>
          <ModalButtonsWrapper>
            <ModalCancelButton
              type="button"
              color="black"
              onClick={() => {
                setIsOpen(false);
              }}
              view="text"
            >
              Close
            </ModalCancelButton>
            <ModalSubmitButton
              type="button"
              onClick={() => {
                console.log('Modal submit');
                setIsOpen(false);
              }}
            >
              Okay
            </ModalSubmitButton>
          </ModalButtonsWrapper>
        </CustomModal>
      )}
      {isOpenDeleteModal && (
        <DeleteModal
          showModal={isOpenDeleteModal}
          setShowModal={setIsOpenDeleteModal}
          onSubmit={() => {
            console.log('Delete submit!');
          }}
          title="Danger"
          infoText="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit."
        />
      )}
    </div>
  );
};
