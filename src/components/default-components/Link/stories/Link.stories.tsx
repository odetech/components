import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import Link from '../index';

const meta: Meta<typeof Link> = {
  title: 'Components/Default Components/Link',
  component: Link,
  argTypes: {
    href: { control: 'text' },
    target: { control: 'text' },
    disabled: { control: 'boolean' },
  },
  args: {
    href: 'https://google.com',
    target: '_blank',
    disabled: false,
  },
};
export default meta;

export const Default: StoryFn<typeof Link> = (args) => (
  <Link
    {...args}
  >
    Storybook link
  </Link>
);
