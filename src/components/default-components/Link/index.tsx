import { ReactNode } from 'react';
import { Link } from './LinkStyles';

export type LinkProps = {
  href: string;
  children: ReactNode;
  disabled?: boolean;
  target?: string;
};

export default Link;
