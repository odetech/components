import styled from '@emotion/styled';
import { transparentize } from 'polished';
import isPropValid from '@emotion/is-prop-valid';
import React from 'react';
import { defaultTheme } from '../../../styles/themes';

type LinkProps = {
  disabled?: boolean;
};

export const Link = styled(
  (props: any) => (
    <a
      {...props}
      className={`${props.className} odecloud-link`}
    >
      {props.children}
    </a>
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<LinkProps>`
  color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  text-decoration: none;
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;

  &:hover {
    color: ${({ theme }) => transparentize(0.2, theme?.brand?.primary || defaultTheme.brand.primary)};
  }
  
  ${({ disabled }) => (disabled && `
    opacity: 0.5;
    pointer-events: none;
  `)}
`;
