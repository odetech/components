import React, { ChangeEvent, KeyboardEvent, useState } from 'react';
import {
  InputWithButtonWrapper, InputButton, StyledInputWithHiddenInfo,
} from './InputStyles';
import {
  InputWrapper, StyledLabel,
} from '../Input/InputStyles';
import { EyeIcon, EyeOffIcon } from '../../icons';

export type InputProps = {
  label?: string;
  disabled?: boolean;
  type?: string;
  name: string;
  id?: string;
  value?: string;
  placeholder?: string;
  margin?: string;
  color?: 'orange' | 'red' | string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onKeyDown?: (e: KeyboardEvent) => void;
  onEyeButtonClick?: (e: any) => void;
  autoComplete?: string;
  onClick?: any;
};

export const InputWithHiddenInfo = ({
  // Default props
  disabled = false,
  label,
  margin = '0 0 0 0',
  type = 'text',
  color,
  onEyeButtonClick,
  onClick,
  ...props
}: InputProps) => {
  const [showText, setShowText] = useState(false);

  return (
    <InputWrapper
      className="odecloud-input-with-hidden-info-wrapper"
      margin={margin}
      color={color}
      onClick={onClick}
    >
      {label && <StyledLabel>{label}</StyledLabel>}
      <InputWithButtonWrapper>
        <StyledInputWithHiddenInfo
          className="odecloud-input-with-hidden-info"
          disabled={disabled}
          {...props}
          type={showText ? type : 'password'}
        />
        <InputButton
          view="text"
          type="button"
          disabled={disabled}
          onClick={() => {
            if (onEyeButtonClick && !onClick) {
              onEyeButtonClick({
                defaultOnEyeButtonClick: setShowText,
                showTextValue: !showText,
              });
            } else if (!onClick) {
              setShowText(!showText);
            }
          }}
        >
          {showText ? <EyeOffIcon /> : <EyeIcon />}
        </InputButton>
      </InputWithButtonWrapper>
    </InputWrapper>
  );
};

export default InputWithHiddenInfo;
