import React, { useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import InputWithHiddenInfo from '../index';

const meta: Meta<typeof InputWithHiddenInfo> = {
  title: 'Components/Default Components/InputWithHiddenInfo',
  component: InputWithHiddenInfo,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.width = 'unset';
      }
      return (
        <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
          {story()}
        </MemoryRouter>
      );
    },
  ],
  argTypes: {},
  args: {
    disabled: false,
    onEyeButtonClick: undefined,
    onClick: undefined,
    placeholder: 'Placeholder text',
  },
};
export default meta;

export const Default: StoryFn<typeof InputWithHiddenInfo> = (args) => {
  const [value, setValue] = useState<string>(
    '',
  );

  return (
    <InputWithHiddenInfo
      {...args}
      value={value}
      onChange={(e) => {
        setValue(e.target.value);
      }}
    />
  );
};
