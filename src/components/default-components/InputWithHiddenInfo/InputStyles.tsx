import styled from '@emotion/styled';
import { defaultTheme } from '../../../styles/themes';
import { StyledInput } from '../Input/InputStyles';
import { Button } from '../index';

export const InputWithButtonWrapper = styled('div')`
  width: 100%;
  display: flex;
  align-items: center;
`;

export const StyledInputWithHiddenInfo = styled(StyledInput)`
  padding: 8px 40px 8px 16px;
`;

export const InputButton = styled(Button)`
  padding: 9px 10px 9px 8px;
  margin: 0 0 0 -39px;
  border-radius: 0 10px 10px 0;
  z-index: 1;
  
  svg {
    fill: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
  }
`;
