import React, {
  ChangeEvent, KeyboardEvent, FocusEvent,
} from 'react';
import {
  DeleteValueButton, InputWrapper, StyledInput, StyledLabel,
} from './InputStyles';
import { CloseIcon } from '../../../modules/chat/icons';

export type InputProps = {
  label?: string;
  disabled?: boolean;
  type?: string;
  name: string;
  id?: string;
  value?: string;
  placeholder?: string;
  margin?: string;
  labelView?: 'outside' | 'inside' | string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onDelete?: () => void;
  onKeyDown?: (e: KeyboardEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  autoComplete?: string;
  color?: 'orange' | 'pink' | 'red' | string;
  error?: boolean;
};

export const Input = ({
  // Default props
  disabled = false,
  label,
  labelView = 'outside',
  margin = '0 0 0 0',
  type = 'text',
  color = 'orange',
  onDelete,
  error,
  value,
  ...props
}: InputProps) => (
  <InputWrapper
    margin={margin}
    color={error ? 'red' : color}
    labelView={labelView}
    disabled={disabled}
    notEmpty={!!value}
  >
    {label && (
      <StyledLabel>
        {label}
      </StyledLabel>
    )}
    <StyledInput
      className="odecloud-input"
      disabled={disabled}
      type={type}
      color={error ? 'red' : color}
      withDeleteButton={!!onDelete}
      value={value}
      {...props}
    />
    {onDelete && (
      <DeleteValueButton
        className="odecloud-input-delete-button"
        onClick={onDelete}
      >
        <CloseIcon />
      </DeleteValueButton>
    )}
  </InputWrapper>
);

export default Input;
