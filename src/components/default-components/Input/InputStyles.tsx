import React from 'react';
import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';
import { defaultTheme } from '../../../styles/themes';

export const StyledInput = styled('input', {
  shouldForwardProp: isPropValid,
})<{
  withDeleteButton?: boolean,
}>`
  position: relative;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  padding: ${({ withDeleteButton }) => (withDeleteButton ? '10px 40px 10px 10px' : '10px')};
  width: 100%;
  max-width: 475px;
  min-width: 232px;
  height: 40px;
  transition: 0.3s;
  text-overflow: ellipsis;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};        
  background: transparent;
  border: 1px solid ${({ theme }) => theme?.outline?.[200] || defaultTheme.outline[200]};
  border-radius: 10px;

  &::placeholder {
    color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  }

  &:focus {
    box-shadow: none;
    outline: none;
  }

  &:disabled {
    color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled};
    border-color: ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]} !important;
    cursor: not-allowed;
  }

  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus {
    border-color: ${({ color, theme }) => {
    switch (color) {
      case 'orange':
        return theme?.brand?.primary || defaultTheme.brand.primary;
      case 'pink':
        return theme?.brand?.secondary || defaultTheme.brand.secondary;
      case 'red':
        return theme?.error?.primary || defaultTheme.error.primary;
      default:
        return theme?.brand?.primary || defaultTheme.brand.primary;
    }
  }};
    -webkit-text-fill-color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};    
    -webkit-box-shadow: 0 0 0 1000px transparent inset;
    transition: background-color 5000s ease-in-out 0s;
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
  }
`;

export const StyledLabel = styled('p')`
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  margin: 0 0 4px 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};  
  transition: 0.3s;
`;

export const DeleteValueButton = styled('button')`
  position: absolute;
  right: 0;
  bottom: 0;
  padding: 8px 10px;
  background: transparent;
  outline: none;
  border: none;
  opacity: 0;
  transition: 0.3s;
  
  svg {
    fill: ${({ theme }) => theme?.text?.onSurfaceVarSecond || defaultTheme.text.onSurfaceVarSecond};
    width: 20px;
    height: 20px;
  }
`;

type InputWrapperProps = {
  labelView?: 'outside' | 'inside' | string,
  margin?: string,
  color?: 'orange' | 'red' | string,
  disabled?: boolean,
  notEmpty?: boolean,
};

export const InputWrapper = styled(
  (props: any) => (
    <label
      htmlFor={props.htmlFor}
      {...props}
      className={`${props.className} odecloud-input-wrapper`}
    >
      {props.children}
    </label>
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<InputWrapperProps>`
  margin: ${({ margin }) => (margin)};
  position: relative;
  cursor: pointer;

  ${({ color, theme }) => {
    if (color === 'red') {
      return `
      p {
        color: ${theme?.error?.primary || defaultTheme.error.primary};
      }
      input {
        border-color: ${theme?.error?.primary || defaultTheme.error.primary};
      }
    `;
    }
    return '';
  }}

  ${({ labelView, theme }) => {
    if (labelView === 'inside') {
      return `
      p {
        position: absolute;
        top: -8px;
        left: 12px;
        font-size: 12px;
        line-height: 16px;
        z-index: 1;
        padding: 0 4px;
        background-color: ${theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};
        opacity: 0;
      }

      input {
        padding: 10px 15px;
      }

      &:focus-within {
          input {
            &::placeholder {
              opacity: 0;
            }
          }
          
          p {
            opacity: 1;
          }
        }
    `;
    }
    return '';
  }}

  &:hover {
    .odecloud-input-delete-button {
      opacity: ${({ notEmpty }) => (notEmpty ? 1 : 0)};
    }
  }

  &:focus-within, &:active {
    input {
      border-color: ${({ color, theme }) => {
    let selectedColor;

    switch (color) {
      case 'orange': {
        [selectedColor] = [theme?.brand?.primary || defaultTheme.brand.primary];
        break;
      }
      case 'pink': {
        [selectedColor] = [theme?.brand?.secondary || defaultTheme.brand.secondary];
        break;
      }
      case 'red': {
        [selectedColor] = [theme?.error?.primary || defaultTheme.error.primary];
        break;
      }
      default: {
        [selectedColor] = [theme?.brand?.primary || defaultTheme.brand.primary];
      }
    }

    return selectedColor;
  }}
    }

    button {
      opacity: 1;
    }
  }

  ${({ disabled, theme }) => (disabled && `
    p {
      color:  ${theme?.text?.onSurfaceDisabled || defaultTheme?.text?.onSurfaceDisabled} !important;      
      opacity: 1;
      cursor: not-allowed;
    }

    button {
      display: none !important;
    }
  `)}
`;
