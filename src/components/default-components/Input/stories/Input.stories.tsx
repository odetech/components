import React, { useState } from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import Input from '../index';

const meta: Meta<typeof Input> = {
  title: 'Components/Default Components/Input',
  component: Input,
  argTypes: {
    label: { control: 'text' },
    placeholder: { control: 'text' },
    disabled: { control: 'boolean' },
    color: {
      control: 'select',
      options: [
        'orange',
        'red',
        'pink',
      ],
    },
  },
  args: {
    disabled: false,
    placeholder: 'Placeholder text',
    label: 'Label',
  },
};
export default meta;

export const Default: StoryFn<typeof Input> = (args) => {
  const [value, setValue] = useState<string>(
    '',
  );

  return (
    <Input
      {...args}
      value={value}
      onChange={(e) => {
        setValue(e.target.value);
      }}
      onDelete={() => {
        setValue('');
      }}
    />
  );
};

export const Error = Default.bind({});
Error.args = {
  color: 'red',
};
