import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';
import { transparentize } from 'polished';
import React from 'react';
import { defaultTheme } from '../../../styles/themes';

type ToggleWrapperProps = {
  margin?: string;
  hide?: boolean;
};

export const ToggleWrapper = styled(
  (props: any) => (
    <label
      htmlFor={props.htmlFor}
      {...props}
      className={`${props.className} odecloud-toggle`}
    >
      {props.children}
    </label>
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<ToggleWrapperProps>`
  margin: ${({ margin }) => (margin)};
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 0;
  cursor: pointer;

  ${({ hide }) => (hide ? 'visibility : hidden' : '')};

  input {
    position: absolute;
    z-index: -1;
    opacity: 0;
  }

  & input:checked + div::after {
    left: 16px;
    background: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  }

  & input:checked + div::before {
    background: ${({ theme }) => transparentize(0.9, theme?.brand?.primary || defaultTheme.brand.primary)};    
    border-color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
  }

  & input:disabled + div,
  & input:checked:disabled + div {
    opacity: 0.6;
  }

  &:focus {
    span {
      color: ${({ theme }) => transparentize(0.5, theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar)};      
    }
  }

  &:active, &:hover {
    span {
      color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};
    }
  }

  &:disabled {
    span {
      color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    }
  }
`;

type MainToggleElementProps = {
  disabled?: boolean,
}

export const MainToggleElement = styled('div', {
  shouldForwardProp: isPropValid,
})<MainToggleElementProps>`
  position: relative;
  height: 16px;
  width: 30px;
  cursor: ${({ disabled }) => (disabled ? 'unset' : 'pointer')};
  
  &::before {
    content: '';
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 0;
    width: 30px;
    height: 16px;
    border-radius: 50px;
    border: 0.5px solid ${({ theme }) => theme?.outline?.[300] || defaultTheme.outline[300]};
    background-color: transparent;
    transition: .2s;
  }
  
  &::after {
    content: '';
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 2px;
    width: 12px;
    height: 12px;
    border-radius: 10px;
    background-color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
    transition: .2s;
  }
`;

export const ToggleLabel = styled('span')`
  transition: 0.3s;
  font-weight: 500;
  font-size: 15px;
  line-height: 24px;
  margin: 16px;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};  
  display: flex;
  align-items: center;
`;
