import React, { ChangeEvent, ReactNode } from 'react';
import {
  ToggleWrapper, ToggleLabel, MainToggleElement,
} from './ToggleStyles';

type ToggleProps = {
  id?: string,
  name?: string,
  checked: boolean,
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  disabled?: boolean,
  hide?: boolean,
  label?: string | ReactNode,
  wrapperId?: string,
};

const Toggle = ({
  id,
  checked,
  onChange,
  disabled,
  name,
  hide,
  label,
  wrapperId = '',
  ...props
}: ToggleProps) => (
  <ToggleWrapper
    htmlFor={id}
    id={wrapperId}
    hide={hide}
  >
    {label && <ToggleLabel>{label}</ToggleLabel>}
    <input
      id={id}
      type="checkbox"
      name={name}
      onChange={onChange}
      checked={checked}
      disabled={disabled}
      {...props}
    />
    <MainToggleElement disabled={disabled} />
  </ToggleWrapper>
);

export default Toggle;
