import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import Toggle from '../index';

const meta: Meta<typeof Toggle> = {
  title: 'Components/Default Components/Toggle',
  component: Toggle,
  argTypes: {
    disabled: { control: 'boolean' },
    label: { control: 'text' },
  },
  args: {
    disabled: false,
    label: 'OdeCloud Toggle',
  },
};
export default meta;
export const Default: StoryFn<typeof Toggle> = (args) => (
  <div
    style={{
      maxWidth: 210,
    }}
  >
    <Toggle
      {...args}
    />
  </div>
);
