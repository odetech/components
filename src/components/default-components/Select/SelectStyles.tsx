import React from 'react';
import { transparentize } from 'polished';
import isPropValid from '@emotion/is-prop-valid';
import styled from '@emotion/styled';
import { getVerticalScrollbarStyles } from '~/styles';
import { isValidHex } from '~/helpers';
import { ThemeProps } from '../../../styles/types';
import { defaultTheme } from '../../../styles/themes';

type SelectWrapperProps = {
  margin?: string;
};

export const SelectWrapper = styled(
  (props: any) => (
    <div
      {...props}
      className={`${props.className} odecloud-select-wrapper`}
    />
  ),
  {
    shouldForwardProp: isPropValid,
  },
)<SelectWrapperProps>`
  width: 100%;
  margin: ${({ margin }) => (margin)};

  &:focus {
    p {
      color: ${({ theme }) => transparentize(0.5, theme?.brand?.primary || defaultTheme.brand.primary)};  
    }
    
    input {
      border-color: ${({ theme }) => transparentize(0.5, theme?.brand?.primary || defaultTheme.brand.primary)};
    }
  }

  &:active, &:hover {
    p {
      color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};        
    }
    
    input {
      border-color: ${({ theme }) => theme?.brand?.primary || defaultTheme.brand.primary};    
      background-color: ${({ theme }) => transparentize(0.9, theme?.brand?.primary || defaultTheme.brand.primary)};
    }
  }

  &:disabled {
    p {
      color: ${({ theme }) => theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled};
    }

    input {
      border-color: ${({ theme }) => theme?.outline?.[200] || defaultTheme.outline[200]};
      background-color: ${({ theme }) => theme?.background?.surfaceContainerVar
        || defaultTheme.background.surfaceContainerVar};
    }
  }

  div {    
    ${({ theme }) => getVerticalScrollbarStyles(theme, false)}    
  }
`;

export const StyledLabel = styled('p')`
  transition: 0.3s;
  font-size: 14px;
  line-height: 21px;
  margin: 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};  
`;

export const SelectMenuWrapper = styled('div')`
  .menu {
    animation: fadeIn 0.3s;

    @keyframes fadeIn {
      from {
        opacity: 0;
        margin: -8px 0 0 0;
      }

      to {
        opacity: 1;
        margin: 8px 0 0 0;
      }
    }
  }
`;

export type ColorStylesConfigProps = {
  view?: 'open' | 'closed' | string,
};

export const colorStylesConfig = (
  view: ColorStylesConfigProps['view'],
  valueContainerStyles?: any,
  valueControlStyles?: any,
  theme?: ThemeProps,
) => ({
  placeholder: (styles: any) => ({
    ...styles,
    fontSize: 14,
    fontWeight: 500,
    lineHeight: '24px',
    color: theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar,
  }),
  container: (styles: any) => ({
    ...styles,
    width: '100%',
  }),
  valueContainer: (styles: any) => ({
    ...styles,
    padding: '5px 10px',
    fontSize: 14,
    fontWeight: 500,
    ...valueContainerStyles,
  }),
  control: (styles: any) => {
    const hiddenBorderStyles = {
      border: '1px solid transparent',
    };
    const controlStyles = {
      ...styles,
      background: `${theme?.background?.surfaceContainer || defaultTheme.background.surfaceContainer};`,
      boxShadow: 'unset',
      borderColor: `${theme?.outline?.[300] || defaultTheme.outline[300]};`,
      borderRadius: '7px',

      ':hover': {
        ...styles[':hover'],
        border: `1px solid ${theme?.brand?.primary || defaultTheme.brand.primary}`,
      },

      ':active': {
        ...styles[':hover'],
        border: `1px solid ${theme?.brand?.primary || defaultTheme.brand.primary}`,
      },

      ':focus': {
        ...styles[':hover'],
        border: `1px solid ${theme?.brand?.primary || defaultTheme.brand.primary}`,
      },
      ...valueControlStyles,
    };
    if (view === 'open') {
      return ({
        ...controlStyles,
        ...hiddenBorderStyles,
      });
    }
    return ({
      ...controlStyles,
    });
  },
  indicatorsContainer: (styles: any) => ({
    ...styles,
    padding: '0 8px 0 0',

    svg: {
      fill: `${theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar} !important;`,
    },
  }),
  menu: (styles: any) => ({
    ...styles,
    boxShadow: 'unset',
    border: `1px solid ${theme?.brand?.primary || defaultTheme.brand.primary}`,
    borderColor: theme?.brand?.primary || defaultTheme.brand.primary,
    background: theme?.background?.surfaceContainerHigh || defaultTheme.background.surfaceContainerHigh,
    zIndex: 3,
  }),
  option: (styles: any, {
    data, isDisabled, isFocused,
  }: any) => {
    const dataColor = data.color && data.color.replace(';', '');
    const dataColorValid = isValidHex(dataColor);

    const primaryContainerColor = dataColorValid
      ? dataColor : theme?.brand?.primaryContainer || defaultTheme.brand.primaryContainer;
    const primaryColor = dataColorValid ? dataColor : theme?.brand?.primary || defaultTheme.brand.primary;
    const onSurfaceColor = dataColorValid ? dataColor : theme?.text?.onSurface || defaultTheme.text.onSurface;

    return {
      ...styles,
      background: isDisabled || !isFocused
        ? 'transparent'
        : transparentize(0.8, theme?.brand?.primaryContainer || defaultTheme.brand.primaryContainer),
      // eslint-disable-next-line no-nested-ternary
      color: isDisabled
        ? theme?.text?.onSurfaceDisabled || defaultTheme.text.onSurfaceDisabled
        : (isFocused ? primaryColor : onSurfaceColor),
      cursor: isDisabled ? 'not-allowed' : 'default',

      ':active': {
        ...styles[':active'],
        background:
          !isDisabled && (data.color && data.color.includes('linear-gradient(')
            ? data.color
            : transparentize(0.8, primaryContainerColor)),
        color: primaryColor,
      },

      ':hover': {
        ...styles[':hover'],
        background: !isDisabled && (data.color && data.color.includes('linear-gradient(')
          ? data.color
          : transparentize(0.8, primaryColor)
        ),
        color: theme?.brand?.primary || defaultTheme.brand.primary,
      },
    };
  },
  singleValue: (styles: any) => ({
    ...styles,
    fontSize: 14,
    fontWeight: 500,
    lineHeight: '24px',
    color: theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar,
  }),
  multiValue: (styles: any, { data }: any) => {
    let multiValueStyles = {
      ...styles,
      borderRadius: 28,
      padding: '5px 8px',
    };

    const dataColor = data.color && data.color.replace(';', '');
    if (isValidHex(dataColor)) {
      multiValueStyles = {
        ...multiValueStyles,
        background: dataColor,
      };
    }

    return multiValueStyles;
  },
  multiValueLabel: (styles: any) => ({
    ...styles,
    color: theme?.background?.surface || defaultTheme.background.surface,
    padding: '0 4px 0 6px',
  }),
  multiValueRemove: (styles: any) => ({
    ...styles,
    cursor: 'pointer',
    color: theme?.background?.surface || defaultTheme.background.surface,
    padding: 2,
    ':hover': {
      background: transparentize(0.2, theme?.background?.surface || defaultTheme.background.surface),
      color: theme?.text?.onSurface || defaultTheme.text.onSurface,
    },
  }),
  input: (styles: any) => ({
    ...styles,
    fontSize: 14,
    fontWeight: 500,
    lineHeight: '24px',
    color: theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar,
  }),
});
