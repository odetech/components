import React from 'react';
import { components, OptionProps } from 'react-select';
import { Avatar } from '~/components/avatar';
import styled from '@emotion/styled';

const { Option } = components;

const OptionWrapper = styled('div')`
  display: flex;
  flex-direction: row;
  gap: 8px;
  align-items: center;  
`;

export const UserOptionComponent: React.FC<OptionProps<{
  label: string, value: string | number, firstName: string, lastName: string, avatar?: { secureUrl: string }
}, boolean>> = ({
  children,
  isSelected,
  label,
  ...rest
}) => (
  <Option {...rest} isSelected={isSelected} label={label}>
    <OptionWrapper>
      <Avatar
        size={24}
        user={{
          profile: {
            firstName: rest.data.firstName,
            lastName: rest.data.lastName,
            avatar: {
              secureUrl: rest.data.avatar?.secureUrl || undefined,
            },
          },
        }}
      />
      <span>{children}</span>
    </OptionWrapper>
  </Option>
);
