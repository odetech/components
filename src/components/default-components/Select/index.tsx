/* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */
import React, { KeyboardEvent } from 'react';
import Select, { components as SelectComponents } from 'react-select';
import { useTheme } from '@storybook/theming';
import { ThemeProps } from '../../../styles/types';
import {
  SelectWrapper, StyledLabel, colorStylesConfig, SelectMenuWrapper,
} from './SelectStyles';

const Menu = (menuProps: any) => (
  <SelectMenuWrapper>
    <SelectComponents.Menu
      {...menuProps}
      className="menu"
    />
  </SelectMenuWrapper>
);

export type SelectProps = {
  label?: string,
  disabled?: boolean,
  name: string,
  id?: string,
  value?: any,
  placeholder?: string,
  margin?: string,
  view?: 'open' | 'closed' | string,
  onChange?: (
    value: any,
    actionMeta: any,
  ) => void,
  onKeyDown?: (e: KeyboardEvent) => void,
  closeMenuOnSelect?: boolean,
  defaultValue?: {
    value: string | number,
    label: string,
  },
  isMulti?: boolean,
  options: {
    value: string | number,
    label: string,
    color?: string,
  }[],
  customColorStylesConfig?: any,
  isClearable?: boolean,
  valueContainerStyles?: any,
  valueControlStyles?: any,
  valueMenuStyles?: any,
  components?: any,
  menuPlacement?: 'auto' | 'bottom' | 'top',
  theme?: ThemeProps
};

export const CustomSelect = ({
  // Default props
  disabled = false,
  label,
  view = 'close',
  margin = '0 0 15px 0',
  closeMenuOnSelect = true,
  isMulti = false,
  defaultValue,
  options,
  customColorStylesConfig,
  onChange,
  value,
  placeholder,
  isClearable = false,
  valueContainerStyles,
  valueControlStyles,
  menuPlacement = 'auto',
  components,
  theme,
  ...props
}: SelectProps) => {
  const globalTheme = useTheme();

  const currentTheme = theme || globalTheme;

  return (
    <SelectWrapper
      margin={margin}
    >
      {label && <StyledLabel>{label}</StyledLabel>}
      <Select
        className="odecloud-select"
        closeMenuOnSelect={closeMenuOnSelect}
        defaultValue={defaultValue}
        isMulti={isMulti}
        options={options}
        styles={customColorStylesConfig ? { ...customColorStylesConfig } : colorStylesConfig(
          view,
          valueContainerStyles,
          valueControlStyles,
          currentTheme,
        )}
        isDisabled={disabled}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
        components={{
          IndicatorSeparator: () => null,
          Menu,
          ...components,
        }}
        isClearable={isClearable}
        menuPlacement={menuPlacement}
        {...props}
      />
    </SelectWrapper>
  );
};

export default CustomSelect;
