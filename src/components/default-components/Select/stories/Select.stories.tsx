import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import Select from '../index';

const meta: Meta<typeof Select> = {
  title: 'Components/Default Components/Select',
  component: Select,
  decorators: [
    (story) => {
      if (document.getElementById('storybook-root')) {
        // @ts-ignore
        document.getElementById('storybook-root').style.width = '100%';
      }
      return (
        <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
          {story()}
        </MemoryRouter>
      );
    },
  ],
  argTypes: {
    label: { control: 'text' },
    disabled: { control: 'boolean' },
  },
  args: {
    label: 'Label',
    disabled: false,
    placeholder: 'Placeholder text',
    options: [
      {
        label: 'One',
        value: 1,
      },
      {
        label: 'Two',
        value: 2,
      },
      {
        label: 'Three',
        value: 3,
      },
    ],
  },
};
export default meta;

export const Default: StoryFn<typeof Select> = (args) => (
  <div
    style={{
      width: '100%',
      maxWidth: 500,
    }}
  >
    <Select
      {...args}
    />
  </div>
);
