import React, { useEffect } from 'react';
import { createEvent, createStore, sample } from 'effector';
import { spread } from 'patronum';
import { useUnit } from 'effector-react';
import { usePopper } from 'react-popper';
import ReactDOM from 'react-dom';
import { Placement } from 'react-overlays/usePopper';
import { Wrapper } from './ui';

const createPopoverFactory = (prefix: string, dependency: string) => {
  const popoverHidden = createEvent<void>();
  const $isPopoverShow = createStore(false);
  const $exceptions = createStore<string[]>([]);
  const $popoverNode = createStore<JSX.Element | null>(null).reset(popoverHidden);
  const $popoverWhere = createStore<Element | null>(null);
  const $extraStyles = createStore<React.CSSProperties>({}).reset(popoverHidden);
  const $placement = createStore<Placement | undefined>('right').reset(popoverHidden);

  const popoverShowed = createEvent<{
    node: JSX.Element | null;
    where: Element | null;
    placement?: Placement | undefined;
    exceptions?: string[];
    extraStyles?: React.CSSProperties;
  }>();
  const popoverUpdate = createEvent<{ where: Element | null, node: JSX.Element | null }>();

  sample({
    clock: popoverShowed,
    fn: ({
      node,
      where,
      placement,
      exceptions,
      extraStyles,
    }) => ({
      isPopoverShow: true,
      popoverNode: node,
      popoverWhere: where,
      placement: placement ?? 'right',
      extraStyles: extraStyles ?? {},
      exceptions: Array.isArray(exceptions) ? exceptions : [],
    }),
    target: spread({
      targets: {
        isPopoverShow: $isPopoverShow,
        popoverNode: $popoverNode,
        popoverWhere: $popoverWhere,
        placement: $placement,
        extraStyles: $extraStyles,
        exceptions: $exceptions,
      },
    }),
  });

  sample({
    clock: popoverHidden,
    fn: () => false,
    target: $isPopoverShow,
  });

  sample({
    clock: popoverUpdate,
    target: spread({
      targets: {
        node: $popoverNode,
        where: $popoverWhere,
      },
    }),
  });

  const observer = new IntersectionObserver((entries: any) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const entry of entries) {
      if (!entry.isIntersecting) popoverHidden();
    }
  });

  const PopoverPortal = () => {
    const element = useUnit($popoverWhere);
    const isPopoverShow = useUnit($isPopoverShow);
    const node = useUnit($popoverNode);
    const placement = useUnit($placement);
    const extraStyles = useUnit($extraStyles);
    const exceptions = useUnit($exceptions);
    const [popperElement, setPopperElement] = React.useState<any>(null);
    const {
      styles,
      attributes,
      forceUpdate,
    } = usePopper(element, popperElement, { placement });

    useEffect(() => {
      if (element) {
        observer.disconnect();
        observer.observe(element);
      }
    }, [element]);

    useEffect(() => {
      const handleClickOutside = (event: any) => {
        const shouldBeCall = exceptions
          .some(
            (exceptionSelector) => document.querySelector(exceptionSelector)?.contains(event.target),
          );
        if (isPopoverShow && !shouldBeCall && !document.getElementById(`${prefix}-popover`)?.contains(event?.target)
          && !document.getElementById(`${dependency}-popover`)?.contains(event?.target)) {
          popoverHidden();
        }
      };
      document.addEventListener('click', handleClickOutside, true);
      return () => {
        document.removeEventListener('click', handleClickOutside, true);
      };
    }, [exceptions, isPopoverShow]);

    useEffect(() => {
      if (isPopoverShow && forceUpdate) {
        forceUpdate();
      }
    }, [isPopoverShow, node]);

    return ReactDOM.createPortal(
      <Wrapper
        ref={setPopperElement}
        id={`${prefix}-popover`}
        data-popover-show={isPopoverShow ? 'true' : 'false'}
        style={{
          ...styles.popper,
          ...extraStyles,
        }}
        {...attributes.popper}
      >
        {node}
      </Wrapper>,
      document.body,
    );
  };

  return {
    $isPopoverShow,
    $popoverWhere,
    $popoverNode,
    popoverHidden,
    popoverShowed,
    popoverUpdate,
    PopoverPortal,
  };
};

// @ts-ignore
const $$popoverMain = createPopoverFactory('main-ode', 'secondary-ode');
// @ts-ignore
const $$popoverSecondary = createPopoverFactory('secondary-ode', 'main-ode');

sample({
  clock: [$$popoverMain.$popoverWhere, $$popoverMain.$isPopoverShow],
  target: $$popoverSecondary.popoverHidden,
});

export {
  $$popoverSecondary,
  $$popoverMain,
  createPopoverFactory,
};
