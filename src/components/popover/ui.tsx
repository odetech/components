import styled from "@emotion/styled";

export const Wrapper = styled("div")`
  &[data-popover-show="false"] {
      z-index: -9999;
  }

  &[data-popover-show="true"] {
      z-index: 9999;
  }
`;
