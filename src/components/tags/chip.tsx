import React from 'react';
import styled from '@emotion/styled';
import { CloseIcon, PencilIcon } from '~/components/icons';
import { defaultTheme } from '../../styles/themes';
import { MORE_TAGS_BUTTON } from './lib';

export const HashtagWrapper = styled('div')<{
  id?: string,
  isSize?:boolean;
}>`
  background-color: ${({ theme }) => theme?.background?.surfaceContainerVar
  || defaultTheme.background.surfaceContainerVar};
  display: flex;
  gap: 8px;
  align-items: center;
  white-space: nowrap;
  justify-content: center;
  padding: 4px 8px;
  border-radius: 4px;

  & div {
    font-size: 12px;
    font-weight: 400;
    text-overflow: ellipsis;
    width: 100%;
    overflow: hidden;
    line-height: 16px;
    text-align: center;
  }

  & > svg {
    min-width: 16px;
    max-height: 16px;
  }

  & button {
    display: flex;
    padding: 0;
    align-items: center;
    justify-content: center;
    background-color: transparent;
    border: none;
    box-shadow: none;  

    & svg {
      width: 8px;
      height: 8px;
    }
  }
  
  & > svg {
    stroke: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    fill: transparent;
  }

  max-width: 140px;
  max-height: 24px;
  width: max-content;  

  ${({ isSize }) => (isSize ? `
    width: 32px;
    max-width: 32px;
    min-width: 32px;
  ` : '')}  
`;

type HashtagChipProps = {
  tag?: string;
  id?: string;
  withIcon?: boolean;
  onClick?: () => void;
  handleRemove?: () => void;
};

export const HashtagChip = ({
  handleRemove, tag, id, withIcon, onClick,
}: HashtagChipProps) => (tag ? (
  <HashtagWrapper id={id} isSize={tag === MORE_TAGS_BUTTON} onClick={onClick}>
    {withIcon && <PencilIcon />}
    <div>
      {tag}
    </div>
    {handleRemove && (
      <button type="button" onClick={handleRemove}>
        <CloseIcon />
      </button>
    )}
  </HashtagWrapper>
) : null);
