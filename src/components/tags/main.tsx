import React, { useEffect, useMemo } from 'react';
import { useUnit } from 'effector-react';
import { $$popoverMain } from '~/components/popover/model';
import styled from '@emotion/styled';
import { useTheme } from '@storybook/theming';
import { defaultTheme } from '../../styles/themes';
import { TagsSelect } from './select';
import { HashtagsView, StyledMini } from './view';

type TOptionBase = { label: string, value: string };

type TagsElementsProps = {
  allTags: TOptionBase[];
  value: TOptionBase[];
  onChange?: (values: TOptionBase[]) => void;
  onChangeTags?: (values: TOptionBase[]) => void;
  taskId: string;
  setCanClose?: (value: boolean) => void;
  close?: () => void;
  canClose?: boolean;
  isActive?: boolean;
  customStyles?: {
    width: number;
    padding: number;
  };
};

export const TagsElements = ({
  allTags,
  value,
  onChange,
  onChangeTags,
  taskId,
  isActive,
  canClose,
  setCanClose,
  close,
  customStyles,
}: TagsElementsProps) => {
  const theme = useTheme();
  const popoverModel = useUnit({
    show: $$popoverMain.popoverShowed,
    update: $$popoverMain.popoverUpdate,
    where: $$popoverMain.$popoverWhere,
    isShow: $$popoverMain.$isPopoverShow,
  });

  const possibleValues = useMemo(() => {
    const allValues = allTags.map((t) => t.value);
    return value.filter((t) => allValues.includes(t.value));
  }, [allTags, value]);

  const onRemove = (val: string) => {
    if (onChangeTags) onChangeTags(allTags.filter((option: any) => option.value !== val));
    if (onChange) onChange(possibleValues.filter((option: any) => option.value !== val));
  };

  const onAdd = (val: string) => {
    const existingOption = allTags.some((option: { value: string }) => option.value === val);
    if (!existingOption && val && onChangeTags && onChange) {
      onChangeTags([...allTags, { label: val, value: val }]);
      onChange([...possibleValues, { label: val, value: val }]);
    }
  };

  const onEdit = (oldValue: string, val: string) => {
    const copyOptions = [...allTags];
    const index = copyOptions.findIndex((p: any) => p.value === oldValue);
    copyOptions[index] = { label: val, value: val };
    if (onChangeTags) onChangeTags(copyOptions);
    if (onChange) {
      const copyValueOptions = [...possibleValues];
      const indexValue = copyValueOptions.findIndex((p: any) => p.value === oldValue);
      copyValueOptions[indexValue] = copyOptions[index];
      onChange(copyValueOptions);
    }
  };

  const showSelect = () => {
    if (onChangeTags && onChange) {
      popoverModel.show({
        placement: 'bottom-start',
        where: document.getElementById(`tags-wrapper-${taskId}-place`),
        extraStyles: {
          padding: '8px',
          borderRadius: '10px',
          border: `1px solid ${theme?.outline?.[300] || defaultTheme.outline[300]}`,
          background: 'white',
          left: '-8px',
          top: '-33px',
        },
        node: (
          <TagsSelect
            options={allTags}
            onMoveItems={onChangeTags}
            onChange={onChange}
            onRemove={onRemove}
            onAdd={onAdd}
            onEdit={onEdit}
            value={possibleValues}
            customStyles={customStyles}
          />
        ),
      });
    }
    if (setCanClose) setCanClose(true);
  };

  useEffect(() => {
    if (isActive) showSelect();
  }, [isActive]);

  useEffect(() => {
    if (!popoverModel.isShow && canClose && close && possibleValues.length === 0) {
      close();
    }
    return () => {
      if (canClose && setCanClose) {
        setCanClose(false);
      }
    };
  }, [popoverModel.isShow, canClose, close]);

  useEffect(() => {
    if (onChangeTags && onChange && popoverModel.where?.id === `tags-wrapper-${taskId}-place`) {
      popoverModel.update({
        node: <TagsSelect
          options={allTags}
          onMoveItems={onChangeTags}
          onChange={onChange}
          onRemove={onRemove}
          onAdd={onAdd}
          onEdit={onEdit}
          value={possibleValues}
          customStyles={customStyles}
        />,
        where: document.getElementById(`tags-wrapper-${taskId}-place`),
      });
      if (setCanClose) setCanClose(true);
    }
  }, [possibleValues, allTags]);

  return (
    <div
      id={`tags-wrapper-${taskId}`}
      style={{
        width: `${customStyles?.width || 295}px`,
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <StyledMini id={`tags-wrapper-${taskId}-place`} />
      {!(isActive && possibleValues.length === 0) && (
        !popoverModel.isShow || popoverModel.where?.id !== `tags-wrapper-${taskId}-place`
      ) && (
        <HashtagsView
          onMouseDown={showSelect}
          readOnly={!(onChangeTags && onChange)}
          tags={possibleValues.map((tag) => tag.label)}
          taskId={taskId}
          customStyles={customStyles}
        />
      )}
    </div>
  );
};
