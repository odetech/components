import React from "react";
import { HashtagChip } from "../chip";

export const MultiValue = ({ children, ...props }: any) => (
  <HashtagChip tag={props.data.value} id={`${props.data.value}-id`} handleRemove={props.removeProps.onClick} />
);
