import Select from 'react-select/creatable';
import React, {
  useEffect, useMemo, useRef, useState,
} from 'react';
import { CSSObject } from '@emotion/react';
import { useUnit } from 'effector-react';
import { $$popoverSecondary } from '~/components/popover/model';
import { usePreviousValue } from '~/hooks/usePreviousValue';
import { NotFoundMessage, SortableOption } from './option';
import { UpdatedList } from './list';
import { MultiValue } from './multiValue';

type TagsSelectProps = {
  options: { label: string, value: string }[];
  onMoveItems?: (values: any) => void;
  onChange: (values: any) => void;
  onAdd?: (values: any) => void;
  unControl?: boolean;
  showIndicaton?: boolean;
  id?: string;
  placeholder?: string;
  isDisabled?: boolean;
  styles?: any;
  onRemove?: (values: any) => void;
  onEdit?: (values: any, val: any) => void;
  value: {label: string; value: string;}[];
  customStyles?: {
    width: number;
    padding: number;
  };
  [key: string]: any;
};

export const TagsSelect = ({
  options, onMoveItems, onChange, value, onRemove, onEdit, placeholder,
  onAdd, customStyles, unControl, showIndicaton, styles, isDisabled, id,
  ...rest
}: TagsSelectProps) => {
  const [val, setVal] = useState('');
  const selectRef = useRef<any>();

  const popoverShowing = useUnit($$popoverSecondary.$isPopoverShow);
  const where = useUnit($$popoverSecondary.$popoverWhere);

  const previousOptions = usePreviousValue(options);

  const [menuIsOpen, setMenuIsOpen] = useState(false);

  useEffect(() => {
    if (!options.length && menuIsOpen && unControl) {
      setMenuIsOpen(false);
    }
  }, [options]);

  useEffect(() => {
    if (isDisabled) {
      setMenuIsOpen(false);
    }
  }, [isDisabled]);

  useEffect(() => {
    if (!popoverShowing && selectRef.current && id && document.getElementById(id)?.contains(where)) {
      selectRef.current.focus();
    }
  }, [popoverShowing]);

  useEffect(() => {
    const isOpen = (unControl ? menuIsOpen : true);
    if (isOpen && options.length < previousOptions.length) {
      selectRef.current.focus();
    }
  }, [options, previousOptions]);

  const onFocus = () => {
    setMenuIsOpen(true);
  };

  const onBlur = () => {
    setMenuIsOpen(popoverShowing || false);
  };

  const handleMouseEnter = () => setMenuIsOpen(true);
  const handleMouseLeave = () => setMenuIsOpen(popoverShowing || false);

  const components = useMemo(() => {
    const basicComponents: any = {
      MenuList: UpdatedList,
      Option: SortableOption,
      MultiValue,
      NoOptionsMessage: NotFoundMessage,
      IndicatorSeparator: null,
      DropdownIndicator: showIndicaton ? undefined : null,
    };
    Object.keys(basicComponents)
      .forEach((key: string) => basicComponents[key] === undefined && delete basicComponents[key]);
    return basicComponents;
  }, [showIndicaton, onAdd]);

  return (
    <Select
      options={options}
      inputValue={val}
      autoFocus={false}
      onInputChange={setVal}
      onBlur={onBlur}
      ref={selectRef}
      onFocus={onFocus}
      // @ts-ignore
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      isClearable={false}
      updated={onMoveItems}
      isSearchable
      hideSelectedOptions={false}
      onRemove={onRemove}
      formatCreateLabel={(inputValue) => {
        const existingOption = options.find((option) => option.label.toLowerCase().includes(inputValue.toLowerCase()));
        const choseOption = value.some((option) => existingOption?.value === option?.value);
        return existingOption?.value && !choseOption ? '--pass-label' : '--create-label';
      }}
      onCreateOption={onAdd}
      onAdd={onAdd}
      noOptionsMessage={() => 'No tags found'}
      menuIsOpen={unControl ? menuIsOpen : true}
      onChange={onChange}
      onEdit={onEdit}
      // @ts-ignore
      onClear={() => setVal('')}
      isMulti
      placeholder={placeholder ?? 'Search/add tag'}
      value={value}
      styles={{
        placeholder(base: CSSObject): CSSObject {
          return {
            ...base,
            fontSize: '12px',
            fontWeight: 400,
            lineHeight: '16px',
            color: '#767776',
          };
        },
        input(base: CSSObject): CSSObject {
          return {
            ...base,
            height: '22px',
          };
        },
        control(base: CSSObject, props: any): CSSObject {
          return {
            ...base,
            borderRadius: '2px',
            padding: '0',
            outline: '0',
            minHeight: '22px',
            outlineOffset: 0,
            border: 0,
            cursor: props.isDisabled ? 'not-allowed' : 'pointer',
            background: 'white',
            boxShadow: 'none',
            '&:hover': {
              outline: '0',
              background: 'white',
              border: 0,
              boxShadow: 'none',
            },
          };
        },
        valueContainer(base: CSSObject): CSSObject {
          return {
            ...base,
            display: 'flex',
            padding: '0px',
            gap: '8px',
          };
        },
        container(base: CSSObject, props: any): CSSObject {
          return {
            ...base,
            width: `${customStyles?.width || 295}px`,
            cursor: props.isDisabled ? 'not-allowed' : 'pointer',
            opacity: props.isDisabled ? 0.5 : 1,
            pointerEvents: 'all',
          };
        },
        menu(base: CSSObject): CSSObject {
          return {
            ...base,
            padding: '0',
            width: `${(customStyles?.width || 295) + 16}px`,
            overflow: 'hidden',
            borderRadius: '8px',
            marginLeft: '-8px',
            marginTop: '12px',
            border: '1px solid #F7942A',
            boxShadow: 'none',
          };
        },
        menuList(base: CSSObject): CSSObject {
          return {
            ...base,
            padding: '0',
            width: `${(customStyles?.width || 295) + 16}px`,
          };
        },
        option: (provided, state) => ({
          ...provided,
          display: 'flex',
          alignItems: 'center',
          opacity: state.isDisabled ? 0.5 : 1,
          visibility: state.isDisabled ? 'hidden' : 'visible',
        }),
        ...styles,
      }}
      // @ts-ignore
      components={components}
      axis="y"
      isDisabled={isDisabled}
      id={id}
      {...rest}
    />
  );
};
