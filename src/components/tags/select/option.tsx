import React, { Fragment, useEffect, useState } from 'react';
import { SortableHandle, SortableElement } from 'react-sortable-hoc';
import {
  DotsIcon, DragIcon, TrashIcon2, CloseIcon,
} from '~/components/icons';
import styled from '@emotion/styled';
import { Button } from '~/components/default-components';
import { useUnit } from 'effector-react';
import { $$popoverSecondary } from '~/components/popover/model';
import { HashtagChip } from '../chip';

const Option = styled('div')<{ isSelected?: boolean, isActive?:boolean }>`
  display: flex;
  align-items: center;
  z-index: 99999;
  padding: 8px 16px 8px 12px;
  gap: 4px;
  cursor: pointer;
  background: ${({ isSelected }) => (isSelected ? '#FEF4EA' : 'white')};

  & > button {
    visibility: ${({ isActive }) => (isActive ? 'visible' : 'hidden')};
  }
    
  &:hover {
    background: #FEF4EA;
    & > button {
      visibility: visible;
    }
  }
    
`;

const OptionButton = styled(Button)<{ isActive: boolean }>`
  margin-left: auto;
  padding: 0;
  background: ${({ isActive }) => (isActive ? '#CECECE' : 'inherit')};
  width: 24px;
  height: 24px;  
    
  svg {
    fill: #3B3C3B;
  }  
  
  &:hover, &:active, &:focus {
    background: ${({ isActive }) => (isActive ? '#CECECE' : 'inherit')};
  } 
`;

const InputWrapper = styled('div')`
  display: flex;
  align-items: center;
  position: relative;
  padding: 10px 20px 0 20px;
    
  button {
    position: absolute;
    right: 30px;
    box-shadow: none;
    background: transparent;
    border: 0;
    width: 20px;
    height: 20px;
    padding: 0;  

    &:hover, &:active, &:focus {
      box-shadow: none;
      background: transparent;
      border: 0;
    }
  }  
`;

const DeleteButton = styled('button')`
  display: flex;
  align-items: center;
  gap: 10px;
  box-shadow: none;
  background: transparent;
  border: 0;
  font-size: 14px;
  font-weight: 500;
  padding: 5px 20px;
  margin-bottom: 10px;  
  line-height: 20px;
  color: #3B3C3B;

  &:hover, &:active, &:focus {
    box-shadow: none;
    background: #FEF4EA;
    border: 0;
  }  
`;

const AddButton = styled('button')`
  box-shadow: none;
  background: transparent;
  border: 0;
  text-align: left;
  width: 100%;

  &:hover, &:active, &:focus {
    box-shadow: none;
    background: transparent;
    border: 0;
  }
`;

const MenuInput = styled('input')`
  border-radius: 10px;
  border: 1px solid #F7942A;
  background: #fff;
  width: 100%;
  padding: 10px 40px 10px 8px;
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: 20px;
  color: #3B3C3B;
`;

const OptionEditorWrapper = styled('div')`
  border-radius: 6px;
  border: 1px solid #E2E2E2;
  display: flex;
  gap: 8px;
  flex-direction: column;
  width: 270px;
  background: #FFF;
  box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.10);
`;

const ErrorStyled = styled('span')<{ errorBottom?: number }>`
  position: absolute;
  font-size: 11px;
  color: #F33822;
  white-space: pre-wrap;
  max-width: fit-content;
  left: 3px;
  bottom: -12px;
`;

type MenuProps = {
  onRemove: (val: any) => void;
  onFinish: (val: any) => void;
  tag: string;
};

// eslint-disable-next-line new-cap
const DragHandle = SortableHandle(() => <span><DragIcon /></span>);

const Menu = ({ onRemove, tag, onFinish }: MenuProps) => {
  const [value, setValue] = useState(tag);
  const [error, setError] = useState(false);

  useEffect(() => {
    setValue(tag);
  }, [tag]);

  const onClear = () => setValue('');

  return (
    <OptionEditorWrapper>
      <InputWrapper>
        <MenuInput
          value={value}
          onChange={(e) => {
            setValue(e.target.value);
            setError(false);
          }}
          onKeyUp={(e) => {
            if (e.key === 'Enter') {
              if (!value) setError(true);
              else onFinish(value);
            }
          }}
        />
        {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
        <button type="button" onClick={onClear}><CloseIcon /></button>
        {error && <ErrorStyled>Tag is required</ErrorStyled>}
      </InputWrapper>
      <DeleteButton type="button" onClick={() => onRemove(tag)}>
        <span><TrashIcon2 /></span>
        <span>Delete</span>
      </DeleteButton>
    </OptionEditorWrapper>
  );
};

// @ts-ignore
// eslint-disable-next-line new-cap
const Item = SortableElement((props) => {
  const popoverModel = useUnit({
    show: $$popoverSecondary.popoverShowed,
    hide: $$popoverSecondary.popoverHidden,
    where: $$popoverSecondary.$popoverWhere,
    isShow: $$popoverSecondary.$isPopoverShow,
  });

  const id = `${props.data.value}-option`.replace(/\s/gi, 'S');
  const toggle = (e: any) => {
    e.stopPropagation();
    if (popoverModel.isShow) {
      popoverModel.hide();
    } else {
      // @ts-ignore
      popoverModel.show({
        placement: 'bottom-start',
        node: (
          <Menu
            onRemove={() => {
              popoverModel.hide();
              props.onRemove(props.data.value);
            }}
            tag={props.data.value}
            onFinish={(val) => {
              popoverModel.hide();
              props.onEdit(props.data.value, val);
            }}
          />
        ),
        where: document.getElementById(id),
        exceptions: [`#${id}`],
      });
    }
  };
  const isActive = popoverModel.isShow && popoverModel.where?.id === id;
  return (
    <Option
      onClick={(e) => {
        e.stopPropagation();
        e.preventDefault();
        props.onClick(props.data);
      }}
      id={id}
      isSelected={props.isSelected}
      isActive={isActive}
    >
      {props.onMove && <DragHandle /> }
      <HashtagChip tag={props.data.value} id={`${props.data.value}-id`} />
      {props.onRemove && props.onAdd && (
        <OptionButton
          type="button"
          isActive={isActive}
          onClick={(e: any) => {
            e.stopPropagation();
            e.preventDefault();
            toggle(e);
          }}
        >
          <DotsIcon height="8px" width="16px" />
        </OptionButton>
      )}
    </Option>
  );
});

type CreateItemProps = {
  onCreate: (val: any) => void;
  inputValue?: string;
};

const CreateItem = ({ onCreate, inputValue }: CreateItemProps) => (
  <Fragment>
    <Option>No tags found</Option>
    <Option isActive>
      <AddButton type="button" onClick={() => onCreate(inputValue)}>Create ?</AddButton>
    </Option>
  </Fragment>
);

export const NotFoundMessage = ({ selectProps }: any) => (
  <CreateItem
    onCreate={(val) => {
      selectProps.onAdd(val);
      selectProps.onClear();
    }}
    inputValue={selectProps.inputValue}
  />
);

export const SortableOption = ({
  data, selectOption, selectProps, options, label, isSelected,
}: any) => {
  const index = options.findIndex((p: any) => p.value === data.value);

  if (label === '--pass-label') {
    return null;
  }
  if (!selectProps.onAdd && label === '--create-label') {
    return <Option>No tags found</Option>;
  }

  if (selectProps.onAdd && label === '--create-label') {
    return (
      <CreateItem
        onCreate={(val) => {
          selectProps.onAdd(val);
          selectProps.onClear();
        }}
        inputValue={selectProps.inputValue}
      />
    );
  }
  return (
    <Item
      // @ts-ignore
      data={data}
      index={index}
      isSelected={isSelected}
      onClick={selectOption}
      onRemove={selectProps.onRemove}
      onMove={selectProps.updated}
      onEdit={selectProps.onEdit}
      onAdd={selectProps.onAdd}
    />
  );
};
