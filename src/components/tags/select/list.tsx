import { SortableContainer } from 'react-sortable-hoc';
import React from 'react';
import { components } from 'react-select';
import { $$popoverSecondary } from '~/components/popover/model';

// eslint-disable-next-line new-cap
const SortableOptionsList = SortableContainer(components.MenuList);

export const UpdatedList = (props: any) => {
  const onSortEnd = ({ oldIndex, newIndex }: any) => {
    // eslint-disable-next-line react/destructuring-assignment
    const copyOptions = [...props.options];
    const element = copyOptions.splice(oldIndex, 1)[0];
    copyOptions.splice(newIndex, 0, element);
    // @ts-ignore
    // eslint-disable-next-line react/destructuring-assignment
    if (props.selectProps.updated) props.selectProps.updated(copyOptions);
  };

  const onSortStart = () => {
    $$popoverSecondary.popoverHidden();
  };

  return (
    <SortableOptionsList onSortEnd={onSortEnd} onSortStart={onSortStart} useDragHandle {...props}>
      {/* eslint-disable-next-line react/destructuring-assignment */}
      {props.children}
    </SortableOptionsList>
  );
};
