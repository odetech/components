import React, {
  useEffect, useRef, useState,
} from 'react';
import styled from '@emotion/styled';
import ReactTooltip from 'react-tooltip';
import { defaultTheme } from '~/styles/themes';
import { HashtagChip } from './chip';
import {
  BETWEEN_TAGS, CONTAINTER_SIZE, COUNT_WIDTH, MORE_TAGS_BUTTON, PADDING, PARENT_WIDTH,
} from './lib';

type HashtagViewProps = {
  tags: string[];
  taskId: string;
  onMouseDown?: any;
  customStyles?: {
    width: number;
    padding: number;
  };
  readOnly: boolean;
  [key: string]: any;
};

const TagsWrapper = styled('div')<{
  isCalc: boolean;
  readOnly: boolean;
  padding: number;
  width: number;
}>`
  display: flex;
  align-items: center;
  gap: 4px;
  width: 100%;  
  max-width: ${({ width }) => width}px;  
  min-height: 24px;  
  padding: 0 ${({ padding }) => padding}px;   
  overflow: hidden;
  cursor: pointer;
  visibility: ${({ isCalc }) => (isCalc ? 'visible' : ' hidden')};
    
  ${({ readOnly, theme }) => (!readOnly ? `
    &:hover {
    outline: 1px solid ${theme?.outline?.[200] || defaultTheme.outline[200]};
    outline-offset: 8px;
    border-radius: 2px;
  }
  ` : '')}
`;

const StyledList = styled('ul')`
  list-style-type: none;
  display: flex;
  flex-direction: column;
  gap: 4px;
`;

export const HashtagsView = ({
  tags, taskId, customStyles, onMouseDown, readOnly,
}: HashtagViewProps) => {
  // inner tags
  const [innerTags, setInnerTags] = useState(tags);

  // for remove glitches
  const [isCalc, setIsCalc] = useState(false);

  // Check possibly width
  const tagsWrapperRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    setTimeout(() => {
      if (tagsWrapperRef?.current) {
        const copyTags = [...tags];
        const wrapper = tagsWrapperRef.current;
        const CURRENT_PADDING = customStyles ? customStyles.padding : PADDING;
        const containerSize = customStyles ? customStyles.width : CONTAINTER_SIZE;
        const parentWidth = customStyles ? (customStyles.width - CURRENT_PADDING) : PARENT_WIDTH;
        const scrollWidth = (wrapper?.scrollWidth || parentWidth) - CURRENT_PADDING;
        const elements = wrapper.children;
        if (scrollWidth > parentWidth) {
          let visibleWidth = 0;
          let elementIndex = 0;
          let lastWidth = 0;
          lastWidth = (elements[elementIndex]?.getBoundingClientRect().width || 0);
          visibleWidth += lastWidth;
          if (elements.length > 1) {
            visibleWidth += BETWEEN_TAGS;
          }
          while (visibleWidth < parentWidth) {
            elementIndex += 1;
            lastWidth = (elements[elementIndex]?.getBoundingClientRect().width || 0);
            visibleWidth += lastWidth + BETWEEN_TAGS;
          }
          // we get cut elementIndex and we backed to previous
          if (visibleWidth < parentWidth) {
            elementIndex -= 1;
          }
          if ((visibleWidth - lastWidth + BETWEEN_TAGS + CURRENT_PADDING + COUNT_WIDTH) > containerSize) {
            elementIndex -= 1;
          }
          copyTags.splice(elementIndex, 0, MORE_TAGS_BUTTON);
          setInnerTags(Array.from(new Set(copyTags.slice(0, elementIndex + 1))));
        }
        setIsCalc(true);
      }
    }, 100);
  }, [tags]);

  return (
    <TagsWrapper
      onMouseDown={!readOnly && onMouseDown}
      readOnly={readOnly}
      ref={tagsWrapperRef}
      isCalc={isCalc}
      width={customStyles ? customStyles.width : PARENT_WIDTH}
      data-for={`${taskId}-hashtags-tooltip`}
      data-tip
      padding={customStyles ? customStyles.padding : PADDING}
    >
      {[...innerTags].map((tag) => (
        <HashtagChip
          tag={
            // eslint-disable-next-line no-nested-ternary
            tag === MORE_TAGS_BUTTON
              ? (tags.length - innerTags.length + 1 ? `+${tags.length - innerTags.length + 1}` : undefined) : tag
          }
          id={`${taskId}-${tag}-id`}
        />
      ))}
      {innerTags.includes(MORE_TAGS_BUTTON) && (tags.length - innerTags.length + 1) && (
        <ReactTooltip id={`${taskId}-hashtags-tooltip`}>
          <StyledList>
            {[...tags].map((tag) => <li key={`${taskId}-${tag}`}>{tag}</li>)}
          </StyledList>
        </ReactTooltip>
      )}
    </TagsWrapper>
  );
};

export const StyledMini = styled('span')`
  width: 1px;
  height: 24px;
`;
