/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { Fragment } from 'react';
import styled from '@emotion/styled';
import { NavLink } from 'react-router-dom';
import { defaultTheme } from '../../../styles/themes';
import { CloudIcon } from './Links/icons';
import {
  OdeTask,
  TimeTracker,
  Conversations,
  Profile,
  OdeSocial,
  Admin,
} from './Links';

type SidebarProps = {
  currentUserId?: string;
  isCurrentUserStaff?: boolean;
  currentUserAppStoresApps?: string[];
  unreadNotificationsTypes?: string[];
  appName: 'odetask' | 'odesocial';
  redirectAppStore?: any;
};

const isOdeSocial = (types: string[] | undefined): boolean => {
  if (!types) return false;
  return types.some((type) => /odesocial-(ae|ac|ab)/.test(type));
};

const isOdeTask = (types: string[] | undefined): boolean => {
  if (!types) return false;
  return types.includes('odetask-ba');
};

const isNewConnection = (types: string[] | undefined): boolean => {
  if (!types) return false;
  return types.includes('odesocial-ad');
};

const isConversations = (types: string[] | undefined): boolean => {
  if (!types) return false;
  return types.some((type) => /odetask-(ae|ac|ab)/.test(type));
};

const isTimeTracker = (types: string[] | undefined): boolean => {
  if (!types) return false;
  return types.includes('odetask-timetracker');
};

const renderLinks: React.FC<SidebarProps> = ({
  currentUserId,
  currentUserAppStoresApps,
  appName,
  unreadNotificationsTypes,
}) => (
  <Fragment>
    <OdeSocial
      currentUserAppStoresApps={currentUserAppStoresApps}
      type={appName}
      showNotificationsBalloon={isOdeSocial(unreadNotificationsTypes)}
    />
    <OdeTask
      type={appName}
      currentUserAppStoresApps={currentUserAppStoresApps}
      showNotificationsBalloon={isOdeTask(unreadNotificationsTypes)}
    />
    <Conversations
      type={appName}
      currentUserAppStoresApps={currentUserAppStoresApps}
      showNotificationsBalloon={isConversations(unreadNotificationsTypes)}
    />
    <TimeTracker
      type={appName}
      currentUserAppStoresApps={currentUserAppStoresApps}
      showNotificationsBalloon={isTimeTracker(unreadNotificationsTypes)}
    />
    <Admin
      type={appName}
      currentUserAppStoresApps={currentUserAppStoresApps}
    />
    <Profile
      type={appName}
      currentUserId={currentUserId}
      currentUserAppStoresApps={currentUserAppStoresApps}
      showNotificationsBalloon={isNewConnection(unreadNotificationsTypes)}
    />
  </Fragment>
);

export const Sidebar: React.FC<SidebarProps> = ({
  currentUserId,
  currentUserAppStoresApps,
  appName,
  redirectAppStore,
  unreadNotificationsTypes,
  isCurrentUserStaff,
}) => (
  <SidebarStyled>
    <SidebarMainLink to="/">
      <CloudIcon />
    </SidebarMainLink>
    <SidebarLinks>
      {renderLinks({
        currentUserId,
        currentUserAppStoresApps,
        appName,
        redirectAppStore,
        unreadNotificationsTypes,
        isCurrentUserStaff,
      })}
    </SidebarLinks>
  </SidebarStyled>
);

const SidebarStyled = styled('div')`
  position: fixed;
  left: 0;
  top: 0;
  bottom: 0;
  transition: width .3s;
  z-index: 6;
  display:flex;
  flex-direction:column;
  align-items:center;  
  padding: 14px 0 14px 14px;
`;

const SidebarMainLink = styled(NavLink)`
  display: inline-flex;
  padding: 9px 6px;
  border-radius: 5px;
  background: ${({ theme }) => theme?.brand?.secondary || defaultTheme.brand.secondary};
  background-size: 1px 200%;
  margin-bottom: 54px;
  transition: background .3s ease-out;
  
  &:hover {
    background-position: 100px;
  }

  svg {
    fill: ${defaultTheme.background.surfaceContainerHigh};
  }
`;

const SidebarLinks = styled('nav')`
  border-radius: 54px;
  transition: background-color .3s;
  position: relative;
  
  &:hover {
    background-color: ${({ theme }) => theme?.brand?.primaryContainer || defaultTheme.brand.primaryContainer};
  }
`;
