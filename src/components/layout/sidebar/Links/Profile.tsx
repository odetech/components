import React from 'react';
import { OdetaskAppStore } from './components/odetask/OdeTaskAppStore';
import { OdeSocialRoute } from './components/odesocial/OdeSocialRoute';
import { ProfileIcon } from './icons';

type ProfileProps = {
  currentUserAppStoresApps?: string[],
  type: 'odetask' | 'odesocial',
  currentUserId?: string,
  showNotificationsBalloon?: boolean,
}

export const Profile: React.FC<ProfileProps> = ({
  type,
  currentUserAppStoresApps,
  currentUserId,
  showNotificationsBalloon,
}) => {
  const title = 'Profile';

  const getCorrectEnvironment = () => {
    const currentLocation = window.location;
    const { origin } = currentLocation;

    if (origin.includes('beta')) {
      return 'https://feed-beta.odecloud.app/';
    }
    return 'https://feed.odecloud.app/';
  };

  if (type === 'odetask') {
    return (
      <OdetaskAppStore
        title={title}
        currentUserAppStoresApps={currentUserAppStoresApps}
        path={`${getCorrectEnvironment()}profile/${currentUserId}`}
        icon={ProfileIcon}
        showNotificationsBalloon={showNotificationsBalloon}
      />
    );
  }
  return (
    <OdeSocialRoute
      title={title}
      path={`/profile/${currentUserId}`}
      icon={ProfileIcon}
      showNotificationsBalloon={showNotificationsBalloon}
    />
  );
};
