import React from 'react';
import { AdminIcon } from './icons';
import { OdeTaskRoute } from './components/odetask/OdeTaskRoute';
import { OdeSocialAppStore } from './components/odesocial/OdeSocialAppStore';

type ConversationsProps = {
  currentUserAppStoresApps?: string[],
  type: 'odetask' | 'odesocial',
  showNotificationsBalloon?: boolean,
}

export const Admin: React.FC<ConversationsProps> = ({
  type,
  currentUserAppStoresApps,
  showNotificationsBalloon,
}) => {
  const title = 'Admin';

  if (type === 'odetask') {
    const activePaths = ['/projects', '/clients'];
    return (
      <OdeTaskRoute
        title={title}
        path="/projects"
        icon={AdminIcon}
        isActivePaths={activePaths}
        showNotificationsBalloon={showNotificationsBalloon}
      />
    );
  }
  return (
    <OdeSocialAppStore
      path="/redirect/odetask?page=projects"
      title={title}
      currentUserAppStoresApps={currentUserAppStoresApps}
      showNotificationsBalloon={showNotificationsBalloon}
      icon={AdminIcon}
    />
  );
};
