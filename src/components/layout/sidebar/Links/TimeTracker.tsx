import React from 'react';
import { OdeTaskRoute } from './components/odetask/OdeTaskRoute';
import { TimeTrackerIcon } from './icons';
import { OdeSocialAppStore } from './components/odesocial/OdeSocialAppStore';

type TimeTrackerProps = {
  type: 'odetask' | 'odesocial',
  currentUserAppStoresApps?: string[],
  showNotificationsBalloon?: boolean,
}

export const TimeTracker: React.FC<TimeTrackerProps> = ({
  type,
  currentUserAppStoresApps,
  showNotificationsBalloon,
}) => {
  const title = 'Time Tracker';

  if (type === 'odetask') {
    return (
      <OdeTaskRoute
        title={title}
        path="/time-tracker"
        icon={TimeTrackerIcon}
        showNotificationsBalloon={showNotificationsBalloon}
      />
    );
  } if (type === 'odesocial') {
    return (
      <OdeSocialAppStore
        path="/redirect/odetask?page=time-tracker"
        title={title}
        currentUserAppStoresApps={currentUserAppStoresApps}
        icon={TimeTrackerIcon}
        showNotificationsBalloon={showNotificationsBalloon}
      />
    );
  }
  return null;
};
