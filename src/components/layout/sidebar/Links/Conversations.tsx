import React from 'react';
import { ConversationsIcon } from './icons';
import { OdeTaskRoute } from './components/odetask/OdeTaskRoute';
import { OdeSocialAppStore } from './components/odesocial/OdeSocialAppStore';

type ConversationsProps = {
  currentUserAppStoresApps?: string[],
  type: 'odetask' | 'odesocial',
  showNotificationsBalloon?: boolean,
}

export const Conversations: React.FC<ConversationsProps> = ({
  type,
  currentUserAppStoresApps,
  showNotificationsBalloon,
}) => {
  const title = 'Conversations';

  if (type === 'odetask') {
    return (
      <OdeTaskRoute
        title={title}
        path="/conversations"
        icon={ConversationsIcon}
        showNotificationsBalloon={showNotificationsBalloon}
      />
    );
  }
  return (
    <OdeSocialAppStore
      path="/redirect/odetask?page=conversations"
      title={title}
      currentUserAppStoresApps={currentUserAppStoresApps}
      showNotificationsBalloon={showNotificationsBalloon}
      icon={ConversationsIcon}
    />
  );
};
