import React from 'react';
import { OdeTaskRoute } from './components/odetask/OdeTaskRoute';
import { DashboardIcon } from './icons';
import { OdeSocialAppStore } from './components/odesocial/OdeSocialAppStore';

type OdeTaskProps = {
  type: 'odetask' | 'odesocial',
  currentUserAppStoresApps?: string[],
  showNotificationsBalloon?: boolean,
}

export const OdeTask: React.FC<OdeTaskProps> = ({
  type,
  currentUserAppStoresApps,
  showNotificationsBalloon,
}) => {
  const title = 'OdeTask';

  if (type === 'odetask') {
    return (
      <OdeTaskRoute
        title={title}
        path="/"
        withQuery
        icon={DashboardIcon}
        showNotificationsBalloon={showNotificationsBalloon}
      />
    );
  }
  return (
    <OdeSocialAppStore
      path="/redirect/odetask"
      title={title}
      currentUserAppStoresApps={currentUserAppStoresApps}
      showNotificationsBalloon={showNotificationsBalloon}
      icon={DashboardIcon}
    />
  );
};
