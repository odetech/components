import React from 'react';
import { OdeSocialRoute } from './OdeSocialRoute';

type OdeTaskRouteProps = {
  title: string,
  path: string,
  icon: React.FC,
  currentUserAppStoresApps?: string[],
  showNotificationsBalloon?: boolean,
}

export const OdeSocialAppStore: React.FC<OdeTaskRouteProps> = ({
  path,
  title,
  icon,
  currentUserAppStoresApps,
  showNotificationsBalloon,
}) => {
  if (currentUserAppStoresApps?.includes('odetask')) {
    return (
      <OdeSocialRoute
        title={title}
        path={path}
        icon={icon}
        showNotificationsBalloon={!!showNotificationsBalloon}
      />
    );
  }
  return null;
};
