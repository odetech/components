import React from 'react';
import { ShowNotificationsBalloon, SidebarRoute } from '../styles';

type OdeSocialRoute = {
  title: string,
  path: string,
  icon: React.FC,
  isActivePaths?: string[],
  showNotificationsBalloon?: boolean,
}

export const OdeSocialRoute: React.FC<OdeSocialRoute> = ({
  title,
  path,
  icon: Icon,
  isActivePaths,
  showNotificationsBalloon,
}) => (
  <SidebarRoute
    key={title}
    to={path}
    exact
    activeClassName="active"
    isActive={(match, location) => (match && match.isExact)
        || (!!isActivePaths && isActivePaths.some((path) => location.pathname.includes(path)))}
  >
    {showNotificationsBalloon
      && <ShowNotificationsBalloon />}
    <Icon />
    <span>{title}</span>
  </SidebarRoute>
);
