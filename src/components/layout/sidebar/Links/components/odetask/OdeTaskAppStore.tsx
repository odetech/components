import React from 'react';
import { ShowNotificationsBalloon, SidebarAppStoreButton } from '../styles';

type OdetaskAppStoreProps = {
  title: string,
  path: string,
  currentUserAppStoresApps?: string[],
  icon: React.FC,
  showNotificationsBalloon?: boolean,
}

export const OdetaskAppStore: React.FC<OdetaskAppStoreProps> = ({
  title,
  path,
  currentUserAppStoresApps,
  icon: Icon,
  showNotificationsBalloon,
}) => {
  if (currentUserAppStoresApps?.includes('odesocial')) {
    return (
      <SidebarAppStoreButton
        key={title}
        href={path}
      >
        {showNotificationsBalloon && <ShowNotificationsBalloon />}
        <Icon />
        <span>{title}</span>
      </SidebarAppStoreButton>
    );
  }
  return null;
};
