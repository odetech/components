import React from 'react';
import { useLocation } from 'react-router-dom';
import { ShowNotificationsBalloon, SidebarRoute } from '../styles';

type OdeTaskRouteProps = {
  title: string,
  path: string,
  icon: React.FC,
  isActivePaths?: string[],
  withQuery?: boolean,
  showNotificationsBalloon?: boolean,
}

export const OdeTaskRoute: React.FC<OdeTaskRouteProps> = ({
  path,
  title,
  icon: Icon,
  withQuery,
  isActivePaths,
  showNotificationsBalloon,
}) => {
  const { search } = useLocation();

  return (
    <SidebarRoute
      key={title}
      to={withQuery ? `${path}${search}` : path}
      exact
      activeClassName="active"
      isActive={(match, location) => (match && match.isExact)
        || (!!isActivePaths && isActivePaths.some((innerPath) => location.pathname.includes(innerPath)))}
    >
      {showNotificationsBalloon
      && <ShowNotificationsBalloon />}
      <Icon />
      <span>{title}</span>
    </SidebarRoute>
  );
};
