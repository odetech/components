import styled from '@emotion/styled';
import { css } from '@emotion/react';
import { NavLink } from 'react-router-dom';
import { defaultTheme } from '../../../../../styles/themes';
import { ThemeProps } from '../../../../../styles/types';

const getSidebarStyles = (theme: ThemeProps) => css`
  background: transparent;
  border-radius: 50%;
  width: 44px;
  height: 44px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: background .3s;
  position: relative;
  line-height: 1.15;
  
  &:not(:last-child) {
    margin-bottom: 30px;
  }
  
  & span {
    pointer-events: none;
  }
  
  &:hover:not([class*="active"]) {
    background: ${theme?.brand?.primaryContainerVar || defaultTheme.brand.primaryContainerVar};
    
    & span {
      opacity: 1;
    }
  }
  
  &:active {
    background: ${theme?.brand?.primaryContainerVar || defaultTheme.brand.primaryContainerVar};
    
    & span {
      background: ${theme?.brand?.primaryContainerVar || defaultTheme.brand.primaryContainerVar};
    }
  }
  
  &:not([class*="active"]) span {
    opacity: 0;
    font-weight: 500;
    color: ${theme?.text?.onSurface || defaultTheme.text.onSurface};
    background: ${theme?.brand?.primaryContainerVar || defaultTheme.brand.primaryContainerVar};
    white-space: nowrap;
    padding: 10px;
    border-radius: 41px;
    position: absolute;
    left: calc(100% + 5px);
    transition: opacity .4s;
  }
  
  &.active {
    background: linear-gradient(90deg, ${theme?.gradient?.orange?.start || defaultTheme.gradient.orange.start} 
     -25%, ${theme?.gradient?.orange?.end || defaultTheme.gradient.orange.end} 122.73%);
    
    & > svg,
    & > svg path {
      fill: white !important;
      z-index: 1;
    }
    
    & span {
      opacity: 0;
      position: absolute;
      left: calc(100% - 44px);
      transition: opacity .4s;
      color: white;
      padding: 14px 10px 14px 60px;
      border-radius: 41px;
      background: linear-gradient(90deg, ${theme?.gradient?.orange?.start || defaultTheme.gradient.orange.start}
       -26.14%, ${theme?.gradient?.orange?.end || defaultTheme.gradient.orange.end} 122.73%);
      white-space: nowrap;
    }
    
    &:hover span {
      opacity: 1;
    }
  }
`;

export const SidebarAppStoreButton = styled('a')`
  ${({ theme }) => getSidebarStyles(theme)}
`;

export const SidebarRoute = styled(NavLink)`
  ${({ theme }) => getSidebarStyles(theme)}
`;

export const ShowNotificationsBalloon = styled('div')`
  position: absolute;
  top: 5px;
  right: 7px;  
  width: 7px;
  height: 7px;
  background-color: ${({ theme }) => theme?.info?.primary || defaultTheme.info.primary};
  border-radius: 50%;
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
`;
