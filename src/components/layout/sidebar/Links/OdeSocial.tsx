import React from 'react';
import { OdetaskAppStore } from './components/odetask/OdeTaskAppStore';
import { OdeSocialRoute } from './components/odesocial/OdeSocialRoute';
import { FeedIcon } from './icons';

type OdesocialProps = {
  currentUserAppStoresApps?: string[],
  type: 'odetask' | 'odesocial',
  showNotificationsBalloon?: boolean,
}

export const OdeSocial: React.FC<OdesocialProps> = ({
  type,
  currentUserAppStoresApps,
  showNotificationsBalloon,
}) => {
  const title = 'OdeSocial';

  const activePaths = ['/articles', '/chat'];

  if (type === 'odetask') {
    return (
      <OdetaskAppStore
        title={title}
        currentUserAppStoresApps={currentUserAppStoresApps}
        path="/redirect/odesocial"
        icon={FeedIcon}
      />
    );
  }
  return (
    <OdeSocialRoute
      title={title}
      path="/"
      isActivePaths={activePaths}
      icon={FeedIcon}
      showNotificationsBalloon={showNotificationsBalloon}
    />
  );
};
