import React from 'react';
import { ComponentStory, Meta } from '@storybook/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { MemoryRouter } from 'react-router-dom';
import { Sidebar } from '../Sidebar';

const store = configureStore({
  reducer: {},
});
export default {
  component: Sidebar,
  decorators: [
    (story) => (
      <Provider store={store}>
        <MemoryRouter initialEntries={['/path/58270ae9-c0ce-42e9-b0f6-f1e6fd924cf7']}>
          {story()}
        </MemoryRouter>
      </Provider>
    ),
  ],
  argTypes: { unreadNotificationsTypes: { control: 'object' } },
  args: { unreadNotificationsTypes: [] },

} as Meta<typeof Sidebar>;

export const OdeTask = () => (
  <Sidebar
    appName="odetask"
    currentUserId="Wj2PKhcSacD3YiHaP"
    redirectAppStore={() => console.log(123)}
    currentUserAppStoresApps={['odetask', 'odesocial']}
  />
);

export const OdeSocial: ComponentStory<typeof Sidebar & any> = (args) => (
  <Sidebar
    appName="odesocial"
    currentUserId="Wj2PKhcSacD3YiHaP"
    redirectAppStore={() => console.log(123)}
    currentUserAppStoresApps={['odetask', 'odesocial']}
    isCurrentUserStaff
    {...args}
  />
);
