import React, { ReactNode } from 'react';
import { Field } from 'formik';
import { FieldWrapper, FormikErrorTextStyle, StyledLabel } from './FormikFieldStyles';

type FormikFieldProps = {
  field: any,
  children: ReactNode,
  errorText?: string,
  errorPlacement?: string,
  disabled?: boolean,
  label?: string,
  margin?: string,
  id?: string,
  wrapperId?: string,
  unclickableFormikField?: boolean,
}

const FormikField = ({
  errorText,
  disabled = false,
  field = null,
  label,
  margin = '0 0 10px 0',
  children,
  id,
  wrapperId,
  unclickableFormikField,
}: FormikFieldProps) => (
  <FieldWrapper
    disabled={disabled}
    margin={margin}
    htmlFor={id}
    id={wrapperId}
    unclickableFormikField={unclickableFormikField}
  >
    {label && <StyledLabel id={id}>{label}</StyledLabel>}
    <Field {...field}>
      {children}
    </Field>
    {errorText && <FormikErrorTextStyle>{errorText}</FormikErrorTextStyle>}
  </FieldWrapper>
);

export default FormikField;
