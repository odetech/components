import styled from '@emotion/styled';
import { transparentize } from 'polished';
import isPropValid from '@emotion/is-prop-valid';
import { defaultTheme } from '../../../styles/themes';

export const FormikErrorTextStyle = styled('div')`
  font-size: 11px;
  color: ${({ theme }) => theme?.error?.primary || defaultTheme.error.primary};  
  white-space: pre-wrap;
  max-width: fit-content;
`;

type FieldWrapperProps = {
  disabled: boolean,
  margin: string,
  unclickableFormikField?: boolean,
};

export const FieldWrapper = styled('label', {
  shouldForwardProp: isPropValid,
})<FieldWrapperProps>`
  width: 100%;
  opacity: ${({ disabled }) => (disabled ? '0.6' : '1')};
  align-items: center;
  margin: ${({ margin }) => (margin)};
  cursor: ${({ disabled, unclickableFormikField }) => {
    if (disabled) {
      return 'not-allowed';
    } if (unclickableFormikField) {
      return 'default';
    }
    return 'pointer';
  }};

  &:hover {
    p {
      color: ${({ theme }) => transparentize(0.5, theme?.text?.onSecondary || defaultTheme.text.onSecondary)};      
    }
  }

  &:active, &:focus {
    p {
      color: ${({ theme }) => theme?.text?.onSecondary || defaultTheme.text.onSecondary};          
    }
  }

  &:disabled {
    color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};
  }
`;

export const StyledLabel = styled('p')`
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  margin: 0 0 4px 0;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};  
`;
