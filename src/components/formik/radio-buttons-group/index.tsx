/* eslint-disable react/jsx-no-useless-fragment */
import React, { ChangeEvent } from 'react';
import { RadioButtonsWrapper } from './FormikRadioButtonsGroupStyles';
import RadioButton from '../../default-components/RadioButton';
import FormikField from '../field';

type FormikRadioButtonsGroupProps = {
  disabled?: boolean,
  name: string,
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void,
  margin?: string,
  errorText?: string,
  radioButtons: {
    label: string,
    id: string,
    value: string,
  }[],
  unclickableFormikField?: boolean,
  id?: string,
  wrapperId?: string,
  label?: string,
}

const FormikRadioButtonsGroup = ({
  disabled = false,
  errorText,
  margin,
  onChange,
  radioButtons,
  unclickableFormikField,
  id,
  wrapperId,
  label,
  ...otherProps
}: FormikRadioButtonsGroupProps) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    margin={margin}
    unclickableFormikField={unclickableFormikField}
    id={id}
    wrapperId={wrapperId}
    label={label}
  >
    {({ field, form }: {
      field: {
        name: string,
        value: any,
        onChange: (e: ChangeEvent<HTMLInputElement>) => void,
        onBlur: () => void,
      },
      form: any,
    }) => (
      <RadioButtonsWrapper>
        {radioButtons.map((radioButtonData, index) => (
          <RadioButton
            {...otherProps}
            /* eslint-disable-next-line react/no-array-index-key */
            key={`${field.name}-${index}`}
            id={radioButtonData.id}
            checked={radioButtonData.value === field.value}
            value={radioButtonData.value}
            label={radioButtonData.label}
            onChange={(e) => {
              form.setFieldValue(field.name, e.target.value);
              if (onChange) {
                onChange(e);
              }
            }}
          />
        ))}
      </RadioButtonsWrapper>
    )}
  </FormikField>
);

export default FormikRadioButtonsGroup;
