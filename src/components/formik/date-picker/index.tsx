import React from 'react';
import { DatePicker } from '../../default-components';
import FormikField from '../field';

type FormikDatePickerProps = {
  name: string,
  label?: string,
  errorText?: any,
  disabled?: boolean,
  showPopperArrow?: boolean,
  placeholder?: string,
  margin?: string,
  view?: string,
  minDate?: Date;
  minTime?: Date;
  maxTime?: Date;
  maxDate?: Date;
  filterTime?: (date: Date) => boolean;
  showTimeSelect?: boolean;
  showIcon?: boolean;
  dateFormat?: string;
}

const FormikDatePicker = ({
  label,
  errorText,
  disabled,
  margin,
  ...otherProps
}: FormikDatePickerProps) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }: {
      field: {
        name: string,
        value: any,
      },
      form: any,
    }) => (
      <DatePicker
        {...otherProps}
        {...field}
        startDate={field.value}
        disabled={disabled}
        onChange={(date: Date) => {
          form.setFieldValue(field.name, date);
        }}
      />
    )}
  </FormikField>
);

export default FormikDatePicker;
