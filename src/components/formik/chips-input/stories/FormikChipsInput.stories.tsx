import React from 'react';
import type { Meta, StoryFn } from '@storybook/react';
import { Form, Formik } from 'formik';
import FormikChipsInput from '../index';

const meta: Meta<typeof FormikChipsInput> = {
  title: 'Components/Formik/ChipsInput',
  component: FormikChipsInput,
  argTypes: {
  },
  args: {
  },
};

export default meta;

export const Default: StoryFn<typeof FormikChipsInput> = () => (
  <Formik
    initialValues={{
      someArray: [{
        bulletPoints: ['111', '222', '333'],
      }],
    }}
    onSubmit={(values) => {
      console.log('values', values);
    }}
    enableReinitialize
    validateOnBlur={false}
    validateOnChange
    validateOnMount={false}
  >
    {({
      values,
    }) => (
      <Form
        autoComplete="off"
        onKeyDown={(e) => {
          if (e.keyCode === 13) {
            e.preventDefault();
          }
        }}
      >
        {values.someArray.map((_, index) => (
          <FormikChipsInput
            name={`someArray.${index}.bulletInput`}
            arrayName={`someArray.${index}.bulletPoints`}
            placeholder="Add chips"
            label="Chips input label"
            id={`someArray.${index}.bulletInput`}
            wrapperId={`someArray.${index}.bulletPoints`}
            handleKeyDownItems={['Enter', 'Tab', ',']}
          />
        ))}
      </Form>
    )}
  </Formik>
);
