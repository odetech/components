import React from 'react';
import styled from '@emotion/styled';
import { defaultTheme } from '../../../styles/themes';

export const ChipsInputWrapper = styled(
  (props: any) => (
    <div
      {...props}
      className={`${props.className} odecloud-chips-input-wrapper`}
    />
  ),
)`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  border: 1px solid ${({ theme }) => theme?.outline?.[100] || defaultTheme.outline[100]};
  padding: 14px 16px 8px 16px;
  border-radius: 7px;

  input {
    border: unset;
    background: transparent;
    padding: 0;
    
    &:hover, &:active, &:focus {
      background: transparent !important;
    }
  }

  button {
    svg {
      transform: rotate(45deg);
      fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    }
  }
`;

export const Chip = styled(
  (props: any) => (
    <div
      {...props}
      className={`${props.className} odecloud-chip-wrapper`}
    />
  ),
)`
  display: flex;
  align-items: center;
  background: ${({ theme }) => theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar};
  font-size: 14px;
  line-height: 21px;
  border-radius: 14px;
  padding: 5px 10px;
  margin: 0 10px 5px 0;
  max-width: 250px;

  span {
    margin: 0 5px 0 0;
    overflow: hidden;
    text-overflow: ellipsis;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }
  
  button {
    border: unset;
    background: transparent;
    padding: 0;
  }
`;

export const InputWrapper = styled('div')`
  width: unset !important;
`;
