import React, { KeyboardEvent, useEffect, useState } from 'react';
import { getDeepValue } from '~/helpers';
import { Input } from '../../default-components';
import FormikField from '../field';
import { PlusIcon } from '../../icons';
import { Chip, ChipsInputWrapper, InputWrapper } from './FormikChipsInputStyles';

type FormikChipsInputProps = {
  name: string,
  arrayName: string,
  id: string,
  label?: string,
  errorText?: string,
  disabled?: boolean,
  type?: string,
  placeholder?: string,
  margin?: string,
  view?: string,
  wrapperId?: string,
  handleKeyDownItems?: string[],
};

const FormikChipsInput = ({
  label,
  errorText,
  disabled,
  type = 'text',
  margin,
  arrayName,
  id,
  wrapperId,
  handleKeyDownItems,
  ...otherProps
}: FormikChipsInputProps) => {
  const [isPasted, setIsPasted] = useState<boolean>(false);

  useEffect(() => {
    window.addEventListener('paste', (event: any) => {
      if (event.target?.getAttribute('id') === id) {
        setIsPasted(true);
      }
    });
  }, []);
  return (
    <FormikField
      field={otherProps}
      errorText={errorText}
      disabled={disabled}
      label={label}
      margin={margin}
      id={id}
      wrapperId={wrapperId}
    >
      {({ field, form }: {
        field: {
          name: string,
          arrayName: string,
          value: any,
          onChange: () => void,
          onBlur: () => void,
        },
        form: any,
      }) => {
        const arrayValue = getDeepValue(form.values, arrayName);

        const handleKeyDown = (e: KeyboardEvent) => {
          const keys = handleKeyDownItems || ['Enter', 'Tab', ' ', ','];
          if (keys.includes(e.key)) {
            e.preventDefault();
            const value = field.value.trim();
            if (value && !errorText) {
              form.setFieldValue(field.name, '');
              form.setFieldValue(arrayName, [...arrayValue, value]);
            }
          }
        };

        const handleDelete = (item: string) => {
          const updatedItemsArray = arrayValue.filter((i: string) => i !== item);
          form.setFieldValue(arrayName, updatedItemsArray);
        };

        if (isPasted && field.value) {
          const value = field.value.trim();
          form.setFieldValue(arrayName, [...arrayValue, value]);
          form.setFieldValue(field.name, '');
          setIsPasted(false);
        }

        return (
          <ChipsInputWrapper>
            {arrayValue.map((item: string) => (
              <Chip
                key={item}
              >
                <span>
                  {item}
                </span>
                <button
                  type="button"
                  className="button"
                  onClick={(e) => {
                    e.preventDefault();
                    handleDelete(item);
                  }}
                >
                  <PlusIcon />
                </button>
              </Chip>
            ))}
            <InputWrapper>
              <Input
                {...otherProps}
                {...field}
                id={id}
                disabled={disabled}
                type={type}
                onKeyDown={handleKeyDown}
                onChange={(e) => {
                  let { value }: { value: string | number } = e.target;
                  if (type === 'number') {
                    if (value === '') {
                      form.setFieldValue(field.name, value);
                      return;
                    }
                    const number = Number(value);
                    if (!Number.isNaN(number)) {
                      value = number;
                    }
                  }
                  form.setFieldValue(field.name, value);
                }}
              />
            </InputWrapper>
          </ChipsInputWrapper>
        );
      }}
    </FormikField>
  );
};

export default FormikChipsInput;
