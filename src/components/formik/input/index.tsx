import React from 'react';
import { Input } from '../../default-components';
import FormikField from '../field';

type FormikInputProps = {
  name: string,
  id?: string,
  label?: string,
  errorText?: string,
  disabled?: boolean,
  type?: string,
  placeholder?: string,
  margin?: string,
  view?: string,
  autoComplete?: string,
};

const FormikInput = ({
  label,
  errorText,
  disabled,
  type = 'text',
  margin,
  autoComplete,
  ...otherProps
}: FormikInputProps) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }: {
        field: {
          name: string,
          value: any,
          onChange: () => void,
          onBlur: () => void,
        },
        form: any,
      }) => (
        <Input
          {...otherProps}
          {...field}
          disabled={disabled}
          type={type}
          onChange={(e) => {
            let { value }: { value: string | number } = e.target;
            if (type === 'number') {
              if (value === '') {
                form.setFieldValue(field.name, value);
                return;
              }
              const number = Number(value);
              if (!Number.isNaN(number)) {
                value = number;
              }
            }
            form.setFieldValue(field.name, value);
          }}
          autoComplete={autoComplete}
        />
    )}
  </FormikField>
);

export default FormikInput;
