import React, { KeyboardEvent } from 'react';
import { ThemeProps } from '~/styles/types';
import { Select } from '../../default-components';
import FormikField from '../field';

type FormikSelectProps = {
  name: string,
  id?: string,
  label?: string,
  errorText?: string,
  disabled?: boolean,
  placeholder?: string,
  margin?: string,
  view?: string,
  onChange?: () => void,
  onKeyDown?: (e: KeyboardEvent) => void,
  isMulti?: boolean,
  closeMenuOnSelect?: boolean,
  defaultValue?: {
    value: string | number,
    label: string,
  },
  options: {
    value: string | number,
    label: string,
    color?: string,
  }[],
  customColorStylesConfig?: any,
  menuPlacement?: 'auto' | 'bottom' | 'top',
  theme?: ThemeProps
}

const FormikSelect = ({
  label,
  errorText,
  disabled,
  margin,
  onChange,
  theme,
  ...otherProps
}: FormikSelectProps) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }: {
      field: {
        name: string,
        value: any,
        onChange: () => void,
        onBlur: () => void,
      },
      form: any,
    }) => (
      <Select
        {...otherProps}
        {...field}
        margin="0"
        onChange={(data) => {
          form.setFieldValue(field.name, data);
          if (onChange) {
            onChange();
          }
        }}
        disabled={disabled}
        theme={theme}
      />
    )}
  </FormikField>
);

export default FormikSelect;
