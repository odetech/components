import React from 'react';
import { Textarea } from '../../default-components';
import FormikField from '../field';

type FormikInputProps = {
  name: string,
  label?: string,
  errorText?: string,
  disabled?: boolean,
  placeholder?: string,
  margin?: string,
  view?: string,
  minHeight?: string,
}

const Index = ({
  label,
  errorText,
  disabled,
  margin,
  ...otherProps
}: FormikInputProps) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }: {
      field: {
        name: string,
        value: any,
        onChange: () => void,
        onBlur: () => void,
      },
      form: any,
    }) => (
      <Textarea
        {...otherProps}
        {...field}
        disabled={disabled}
        onChange={(e) => {
          const { value }: { value: string } = e.target;
          form.setFieldValue(field.name, value);
        }}
      />
    )}
  </FormikField>
);

export default Index;
