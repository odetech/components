import React from 'react';
import { InputWithHiddenInfo } from '../../default-components';
import FormikField from '../field';

type FormikInputWithHiddenInfoProps = {
  name: string,
  id?: string,
  label?: string,
  errorText?: string,
  disabled?: boolean,
  type?: string,
  placeholder?: string,
  margin?: string,
  view?: string,
  onEyeButtonClick?: (e: any) => void;
  autoComplete?: string,
};

const FormikInputWithHiddenInfo = ({
  label,
  errorText,
  disabled,
  type = 'text',
  margin,
  onEyeButtonClick,
  autoComplete,
  ...otherProps
}: FormikInputWithHiddenInfoProps) => (
  <FormikField
    field={otherProps}
    errorText={errorText}
    disabled={disabled}
    label={label}
    margin={margin}
  >
    {({ field, form }: {
      field: {
        name: string,
        value: any,
        onChange: () => void,
        onBlur: () => void,
      },
      form: any,
    }) => (
      <InputWithHiddenInfo
        {...otherProps}
        {...field}
        disabled={disabled}
        type={type}
        onChange={(e) => {
          let { value }: { value: string | number } = e.target;
          if (type === 'number') {
            if (value === '') {
              form.setFieldValue(field.name, value);
              return;
            }
            const number = Number(value);
            if (!Number.isNaN(number)) {
              value = number;
            }
          }
          form.setFieldValue(field.name, value);
        }}
        onEyeButtonClick={onEyeButtonClick}
        autoComplete={autoComplete}
      />
    )}
  </FormikField>
);
export default FormikInputWithHiddenInfo;
