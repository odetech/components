import styled from '@emotion/styled';
import isPropValid from '@emotion/is-prop-valid';

type ToggleBlockWrapProps = {
  margin?: string,
}

export const ToggleBlockWrap = styled('div', {
  shouldForwardProp: isPropValid,
})<ToggleBlockWrapProps>`
  display: flex;
  align-items: center;
  ${(props) => (props.margin && `margin: ${props.margin};`)};
`;

type ToggleBlockWrapLabelProps = {
  largeFontSizeLabel?: string,
}

export const ToggleBlockWrapLabel = styled('p', {
  shouldForwardProp: isPropValid,
})<ToggleBlockWrapLabelProps>`
  margin-bottom: 0;
  margin-right: 20px;
  
  ${(props) => (props.largeFontSizeLabel && `
    font-size: 18px;
    line-heihgt: 22px;
  `)}
`;
