import styled from '@emotion/styled';
import { defaultTheme } from '../../styles/themes';

type AvatarWrapProps = {
  size: number,
  isSquare: boolean,
};

export const AvatarWrap = styled('div')<AvatarWrapProps>`
  width: ${({ size }) => size}px;
  min-width: ${({ size }) => size}px;
  height: ${({ size }) => (size ? `${size}px` : '15px')};
  border-radius: ${({ isSquare }) => (isSquare ? '6px' : '50%')};
  overflow: hidden;
  flex-shrink: 1;
  position: relative;
`;

export const AvatarImage = styled('img')`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
`;

type AvatarPlaceholderProps = {
  fontSize?: number,
  isSquare?: boolean,
};

export const AvatarPlaceholder = styled('div')<AvatarPlaceholderProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${({ fontSize }) => (fontSize ? `${fontSize}px` : '15px')};
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => theme?.background?.surfaceContainerVar
    || defaultTheme.background.surfaceContainerVar};
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};  
  border-radius: ${({ isSquare }) => (isSquare ? '6px' : '50%')};
  border: 1px solid ${({ theme }) => theme?.outline?.[200] || defaultTheme.outline[200]};
`;
