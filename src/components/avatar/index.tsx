import React from 'react';
import {
  AvatarWrap, AvatarImage, AvatarPlaceholder,
} from './AvatarStyles';

type AvatarProps = {
  size: number,
  user: any,
  avatarPlaceholderFontSize?: number,
  isSquare?: boolean,
};

export const Avatar = ({
  user, size, isSquare, avatarPlaceholderFontSize,
}: AvatarProps) => {
  if (!user?.profile) return null;

  const { firstName, lastName, avatar } = user.profile;

  return (
    <AvatarWrap size={size} isSquare={!!isSquare}>
      {avatar?.secureUrl ? (
        <AvatarImage
          src={avatar.secureUrl}
        />
      ) : (
        <AvatarPlaceholder
          isSquare={!!isSquare}
          fontSize={avatarPlaceholderFontSize}
        >
          {firstName.charAt(0)}
          {lastName.charAt(0)}
        </AvatarPlaceholder>
      )}
    </AvatarWrap>
  );
};
