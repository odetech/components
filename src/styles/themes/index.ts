import { rgba } from 'polished';
import { ThemeProps, Themes } from '../types';
import { Colors } from '../palette/index';

export const defaultTheme: ThemeProps = {
  brand: {
    primary: Colors.primary[500],
    primaryContainer: Colors.primary[100],
    primaryContainerVar: rgba(Colors.primary[500], 0.1),
    secondary: Colors.secondary[500],
    secondaryContainer: Colors.secondary[100],
    secondaryContainerSecond: Colors.secondary[200],
  },
  text: {
    onSurface: Colors.grey[800],
    onSurfaceVar: Colors.grey[500],
    onSurfaceVarSecond: Colors.grey[600],
    onSurfaceDisabled: Colors.grey[400],
    onPrimary: Colors.grey[0],
    onSecondary: Colors.primary[500],
  },
  background: {
    surface: Colors.grey[50],
    surfaceContainer: Colors.grey[0],
    surfaceContainerVar: Colors.grey[100],
    surfaceContainerHigh: Colors.grey[0],
    surfaceContainerHighest: Colors.grey[0],
  },
  success: {
    primary: Colors.green[500],
    onPrimary: Colors.grey[0],
    primaryContainer: rgba(Colors.green[500], 0.1),
    onPrimaryContainer: Colors.green[500],
  },
  error: {
    primary: Colors.red[300],
    onPrimary: Colors.grey[0],
    primaryContainer: rgba(Colors.red[500], 0.1),
    onPrimaryContainer: Colors.red[300],
  },
  info: {
    primary: Colors.blue[500],
    onPrimary: Colors.grey[0],
    primaryContainer: rgba(Colors.blue[500], 0.1),
    onPrimaryContainer: Colors.blue[500],
  },
  outline: {
    100: Colors.grey[200],
    200: Colors.grey[300],
    300: Colors.grey[500],
  },
  visualisation: {
    white: Colors.hue.white[0],
    black: Colors.hue.black[0],
    purple: Colors.hue.purple[0],
    yellow: Colors.hue.yellow[0],
    warmRed: Colors.hue.warmRed[0],
    blueSky: Colors.hue.blueSky[0],
    gray: Colors.hue.gray[0],
    pink: Colors.hue.pink[2],
    turquoiseGreen: Colors.hue.turquoiseGreen[0],
    brown: Colors.hue.brown[0],
    raspberry: Colors.hue.raspberry[0],
    diodeBlue: Colors.hue.diodeBlue[0],
    greenGrass: Colors.hue.greenGrass[0],
    terracotta: Colors.hue.terracotta[0],
    lilac: Colors.hue.lilac[0],
    teal: Colors.hue.teal[0],
    cherry: Colors.hue.cherry[0],
  },
  gradient: {
    orange: {
      start: Colors.hue.orange[2],
      end: Colors.hue.orange[3],
    },
    purple: {
      start: Colors.hue.purple[3],
      end: Colors.hue.purple[4],
    },
    green: {
      start: Colors.hue.green[2],
      end: Colors.hue.green[3],
    },
    pink: {
      start: Colors.hue.pink[0],
      end: Colors.hue.pink[1],
    },
    blue: {
      start: Colors.hue.blue[0],
      end: Colors.hue.blue[1],
    },
  },
  shadow: {
    drop: {
      1: `0px 16px 4px 0px ${rgba(Colors.hue.black[0], 0)}, 
        0px 10px 4px 0px ${rgba(Colors.hue.black[0], 0)},
        0px 6px 3px 0px ${rgba(Colors.hue.black[0], 0.02)}, 
        0px 3px 3px 0px ${rgba(Colors.hue.black[0], 0.03)}, 
        0px 1px 1px 0px ${rgba(Colors.hue.black[0], 0.03)}`,
      2: `0px 30px 8px 0px ${rgba(Colors.hue.black[0], 0)}, 
        0px 19px 8px 0px ${rgba(Colors.hue.black[0], 0.01)},
        0px 11px 6px 0px ${rgba(Colors.hue.black[0], 0.03)}, 
        0px 1px 3px 0px ${rgba(Colors.hue.black[0], 0.05)}, 
        0px 5px 5px 0px ${rgba(Colors.hue.black[0], 0.04)}`,
      3: `0px 89px 25px 0px ${rgba(Colors.hue.black[0], 0)}, 
        0px 57px 23px 0px ${rgba(Colors.hue.black[0], 0)},
        0px 14px 14px 0px ${rgba(Colors.hue.black[0], 0.03)}, 
        0px 32px 19px 0px ${rgba(Colors.hue.black[0], 0.02)}, 
        0px 4px 8px 0px ${rgba(Colors.hue.black[0], 0.03)}`,
    },
    inner: {
      1: `0px 32px 13px 0px ${rgba(Colors.hue.black[0], 0.01)} inset,
        0px 2px 4px 0px ${rgba(Colors.hue.black[0], 0.05)} inset, 
        0px 49px 14px 0px ${rgba(Colors.hue.black[0], 0)} inset, 
        0px 18px 11px 0px ${rgba(Colors.hue.black[0], 0.03)} inset, 
        0px 8px 8px 0px ${rgba(Colors.hue.black[0], 0.04)} inset`,
    },
  },
};

export const darkTheme: ThemeProps = {
  brand: {
    primary: Colors.primary[500],
    primaryContainer: rgba(Colors.hue.black[0], 0.8),
    primaryContainerVar: rgba(Colors.primary[500], 0.1),
    secondary: Colors.secondary[400],
    secondaryContainer: rgba(Colors.hue.white[0], 0.1),
    secondaryContainerSecond: rgba(Colors.hue.black[0], 0.7),
  },
  text: {
    onSurface: Colors.grey[200],
    onSurfaceVar: Colors.grey[400],
    onSurfaceVarSecond: Colors.grey[200],
    onSurfaceDisabled: Colors.grey[600],
    onPrimary: Colors.grey[900],
    onSecondary: Colors.primary[500],
  },
  background: {
    surface: Colors.grey[900],
    surfaceContainer: Colors.grey[850],
    surfaceContainerVar: rgba(Colors.hue.white[0], 0.1),
    surfaceContainerHigh: Colors.grey[850],
    surfaceContainerHighest: Colors.grey[850],
  },
  success: {
    primary: Colors.green[400],
    onPrimary: Colors.grey[900],
    primaryContainer: rgba(Colors.green[400], 0.1),
    onPrimaryContainer: Colors.grey[900],
  },
  error: {
    primary: Colors.red[300],
    onPrimary: Colors.grey[900],
    primaryContainer: rgba(Colors.red[300], 0.1),
    onPrimaryContainer: Colors.grey[900],
  },
  info: {
    primary: Colors.blue[300],
    onPrimary: Colors.grey[900],
    primaryContainer: rgba(Colors.blue[300], 0.1),
    onPrimaryContainer: Colors.grey[900],
  },
  outline: {
    100: Colors.grey[800],
    200: Colors.grey[700],
    300: Colors.grey[600],
  },
  visualisation: {
    white: Colors.hue.white[0],
    black: Colors.hue.black[0],
    purple: Colors.hue.purple[1],
    yellow: Colors.hue.yellow[0],
    warmRed: Colors.hue.warmRed[1],
    blueSky: Colors.hue.blueSky[1],
    gray: Colors.hue.gray[1],
    pink: Colors.hue.pink[3],
    turquoiseGreen: Colors.hue.turquoiseGreen[1],
    brown: Colors.hue.brown[1],
    raspberry: Colors.hue.raspberry[0],
    diodeBlue: Colors.hue.diodeBlue[1],
    greenGrass: Colors.hue.greenGrass[1],
    terracotta: Colors.hue.terracotta[1],
    lilac: Colors.hue.lilac[1],
    teal: Colors.hue.teal[1],
    cherry: Colors.hue.cherry[1],
  },
  gradient: {
    orange: {
      start: Colors.hue.orange[0],
      end: Colors.hue.orange[1],
    },
    purple: {
      start: Colors.hue.purple[2],
      end: Colors.hue.purple[5],
    },
    green: {
      start: Colors.hue.green[0],
      end: Colors.hue.green[1],
    },
    pink: {
      start: Colors.hue.pink[4],
      end: Colors.hue.pink[5],
    },
    blue: {
      start: Colors.hue.blue[2],
      end: Colors.hue.blue[3],
    },
  },
  shadow: {
    drop: {
      1: '',
      2: '',
      3: '',
    },
    inner: {
      1: '',
    },
  },
};

export const themes: Themes = {
  light: defaultTheme,
  dark: darkTheme,
};
