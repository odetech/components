import { ThemeColors } from '../types';

export const Colors: ThemeColors = {
  blue: {
    900: '#005389',
    800: '#006DB0',
    700: '#007CC4',
    600: '#0088D7',
    500: '#0095E9',
    400: '#3EA4FB',
    300: '#6AB2FF',
    200: '#9AC9FF',
    100: '#E5F4FD',
    50: '#F8FCFF',
  },
  green: {
    900: '#0C6F00',
    800: '#238E11',
    700: '#329E1E',
    600: '#41B22A',
    500: '#4BC133',
    400: '#67CD53',
    300: '#7FDA6F',
    200: '#A3EA97',
    100: '#C6FDBD',
    50: '#E2FFDE',
  },
  red: {
    900: '#BA0000',
    800: '#CA0000',
    700: '#D70003',
    600: '#E90006',
    500: '#F80000',
    400: '#F8292B',
    300: '#F25354',
    200: '#FF7D7B',
    100: '#FEE5E5',
    50: '#FFF8F8',
  },
  primary: {
    900: '#875C00',
    800: '#B27100',
    700: '#CA7E00',
    600: '#E38A12',
    500: '#F7942A',
    400: '#F9A955',
    300: '#FCD4AA',
    200: '#FDEAD4',
    100: '#FEF4EA',
    50: '#FFF9F4',
  },
  secondary: {
    900: '#A71454',
    800: '#BE245A',
    700: '#CB3160',
    600: '#DA3B64',
    500: '#E54367',
    400: '#F0637B',
    300: '#FA8899',
    200: '#FFB7BF',
    100: '#FCECF0',
    50: '#FEFBFC',
  },
  grey: {
    900: '#191919',
    850: '#202020',
    800: '#3B3C3B',
    700: '#626362',
    600: '#767776',
    500: '#9C9C9C',
    400: '#B1B1B1',
    300: '#CECECE',
    200: '#E5E5E5',
    100: '#F0F0F0',
    50: '#FAFAFA',
    0: '#FFFFFF',
  },
  hue: {
    orange: {
      0: '#F45241',
      1: '#F7BF47',
      2: '#F12711',
      3: '#F5AF19',
    },
    green: {
      0: '#3AB55D',
      1: '#5AD83B',
      2: '#31CE0A',
      3: '#09A334',
    },
    pink: {
      0: '#F857A6',
      1: '#FF5858',
      2: '#F20CA0',
      3: '#F870C7',
      4: '#F979B8',
      5: '#FF7979',
    },
    white: {
      0: '#FFFFFF',
    },
    black: {
      0: '#000000',
    },
    purple: {
      0: '#7C4EFF',
      1: '#AD90FF',
      2: '#FF33FF',
      3: '#FF00FF',
      4: '#0000FF',
      5: '#5985FC',
    },
    yellow: {
      0: '#EEBF18',
    },
    warmRed: {
      0: '#FF5C5D',
      1: '#FF7879',
    },
    blue: {
      0: '#00C2FF',
      1: '#0960D8',
      2: '#66DAFF',
      3: '#4F96F7',
    },
    blueSky: {
      0: '#009BF3',
      1: '#0EA8FF',
    },
    gray: {
      0: '#67738B',
      1: '#99A1B3',
    },
    turquoiseGreen: {
      0: '#008768',
      1: '#00B78D',
    },
    brown: {
      0: '#826657',
      1: '#B49C8F',
    },
    raspberry: {
      0: '#E6135B',
    },
    diodeBlue: {
      0: '#004FBA',
      1: '#5DA2FF',
    },
    greenGrass: {
      0: '#15AC2D',
      1: '#17C032',
    },
    terracotta: {
      0: '#AD633D',
      1: '#CC7447',
    },
    lilac: {
      0: '#B51EFF',
      1: '#D47CFF',
    },
    teal: {
      0: '#22A69A',
      1: '#25B5A8',
    },
    cherry: {
      0: '#BD185B',
      1: '#EE7CAA',
    },
  },
};
