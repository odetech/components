import { Theme } from '@storybook/theming';
import isPropValid from '@emotion/is-prop-valid';
import { transparentize } from 'polished';
import {
  Dropdown, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledTooltip,
} from 'reactstrap';
import styled from '@emotion/styled';
import { defaultTheme } from './themes/index';
import { ThemeProps } from './types';

export type SvgProps = {
  cssWidth?: string;
  cssHeight?: string;
  fill?: string;
  stroke?: string;
};

export const StyledIcon = styled('svg')<SvgProps>`
  width: ${({ cssWidth }) => (cssWidth || '16px')};
  height: ${({ cssHeight }) => (cssHeight || '16px')};
  fill: ${({ fill }) => fill};
  transition: fill .3s;
`;

export const DefaultAppStyles = styled.div<{ theme: Theme }>`
  font-family: 'Poppins', sans-serif !important;
  color: ${({ theme }) => theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar};          
  font-weight: 500;
  font-size: 15px;
  line-height: 22px;
  background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};            
  height: 100%;
  min-height: 100vh;
  max-height: 100%;

  @font-face {
    font-family: "Poppins";
    src: url("../../src/styles/fonts/Poppins-Light.ttf") format("truetype");
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
  }

  @font-face {
    font-family: "Poppins";
    src: url("../../src/styles/fonts/Poppins-Regular.ttf") format("truetype");
    font-weight: 400;
    font-style: normal;
    font-stretch: normal;
  }

  @font-face {
    font-family: "Poppins";
    src: url("../../src/styles/fonts/Poppins-Medium.ttf") format("truetype");
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
  }

  @font-face {
    font-family: "Poppins";
    src: url("../../src/styles/fonts/Poppins-SemiBold.ttf") format("truetype");
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
  }

  @font-face {
    font-family: "Poppins";
    src: url("../../src/styles/fonts/Poppins-ExtraBold.ttf") format("truetype");
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
  }

  img {
    max-width: 100%;
  }

  .content-pdf-button {
    display: flex;
    align-items: center;
    overflow: hidden;
    width: 100%;

    img {
      height: 24px;
      width: 24px;
      margin-right: 4px;
    }

    span {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    &:hover {
      text-decoration: underline;
    }
  }

  div:has(> #timeTrackerPanelTooltip) > button {
    border: 0;
  }
`;

/* @Deprecated */
export const VerticalScrollbarStyles = `
  overflow-y: auto;

  &::-webkit-scrollbar-track {
    border-radius: 2px;
    width: 6px;
    background-color: ${defaultTheme.background.surfaceContainerVar};
  }

  &::-webkit-scrollbar {
    border-radius: 2px;
    width: 6px;
    background-color: ${defaultTheme.background.surfaceContainerVar};
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 2px;
    width: 6px;
    background-color: ${transparentize(0.5, defaultTheme.text.onSurfaceVar)};
  }
`;

export const getVerticalScrollbarStyles = (theme: ThemeProps, addOverflowYAuto = true) => `
  ${addOverflowYAuto && 'overflow-y: auto;'}

  &::-webkit-scrollbar-track {
    border-radius: 2px;
    width: 6px;
    background-color: ${theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar};
  }

  &::-webkit-scrollbar {
    border-radius: 2px;
    width: 6px;
    background-color: ${theme?.background?.surfaceContainerVar || defaultTheme.background.surfaceContainerVar};
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 2px;
    width: 6px;
    background-color: ${transparentize(0.5, theme?.text?.onSurfaceVar || defaultTheme.text.onSurfaceVar)};    
  }
`;

// Dropdown components start
export const StyledDropdown = styled(Dropdown)`
  .btn {
    width: 100%;
    background: transparent;
    border: unset;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};    
    padding: 4px 8px;
    border-radius: 5px;

    svg {
      fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    }

    &:hover, &:active, &:focus {
      color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface} !important;
      background: ${({ theme }) => theme?.background?.surfaceContainerHigh
        || defaultTheme.background.surfaceContainerHigh} !important;      
      outline: none;
      box-shadow: unset !important;
    }
  }
`;

export const StyledDropdownToggle = styled(DropdownToggle)`
  display: flex;
  align-items: center;
`;

export const StyledDropdownMenu = styled(DropdownMenu)`
  box-shadow: ${({ theme }) => theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};
  border: unset;
  padding: 10px 0;
  width: 100%;
  max-width: 180px;  
  min-width: 11rem;
  transition: 0s;
`;

export const StyledDropdownItem = styled(DropdownItem)`
  position: relative;
  z-index: 2;
  font-weight: 500;
  font-size: 14px !important;
  line-height: 21px;
  padding: 6px 24px !important;
  margin: 2px 0;
  color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};    

  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }

  &:hover, &:active, &:focus {
    background: ${({ theme }) => theme?.background?.surfaceContainerVar
      || defaultTheme.background.surfaceContainerVar};    
    outline: none;  
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
  }

  svg {
    fill: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};
    margin-right: 10px;
  }
`;

export const StyledTooltip = styled(UncontrolledTooltip)`
  .tooltip {
    opacity: unset !important;
    
    .arrow::before {      
      border: unset;
    }
  }

  .tooltip-inner {
    max-width: 228px;
    padding: 8px 12px;
    border-radius: 8px;
    color: ${({ theme }) => theme?.text?.onSurface || defaultTheme.text.onSurface};    
    background: ${({ theme }) => theme?.background?.surface || defaultTheme.background.surface};        
    display: flex;
    flex-direction: column;
    gap: 8px;
    box-shadow: ${({ theme }) => theme?.shadow?.inner?.[1] || defaultTheme.shadow.inner[1]};
    text-align: left;
    font-size: 12px;
    font-weight: 400;
    line-height: 16px;

    img {
      height: 28px;
    }

    svg {
      width: 24px;
      height: 24px;
    }
  }
`;

export type CardProps = {
  padding?: string,
  borderRadius?: string,
  addBorder?: boolean,
  shadow?: string,
};

export const Card = styled('div', {
  shouldForwardProp: isPropValid,
})<CardProps>`
  background-color: ${({ theme }) => theme?.background?.surfaceContainer
  || defaultTheme.background.surfaceContainer};    
  padding: ${({ padding }) => padding || '12px 16px'};
  box-shadow: ${({ theme, shadow }) => shadow || theme?.shadow?.drop?.[1] || defaultTheme.shadow.drop[1]};  
  position: relative;  

  &:not(:last-child) {
    margin-bottom: 20px;
  }

  @media screen and (min-width: 769px) {
    border-radius: ${({ borderRadius }) => borderRadius || '15px'};
    padding: ${({ padding }) => padding || '20px'};
  }

  ${({ addBorder, theme }) => addBorder && `border: 0.5px solid ${theme.outline[100]};`}
`;
