type ColorShades = {
  900: string;
  800: string;
  700: string;
  600: string;
  500: string;
  400: string;
  300: string;
  200: string;
  100: string;
  50: string;
};

type ColorShadesWithExtra = ColorShades & {
  850: string;
  0: string;
};

type HueShades<T extends string> = Record<T, string>;

type Hue = {
  orange: HueShades<'0' | '1' | '2' | '3'>;
  green: HueShades<'0' | '1' | '2' | '3'>;
  pink: HueShades<'0' | '1' |'2' | '3' | '4' | '5'>;
  white: HueShades<'0'>;
  black: HueShades<'0'>;
  purple: HueShades<'0' | '1' | '2' | '3' | '4' | '5'>;
  yellow: HueShades<'0'>;
  warmRed: HueShades<'0' | '1'>;
  blue: HueShades<'0' | '1' | '2' | '3'>;
  blueSky: HueShades<'0' | '1'>;
  gray: HueShades<'0' | '1'>;
  turquoiseGreen: HueShades<'0' | '1'>;
  brown: HueShades<'0' | '1'>;
  raspberry: HueShades<'0'>;
  diodeBlue: HueShades<'0' | '1'>;
  greenGrass: HueShades<'0' | '1'>;
  terracotta: HueShades<'0' | '1'>;
  lilac: HueShades<'0' | '1'>;
  teal: HueShades<'0' | '1'>;
  cherry: HueShades<'0' | '1'>;
}

export type HueKey = keyof Hue;

type Visualisation = {
  [
    K in Extract<HueKey, 'purple'
    | 'yellow'
    | 'warmRed'
    | 'blueSky'
    | 'gray'
    | 'pink'
    | 'turquoiseGreen'
    | 'brown'
    | 'raspberry'
    | 'diodeBlue'
    | 'greenGrass'
    | 'terracotta'
    | 'lilac'
    | 'teal'
    | 'cherry'
    | 'white'
    | 'black'
  >
]: string;
};

type GradientBound = {
  start: string,
  end: string
}

type Gradient = {
  [K in Extract<HueKey, 'orange' | 'purple' | 'green' | 'pink' | 'blue'>]: GradientBound;
};

export type ThemeColors = {
  blue: ColorShades;
  green: ColorShades;
  red: ColorShades;
  primary: ColorShades;
  secondary: ColorShades;
  grey: ColorShadesWithExtra;
  hue: Hue,
};

export type ThemeProps = {
  brand: {
    primary: string;
    primaryContainer: string;
    primaryContainerVar: string;
    secondary: string;
    secondaryContainer: string;
    secondaryContainerSecond: string;
  },
  text: {
    onSurface: string;
    onSurfaceVar: string;
    onSurfaceVarSecond: string;
    onSurfaceDisabled: string;
    onPrimary: string;
    onSecondary: string;
  },
  background: {
    surface: string;
    surfaceContainer: string;
    surfaceContainerVar: string;
    surfaceContainerHigh: string;
    surfaceContainerHighest: string;
  },
  success: {
    primary: string;
    onPrimary: string;
    primaryContainer: string;
    onPrimaryContainer: string;
  },
  error: {
    primary: string;
    onPrimary: string;
    primaryContainer: string;
    onPrimaryContainer: string;
  },
  info: {
    primary: string;
    onPrimary: string;
    primaryContainer: string;
    onPrimaryContainer: string;
  },
  outline: {
    100: string;
    200: string;
    300: string;
  },
  visualisation: Visualisation,
  gradient: Gradient,
  shadow: {
    drop: {
      1: string;
      2: string;
      3: string;
    };
    inner: {
      1: string
    }
  }
};

export const ThemesKey = {
  Light: 'light',
  Dark: 'dark',
} as const;

export type ThemeKey = typeof ThemesKey[keyof typeof ThemesKey];

export type Themes = {
  [ThemesKey.Light]: ThemeProps;
  [ThemesKey.Dark]?: ThemeProps;
};
